collection_name = 50_community

UID := $(shell id --user)

ifeq ($(UID),0)
    prefix = /usr/local
    datadir = $(prefix)/share/games
else
    ifeq ($(XDG_DATA_HOME),)
        XDG_DATA_HOME := $(HOME)/.local/share
    endif
    prefix = $(XDG_DATA_HOME)
    datadir = $(prefix)
endif
collections_path = $(DESTDIR)$(datadir)/play.it/collections

# Install the game scripts

.PHONY: install uninstall

install:
	install -D --mode=755 --target-directory=$(collections_path)/$(collection_name) games/*

uninstall:
	rm $(collections_path)/$(collection_name)/play-*.sh
	rmdir -p --ignore-fail-on-non-empty $(collections_path)/$(collection_name)

# Generate tarballs, for easier packaging

.PHONY: dist

## The generated tarball is signed with gpg by default,
## NO_SIGN should be set to a non-0 value to skip the signature.
NO_SIGN := 0

dist: DATE := $(shell date +%F)
dist: TARBALL := $(collection_name)_$(DATE).tar.gz
dist: TAR_OPTIONS := --sort=name --mtime=2023-08-25 --owner=root:0 --group=root:0 --use-compress-program='gzip --no-name'
dist: games/*.sh LICENSE Makefile README.md
	mkdir --parents dist
	LC_ALL=C tar cf dist/$(TARBALL) $(TAR_OPTIONS) games/*.sh LICENSE Makefile README.md
ifeq ($(NO_SIGN),0)
	rm --force dist/$(TARBALL).asc
	gpg --armor --detach-sign dist/$(TARBALL)
endif

# Run syntax checks, relying on ShellCheck

GAME_SCRIPTS := $(wildcard games/play-*.sh)
GAME_SCRIPTS_TESTS := $(addprefix shellcheck_, $(GAME_SCRIPTS))
.PHONY: check $(GAME_SCRIPTS_TESTS)
check: $(GAME_SCRIPTS_TESTS)

$(GAME_SCRIPTS_TESTS): SHELLCHECK_SOURCES := /usr/share/games/play.it:/usr/share/play.it
## This is a unicode quote. Delete and retype it (or ignore/doublequote for literal).
$(GAME_SCRIPTS_TESTS): SHELLCHECK_EXCLUDE += --exclude=SC1112
## Expressions don't expand in single quotes, use double quotes for that.
$(GAME_SCRIPTS_TESTS): SHELLCHECK_EXCLUDE += --exclude=SC2016
## foo appears unused. Verify it or export it.
$(GAME_SCRIPTS_TESTS): SHELLCHECK_EXCLUDE += --exclude=SC2034
## Don't use variables in the printf format string. Use printf '..%s..' "$foo".
$(GAME_SCRIPTS_TESTS): SHELLCHECK_EXCLUDE += --exclude=SC2059
## In POSIX sh, 'local' is undefined.
$(GAME_SCRIPTS_TESTS): SHELLCHECK_EXCLUDE += --exclude=SC3043
$(GAME_SCRIPTS_TESTS): shellcheck_%: %
	shellcheck --extended-analysis=false --external-sources --source-path=$(SHELLCHECK_SOURCES) $(SHELLCHECK_EXCLUDE) $<
