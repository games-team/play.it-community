# ./play.it community-maintained games collection

The codebase is maintained at [https://git.dotslashplay.it/games-community/]

Bug reports and feature requests are tracked at [https://forge.dotslashplay.it/play.it/games/-/issues]

[https://git.dotslashplay.it/games-community/]: https://git.dotslashplay.it/games-community/
[https://forge.dotslashplay.it/play.it/games/-/issues]: https://forge.dotslashplay.it/play.it/games/-/issues

## Description

./play.it is a free software building native packages from installers for
Windows or Linux, mainly those sold by stores focusing on DRM-free games
distribution. The goal is that a game installed via ./play.it is
indistinguishable from a game installed via the official repositories of your
favourite distribution.

This specific repository provides game scripts that can be used with ./play.it
to install a collection of DRM-free games. It is maintained by the same team
than the ./play.it library and tools.

## Installation

### Distributions providing ./play.it

The following distributions provide installation instructions in their official documentation:

- [Debian]
- [Gentoo]
- [Ubuntu] (French article)

[Debian]: https://wiki.debian.org/Games/PlayIt#Installation
[Gentoo]: https://wiki.gentoo.org/wiki/Play.it#Installation
[Ubuntu]: https://doc.ubuntu-fr.org/play.it#installation

In most cases, these instructions should work in the same way for derivatives of these distributions.

### Installation from git

If your distribution does not already have a package for ./play.it, you can
install it from this git repository.

```
git clone --branch main --depth 1 https://git.dotslashplay.it/games-community play.it-community.git
cd play.it-community.git
make install
```

## Usage

Usage instructions are provided on the main ./play.it repository:
[./play.it: Installer for DRM-free commercial games — Usage]

[./play.it: Installer for DRM-free commercial games — Usage]: https://git.dotslashplay.it/scripts/about/#usage

## Contributing

### First contribution

There is no real rule for your first contribution. You can host your updated code anywhere you like and contact us via any way described in the [Contact information] section below. It is OK to expose your updated code through GitHub or GitLab.com if you have nothing better at hand, but we would be thankful if you would instead use some hosting outside of these big silos.

[Contact information]: #contact-information

We do not enforce any workflow like merge/pull requests or any such thing. We are interested in the result of your work, not in how you got to it. So shatter your shackles, and for once take pleasure in working *however you like*! You are not even required to use git if you do not know or do not like this tool.

Please try to follow these simple guidelines, and your contribution is probably going to be accepted quickly:

- Run `make check` before submitting your code, to ensure you did not break anything by accident;
- Use tabs for indentation. No real developer would ever indent anything with spaces.

### Regular contributions

If you keep contributing on a more regular basis (sending 3 patches in the same year would already make you a regular) we can grant you direct write access to the repositories hosted at git.dotslashplay.it. This is not mandatory, if for some reason you can not or do not want to work with git you can simply keep following the "First contribution" guidelines, and forget about this whole "Regular contributions" section.

To grant you such an access we need nothing but a public SSH key, that you can send us through any way described in the [Contact information] section below.

[Contact information]: #contact-information

Once you have been granted access, you should add the following to your local SSH configuration:

```
Host git.dotslashplay.it
    Port 1962
    User gitolite3
```

You should then update the remote of your local repository, with the following command (assuming a remote named "upstream"):

```
git remote set-url --push upstream ssh://git.dotslashplay.it/games-community
```

Since these repositories are worked on by several people, there are a couple extra guidelines that you should follow:

- Your work should always be pushed to a dedicated branch, never on the main branch;
- Game script updates, including bug fixes, should be pushed to branches named with a "update/" prefix;
- Addition of new game scripts should be pushed to branches named with a "new/" prefix;
- You are allowed to push code to branches opened by other contributors, but please communicate with them if you plan to do so;
- Force push and branches deletion are not allowed, if you want a branch to be deleted please ask us to do it for you.

## Supported games

- 1365
- 1954 Alcatraz
- 6180 The Moon
- 80 Days
- A Bewitching Revolution
- A Boy and His Blob
- A Hat in Time
- A House of Many Doors
- A Jugglerʼs Tale
- A Lullaby of Colors
- A Musical Story
- A Plague Tale: Innocence
- A Short Hike
- A Story About My Uncle
- A Tale of Paper: Refolded (full game and free demo)
- Abandon Ship
- Abzû
- Accelerate
- Action Henk
- AER: Memories of Old
- Affinity
- Afterdream
- Afterlife
- Age of Wonders series:
    - Age of Wonders
    - Age of Wonders: Planetfall
- Airline Tycoon Deluxe
- AI War 2
- Akalabeth: World of Doom
- Alan Wake’s American Nightmare
- Alba: A Wildlife Adventure
- Amnesia: The Dark Descent
- Among the Sleep
- Anarcute
- Anima: Gate of Memories
- Anno series:
    - Anno 1404
    - Anno 1503
- Anodyne
- Anomaly series:
    - Anomaly: Warzone Earth
    - Anomaly 2
    - Anomaly Defenders
- Ape Out
- Apocalipsis
- Aquamarine
- Aquaria
- Aragami
- Arcanum: Of Steamworks and Magick Obscura
- Armikrog
- Art of Fighting 2
- Ascendant
- As Far As The Eye
- Ash of Gods
- Assassinʼs Creed
- Assault Android Cactus
- Asura
- Atlantis: The Lost Tales
- Atone: Heart of the Elder Tree
- Aven Colony
- Aztez
- Backbone
- Bad Dream series:
    - Bad Dream: Coma
    - Bad Dream Fever
- Bad Mojo
- Banished
- Baseball Stars 2
- Basingstoke
- Batman: Arkham series:
    - Batman: Arkham Asylum
    - Batman: Arkham City
    - Batman: Arkham Origins
    - Batman: Arkham Knight
- Battle Chef Brigade
- Battlefleet Gothic: Armada
- Battle Worlds: Kronos
- Beacon Pines
- Bear and Breakfast
- Beatbuddy: Tale of the Guardians
- Beat Cop
- Bee Simulator
- Beholder series:
    - Beholder
    - Beholder 2
- Besiege
- BFF or Die
- Binaries
- Bio Menace
- Biped
- BIT RAT: Singularity
- Bit.Trip games:
    - Bit.Trip BEAT
    - Bit.Trip Runner
    - Bit.Trip Presents… Runner2: Future Legend of Rhythm Alien
    - Bit.Trip: Runner 3
- Blade Runner
- Bleed series:
    - Bleed
    - Bleed 2
- Blind Men
- Blocks that matter
- Bombshell
- Book of Demons
- Book of Hours
- Boxville
- Braid
- Braveland series:
    - Braveland
    - Braveland Pirate
    - Braveland Wizard
- Breach & Clear:
    - Breach & Clear
    - Breach & Clear - DEADline
- Broforce
- Brothers: A Tale of Two Sons
- Brütal Legend
- Bubbles the Cat
- Butcher (full game and free demo)
- Calico
- Call of Cthulhu adventure games:
    - Prisoner of Ice
    - Shadow of the Comet
- Candleman
- Capsized
- Caravan
- Card City Nights
- Carto (full game and free demo)
- Cat Quest series:
    - Cat Quest
    - Cat Quest 2
- Cats are Liquid series:
    - A Light in the Shadows
    - A Better Place
- Cayne
- Celeste
- Chained Echoes
- Chaos Reborn
- Charterstone
- Chroma Squad
- Chronology
- Chuchel
- Cineris Somnia
- Civilization 4
- Cleo - A Pirate’s Tale
- Cloud Gardens
- Codemancer
- Coffee Noir - Business Detective Game (full game and free demo)
- Commandos 3: Destination Berlin
- Convoy
- Cook, Serve, Delicious! 2!!
- Cornerstone: The Song of Tyrim
- Costume Quest
- Cotrio
- Creaks
- Creepy Tale series:
    - Creepy Tale
    - Creepy Tale 2
- CrossCode (full game and free demo)
- Crossing Souls (free demo only)
- Cryptark
- Crypt of the NecroDancer:
    - Crypt of the NecroDancer
    - Amplified
- Cube Escape
- Dark Echo
- Darksiders 2
- Darwinia
- Dawnmaker
- Deadbolt
- Dead Cells:
    - Dead Cells
    - Rise of the Giant
    - The Bad Seed
- Deadlight
- Dear Esther
- Death’s Door
- Death’s Life
- Deep Sky Derelicts
- Defenderʼs Quest: Valley of the Forgotten
- Delores: A Thimbleweed Park Mini-Adventure
- Deltarune - Chapter 1
- Detention
- Dex
- Dicey Dungeons
- Dinʼs Curse
- Disney 1994 games:
    - Aladdin
    - The Jungle Book
    - The Lion King
- Divinity series:
    - Divine Divinity
    - Divinity: Original Sin
- Doctor Who: The Lonely Assassins
- Donʼt Starve:
    - Donʼt Starve
    - Reign of Giants
    - Shipwrecked
- Donut County
- Door Kickers
- Dorfromantik
- Dracula: Love Kills
- Dragon Age: Origins
- Dragonsphere
- Dream
- Dreaming Sarah
- D: The Game
- Duet
- Duke Nukem 3D
- Dungeons 2:
    - Dungeons 2
    - A Chance of Dragons
    - A Song of Sand and Fire
- Dungeons & Lesbians
- Dust: An Elysian Tail
- Dustforce DX
- Earthlock
- Earthworm Jim
- The Indifferent Wonder of an Edible Place
- Element4l
- Elephantasy
- else Heart.Break()
- Empire Earth:
    - Empire Earth
    - The Art of Conquest
- Encodya (full game and free demo)
- Endless Space
- Endzone: A World Apart
- Enter the Gungeon
- Epistory - Typing Chronicles
- Equaboreal 12.21
- Escape Goat series:
    - Escape Goat
    - Escape Goat 2
- Eschalon: Book 1
- Ether One
- Etherlords
- Europa Universalis 2
- Evan’s Remains
- Everspace
- Everything
- Everything is Garbage
- Evoland series:
    - Evoland
    - Evoland 2, A Slight Case of Spacetime Continuum Disorder
- Factory Town
- Fall of Porcupine
- Fallout series:
    - Fallout
    - Fallout 2
    - Fallout Tactics
    - Fallout: New Vegas
- Fantasy General
- Farabel
- Far From Noise
- FAR: Lone Sails
- Fatal Fury Special
- Fear Equation
- Feist
- Fez
- Figment series:
    - Figment
    - Figment 2: Creed Valley
- FixFox
- Flashout 3D: Enhanced Edition
- Flat Kingdom
- FlatOut 2
- Florence
- Flower
- Flux Caves
- Forced
- Forgotton Anne
- Fossil Echo
- Fotonica
- Four Sided Fantasy
- Fox & Flock
- Fragments of Euclid
- Fruits of a Feather
- Fugue
- Full Throttle Remastered
- Gabriel Knight 1: Sins of the Fathers
- Gangsters: Organized Crime
- Garden In!
- Gathering Sky
- GemCraft - Frostborn Wrath
- Ghost of a Tale
- Giana Sisters series:
    - Giana Sisters: Twisted Dreams
    - Giana Sisters: Rise of the Owlverlord
- Giants: Citizen Kabuto
- Glittermitten Grove
- Gnomoria
- Godhood
- Golden Treasure
- Gorogoa
- Graveyard Keeper
- GreedCorp
- Grip
- Gris
- Growbot
- Grow: Song of the Evertree
- Guide of the Butterfly
- Gun-Toting Cats
- Habitat
- Hacknet
- Haimrik
- Halcyon 6
- Hammerwatch
- Hand Of Fate
- Hard West
- Haven
- Headspun
- Heal
- Heavenʼs Vault
- Helium Rain
- Hellblade: Senuaʼs Sacrifice
- Helltaker
- Help Will Come Tomorrow
- Her Majestyʼs SPIFFING
- Heroes Chronicles:
    - Chapter 1 - Warlords of the Wasteland
    - Chapter 2 - Conquest of the Underworld
    - Chapter 3 - Masters of the Elements
    - Chapter 4 - Clash of the Dragons
    - Chapter 5 - The World Tree
    - Chapter 6 - The Fiery Moon
    - Chapter 7 - Revolt of the Beastmasters
    - Chapter 8 - The Sword of Frost
- Heroes of Might and Magic series:
    - Heroes of Might and Magic: A Strategic Quest
    - Heroes of Might and Magic 2: The Price of Loyalty
    - Heroes of Might and Magic 3
- Heroine’s Quest: The Herald of Ragnarok
- Her Story
- Hitman: Codename 47
- Hiveswap: Act 1
- Hive Time
- Hoa
- Hob
- Hollow Knight
- Homeseek
- Horizon Zero Dawn
- Hotline Miami
- Hot Pot Panic
- Hot Tin Roof: The Cat That Wore a Fedora
- Hounds of Valor (demo)
- Hugo 1: Hugo’s House of Horrors
- HuniePop
- Iconoclasts
- Ikenfell
- Impossible Creatures
- inbento
- Indivisible
- Infinium Strike
- Inkulinati (full game and free demo)
- Inner
- Inner Space
- Intrusion 2
- Invisible Inc.
- Iratus: Lord of the Dead
- Iris and the Giant
- Ironclad Tactics
- Itorah
- Ixion
- Jars
- Jazzpunk
- Jotun
- Journey of a Roach
- Journey to the Savage Planet
- Just Cause 2
- Just one, must choose
- JYDGE
- Kapital: Sparks of Revolution
- Kerbal Space Program
- Kero Blaster
- Kind Words
- Kingdom Come: Deliverance:
    - Kingdom Come: Deliverance
    - A woman's lot
    - Band of bastards
    - From the ashes
    - HD Sound pack
    - HD Texture pack
    - HD Voice pack
    - The amorous adventures of bold sir Hans Capon
    - Treasures of the past
- Kingdom New Lands
- King of the Monsters
- King’s Bounty: The Legend
- Kingsway
- Knights of Pen and Paper
- Kona
- Kraken Academy
- Kyn
- La•Mulana
- Last Day of June
- Layers of Fear
- Legend of Grimrock
- Legend of Keepers:
    - Legend of Keepers: Prologue (free demo)
    - Legend of Keepers: Career of a Dungeon Manager (full game)
- Lego Batman: The Videogame
- Lego Harry Potter: Years 1-4
- Lennaʼs Inception
- Lieve Oma
- Lilaʼs Sky Ark
- Limbo
- Litil Divil
- Little Big Adventure series:
    - Little Big Adventure
    - Little Big Adventure 2
- Little Bug
- Little Nightmares 1
- Location Withheld
- Lo-Fi Room
- Lonesome Village
- Long Live the Queen
- Lords of Xulima
- Lorelai
- Loria (full game and free demo)
- Lost Ruins
- Lost Words: Beyond the Page
- Lovers in a Dangerous Spacetime
- Lumini
- Lumino City
- Lumo
- Luna
- LUNA The Shadow Dust
- Lynn, The Girl Drawn On Puzzles
- Mable & the Wood
- Maniac Mansion
- Man Oʼ War: Corsair - Warhammer Naval Battles
- Marble Age
- Mark of the Ninja:
    - Mark of the Ninja
    - Mark of the Ninja - Special Edition
- Master of Magic
- Master of Orion 2: Battle at Antares
- Medieval Dynasty
- Melodyʼs Escape
- Mesmer
- Metal Slug series:
    - Metal Slug 2
    - Metal Slug 3
- Mhakna Gramura and Fairy Bell
- Mimo
- Minecraft 4K
- Mini Metro
- Minit
- Minute of Islands
- Mirrorʼs Edge
- Mobius
- Momodora: Reverie Under the Moonlight
- Monad
- Mon-cuties for All
- Monster Prom
- Monster Sanctuary
- Monstrata Fracture
- Moon Hunters
- Morphopolis
- Mortal Kombat series:
    - Mortal Kombat 1
    - Mortal Kombat 2
    - Mortal Kombat 3
- Multiwinia
- Museum of Mechanics: Lockpicking
- Mushroom 11
- Mutazione
- My Memory of Us
- Oh Jeez, Oh No, My Rabbits Are Gone !!!
- My Time at Portia
- Nanotale - Typing Chronicles
- Nebuchadnezzar
- Neon Code
- neon Drive
- Neurodeck: Psychological Deckbuilder
- Never Alone
- Neverending Nightmares
- Neverwinter Nights:
    - Neverwinter Nights
    - Neverwinter Nights: Enhanced Edition
- Niche
- Night in the Woods
- Nihilumbra
- Noctropolis
- Noita
- Nomads of Driftland
- No Manʼs Sky
- Nonsense at Nightfall
- No Pineapple Left Behind
- Nowhere Prophet
- NyxQuest: Kindred Spirits
- Obduction
- Ocean’s Heart
- Octodad Dadliest Catch
- Oddworld: Abeʼs Oddysee
- Old World
- One Hand Clapping
- Order of the Thorne: The Kingʼs Challenge
- Ori and the Blind Forest
- Osmos
- Our Life: Beginnings & Always
- Out of Line
- Out There: Ω Edition
- Overboard!
- Overgrowth
- Overload
- Owlboy
- Pacapong
- Pandemonium!
- Panmorphia
- Pan-Pan
- Paper Sorcerer
- Papers, Please
- Papetura
- Papo and Yo
- Paradise Killer
- Paramedium: A Noise in the Attic
- Particulars
- Pavilion
- Peck N Run
- Pendula Swing: The Complete Journey
- Perimeter
- Phoning Home
- Pid
- Pier Solar and the Great Architects
- Pikuniku
- Pilgrims
- Pine
- Pink Heaven
- Pink Hour
- Pinstripe
- Pirates:
    - Pirates!
    - Pirates Gold
- Pixel Piracy
- Planet Alpha
- Plant Daddy
- Pokemon Insurgence
- Political Animals
- Poly Bridge
- Populous series:
    - Populous: Promised Lands
    - Populous 2
    - Populous: The Beginning
- Potion Craft: Alchemy simulator (full game and free demo)
- Prince of Persia (2008)
- Prison Architect
- Privateer
- Project Feline
- Proteus
- Psychonauts
- Puddle
- Pulse
- Q.U.B.E. 2
- Quench
- Quern - Undying Thoughts
- Quiet as a Stone
- Race The Sun:
    - Race The Sun
    - Sunrise
- Ravenous Devils
- Reassembly
- Rebel Galaxy
- République
- Restless Soul
- Retrace
- Retro City Rampage
- Reventure
- RiME
- Rimworld
- Risk of Rain
- Road 96
- Robin Hood: The Legend of Sherwood
- Rogue legacy
- Röki
- Rymdkapsel
- Sable
- Sagebrush
- Sally Face
- Samurai Shodown 2
- Satellite Reign
- Scarf
- Scarlet Hood and the Wicked Wood
- Seasons After Fall
- Secrets of Raetikon
- Sengoku 3
- Sentris
- Shantae and the Pirateʼs Curse
- Shelter series:
    - Shelter
    - Shelter 2
    - Paws: A Shelter 2 Game
- Shovel Knight: Treasure Trove
- Sigma Theory: Global Cold War
- Six Cats Under
- Skullgirls
- Skyshineʼs Bedlam
- Slay the Spire
- Slime Rancher
- Slipways
- Softporn Adventure
- Solar 2
- Solar Flux
- Solstice
- SOMA
- Sort the Court
- South Scrimshaw, Part One
- Sovereign Syndicate
- Spacecom
- Space Pirates and Zombies
- Space Tail: Every Journey Leads Home
- Spellforce
- Spelunky
- Spiritfarer
- Spore
- SpringBack
- Stardew Valley
- Stargunner
- Starship Titanic
- Starship Traveller
- Star-Twine
- Star Vikings (free demo only)
- Star Wars Battlefront 2
- SteamWorld games:
    - SteamWorld Dig
    - SteamWorld Dig 2
    - SteamWorld Heist
    - SteamWorld Quest
- Steel Rats
- Sticky Business
- Still There
- Stories: The Path of Destinies
- Strafe
- Strike Suit Zero
- Sudden Strike 4:
    - Sudden Strike 4
    - Africa: Desert War
    - Battle of Kursk
    - Finland: Winter Storm
    - Road to Dunkirk
    - The Pacific War
- Sumatra: Fate of Yandi
- Summer in Mara
- Sunblaze
- Sundered
- Sunlight
- Superhot
- Super Meat Boy
- Supercow
- Surviving Mars:
    - Surviving Mars
    - Digital Deluxe
    - Mysteries Resupply Pack
    - Stellaris Dome Set
- Symmetry
- Symphonia
- Symphony
- System Shock 2
- Tag: The Power of Paint
- Tales of the Neon Sea
- Talk to Me
- Tandem: A Tale of Shadows
- Tangledeep
- Teen Agent
- Tengami
- Terraria
- Teslagrad
- Tetrobot and Co.
- The 7th Guest
- Thea 2: The Shattering
- The Adventures of Shuggy
- The Aquatic Adventure of the Last Human
- The Bardʼs Tale 4
- The Binding of Isaac:
    - Rebirth
    - Afterbirth
    - Afterbirth +
    - Repentance
- The Coma
- The Council
- The Deer God
- The Difference Between Us
- The Dig
- The Elder Scrolls series:
    - The Elder Scrolls: Arena
    - The Elder Scrolls 2: Daggerfall
    - The Elder Scrolls 5: Skyrim
    - The Elder Scrolls 5: Skyrim - Anniversary Upgrade
- The Fall
- The Flame in the Flood
- The Girl and the Robot
- The Great Perhaps
- The Guild:
    - The Guild 2
    - The Guild 2 Renaissance
- The Hive
- The King of Fighters 2000
- The Kingʼs Bird
- The Last Tinker: City of Colors
- The Longing
- Theme Hospital
- The Night Fisherman
- The Planet Crafter
- The Red Strings Club
- There is no Game: Wrong Dimension
- The Search
- The Settlers 3
- The Silver Case (free demo)
- The Smurfs: Mission Vileaf
- The Sojourn
- The Stanley Parable
- The Stillness of the Wind
- The Subject
- The Swords of Ditto: Mormoʼs Curse
- The Temple of Elemental Evil
- The Vanishing of Ethan Carter
- The Warlock of Firetop Mountain
- The Westport Independent
- The Witcher series:
    - The Witcher 2: Assassins of Kings
    - The Witcher 3: Wild Hunt
- The World Begins With You
- The World Next Door
- Thief 3: Deadly Shadows
- This War of Mine:
    - This War of Mine
    - The Little Ones
    - Fading Embers
    - Fatherʼs Promise
    - The Last Broadcast
- Thomas Was Alone
- Tidalis
- Time Loader
- Tiny and Big: Grandpaʼs Leftovers
- Tiny Hunter
- To Be or Not To Be
- Toki Tori 2
- Tooth and Tail
- Toren
- Total Annihilation: Kingdoms
- Touhou series:
    - Touhou Chireiden ~ Subterranean Animism (free demo only)
    - Touhou Seirensen ~ Undefined Fantastic Object (free demo only)
    - Touhou Touhou Shinreibyou ~ Ten Desires (free demo only)
    - Touhou Kishinjou ~ Double Dealing Character (free demo only)
    - Touhou Hifuu Nightmare Diary ~ Violet Detector
    - Touhou Koumakyou ~ the Embodiment of Scarlet Devil (free demo only)
    - Touhou Kaeizuka ~ Phantasmagoria of Flower View (free demo only)
- Tower of Guns
- Townscaper
- TrackMania Nations Forever
- Trine series:
    - Trine
    - Trine 2
- Triple Triad Gold
- Trüberbrook
- Tulpa
- TUNIC
- Turnip Boy Commits Tax Evasion
- Tzar: The Burden of the Crown
- Ultima 4: Quest of the Avatar
- Undertale
- unEpic
- Unsung Warriors Prologue Prologue
- Unwording
- Urtuk: The Desolation
- Us Lovely Corpses
- Utopias
- Vagrus - The Riven Realms: Prologue
- Valhalla Hills
- Vambrace: Cold Soul
- Vampire: The Masquerade - Redemption
- The Incredible Adventures of Van Helsing: Final Cut
- Vaporum
- Volgarr the Viking
- VVVVVV
- Wandersong
- Warhammer 40,000:
    - Dakka Squadron
    - Mechanicus
- War§ow
- Wayward Manor
- We, the Revolution
- What Happened to Survey Team 4?
- What Remains of Edith Finch
- while True: learn()
- Windosill
- Windward
- Wing Commander series:
    - Wing Commander
    - Wing Commander 2
    - Wing Commander 3
- Wingspan
- Witch Thief
- WitchWay
- World to the West
- Worms United
- Xenonauts
- X-Morph: Defense
- Yoku’s Island Express
- Yono and the Celestial Elephants
- Yooka Laylee
- Ys 1: Ancient Ys Vanished Omen
- Yume Nikki
- Zak McKracken and the Alien Mindbenders
- Ziggurat
- Zniw Adventure
- Zodiacats

## Contact information

### IRC channel

Some ./play.it developers and users can be reached on IRC, channel is `#play.it`
on network `irc.oftc.net`. The main language on this IRC channel is English, but
some of us understand French too.

### E-mail

A contact e-mail for feedback can usually be found in each ./play.it game
script. Open one of these files with any text editor to see the contact e-mail.
