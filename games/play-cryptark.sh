#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 VA
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Cryptark
# send your bug reports to contact@dotslashplay.it
###

script_version=20230207.1

GAME_ID='cryptark'
GAME_NAME='Cryptark'

ARCHIVE_BASE_1='cryptark_en_1_23_22933.sh'
ARCHIVE_BASE_1_MD5='7fbca12cab4fae1a36a365fdf004a678'
ARCHIVE_BASE_1_TYPE='mojosetup'
ARCHIVE_BASE_1_SIZE='700000'
ARCHIVE_BASE_1_VERSION='1.23-gog22933'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/cryptark'

ARCHIVE_BASE_0='cryptark_en_1_2_15203.sh'
ARCHIVE_BASE_0_MD5='53083f1fef847a30eb99914821c8649a'
ARCHIVE_BASE_0_TYPE='mojosetup'
ARCHIVE_BASE_0_SIZE='700000'
ARCHIVE_BASE_0_VERSION='1.2-gog15203'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
Content
monoconfig
monomachineconfig
Cryptark.exe
Cryptark.png
gamecontrollerdb.txt
BrashMonkeySpriterXNA.dll
CommandConsoleSharedTypes.dll
FNA.dll
FNA.dll.config
Lidgren.Network.dll
MSCommon.dll
ProjectMercury.dll
spine-csharp_xna.dll
spine-xna.dll
TexturePackingRuntime.dll'
CONTENT_DOC_MAIN_FILES='
Linux.README'

APP_MAIN_EXE='Cryptark.exe'
APP_MAIN_ICON='Cryptark.png'

PKG_MAIN_DEPS='mono'
PKG_MAIN_DEPENDENCIES_LIBRARIES='
libopenal.so.1
libSDL2-2.0.so.0
libSDL2_image-2.0.so.0'
PKG_MAIN_DEPENDENCIES_MONO_LIBRARIES='
mscorlib.dll
Mono.Posix.dll
Mono.Security.dll
System.dll
System.Configuration.dll
System.Core.dll
System.Data.dll
System.Drawing.dll
System.Numerics.dll
System.Runtime.Serialization.dll
System.Security.dll
System.Web.dll
System.Web.Extensions.dll
System.Xml.dll
System.Xml.Linq.dll'

# Include shipped libraries that can not be replaced by system ones

CONTENT_LIBS_LIBS32_PATH="${CONTENT_PATH_DEFAULT}/lib"
CONTENT_LIBS_LIBS32_FILES='
libmojoshader.so
libtheorafile.so'
CONTENT_LIBS_LIBS64_PATH="${CONTENT_PATH_DEFAULT}/lib64"
CONTENT_LIBS_LIBS64_FILES='
libmojoshader.so
libtheorafile.so'

PACKAGES_LIST='PKG_MAIN PKG_LIBS32 PKG_LIBS64'

PKG_LIBS_ID="${GAME_ID}-libs"

PKG_LIBS32_ID="$PKG_LIBS_ID"
PKG_LIBS32_ARCH='32'

PKG_LIBS64_ID="$PKG_LIBS_ID"
PKG_LIBS64_ARCH='64'

PKG_MAIN_DEPS="$PKG_MAIN_DEPS $PKG_LIBS_ID"

# Load common functions

target_version='2.21'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
