#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Trine 1
# send your bug reports to contact@dotslashplay.it
###

script_version=20250303.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='trine-1'
GAME_NAME='Trine'

ARCHIVE_BASE_0='gog_trine_enchanted_edition_2.0.0.2.sh'
ARCHIVE_BASE_0_MD5='0e8d2338b568222b28cf3c31059b4960'
ARCHIVE_BASE_0_TYPE='mojosetup'
ARCHIVE_BASE_0_SIZE='1500000'
ARCHIVE_BASE_0_VERSION='2.12.508-gog2.0.0.2'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/trine_enchanted_edition'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN_PATH='data/noarch/game/lib/lib32'
CONTENT_LIBS_BIN_FILES='
libCg.so
libCgGL.so
libSDL-1.3.so.0'
CONTENT_GAME_BIN_FILES='
bin/trine1_linux_32bit'
CONTENT_GAME_DATA_FILES='
data
*.fbq
*.png'
CONTENT_DOC_DATA_FILES='
*.txt'

APP_MAIN_EXE='bin/trine1_linux_32bit'
APP_MAIN_ICON='trine1.png'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libasound.so.2
libc.so.6
libdl.so.2
libfreetype.so.6
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libgdk-x11-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libGLU.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libogg.so.0
libpango-1.0.so.0
libpng12.so.0
libpthread.so.0
librt.so.1
libstdc++.so.6
libudev.so.0
libX11.so.6
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game icon

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
