#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2018 Jacek Szafarkiewicz
set -o errexit

###
# Xenonauts
# send your bug reports to contact@dotslashplay.it
###

script_version=20240224.4

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='xenonauts'
GAME_NAME='Xenonauts'

ARCHIVE_BASE_GOG_1_NAME='xenonauts_en_1_65_21328.sh'
ARCHIVE_BASE_GOG_1_MD5='bff1d949f13f2123551a964475ea655e'
ARCHIVE_BASE_GOG_1_SIZE='2900000'
ARCHIVE_BASE_GOG_1_VERSION='1.65-gog21328'
ARCHIVE_BASE_GOG_1_URL='https://www.gog.com/game/xenonauts'

ARCHIVE_BASE_GOG_0_NAME='gog_xenonauts_2.1.0.4.sh'
ARCHIVE_BASE_GOG_0_MD5='7830dee208e779f97858ee81a97c9327'
ARCHIVE_BASE_GOG_0_SIZE='2900000'
ARCHIVE_BASE_GOG_0_VERSION='1.63-gog2.1.0.4'

ARCHIVE_BASE_HUMBLE_0_NAME='Xenonauts-DRMFree-Linux-2016-03-03.sh'
ARCHIVE_BASE_HUMBLE_0_MD5='f4369e987381b84fde64be569fbab913'
ARCHIVE_BASE_HUMBLE_0_SIZE='2700000'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.65-humble160303'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/xenonauts'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='data/noarch'
CONTENT_GAME_BIN_PATH_HUMBLE='data/x86'
CONTENT_GAME_BIN_FILES='
Xenonauts.bin.x86'
## Xenonauts.bin.x86 is linked against libsteam_api.so, so this library can not be dropped.
CONTENT_LIBS_BIN_PATH_GOG="${CONTENT_PATH_DEFAULT_GOG}/lib"
CONTENT_LIBS_BIN_PATH_HUMBLE="${CONTENT_GAME_BIN_PATH_HUMBLE}/lib"
CONTENT_LIBS_BIN_FILES='
libsteam_api.so'
CONTENT_GAME_DATA_FILES='
assets
extras
Icon.*'
CONTENT_DOC_DATA_FILES='
README.linux
*.pdf'

APP_MAIN_EXE='Xenonauts.bin.x86'
APP_MAIN_ICON='Icon.png'
## Skip the launcher, as it can cause a crash related to fonts rendering on some setups.
APP_MAIN_OPTIONS="${APP_MAIN_OPTIONS:-} -nolauncher"
## The game crashes on launch when using the wayland backend of SDL,
## even when using the system-provided build of SDL.
APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}"'
# The game crashes on launch when using the wayland backend of SDL
if [ "${SDL_VIDEODRIVER:-}" = "wayland" ]; then
	unset SDL_VIDEODRIVER
fi
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
libSDL2-2.0.so.0
libstdc++.so.6
libX11.so.6
libXext.so.6
libXinerama.so.1
libXxf86vm.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
