#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Solar 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20240831.5

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='solar-2'
GAME_NAME='Solar 2'

ARCHIVE_BASE_0_NAME='Solar2_v1.26_amd64.tar.gz'
ARCHIVE_BASE_0_MD5='64e953238fbdce8d8671578ddba05559'
ARCHIVE_BASE_0_SIZE='103714'
ARCHIVE_BASE_0_VERSION='1.26-humble1'
ARCHIVE_BASE_0_URL='https://www.humblebundle.com/store/solar-2'

ARCHIVE_BASE_XNA_0_NAME='solar2-linux-1.10_1409159048.tar.gz'
ARCHIVE_BASE_XNA_0_MD5='243918907eea486fdc820b7cac0c260b'
ARCHIVE_BASE_XNA_0_SIZE='130000'
ARCHIVE_BASE_XNA_0_VERSION='1.10-humble1'
ARCHIVE_BASE_XNA_0_URL='https://www.humblebundle.com/store/solar-2'

## Optional icons pack (only useful for the XNA build)
ARCHIVE_OPTIONAL_ICONS_NAME_XNA='solar-2_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_MD5_XNA='d8f8557a575cb5b5824d72718428cd33'
ARCHIVE_OPTIONAL_ICONS_URL_XNA='https://downloads.dotslashplay.it/resources/solar-2/'
CONTENT_ICONS_PATH='.'
CONTENT_ICONS_FILES='
16x16
32x32
48x48
64x64'

# Archives content

CONTENT_PATH_DEFAULT='Solar2'
CONTENT_LIBS_BIN_FILES='
libclrgc.so
libclrjit.so
libcoreclr.so
libcoreclrtraceptprovider.so
libhostfxr.so
libhostpolicy.so
libmscordaccore.so
libmscordbi.so
libSystem.Globalization.Native.so
libSystem.IO.Compression.Native.so
libSystem.Native.so
libSystem.Net.Security.Native.so
libSystem.Security.Cryptography.Native.OpenSsl.so'
## The game crashes on launch if the shipped OpenAL is not used:
##
## Unhandled exception. System.NullReferenceException: Object reference not set to an instance of an object.
##    at Solar2.Moosic..ctor(AudioManager audioManager, PowerMain main)
##    at Solar2.World..ctor(Epsilon epsilon, SpriteSheet[] sheets)
##    at Solar2.Epsilon.LoadContent()
##    at Power.PowerMain.LoadScreen(GameScreen screen, Nullable`1 controllingPlayer)
##    at Solar2.LoadingScreen.BackgroundWorkerThread()
CONTENT_LIBS0_BIN_FILES='
libopenal.so.1'
CONTENT_GAME_BIN_FILES='
Solar2
Solar2.deps.json
Solar2.runtimeconfig.json'
CONTENT_GAME_DATA_FILES='
Content
solar2icon_512x512_transparent.png'
## TODO: Some of these shipped Mono libraries could probably be replaced by system-provided ones
CONTENT_GAME0_DATA_FILES='
*.dll'

## Old XNA build

CONTENT_GAME_MAIN_FILES_XNA='
mono
Languages
MonoContent
display.txt
Solar2.exe
EasyStorage.Linux.dll
GamepadBridge.dll
Lidgren.Network.dll
MonoGame.Framework.Linux.dll
MonoGame.Framework.Linux.dll.config
Power.Linux.dll
PowerRuntime.Linux.dll
Tao.Sdl.dll
Tao.Sdl.dll.config'

# Launchers

## TODO: Check if these paths are used by the current MonoGame builds
USER_PERSISTENT_DIRECTORIES='
Languages'
USER_PERSISTENT_FILES='
display.txt'

APP_MAIN_EXE='Solar2'
APP_MAIN_ICON='solar2icon_512x512_transparent.png'
## Prevent the game from escaping the prefix
APP_MAIN_PRERUN='
# Prevent the game from escaping the prefix
if [ -h "Solar2.dll" ]; then
	file_source=$(realpath "Solar2.dll")
	cp --remove-destination "$file_source" "Solar2.dll"
fi
'

## Old XNA build

APP_MAIN_EXE_XNA='Solar2.exe'

# Packages

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
liblttng-ust.so.0
libm.so.6
libopenal.so.1
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libstdc++.so.6
libz.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

## Old XNA build

PACKAGES_LIST_XNA='
PKG_MAIN'

PKG_MAIN_PROVIDES_XNA='
solar-2-data'
PKG_MAIN_DEPENDENCIES_LIBRARIES_XNA='
libopenal.so.1
libSDL_mixer-1.2.so.0'
PKG_MAIN_DEPENDENCIES_MONO_LIBRARIES_XNA='
mscorlib.dll
Mono.Posix.dll
Mono.Security.dll
OpenTK.dll
System.dll
System.Configuration.dll
System.Core.dll
System.Data.dll
System.Drawing.dll
System.Security.dll
System.Xml.dll'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

case "$(current_archive)" in
	('ARCHIVE_BASE_XNA_'*)
		## Old XNA build - No icon is shipped (not even included in the .exe file),
		## but an optional icons pack is supported
		unset APP_MAIN_ICON
		content_inclusion_icons
	;;
	(*)
		content_inclusion_icons 'PKG_DATA'
	;;
esac
content_inclusion_default

# Write launchers

case "$(current_archive)" in
	('ARCHIVE_BASE_XNA_'*)
		## Old XNA build - libSDL_mixer-1.2.so.0 must be available at lib/libSDL_mixer-1.2.so.0 to avoid a game crash.
		## WARNING: path_libraries_system can not be used when the package is architecture-agnostic
		case "$(option_value 'package')" in
			('arch')
				path_libraries_64='/usr/lib'
				path_libraries_32='/usr/lib32'
			;;
			('deb')
				path_libraries_64='/usr/lib/x86_64-linux-gnu'
				path_libraries_32='/usr/lib/i386-linux-gnu'
			;;
			('gentoo'|'egentoo')
				path_libraries_64='/usr/lib64'
				path_libraries_32='/usr/lib'
			;;
		esac
		APP_MAIN_PRERUN="$(application_prerun 'APP_MAIN')
		# libSDL_mixer-1.2.so.0 must be available at lib/libSDL_mixer-1.2.so.0 to avoid a game crash
		if [ ! -e 'lib/libSDL_mixer-1.2.so.0' ]; then
			mkdir --parents 'lib'
			if [ -e '${path_libraries_64}/libSDL_mixer-1.2.so.0' ]; then
				ln --symbolic '${path_libraries_64}/libSDL_mixer-1.2.so.0' 'lib/libSDL_mixer-1.2.so.0'
			elif [ -e '${path_libraries_32}/libSDL_mixer-1.2.so.0' ]; then
				ln --symbolic '${path_libraries_32}/libSDL_mixer-1.2.so.0' 'lib/libSDL_mixer-1.2.so.0'
			fi
		fi
		"
		launchers_generation
	;;
	(*)
		## MonoGame builds - The game engine fails to find the shipped libraries if they are not in the game directory
		APP_MAIN_PRERUN="$(application_prerun 'APP_MAIN')
		# The game engine fails to find the shipped libraries if they are not in the game directory
		ln --force --symbolic '$(path_libraries)'/*.so .
		"
		launchers_generation 'PKG_BIN'
	;;
esac

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
