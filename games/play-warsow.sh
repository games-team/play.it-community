#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Warsow
# send your bug reports to contact@dotslashplay.it
###

script_version=20240622.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='warsow'
GAME_NAME='War§ow'

## This installer is no longer available from gog.com.
ARCHIVE_BASE_0_NAME='gog_warsow_2.1.0.3.sh'
ARCHIVE_BASE_0_MD5='028efe7a5f4dfd8851c2146431c7ca4a'
ARCHIVE_BASE_0_SIZE='479771'
ARCHIVE_BASE_0_VERSION='2.1-gog2.1.0.3'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN_PATH="${CONTENT_PATH_DEFAULT}/libs"
CONTENT_LIBS_BIN64_PATH="$CONTENT_LIBS_BIN_PATH"
CONTENT_LIBS_BIN64_FILES='
libangelwrap_x86_64.so
libcin_x86_64.so
libftlib_x86_64.so
libirc_x86_64.so
libref_gl_x86_64.so
libsnd_openal_x86_64.so
libsnd_qf_x86_64.so
libui_x86_64.so'
CONTENT_LIBS_BIN32_PATH="$CONTENT_LIBS_BIN_PATH"
CONTENT_LIBS_BIN32_FILES='
libangelwrap_i386.so
libcin_i386.so
libftlib_i386.so
libirc_i386.so
libref_gl_i386.so
libsnd_openal_i386.so
libsnd_qf_i386.so
libui_i386.so'
CONTENT_GAME_BIN64_FILES='
warsow.x86_64
wsw_server.x86_64
wswtv_server.x86_64'
CONTENT_GAME_BIN32_FILES='
warsow.i386
wsw_server.i386
wswtv_server.i386'
CONTENT_GAME_DATA_FILES='
basewsw'
CONTENT_DOC_DATA_PATH="${CONTENT_PATH_DEFAULT}/docs"
CONTENT_DOC_DATA_FILES='
*.txt'

APP_MAIN_EXE_BIN64='warsow.x86_64'
APP_MAIN_EXE_BIN32='warsow.i386'
APP_MAIN_ICON='../support/icon.png'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libstdc++.so.6'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
