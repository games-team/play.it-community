#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Rymdkapsel
# send your bug reports to contact@dotslashplay.it
###

script_version=20240728.3

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='rymdkapsel'
GAME_NAME='Rymdkapsel'

ARCHIVE_BASE_0_NAME='rymdkapsel-3.0.1-2014-01-31.deb'
ARCHIVE_BASE_0_MD5='f19110f945625a39d154401e1083662e'
ARCHIVE_BASE_0_SIZE='74942'
ARCHIVE_BASE_0_VERSION='3.0.1-humble1'
ARCHIVE_BASE_0_URL='https://www.humblebundle.com/store/rymdkapsel'

CONTENT_PATH_DEFAULT='usr/games/rymdkapsel'
CONTENT_LIBS_BIN_FILES='
regexp.dso
std.dso
zlib.dso
lime.ndll'
CONTENT_GAME_BIN_FILES='
rymdkapsel'
CONTENT_GAME_DATA_FILES='
assets
manifest'
CONTENT_DOC_DATA_PATH='usr/share/doc/rymdkapsel'
CONTENT_DOC_DATA_FILES='
copyright'

APP_MAIN_EXE='rymdkapsel'
APP_MAIN_ICONS_LIST='APP_MAIN_ICON_32 APP_MAIN_ICON_48 APP_MAIN_ICON_64 APP_MAIN_ICON_128'
APP_MAIN_ICON_32='../../share/icons/hicolor/32x32/apps/rymdkapsel.png'
APP_MAIN_ICON_48='../../share/icons/hicolor/48x48/apps/rymdkapsel.png'
APP_MAIN_ICON_64='../../share/icons/hicolor/64x64/apps/rymdkapsel.png'
APP_MAIN_ICON_128='../../share/icons/hicolor/128x128/apps/rymdkapsel.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libstdc++.so.6'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

## Force the use of system-provided SDL.
path_libraries_system=$(
	set_current_package 'PKG_BIN'
	path_libraries_system
)
APP_MAIN_PRERUN="$(application_prerun 'APP_MAIN')
# Force the use of system-provided SDL.
export SDL_DYNAMIC_API='${path_libraries_system}/libSDL2-2.0.so.0'
"

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
