#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Super Meat Boy
# send your bug reports to contact@dotslashplay.it
###

script_version=20240806.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='super-meat-boy'
GAME_NAME='Super Meat Boy'

ARCHIVE_BASE_0_NAME='supermeatboy-linux-11112013-bin'
ARCHIVE_BASE_0_MD5='fc024d757395eebfdee342571671332b'
## This archive is a MojoSetup installer that is not using Makeself
ARCHIVE_BASE_0_EXTRACTOR='bsdtar'
ARCHIVE_BASE_0_SIZE='250899'
ARCHIVE_BASE_0_VERSION='1.0-humble131111'
ARCHIVE_BASE_0_URL='https://www.humblebundle.com/store/super-meat-boy'

CONTENT_PATH_DEFAULT='data'
CONTENT_GAME_BIN64_FILES='
amd64/SuperMeatBoy'
## TODO: The shipped libraries should be installed in the system libraries path
## TODO: Check if some shipped libraries can be dropped
CONTENT_GAME0_BIN64_FILES='
amd64/libmariadb.so.1
amd64/libopenal.so.1
amd64/libSDL2-2.0.so.0'
## TODO: Check if the Steam library is required
CONTENT_GAME1_BIN64_FILES='
amd64/libsteam_api.so'
CONTENT_GAME_BIN32_FILES='
x86/SuperMeatBoy'
## TODO: The shipped libraries should be installed in the system libraries path
## TODO: Check if some shipped libraries can be dropped
CONTENT_GAME0_BIN32_FILES='
x86/libmariadb.so.1
x86/libopenal.so.1
x86/libSDL2-2.0.so.0'
## TODO: Check if the Steam library is required
CONTENT_GAME1_BIN32_FILES='
x86/libsteam_api.so'
CONTENT_GAME_DATA_FILES='
Levels
resources
buttonmap.cfg
gameaudio.dat
gamedata.dat
supermeatboy.png
locdb.txt'
## TODO: Check if the Steam metadata is required
CONTENT_GAME0_DATA_FILES='
steam_appid.txt'
CONTENT_DOC_DATA_FILES='
README-licenses.txt
README-linux.txt'

USER_PERSISTENT_FILES='
*.cfg'

APP_MAIN_EXE_BIN32='x86/SuperMeatBoy'
APP_MAIN_EXE_BIN64='amd64/SuperMeatBoy'
APP_MAIN_ICON='supermeatboy.png'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
