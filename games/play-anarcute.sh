#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Anarcute
# send your bug reports to contact@dotslashplay.it
###

script_version=20240508.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='anarcute'
GAME_NAME='Anarcute'

ARCHIVE_BASE_32BIT_0_NAME='anarcute-windows-32.zip'
ARCHIVE_BASE_32BIT_0_MD5='837ee19a38698bc71b1b78e5c05f9644'
ARCHIVE_BASE_32BIT_0_SIZE='1400000'
ARCHIVE_BASE_32BIT_0_VERSION='1.0-itch1'
ARCHIVE_BASE_32BIT_0_URL='https://plug-in-digital.itch.io/anarcute'

ARCHIVE_BASE_64BIT_0_NAME='anarcute-windows-64.zip'
ARCHIVE_BASE_64BIT_0_MD5='f6af6f32f587cd195581358c1ed5f0c7'
ARCHIVE_BASE_64BIT_0_SIZE='1400000'
ARCHIVE_BASE_64BIT_0_VERSION='1.0-itch1'
ARCHIVE_BASE_64BIT_0_URL='https://plug-in-digital.itch.io/anarcute'

UNITY3D_NAME='anarcute'

CONTENT_PATH_DEFAULT='.'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Roaming/Anarcute'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH_32BIT='32'
PKG_BIN_ARCH_64BIT='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
