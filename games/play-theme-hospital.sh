#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Mopi
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Theme Hospital
# send your bug reports to contact@dotslashplay.it
###

script_version=20240507.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='theme-hospital'
GAME_NAME='Theme Hospital'

ARCHIVE_BASE_1_NAME='setup_theme_hospital_v3_(28027).exe'
ARCHIVE_BASE_1_MD5='e4cba7cfddd5dd2d4baf4761bc86a8c8'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='200000'
ARCHIVE_BASE_1_VERSION='1.0-gog28027'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/theme_hospital'

ARCHIVE_BASE_0_NAME='setup_theme_hospital_2.1.0.8.exe'
ARCHIVE_BASE_0_MD5='c1dc6cd19a3e22f7f7b31a72957babf7'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='210000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.0.0.7'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_DOSBOX_FILES='
*.bat
*.cfg
*.exe
*.ini'
CONTENT_GAME_DATA_FILES='
anims
cfg
data
datam
intro
levels
qdata
qdatam
save
sound'
CONTENT_DOC_DATA_FILES='
*.txt
*.pdf'

USER_PERSISTENT_DIRECTORIES='
save'
USER_PERSISTENT_FILES='
*.cfg
*.ini'

APP_MAIN_EXE='hospital.exe'
APP_MAIN_TYPE_CORSIXTH='custom'
APP_MAIN_ICON='app/goggame-1207659026.ico'
APP_MAIN_ICON_0='goggame-1207659026.ico'

PACKAGES_LIST='
PKG_DOSBOX
PKG_CORSIXTH
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_ENGINE_ID="$GAME_ID"

PKG_DOSBOX_ID="${PKG_ENGINE_ID}-dosbox"
PKG_DOSBOX_PROVIDES="
$PKG_ENGINE_ID"
PKG_DOSBOX_DEPS="$PKG_DATA_ID dosbox"

PKG_CORSIXTH_ID="${PKG_ENGINE_ID}-corsixth"
PKG_CORSIXTH_PROVIDES="
$PKG_ENGINE_ID"
PKG_CORSIXTH_DEPS="$PKG_DATA_ID"
PKG_CORSIXTH_DEPENDENCIES_COMMANDS='
corsix-th'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"


# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Delete unwanted files
	rm --force --recursive \
		'dosbox' \
		'tmp'
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

## DOSBox

launchers_generation 'PKG_DOSBOX'

## CorsixTH

custom_launcher() {
	launcher_headers
	cat <<- 'EOF'
	corsix-th "$@"

	EOF
	launcher_exit
}
launchers_generation 'PKG_CORSIXTH'

# Build packages

## Set the path to game data in CorsixTH configuration.
case "$(option_value 'package')" in
	('arch')
		path_corsixth='/usr/share/CorsixTH'
	;;
	('deb')
		path_corsixth='/usr/share/games/corsix-th'
	;;
	('gentoo'|'egentoo')
		path_corsixth='/usr/share/corsix-th'
	;;
esac
config_file_path="${path_corsixth}/Lua/config_finder.lua"
PKG_CORSIXTH_POSTINST_RUN="$(package_postinst_actions 'PKG_CORSIXTH_POSTINST_RUN')"'
# Set path to game data in CorsixTH configuration
config_file_path="'"$config_file_path"'"
sed_pattern="^  theme_hospital_install = .*$"
sed_replacement="  theme_hospital_install = [['"$(path_game_data)"']],"
sed_expression="s#${sed_pattern}#${sed_replacement}#"
sed --in-place --expression="$sed_expression" "$config_file_path"'
packages_generation
printf '\n'
printf 'CorsixTH:'
print_instructions 'PKG_DATA' 'PKG_CORSIXTH'
printf 'DOSBox:'
print_instructions 'PKG_DATA' 'PKG_DOSBOX'

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
