#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Temple of Elemental Evil
# send your bug reports to contact@dotslashplay.it
###

script_version=20240728.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='the-temple-of-elemental-evil'
GAME_NAME='The Temple of Elemental Evil'

ARCHIVE_BASE_1_NAME='setup_temple_of_elemental_evil_1.0_(15416).exe'
ARCHIVE_BASE_1_MD5='1c7b493f71c2c92050a63535b3abec67'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='1400000'
ARCHIVE_BASE_1_VERSION='1.3-gog15416'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/the_temple_of_elemental_evil'

ARCHIVE_BASE_0_NAME='setup_temple_of_elemental_evil_2.0.0.13.exe'
ARCHIVE_BASE_0_MD5='44ea1e38ed1da26aefb32a39a899f770'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='1400000'
ARCHIVE_BASE_0_VERSION='1.3-gog2.0.0.13'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_BIN_FILES='
miles
*.dll
*.exe
*.pyd'
CONTENT_GAME0_BIN_PATH="${CONTENT_PATH_DEFAULT}/__support/app"
## Required to override the previous value.
CONTENT_GAME0_BIN_PATH_0="$CONTENT_PATH_DEFAULT_0"
CONTENT_GAME0_BIN_FILES='
*.cfg'
CONTENT_GAME_DATA_FILES='
data
modules
toee.ico
*.dat'
CONTENT_DOC_DATA_FILES='
*.pdf
*.txt'

USER_PERSISTENT_DIRECTORIES='
data/maps
data/save
data/scr
modules/toee'
USER_PERSISTENT_FILES='
toee.cfg'

APP_MAIN_EXE='toee.exe'
APP_MAIN_ICON='toee.ico'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
