#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Anomaly 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20241202.2

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='anomaly-2'
GAME_NAME='Anomaly 2'

ARCHIVE_BASE_LINUX_0_NAME='Anomaly2_Linux_1387299615.zip'
ARCHIVE_BASE_LINUX_0_MD5='46f0ecd5363106e9eae8642836c29dfc'
ARCHIVE_BASE_LINUX_0_SIZE='2500000'
ARCHIVE_BASE_LINUX_0_VERSION='1.0-humble1'
ARCHIVE_BASE_LINUX_0_URL='https://www.humblebundle.com/store/anomaly-2'

ARCHIVE_BASE_WINDOWS_0_NAME='Anomaly2_Windows_1387299615.zip'
ARCHIVE_BASE_WINDOWS_0_MD5='2b5ccffcbaee8cfebfd4bb74cacb9fbc'
ARCHIVE_BASE_WINDOWS_0_SIZE='2500000'
ARCHIVE_BASE_WINDOWS_0_VERSION='1.0-humble1'
ARCHIVE_BASE_WINDOWS_0_URL='https://www.humblebundle.com/store/anomaly-2'

ARCHIVE_OPTIONAL_ICONS_NAME='anomaly-2_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_MD5='73ddbd1651e08d6c8bb4735e5e0a4a81'
ARCHIVE_OPTIONAL_ICONS_URL='https://downloads.dotslashplay.it/games/anomaly-2/'
CONTENT_ICONS_PATH='.'
CONTENT_ICONS_FILES='
32x32
48x48
64x64
256x256'

CONTENT_PATH_DEFAULT_LINUX='Anomaly 2 Linux DRM-free'
CONTENT_PATH_DEFAULT_WINDOWS='Anomaly 2 Windows DRM-free/2013-12-17 03-20'
CONTENT_LIBS_BIN_FILES_LINUX='
libopenal.so.1'
CONTENT_GAME_BIN_FILES_LINUX='
*.txt
Anomaly2'
CONTENT_GAME_BIN_FILES_WINDOWS='
*.exe
*.dll
*.txt'
CONTENT_GAME_DATA_FILES='
*.dat
*.idx
*.str'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Application Data/11bitstudios/Anomaly 2'

APP_MAIN_EXE_LINUX='Anomaly2'
APP_MAIN_EXE_WINDOWS='Anomaly 2.exe'
## Share saved games and config between Linux and Windows engines
APP_MAIN_PRERUN_LINUX='
# Share saved games and config between Linux and Windows engines
NEW_LAUNCH_REQUIRED=0
WINE_PERSISTENT_PATH="${PATH_PERSISTENT}/wineprefix/users/${USER}/Application Data/11bitstudios/Anomaly 2"
if [ -e "${HOME}/.Anomaly 2" ] && [ ! -h "${HOME}/.Anomaly 2" ]; then
	for file in \
		config.bin \
		DefaultUser \
		iPhoneProfiles
	do
		if [ -e "${HOME}/.Anomaly 2/${file}" ]; then
			mkdir --parents "$WINE_PERSISTENT_PATH"
			cp --recursive "${HOME}/.Anomaly 2/${file}" "$WINE_PERSISTENT_PATH"
		fi
	done
	mv "${HOME}/.Anomaly 2" "${HOME}/.Anomaly 2.old"
	NEW_LAUNCH_REQUIRED=1
fi
if [ ! -e "${HOME}/.Anomaly 2" ]; then
	rm --force "${HOME}/.Anomaly 2"
	mkdir --parents "$WINE_PERSISTENT_PATH"
	ln --symbolic "$WINE_PERSISTENT_PATH" "${HOME}/.Anomaly 2"
fi
if [ "$NEW_LAUNCH_REQUIRED" -eq 1 ]; then
	"$0"
	exit 0
fi
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ID="$GAME_ID"
PKG_BIN_ID_LINUX="${PKG_BIN_ID}-linux"
PKG_BIN_ID_WINDOWS="${PKG_BIN_ID}-windows"
PKG_BIN_PROVIDES="
$PKG_BIN_ID"
PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES_LINUX='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_LINUX="${PKG_DATA_ID}-linux"
PKG_DATA_ID_WINDOWS="${PKG_DATA_ID}-windows"
PKG_DATA_PROVIDES="
$PKG_DATA_ID"
PKG_DATA_DESCRIPTION='data'

# LD_PRELOAD shim working around infinite loading times

PRELOAD_HACKS_LIST_LINUX='
HACK_TIMERFIX'

HACK_TIMERFIX_NAME='timerfix'
HACK_TIMERFIX_DESCRIPTION='LD_PRELOAD shim working around infinite loading times'
HACK_TIMERFIX_PACKAGE='PKG_BIN'
HACK_TIMERFIX_SOURCE='
#define _GNU_SOURCE
#include <dlfcn.h>
#include <semaphore.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

static int (*_realSemTimedWait)(sem_t *, const struct timespec *) = NULL;

int sem_timedwait(sem_t *sem, const struct timespec *abs_timeout) {
	if (abs_timeout->tv_nsec >= 1000000000) {
		((struct timespec *)abs_timeout)->tv_nsec -= 1000000000;
		((struct timespec *)abs_timeout)->tv_sec++;
	}
	return _realSemTimedWait(sem, abs_timeout);
}
__attribute__((constructor)) void init(void) {
	_realSemTimedWait = dlsym(RTLD_NEXT, "sem_timedwait");
}
'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Build and include the LD_PRELOAD shims

hacks_inclusion_default

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
