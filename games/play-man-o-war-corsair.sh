#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Man O' War: Corsair
# send your bug reports to contact@dotslashplay.it
###

script_version=20231011.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='man-o-war-corsair'
GAME_NAME='Man Oʼ War: Corsair - Warhammer Naval Battles'

ARCHIVE_BASE_1_NAME='setup_man_o_war_corsair_-_warhammer_naval_battles_1.4.4_(38991).exe'
ARCHIVE_BASE_1_MD5='c0b70c63edcc3ca8d8209a9ccb7c9d9e'
ARCHIVE_BASE_1_PART1_NAME='setup_man_o_war_corsair_-_warhammer_naval_battles_1.4.4_(38991)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='69c15c6e3b8238b585c8760327756ab7'
ARCHIVE_BASE_1_PART2_NAME='setup_man_o_war_corsair_-_warhammer_naval_battles_1.4.4_(38991)-2.bin'
ARCHIVE_BASE_1_PART2_MD5='31aa85d6f8c39625e0dfac46b82928b5'
ARCHIVE_BASE_1_PART3_NAME='setup_man_o_war_corsair_-_warhammer_naval_battles_1.4.4_(38991)-3.bin'
ARCHIVE_BASE_1_PART3_MD5='1676e6583b9331b9ba7ad029e4009a46'
ARCHIVE_BASE_1_PART4_NAME='setup_man_o_war_corsair_-_warhammer_naval_battles_1.4.4_(38991)-4.bin'
ARCHIVE_BASE_1_PART4_MD5='9c0637c0277adea7705ef713e924a7ac'
ARCHIVE_BASE_1_SIZE='30000000'
ARCHIVE_BASE_1_VERSION='1.4.4-gog38991'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/man_o_war_corsair'

ARCHIVE_BASE_0_NAME='setup_man_o_war_corsair_-_warhammer_naval_battles_1.4.2_(29576).exe'
ARCHIVE_BASE_0_MD5='296429ab49c28df62ff38235564e36e8'
ARCHIVE_BASE_0_PART1_NAME='setup_man_o_war_corsair_-_warhammer_naval_battles_1.4.2_(29576)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='8c3b24d21c951f6ad9b7699eda024de1'
ARCHIVE_BASE_0_PART2_NAME='setup_man_o_war_corsair_-_warhammer_naval_battles_1.4.2_(29576)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='516a671f6a45d9c03e29f13d51271a78'
ARCHIVE_BASE_0_PART3_NAME='setup_man_o_war_corsair_-_warhammer_naval_battles_1.4.2_(29576)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='38c58a508879661063ca5e9bbea541ab'
ARCHIVE_BASE_0_PART4_NAME='setup_man_o_war_corsair_-_warhammer_naval_battles_1.4.2_(29576)-4.bin'
ARCHIVE_BASE_0_PART4_MD5='1f02de3d92681a080808446636116c6c'
ARCHIVE_BASE_0_SIZE='30000000'
ARCHIVE_BASE_0_VERSION='1.4.2-gog29576'

UNITY3D_NAME='manowarcorsair'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_MODELS_LAND_FILES='
mowdata/landmodels-*'
CONTENT_GAME_MODELS_FILES='
mowdata/charactermodels-*
mowdata/shipmodels-*
mowdata/flyermodels*
mowdata/seamonstermodels*'
CONTENT_GAME_TERRAIN1_FILES='
mowdata/terrain-top*
mowdata/terrain-mid*'
CONTENT_GAME_TERRAIN2_FILES='
mowdata/terrain-bot*
mowdata/terrainshaders*'
CONTENT_GAME0_DATA_FILES='
mowdata'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Local Settings/Application Data/EvilTwinArtworks/ManOWarCorsair'
## The single quote in the key name must be escaped twice.
WINE_REGEDIT_PERSISTENT_KEYS='
HKEY_CURRENT_USER\Software\Evil Twin Artworks\Man O'"'\\''"' War: Corsair'

APP_MAIN_OPTIONS='-force-opengl'

PACKAGES_LIST='PKG_BIN PKG_MODELS_LAND PKG_MODELS PKG_TERRAIN1 PKG_TERRAIN2 PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_MODELS_ID="${PKG_DATA_ID}-models"
PKG_MODELS_DESCRIPTION="$PKG_DATA_DESCRIPTION - models"
PKG_DATA_DEPS="${PKG_DATA_DEPS:-} $PKG_MODELS_ID"
## Ensure smooth upgrades from packages generated with pre-20231011.1 scripts.
PKG_MODELS_PROVIDES='
man-o-war-corsair-models'

PKG_MODELS_LAND_ID="${PKG_MODELS_ID}-land"
PKG_MODELS_LAND_DESCRIPTION="$PKG_MODELS_DESCRIPTION - land"
PKG_MODELS_DEPS="${PKG_MODELS_DEPS:-} $PKG_MODELS_LAND_ID"
## Ensure smooth upgrades from packages generated with pre-20231011.1 scripts.
PKG_MODELS_LAND_PROVIDES='
man-o-war-corsair-models-land'

PKG_TERRAIN_ID="${PKG_DATA_ID}-terrain"
PKG_TERRAIN1_ID="${PKG_TERRAIN_ID}-1"
PKG_TERRAIN2_ID="${PKG_TERRAIN_ID}-2"
PKG_TERRAIN_DESCRIPTION='terrain'
PKG_TERRAIN1_DESCRIPTION="$PKG_TERRAIN_DESCRIPTION - part 1"
PKG_TERRAIN2_DESCRIPTION="$PKG_TERRAIN_DESCRIPTION - part 2"
PKG_DATA_DEPS="${PKG_DATA_DEPS:-} $PKG_TERRAIN1_ID $PKG_TERRAIN2_ID"
## Ensure smooth upgrades from packages generated with pre-20231011.1 scripts.
PKG_TERRAIN1_PROVIDES='
man-o-war-corsair-terrain-1'
PKG_TERRAIN2_PROVIDES='
man-o-war-corsair-terrain-2'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
