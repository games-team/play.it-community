#!/bin/sh
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Divinity: Original Sin
# send your bug reports to contact@dotslashplay.it
###

script_version=20240516.3

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='divinity-original-sin-1'
GAME_NAME='Divinity: Original Sin'

ARCHIVE_BASE_0_NAME='divinity_original_sin_enhanced_edition_en_2_0_119_430_ch_17075.sh'
ARCHIVE_BASE_0_MD5='89f526c1030d6d352b7df65361ab71e6'
ARCHIVE_BASE_0_SIZE='11000000'
ARCHIVE_BASE_0_VERSION='2.0.119.430-gog17075'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/divinity_original_sin_enhanced_edition'

## Optional icons pack
ARCHIVE_OPTIONAL_ICONS_NAME='divinity-original-sin-1_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_MD5='8f01ea213b7b2c966ce3c065333f5406'
ARCHIVE_OPTIONAL_ICONS_URL='https://downloads.dotslashplay.it/games/divinity-original-sin-1/'
CONTENT_ICONS_PATH='.'
CONTENT_ICONS_FILES='
16x16
32x32
48x48
64x64
128x128
256x256
512x512'

CONTENT_PATH_DEFAULT='data/noarch/game'
## TODO: Check if libSDL2-2.0.so.1 can be dropped in favour of a system-provided build of libSDL2-2.0.so.0
CONTENT_LIBS_BIN_FILES='
libbink2.so
libCoreLib.so
libGameEngine.so
libGLEW.so.1.10
libGLEW.so.1.10.0
libicudata.so.54
libicuuc.so.54
libjpeg.so.8
libjpeg.so.8.0.2
libOGLBinding.so
libosiris.so.2
libRenderFramework.so
libSDL2-2.0.so.1'
## The game binary is linked against libsteam_api.so, so this library can not be dropped.
CONTENT_LIBS_BIN_FILES="${CONTENT_LIBS_BIN_FILES:-}
libsteam_api.so"
CONTENT_GAME_BIN_FILES='
EoCApp'
CONTENT_GAME_L10N_EN_FILES='
Data/Localization/English.pak'
CONTENT_GAME_L10N_FR_FILES='
Data/Localization/French.pak'
CONTENT_GAME_DATA_VOICES_FILES='
Data/Localization/Voice.pak
Data/Localization/Voice_1.pak
Data/Localization/Voice_2.pak
Data/Localization/Voice_3.pak
Data/Localization/Voice_4.pak'
CONTENT_GAME_DATA_FILES='
DigitalMap
Data/Localization/Intro.pak
Data/Localization/Reference.pak
Data/Localization/Video.pak
Data/DLC.pak
Data/Effects.pak
Data/Engine.pak
Data/Game.pak
Data/GamePlatform.pak
Data/GLSLShaders.pak
Data/Icons.pak
Data/LowTex.pak
Data/MainLSF.pak
Data/Main.pak
Data/Minimaps.pak
Data/Sound_1.pak
Data/SoundBanks.pak
Data/Sound.pak
Data/Textures_1.pak
Data/Textures_2.pak
Data/Textures.pak'

FAKE_HOME_PERSISTENT_DIRECTORIES='
Larian Studios/Divinity Original Sin Enhanced Edition'

APP_MAIN_EXE='EoCApp'
APP_MAIN_ICON='../support/icon.png'
## The game crashes on launch when using the wayland backend of SDL,
## even when using the system-provided build of SDL.
APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}"'
# The game crashes on launch when using the wayland backend of SDL
if [ "${SDL_VIDEODRIVER:-}" = "wayland" ]; then
	unset SDL_VIDEODRIVER
fi
'

PACKAGES_LIST='
PKG_BIN
PKG_L10N_EN
PKG_L10N_FR
PKG_DATA_VOICES
PKG_DATA'

PKG_L10N_ID="${GAME_ID}-l10n"
PKG_L10N_EN_ID="${PKG_L10N_ID}-en"
PKG_L10N_FR_ID="${PKG_L10N_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_ID"
PKG_L10N_EN_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_FR_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_DESCRIPTION='localization'
PKG_L10N_EN_DESCRIPTION="$PKG_L10N_DESCRIPTION - English"
PKG_L10N_FR_DESCRIPTION="$PKG_L10N_DESCRIPTION - French"
## Ensure easy upgrades from packages generated with pre-20240516.2 game scripts.
PKG_L10N_EN_PROVIDES="${PKG_L10N_EN_PROVIDES:-}
divinity-original-sin-l10n-en"
PKG_L10N_FR_PROVIDES="${PKG_L10N_FR_PROVIDES:-}
divinity-original-sin-l10n-fr"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
## Ensure easy upgrades from packages generated with pre-20240516.2 game scripts.
PKG_DATA_PROVIDES="${PKG_DATA_PROVIDES:-}
divinity-original-sin-data"

PKG_DATA_VOICES_ID="${PKG_DATA_ID}-voices"
PKG_DATA_VOICES_DESCRIPTION="$PKG_DATA_DESCRIPTION - voices"
PKG_DATA_DEPS="${PKG_DATA_DEPS:-} $PKG_DATA_VOICES_ID"
## Ensure easy upgrades from packages generated with pre-20240516.2 game scripts.
PKG_DATA_VOICES_PROVIDES="${PKG_DATA_VOICES_PROVIDES:-}
divinity-original-sin-data-voices"

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_L10N_ID $PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libc++.so.1
libc++abi.so.1
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libopenal.so.1
libpng16.so.16
libpthread.so.0
librt.so.1
libz.so.1'
## Ensure easy upgrades from packages generated with pre-20240516.2 game scripts.
PKG_BIN_PROVIDES="${PKG_BIN_PROVIDES:-}
divinity-original-sin"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Force the use of system-provided SDL

APP_MAIN_PRERUN="$(application_prerun 'APP_MAIN')
# Force the use of the system SDL library
export SDL_DYNAMIC_API='$(path_libraries_system)/libSDL2-2.0.so.0'
"

# Ensure availability of libpcre.so.3

case "$(option_value 'package')" in
	('arch')
		# libpcre.so.3 is not provided from Arch Linux repositories
		CONTENT_LIBS_BIN_FILES="$(content_files 'LIBS_BIN')
		libpcre.so.3
		libpcre.so.3.13.1"
	;;
	(*)
		dependencies_add_native_libraries 'PKG_BIN' 'libpcre.so.3'
	;;
esac

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Generate the configuration of localization packages
	config_source='Data/Localization/language.lsx'
	config_pattern='id="Value" value="[A-Za-z]*" type="20"'
	### English localization
	config_destination_en="$(package_path 'PKG_L10N_EN')$(path_game_data)/${config_source}"
	config_replacement_en='id="Value" value="English" type="20"'
	mkdir --parents "$(dirname "$config_destination_en")"
	sed --expression="s/${config_pattern}/${config_replacement_en}/" \
		"$config_source" > "$config_destination_en"
	### French localization
	config_destination_fr="$(package_path 'PKG_L10N_FR')$(path_game_data)/${config_source}"
	config_replacement_fr='id="Value" value="French" type="20"'
	mkdir --parents "$(dirname "$config_destination_fr")"
	sed --expression="s/${config_pattern}/${config_replacement_fr}/" \
		"$config_source" > "$config_destination_fr"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
case "${LANG%_*}" in
	('fr')
		lang_string='version %s :'
		lang_en='anglaise'
		lang_fr='française'
	;;
	('en'|*)
		lang_string='%s version:'
		lang_en='English'
		lang_fr='French'
	;;
esac
printf '\n'
printf "$lang_string" "$lang_en"
print_instructions 'PKG_BIN' 'PKG_DATA_VOICES' 'PKG_DATA' 'PKG_L10N_EN'
printf "$lang_string" "$lang_fr"
print_instructions 'PKG_BIN' 'PKG_DATA_VOICES' 'PKG_DATA' 'PKG_L10N_FR'

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
