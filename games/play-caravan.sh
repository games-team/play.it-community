#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Caravan
# send your bug reports to contact@dotslashplay.it
###

script_version=20240622.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='caravan'
GAME_NAME='Caravan'

ARCHIVE_BASE_HUMBLE_0_NAME='caravan_linux_v1.1.17513.zip'
ARCHIVE_BASE_HUMBLE_0_MD5='286f762190d74e27c353b5616b9e6dee'
ARCHIVE_BASE_HUMBLE_0_SIZE='600000'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.1.17513-humble1'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/caravan'

ARCHIVE_BASE_GOG_0_NAME='caravan_en_v1_1_19786_19905.sh'
ARCHIVE_BASE_GOG_0_MD5='e6806e10c86267b38a2a2a85bb278dae'
ARCHIVE_BASE_GOG_0_SIZE='430000'
ARCHIVE_BASE_GOG_0_VERSION='1.1.19786-gog19905'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/caravan'

UNITY3D_NAME='Caravan'
UNITY3D_PLUGINS='
ScreenSelector.so'

CONTENT_PATH_DEFAULT_HUMBLE='Caravan'
CONTENT_PATH_DEFAULT_GOG='data/noarch/game'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libGLU.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

case "$(current_archive)" in
	('ARCHIVE_BASE_HUMBLE_'*)
		## Errors during the game data extraction are expected, they should be ignored.
		archive_extraction_default 2>/dev/null || true
		ARCHIVE_INNER_PATH="${PLAYIT_WORKDIR}/gamedata/Caravan_1.1.17513_Full_DEB_Multi_Daedalic_ESD.tar.gz"
		archive_extraction 'ARCHIVE_INNER'
		rm "$ARCHIVE_INNER_PATH"
	;;
	(*)
		archive_extraction_default
	;;
esac

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
