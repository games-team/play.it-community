#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# A Juggler's Tale
# send your bug reports to contact@dotslashplay.it
###

script_version=20240611.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='a-jugglers-tale'
GAME_NAME='A Jugglerʼs Tale'

ARCHIVE_BASE_0_NAME='setup_a_jugglers_tale_1.16.0_(64bit)_(50655).exe'
ARCHIVE_BASE_0_MD5='bb0953d80bf8b9d996bdb453564fe07f'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_a_jugglers_tale_1.16.0_(64bit)_(50655)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='2345e25ad4c4f90574b63b3da7e464ab'
ARCHIVE_BASE_0_SIZE='2800000'
ARCHIVE_BASE_0_VERSION='1.16.0-gog50655'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/a_jugglers_tale'

UNREALENGINE4_NAME='ajugglerstale'

CONTENT_PATH_DEFAULT='.'

APP_MAIN_EXE="${UNREALENGINE4_NAME}.exe"

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
