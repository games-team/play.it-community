#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Skullgirls
# send your bug reports to contact@dotslashplay.it
###

script_version=20230208.1

GAME_ID='skullgirls'
GAME_NAME='Skullgirls'

# This DRM-free archive is no longer sold by Humble Bundle
ARCHIVE_BASE_1='Skullgirls-15719.tar'
ARCHIVE_BASE_1_MD5='104a6976aec70d423756e008a5b8554c'
ARCHIVE_BASE_1_SIZE='4200000'
ARCHIVE_BASE_1_VERSION='15719-humble170628'

ARCHIVE_BASE_0='Skullgirls-1.0.1.sh'
ARCHIVE_BASE_0_MD5='bf110f7d29bfd4b9e075584e41fef402'
ARCHIVE_BASE_0_TYPE='mojosetup'
ARCHIVE_BASE_0_SIZE='4200000'
ARCHIVE_BASE_0_VERSION='1.0.1-humble152310'

CONTENT_PATH_DEFAULT='SkullGirls'
CONTENT_PATH_DEFAULT_0='data'
CONTENT_LIBS_BIN32_PATH='SkullGirls/lib/i686-pc-linux-gnu'
CONTENT_LIBS_BIN32_PATH_0='data/i686/lib/i686-pc-linux-gnu'
## libSDL2_mixer-2.0.so.0 must be included,
## because it includes a non-standard symbol (SDL_AudioStreamGet) that libSDL2_locale.so.0 is relying on.
## libSDL2-2.0.so.0 must be included to prevent a silent crash on start-up.
CONTENT_LIBS_BIN32_FILES='
libSDL2-2.0.so.0
libSDL2_locale.so.0
libSDL2_mixer-2.0.so.0'
CONTENT_GAME_BIN32_PATH_0='data/i686'
CONTENT_GAME_BIN32_FILES='
SkullGirls.i686-pc-linux-gnu'
CONTENT_LIBS_BIN64_PATH='SkullGirls/lib/x86_64-pc-linux-gnu'
CONTENT_LIBS_BIN64_PATH_0='data/x86_64/lib/x86_64-pc-linux-gnu'
CONTENT_LIBS_BIN64_FILES='
libSDL2-2.0.so.0
libSDL2_locale.so.0
libSDL2_mixer-2.0.so.0'
CONTENT_GAME_BIN64_PATH_0='data/x86_64'
CONTENT_GAME_BIN64_FILES='
SkullGirls.x86_64-pc-linux-gnu'
CONTENT_GAME_DATA_PATH_0='data/noarch'
CONTENT_GAME_DATA_FILES='
data01
Salmon
Icon.png
*.txt'

APP_MAIN_EXE_BIN32='SkullGirls.i686-pc-linux-gnu'
APP_MAIN_EXE_BIN64='SkullGirls.x86_64-pc-linux-gnu'
APP_MAIN_ICON='Icon.png'

PACKAGES_LIST='PKG_BIN32 PKG_BIN64 PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN32_ARCH='32'
PKG_BIN32_DEPS="$PKG_DATA_ID"
PKG_BIN32_DEPENDENCIES_LIBRARIES='
libc.so.6
libgcc_s.so.1
libm.so.6
libpthread.so.0
libstdc++.so.6'

PKG_BIN64_ARCH='64'
PKG_BIN64_DEPS="$PKG_BIN32_DEPS"

# Load common functions

target_version='2.21'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

for PKG in 'PKG_BIN32' 'PKG_BIN64'; do
	# shellcheck disable=SC2119
	launchers_write
done

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
