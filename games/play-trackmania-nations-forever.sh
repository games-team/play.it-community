#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Emmanuel Gil Peyrot <linkmauve@linkmauve.fr>
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2024 BetaRays <BetaRays@proton.me>
set -o errexit

###
# TrackMania Nations Forever
# build native packages from the original installers
# send your bug reports to contact@dotslashplay.it
###

script_version=20240326.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='trackmania-nations-forever'
GAME_NAME='TrackMania Nations Forever'

ARCHIVE_BASE_0_NAME='tmnationsforever_setup.exe'
ARCHIVE_BASE_0_MD5='2a36d70989f94ba9369993749ff20640'
ARCHIVE_BASE_0_EXTRACTOR='bsdtar'
ARCHIVE_BASE_0_SIZE='520000'
ARCHIVE_BASE_0_VERSION='2.11.26-1'
ARCHIVE_BASE_0_URL='https://trackmaniaforever.com/nations/'

ARCHIVE_OPTIONAL_TMCP_0_NAME='TMCompetitionPatch_1.5.1.zip'
ARCHIVE_OPTIONAL_TMCP_0_MD5='6420ae68976973e57cf1d5a1e135bae7'
ARCHIVE_OPTIONAL_TMCP_0_SIZE='2200'
ARCHIVE_OPTIONAL_TMCP_0_VERSION='1.5.1'
ARCHIVE_OPTIONAL_TMCP_0_URL='https://donadigo.com/tmcp'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
binkw32.dll
openal32.dll
thumbgbx.dll
thumbgbx.tlb
tmforever.exe
tmforeverlauncher.exe
wrap_oal.dll'
CONTENT_GAME_DATA_FILES='
gamedata
packs
tmforever.map
gbx.ico
launchicon.png
nadeo.ini'
# TMCP
CONTENT_GAME0_BIN_PATH='.'
CONTENT_GAME0_BIN_FILES='
dinput8.dll
TMCompPatch.dll'

APP_MAIN_TYPE='wine'
APP_MAIN_EXE='tmforeverlauncher.exe'
APP_MAIN_ICON='launchicon.png'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

WINE_DIRECT3D_RENDERER='dxvk'

# Store game progress and configuration in persistent paths

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/TmForever'

# innoextract is required to extract the content of an inner archive

SCRIPT_DEPS="$SCRIPT_DEPS innoextract"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

archive_initialize_optional 'ARCHIVE_TMCP' 'ARCHIVE_OPTIONAL_TMCP_0'

# Extract game data

archive_extraction_default
ARCHIVE_INNER_PATH="${PLAYIT_WORKDIR}/gamedata/TmNationsForever_Setup_Tmp.exe"
ARCHIVE_INNER_TYPE='innosetup'
archive_extraction 'ARCHIVE_INNER'
rm "$ARCHIVE_INNER_PATH" "${PLAYIT_WORKDIR}/gamedata/TmNationsForever_Setup_Tmp-1.bin"

if archive_is_available 'ARCHIVE_TMCP'; then
	archive_extraction 'ARCHIVE_TMCP'
	APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}"'
export WINEDLLOVERRIDES="dinput8.dll=n,b;$WINEDLLOVERRIDES"'
fi

# Include game icons

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build package

packages_generation

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

# Print instructions

print_instructions

exit 0
