#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Giants: Citizen Kabuto
# send your bug reports to contact@dotslashplay.it
###

script_version=20230910.2

GAME_ID='giants-citizen-kabuto'
GAME_NAME='Giants: Citizen Kabuto'

ARCHIVE_BASE_0='setup_giants_2.1.0.4.exe'
ARCHIVE_BASE_0_MD5='33015108ece9e52b1f525880f0867e11'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_VERSION='1.4-gog2.1.0.4'
ARCHIVE_BASE_0_SIZE='1600000'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/giants_citizen_kabuto'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
eaxman.dll
gs_ds.dll
*.ini
*.exe'
CONTENT_GAME0_BIN_PATH="${CONTENT_PATH_DEFAULT}/__support/dx7"
CONTENT_GAME0_BIN_FILES='
gg_dx7r.dll'
CONTENT_GAME_DATA_FILES='
bin
music
stream
streamenglish
streamfrench
streamgerman
streamitalian
streamspanish
giants.ico
language_setup.png'
CONTENT_DOC_DATA_FILES='
manual.pdf
*.txt'

USER_PERSISTENT_DIRECTORIES='
savegame'
USER_PERSISTENT_FILES='
*.ini'

APP_MAIN_EXE='giants.exe'
APP_MAIN_ICON='giants.ico'

APP_L10N_ID="${GAME_ID}-language"
APP_L10N_NAME="$GAME_NAME - Language setup"
APP_L10N_EXE='language_setup.exe'
APP_L10N_ICON='giants.ico'
APP_L10N_CAT='Settings'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Set required registry keys

destdir="C:\\\\${GAME_ID}"
registry_dump_init_file='registry-dumps/init.reg'
registry_dump_init_content='Windows Registry Editor Version 5.00

[HKEY_CURRENT_USER\Software\PlanetMoon\Giants]
"1.00.000"=""
"DefPlayer"="Player"
"DestDir"="'"${destdir}"'"
"Language"="9"
"SrcDir"="C:\\"

[HKEY_CURRENT_USER\Software\PlanetMoon\Giants\IDs\Player]
"GameOptions"=dword:00000bf3
"VideoDepth"=dword:00000020
"VideoDevGuid"=hex:e0,3d,e6,84,aa,46,cf,11,81,6f,00,00,c0,20,15,6e
"VideoDrvGuid"=hex:3e,1e,b7,d7,d5,41,cf,11,f3,73,25,a2,0e,c2,cd,35'
CONTENT_GAME_BIN_FILES="${CONTENT_GAME_BIN_FILES:-}
$registry_dump_init_file"
APP_REGEDIT="${APP_REGEDIT:-} $registry_dump_init_file"
SCRIPT_DEPS="${SCRIPT_DEPS:-} iconv"

# Load common functions

target_version='2.25'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Set required registry keys
	mkdir --parents "$(dirname "$registry_dump_init_file")"
	printf '%s' "$registry_dump_init_content" | \
		iconv --from-code=UTF-8 --to-code=UTF-16 --output="$registry_dump_init_file"
)

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game files

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
