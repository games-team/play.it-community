#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Bard's Tale 4
# send your bug reports to contact@dotslashplay.it
###

script_version=20231110.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='the-bards-tale-4'
GAME_NAME='The Bardʼs Tale Ⅳ'

ARCHIVE_BASE_0_NAME='the_bard_s_tale_iv_director_s_cut_update_3_34066.sh'
ARCHIVE_BASE_0_MD5='695a04c36fe4a5ff2a53b93062151047'
ARCHIVE_BASE_0_SIZE='39000000'
ARCHIVE_BASE_0_VERSION='4.20.1-gog34066'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/the_bards_tale_iv_directors_cut_standard_edition'

UNREALENGINE4_NAME='BardsTale4'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_DATA_MOVIES_1_FILES='
BardsTale4/Content/Movies/*.mp3
BardsTale4/Content/Movies/*.mp4
BardsTale4/Content/Movies/*.wav
BardsTale4/Content/Movies/Epilogue_Credits
BardsTale4/Content/Movies/PoMMovies'
CONTENT_GAME_DATA_MOVIES_2_FILES='
BardsTale4/Content/Movies/Anamatics
BardsTale4/Content/Movies/BardIV_FireLogo_Img_Sequence
BardsTale4/Content/Movies/BardIV_InxileLogo_Img_Sequence
BardsTale4/Content/Movies/BardIV_LegalScreen_Img_Sequence
BardsTale4/Content/Movies/Opening_Img_Sequence
BardsTale4/Content/Movies/SpecialFeatures'
CONTENT_GAME_DATA_PAKS_1_FILES='
BardsTale4/Content/Paks/pakchunk0-LinuxNoEditor.pak.split00
BardsTale4/Content/Paks/pakchunk0-LinuxNoEditor.pak.split01
BardsTale4/Content/Paks/pakchunk1-LinuxNoEditor.pak'
CONTENT_GAME_DATA_PAKS_2_FILES='
BardsTale4/Content/Paks/pakchunk2-LinuxNoEditor.pak.split00
BardsTale4/Content/Paks/pakchunk2-LinuxNoEditor.pak.split01
BardsTale4/Content/Paks/pakchunk50-LinuxNoEditor.pak
BardsTale4/Content/Paks/pakchunk60-LinuxNoEditor.pak
BardsTale4/Content/Paks/pakchunk70-LinuxNoEditor.pak
BardsTale4/Content/Paks/pakchunk80-LinuxNoEditor.pak
BardsTale4/Content/Paks/pakchunk90-LinuxNoEditor.pak
BardsTale4/Content/Paks/pakchunk100-LinuxNoEditor.pak
BardsTale4/Content/Paks/pakchunk110-LinuxNoEditor.pak
BardsTale4/Content/Paks/pakchunk120-LinuxNoEditor.pak
BardsTale4/Content/Paks/pakchunk130-LinuxNoEditor.pak
BardsTale4/Content/Paks/pakchunk140-LinuxNoEditor.pak
BardsTale4/Content/Paks/pakchunk150-LinuxNoEditor.pak'

APP_MAIN_EXE="${UNREALENGINE4_NAME}/Binaries/Linux/${UNREALENGINE4_NAME}-Linux-Shipping"
## The following hacks can not be avoided by forcing the use of system-provided SDL,
## because the game crashes on launch when it is used instead of the shipped SDL build.
### The game crashes on launch when the Wayland backend of SDL is used.
APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}"'
# The game crashes on launch when the Wayland backend of SDL is used
if [ "${SDL_VIDEODRIVER:-}" = "wayland" ]; then
	unset SDL_VIDEODRIVER
fi
'
### It seems that the shipped SDL build does not support the alsa backend.
### When it is used, no error is triggered but no sound is played either.
APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}"'
# The game does not output any sound when the ALSA backend of SDL is used
if [ "${SDL_AUDIODRIVER:-}" = "alsa" ]; then
	unset SDL_AUDIODRIVER
fi
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA_MOVIES_1
PKG_DATA_MOVIES_2
PKG_DATA_PAKS_1
PKG_DATA_PAKS_2
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_DATA_MOVIES_ID="${PKG_DATA_ID}-movies"
PKG_DATA_MOVIES_1_ID="${PKG_DATA_MOVIES_ID}-1"
PKG_DATA_MOVIES_2_ID="${PKG_DATA_MOVIES_ID}-2"
PKG_DATA_MOVIES_DESCRIPTION="$PKG_DATA_DESCRIPTION - movies"
PKG_DATA_MOVIES_1_DESCRIPTION="$PKG_DATA_MOVIES_DESCRIPTION - 1"
PKG_DATA_MOVIES_2_DESCRIPTION="$PKG_DATA_MOVIES_DESCRIPTION - 2"
PKG_DATA_DEPS="${PKG_DATA_DEPS:-} $PKG_DATA_MOVIES_1_ID $PKG_DATA_MOVIES_2_ID"

PKG_DATA_PAKS_ID="${PKG_DATA_ID}-paks"
PKG_DATA_PAKS_1_ID="${PKG_DATA_PAKS_ID}-1"
PKG_DATA_PAKS_2_ID="${PKG_DATA_PAKS_ID}-2"
PKG_DATA_PAKS_DESCRIPTION="$PKG_DATA_DESCRIPTION - paks"
PKG_DATA_PAKS_1_DESCRIPTION="$PKG_DATA_PAKS_DESCRIPTION - 1"
PKG_DATA_PAKS_2_DESCRIPTION="$PKG_DATA_PAKS_DESCRIPTION - 2"
PKG_DATA_DEPS="${PKG_DATA_DEPS:-} $PKG_DATA_PAKS_1_ID $PKG_DATA_PAKS_2_ID"

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Check for the presence of extra optional archives

ARCHIVE_OPTIONAL_ICONS_0_NAME='the-bards-tale-4_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_0_URL='https://downloads.dotslashplay.it/games/the-bards-tale-4'
ARCHIVE_OPTIONAL_ICONS_0_MD5='aede8ef2c559ecf8e69e1f5ffef7432d'

archive_initialize_optional \
	'ARCHIVE_ICONS' \
	'ARCHIVE_OPTIONAL_ICONS_0'

# Set the package scripts rebuilding the full files from their chunks

for huge_file in \
	"$(unrealengine4_name)/Content/Paks/pakchunk0-LinuxNoEditor.pak" \
	"$(unrealengine4_name)/Content/Paks/pakchunk2-LinuxNoEditor.pak"
do
	PKG_DATA_POSTINST_RUN="$(package_postinst_actions 'PKG_DATA')
	$(huge_file_concatenate "$huge_file")"
	PKG_DATA_PRERM_RUN="$(package_prerm_actions 'PKG_DATA')
	$(huge_file_delete "$huge_file")"
done

# Extract game data

archive_extraction_default
if archive_is_available 'ARCHIVE_ICONS'; then
	archive_extraction 'ARCHIVE_ICONS'
fi

# Include game data

if archive_is_available 'ARCHIVE_ICONS'; then
	CONTENT_ICONS_DATA_PATH='.'
	CONTENT_ICONS_DATA_FILES='
	16x16
	32x32
	64x64
	128x128
	256x256'
	content_inclusion 'ICONS_DATA' 'PKG_DATA' "$(path_icons)"
else
	APP_MAIN_ICON='../support/icon.png'
	set_current_package 'PKG_DATA'
	# shellcheck disable=SC2119
	icons_inclusion
fi
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
