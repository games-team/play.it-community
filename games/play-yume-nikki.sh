#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 BetaRays
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Yume Nikki
# send your bug reports to contact@dotslashplay.it
###

script_version=20241124.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='yume-nikki'
GAME_NAME='Yume Nikki'

## This archive is no longer available for sale from Playism store,
## as they are now only a Steam keys reseller.
ARCHIVE_BASE_0_NAME='YumeNikki_EN.zip'
ARCHIVE_BASE_0_MD5='fd1e659f777ad81bd61ebd6df573140e'
ARCHIVE_BASE_0_SIZE='66000'
ARCHIVE_BASE_0_VERSION='0.10a-playism'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
RPG_RT.*
Harmony.dll'
CONTENT_GAME_DATA_FILES='
Map*.lmu
Backdrop
Battle
Battle2
BattleCharSet
BattleWeapon
CharSet
ChipSet
FaceSet
Frame
GameOver
Monster
Movie
Music
Panorama
Picture
Sound
System
System2
Title'
CONTENT_DOC_DATA_PATH='YumeNikki_EN'
CONTENT_DOC_DATA_FILES='
*.txt'

APP_MAIN_EXE='RPG_RT.exe'
## Japanese locale is required for correct fonts display.
APP_MAIN_PRERUN='
# Japanese locale is required for correct fonts display
export LC_ALL=ja_JP.UTF-8
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
## TODO: A dependency on the wqy-microhei.ttc font should be added.

# Add requirement for the extration of the inner .lzh archive

SCRIPT_DEPS="${SCRIPT_DEPS:-} lha"

# Add requirements for tweaking the encoding of file names and contents

SCRIPT_DEPS="${SCRIPT_DEPS:-} iconv convmv"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
ARCHIVE_INNER_PATH="${PLAYIT_WORKDIR}/gamedata/YumeNikki_EN/YumeNikki.lzh"
ARCHIVE_INNER_TYPE='lha'
archive_extraction 'ARCHIVE_INNER'
rm "$(archive_path 'ARCHIVE_INNER')"
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Convert file paths to UTF-8 encoding.
	find . -exec \
		convmv --notest -f SHIFT-JIS -t UTF-8 {} + >/dev/null 2>/dev/null
	
	## Fix Windows-style paths.
	sed --in-place 's/¥/\\/g' 'YumeNikki_EN/YumeNikkiREADME.txt'

	## Convert the text files contents to UTF-8 encoding.
	shell_command='contents=$(iconv --from-code CP932 --to-code UTF-8 "$1")'
	shell_command="$shell_command"'; printf "%s" "$contents" > "$1"'
	find 'YumeNikki_EN' -name '*.txt' -exec \
		sh -c "$shell_command" -- '{}' \;
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
