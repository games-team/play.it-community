#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Endzone 1
# send your bug reports to contact@dotslashplay.it
###

script_version=20240606.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='endzone-1'
GAME_NAME='Endzone: A World Apart'

ARCHIVE_BASE_EN_0_NAME='setup_endzone_-_a_world_apart_1.2.8630.30586_(64bit)_(66949).exe'
ARCHIVE_BASE_EN_0_MD5='01a625f5c0e07d324fb07d8ac43fdd14'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_PART1_NAME='setup_endzone_-_a_world_apart_1.2.8630.30586_(64bit)_(66949)-1.bin'
ARCHIVE_BASE_EN_0_PART1_MD5='de1aba989b4798154fce6e4fa2ffbf46'
ARCHIVE_BASE_EN_0_SIZE='4242544'
ARCHIVE_BASE_EN_0_VERSION='1.2.8630.30586-gog66949'
ARCHIVE_BASE_EN_0_URL='https://www.gog.com/game/endzone_a_world_apart'

ARCHIVE_BASE_FR_0_NAME='setup_endzone_-_a_world_apart_1.2.8630.30586_(french_64bit)_(66949).exe'
ARCHIVE_BASE_FR_0_MD5='003ad80a6a2ce4123ffe2d98c5fe0de5'
ARCHIVE_BASE_FR_0_TYPE='innosetup'
ARCHIVE_BASE_FR_0_PART1_NAME='setup_endzone_-_a_world_apart_1.2.8630.30586_(french_64bit)_(66949)-1.bin'
ARCHIVE_BASE_FR_0_PART1_MD5='5f68efd024f84f71edad63e2122f7bf3'
ARCHIVE_BASE_FR_0_SIZE='4244580'
ARCHIVE_BASE_FR_0_VERSION='1.2.8630.30586-gog66949'
ARCHIVE_BASE_FR_0_URL='https://www.gog.com/game/endzone_a_world_apart'

UNITY3D_NAME='endzone'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_L10N_FILES='
localizations'

WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Gentlymad Studios/Endzone'

PACKAGES_LIST='
PKG_BIN
PKG_L10N
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_L10N_BASE
PKG_DATA'

PKG_L10N_BASE_ID="${GAME_ID}-l10n"
PKG_L10N_ID_EN="${PKG_L10N_BASE_ID}-en"
PKG_L10N_ID_FR="${PKG_L10N_BASE_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_BASE_ID"
PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_FR='French localization'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
