#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Darksiders 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20230930.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='darksiders-2'
GAME_NAME='Darksiders Ⅱ'

ARCHIVE_BASE_0_NAME='setup_darksiders2_deathinitive_2.1.0.4.exe'
ARCHIVE_BASE_0_MD5='5b39f665a8367f4ebe7c845b2ebbac81'
ARCHIVE_BASE_0_EXTRACTOR='innoextract'
ARCHIVE_BASE_0_EXTRACTOROPTIONS='--lowercase --gog'
ARCHIVE_BASE_0_PART1_NAME='setup_darksiders2_deathinitive_2.1.0.4-1.bin'
ARCHIVE_BASE_0_PART1_MD5='ec9c85b15f23b79bd87668282b8101ed'
ARCHIVE_BASE_0_PART1_EXTRACTOR='unar'
ARCHIVE_BASE_0_PART2_NAME='setup_darksiders2_deathinitive_2.1.0.4-2.bin'
ARCHIVE_BASE_0_PART2_MD5='4eb87fd00792a0e35b0abd8b7bc124e0'
ARCHIVE_BASE_0_PART2_EXTRACTOR='unar'
ARCHIVE_BASE_0_PART3_NAME='setup_darksiders2_deathinitive_2.1.0.4-3.bin'
ARCHIVE_BASE_0_PART3_MD5='113f5a09d0289db3b1c5a5f85a82849e'
ARCHIVE_BASE_0_PART3_EXTRACTOR='unar'
ARCHIVE_BASE_0_PART4_NAME='setup_darksiders2_deathinitive_2.1.0.4-4.bin'
ARCHIVE_BASE_0_PART4_MD5='ca12e47f9cce195b09a66c7ff27121ab'
ARCHIVE_BASE_0_PART4_EXTRACTOR='unar'
ARCHIVE_BASE_0_SIZE='16000000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.1.0.4'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/darksiders_ii_deathinitive_edition'

CONTENT_PATH_DEFAULT='game'
CONTENT_GAME_BIN_FILES='
darksiders2.exe
*.dll'
CONTENT_GAME_DATA_MEDIA_FILES='
media/media.upak'
CONTENT_GAME_DATA_SOUNDS_FILES='
media/sounds_streamed'
CONTENT_GAME_DATA_FILES='
darksiders2.dsl
media'
CONTENT_DOC_DATA_FILES='
manual.pdf'

WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Local/Darksiders2
users/${USER}/Documents/My Games/Darksiders2'

APP_MAIN_EXE='darksiders2.exe'

PACKAGES_LIST='PKG_BIN PKG_DATA_MEDIA PKG_DATA_SOUNDS PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_DATA_MEDIA_ID="${PKG_DATA_ID}-media"
PKG_DATA_MEDIA_DESCRIPTION="$PKG_DATA_DESCRIPTION - media"
PKG_DATA_DEPS="${PKG_DATA_DEPS:-} $PKG_DATA_MEDIA_ID"

PKG_DATA_SOUNDS_ID="${PKG_DATA_ID}-sounds"
PKG_DATA_SOUNDS_DESCRIPTION="$PKG_DATA_DESCRIPTION - sounds"
PKG_DATA_DEPS="${PKG_DATA_DEPS:-} $PKG_DATA_SOUNDS_ID"

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
