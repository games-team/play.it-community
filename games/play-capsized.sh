#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 VA
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Capsized
# send your bug reports to contact@dotslashplay.it
###

script_version=20240830.3

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='capsized'
GAME_NAME='Capsized'

ARCHIVE_BASE_GOG_0_NAME='gog_capsized_2.0.0.2.sh'
ARCHIVE_BASE_GOG_0_MD5='f516c52b4614d63c3cfa4e1ed43934b8'
ARCHIVE_BASE_GOG_0_SIZE='670000'
ARCHIVE_BASE_GOG_0_VERSION='1.0-gog2.0.0.2'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/capsized'

ARCHIVE_BASE_HUMBLE_0_NAME='capsized-12212015-bin'
ARCHIVE_BASE_HUMBLE_0_MD5='10515ca5f73e38151e17766cba97f3ed'
## This MojoSetup installer is not relying on a Makeself wrapper
ARCHIVE_BASE_HUMBLE_0_EXTRACTOR='bsdtar'
ARCHIVE_BASE_HUMBLE_0_SIZE='650000'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.0-humble151221'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/capsized'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='data'
CONTENT_LIBS_FILES='
libmojoshader.so
libtheoraplay.so'
## The shipped build of OpenAL must be used, to prevent a crash on launch
CONTENT_LIBS_FILES="${CONTENT_LIBS_FILES:-}
libopenal.so.1"
CONTENT_LIBS_LIBS64_PATH_GOG="${CONTENT_PATH_DEFAULT_GOG}/lib64"
CONTENT_LIBS_LIBS64_PATH_HUMBLE="${CONTENT_PATH_DEFAULT_HUMBLE}/lib64"
CONTENT_LIBS_LIBS64_FILES="$CONTENT_LIBS_FILES"
CONTENT_LIBS_LIBS32_PATH_GOG="${CONTENT_PATH_DEFAULT_GOG}/lib"
CONTENT_LIBS_LIBS32_PATH_HUMBLE="${CONTENT_PATH_DEFAULT_HUMBLE}/lib"
CONTENT_LIBS_LIBS32_FILES="$CONTENT_LIBS_FILES"
CONTENT_GAME_MAIN_FILES='
Content
mono
de
es
fr
it
Capsized.bmp
NePlusUltra.exe
FarseerPhysicsXNA.dll
FNA.dll
FNA.dll.config
Lidgren.Network.dll
ProjectMercury.dll'
CONTENT_DOC_MAIN_FILES='
Linux.README'

APP_MAIN_EXE='NePlusUltra.exe'
APP_MAIN_ICON='Capsized.bmp'

PACKAGES_LIST='
PKG_MAIN
PKG_LIBS64
PKG_LIBS32'

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_LIBS'
PKG_MAIN_DEPENDENCIES_LIBRARIES='
libSDL2-2.0.so.0'
PKG_MAIN_DEPENDENCIES_MONO_LIBRARIES='
mscorlib.dll
Mono.Posix.dll
Mono.Security.dll
System.dll
System.Configuration.dll
System.Core.dll
System.Data.dll
System.Drawing.dll
System.Runtime.Serialization.dll
System.Security.dll
System.Xml.dll'

PKG_LIBS_ID="${GAME_ID}-libs"
PKG_LIBS64_ID="$PKG_LIBS_ID"
PKG_LIBS32_ID="$PKG_LIBS_ID"
PKG_LIBS_DESCRIPTION='Shipped libraries'
PKG_LIBS64_DESCRIPTION="$PKG_LIBS_DESCRIPTION"
PKG_LIBS32_DESCRIPTION="$PKG_LIBS_DESCRIPTION"
PKG_LIBS64_ARCH='64'
PKG_LIBS32_ARCH='32'
PKG_LIBS_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libm.so.6
libogg.so.0
libpthread.so.0
librt.so.1
libtheoradec.so.1
libtheora.so.0
libvorbis.so.0'
PKG_LIBS32_DEPENDENCIES_LIBRARIES="$PKG_LIBS_DEPENDENCIES_LIBRARIES"
PKG_LIBS64_DEPENDENCIES_LIBRARIES="$PKG_LIBS_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
