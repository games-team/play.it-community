#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 mortalius
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Kingdom New Lands
# send your bug reports to contact@dotslashplay.it
###

script_version=20221210.1

GAME_ID='kingdom-new-lands'
GAME_NAME='Kingdom New Lands'

UNITY3D_NAME='Kingdom'

ARCHIVE_BASE_1='kingdom_new_lands_en_1_2_8_19096.sh'
ARCHIVE_BASE_1_MD5='3499d709e78410ef7f447c12e3c66039'
ARCHIVE_BASE_1_VERSION='1.2.8-gog19096'
ARCHIVE_BASE_1_SIZE='450000'
ARCHIVE_BASE_1_TYPE='mojosetup'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/kingdom_new_lands'

ARCHIVE_BASE_0='gog_kingdom_new_lands_2.6.0.8.sh'
ARCHIVE_BASE_0_MD5='0d662366f75d5da214e259d792e720eb'
ARCHIVE_BASE_0_TYPE='mojosetup'
ARCHIVE_BASE_0_SIZE='420000'
ARCHIVE_BASE_0_VERSION='1.2.3-gog2.6.0.8'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN32_FILES="
${UNITY3D_NAME}_Data/Mono/x86
${UNITY3D_NAME}_Data/Plugins/x86
${UNITY3D_NAME}.x86"
CONTENT_GAME_BIN64_FILES="
${UNITY3D_NAME}_Data/Mono/x86_64
${UNITY3D_NAME}_Data/Plugins/x86_64
${UNITY3D_NAME}.x86_64"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data"

PACKAGES_LIST='PKG_BIN32 PKG_BIN64 PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN32_ARCH='32'
PKG_BIN32_DEPS="$PKG_DATA_ID"
PKG_BIN32_DEPENDENCIES_LIBRARIES='
ld-linux-x86-64.so.2
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

PKG_BIN64_ARCH='64'
PKG_BIN64_DEPS="$PKG_BIN32_DEPS"
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN32_DEPENDENCIES_LIBRARIES"

# Load common functions

target_version='2.19'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

for PKG in 'PKG_BIN32' 'PKG_BIN64'; do
	# shellcheck disable=SC2119
	launchers_write
done

# Build package

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
