#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Unwording
# send your bug reports to contact@dotslashplay.it
###

script_version=20231110.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='unwording'
GAME_NAME='Unwording'

ARCHIVE_BASE_0_NAME='unwording_1_02_64447.sh'
ARCHIVE_BASE_0_MD5='398d9c4d58b90b2632410d60704b9202'
ARCHIVE_BASE_0_SIZE='1500000'
ARCHIVE_BASE_0_VERSION='1.02-gog64447'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/unwording'

UNITY3D_NAME='Unwording'
UNITY3D_PLUGINS='
lib_burst_generated.so'

CONTENT_PATH_DEFAULT='data/noarch/game'

## The game comes with a generic Unity3D icon,
## so we use the GOG-provided icon instead.
APP_MAIN_ICON='../support/icon.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
