#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 BetaRays
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Blocks that matter
# send your bug reports to contact@dotslashplay.it
###

script_version=20250305.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='blocks-that-matter'
GAME_NAME='Blocks that matter'

ARCHIVE_BASE_GOG_0_NAME='gog_blocks_that_matter_2.0.0.3.sh'
ARCHIVE_BASE_GOG_0_MD5='af9cec2b6104720c32718c02be120657'
ARCHIVE_BASE_GOG_0_SIZE='200000'
ARCHIVE_BASE_GOG_0_VERSION='1.1.0.4c-gog2.0.0.3'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/blocks_that_matter'

ARCHIVE_BASE_HUMBLE_0_NAME='BlocksThatMatter_linux_1.1.0.3.zip'
ARCHIVE_BASE_HUMBLE_0_MD5='a3c8509ba8b3ffc52522a1eac3dc0f10'
ARCHIVE_BASE_HUMBLE_0_SIZE='190000'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.1.0.3-humble1'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/blocks-that-matter'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='Blocks That Matter linux 1.1.0.3'
## Include shipped libraries that can not be replaced by system ones.
CONTENT_LIBS_LIBS32_FILES='
libjinput-linux.so
liblwjgl.so'
CONTENT_LIBS_LIBS64_FILES='
libjinput-linux64.so
liblwjgl64.so'
CONTENT_GAME_MAIN_FILES='
BTM_lib
BTM.jar
BTM.png
BTM.bftm
config/*.xml'
CONTENT_DOC_MAIN_RELATIVE_PATH='README'
CONTENT_DOC_MAIN_FILES='
*.txt'

USER_PERSISTENT_FILES='
config/*.xml'

APP_MAIN_TYPE='java'
APP_MAIN_EXE='BTM.jar'
APP_MAIN_ICON='BTM.png'

PACKAGES_LIST='
PKG_MAIN
PKG_LIBS64
PKG_LIBS32'

PKG_MAIN_DEPENDENCIES_LIBRARIES='
libopenal.so.1'
PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_LIBS'

PKG_LIBS_ID="${GAME_ID}-libs"
PKG_LIBS64_ID="$PKG_LIBS_ID"
PKG_LIBS32_ID="$PKG_LIBS_ID"
PKG_LIBS64_ARCH='64'
PKG_LIBS32_ARCH='32'
PKG_LIBS_DEPENDENCIES_LIBRARIES='
libc.so.6
libm.so.6
libpthread.so.0
libX11.so.6
libXcursor.so.1
libXext.so.6
libXrandr.so.2
libXxf86vm.so.1'
PKG_LIBS64_DEPENDENCIES_LIBRARIES="$PKG_LIBS_DEPENDENCIES_LIBRARIES"
PKG_LIBS32_DEPENDENCIES_LIBRARIES="$PKG_LIBS_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

## The game binary looks for libopenal.so (32-bit) and libopenal64.so (64-bit) instead of libopenal.so.1
## Unversioned libopenal.so is already provided by the base library packages on Arch Linux and Gentoo.
case "$(option_value 'package')" in
	('arch')
		library_source='/usr/lib/libopenal.so.1'
		library_destination="$(package_path 'PKG_LIBS64')$(path_libraries)/libopenal64.so"
		mkdir --parents "$(dirname "$library_destination")"
		ln --symbolic "$library_source" "$library_destination"
	;;
	('deb')
		library_source_32bit='/usr/lib/i386-linux-gnu/libopenal.so.1'
		library_source_64bit='/usr/lib/x86_64-linux-gnu/libopenal.so.1'
		library_destination_32bit="$(package_path 'PKG_LIBS32')$(path_libraries)/libopenal.so"
		library_destination_64bit="$(package_path 'PKG_LIBS64')$(path_libraries)/libopenal64.so"
		mkdir --parents \
			"$(dirname "$library_destination_32bit")" \
			"$(dirname "$library_destination_64bit")"
		ln --symbolic "$library_source_32bit" "$library_destination_32bit"
		ln --symbolic "$library_source_64bit" "$library_destination_64bit"
	;;
	('gentoo'|'egentoo')
		library_source='/usr/lib64/libopenal.so.1'
		library_destination="$(package_path 'PKG_LIBS64')$(path_libraries)/libopenal64.so"
		mkdir --parents "$(dirname "$library_destination")"
		ln --symbolic "$library_source" "$library_destination"
	;;
esac

## The path to liblwjgl.so needs to be given on the command line
APP_MAIN_JAVA_OPTIONS="$(application_java_options 'APP_MAIN') -Dorg.lwjgl.librarypath='$(path_libraries)'"

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
