#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 VA
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Costume Quest
# send your bug reports to contact@dotslashplay.it
###

script_version=20230227.2

GAME_ID='costume-quest'
GAME_NAME='Costume Quest'

ARCHIVE_BASE_0='gog_costume_quest_2.0.0.3.sh'
ARCHIVE_BASE_0_MD5='3c2f2126be1ca2148f333c453341b810'
ARCHIVE_BASE_0_TYPE='mojosetup'
ARCHIVE_BASE_0_SIZE='650000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.0.0.3'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/costume_quest'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN_PATH="${CONTENT_PATH_DEFAULT}/lib"
CONTENT_LIBS_BIN_FILES='
libfmodevent-4.42.16.so
libfmodeventnet-4.42.16.so
libfmodex-4.42.16.so
libSDL2-2.0.so.0'
CONTENT_GAME_BIN_FILES='
Cq.bin.x86'
CONTENT_GAME_DATA_FILES='
DFCONFIG
Data/Config
Linux/Packs
OGL/Shaders
Win/Audio/Music
Win/Audio/Music_DLC
Win/Audio/SFX
Win/Audio/CostumeQuest_USEnglish
Win/Audio/Voice'
CONTENT_DOC_DATA_PATH='data/noarch/docs'
CONTENT_DOC_DATA_FILES='*'

APP_MAIN_EXE='Cq.bin.x86'
APP_MAIN_ICON='../support/icon.png'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libstdc++.so.6
libopenal.so.1
libGL.so.1
libGLU.so.1
libSDL2-2.0.so.0'

# Load common functions

target_version='2.21'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "${PLAYIT_WORKDIR}"

exit 0
