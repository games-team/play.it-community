#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Grow: Song of the Evertree
# send your bug reports to contact@dotslashplay.it
###

script_version=20230705.2

GAME_ID='grow-song-of-the-evertree'
GAME_NAME='Grow: Song of the Evertree'

ARCHIVE_BASE_0='setup_grow_song_of_the_evertree_1.0.6.3369_(64bit)_(54063).exe'
ARCHIVE_BASE_0_MD5='a099df449e5f892ea7684b6a21868a2b'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1='setup_grow_song_of_the_evertree_1.0.6.3369_(64bit)_(54063)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='34d143e6eb44b5a3637419cb9245c5ad'
ARCHIVE_BASE_0_SIZE='2600000'
ARCHIVE_BASE_0_VERSION='1.0.6.3369-gog54063'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/grow_song_of_the_evertree'

UNITY3D_NAME='grow'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES="
${UNITY3D_NAME}.exe
${UNITY3D_NAME}_data/plugins
monobleedingedge
unityplayer.dll"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_data"

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/505 Games/Grow_ Song of The Evertree'

APP_MAIN_TYPE='wine'
APP_MAIN_EXE="${UNITY3D_NAME}.exe"

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

target_version='2.23'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Delete unwanted files
	rm --recursive \
		'__redist' \
		'app' \
		'commonappdata' \
		'tmp'
)

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
