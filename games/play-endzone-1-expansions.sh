#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Endzone 1 expansions:
# - Halloween
# send your bug reports to contact@dotslashplay.it
###

script_version=20240606.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='endzone-1'
GAME_NAME='Endzone: A World Apart'

EXPANSION_ID_HALLOWEEN='halloween'
EXPANSION_NAME_HALLOWEEN='Halloween'

# Archives

## Halloween

ARCHIVE_BASE_HALLOWEEN_EN_0_NAME='setup_endzone_-_a_world_apart_happy_halloween_1.2.8630.30586_(64bit)_(66949).exe'
ARCHIVE_BASE_HALLOWEEN_EN_0_MD5='377587121fed6ba7f8a2d914cdbfdbf7'
ARCHIVE_BASE_HALLOWEEN_EN_0_TYPE='innosetup'
ARCHIVE_BASE_HALLOWEEN_EN_0_SIZE='26268'
ARCHIVE_BASE_HALLOWEEN_EN_0_VERSION='1.2.8630.30586-gog66949'
ARCHIVE_BASE_HALLOWEEN_EN_0_URL='https://www.gog.com/game/endzone_a_world_apart_halloween'

ARCHIVE_BASE_HALLOWEEN_FR_0_NAME='setup_endzone_-_a_world_apart_happy_halloween_1.2.8630.30586_(french_64bit)_(66949).exe'
ARCHIVE_BASE_HALLOWEEN_FR_0_MD5='f50c8540bc740a57d4b430c92d608160'
ARCHIVE_BASE_HALLOWEEN_FR_0_TYPE='innosetup'
ARCHIVE_BASE_HALLOWEEN_FR_0_SIZE='26268'
ARCHIVE_BASE_HALLOWEEN_FR_0_VERSION='1.2.8630.30586-gog66949'
ARCHIVE_BASE_HALLOWEEN_FR_0_URL='https://www.gog.com/game/endzone_a_world_apart_halloween'


CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_MAIN_FILES='
mods'

PKG_PARENT_ID="$GAME_ID"

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_PARENT'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
