#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Book of Hours
# send your bug reports to contact@dotslashplay.it
###

script_version=20241209.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='book-of-hours'
GAME_NAME='Book of Hours'

ARCHIVE_BASE_17_NAME='book_of_hours_2024_12_m_5_78248.sh'
ARCHIVE_BASE_17_MD5='e11bf31073b691d65e64f68b8dc30ee2'
ARCHIVE_BASE_17_SIZE='2495307'
ARCHIVE_BASE_17_VERSION='2024.12.m.5-gog78248'
ARCHIVE_BASE_17_URL='https://www.gog.com/game/book_of_hours'

ARCHIVE_BASE_16_NAME='book_of_hours_2024_11_k_1_77536.sh'
ARCHIVE_BASE_16_MD5='2eb3f24916e026060340266ffdab10a8'
ARCHIVE_BASE_16_SIZE='2495679'
ARCHIVE_BASE_16_VERSION='2024.11.k.1-gog77536'

ARCHIVE_BASE_15_NAME='book_of_hours_2024_10_j_14_77218.sh'
ARCHIVE_BASE_15_MD5='ede5fbd4a0fab2ed561170ef60019664'
ARCHIVE_BASE_15_SIZE='2495637'
ARCHIVE_BASE_15_VERSION='2024.10.j.14-gog77218'

ARCHIVE_BASE_13_NAME='book_of_hours_2024_9_i_8_76549.sh'
ARCHIVE_BASE_13_MD5='868e191034df3652eef1cbe096a2a9ba'
ARCHIVE_BASE_13_SIZE='2465383'
ARCHIVE_BASE_13_VERSION='2024.09.i.8-gog76549'

ARCHIVE_BASE_11_NAME='book_of_hours_2024_7_f_15_74868.sh'
ARCHIVE_BASE_11_MD5='48d1d37e95221d38f45bb5fdfe05a296'
ARCHIVE_BASE_11_SIZE='2486574'
ARCHIVE_BASE_11_VERSION='2024.07.f.15-gog74868'

ARCHIVE_BASE_10_NAME='book_of_hours_2024_3_e_17_72226.sh'
ARCHIVE_BASE_10_MD5='c4185397829ad4d98a7fa25eddd24235'
ARCHIVE_BASE_10_SIZE='2028310'
ARCHIVE_BASE_10_VERSION='2024.03.e.17-gog72226'

ARCHIVE_BASE_8_NAME='book_of_hours_2024_2_e_11_71531.sh'
ARCHIVE_BASE_8_MD5='253b536e020ae39d617cc0746be3d5ee'
ARCHIVE_BASE_8_SIZE='1998185'
ARCHIVE_BASE_8_VERSION='2024.02.e.11-gog71531'

ARCHIVE_BASE_7_NAME='book_of_hours_2023_12_d_12_69519.sh'
ARCHIVE_BASE_7_MD5='f99ac227e448683eb9cb9586e0d43cc8'
ARCHIVE_BASE_7_SIZE='1938644'
ARCHIVE_BASE_7_VERSION='2023.12.d.12-gog69519'

ARCHIVE_BASE_4_NAME='book_of_hours_2023_11_d_4_69228.sh'
ARCHIVE_BASE_4_MD5='f0dfed821caeb83d795a176dd9571281'
ARCHIVE_BASE_4_SIZE='1934820'
ARCHIVE_BASE_4_VERSION='2023.11.d.4-gog69228'

ARCHIVE_BASE_3_NAME='book_of_hours_2023_10_c_11_68174.sh'
ARCHIVE_BASE_3_MD5='958bd653dd8a2453c9920ab60fc96e29'
ARCHIVE_BASE_3_SIZE='1932668'
ARCHIVE_BASE_3_VERSION='2023.10.c.11-gog68174'

ARCHIVE_BASE_2_NAME='book_of_hours_2023_9_b_12_67790.sh'
ARCHIVE_BASE_2_MD5='2875c037ee2ff44b41191471449248a1'
ARCHIVE_BASE_2_SIZE='1930344'
ARCHIVE_BASE_2_VERSION='2023.09.b.12-gog67790'

ARCHIVE_BASE_1_NAME='book_of_hours_2023_8_g_2_67165.sh'
ARCHIVE_BASE_1_MD5='4d6c689f68c4debb4ad68a3dc1c018b5'
ARCHIVE_BASE_1_SIZE='1927812'
ARCHIVE_BASE_1_VERSION='2023.08.g.2-gog67165'

UNITY3D_NAME='bh'

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libz.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Prevent the inclusion of Steam libraries
	rm --force --recursive "$(unity3d_name)_Data/Plugins"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
