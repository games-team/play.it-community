#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Brothers: A Tale of Two Sons
# send your bug reports to contact@dotslashplay.it
###

script_version=20240516.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='brothers-a-tale-of-two-sons'
GAME_NAME='Brothers: A Tale of Two Sons'

ARCHIVE_BASE_0_NAME='setup_brothers_-_a_tale_of_two_sons_gog-2_(6538).exe'
ARCHIVE_BASE_0_MD5='59101e78c4e0687db9e07d45533cdea2'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='1500000'
ARCHIVE_BASE_0_VERSION='1.2-gog6538'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/brothers_a_tale_of_two_sons'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
binaries
engine/config
engine/shaders'
CONTENT_GAME_DATA_FILES='
p13
engine/localization
engine/splash'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/My Games/Brothers - A Tale of Two Sons'
## Install required .NET framework.
## TODO: Check if Mono could be used instead.
WINE_DLLOVERRIDES_DEFAULT='winemenubuilder.exe,mshtml='
WINE_WINETRICKS_VERBS='dotnet40'

APP_MAIN_EXE='binaries/win32/brothers.exe'
## Explicitly setting the application type is required, or it would be misidentified as a Mono game.
APP_MAIN_TYPE='wine'

PACKAGES_LIST='
PKG_DATA
PKG_BIN'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
