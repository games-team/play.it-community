#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Ys 1
# send your bug reports to contact@dotslashplay.it
###

script_version=20230428.3

GAME_ID='ys-1'
GAME_NAME='Ys Ⅰ: Ancient Ys Vanished Omen'

ARCHIVE_BASE_0='setup_ys_1_2.0.0.1.exe'
ARCHIVE_BASE_0_MD5='ff6c2e8ab34ea15226521fb8b6c8c23b'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='700000'
ARCHIVE_BASE_0_VERSION='1.1.0-gog2.0.0.1'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/ys_i_ii_chronicles'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
config.exe
ys1plus.exe'
CONTENT_GAME_DATA_FILES='
release'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Saved Games/FALCOM/ys12c'

APP_MAIN_EXE='ys1plus.exe'

APP_CONFIG_ID="${GAME_ID}-config"
APP_CONFIG_NAME="$GAME_NAME - configuration"
APP_CONFIG_EXE='config.exe'
APP_CONFIG_CAT='Settings'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
video/mpeg, systemstream=(boolean)true, mpegversion=(int)1'

# Load common functions

target_version='2.23'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
