#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Forgotton Anne
# send your bug reports to contact@dotslashplay.it
###

script_version=20240403.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='forgotton-anne'
GAME_NAME='Forgotton Anne'

ARCHIVE_BASE_0_NAME='setup_forgotton_anne_5.5.3_(29552).exe'
ARCHIVE_BASE_0_MD5='2f6b17e78651f6ccc9070705b879a6ae'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_forgotton_anne_5.5.3_(29552)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='8f6e836ff3519e4759af8c51ed89655d'
ARCHIVE_BASE_0_PART2_NAME='setup_forgotton_anne_5.5.3_(29552)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='728970d510b82fc1d9d336c9e26fb8c3'
ARCHIVE_BASE_0_SIZE='9500000'
ARCHIVE_BASE_0_VERSION='5.5.3-gog29552'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/forgotton_anne'

UNITY3D_NAME='forgottonanne'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME0_DATA_PATH='app'
CONTENT_GAME0_DATA_FILES="
${UNITY3D_NAME}_data"

WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Local/GOG.com/Galaxy/Applications/51445449926814491'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
