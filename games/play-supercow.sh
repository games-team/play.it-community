#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2025 tyzef <stefan@tyzef.infini.fr>
# SPDX-FileCopyrightText: © 2025 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Supercow
# send your bug reports to contact@dotslashplay.it
###

script_version=20250226.3

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='supercow'
GAME_NAME='Supercow'

ARCHIVE_BASE_0_NAME='Supercow.exe'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='78363'
ARCHIVE_BASE_0_VERSION='1.0-1'
ARCHIVE_BASE_0_URL='https://www.gametop.com/download-free-games/supercow/'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
wrapgame.exe
*.dll'
CONTENT_GAME_DATA_FILES='
data'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Roaming/Super-Cow'

APP_MAIN_EXE='wrapgame.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
