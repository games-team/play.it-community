#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 HS-157
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Rimworld
# send your bug reports to contact@dotslashplay.it
###

script_version=20231108.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='rimworld'
GAME_NAME='Rimworld'

ARCHIVE_BASE_0_NAME='RimWorld1-1-2654Linux.zip'
ARCHIVE_BASE_0_MD5='4391d550e8da14b7826a63dbd75cbc44'
ARCHIVE_BASE_0_SIZE='350000'
ARCHIVE_BASE_0_VERSION='1.1.2654-1'
ARCHIVE_BASE_0_URL='https://rimworldgame.com/getmygame/'

UNITY3D_NAME='RimWorldLinux'
## TODO: Add an explicit list of Unity3D plugins to include.

CONTENT_PATH_DEFAULT='RimWorld1-1-2654Linux'
CONTENT_GAME0_BIN_FILES="
${UNITY3D_NAME}_Data/Plugins"
CONTENT_GAME0_DATA_FILES='
Data/Core'

USER_PERSISTENT_DIRECTORIES='
Mods'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
## TODO: Add a list of required native libraries.

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

## Errors during the game data extraction from the archive are expected, they should be ignored.
archive_extraction_defayt 2>/dev/null || true

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
