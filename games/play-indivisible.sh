#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Indivisible
# send your bug reports to contact@dotslashplay.it
###

script_version=20230623.1

GAME_ID='indivisible'
GAME_NAME='Indivisible'

ARCHIVE_BASE_0='indivisible_42940_39272.sh'
ARCHIVE_BASE_0_MD5='7328915691beb21dc77494faf915cfbe'
ARCHIVE_BASE_0_SIZE='6300000'
ARCHIVE_BASE_0_VERSION='42940-gog39272'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/indivisible'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN_PATH='data/noarch/game/lib/x86_64-pc-linux-gnu'
CONTENT_LIBS_BIN_FILES='
libavcodec.so.57
libavfilter.so.6
libavformat.so.57
libavutil.so.55
libdiscord-rpc.so
libGalaxy64.so
libSDL2_locale.so.0
libswresample.so.2
libswscale.so.4
libvpx.so.5'
CONTENT_GAME_BIN_FILES='
Indivisible.x86_64-pc-linux-gnu'
CONTENT_GAME_DATA_FILES='
pkgs'
CONTENT_DOC_DATA_FILES='
LICENSES.md'

APP_MAIN_EXE='Indivisible.x86_64-pc-linux-gnu'
APP_MAIN_ICON='../support/icon.png'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
ld-linux-x86-64.so.2
libc.so.6
libdl.so.2
libFAudio.so.0
libgcc_s.so.1
libGL.so.1
libm.so.6
libogg.so.0
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libSDL_kitchensink.so.1
libstdc++.so.6
libvorbisenc.so.2
libvorbisfile.so.3
libvorbis.so.0'

# Use shipped build of libSDL_kitchensink.so.1,
# to avoid a symbol lookup error:
# undefined symbol: Kit_GetPlayerVideoDataOGL

PKG_BIN_DEPENDENCIES_LIBRARIES=$(
	printf '%s' "$PKG_BIN_DEPENDENCIES_LIBRARIES" | \
		grep --invert-match --fixed-strings --line-regexp 'libSDL_kitchensink.so.1'
)
CONTENT_LIBS_BIN_FILES="$CONTENT_LIBS_BIN_FILES
libSDL_kitchensink.so.1"

# Load common functions

target_version='2.23'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build package

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
