#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2017 Mopi
set -o errexit

###
# SteamWorld games:
# - SteamWorld Dig 1
# - SteamWorld Dig 2
# - SteamWorld Heist
# - SteamWorld Quest
# send your bug reports to contact@dotslashplay.it
###

script_version=20241124.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID_DIG1='steamworld-dig-1'
GAME_NAME_DIG1='SteamWorld Dig: A Fistful of Dirt'

GAME_ID_DIG2='steamworld-dig-2'
GAME_NAME_DIG2='SteamWorld Dig 2'

GAME_ID_HEIST='steamworld-heist'
GAME_NAME_HEIST='SteamWorld Heist'

GAME_ID_QUEST='steamworld-quest'
GAME_NAME_QUEST='SteamWorld Quest: Hand of Gilgamech'

# Archives

## SteamWorld Dig 1

ARCHIVE_BASE_DIG1_GOG_0_NAME='gog_steamworld_dig_2.0.0.7.sh'
ARCHIVE_BASE_DIG1_GOG_0_MD5='2f2ed68e00f151ff3c4d0092d8d6b15b'
ARCHIVE_BASE_DIG1_GOG_0_SIZE='79000'
ARCHIVE_BASE_DIG1_GOG_0_VERSION='1.10-gog2.0.0.7'
ARCHIVE_BASE_DIG1_GOG_0_URL='https://www.gog.com/game/steamworld_dig'

ARCHIVE_BASE_DIG1_HUMBLE_0_NAME='SteamWorldDig_linux_1393468453.tar.gz'
ARCHIVE_BASE_DIG1_HUMBLE_0_MD5='de6ff6273c4e397413d852472d51e788'
ARCHIVE_BASE_DIG1_HUMBLE_0_SIZE='77000'
ARCHIVE_BASE_DIG1_HUMBLE_0_VERSION='1.10-humble140220'
ARCHIVE_BASE_DIG1_HUMBLE_0_URL='https://www.humblebundle.com/store/steamworld-dig'

## SteamWorld Dig 2

ARCHIVE_BASE_DIG2_0_NAME='steamworld_dig_2_en_1_1_15062.sh'
ARCHIVE_BASE_DIG2_0_MD5='baafa458aaef7fc9c80e076d48e754f0'
ARCHIVE_BASE_DIG2_0_SIZE='220000'
ARCHIVE_BASE_DIG2_0_VERSION='1.1-gog15062'
ARCHIVE_BASE_DIG2_0_URL='https://www.gog.com/game/steamworld_dig'

## SteamWorld Heist

ARCHIVE_BASE_HEIST_0_NAME='SteamWorldHeist.tar.gz'
ARCHIVE_BASE_HEIST_0_MD5='79a499459c09d7881efeb95be7abc559'
ARCHIVE_BASE_HEIST_0_VERSION='1.0-humble170131'
ARCHIVE_BASE_HEIST_0_SIZE='200000'
ARCHIVE_BASE_HEIST_0_URL='https://www.humblebundle.com/store/steamworld-heist'

## SteamWorld Quest

ARCHIVE_BASE_QUEST_0_NAME='steamworld_quest_hand_of_gilgamech_2_1_31741.sh'
ARCHIVE_BASE_QUEST_0_MD5='cffa9b745ef4ca37123b5928c37effe0'
ARCHIVE_BASE_QUEST_0_SIZE='1200000'
ARCHIVE_BASE_QUEST_0_VERSION='2.1-gog31741'
ARCHIVE_BASE_QUEST_0_URL='https://www.gog.com/game/steamworld_quest_hand_of_gilgamech'

# Archives content

CONTENT_GAME_DATA_FILES='
icon.bmp
icon.png
Bundle
BundlePC'
CONTENT_DOC_DATA_FILES='
Licenses
readme.txt'

## SteamWorld Dig 1

CONTENT_PATH_DEFAULT_DIG1_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_DIG1_HUMBLE='SteamWorldDig'
CONTENT_GAME_BIN_FILES_DIG1='
SteamWorldDig'

## SteamWorld Dig 2

CONTENT_PATH_DEFAULT_DIG2='data/noarch/game'
CONTENT_GAME_BIN_FILES_DIG2='
Dig2'

## SteamWorld Dig 2

CONTENT_PATH_DEFAULT_HEIST='SteamWorldHeist'
CONTENT_GAME_BIN_FILES_HEIST='
Heist'

## SteamWorld Dig 2

CONTENT_PATH_DEFAULT_QUEST='data/noarch/game'
CONTENT_GAME_BIN_FILES_QUEST='
Quest'


APP_MAIN_ICON='icon.png'
APP_MAIN_EXE_DIG1='SteamWorldDig'
APP_MAIN_EXE_DIG2='Dig2'
APP_MAIN_EXE_HEIST='Heist'
APP_MAIN_EXE_QUEST='Quest'
## The game crashes on launch when SDL_VIDEODRIVER is set to "wayland"
## TODO: Check if all supported games are affected
APP_MAIN_PRERUN='
# The game crashes on launch when SDL_VIDEODRIVER is set to "wayland"
if [ "${SDL_VIDEODRIVER:-}" = "wayland" ]; then
	unset SDL_VIDEODRIVER
fi
'

# Packages

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_DESCRIPTION='data'

## SteamWorld Dig 1

PKG_BIN_ARCH_DIG1='32'
PKG_BIN_DEPENDENCIES_LIBRARIES_DIG1='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

PKG_DATA_ID_DIG1="${GAME_ID_DIG1}-data"

## SteamWorld Dig 2

PKG_BIN_ARCH_DIG2='64'
PKG_BIN_DEPENDENCIES_LIBRARIES_DIG2='
libc.so.6
libgcc_s.so.1
libGL.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
libSDL2-2.0.so.0
libstdc++.so.6'

PKG_DATA_ID_DIG2="${GAME_ID_DIG2}-data"

## SteamWorld Heist

PKG_BIN_ARCH_HEIST='32'
PKG_BIN_DEPENDENCIES_LIBRARIES_HEIST='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
libstdc++.so.6'

PKG_DATA_ID_HEIST="${GAME_ID_HEIST}-data"

## SteamWorld Quest

PKG_BIN_ARCH_QUEST='64'
PKG_BIN_DEPENDENCIES_LIBRARIES_QUEST='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
libSDL2-2.0.so.0
libstdc++.so.6'

PKG_DATA_ID_QUEST="${GAME_ID_QUEST}-data"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
