#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Jacek Szafarkiewicz
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Thief 3
# send your bug reports to contact@dotslashplay.it
###

script_version=20240428.2

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='thief-3'
GAME_NAME='Thief 3: Deadly Shadows'

ARCHIVE_BASE_1_NAME='setup_thief_-_deadly_shadows_1.1_(21683).exe'
ARCHIVE_BASE_1_MD5='153723f2908242cf27a23ad58d0608b5'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_thief_-_deadly_shadows_1.1_(21683)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='8c549826345776df192fb1721740096f'
ARCHIVE_BASE_1_SIZE='2300000'
ARCHIVE_BASE_1_VERSION='1.1-gog2.21683'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/thief_3'

ARCHIVE_BASE_0_NAME='setup_thief3_2.0.0.6.exe'
ARCHIVE_BASE_0_MD5='e5b84de58a1037f3e8aa3a1bb2a982be'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='2300000'
ARCHIVE_BASE_0_VERSION='1.1-gog2.0.0.6'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_BIN_FILES='
system'
CONTENT_GAME0_BIN_PATH='__support/app'
CONTENT_GAME0_BIN_PATH_0='app'
CONTENT_GAME0_BIN_FILES='
user.ini'
CONTENT_GAME_DATA_FILES='
content'
CONTENT_DOC_DATA_FILES='
eula.txt
readme.rtf
*.pdf'

USER_PERSISTENT_DIRECTORIES='
saves'
USER_PERSISTENT_FILES='
*.ini'

APP_MAIN_EXE='system/t3.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
## Ensure easy upgrades from packages generated with pre-20201031.16 game scripts.
PKG_DATA_PROVIDES="${PKG_DATA_PROVIDES:-}
thief3-data"

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID wine"
## Ensure easy upgrades from packages generated with pre-20201031.16 game scripts.
PKG_BIN_PROVIDES="${PKG_BIN_PROVIDES:-}
thief3"

# Set up required registry keys

registry_dump_required_file='registry-dumps/required-entries.reg'
# shellcheck disable=SC1003
registry_dump_required_content='Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\Software\Ion Storm\Thief - Deadly Shadows]
"ION_ROOT"="C:\\'"${GAME_ID}"'"
"SaveGamePath"="C:\\'"${GAME_ID}"'\\saves"

[HKEY_LOCAL_MACHINE\Software\Ion Storm\Thief - Deadly Shadows\SecuROM\Locale]
"ADMIN_RIGHTS"="Application requires Windows administrator rights."
"ANALYSIS_DISCLAIMER"="Dear Software User,\\n\\nThis test program has been developed with your personal interest in mind to check for possible hardware and/or software incompatibility on your PC. To shorten the analysis time, system information is collected (similar to the Microsoft'\''s msinfo32.exe program).\\n\\nData will be compared with our knowledge base to discover hardware/software conflicts. Submitting the log file is totally voluntary. The collected data is for evaluation purposes only and is not used in any other manner.\\n\\nYour Support Team\\n\\nDo you want to start?"
"ANALYSIS_DONE"="The Information was successfully collected and stored to the following file:\\n\\n\\\"%FILE%\\\"\\n\\nPlease contact Customer Support for forwarding instructions."
"AUTH_TIMEOUT"="Unable to authenticate original disc within time limit."
"EMULATION_DETECTED"="Conflict with Disc Emulator Software detected."
"NO_DISC"="No disc inserted."
"NO_DRIVE"="No CD or DVD drive found."
"NO_ORIG_FOUND"="Please insert the original disc instead of a backup."
"TITLEBAR"="Thief: Deadly Shadows"
"WRONG_DISC"="Wrong Disc inserted.  Please insert the Thief: Deadly Shadows disc into your CD/DVD drive."'
CONTENT_GAME_BIN_FILES="${CONTENT_GAME_BIN_FILES:-}
$registry_dump_required_file"
APP_REGEDIT="${APP_REGEDIT:-} $registry_dump_required_file"
SCRIPT_DEPS="${SCRIPT_DEPS:-} iconv"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Set up required registry keys.
	mkdir --parents "$(dirname "$registry_dump_required_file")"
	printf '%s' "$registry_dump_required_content" | \
		iconv --from-code=UTF-8 --to-code=UTF-16 --output="$registry_dump_required_file"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
