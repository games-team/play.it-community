#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2024 Fabien Givors <captnfab@debian-facile.org>
set -o errexit

###
# The Elder Scrolls 5: Skyrim
# send your bug reports to contact@dotslashplay.it
###

script_version=20241124.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='the-elder-scrolls-5-skyrim'
GAME_NAME='The Elder Scrolls V: Skyrim'

ARCHIVE_BASE_1_NAME='setup_the_elder_scrolls_v_skyrim_special_edition_0.1.3905696_(64bit)_(70738).exe'
ARCHIVE_BASE_1_MD5='5a73debc2115209be1abd257aa0be329'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_the_elder_scrolls_v_skyrim_special_edition_0.1.3905696_(64bit)_(70738)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='c8d28d38dc2a58102b4553ba61026591'
ARCHIVE_BASE_1_PART2_NAME='setup_the_elder_scrolls_v_skyrim_special_edition_0.1.3905696_(64bit)_(70738)-2.bin'
ARCHIVE_BASE_1_PART2_MD5='b262b033d7e30b1d7289e36e0901193c'
ARCHIVE_BASE_1_PART3_NAME='setup_the_elder_scrolls_v_skyrim_special_edition_0.1.3905696_(64bit)_(70738)-3.bin'
ARCHIVE_BASE_1_PART3_MD5='e249eaf58788328b47b4ba452cee0339'
ARCHIVE_BASE_1_PART4_NAME='setup_the_elder_scrolls_v_skyrim_special_edition_0.1.3905696_(64bit)_(70738)-4.bin'
ARCHIVE_BASE_1_PART4_MD5='3f82c21b9db4bc45adf65389125dd643'
ARCHIVE_BASE_1_PART5_NAME='setup_the_elder_scrolls_v_skyrim_special_edition_0.1.3905696_(64bit)_(70738)-5.bin'
ARCHIVE_BASE_1_PART5_MD5='20a789f518bfc5f5654315c6770ebeea'
ARCHIVE_BASE_1_PART6_NAME='setup_the_elder_scrolls_v_skyrim_special_edition_0.1.3905696_(64bit)_(70738)-6.bin'
ARCHIVE_BASE_1_PART6_MD5='1fc7001e970abf73f0e2cecfbddd6b9c'
ARCHIVE_BASE_1_PART7_NAME='setup_the_elder_scrolls_v_skyrim_special_edition_0.1.3905696_(64bit)_(70738)-7.bin'
ARCHIVE_BASE_1_PART7_MD5='11c895a76cb12719e6252757b301089b'
ARCHIVE_BASE_1_SIZE='29202245'
ARCHIVE_BASE_1_VERSION='0.1.3905696-gog70738'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/the_elder_scrolls_v_skyrim_special_edition'

ARCHIVE_BASE_0_NAME='setup_the_elder_scrolls_v_skyrim_special_edition_1.6.659.0.8_(64bit)_(59094).exe'
ARCHIVE_BASE_0_MD5='0b6d1c931788f66d4a3b02138733f05a'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_the_elder_scrolls_v_skyrim_special_edition_1.6.659.0.8_(64bit)_(59094)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='163b8af958071f85e78613834375209c'
ARCHIVE_BASE_0_PART2_NAME='setup_the_elder_scrolls_v_skyrim_special_edition_1.6.659.0.8_(64bit)_(59094)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='c3852b6ae2bcc46fa0e4f085095bd12f'
ARCHIVE_BASE_0_PART3_NAME='setup_the_elder_scrolls_v_skyrim_special_edition_1.6.659.0.8_(64bit)_(59094)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='a44293c54ebf39efd8f32cc4cc154220'
ARCHIVE_BASE_0_PART4_NAME='setup_the_elder_scrolls_v_skyrim_special_edition_1.6.659.0.8_(64bit)_(59094)-4.bin'
ARCHIVE_BASE_0_PART4_MD5='0dc37a5bd97295b5f163435c75868d03'
ARCHIVE_BASE_0_PART5_NAME='setup_the_elder_scrolls_v_skyrim_special_edition_1.6.659.0.8_(64bit)_(59094)-5.bin'
ARCHIVE_BASE_0_PART5_MD5='162f6baf1d24594f67a1710edef30c42'
ARCHIVE_BASE_0_PART6_NAME='setup_the_elder_scrolls_v_skyrim_special_edition_1.6.659.0.8_(64bit)_(59094)-6.bin'
ARCHIVE_BASE_0_PART6_MD5='5274ded2391553e9595b170af66ae0c3'
ARCHIVE_BASE_0_SIZE='29000000'
ARCHIVE_BASE_0_VERSION='1.6.659.0.8-gog59094'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/the_elder_scrolls_v_skyrim_special_edition'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
skyrimselauncher.exe
skyrimse.exe
bink2w64.dll
galaxy64.dll
high.ini
low.ini
medium.ini
ultra.ini
skyrimconsoledefault.ini'
CONTENT_GAME_L10N_EN_FILES='
skyrim_default_en.ini
data/skyrim - voices_en0.bsa'
CONTENT_GAME_L10N_FR_FILES='
skyrim_default_fr.ini
data/skyrim - voices_fr0.bsa'
CONTENT_GAME_DATA_TEXTURES_FILES='
data/skyrim - textures?.bsa'
CONTENT_GAME_DATA_FILES='
data/ccbgssse001-fish.bsa
data/ccbgssse001-fish.esm
data/ccbgssse025-advdsgs.bsa
data/ccbgssse025-advdsgs.esm
data/ccbgssse037-curios.bsa
data/ccbgssse037-curios.esl
data/ccqdrsse001-survivalmode.bsa
data/ccqdrsse001-survivalmode.esl
data/dawnguard.esm
data/dragonborn.esm
data/hearthfires.esm
data/marketplacetextures.bsa
data/_resourcepack.bsa
data/_resourcepack.esl
data/skyrim.esm
data/skyrim - animations.bsa
data/skyrim - interface.bsa
data/skyrim - meshes0.bsa
data/skyrim - meshes1.bsa
data/skyrim - misc.bsa
data/skyrim - shaders.bsa
data/skyrim - sounds.bsa
data/update.esm
data/video
skyrim.ccc'

WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/My Games/Skyrim Special Edition GOG'

APP_MAIN_EXE='skyrimse.exe'
APP_MAIN_ICON='skyrimselauncher.exe'

PACKAGES_LIST='
PKG_BIN
PKG_L10N_EN
PKG_L10N_FR
PKG_DATA_TEXTURES
PKG_DATA'

PKG_L10N_ID="${GAME_ID}-l10n"
PKG_L10N_EN_ID="${PKG_L10N_ID}-en"
PKG_L10N_FR_ID="${PKG_L10N_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_ID"
PKG_L10N_EN_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_FR_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_EN_DESCRIPTION='English localization'
PKG_L10N_FR_DESCRIPTION='French localization'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_DATA_TEXTURES_ID="${PKG_DATA_ID}-textures"
PKG_DATA_TEXTURES_DESCRIPTION="$PKG_DATA_DESCRIPTION - textures"
PKG_DATA_DEPS="${PKG_DATA_DEPS:-} $PKG_DATA_TEXTURES_ID"

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_L10N_ID $PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
audio/x-wma, wmaversion=(int)1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default
## Rename language-specific configuration file
mv \
	"$(package_path 'PKG_L10N_EN')$(path_game_data)"/skyrim_default_??.ini \
	"$(package_path 'PKG_L10N_EN')$(path_game_data)"/skyrim_default.ini
mv \
	"$(package_path 'PKG_L10N_FR')$(path_game_data)"/skyrim_default_??.ini \
	"$(package_path 'PKG_L10N_FR')$(path_game_data)"/skyrim_default.ini

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
case "$(messages_language)" in
	('fr')
		lang_string='version %s :'
		lang_en='anglaise'
		lang_fr='française'
	;;
	('en'|*)
		lang_string='%s version:'
		lang_en='English'
		lang_fr='French'
	;;
esac
printf '\n'
printf "$lang_string" "$lang_en"
print_instructions 'PKG_BIN' 'PKG_L10N_EN' 'PKG_DATA' 'PKG_DATA_TEXTURES'
printf "$lang_string" "$lang_fr"
print_instructions 'PKG_BIN' 'PKG_L10N_FR' 'PKG_DATA' 'PKG_DATA_TEXTURES'

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
