#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 bbob
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Symmetry
# send your bug reports to contact@dotslashplay.it
###

script_version=20240811.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='symmetry'
GAME_NAME='Symmetry'

ARCHIVE_BASE_0_NAME='setup_symmetry_1.0.2.win_(21945).exe'
ARCHIVE_BASE_0_MD5='2a0d1919d41b48b8dc5af3071ad7babc'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='210000'
ARCHIVE_BASE_0_VERSION='1.0.2-gog21945'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/symmetry'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
*.dll
symmetry.exe
options.ini'
CONTENT_GAME_DATA_FILES='
fonts
texts
*.chroma
*.ogg
*.png'

USER_PERSISTENT_FILES='
*.ini'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Local/Symmetry'
## TODO: Check if this winetricks verb is still required with current WINE builds.
WINE_WINETRICKS_VERBS='d3dcompiler_43'

APP_MAIN_EXE='symmetry.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# An inner archive uses the cabinet archive format

SCRIPT_DEPS="${SCRIPT_DEPS:-} cabextract"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
ARCHIVE_INNER_CAB_PATH="${PLAYIT_WORKDIR}/gamedata/symmetry.cab"
ARCHIVE_INNER_CAB_TYPE='cabinet'
mv "${PLAYIT_WORKDIR}/gamedata/symmetry.exe" "$(archive_path 'ARCHIVE_INNER_CAB')"
archive_extraction 'ARCHIVE_INNER_CAB'
rm "$(archive_path 'ARCHIVE_INNER_CAB')"
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Prevent inclusion of unwanted files.
	rm --recursive \
		'__redist' \
		'commonappdata' \
		'tmp'

	## Fix file paths.
	find . -type f | while read -r file_source; do
		file_destination=$(printf '%s' "$file_source" | sed 's#@#/#g')
		if [ "$file_destination" != "$file_source" ]; then
			mkdir --parents "$(dirname "$file_destination")"
			mv "$file_source" "$file_destination"
		fi
	done
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
