#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Chronology
# send your bug reports to contact@dotslashplay.it
###

script_version=20240122.4

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='chronology'
GAME_NAME='Chronology'

ARCHIVE_BASE_0_NAME='Chronology Windows 1.0.20.zip'
ARCHIVE_BASE_0_MD5='08236874d0553d9953c56ee3472468f2'
ARCHIVE_BASE_0_SIZE='240000'
ARCHIVE_BASE_0_VERSION='1.0.20-itch1'
ARCHIVE_BASE_0_URL='https://bedtimedigitalgames.itch.io/chronology'

CONTENT_PATH_DEFAULT='Chronology Windows 1.0.20'
CONTENT_GAME_BIN_FILES='
Chronology.exe
*.dll'
CONTENT_GAME_DATA_FILES='
Content
Screengroup.dat'
CONTENT_DOC_DATA_FILES='
BuildDate.txt
License.txt'

## The game engine expects write access to files under Content.
USER_PERSISTENT_DIRECTORIES='
Content'

WINE_DLLOVERRIDES_DEFAULT='winemenubuilder.exe,mshtml='
## The game fails to start if Mono is used instead of .NET.
WINE_WINETRICKS_VERBS="${WINE_WINETRICKS_VERBS:-} dotnet40"
## The game crashes when trying to play if the PE32 OpenAL library is missing.
WINE_WINETRICKS_VERBS="${WINE_WINETRICKS_VERBS:-} openal"

APP_MAIN_EXE='Chronology.exe'
## The application type must be set explicitly,
## or it would be wrongly guessed as a Mono one.
APP_MAIN_TYPE='wine'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
