#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Strike Suit Zero
# send your bug reports to contact@dotslashplay.it
###

script_version=20231009.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='strike-suit-zero'
GAME_NAME='Strike Suit Zero'

ARCHIVE_BASE_0_NAME='StrikeSuitZero_linux_1389211698.zip'
ARCHIVE_BASE_0_MD5='94b1c2907ae61deb27eb77fee3fb9c19'
ARCHIVE_BASE_0_SIZE='2400000'
ARCHIVE_BASE_0_VERSION='1.0-humble1'
ARCHIVE_BASE_0_URL='https://www.humblebundle.com/store/strike-suit-zero'

CONTENT_PATH_DEFAULT='.'
CONTENT_LIBS_BIN_PATH="${CONTENT_PATH_DEFAULT}/linux/main/binary"
## TODO: Check if the Steam library could be dropped.
CONTENT_LIBS_BIN_FILES='
libfmodevent.so
libfmodeventnet.so
libfmodex.so
libfmodex-4.44.12.so
libsteam_api.so'
CONTENT_GAME_BIN_FILES='
linux/main/binary/StrikeSuitZero'
CONTENT_GAME_DATA_FILES='
linux/main/art
linux/main/audio
linux/main/fonts
linux/main/gui
linux/main/level
linux/main/levels
linux/main/localisation
linux/main/particles
linux/main/scripts
linux/main/shaders
linux/main/system
linux/main/textures
linux/main/video
linux/main/index.toc
linux/main/index.toc.txt'

USER_PERSISTENT_FILES='
*.sav'

APP_MAIN_EXE='linux/main/binary/StrikeSuitZero'
## Run the game binary from its parent directory.
APP_MAIN_PRERUN='# Run the game binary from its parent directory
cd "$(dirname "$APP_EXE")"
APP_EXE=$(basename "$APP_EXE")'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
libSDL2-2.0.so.0
libstdc++.so.6'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
ARCHIVE_OPTIONAL_ICONS_0_NAME='strike-suit-zero_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_0_MD5='3fe8bbad7ecca5c0e3afdbbfedb8945d'
ARCHIVE_OPTIONAL_ICONS_0_URL='https://downloads.dotslashplay.it/games/strike-suit-zero'
archive_initialize_optional \
	'ARCHIVE_ICONS' \
	'ARCHIVE_OPTIONAL_ICONS'
## TODO: The library should provide a function to check if a given optional archive has been provided.
if [ -n "$(archive_path 'ARCHIVE_ICONS' 2>/dev/null || true)" ]; then
	archive_extraction 'ARCHIVE_ICONS'
fi

# Include game data

## TODO: The library should provide a function to check if a given optional archive has been provided.
if [ -n "$(archive_path 'ARCHIVE_ICONS' 2>/dev/null || true)" ]; then
	CONTENT_ICONS_DATA_PATH='.'
	CONTENT_ICONS_DATA_FILES='
	16x16
	32x32
	48x48'
	content_inclusion 'ICONS_DATA' 'PKG_DATA' "$(path_icons)"
fi
content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
