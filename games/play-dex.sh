#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2020 Hoël Bézier
set -o errexit

###
# Dex
# send your bug reports to contact@dotslashplay.it
###

script_version=20231016.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='dex-game'
GAME_NAME='Dex'

ARCHIVE_BASE_1_NAME='dex_en_6_0_0_0_build_5553_17130.sh'
ARCHIVE_BASE_1_MD5='3d6f8797fab72dcb867c92bf5a84b4dd'
ARCHIVE_BASE_1_SIZE='6300000'
ARCHIVE_BASE_1_VERSION='6.0.0.0.5553-gog17130'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/dex'

ARCHIVE_BASE_0_NAME='gog_dex_2.3.0.4.sh'
ARCHIVE_BASE_0_MD5='199a1acc59879124e8e1c532909fd879'
ARCHIVE_BASE_0_SIZE='6200000'
ARCHIVE_BASE_0_VERSION='5.4.0.0-gog2.3.0.4'

UNITY3D_NAME='Dex'
UNITY3D_PLUGINS='
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_DOC_DATA_PATH='data/noarch/docs'
CONTENT_DOC_DATA_FILES='
Controller_readme.pdf'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXrandr.so.2'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
