#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Daguhh
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Shovel Knight
# send your bug reports to contact@dotslashplay.it
###

script_version=20240619.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='shovel-knight'
GAME_NAME='Shovel Knight: Treasure Trove'

ARCHIVE_BASE_0_NAME='shovel_knight_treasure_trove_4_1b_arby_s_46298.sh'
ARCHIVE_BASE_0_MD5='2f5e07e20ac615eb248ab81c25974d53'
ARCHIVE_BASE_0_SIZE='500000'
ARCHIVE_BASE_0_VERSION='4.1b-gog46298'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/shovel_knight'

CONTENT_PATH_DEFAULT='data/noarch/game'
## TODO: The libraries should be installed in the system libraries path.
CONTENT_GAME_BIN64_PATH="${CONTENT_PATH_DEFAULT}/64"
CONTENT_GAME_BIN64_FILES='
ShovelKnight
lib'
CONTENT_GAME_BIN32_PATH="${CONTENT_PATH_DEFAULT}/32"
CONTENT_GAME_BIN32_FILES='
ShovelKnight
lib'
CONTENT_GAME_DATA_FILES='
data'
CONTENT_DOC_DATA_PATH='data/noarch/docs'
CONTENT_DOC_DATA_FILES='*.txt'

APP_MAIN_EXE='ShovelKnight'
APP_MAIN_ICON='../support/icon.png'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
## TODO: The list of required native libraries might not be complete.
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libGL.so.1
libSDL2-2.0.so.0
libstdc++.so.6'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
