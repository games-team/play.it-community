#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Aquamarine
# send your bug reports to contact@dotslashplay.it
###

script_version=20241111.2

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='aquamarine'
GAME_NAME='Aquamarine'

ARCHIVE_BASE_7_NAME='aquamarine_2_1_1_71763.sh'
ARCHIVE_BASE_7_MD5='c875b669a6fafab88361e34cf9e28ec9'
ARCHIVE_BASE_7_SIZE='1152842'
ARCHIVE_BASE_7_VERSION='2.1.1-gog71763'
ARCHIVE_BASE_7_URL='https://www.gog.com/game/aquamarine'

ARCHIVE_BASE_6_NAME='aquamarine_1_0_8_5_54515.sh'
ARCHIVE_BASE_6_MD5='7978066a0495d061245c4ae23c968874'
ARCHIVE_BASE_6_SIZE='726340'
ARCHIVE_BASE_6_VERSION='1.0.8.5-gog54515'

ARCHIVE_BASE_5_NAME='aquamarine_1_0_8_4_54429.sh'
ARCHIVE_BASE_5_MD5='bdec12ffb5efb87a4cf6187ae204c9a2'
ARCHIVE_BASE_5_SIZE='730000'
ARCHIVE_BASE_5_VERSION='1.0.8.4-gog54429'

ARCHIVE_BASE_4_NAME='aquamarine_1_0_8_3_54388.sh'
ARCHIVE_BASE_4_MD5='58a324f7ee185f7ee2d73724d1aa8806'
ARCHIVE_BASE_4_SIZE='730000'
ARCHIVE_BASE_4_VERSION='1.0.8.3-gog54388'

ARCHIVE_BASE_3_NAME='aquamarine_1_0_7_4_54070.sh'
ARCHIVE_BASE_3_MD5='00a23ecfa6d19d1c7d27b4baf06ebfe1'
ARCHIVE_BASE_3_SIZE='650000'
ARCHIVE_BASE_3_VERSION='1.0.7.4-gog54070'

ARCHIVE_BASE_2_NAME='aquamarine_1_0_6_53400.sh'
ARCHIVE_BASE_2_MD5='2182230847d3d09a813ae7bdbc7b3b25'
ARCHIVE_BASE_2_SIZE='810000'
ARCHIVE_BASE_2_VERSION='1.0.6-gog53400'

ARCHIVE_BASE_1_NAME='aquamarine_1_0_2_53166.sh'
ARCHIVE_BASE_1_MD5='f9698d39f38b31ed45fbacb807522c1e'
ARCHIVE_BASE_1_SIZE='810000'
ARCHIVE_BASE_1_VERSION='1.0.2-gog53166'

ARCHIVE_BASE_0_NAME='aquamarine_1_0_0_53074.sh'
ARCHIVE_BASE_0_MD5='c4ca19389d9fe833a009f457684671d1'
ARCHIVE_BASE_0_SIZE='810000'
ARCHIVE_BASE_0_VERSION='1.0.0-gog53074'

UNITY3D_NAME='Aquamarine'
UNITY3D_PLUGINS='
libfmodstudio.so
libresonanceaudio.so'
## TODO: Check if the Steam library is required
UNITY3D_PLUGINS="$UNITY3D_PLUGINS
libsteam_api.so"

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
