#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Trüberbrook
# send your bug reports to contact@dotslashplay.it
###

script_version=20231115.3

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='truberbrook'
GAME_NAME='Trüberbrook'

ARCHIVE_BASE_0_NAME='tr_berbrook_1_6_31066.sh'
ARCHIVE_BASE_0_MD5='d80939b1304181c5f755a768289d9189'
ARCHIVE_BASE_0_SIZE='5500000'
ARCHIVE_BASE_0_VERSION='1.6-gog31066'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/truberbrook'

UNITY3D_NAME='Truberbrook'
UNITY3D_PLUGINS='
libfmodL.so
libfmod.so
libfmodstudioL.so
libfmodstudio.so
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'

## The game crashes on launch when using the wayland backend of SDL,
## even when using the system-provided build of SDL.
APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}"'
# The game crashes on launch when using the wayland backend of SDL
if [ "${SDL_VIDEODRIVER:-}" = "wayland" ]; then
	unset SDL_VIDEODRIVER
fi
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
