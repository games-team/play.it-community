#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Project Feline
# send your bug reports to contact@dotslashplay.it
###

script_version=20240504.2

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='project-feline'
GAME_NAME='Project Feline'

## This game archive used to be available from itch.io,
## but the developer decided to drop Linux support.
ARCHIVE_BASE_0_NAME='feline-linux.zip'
ARCHIVE_BASE_0_MD5='5d59dd8b486c250f7ee918d31fee3c69'
ARCHIVE_BASE_0_SIZE='870000'
ARCHIVE_BASE_0_VERSION='0.10.0-itch.2020.12.13'

## Optional icons pack
ARCHIVE_OPTIONAL_ICONS_NAME='project-feline_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_MD5='a92f62d8bc80aa93c7d69ca1021f677a'
ARCHIVE_OPTIONAL_ICONS_URL='https://downloads.dotslashplay.it/games/project-feline/'
CONTENT_ICONS_PATH='.'
CONTENT_ICONS_FILES='
256x256'

UNREALENGINE4_NAME='Feline'

CONTENT_PATH_DEFAULT='Feline_0.10.0_Linux'
CONTENT_LIBS_BIN_PATH='Feline_0.10.0_Linux/Engine/Binaries/ThirdParty/PhysX3/Linux/x86_64-unknown-linux-gnu'
CONTENT_LIBS_BIN_FILES='
libAPEX_ClothingPROFILE.so
libApexCommonPROFILE.so
libApexFrameworkPROFILE.so
libAPEX_LegacyPROFILE.so
libApexSharedPROFILE.so
libNvParameterizedPROFILE.so
libRenderDebugPROFILE.so'

APP_MAIN_EXE='Feline/Binaries/Linux/Feline'

USER_PERSISTENT_DIRECTORIES='
Feline/Saved/Config'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
