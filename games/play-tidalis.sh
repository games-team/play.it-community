#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Tidalis
# send your bug reports to contact@dotslashplay.it
###

script_version=20240331.5

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='tidalis'
GAME_NAME='Tidalis'

ARCHIVE_BASE_64BIT_0_NAME='Tidalis_Linux64_v1.5.zip'
ARCHIVE_BASE_64BIT_0_MD5='d5893c3ed40ab2266359c88b361ddb57'
ARCHIVE_BASE_64BIT_0_SIZE='630000'
ARCHIVE_BASE_64BIT_0_VERSION='1.5-humble160517'
ARCHIVE_BASE_64BIT_0_URL='https://www.humblebundle.com/store/tidalis'

ARCHIVE_BASE_32BIT_0_NAME='Tidalis_Linux32_v1.5.zip'
ARCHIVE_BASE_32BIT_0_MD5='c5fd83dd7e6221a5a91e326fc36c9043'
ARCHIVE_BASE_32BIT_0_SIZE='630000'
ARCHIVE_BASE_32BIT_0_VERSION='1.5-humble160517'
ARCHIVE_BASE_32BIT_0_URL='https://www.humblebundle.com/store/tidalis'

UNITY3D_NAME='TidalisLinux'
## TODO: Check if the Steam libraries can be dropped.
UNITY3D_PLUGINS='
libCSteamworks.so
libsteam_api.so'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME0_DATA_FILES='
RuntimeData'
## The sysrequirements.txt file is actually what unlocks access to the full game.
CONTENT_GAME1_DATA_FILES='
sysrequirements.txt'
CONTENT_DOC_FILES='
TidalisManual.pdf
TidalisLicense.txt'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH_64BIT='64'
PKG_BIN_ARCH_32BIT='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libGLU.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXext.so.6'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Do not disable the MAP_32BIT flag, as it would cause a crash on launch

unity3d_disable_map32bit() { return 0 ; }

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Delete Apple archives junk.
	rm --recursive '__MACOSX'
	find . -name '*.rsrc' -delete

	## Prevent the inclusion of the game updater.
	rm --recursive 'UDA'

	## Prevent inclusion of files for the wrong architecture.
	case "$(current_archive)" in
		('ARCHIVE_BASE_64BIT_'*)
			rm --recursive \
				"$(unity3d_name)_Data/Mono/x86" \
				"$(unity3d_name)_Data/Plugins/x86"
		;;
		('ARCHIVE_BASE_32BIT_'*)
			rm --recursive \
				"$(unity3d_name)_Data/Mono/x86_64" \
				"$(unity3d_name)_Data/Plugins/x86_64"
		;;
	esac
)

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
