#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Hitman 1
# send your bug reports to contact@dotslashplay.it
###

script_version=20240522.2

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='hitman'
GAME_NAME='Hitman: Codename 47'

ARCHIVE_BASE_0_NAME='setup_hitman_codename_47_b192_(17919).exe'
ARCHIVE_BASE_0_MD5='018d8191bfa45c16995537e3a93f96bd'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='340000'
ARCHIVE_BASE_0_VERSION='1.0b192-gog17919'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/hitman'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
*.dlc
*.dll
*.exe
*.ini'
CONTENT_GAME_DATA_FILES='
repeat.*
intro.zip
optionsscreen.zip
alllevels
c0_*
c1_*
c2_*
c3_*
c4_*
c5_*
cutscenes
music
setup
sounds'
CONTENT_DOC_DATA_FILES='
*.pdf
*.txt'

USER_PERSISTENT_FILES='
*.cfg
*.ini
*.sav'

WINE_VIRTUAL_DESKTOP='auto'
## Without dmsynth, the game is stuck on a black screen instead of playing the intro videos.
WINE_WINETRICKS_VERBS='dmsynth'

APP_MAIN_EXE='hitman.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_BIN'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
