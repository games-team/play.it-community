#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Vampire: The Masquerade - Redemption
# send your bug reports to contact@dotslashplay.it
###

script_version=20231006.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='vampire-the-masquerade-redemption'
GAME_NAME='Vampire: The Masquerade - Redemption'

ARCHIVE_BASE_0_NAME='setup_vampire_the_masquerade_redemption_2.0.0.3.exe'
ARCHIVE_BASE_0_MD5='96c19b0ce487e53dd8fe24ef963d31fc'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='1300000'
ARCHIVE_BASE_0_VERSION='1.1-gog2.0.0.3'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/vampire_the_masquerade_redemption'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
*.dll
*.exe
*.inf
*.isu
extras/cs
miles'
CONTENT_GAME_DATA_FILES='
*.dat
*.nob
*.pub
*.zip
vampireankh.ico
chronicles
extras
sounds
video'
CONTENT_DOC_DATA_FILES='
*.pdf
*.rtf'

USER_PERSISTENT_DIRECTORIES='
savegames'
USER_PERSISTENT_FILES='
*.ini'

APP_MAIN_EXE='vampire.exe'
APP_MAIN_ICON='vampireankh.ico'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

## TODO: The game can mess up with the display gamma setting, we should restore the previous value after exiting it.

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Set required registry keys

# shellcheck disable=SC1003
installdir='C:\\'"$(game_id)"
# shellcheck disable=SC1003
installdirexe='C:\\'"$(game_id)"'\\vampire.exe'

registry_dump_init_file='registry-dumps/init.reg'
## TODO: The CD key should be extracted from a file provided with the game installer,
##       instead of hardcoded in this script.
registry_dump_init_content='Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\Software\Activision\Vampire: The Masquerade - Redemption\v1.0]
"CDDRIVE"="c:"
"INSTALLDIR"="'"${installdir}"'"
"INSTALLDIREXE"="'"${installdirexe}"'"
"INSTALLTYPE"=dword:00000002
"Version"="0.1"

[HKEY_LOCAL_MACHINE\Software\WON\CDKeys]
"Vampire"=hex:4e,07,4a,bf,8e,4d,c1,71,9c,de,2b,b9,91,2e,a3,e9'
CONTENT_GAME_BIN_FILES="$(content_files 'GAME_BIN')
$registry_dump_init_file"
APP_REGEDIT="${APP_REGEDIT:-} $registry_dump_init_file"
SCRIPT_DEPS="${SCRIPT_DEPS:-} iconv"
check_deps

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Set required registry keys
	mkdir --parents "$(dirname "$registry_dump_init_file")"
	printf '%s' "$registry_dump_init_content" | \
		iconv --from-code=UTF-8 --to-code=UTF-16 --output="$registry_dump_init_file"
)

# Include game data

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
