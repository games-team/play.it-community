#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 mortalius
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Hammerwatch
# send your bug reports to contact@dotslashplay.it
###

script_version=20241122.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='hammerwatch'
GAME_NAME='Hammerwatch'

ARCHIVE_BASE_GOG_0_NAME='gog_hammerwatch_2.1.0.7.sh'
ARCHIVE_BASE_GOG_0_MD5='2d1f01b73f43e0b6399ab578c52c6cb6'
ARCHIVE_BASE_GOG_0_SIZE='230000'
ARCHIVE_BASE_GOG_0_VERSION='1.32-gog2.1.0.7'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/hammerwatch'

ARCHIVE_BASE_HUMBLE_1_NAME='hammerwatch_linux_141.zip'
ARCHIVE_BASE_HUMBLE_1_MD5='a342298f2201a33a616e412b70c4a7f8'
ARCHIVE_BASE_HUMBLE_1_SIZE='230000'
ARCHIVE_BASE_HUMBLE_1_VERSION='1.41-humble180913'
ARCHIVE_BASE_HUMBLE_1_URL='https://www.humblebundle.com/store/hammerwatch'

ARCHIVE_BASE_HUMBLE_0_NAME='hammerwatch_linux1.32.zip'
ARCHIVE_BASE_HUMBLE_0_MD5='c31f4053bcde3dc34bc8efe5f232c26e'
ARCHIVE_BASE_HUMBLE_0_SIZE='230000'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.32-humble160405'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='.'
CONTENT_PATH_DEFAULT_HUMBLE_0='Hammerwatch'
CONTENT_LIBS_FILES='
libfmod.so
libfmod.so.6
libfmod.so.6.0'
## TODO: Check if the Steam libraries are required.
CONTENT_LIBS_FILES="$CONTENT_LIBS_FILES
libCSteamworks.so
libsteam_api.so"
CONTENT_LIBS_LIBS64_RELATIVE_PATH='lib64'
CONTENT_LIBS_LIBS64_FILES="$CONTENT_LIBS_FILES"
CONTENT_LIBS_LIBS32_RELATIVE_PATH='lib'
CONTENT_LIBS_LIBS32_FILES="$CONTENT_LIBS_FILES"
CONTENT_GAME_MAIN_FILES='
editor
levels
mono
assets.bin
Hammerwatch.exe
FarseerPhysicsOTK.dll
Lidgren.Network.dll
Pngcs.dll
SDL2-CS.dll
SDL2-CS.dll.config
ICSharpCode.SharpZipLib.dll
Steamworks.NET.dll
Steamworks.NET.dll.config
TiltedEngine.dll'

USER_PERSISTENT_FILES='
*.txt
*.xml'
USER_PERSISTENT_DIRECTORIES='
levels'

APP_MAIN_EXE='Hammerwatch.exe'
APP_MAIN_ICON='Hammerwatch.exe'
## Copy the game binary into the game prefix
APP_MAIN_PRERUN='
# Copy the game binary into the user prefix
exe_destination="${PATH_PREFIX}/Hammerwatch.exe"
if [ -h "$exe_destination" ]; then
	cp --remove-destination "$(realpath "$exe_destination")" "$exe_destination"
fi
unset exe_destination
'

PACKAGES_LIST='
PKG_MAIN
PKG_LIBS64
PKG_LIBS32'

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_LIBS'
PKG_MAIN_DEPENDENCIES_LIBRARIES='
libSDL2-2.0.so.0'
PKG_MAIN_DEPENDENCIES_MONO_LIBRARIES='
mscorlib.dll
Mono.Posix.dll
Mono.Security.dll
System.dll
System.Configuration.dll
System.Core.dll
System.Data.dll
System.Drawing.dll
System.Runtime.Serialization.dll
System.Security.dll
System.Xml.dll
System.Xml.Linq.dll'

PKG_LIBS_ID="${GAME_ID}-libs"
PKG_LIBS64_ID="$PKG_LIBS_ID"
PKG_LIBS32_ID="$PKG_LIBS_ID"
PKG_LIBS64_ARCH='64'
PKG_LIBS32_ARCH='32'
PKG_LIBS_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6'
PKG_LIBS64_DEPENDENCIES_LIBRARIES="$PKG_LIBS_DEPENDENCIES_LIBRARIES"
PKG_LIBS32_DEPENDENCIES_LIBRARIES="$PKG_LIBS_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Replace duplicated libraries with symbolic links.
	rm \
		'lib/libfmod.so' 'lib/libfmod.so.6' \
		'lib64/libfmod.so' 'lib64/libfmod.so.6'
	ln --symbolic 'libfmod.so.6.0' 'lib/libfmod.so.6'
	ln --symbolic 'libfmod.so.6' 'lib/libfmod.so'
	ln --symbolic 'libfmod.so.6.0' 'lib64/libfmod.so.6'
	ln --symbolic 'libfmod.so.6' 'lib64/libfmod.so'
)

# Include game data

content_inclusion_icons 'PKG_MAIN'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_MAIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
