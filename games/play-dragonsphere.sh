#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Dragonsphere
# send your bug reports to contact@dotslashplay.it
###

script_version=20240619.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='dragonsphere'
GAME_NAME='Dragonsphere'

ARCHIVE_BASE_0_NAME='gog_dragonsphere_2.0.0.3.sh'
ARCHIVE_BASE_0_MD5='9dd42821c144aa87fd01b595607471c9'
ARCHIVE_BASE_0_SIZE='91000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.0.0.3'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/dragonsphere'

CONTENT_PATH_DEFAULT='data/noarch/data'
CONTENT_GAME_MAIN_FILES='
GAME.GOG
GAME.INS'
CONTENT_GAME0_MAIN_PATH="${CONTENT_PATH_DEFAULT}/DRAGON"
## FIXME: An explicit list of files should be set.
CONTENT_GAME0_MAIN_FILES='
*'

USER_PERSISTENT_FILES='
SAVES.DIR
CONFIG.DRA
DRAG*.SAV'

GAME_IMAGE='GAME.INS'

APP_MAIN_EXE='MAINMENU.EXE'
APP_MAIN_ICON='../support/icon.png'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Convert all file paths to uppercase.
	toupper .
)

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
