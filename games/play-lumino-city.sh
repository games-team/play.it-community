#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Lumino City
# send your bug reports to contact@dotslashplay.it
###

script_version=20240619.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='lumino-city'
GAME_NAME='Lumino City'

ARCHIVE_BASE_0_NAME='setup_lumino_city_1.0_(21180).exe'
ARCHIVE_BASE_0_MD5='32f0e9251c7936180321f30811bab8d9'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='930000'
ARCHIVE_BASE_0_VERSION='1.0-gog21180'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/lumino_city'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
adobe air
lumino city.exe'
CONTENT_GAME_DATA_FILES='
assets
luminocitysoundtrack
meta-inf
mimetype
*.swf'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Roaming/com.stateofplaygames.LuminoCity'

APP_MAIN_EXE='lumino city.exe'
APP_MAIN_ICON='goggame-1207667103.ico'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
