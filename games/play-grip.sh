#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Grip
# send your bug reports to contact@dotslashplay.it
###

script_version=20240611.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='grip-game'
GAME_NAME='Grip'

ARCHIVE_BASE_0_NAME='setup_grip_1.5.3_v2_(50529).exe'
ARCHIVE_BASE_0_MD5='1180ad9aae91fd168fb32d14c8516b05'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_grip_1.5.3_v2_(50529)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='ee3a558625e2a7d58f9be0bc9cbc6290'
ARCHIVE_BASE_0_PART2_NAME='setup_grip_1.5.3_v2_(50529)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='427b9d550c93423010f688f3dd886704'
ARCHIVE_BASE_0_SIZE='12000000'
ARCHIVE_BASE_0_VERSION='1.5.3-gog50529'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/grip'

UNREALENGINE4_NAME='grip'

CONTENT_PATH_DEFAULT='.'

HUGE_FILES_DATA="
${UNREALENGINE4_NAME}/content/paks/grip-windowsnoeditor.pak"

APP_MAIN_EXE='grip.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
