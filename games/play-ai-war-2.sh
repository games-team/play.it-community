#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# AI War 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20240806.2

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='ai-war-2'
GAME_NAME='AI War 2'

ARCHIVE_BASE_1_NAME='ai_war_2_5_607_75309.sh'
ARCHIVE_BASE_1_MD5='cace7c4dc863f420da52b6d6a1818c2f'
ARCHIVE_BASE_1_SIZE='3461745'
ARCHIVE_BASE_1_VERSION='5.607-gog75309'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/ai_war_2'

ARCHIVE_BASE_0_NAME='ai_war_2_4_006_54416.sh'
ARCHIVE_BASE_0_MD5='3bbcfa00ea5014d5d6f1258d780c97b0'
ARCHIVE_BASE_0_SIZE='3400000'
ARCHIVE_BASE_0_VERSION='4.006-gog54416'

UNITY3D_NAME='AIWar2Linux'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME0_BIN_FILES='
ArcenNetworkLib
CodeExternal
PlayerData
ReliableDLLStorage'
## This game seems to use a strange plugins system, so we include its directory as-is.
CONTENT_GAME1_BIN_FILES="
${UNITY3D_NAME}_Data/Plugins"
CONTENT_GAME0_DATA_FILES='
AIW2ModdingAndGUI
AssetBundles_Linux
GameData
GlobalBundles
XMLMods'

USER_PERSISTENT_DIRECTORIES='
PlayerData'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libz.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
