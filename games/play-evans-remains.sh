#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Evan's Remains
# send your bug reports to contact@dotslashplay.it
###

script_version=20240728.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='evans-remains'
GAME_NAME='Evan’s Remains'

ARCHIVE_BASE_0_NAME='setup_evans_remains_1.0_(41967).exe'
ARCHIVE_BASE_0_MD5='a91b5a8801daa0befc00e581e28d676a'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='180000'
ARCHIVE_BASE_0_VERSION='3.3.3-gog41967'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/evans_remains'

CONTENT_PATH_DEFAULT='.'
CONTENT_FONTS_DATA_FILES='
basis33.ttf
jf-dot-ayu18.ttf
koharufont.ttf
unifont-13.0.01.ttf'
CONTENT_GAME_BIN_FILES='
evansremains.exe
options.ini'
CONTENT_GAME_DATA_FILES='
data.win
*.csv'
CONTENT_DOC_DATA_FILES='
license.txt'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Local/Evan_v_3_3_3'

APP_MAIN_EXE='evansremains.exe'
APP_MAIN_ICON='evansremains.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Work around the shipped icon not using a square ratio

SCRIPT_DEPS="$SCRIPT_DEPS convert"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Work around the shipped icon not using a square ratio.
	icon_source_png='tmp/evansremains.exe_14_152_2057.png'
	icon_source_png_cropped='tmp/evansremains.exe_14_152_2057_cropped.png'
	icon_converted_128='evansremains_128.png'
	icon_converted_192='evansremains_192.png'
	icon_extract_png_from_file 'APP_MAIN_ICON' 'tmp'
	convert "$icon_source_png" -crop 149x149+1x0 "$icon_source_png_cropped"
	convert "$icon_source_png_cropped" -resize 128x128 "$icon_converted_128"
	convert "$icon_source_png_cropped" -resize 192x192 "$icon_converted_192"
	rm --recursive 'tmp'
)

# Include game data

## Work around the shipped icon not using a square ratio.
APP_MAIN_ICON_128='evansremains_128.png'
APP_MAIN_ICON_192='evansremains_192.png'
APP_MAIN_ICONS_LIST='
APP_MAIN_ICON_128
APP_MAIN_ICON_192'
content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
