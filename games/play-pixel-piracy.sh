#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 mortalius
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Pixel Piracy
# send your bug reports to contact@dotslashplay.it
###

script_version=20241130.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='pixel-piracy'
GAME_NAME='Pixel Piracy'

ARCHIVE_BASE_GOG_0_NAME='gog_pixel_piracy_2.5.0.9.sh'
ARCHIVE_BASE_GOG_0_MD5='b689db9e42afa0a83d364f95cfb4d6bf'
ARCHIVE_BASE_GOG_0_SIZE='290000'
ARCHIVE_BASE_GOG_0_VERSION='1.1.21-gog2.5.0.9'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/pixel_piracy'

## This DRM-free archive is no longer sold by Humble Bundle
ARCHIVE_BASE_HUMBLE_0_NAME='pixelpiracy_Linux1.1.21.zip'
ARCHIVE_BASE_HUMBLE_0_MD5='18fb2d3f8adf6f320d507653298dc504'
ARCHIVE_BASE_HUMBLE_0_SIZE='284617'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.1.21-humble150612'

UNITY3D_NAME='linux'
UNITY3D_PLUGINS='
ScreenSelector.so'
## TODO: Check if the Steam libraries are required
UNITY3D_PLUGINS="$UNITY3D_PLUGINS
libCSteamworks.so
libsteam_api.so"

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='Linux'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libGLU.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
