#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# A Story About My Uncle
# send your bug reports to contact@dotslashplay.it
###

script_version=20240327.2

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='a-story-about-my-uncle'
GAME_NAME='A Story About My Uncle'

## This DRM-free archive is no longer sold by humblebundle.com,
## they currently sell only Steam keys for this game.
ARCHIVE_BASE_0_NAME='Linux-NoDRM-ASAMU_5188.zip'
ARCHIVE_BASE_0_MD5='71f9f3add29a733c4a1a7d18d738d3d6'
ARCHIVE_BASE_0_SIZE='1300000'
ARCHIVE_BASE_0_VERSION='5188-humble170516'

CONTENT_PATH_DEFAULT='.'
CONTENT_LIBS_FILES='
libPhysXCooking.so
libPhysXCore.so
libPhysXExtensions.so.1
libPhysXLoader.so.1
libSDL2-2.0.so.0
libsteam_api.so
PhysXUpdateLoader.so'
CONTENT_LIBS_BIN64_PATH='Binaries/linux-amd64'
CONTENT_LIBS_BIN64_FILES="$CONTENT_LIBS_FILES"
CONTENT_LIBS_BIN32_PATH='Binaries/linux-x86'
CONTENT_LIBS_BIN32_FILES="$CONTENT_LIBS_FILES"
CONTENT_GAME_BIN64_FILES='
Binaries/linux-amd64/ASAMU'
CONTENT_GAME_BIN32_FILES='
Binaries/linux-x86/ASAMU'
CONTENT_GAME_DATA_FILES='
ASAMU
Engine'

APP_MAIN_EXE_BIN64='Binaries/linux-amd64/ASAMU'
APP_MAIN_EXE_BIN32='Binaries/linux-x86/ASAMU'

USER_PERSISTENT_DIRECTORIES='
ASAMU/Cloud
ASAMU/Config'
USER_PERSISTENT_FILES='
ASAMU/Saves'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
librt.so.1
libstdc++.so.6'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Check for the presence of optional archives

ARCHIVE_OPTIONAL_ICONS_NAME='a-story-about-my-uncle_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_MD5='db4eb7ab666e61ea5fc983102099ab31'
ARCHIVE_OPTIONAL_ICONS_URL='https://downloads.dotslashplay.it/resources/a-story-about-my-uncle/'
archive_initialize_optional \
	'ARCHIVE_ICONS' \
	'ARCHIVE_OPTIONAL_ICONS'

# Extract game data

archive_extraction_default
if archive_is_available 'ARCHIVE_ICONS'; then
	archive_extraction 'ARCHIVE_ICONS'
fi

# Include game data

if archive_is_available 'ARCHIVE_ICONS'; then
	CONTENT_ICONS_PATH='.'
	CONTENT_ICONS_FILES='
	32x32'
	content_inclusion 'ICONS' 'PKG_DATA' "$(path_icons)"
fi
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
