#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Deadlight
# send your bug reports to contact@dotslashplay.it
###

script_version=20240501.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='deadlight'
GAME_NAME='Deadlight'

ARCHIVE_BASE_1_NAME='setup_deadlight_directors_cut_gog-2(cs)_(19167).exe'
ARCHIVE_BASE_1_MD5='70b8df3f8162cc0a9a43e531b6a723c5'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_deadlight_directors_cut_gog-2(cs)_(19167)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='6392568bcc7c4a4613e09cdbb40afafd'
ARCHIVE_BASE_1_PART2_NAME='setup_deadlight_directors_cut_gog-2(cs)_(19167)-2.bin'
ARCHIVE_BASE_1_PART2_MD5='fa080fbdd39369969be4ac54c2aa7bd2'
ARCHIVE_BASE_1_SIZE='5100000'
ARCHIVE_BASE_1_VERSION='1.0-gog19167'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/deadlight_directors_cut'

ARCHIVE_BASE_0_NAME='setup_deadlight_directors_cut_2.0.0.2.exe'
ARCHIVE_BASE_0_MD5='1a753e4f9a209de5bd916ac98b88a9b7'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_deadlight_directors_cut_2.0.0.2-1.bin'
ARCHIVE_BASE_0_PART1_MD5='246b4684d801ea84b6490e01bcf5c659'
ARCHIVE_BASE_0_PART1_TYPE='rar'
ARCHIVE_BASE_0_PART2_NAME='setup_deadlight_directors_cut_2.0.0.2-2.bin'
ARCHIVE_BASE_0_PART2_MD5='3cb7cdd9fea50a66d7f06a6b2d0426c6'
ARCHIVE_BASE_0_SIZE='5100000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.0.0.2'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='game'
CONTENT_GAME_BIN_FILES='
engine
binaries'
CONTENT_GAME_DATA_FILES='
lotdgame'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/My Games/UnrealEngine3/LOTDGame'
WINE_WINEPREFIX_TWEAKS='mono'

APP_MAIN_EXE='binaries/win64/lotdgame.exe'
## The application type must be set explicitly, or it would be wrongly guessed as a Mono one.
APP_MAIN_TYPE='wine'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
