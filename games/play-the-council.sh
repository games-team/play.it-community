#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Council
# send your bug reports to contact@dotslashplay.it
###

script_version=20230730.7

GAME_ID='the-council'
GAME_NAME='The Council'

ARCHIVE_BASE_0='setup_the_council_0.9.5.6359_(33875).exe'
ARCHIVE_BASE_0_MD5='7e49e444eaf67a95698dc1929979754a'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1='setup_the_council_0.9.5.6359_(33875)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='8567d5d7bb55af6b5a13b57144d32342'
ARCHIVE_BASE_0_PART2='setup_the_council_0.9.5.6359_(33875)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='2946c8da1f85f498da17661b159bfe4f'
ARCHIVE_BASE_0_PART3='setup_the_council_0.9.5.6359_(33875)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='6d070987d39d722d3ceb1c7475f88c62'
ARCHIVE_BASE_0_SIZE='24000000'
ARCHIVE_BASE_0_VERSION='0.9.5.6359-gog33875'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/the_council'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
library
crashreport.exe
the council.exe
*.dll'
CONTENT_GAME_DATA_EPISODES_FILES='
data/packages/episode_*'
CONTENT_GAME_DATA_SOUND_FILES='
data/sound'
CONTENT_GAME_DATA_FILES='
defaultparameter
data
packagelist'

WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/My Games/The Council'

APP_MAIN_EXE='the council.exe'

PACKAGES_LIST='PKG_BIN PKG_DATA_EPISODES PKG_DATA_SOUND PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_DATA_EPISODES_ID="${PKG_DATA_ID}-episodes"
PKG_DATA_EPISODES_DESCRIPTION="$PKG_DATA_DESCRIPTION - episodes"
PKG_DATA_DEPS="${PKG_DATA_DEPS:-} $PKG_DATA_EPISODES_ID"

PKG_DATA_SOUND_ID="${PKG_DATA_ID}-sound"
PKG_DATA_SOUND_DESCRIPTION="$PKG_DATA_DESCRIPTION - sound"
PKG_DATA_DEPS="${PKG_DATA_DEPS:-} $PKG_DATA_SOUND_ID"

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

target_version='2.24'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Prevent inclusion of unwanted files
	rm --recursive \
		'__redist' \
		'commonappdata' \
		'tmp'
)

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build package

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
