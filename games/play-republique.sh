#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Republique
# send your bug reports to contact@dotslashplay.it
###

script_version=20231009.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='republique'
GAME_NAME='République'

## This game is no longer available for sale from gog.com
ARCHIVE_BASE_0_NAME='setup_republique_remastered_33555_(18824).exe'
ARCHIVE_BASE_0_MD5='e1c57ea3489a5ffd222990ab28a9ffe7'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_republique_remastered_33555_(18824)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='f8d81dd49c0a17e772601880c1d76058'
ARCHIVE_BASE_0_PART2_NAME='setup_republique_remastered_33555_(18824)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='485f3c68ca8bf61e328f2e659afc59b2'
ARCHIVE_BASE_0_SIZE='7800000'
ARCHIVE_BASE_0_VERSION='1.0-gog18824'

ARCHIVE_BASE_RAR_0_NAME='setup_republique_remastered_2.2.0.4.exe'
ARCHIVE_BASE_RAR_0_MD5='e2a15feb5b9217bebf0fec26e2a81869'
ARCHIVE_BASE_RAR_0_EXTRACTOR='innoextract'
ARCHIVE_BASE_RAR_0_EXTRACTOR_OPTIONS='--lowercase --gog'
ARCHIVE_BASE_RAR_0_PART1_NAME='setup_republique_remastered_2.2.0.4-1.bin'
ARCHIVE_BASE_RAR_0_PART1_MD5='b7a869b84db07e5981b9eb8be9ff2bda'
ARCHIVE_BASE_RAR_0_PART1_EXTRACTOR='unar'
ARCHIVE_BASE_RAR_0_PART2_NAME='setup_republique_remastered_2.2.0.4-2.bin'
ARCHIVE_BASE_RAR_0_PART2_MD5='604d6b34fa4b2cc24f79659922f188e2'
ARCHIVE_BASE_RAR_0_SIZE='7800000'
ARCHIVE_BASE_RAR_0_VERSION='1.0-gog2.2.0.4'

UNITY3D_NAME='republique'

CONTENT_PATH_DEFAULT='app'

USER_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Camouflaj/R__publique'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
