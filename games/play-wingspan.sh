#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Wingspan
# send your bug reports to contact@dotslashplay.it
###

script_version=20240706.2

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='wingspan'
GAME_NAME='Wingspan'

ARCHIVE_BASE_2_NAME='setup_wingspan_205_(73380).exe'
ARCHIVE_BASE_2_MD5='11f9487bfac4640f9c6b60ba571b9d3f'
ARCHIVE_BASE_2_TYPE='innosetup'
ARCHIVE_BASE_2_PART1_NAME='setup_wingspan_205_(73380)-1.bin'
ARCHIVE_BASE_2_PART1_MD5='d45eed5ada273495ec6686283cf2cb33'
ARCHIVE_BASE_2_SIZE='1713414'
ARCHIVE_BASE_2_VERSION='205-gog73380'
ARCHIVE_BASE_2_URL='https://www.gog.com/game/wingspan'

ARCHIVE_BASE_1_NAME='setup_wingspan_102_(53871).exe'
ARCHIVE_BASE_1_MD5='1628a30ffccd86d875895e2d166c1cde'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_wingspan_102_(53871)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='7a2baede2444e183941698a73e7a3044'
ARCHIVE_BASE_1_SIZE='1100000'
ARCHIVE_BASE_1_VERSION='102-gog53871'

ARCHIVE_BASE_0_NAME='setup_wingspan_83_(50869).exe'
ARCHIVE_BASE_0_MD5='5d68e6a8027e34504e86075a96e8bc15'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_wingspan_83_(50869)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='110175d9b0bca8b33b13e826da0a7b99'
ARCHIVE_BASE_0_SIZE='1100000'
ARCHIVE_BASE_0_VERSION='83-gog50869'

UNITY3D_NAME='wingspan'

CONTENT_PATH_DEFAULT='.'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Monster Couch/Wingspan'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
