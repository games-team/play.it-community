#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Closure
# send your bug reports to contact@dotslashplay.it
###

script_version=20241009.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='closure'
GAME_NAME='Closure'

ARCHIVE_BASE_0_NAME='Closure-Linux-1.1-2012-12-28.sh'
ARCHIVE_BASE_0_MD5='a7c2f7fe17ff67f376da70611abdce97'
ARCHIVE_BASE_0_SIZE='800000'
ARCHIVE_BASE_0_VERSION='1.1-humble.2012.12.28'
ARCHIVE_BASE_0_URL='https://www.humblebundle.com/store/closure'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN64_FILES='
Closure.bin.x86_64'
CONTENT_GAME_BIN32_FILES='
Closure.bin.x86'
CONTENT_GAME_DATA_FILES='
resources
Closure.png'
CONTENT_DOC_DATA_FILES='
README.linux'

APP_MAIN_EXE_BIN64='Closure.bin.x86_64'
APP_MAIN_EXE_BIN32='Closure.bin.x86'
APP_MAIN_ICON='Closure.png'
## The game crashes on launch when the Wayland backend of SDL is used.
APP_MAIN_PRERUN='
# The game crashes on launch when the Wayland backend of SDL is used
if [ "${SDL_VIDEODRIVER:-}" = "wayland" ]; then
	unset SDL_VIDEODRIVER
fi
'
## The game renders nothing but a black screen when using the SDL1 → SDL2 compatibility wrapper
APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}
# The game renders nothing but a black screen when using the SDL1 → SDL2 compatibility wrapper
export SDL12COMPAT_OPENGL_SCALING=0
"

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libalut.so.0
libCgGL.so
libCg.so
libc.so.6
libgcc_s.so.1
libGL.so.1
libm.so.6
libopenal.so.1
librt.so.1
libSDL-1.2.so.0
libstdc++.so.6'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
ARCHIVE_INNER_BIN64_PATH="${PLAYIT_WORKDIR}/gamedata/instarchive_linux_x86_64"
ARCHIVE_INNER_BIN64_TYPE='tar.xz'
archive_extraction 'ARCHIVE_INNER_BIN64'
rm "$(archive_path 'ARCHIVE_INNER_BIN64')"
ARCHIVE_INNER_BIN32_PATH="${PLAYIT_WORKDIR}/gamedata/instarchive_linux_x86"
ARCHIVE_INNER_BIN32_TYPE='tar.xz'
archive_extraction 'ARCHIVE_INNER_BIN32'
rm "$(archive_path 'ARCHIVE_INNER_BIN32')"
ARCHIVE_INNER_DATA_PATH="${PLAYIT_WORKDIR}/gamedata/instarchive_all"
ARCHIVE_INNER_DATA_TYPE='tar.xz'
archive_extraction 'ARCHIVE_INNER_DATA'
rm "$(archive_path 'ARCHIVE_INNER_DATA')"

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
