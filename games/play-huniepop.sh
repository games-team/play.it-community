#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Mopi
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# HuniePop
# send your bug reports to contact@dotslashplay.it
###

script_version=20240610.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='huniepop'
GAME_NAME='HuniePop'

ARCHIVE_BASE_1_NAME='gog_huniepop_2.0.0.3.sh'
ARCHIVE_BASE_1_MD5='d229aea2b601137537f7be46c7327660'
ARCHIVE_BASE_1_SIZE='926917'
ARCHIVE_BASE_1_VERSION='1.2.0-gog2.0.0.3'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/huniepop'

ARCHIVE_BASE_0_NAME='gog_huniepop_2.0.0.2.sh'
ARCHIVE_BASE_0_MD5='020cd6a015bd79a907f6c607102d797a'
ARCHIVE_BASE_0_SIZE='940000'
ARCHIVE_BASE_0_VERSION='1.2.0-gog2.0.0.2'

UNITY3D_NAME='HuniePop'
## TODO: Check if the Steam libraries are required.
UNITY3D_PLUGINS='
libCSteamworks.so
libsteam_api.so'

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libGLU.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXext.so.6'
## TODO: Check if lsb_release is still required to prevent a failure to launch on Arch Linux.
PKG_BIN_DEPENDENCIES_COMMANDS='
lsb_release'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
