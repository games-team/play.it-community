#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Emmanuel Gil Peyrot <linkmauve@linkmauve.fr>
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Populous 3
# send your bug reports to contact@dotslashplay.it
###

script_version=20240516.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='populous-3'
GAME_NAME='Populous: The Beginning'

ARCHIVE_BASE_1_NAME='setup_populous_the_beginning_1.02_depfix2_(48318).exe'
ARCHIVE_BASE_1_MD5='12cb9973783ad50789b7531e60bfcb24'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='510000'
ARCHIVE_BASE_1_VERSION='1.02-gog48318'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/populous_the_beginning'

ARCHIVE_BASE_0_NAME='setup_populous_the_beginning_2.0.0.5.exe'
ARCHIVE_BASE_0_MD5='7e4545d04a3d00193507aa82dea14e50'
ARCHIVE_BASE_0_VERSION='1.02-gog2.0.0.2'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='430000'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_BIN_FILES='
*.dll
*.exe'
CONTENT_GAME_DATA_FILES='
data
fmv
language
levels
levluw
objects
rddata
sound'
CONTENT_DOC_DATA_FILES='
*.pdf
*.txt'

USER_PERSISTENT_DIRECTORIES='
SAVE'

WINE_VIRTUAL_DESKTOP='auto'

APP_MAIN_EXE='poptb.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Set required registry keys

registry_dump_init_file='registry-dumps/init.reg'
# shellcheck disable=SC1003
registry_dump_init_content='Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\Software\Bullfrog Productions Ltd\Populous: The Beginning]
"BuildTypeCode"=dword:00000001
"InstallDirectory"="\\'"$GAME_ID"'"
"InstallDrive"="C:"
"InstallPath"="C:\\'"$GAME_ID"'"
"Language"=dword:00000009
"Version"="1.01"'
CONTENT_GAME_BIN_FILES="${CONTENT_GAME_BIN_FILES:-}
$registry_dump_init_file"
APP_REGEDIT="${APP_REGEDIT:-} $registry_dump_init_file"
SCRIPT_DEPS="${SCRIPT_DEPS:-} iconv"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Delete unwanted files.
	rm --force --recursive \
		'__redist' \
		'commonappdata' \
		'tmp'

	## Set required registry keys.
	mkdir --parents "$(dirname "$registry_dump_init_file")"
	printf '%s' "$registry_dump_init_content" | \
		iconv \
		--from-code=UTF-8 --to-code=UTF-16 \
		--output="$registry_dump_init_file"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
