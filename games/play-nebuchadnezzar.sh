#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Nebuchadnezzar
# send your bug reports to contact@dotslashplay.it
###

script_version=20230814.1

GAME_ID='nebuchadnezzar'
GAME_NAME='Nebuchadnezzar'

ARCHIVE_BASE_0='nebuchadnezzar_1_4_11_63946.sh'
ARCHIVE_BASE_0_MD5='7f4bb453c7fb510fd10c125ec17dcd53'
ARCHIVE_BASE_0_SIZE='1600000'
ARCHIVE_BASE_0_VERSION='1.4.11-gog63946'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/nebuchadnezzar'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN_FILES='
binary/linux/nebuchadnezzar'
CONTENT_GAME_DATA_FILES='
data'

APP_MAIN_EXE='binary/linux/nebuchadnezzar'
APP_MAIN_ICON='../support/icon.png'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
liballegro.so.5.2
liballegro_acodec.so.5.2
liballegro_audio.so.5.2
liballegro_font.so.5.2
liballegro_image.so.5.2
liballegro_primitives.so.5.2
liballegro_ttf.so.5.2
libc.so.6
libgcc_s.so.1
liblua5.3.so.0
libm.so.6
libpng16.so.16
libpthread.so.0
libstdc++.so.6'

# Run the game binary from its parent directory

APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}"'
# Run the game binary from its parent directory
cd "$(dirname "$APP_EXE")"
APP_EXE=$(basename "$APP_EXE")
'

# Load common functions

target_version='2.25'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
