#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 HS-157
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Tiny Hunter
# send your bug reports to contact@dotslashplay.it
###

script_version=20240616.2

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='tiny-hunter'
GAME_NAME='Tiny Hunter'

ARCHIVE_BASE_1_NAME='tinyhunter_linux.zip'
ARCHIVE_BASE_1_MD5='cb14caf2a47efaee84c0cdc6f8be82c6'
ARCHIVE_BASE_1_SIZE='157912'
ARCHIVE_BASE_1_VERSION='1.0-itch.2020.10.15'
ARCHIVE_BASE_1_URL='https://nojigames.itch.io/tinyhunter'

ARCHIVE_BASE_0_NAME='TinyHunterWindowMacLinux.zip'
ARCHIVE_BASE_0_MD5='9fa1ab03e2769365229a29b6b37995f7'
ARCHIVE_BASE_0_SIZE='350000'
ARCHIVE_BASE_0_VERSION='1.0-itch'
ARCHIVE_BASE_0_URL='https://nojigames.itch.io/tinyhunter'

UNITY3D_NAME='tinyhunter_linux'
UNITY3D_NAME_0='tinyhunter'
## No Unity3D plugin is shipped.

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='TinyHunterWindowMacLinux/Linux'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
