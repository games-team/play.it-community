#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Our Life: Beginnings & Always
# send your bug reports to contact@dotslashplay.it
###

script_version=20230409.1

GAME_ID='our-life-beginning-and-always'
GAME_NAME='Our Life: Beginnings & Always'

ARCHIVE_BASE_0='our_life_beginnings_always_1_6_2_59318.sh'
ARCHIVE_BASE_0_MD5='543ec491817a024342902bd379d56fe6'
ARCHIVE_BASE_0_TYPE='mojosetup'
ARCHIVE_BASE_0_SIZE='860000'
ARCHIVE_BASE_0_VERSION='1.6.2-gog59318'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/our_life_beginnings_always'

RENPY_NAME='OurLife'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN_PATH="${CONTENT_PATH_DEFAULT}/lib/linux-x86_64"
CONTENT_LIBS_BIN_FILES='
librenpython.so'
CONTENT_GAME_BIN_FILES="
lib/linux-x86_64/python
lib/linux-x86_64/pythonw
lib/linux-x86_64/zsync
lib/linux-x86_64/zsyncmake
lib/linux-x86_64/${RENPY_NAME}"
CONTENT_GAME_DATA_FILES="
game
renpy
lib/python2.7
${RENPY_NAME}.py"

APP_MAIN_EXE="lib/linux-x86_64/${RENPY_NAME}"
APP_MAIN_ICON='icon.png'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libGL.so.1
libm.so.6
libpthread.so.0
libutil.so.1'

# Load common functions

target_version='2.22'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
