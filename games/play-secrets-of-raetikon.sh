#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Mopi
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Secrets of Raetikon
# send your bug reports to contact@dotslashplay.it
###

script_version=20231111.5

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='secrets-of-raetikon'
GAME_NAME='Secrets of Raetikon'

ARCHIVE_BASE_0_NAME='SecretsofRaetikonLinux1.1.zip'
ARCHIVE_BASE_0_MD5='16a81710ce12480c9cd75a6992d2956c'
ARCHIVE_BASE_0_SIZE='130000'
ARCHIVE_BASE_0_VERSION='1.1-humble140731'
ARCHIVE_BASE_0_URL='https://www.humblebundle.com/store/secrets-of-rtikon'

CONTENT_PATH_DEFAULT='Raetikon'
CONTENT_LIBS_BIN64_PATH="${CONTENT_PATH_DEFAULT}/lib64"
CONTENT_LIBS_BIN64_FILES='
libfmodex64-4.44.32.so
libfmodex64.so'
CONTENT_LIBS_BIN32_PATH="${CONTENT_PATH_DEFAULT}/lib32"
CONTENT_LIBS_BIN32_FILES='
libfmodex-4.44.32.so
libfmodex.so'
CONTENT_GAME_BIN64_FILES='
Raetikon64'
CONTENT_GAME_BIN32_FILES='
Raetikon'
CONTENT_GAME_DATA_FILES='
data
steam_appid.txt'
CONTENT_DOC_DATA_PATH='.'
CONTENT_DOC_DATA_FILES='
Manual.pdf'
## The game binaries are linked against libsteam_api.so, so it can not be dropped.
CONTENT_LIBS_BIN64_FILES="${CONTENT_LIBS_BIN64_FILES:-}
libsteam_api.so"
CONTENT_LIBS_BIN32_FILES="${CONTENT_LIBS_BIN32_FILES:-}
libsteam_api.so"

APP_MAIN_EXE_BIN32='Raetikon'
APP_MAIN_EXE_BIN64='Raetikon64'
## Create a minimal configuration file to avoid a black screen on Intel chipset.
APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}"'
# Create a minimal configuration file to avoid a black screen on Intel chipset
config_file="${HOME}/.secrets-of-raetikon/Options.xml"
if [ ! -e "$config_file" ]; then
	mkdir --parents "$(dirname "$config_file")"
	cat > "$config_file" <<- EOF
	<root>
	    <rendering>
	        <postProcessing value="false" />
	    </rendering>
	</root>
	EOF
fi
'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libasound.so.2
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
libstdc++.so.6
libX11.so.6
libXrandr.so.2'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN64'
# shellcheck disable=SC2119
launchers_write
set_current_package 'PKG_BIN32'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
