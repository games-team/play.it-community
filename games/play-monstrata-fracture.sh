#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Monstrata Fracture
# send your bug reports to contact@dotslashplay.it
###

script_version=20231127.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='monstrata-fracture'
GAME_NAME='Monstrata Fracture'

ARCHIVE_BASE_1_NAME='monstrata-fracture-win-linux.zip'
ARCHIVE_BASE_1_MD5='9839d978298251f296febd5bc9a9b550'
ARCHIVE_BASE_1_SIZE='250000'
ARCHIVE_BASE_1_VERSION='1.2.11-itch1'
ARCHIVE_BASE_1_URL='https://astralore.itch.io/monstrata-fracture'

ARCHIVE_BASE_0_NAME='monstrata-fracture-win-osx-linux.zip'
ARCHIVE_BASE_0_MD5='7fa34744e2ff3ad7b745909ebfea51fc'
ARCHIVE_BASE_0_SIZE='270000'
ARCHIVE_BASE_0_VERSION='1.2.11-itch1'

CONTENT_PATH_DEFAULT_1='Monstrata-1.2.11-pc'
CONTENT_PATH_DEFAULT_0='Monstrata-1.2.11-market'
CONTENT_LIBS_BIN_FILES='
libavcodec.so.57
libavformat.so.57
libavresample.so.3
libavutil.so.55
libGLEW.so.1.7
libpython2.7.so.1.0
libswresample.so.2
libswscale.so.4'
CONTENT_LIBS_BIN32_PATH_1="${CONTENT_PATH_DEFAULT_1}/lib/linux-x86"
CONTENT_LIBS_BIN32_PATH_0="${CONTENT_PATH_DEFAULT_0}/lib/linux-x86"
CONTENT_LIBS_BIN64_PATH_1="${CONTENT_PATH_DEFAULT_1}/lib/linux-x86_64"
CONTENT_LIBS_BIN64_PATH_0="${CONTENT_PATH_DEFAULT_0}/lib/linux-x86_64"
CONTENT_LIBS_BIN32_FILES="$CONTENT_LIBS_BIN_FILES"
CONTENT_LIBS_BIN64_FILES="$CONTENT_LIBS_BIN_FILES"
CONTENT_GAME_BIN32_FILES='
lib/linux-i686/eggs
lib/linux-i686/lib
lib/linux-i686/Monstrata
lib/linux-i686/python
lib/linux-i686/pythonw
lib/linux-i686/zsync
lib/linux-i686/zsyncmake'
CONTENT_GAME_BIN64_FILES='
lib/linux-x86_64/eggs
lib/linux-x86_64/lib
lib/linux-x86_64/Monstrata
lib/linux-x86_64/python
lib/linux-x86_64/pythonw
lib/linux-x86_64/zsync
lib/linux-x86_64/zsyncmake'
CONTENT_GAME_DATA_FILES='
game
renpy
lib/pythonlib2.7
Monstrata.py'
CONTENT_DOC_DATA_FILES='
credits.txt'

APP_MAIN_EXE_BIN64='lib/linux-x86_64/Monstrata'
APP_MAIN_EXE_BIN32='lib/linux-i686/Monstrata'
APP_MAIN_OPTIONS='-EO Monstrata.py'
APP_MAIN_ICON='Monstrata.exe'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libGL.so.1
libGLU.so.1
libm.so.6
libpng12.so.0
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libSDL2_image-2.0.so.0
libSDL2_ttf-2.0.so.0
libutil.so.1
libX11.so.6
libXext.so.6
libXi.so.6
libXmu.so.6
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN32'
# shellcheck disable=SC2119
launchers_write
set_current_package 'PKG_BIN64'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
