#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Perimeter
# send your bug reports to contact@dotslashplay.it
###

script_version=20240605.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='perimeter'
GAME_NAME='Perimeter'

ARCHIVE_BASE_0_NAME='setup_perimeter_1.03_(19064).exe'
ARCHIVE_BASE_0_MD5='a65c86a8de9b938bc070bcbe5900aef2'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_perimeter_1.03_(19064)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='ba08f50e95ce6424d214f57383d338c8'
ARCHIVE_BASE_0_SIZE='3800000'
ARCHIVE_BASE_0_VERSION='1.03-gog19064'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/perimeter'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
binkw32.dll
*.exe'
CONTENT_GAME0_BIN_PATH='app/__support/app'
CONTENT_GAME0_BIN_FILES='
*'
CONTENT_GAME_DATA_FILES='
cache_font
resource
scripts
perimeter.ico'
CONTENT_DOC_DATA_FILES='
manual.pdf
readme.txt'

USER_PERSISTENT_DIRECTORIES='
resource/saves'
USER_PERSISTENT_FILES='
*.ini'

APP_MAIN_EXE='perimeter.exe'
APP_MAIN_ICON='perimeter.ico'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libXcursor.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
