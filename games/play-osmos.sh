#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Osmos
# send your bug reports to contact@dotslashplay.it
###

script_version=20241124.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='osmos'
GAME_NAME='Osmos'

ARCHIVE_BASE_0_NAME='Osmos_1.6.1.tar.gz'
ARCHIVE_BASE_0_MD5='ed2cb029c20c25de719c28062e6fc9cf'
ARCHIVE_BASE_0_SIZE='32000'
ARCHIVE_BASE_0_VERSION='1.6.1-humble1'
ARCHIVE_BASE_0_URL='https://www.humblebundle.com/store/osmos'

CONTENT_PATH_DEFAULT='Osmos'
CONTENT_GAME_BIN64_FILES='
Osmos.bin64'
CONTENT_GAME_BIN32_FILES='
Osmos.bin32'
CONTENT_GAME_DATA_FILES='
Fonts
Icons
Sounds
Textures
*.cfg
*.loc'
CONTENT_DOC_DATA_FILES='
*.html
*.txt'

USER_PERSISTENT_FILES='
*.cfg'

APP_MAIN_EXE_BIN64='Osmos.bin64'
APP_MAIN_EXE_BIN32='Osmos.bin32'
for icon_size in 16 22 32 36 48 64 72 96 128 192 256; do
	APP_MAIN_ICONS_LIST="${APP_MAIN_ICONS_LIST:-} APP_MAIN_ICON_${icon_size}"
	export "APP_MAIN_ICON_${icon_size}"="Icons/${icon_size}x${icon_size}.png"
done

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libfreetype.so.6
libgcc_s.so.1
libGL.so.1
libGLU.so.1
libm.so.6
libopenal.so.1
libstdc++.so.6
libvorbisfile.so.3
libX11.so.6'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
