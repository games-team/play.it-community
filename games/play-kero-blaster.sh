#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 BetaRays
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Kero Blaster
# send your bug reports to contact@dotslashplay.it
###

script_version=20240610.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='kero-blaster'
GAME_NAME='Kero Blaster'

## The following archive used to be sold from Playism store,
## before it closed down in favour of Steam.
ARCHIVE_BASE_0_NAME='KeroBlaster_EN_v1501a.zip'
ARCHIVE_BASE_0_MD5='c6ba58d37b5344d08c7d9a94506266b0'
ARCHIVE_BASE_0_SIZE='20000'
ARCHIVE_BASE_0_VERSION='1.501-playism1501a'

CONTENT_PATH_DEFAULT='KeroBlasterEn'
CONTENT_GAME_BIN_FILES='
KeroBlaster.exe'
CONTENT_GAME_DATA_FILES='
rsc_k'
CONTENT_DOC_DATA_FILES='
ReadmeEn.txt'

APP_MAIN_EXE='KeroBlaster.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libasound.so.2
libmpg123.so.0'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
