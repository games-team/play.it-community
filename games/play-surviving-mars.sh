#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Janeene Beeforth
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Surviving Mars
# send your bug reports to contact@dotslashplay.it
###

script_version=20231111.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='surviving-mars'
GAME_NAME='Surviving Mars'

ARCHIVE_BASE_5_NAME='surviving_mars_cernan_update_29871.sh'
ARCHIVE_BASE_5_MD5='0c204fb895101f8b3f844a1fa06e2feb'
ARCHIVE_BASE_5_SIZE='6200000'
ARCHIVE_BASE_5_VERSION='1.0-gog29871'
ARCHIVE_BASE_5_URL='https://www.gog.com/game/surviving_mars'

ARCHIVE_BASE_4_NAME='surviving_mars_sagan_rc3_update_24111.sh'
ARCHIVE_BASE_4_MD5='22e5cbc7188ff1cb8fd5dabf7cdca0bf'
ARCHIVE_BASE_4_SIZE='4700000'
ARCHIVE_BASE_4_VERSION='1.0-gog24111'

ARCHIVE_BASE_3_NAME='surviving_mars_sagan_rc1_update_23676.sh'
ARCHIVE_BASE_3_MD5='2e5058a9f1076f894c0b074fd24e3597'
ARCHIVE_BASE_3_SIZE='4700000'
ARCHIVE_BASE_3_VERSION='1.0-gog23676'

ARCHIVE_BASE_2_NAME='surviving_mars_en_davinci_rc1_22763.sh'
ARCHIVE_BASE_2_MD5='aa513fee4b4c10318831712d4663bfc0'
ARCHIVE_BASE_2_SIZE='4400000'
ARCHIVE_BASE_2_VERSION='1.0-gog22763'

ARCHIVE_BASE_1_NAME='surviving_mars_en_180619_curiosity_hotfix_3_21661.sh'
ARCHIVE_BASE_1_MD5='241f1cb8305becab5d55c8d104bd2c18'
ARCHIVE_BASE_1_SIZE='4100000'
ARCHIVE_BASE_1_VERSION='1.0-gog21661'

ARCHIVE_BASE_0_NAME='surviving_mars_en_curiosity_update_21183.sh'
ARCHIVE_BASE_0_MD5='ab9a61d04a128f19bc9e003214fe39a9'
ARCHIVE_BASE_0_SIZE='4000000'
ARCHIVE_BASE_0_VERSION='1.0-gog21183'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN_FILES='
libpops_api.so'
CONTENT_GAME_BIN_FILES='
MarsGOG'
CONTENT_GAME_DATA_FILES='
DLC
Licenses
Local
ModTools
Movies
Packs
ShaderPreprocessorTemp'

APP_MAIN_EXE='MarsGOG'
APP_MAIN_ICON='../support/icon.png'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libcurl-gnutls.so.4
libdl.so.2
libgcc_s.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
libresolv.so.2
libSDL2-2.0.so.0
libssl.so.1.0.0
libstdc++.so.6
libuuid.so.1
libX11.so.6
libXext.so.6
libXrandr.so.2
libXrender.so.1
libXt.so.6
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
