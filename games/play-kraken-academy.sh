#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Kraken Academy
# send your bug reports to contact@dotslashplay.it
###

script_version=20230406.1

GAME_ID='kraken-academy'
GAME_NAME='Kraken Academy'

ARCHIVE_BASE_0='kraken_academy_1_0_12_1_51227.sh'
ARCHIVE_BASE_0_MD5='8f5a82e43e81913a0181029d968e13f5'
ARCHIVE_BASE_0_TYPE='mojosetup'
ARCHIVE_BASE_0_SIZE='330000'
ARCHIVE_BASE_0_VERSION='1.0.12.1-gog51227'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/kraken_academy'

UNITY3D_NAME='Kraken Academy'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN_PATH="${CONTENT_PATH_DEFAULT}/${UNITY3D_NAME}_Data/Plugins"
CONTENT_LIBS_BIN_FILES='
libfmodstudio.so
libresonanceaudio.so'
CONTENT_LIBS0_BIN_FILES='
GameAssembly.so
UnityPlayer.so'
CONTENT_GAME_BIN_FILES="
${UNITY3D_NAME}"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data/il2cpp_data
${UNITY3D_NAME}_Data/Resources
${UNITY3D_NAME}_Data/StreamingAssets
${UNITY3D_NAME}_Data/app.info
${UNITY3D_NAME}_Data/boot.config
${UNITY3D_NAME}_Data/data.unity3d
${UNITY3D_NAME}_Data/*.json
${UNITY3D_NAME}_Data/*.resource"

USER_PERSISTENT_DIRECTORIES="
${UNITY3D_NAME}_Data/StreamingAssets/Saves"

APP_MAIN_EXE="$UNITY3D_NAME"

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6'

# Load common functions

target_version='2.22'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Link GameAssembly.so in the game data path
# as the game engine fails to find it otherwise,
# even if it is in LD_LIBRARY_PATH.

file_name='GameAssembly.so'
file_source="$(path_libraries)/${file_name}"
file_destination="$(package_path 'PKG_BIN')$(path_game_data)/${file_name}"
mkdir --parents "$(dirname "$file_destination")"
ln --symbolic "$file_source" "$file_destination"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
