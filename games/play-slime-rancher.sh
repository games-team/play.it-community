#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Slime Rancher
# send your bug reports to contact@dotslashplay.it
###

script_version=20230405.1

GAME_ID='slime-rancher'
GAME_NAME='Slime Rancher'

ARCHIVE_BASE_0='slime_rancher_1_4_4_51897.sh'
ARCHIVE_BASE_0_MD5='1933e39e9f21f09b60312ced60640aa6'
ARCHIVE_BASE_0_TYPE='mojosetup'
ARCHIVE_BASE_0_SIZE='1200000'
ARCHIVE_BASE_0_VERSION='1.4.4-gog51897'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/slime_rancher'

UNITY3D_NAME='SlimeRancher'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN_FILES='
UnityPlayer.so'
CONTENT_GAME_BIN_FILES="
${UNITY3D_NAME}.x86_64
${UNITY3D_NAME}_Data/MonoBleedingEdge/x86_64"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data/GI
${UNITY3D_NAME}_Data/Managed
${UNITY3D_NAME}_Data/MonoBleedingEdge/etc
${UNITY3D_NAME}_Data/Resources
${UNITY3D_NAME}_Data/StreamingAssets
${UNITY3D_NAME}_Data/globalgamemanagers
${UNITY3D_NAME}_Data/boot.config
${UNITY3D_NAME}_Data/app.info
${UNITY3D_NAME}_Data/level?
${UNITY3D_NAME}_Data/*.assets
${UNITY3D_NAME}_Data/*.resource
${UNITY3D_NAME}_Data/*.resS"

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libz.so.1'

# Load common functions

target_version='2.22'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
