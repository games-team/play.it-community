#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Anna Lea
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Beat Cop
# send your bug reports to contact@dotslashplay.it
###

script_version=20241112.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='beat-cop'
GAME_NAME='Beat Cop'

ARCHIVE_BASE_0_NAME='beat_cop_en_1_1_744_17129.sh'
ARCHIVE_BASE_0_MD5='090be367a794f7c8a0043a94bea71bf8'
ARCHIVE_BASE_0_SIZE='360000'
ARCHIVE_BASE_0_VERSION='1.1.744-gog17129'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/beat_cop'

UNITY3D_NAME='BeatCop'

CONTENT_PATH_DEFAULT='data/noarch/game'
## FIXME: The list of Unity3D plugins to include should be set using UNITY3D_PLUGINS
CONTENT_GAME0_BIN64_FILES="
${UNITY3D_NAME}_Data/Plugins/x86_64"
CONTENT_GAME0_BIN32_FILES="
${UNITY3D_NAME}_Data/Plugins/x86"
## FIXME: Shipped libraries to include should be listed using CONTENT_xxx_LIBS_{PATH,FILES}
CONTENT_GAME1_BIN64_FILES='
lib64'
CONTENT_GAME1_BIN32_FILES='
lib32'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
# TODO: The list of dependencies should be completed
PKG_BIN_DEPENDENCIES_LIBRARIES='
libstdc++.so.6
libSDL2-2.0.so.0
libgdk-x11-2.0.so.0
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
