#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Accelerate
# send your bug reports to contact@dotslashplay.it
###

script_version=20231107.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='accelerate'
GAME_NAME='Accelerate'

ARCHIVE_BASE_0_NAME='acceleratelinux.zip'
ARCHIVE_BASE_0_MD5='42d13702bbec46f8cd2c357886fde0c0'
ARCHIVE_BASE_0_SIZE='230000'
ARCHIVE_BASE_0_VERSION='1.0-itch'
ARCHIVE_BASE_0_URL='https://whitehatcat.itch.io/accelerate'

CONTENT_PATH_DEFAULT='acceleratelinux'
CONTENT_GAME_BIN_FILES='
acceleratelinux.x86_64'
CONTENT_GAME_DATA_FILES='
acceleratelinux.pck'

APP_MAIN_EXE='acceleratelinux.x86_64'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libasound.so.2
libc.so.6
libdl.so.2
libGL.so.1
libm.so.6
libpthread.so.0
libpulse.so.0
libX11.so.6
libXcursor.so.1
libXinerama.so.1
libXi.so.6
libXrandr.so.2
libXrender.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
