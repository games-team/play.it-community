#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Order of the Thorne
# send your bug reports to contact@dotslashplay.it
###

script_version=20240624.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='order-of-the-thorne'
GAME_NAME='Order of the Thorne: The Kingʼs Challenge'

ARCHIVE_BASE_0_NAME='gog_order_of_the_thorne_the_king_s_challenge_2.0.0.1.sh'
ARCHIVE_BASE_0_MD5='16dde031dcfb730be1d94bde77306b0d'
ARCHIVE_BASE_0_SIZE='790000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.0.0.1'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/order_of_the_thorne_the_kings_challenge'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
acsetup.cfg
agsgame.dat
modules.lst
*.vox'
CONTENT_DOC_MAIN_FILES='
licenses'

APP_MAIN_SCUMMID='ags:oott'
APP_MAIN_ICON='oott-tkc.png'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
