#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Neverwinter Nights 1 Enhanced Edition localization packs:
# - German
# - French
# - Polish
# send your bug reports to contact@dotslashplay.it
###

script_version=20241212.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='neverwinter-nights-1-enhanced-edition'
GAME_NAME='Neverwinter Nights: Enhanced Edition'

GAME_NAME_DE="$GAME_NAME - German localization"
GAME_NAME_FR="$GAME_NAME - French localization"
GAME_NAME_PL="$GAME_NAME - Polish localization"

ARCHIVE_BASE_DE_6_NAME='neverwinter_nights_enhanced_edition_german_extras_88_8193_36_13_72824.sh'
ARCHIVE_BASE_DE_6_MD5='082281faf84fc9be25e1aaf21a99a1ce'
ARCHIVE_BASE_DE_6_SIZE='892427'
ARCHIVE_BASE_DE_6_VERSION='88.8193.36.13-gog72824'
ARCHIVE_BASE_DE_6_URL='https://www.gog.com/game/neverwinter_nights_enhanced_edition_german_extras'

ARCHIVE_BASE_FR_6_NAME='neverwinter_nights_enhanced_edition_french_extras_88_8193_36_13_72824.sh'
ARCHIVE_BASE_FR_6_MD5='088b8c890ecbf09b86a7c75e07ef43f1'
ARCHIVE_BASE_FR_6_SIZE='833009'
ARCHIVE_BASE_FR_6_VERSION='88.8193.36.13-gog72824'
ARCHIVE_BASE_FR_6_URL='https://www.gog.com/game/neverwinter_nights_enhanced_edition_french_extras'

ARCHIVE_BASE_PL_6_NAME='neverwinter_nights_enhanced_edition_polish_extras_88_8193_36_13_72824.sh'
ARCHIVE_BASE_PL_6_MD5='89e2dced24b885c04e1c45bc71e9960f'
ARCHIVE_BASE_PL_6_SIZE='820818'
ARCHIVE_BASE_PL_6_VERSION='88.8193.36.13-gog72824'
ARCHIVE_BASE_PL_6_URL='https://www.gog.com/game/neverwinter_nights_enhanced_edition_polish_extras'

ARCHIVE_BASE_DE_5_NAME='neverwinter_nights_enhanced_edition_german_extras_87_8193_35_40_65678.sh'
ARCHIVE_BASE_DE_5_MD5='596b075b63b8f9f76bbf1eeaa8150cfb'
ARCHIVE_BASE_DE_5_SIZE='892816'
ARCHIVE_BASE_DE_5_VERSION='87.8193.35.40-gog65678'

ARCHIVE_BASE_FR_5_NAME='neverwinter_nights_enhanced_edition_french_extras_87_8193_35_40_65678.sh'
ARCHIVE_BASE_FR_5_MD5='88b889d8eefadfde33d0fad8bac1b9be'
ARCHIVE_BASE_FR_5_SIZE='833404'
ARCHIVE_BASE_FR_5_VERSION='87.8193.35.40-gog65678'

ARCHIVE_BASE_PL_5_NAME='neverwinter_nights_enhanced_edition_polish_extras_87_8193_35_40_65678.sh'
ARCHIVE_BASE_PL_5_MD5='c78a405c135ffad7e126bd44113ecc86'
ARCHIVE_BASE_PL_5_SIZE='833404'
ARCHIVE_BASE_PL_5_VERSION='87.8193.35.40-gog65678'

ARCHIVE_BASE_DE_4_NAME='neverwinter_nights_enhanced_edition_german_extras_85_8193_33_50393.sh'
ARCHIVE_BASE_DE_4_MD5='d20a22fc2dfdeca4bb71bb7fd7e13a82'
ARCHIVE_BASE_DE_4_SIZE='900000'
ARCHIVE_BASE_DE_4_VERSION='85.8193.33-gog50393'

ARCHIVE_BASE_FR_4_NAME='neverwinter_nights_enhanced_edition_french_extras_85_8193_33_50393.sh'
ARCHIVE_BASE_FR_4_MD5='f39ecc55b15740322d8ae860ccfecbc5'
ARCHIVE_BASE_FR_4_SIZE='840000'
ARCHIVE_BASE_FR_4_VERSION='85.8193.33-gog50393'

ARCHIVE_BASE_DE_1_NAME='neverwinter_nights_enhanced_edition_german_extras_81_8193_16_41300.sh'
ARCHIVE_BASE_DE_1_MD5='1e81fcf9d40bcf23dec0a77069222a52'
ARCHIVE_BASE_DE_1_SIZE='892298'
ARCHIVE_BASE_DE_1_VERSION='81.8193.16-gog41300'

ARCHIVE_BASE_FR_1_NAME='neverwinter_nights_enhanced_edition_french_extras_81_8193_16_41300.sh'
ARCHIVE_BASE_FR_1_MD5='1fe0cc196c146834ff186935ae2d3d66'
ARCHIVE_BASE_FR_1_SIZE='832881'
ARCHIVE_BASE_FR_1_VERSION='81.8193.16-gog41300'

ARCHIVE_BASE_PL_1_NAME='neverwinter_nights_enhanced_edition_polish_extras_81_8193_16_41300.sh'
ARCHIVE_BASE_PL_1_MD5='8ff3b7d282b9a3822425823e9efd545f'
ARCHIVE_BASE_PL_1_SIZE='820690'
ARCHIVE_BASE_PL_1_VERSION='81.8193.16-gog41300'

ARCHIVE_BASE_FR_0_NAME='neverwinter_nights_enhanced_edition_french_extras_80_8193_9_37029.sh'
ARCHIVE_BASE_FR_0_MD5='5e0564a161259b003c7dc0f8d8aa743f'
ARCHIVE_BASE_FR_0_SIZE='840000'
ARCHIVE_BASE_FR_0_VERSION='80.8193.9-gog37029'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
lang/??/data'

PKG_PARENT_ID="$GAME_ID"

PKG_MAIN_ID="${GAME_ID}-l10n"
PKG_MAIN_ID_DE="${PKG_MAIN_ID}-de"
PKG_MAIN_ID_FR="${PKG_MAIN_ID}-fr"
PKG_MAIN_ID_PL="${PKG_MAIN_ID}-pl"
PKG_MAIN_PROVIDES="
$PKG_MAIN_ID"
PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_PARENT'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
