#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# SpringBack
# send your bug reports to contact@dotslashplay.it
###

script_version=20241219.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='springback'
GAME_NAME='SpringBack'

ARCHIVE_BASE_1_NAME='Springback_1.1.429.rar'
ARCHIVE_BASE_1_MD5='13516502d68c29fd38851adf03b82934'
ARCHIVE_BASE_1_SIZE='241965'
ARCHIVE_BASE_1_VERSION='1.1.429-itch1'
ARCHIVE_BASE_1_URL='https://sweet-arsenic.itch.io/springback'

ARCHIVE_BASE_0_NAME='springback-windows-universal.zip'
ARCHIVE_BASE_0_MD5='61b073d967bbd27433baaedc9d97c3e1'
ARCHIVE_BASE_0_SIZE='230000'
ARCHIVE_BASE_0_VERSION='1.0-itch1'

UNITY3D_NAME='SpringBack'

CONTENT_PATH_DEFAULT='Springback_1.1.429'
CONTENT_PATH_DEFAULT_0='.'

WINE_REGEDIT_PERSISTENT_KEYS='
HKEY_CURRENT_USER\Software\Sweet Arsenic\SpringBack'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

## Ignore some harmless errors during the extraction of some *.mdb files
archive_extraction_default 2>/dev/null || true

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
