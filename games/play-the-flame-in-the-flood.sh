#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Flame in the Flood
# send your bug reports to contact@dotslashplay.it
###

script_version=20240610.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='the-flame-in-the-flood'
GAME_NAME='The Flame in the Flood'

ARCHIVE_BASE_0_NAME='setup_the_flame_in_the_flood_1.3.003_(31352).exe'
ARCHIVE_BASE_0_MD5='c58e4985cb0a93a27f6bcd542a313ff1'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='2900000'
ARCHIVE_BASE_0_VERSION='1.3.003-gog31352'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/the_flame_in_the_flood'

UNREALENGINE4_NAME='rivergame'

CONTENT_PATH_DEFAULT='.'

APP_MAIN_EXE='rivergame.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
