#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Hoël Bézier
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Dead Cells
# send your bug reports to contact@dotslashplay.it
###

script_version=20240615.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='dead-cells'
GAME_NAME='Dead Cells'

ARCHIVE_BASE_1_NAME='dead_cells_1_9_1_39495.sh'
ARCHIVE_BASE_1_MD5='d39f39354e1546e9fb940e7738992b16'
ARCHIVE_BASE_1_SIZE='690000'
ARCHIVE_BASE_1_VERSION='1.9.1-gog39495'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/dead_cells'

ARCHIVE_BASE_0_NAME='dead_cells_1_8_0_37766.sh'
ARCHIVE_BASE_0_MD5='93a03894720c4e232566300c61ee153b'
ARCHIVE_BASE_0_SIZE='670000'
ARCHIVE_BASE_0_VERSION='1.8.0-gog37766'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN_FILES='
libhl.so'
CONTENT_GAME_BIN_FILES='
deadcells
*.hdll'
CONTENT_GAME_DATA_FILES='
hlboot.dat
res.pak'

USER_PERSISTENT_DIRECTORIES='
logs
save'

APP_MAIN_EXE='deadcells'
APP_MAIN_ICON='../support/icon.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libmbedtls.so.12
libopenal.so.1
libpng16.so.16
libSDL2-2.0.so.0
libturbojpeg.so.0
libuv.so.1
libvorbisfile.so.3'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
