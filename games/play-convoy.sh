#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2019 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Convoy
# send your bug reports to contact@dotslashplay.it
###

script_version=20231115.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='convoy'
GAME_NAME='Convoy'

ARCHIVE_BASE_3_NAME='convoy_1_1_54_27852.sh'
ARCHIVE_BASE_3_MD5='2f7dd6c597e07638650cc01883e0367f'
ARCHIVE_BASE_3_SIZE='860000'
ARCHIVE_BASE_3_VERSION='1.1.54-gog27852'
ARCHIVE_BASE_3_URL='https://www.gog.com/game/convoy'

ARCHIVE_BASE_2_NAME='convoy_1_1_53_27205.sh'
ARCHIVE_BASE_2_MD5='cda02a99f12adc608a0193f75fc9d7d3'
ARCHIVE_BASE_2_SIZE='860000'
ARCHIVE_BASE_2_VERSION='1.1.53-gog27205'

ARCHIVE_BASE_1_NAME='convoy_1_1_52_26363.sh'
ARCHIVE_BASE_1_MD5='99b331906d75443f08c4f787bc83a7ef'
ARCHIVE_BASE_1_SIZE='860000'
ARCHIVE_BASE_1_VERSION='1.1.52-gog26363'

ARCHIVE_BASE_0='gog_convoy_2.4.0.7.sh'
ARCHIVE_BASE_0_MD5='2d66599173990eb202a43dbc547c80f5'
ARCHIVE_BASE_0_SIZE='860000'
ARCHIVE_BASE_0_VERSION='1.1.51-gog2.4.0.7'

UNITY3D_NAME='Convoy'
UNITY3D_PLUGINS='
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN64'
# shellcheck disable=SC2119
launchers_write
set_current_package 'PKG_BIN32'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
