#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2015 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Heroes of Might and Magic 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20230916.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='heroes-of-might-and-magic-2'
GAME_NAME='Heroes of Might and Magic Ⅱ: The Price of Loyalty'

ARCHIVE_BASE_EN_2_NAME='setup_heroes_of_might_and_magic_2_gold_1.01_(2.1)_(33438).exe'
ARCHIVE_BASE_EN_2_MD5='ff7738a587f10a16116743fe26267f65'
ARCHIVE_BASE_EN_2_TYPE='innosetup'
ARCHIVE_BASE_EN_2_SIZE='590000'
ARCHIVE_BASE_EN_2_VERSION='2.1-gog33438'
ARCHIVE_BASE_EN_2_URL='https://www.gog.com/game/heroes_of_might_and_magic_2_gold_edition'

ARCHIVE_BASE_FR_2_NAME='setup_heroes_of_might_and_magic_2_gold_1.01_(2.1)_(french)_(33438).exe'
ARCHIVE_BASE_FR_2_MD5='b4613fc6b238c83d37247851d5e5a27e'
ARCHIVE_BASE_FR_2_TYPE='innosetup'
ARCHIVE_BASE_FR_2_SIZE='510000'
ARCHIVE_BASE_FR_2_VERSION='2.1-gog33438'
ARCHIVE_BASE_FR_2_URL='https://www.gog.com/game/heroes_of_might_and_magic_2_gold_edition'

ARCHIVE_BASE_EN_1_NAME='setup_heroes_of_might_and_magic_2_gold_1.0_hotfix_(29821).exe'
ARCHIVE_BASE_EN_1_MD5='2dc1cb74c1e8de734fa97fc3d2484212'
ARCHIVE_BASE_EN_1_TYPE='innosetup'
ARCHIVE_BASE_EN_1_SIZE='480000'
ARCHIVE_BASE_EN_1_VERSION='2.1-gog29821'

ARCHIVE_BASE_FR_1_NAME='setup_heroes_of_might_and_magic_2_gold_1.0_hotfix_(french)_(29821).exe'
ARCHIVE_BASE_FR_1_MD5='1b43a2ce13128d77e8f2e40e72635af1'
ARCHIVE_BASE_FR_1_TYPE='innosetup'
ARCHIVE_BASE_FR_1_SIZE='410000'
ARCHIVE_BASE_FR_1_VERSION='2.1-gog29821'

ARCHIVE_BASE_EN_0_NAME='setup_homm2_gold_2.1.0.29.exe'
ARCHIVE_BASE_EN_0_MD5='b6785579d75e47936517a79374b17ebc'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_SIZE='480000'
ARCHIVE_BASE_EN_0_VERSION='2.1-gog2.1.0.29'

ARCHIVE_BASE_FR_0_NAME='setup_homm2_gold_french_2.1.0.29.exe'
ARCHIVE_BASE_FR_0_MD5='c49d8f5d0f6d56e54cf6f9c7a526750f'
ARCHIVE_BASE_FR_0_TYPE='innosetup'
ARCHIVE_BASE_FR_0_SIZE='410000'
ARCHIVE_BASE_FR_0_VERSION='2.1-gog2.1.0.29'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_EN_0='app'
CONTENT_PATH_DEFAULT_FR_0='app'
CONTENT_GAME_MAIN_FILES='
wing.32
homm2.gog
homm2.ins
homm2.inst
data
games
journals
maps
sound
*.exe
*.cfg'
CONTENT_GAME0_MAIN_PATH='sys'
CONTENT_GAME0_MAIN_FILES='
wing32.dll'
CONTENT_DOC_MAIN_FILES='
eula
help
readme.txt
h2camp.txt
polcamp.txt
*.pdf'
CONTENT_GAME_MUSIC_SW_PATH="${CONTENT_PATH_DEFAULT}/music/sw"
CONTENT_GAME_MUSIC_SW_FILES='
*.ogg'
CONTENT_GAME_MUSIC_POL_PATH="${CONTENT_PATH_DEFAULT}/music/pol"
CONTENT_GAME_MUSIC_POL_FILES='
*.ogg'

GAME_IMAGE='homm2.ins'
GAME_IMAGE_TYPE='cdrom'

USER_PERSISTENT_DIRECTORIES='
games
maps'
USER_PERSISTENT_FILES='
data/standard.hs
*.cfg'

APP_MAIN_EXE='heroes2.exe'
APP_MAIN_ICON='app/goggame-1207658785.ico'
APP_MAIN_ICON_EN_0='goggame-1207658785.ico'
APP_MAIN_ICON_FR_0='goggame-1207658785.ico'

APP_EDITOR_ID="${GAME_ID}-editor"
APP_EDITOR_NAME="$GAME_NAME - editor"
APP_EDITOR_EXE='editor2.exe'
APP_EDITOR_ICON='app/goggame-1207658785.ico'
APP_EDITOR_ICON_EN_0='goggame-1207658785.ico'
APP_EDITOR_ICON_FR_0='goggame-1207658785.ico'

PACKAGES_LIST='PKG_MAIN PKG_MUSIC_SW PKG_MUSIC_POL'

PKG_MUSIC_ID="${GAME_ID}-music"

PKG_MUSIC_SW_ID="${PKG_MUSIC_ID}-sw"
PKG_MUSIC_SW_PROVIDES="
$PKG_MUSIC_ID"
PKG_MUSIC_SW_DESCRIPTION='The Succession Wars music'

PKG_MUSIC_POL_ID="${PKG_MUSIC_ID}-pol"
PKG_MUSIC_POL_PROVIDES="
$PKG_MUSIC_ID"
PKG_MUSIC_POL_DESCRIPTION='The Price of Loyalty music'

PKG_MAIN_ID="$GAME_ID"
PKG_MAIN_ID_EN="${PKG_MAIN_ID}-en"
PKG_MAIN_ID_FR="${PKG_MAIN_ID}-fr"
PKG_MAIN_PROVIDES="
$PKG_MAIN_ID"
PKG_MAIN_DESCRIPTION_EN='English version'
PKG_MAIN_DESCRIPTION_FR='French version'
PKG_MAIN_DEPS="$PKG_MUSIC_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Ensure consistent name for disk image
	disk_image_alternative_name='homm2.inst'
	if [ -e "$disk_image_alternative_name" ]; then
		mv "$disk_image_alternative_name" "$GAME_IMAGE"
	fi

	# Copy common music files shared between both soundtracks
	rm \
		'music/homm2_04.ogg' \
		'music/homm2_05.ogg' \
		'music/homm2_06.ogg' \
		'music/homm2_07.ogg' \
		'music/homm2_08.ogg' \
		'music/homm2_09.ogg'
	cp --link 'music'/*.ogg 'music/sw'
	cp --link 'music'/*.ogg 'music/pol'
	rm 'music'/*.ogg
)

# Include game icons

PKG='PKG_MAIN'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion 'GAME_MUSIC_SW' 'PKG_MUSIC_SW' "$(path_game_data)/music"
content_inclusion 'GAME_MUSIC_POL' 'PKG_MUSIC_POL' "$(path_game_data)/music"
content_inclusion_default

# Write launchers

PKG='PKG_MAIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

printf '\n'
printf '%s:' "$(package_description 'PKG_MUSIC_SW')"
print_instructions 'PKG_MAIN' 'PKG_MUSIC_SW'
printf '%s:' "$(package_description 'PKG_MUSIC_POL')"
print_instructions 'PKG_MAIN' 'PKG_MUSIC_POL'

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
