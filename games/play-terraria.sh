#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Terraria
# send your bug reports to contact@dotslashplay.it
###

script_version=20240831.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='terraria'
GAME_NAME='Terraria'

ARCHIVE_BASE_4_NAME='terraria_v1_4_3_6_v2_54359.sh'
ARCHIVE_BASE_4_MD5='da991dc72abb5763f148dacb543d65ca'
ARCHIVE_BASE_4_SIZE='740000'
ARCHIVE_BASE_4_VERSION='1.4.3.6.v2-gog54359'
ARCHIVE_BASE_4_URL='https://www.gog.com/game/terraria'

ARCHIVE_BASE_3_NAME='terraria_v1_4_3_2_51716.sh'
ARCHIVE_BASE_3_MD5='fcc729577dd6a4b9ed2429c624cc6562'
ARCHIVE_BASE_3_SIZE='740000'
ARCHIVE_BASE_3_VERSION='1.4.3.2-gog51716'

ARCHIVE_BASE_2_NAME='terraria_english_v1_4_1_2_42620.sh'
ARCHIVE_BASE_2_MD5='c84d418147004790d97f47c36a1987ba'
ARCHIVE_BASE_2_SIZE='730000'
ARCHIVE_BASE_2_VERSION='1.4.1.2-gog42620'

ARCHIVE_BASE_1_NAME='terraria_v1_4_1_1_41975.sh'
ARCHIVE_BASE_1_MD5='e0158c754f9a7259d28f1cd3c1e1c747'
ARCHIVE_BASE_1_SIZE='720000'
ARCHIVE_BASE_1_VERSION='1.4.1.1-gog41975'

ARCHIVE_BASE_0_NAME='terraria_v1_4_1_0_41944.sh'
ARCHIVE_BASE_0_MD5='6d8fd3976503695205e80ba10e8249de'
ARCHIVE_BASE_0_SIZE='720000'
ARCHIVE_BASE_0_VERSION='1.4.1.0-gog41944'

ARCHIVE_BASE_MULTIARCH_4_NAME='terraria_v1_4_0_5_38805.sh'
ARCHIVE_BASE_MULTIARCH_4_MD5='88940054c5d5a5f556f0bd955559426a'
ARCHIVE_BASE_MULTIARCH_4_SIZE='760000'
ARCHIVE_BASE_MULTIARCH_4_VERSION='1.4.0.5-gog38805'

ARCHIVE_BASE_MULTIARCH_3_NAME='terraria_1_4_0_4_38513.sh'
ARCHIVE_BASE_MULTIARCH_3_MD5='5704d188ab8374f0a36e86bad8adb5a1'
ARCHIVE_BASE_MULTIARCH_3_SIZE='760000'
ARCHIVE_BASE_MULTIARCH_3_VERSION='1.4.0.4-gog38513'

ARCHIVE_BASE_MULTIARCH_2_NAME='terraria_v1_4_0_2_38384.sh'
ARCHIVE_BASE_MULTIARCH_2_MD5='85d3ddcbafdef8412e4f96f3adbc2ed9'
ARCHIVE_BASE_MULTIARCH_2_SIZE='760000'
ARCHIVE_BASE_MULTIARCH_2_VERSION='1.4.0.2-gog38384'

ARCHIVE_BASE_MULTIARCH_1_NAME='terraria_en_1_3_5_3_14602.sh'
ARCHIVE_BASE_MULTIARCH_1_MD5='c99fdc0ae15dbff1e8147b550db4e31a'
ARCHIVE_BASE_MULTIARCH_1_SIZE='490000'
ARCHIVE_BASE_MULTIARCH_1_VERSION='1.3.5.3-gog14602'

ARCHIVE_BASE_MULTIARCH_0_NAME='gog_terraria_2.17.0.21.sh'
ARCHIVE_BASE_MULTIARCH_0_MD5='90ec196ec38a7f7a5002f5a8109493cc'
ARCHIVE_BASE_MULTIARCH_0_SIZE='490000'
ARCHIVE_BASE_MULTIARCH_0_VERSION='1.3.5.3-gog2.17.0.21'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_FILES='
libFNA3D.so.0
libmojoshader.so'
CONTENT_LIBS_LIBS32_PATH="${CONTENT_PATH_DEFAULT}/lib"
CONTENT_LIBS_LIBS32_FILES="$CONTENT_LIBS_FILES"
CONTENT_LIBS_LIBS64_PATH="${CONTENT_PATH_DEFAULT}/lib64"
CONTENT_LIBS_LIBS64_FILES="$CONTENT_LIBS_FILES"
CONTENT_GAME_MAIN_FILES='
Content
Terraria.png
monoconfig
monomachineconfig
open-folder
Terraria.exe
TerrariaServer.exe
FNA.dll
FNA.dll.config
SteelSeriesEngineWrapper.dll'
CONTENT_DOC_MAIN_FILES='
changelog.txt'

APP_MAIN_EXE='Terraria.exe'
APP_MAIN_ICON='Terraria.png'

APP_SERVER_ID="$GAME_ID-server"
APP_SERVER_NAME="$GAME_NAME Server"
APP_SERVER_EXE='TerrariaServer.exe'
APP_SERVER_ICON='Terraria.png'

PACKAGES_LIST='
PKG_MAIN
PKG_LIBS64
PKG_LIBS32'

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_LIBS'
PKG_MAIN_DEPENDENCIES_LIBRARIES='
libFAudio.so.0
libGL.so.1
libopenal.so.1
libSDL2-2.0.so.0'
PKG_MAIN_DEPENDENCIES_LIBRARIES_MULTIARCH='
libGL.so.1
libopenal.so.1
libSDL2-2.0.so.0'
PKG_MAIN_DEPENDENCIES_MONO_LIBRARIES='
mscorlib.dll
Mono.Posix.dll
Mono.Security.dll
System.dll
System.Configuration.dll
System.Core.dll
System.Data.dll
System.Drawing.dll
System.Numerics.dll
System.Runtime.Serialization.dll
System.Security.dll
System.Windows.Forms.dll
System.Xml.dll
System.Xml.Linq.dll
WindowsBase.dll'

PKG_LIBS_ID="${GAME_ID}-libs"
PKG_LIBS64_ID="$PKG_LIBS_ID"
PKG_LIBS32_ID="$PKG_LIBS_ID"
PKG_LIBS_DESCRIPTION='Shipped libraries'
PKG_LIBS64_DESCRIPTION="$PKG_LIBS_DESCRIPTION"
PKG_LIBS32_DESCRIPTION="$PKG_LIBS_DESCRIPTION"
PKG_LIBS64_ARCH='64'
PKG_LIBS32_ARCH='32'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

## Always run the server in a terminal
server_desktop_file=$(launcher_desktop_filepath 'PKG_MAIN' 'APP_SERVER')
cat >> "$server_desktop_file" << EOF
Terminal=true
EOF

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
