#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Race The Sun
# send your bug reports to contact@dotslashplay.it
###

script_version=20231104.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='race-the-sun'
GAME_NAME='Race The Sun'

ARCHIVE_BASE_GOG_0_NAME='gog_race_the_sun_2.4.0.8.sh'
ARCHIVE_BASE_GOG_0_MD5='e3f4e66a5fafe966000ab4e0dcfb7aeb'
ARCHIVE_BASE_GOG_0_SIZE='200000'
ARCHIVE_BASE_GOG_0_VERSION='1.51-gog2.4.0.8'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/race_the_sun'

ARCHIVE_BASE_HUMBLE_0_NAME='RaceTheSunLINUX_1.50.zip'
ARCHIVE_BASE_HUMBLE_0_MD5='e225afb660090b9aa8281574b658accf'
ARCHIVE_BASE_HUMBLE_0_SIZE='190000'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.50-humble170131'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/race-the-sun'

UNITY3D_NAME='RaceTheSun'
## TODO: Check if the Steam libraries can be dropped.
UNITY3D_PLUGINS='
libCSteamworks.so
libsteam_api.so
ScreenSelector.so'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='RaceTheSunLINUX_1.50'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN32_ARCH='32'
PKG_BIN64_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXrandr.so.2
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN64'
# shellcheck disable=SC2119
launchers_write
set_current_package 'PKG_BIN32'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
