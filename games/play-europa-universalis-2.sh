#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Europa Universalis 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20230730.2

GAME_ID='europa-universalis-2'
GAME_NAME='Europa Universalis Ⅱ'

ARCHIVE_BASE_0='setup_europa_universalis2_2.0.0.1.exe'
ARCHIVE_BASE_0_MD5='eb19eab80b45105d571bf684e18118a4'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_VERSION='1.09-gog2.0.0.1'
ARCHIVE_BASE_0_SIZE='430000'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/europa_universalis_ii'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
config
*.cfg
*.dll
*.exe
*.ini
*.tlb'
CONTENT_GAME_DATA_FILES='
ai
avi
db
gfx
map
music
scenarios
sfx
tutorial'
CONTENT_DOC_DATA_FILES='
*.doc
*.pdf
*.txt'

USER_PERSISTENT_DIRECTORIES='
config
scenarios/save games'
USER_PERSISTENT_FILES='
config.eu
history.txt
*.cfg
*.ini'

APP_MAIN_EXE='eu2.exe'

APP_SETTINGS_ID="${GAME_ID}-settings"
APP_SETTINGS_NAME="$GAME_NAME - Settings"
APP_SETTINGS_CAT='Settings'
APP_SETTINGS_EXE='eu2_settings.exe'

APP_EDITOR_ID="${GAME_ID}-editor"
APP_EDITOR_NAME="$GAME_NAME - Scenario editor"
APP_EDITOR_EXE='scenario editor.exe'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
application/x-id3'

# Fix music playback

SCRIPT_DEPS="${SCRIPT_DEPS:-} dos2unix unix2dos"

# Load common functions

target_version='2.24'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Fix music playback
	config_file='music.ini'
	config_field='music'
	config_value="C:\\\\$(game_id)\\\\music"
	sed_pattern="s/^${config_field}=.*$/${config_field}=${config_value}/"
	dos2unix --quiet "$config_file"
	sed --in-place "$sed_pattern" "$config_file"
	unix2dos --quiet "$config_file"
)


# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build package

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
