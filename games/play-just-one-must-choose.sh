#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Just one, must choose
# send your bug reports to contact@dotslashplay.it
###

script_version=20240404.2

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='just-one-must-choose'
GAME_NAME='Just one, must choose'

ARCHIVE_BASE_0_NAME='Just one, must choose - Linux.zip'
ARCHIVE_BASE_0_MD5='1a93360c282cee3b8e11a6f04698a2dd'
ARCHIVE_BASE_0_SIZE='5100'
ARCHIVE_BASE_0_VERSION='1.0-itch.2019.08.04'
ARCHIVE_BASE_0_URL='https://gooonzalo.itch.io/just-one-must-choose'

CONTENT_PATH_DEFAULT='application.linux64'
CONTENT_GAME_MAIN_FILES='
source
lib'

APP_MAIN_TYPE='java'
APP_MAIN_EXE='Only_One'
APP_MAIN_JAVA_OPTIONS='-Djna.nosys=true -Djava.library.path=lib -cp lib/Only_One.jar:lib/core.jar:lib/jogl-all.jar:lib/gluegen-rt.jar:lib/jogl-all-natives-linux-amd64.jar:lib/gluegen-rt-natives-linux-amd64.jar'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# The launcher target is not a .jar archive

launcher_target_presence_check() { true ; }
game_exec_line() {
	cat <<- EOF
	java $APP_MAIN_JAVA_OPTIONS "\$APP_EXE" "\$@"
	EOF
}

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Write launchers

# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
