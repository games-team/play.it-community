#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Settlers 3
# send your bug reports to contact@dotslashplay.it
###

script_version=20240522.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='the-settlers-3'
GAME_NAME='The Settlers Ⅲ'

ARCHIVE_BASE_EN_0_NAME='setup_the_settlers_3_-_ultimate_collection_1.60_v2_(30349).exe'
ARCHIVE_BASE_EN_0_MD5='169a48088443cf5ccf92dcca1b747316'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_SIZE='1400000'
ARCHIVE_BASE_EN_0_VERSION='1.60-gog30349'
ARCHIVE_BASE_EN_0_URL='https://www.gog.com/game/the_settlers_3_ultimate_collection'

ARCHIVE_BASE_DE_0_NAME='setup_the_settlers_3_-_ultimate_collection_1.60_v2_(german)_(30349).exe'
ARCHIVE_BASE_DE_0_MD5='b667efd448e4ef471dd53a38342abc67'
ARCHIVE_BASE_DE_0_TYPE='innosetup'
ARCHIVE_BASE_DE_0_SIZE='1400000'
ARCHIVE_BASE_DE_0_VERSION='1.60-gog30349'
ARCHIVE_BASE_DE_0_URL='https://www.gog.com/game/the_settlers_3_ultimate_collection'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
_inmm.dll
bglwinr.dll
ir50_32.dll
ir50_qc.dll
ir50_qcx.dll
libogg-0.dll
libvorbis-0.dll
libvorbisfile-3.dll
msvcirt.dll
msvcrt.dll
newtoolr.dll
patchw32.dll
fileconvert.exe
s3.exe
s3_multi.exe
setups3.exe'
CONTENT_GAME_L10N_FILES='
manual
s3editor
solution
tips
s3/add
s3/mis_v
s3qota/add
s3qota/mis_v
snd/siedler3_01.dat
data.dat
s3cd1.dat'
CONTENT_GAME_DATA_FILES='
gfx
map
music
s3mcd
s3/mis_m
s3qota/mis_m
snd/siedler3_00.dat
gsdd_lnk.gif
s3cd2.dat'
CONTENT_GAME0_DATA_PATH='app'
CONTENT_GAME0_DATA_FILES='
s3.dat
s3gold1.dat
s3gold2.dat
s3mcd1.dat
s3qota1.dat
solution/_vti_pvt/service.lck
solution/_vti_pvt/structure.cnf'
CONTENT_DOC_DATA_FILES='
the settlers iii - manual.pdf
readme.txt'

USER_PERSISTENT_DIRECTORIES='
save'

## "icodecs" winetricks verb is required for correct intro movie playback.
WINE_WINETRICKS_VERBS="${WINE_WINETRICKS_VERBS:-} icodecs"
## Disable csmt, as it would cause performance issues with single CPU affinity.
WINE_WINETRICKS_VERBS="${WINE_WINETRICKS_VERBS:-} csmt=off"

APP_MAIN_EXE='s3.exe'
APP_MAIN_ICON='s3gold.ico'

PACKAGES_LIST='
PKG_BIN
PKG_L10N
PKG_DATA'

PKG_L10N_ID="${GAME_ID}-l10n"
PKG_L10N_ID_EN="${PKG_L10N_ID}-en"
PKG_L10N_ID_DE="${PKG_L10N_ID}-de"
PKG_L10N_PROVIDES="
$PKG_L10N_ID"
PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_DE='German localization'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_L10N $PKG_DATA_ID"

# Set required registry keys

registry_dump_init_file='registry-dumps/init.reg'
# shellcheck disable=SC1003
registry_dump_init_content='Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\Software\BlueByte\S3Editor]
"Browser"=""
"Editor"=dword:00000001
"EditorPath"="C:\\'"$GAME_ID"'\\S3Editor"
"GameExe"="\\S3.exe"
"Help"="C:\\'"$GAME_ID"'\\S3Editor\\Help\\English\\Index.htm"
"Language"=dword:00000009
"SourceMapDir"="C:\\'"$GAME_ID"'\\S3Editor\\Source\\"
"UserMapDir"="\\Map\\User\\"

[HKEY_LOCAL_MACHINE\Software\BlueByte\S3PLUSCD]
"EditorPathPlus"="\\S3Editor"
"EditorPlus"=dword:00000001
"GameExePlus"="\\S3.EXE"
"HelpPlus"="\\S3Editor\\Help\\English\\Index.htm"
"LanguagePlus"=dword:00000009
"SourceMapDirPlus"="\\S3Editor\\Source\\"
"UserMapDirPlus"="\\Map\\User\\"

[HKEY_LOCAL_MACHINE\Software\BlueByte\Siedler3\1.0\General]
"BannerId"=dword:00000000
"BannerSubId"=dword:00000000
"BuildHelp"=dword:00000001
"Campaigns"=hex:20,72,d5,87,23,9c,bf,aa,b1,14,78,5c,d2,f9,5b,63,13,57,37,5b,7a,\
  65,7b,3d,7b,3b,48,62,fa,a0,77,6c,56,7a,43,1a,81,91,32,a4,df,fb,53,70,e2,48,\
  2b,ed,1b,87,31,b1,d1,00
"CampaignsPlus"=hex:c2,43,d4,d6,2c,17,a2,80,e4,d1,56,8b,a1,94,17,72,d9,5b,9d,\
  40,04,23,cd,69,74,1a,0b,cc,f4,c5,c6,c6,b8,e1,60,d8,b5,ee,16,73,0c,63,59,75,\
  b3,b7,e5,2c,d2,db,bd,b7,20,00
"CDAudio"=dword:00000002
"ClanFlag"=dword:00000000
"FirstLogon"=dword:00000001
"FogSpeed"=dword:00000001
"Font"="MS Sans Serif"
"FontSize"=dword:0000000f
"GameZoneSelection"=dword:000001ff
"GDIMouse"=dword:00000001
"Gold"=dword:00000001
"Intro"=dword:00000001
"Language"=dword:00000009
"MessageLevel"=dword:00000009
"Mission"=dword:00000001
"Newbie"=dword:00000000
"NoAlpha"=dword:00000000
"Playername"="Player"
"Plus"=dword:00000001
"Resolution"=dword:00000000
"ScrollMode"=dword:00000000
"ScrollSpeed"=dword:00000005
"SoundFormat"=dword:00000001
"Tips&Tricks"=dword:00000001
"VideoFormat"=dword:00000001
"WaitVBlank"=dword:00000001

[HKEY_LOCAL_MACHINE\Software\BlueByte\Siedler3\1.0\Patches]
"convert"="0"'
CONTENT_GAME_BIN_FILES="${CONTENT_GAME_BIN_FILES:-}
$registry_dump_init_file"
APP_REGEDIT="${APP_REGEDIT:-} $registry_dump_init_file"
SCRIPT_DEPS="${SCRIPT_DEPS:-} iconv"

# Set serial number

registry_dump_serial_file='registry-dumps/serial.reg'
registry_dump_serial_content='Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\Software\BlueByte\Siedler3\1.0\General]
"SingleNumber"="%SingleNumber%"'
CONTENT_GAME_BIN_FILES="${CONTENT_GAME_BIN_FILES:-}
$registry_dump_serial_file"
APP_REGEDIT="${APP_REGEDIT:-} $registry_dump_serial_file"
SCRIPT_DEPS="${SCRIPT_DEPS:-} iconv"
APP_MAIN_PRERUN="$APP_MAIN_PRERUN
# Set serial number
## For some reason the game erases it on first launch,
## so we reset it prior to each launch.
\$(regedit_command) '$registry_dump_serial_file'"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Include required extra DLL library.
	mv '__support/add/winmmxp.dll' '_inmm.dll'

	## Set required registry keys.
	mkdir --parents "$(dirname "$registry_dump_init_file")"
	printf '%s' "$registry_dump_init_content" | \
		iconv \
		--from-code=UTF-8 --to-code=UTF-16 \
		--output="$registry_dump_init_file"

	## Set serial number.
	game_serial=$(grep 'SingleNumber' 'regs.cmd' | cut --delimiter='"' --fields=4)
	mkdir --parents "$(dirname "$registry_dump_serial_file")"
	printf '%s' "$registry_dump_serial_content" | \
		sed "s/%SingleNumber%/${game_serial}/" | \
		iconv \
		--from-code=UTF-8 --to-code=UTF-16 \
		--output="$registry_dump_serial_file"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

## Set the CPU affinity to a single core, to avoid performance issues.
## cf. https://bugs.winehq.org/show_bug.cgi?id=32478
game_exec_line() {
	cat <<- 'EOF'
	# Set the CPU affinity to a single core, to avoid performance issues.
	# cf. https://bugs.winehq.org/show_bug.cgi?id=32478
	taskset --cpu-list 0 $(wine_command) "$APP_EXE" "$@"
	EOF
}

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
