#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Soma
# send your bug reports to contact@dotslashplay.it
###

script_version=20230216.2

GAME_ID='soma'
GAME_NAME='SOMA'

ARCHIVE_BASE_1='SOMA_Linux_v110.zip'
ARCHIVE_BASE_1_MD5='46e9dadf90d347e0f384e636e71ce746'
ARCHIVE_BASE_1_SIZE='22000000'
ARCHIVE_BASE_1_VERSION='1.10-humble2'
ARCHIVE_BASE_1_URL='https://www.humblebundle.com/store/soma'

ARCHIVE_BASE_0='SOMA_Humble_Linux_1109.zip'
ARCHIVE_BASE_0_MD5='63f4c611fed4df25bee3fb89177ab57f'
ARCHIVE_BASE_0_VERSION='1109-humble1'
ARCHIVE_BASE_0_SIZE='22000000'

CONTENT_PATH_DEFAULT='SOMA'
CONTENT_PATH_DEFAULT_0='Linux'
CONTENT_LIBS_BIN_PATH="${CONTENT_PATH_DEFAULT}/lib64"
CONTENT_LIBS_BIN_PATH_0="${CONTENT_PATH_DEFAULT_0}/lib64"
CONTENT_LIBS_BIN_FILES='
libfmodevent64-4.44.62.so
libfmodex64-4.44.62.so'
CONTENT_GAME_BIN_FILES='
Soma.bin.x86_64'
CONTENT_GAME_ENTITIES_FILES='
entities'
CONTENT_GAME_SOUNDS_FILES='
sounds'
CONTENT_GAME_DATA_FILES='
billboards
combos
config
core
detail_meshes
fonts
graphics
gui
lang
lights
maps
music
particles
script
_shadersource
static_objects
terminals
textures
undergrowth
*.bmp
*.cfg
*.hps
*.rar
*.xml'
CONTENT_DOC_DATA_FILES='
README.linux'

USER_PERSISTENT_FILES='
*.cfg'
USER_PERSISTENT_DIRECTORIES='
config'

APP_MAIN_EXE='Soma.bin.x86_64'
APP_MAIN_ICON='Icon.bmp'

PACKAGES_LIST='PKG_BIN PKG_ENTITIES PKG_SOUNDS PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_ENTITIES_ID="${GAME_ID}-entities"
PKG_ENTITIES_DESCRIPTION='entities'

PKG_SOUNDS_ID="${GAME_ID}-sounds"
PKG_SOUNDS_DESCRIPTION='sounds'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID $PKG_ENTITIES_ID $PKG_SOUNDS_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libGLU.so.1
libIL.so.1
libm.so.6
libogg.so.0
libpthread.so.0
libSDL2-2.0.so.0
libstdc++.so.6
libtheora.so.0
libvorbisfile.so.3
libvorbis.so.0
libz.so.1'

# Load common functions

target_version='2.21'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
