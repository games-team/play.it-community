#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Old World
# send your bug reports to contact@dotslashplay.it
###

script_version=20240808.2

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='old-world'
GAME_NAME='Old World'

ARCHIVE_BASE_4_NAME='setup_old_world_73323_(64bit)_(75059).exe'
ARCHIVE_BASE_4_MD5='85c6f18dab6b3a4afa10f8692ddc37f4'
ARCHIVE_BASE_4_TYPE='innosetup'
ARCHIVE_BASE_4_PART1_NAME='setup_old_world_73323_(64bit)_(75059)-1.bin'
ARCHIVE_BASE_4_PART1_MD5='76088aa2846f040a9f847a4c1d37fefa'
ARCHIVE_BASE_4_SIZE='6770335'
ARCHIVE_BASE_4_VERSION='73323-gog75059'
ARCHIVE_BASE_4_URL='https://www.gog.com/game/old_world'

ARCHIVE_BASE_3_NAME='setup_old_world_63874_(64bit)_(60139).exe'
ARCHIVE_BASE_3_MD5='b437df7671daf99926d7bcff6843348e'
ARCHIVE_BASE_3_TYPE='innosetup'
ARCHIVE_BASE_3_PART1_NAME='setup_old_world_63874_(64bit)_(60139)-1.bin'
ARCHIVE_BASE_3_PART1_MD5='9dc2fcaec2c5e31c5c7256ec978ff5de'
ARCHIVE_BASE_3_PART2_NAME='setup_old_world_63874_(64bit)_(60139)-2.bin'
ARCHIVE_BASE_3_PART2_MD5='8aec1dbb9dea13dc7a9dcf818c51b22c'
ARCHIVE_BASE_3_SIZE='7900000'
ARCHIVE_BASE_3_VERSION='63874-gog60139'

ARCHIVE_BASE_2_NAME='setup_old_world_62874_(64bit)_(58888).exe'
ARCHIVE_BASE_2_MD5='3a99a52001d9a8c610b761a0dbd0eaa8'
ARCHIVE_BASE_2_TYPE='innosetup'
ARCHIVE_BASE_2_PART1_NAME='setup_old_world_62874_(64bit)_(58888)-1.bin'
ARCHIVE_BASE_2_PART1_MD5='6d31bb8588a9a470f8b0fc8560b01d5e'
ARCHIVE_BASE_2_PART2_NAME='setup_old_world_62874_(64bit)_(58888)-2.bin'
ARCHIVE_BASE_2_PART2_MD5='2ced4111b8b6fd2e33e4b76301055bb1'
ARCHIVE_BASE_2_SIZE='7900000'
ARCHIVE_BASE_2_VERSION='62874-gog58888'

ARCHIVE_BASE_1_NAME='setup_old_world_62798_(64bit)_(58769).exe'
ARCHIVE_BASE_1_MD5='4d733db625917e8a837be046ec899749'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_old_world_62798_(64bit)_(58769)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='91d87895e8ffbc1b8d84096df5dc3efb'
ARCHIVE_BASE_1_PART2_NAME='setup_old_world_62798_(64bit)_(58769)-2.bin'
ARCHIVE_BASE_1_PART2_MD5='8e2d4632a8bd92ecf58d8f167740c255'
ARCHIVE_BASE_1_SIZE='7900000'
ARCHIVE_BASE_1_VERSION='62798-gog58769'

ARCHIVE_BASE_0_NAME='setup_old_world_62443_(64bit)_(58285).exe'
ARCHIVE_BASE_0_MD5='9d4c86d9c1de385ed3dcf61faf0fd3c9'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_old_world_62443_(64bit)_(58285)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='f1e9ae7126b81bd023e48003642ae310'
ARCHIVE_BASE_0_PART2_NAME='setup_old_world_62443_(64bit)_(58285)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='1b676c5d29f196efbab51fdac0cade5a'
ARCHIVE_BASE_0_SIZE='7900000'
ARCHIVE_BASE_0_VERSION='62443-gog58285'

UNITY3D_NAME='oldworld'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME0_DATA_FILES='
reference'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/My Games/OldWorld'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
