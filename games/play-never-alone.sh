#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2018 BetaRays
set -o errexit

###
# Never Alone
# send your bug reports to contact@dotslashplay.it
###

script_version=20231111.4

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='never-alone'
GAME_NAME='Never Alone'

ARCHIVE_BASE_0_NAME='NeverAlone_ArcticCollection_Linux.1.04.tar.gz'
ARCHIVE_BASE_0_MD5='3da062abaaa9e3e6ff97d4c82c8ea3c3'
ARCHIVE_BASE_0_SIZE='4900000'
ARCHIVE_BASE_0_VERSION='1.04-humble161008'
ARCHIVE_BASE_0_URL='https://www.humblebundle.com/store/never-alone-arctic-collection'

UNITY3D_NAME='Never_Alone'
UNITY3D_PLUGINS='
ScreenSelector.so'

CONTENT_PATH_DEFAULT='NeverAlone_ArcticCollection_Linux.1.04'
CONTENT_GAME0_BIN_FILES="
${UNITY3D_NAME}.x64"
CONTENT_GAME_VIDEOS_FILES="
${UNITY3D_NAME}_Data/StreamingAssets/Videos"

APP_MAIN_EXE="${UNITY3D_NAME}.x64"

PACKAGES_LIST='
PKG_BIN
PKG_VIDEOS
PKG_DATA'

PKG_VIDEOS_ID="$GAME_ID-videos"
PKG_VIDEOS_DESCRIPTION='videos'

PKG_DATA_ID="$GAME_ID-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID $PKG_VIDEOS_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libGLU.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
