#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Phoning Home
# send your bug reports to contact@dotslashplay.it
###

script_version=20240516.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='phoning-home'
GAME_NAME='Phoning Home'

ARCHIVE_BASE_0_NAME='setup_phoning_home_1.4.1_(21995).exe'
ARCHIVE_BASE_0_MD5='8e3f9102af055a2efc1355156ac0e64a'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1='setup_phoning_home_1.4.1_(21995)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='ff6b6b6b4aea7335addd43fe611adec4'
ARCHIVE_BASE_0_SIZE='6200000'
ARCHIVE_BASE_0_VERSION='1.4.1-gog21995'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/phoning_home'

UNITY3D_NAME='phoninghome'

CONTENT_PATH_DEFAULT='.'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Ion Lands/Phoning Home'
WINE_REGEDIT_PERSISTENT_KEYS='
HKEY_CURRENT_USER\Software\Ion Lands\Phoning Home'
## Install required font.
## Without it, resolution options in the game menu are not shown.
WINE_WINETRICKS_VERBS='arial'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
