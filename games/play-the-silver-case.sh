#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2018 BetaRays
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Silver Case
# send your bug reports to contact@dotslashplay.it
###

script_version=20240622.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='the-silver-case'
GAME_NAME='The Silver Case'

GAME_ID_DEMO="${GAME_ID}-demo"
GAME_NAME_DEMO="$GAME_ID (demo)"

ARCHIVE_BASE_DEMO_GOG_0_NAME='setup_the_silver_case_demo_2.0.0.1.exe'
ARCHIVE_BASE_DEMO_GOG_0_MD5='3c133cb2031160370917f120055c63b4'
ARCHIVE_BASE_DEMO_GOG_0_TYPE='innosetup'
ARCHIVE_BASE_DEMO_GOG_0_SIZE='960000'
ARCHIVE_BASE_DEMO_GOG_0_VERSION='1.0-gog2.0.0.1'

## This installer is no longer available from Playism,
## as they turned their store into yet another Steam keys reseller.
ARCHIVE_BASE_DEMO_PLAYISM_0_NAME='TheSilverCase_DEMO_Playism0930.zip'
ARCHIVE_BASE_DEMO_PLAYISM_0_MD5='a1bbd59ead01c4e1dc50c38b3a65c5ea'
ARCHIVE_BASE_DEMO_PLAYISM_0_SIZE='950000'
ARCHIVE_BASE_DEMO_PLAYISM_0_VERSION='1.0-playism0930'

UNITY3D_NAME_DEMO='thesilvercase_trial'

CONTENT_PATH_DEFAULT_DEMO_GOG='app'
CONTENT_PATH_DEFAULT_DEMO_PLAYISM='thesilvercase_demo_0930'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID_DEMO="${GAME_ID_DEMO}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS_DEMO="$PKG_DATA_ID_DEMO"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Convert all file paths to lowercase.
	case "$(current_archive)" in
		('ARCHIVE_BASE_PLAYISM_'*)
			tolower .
		;;
	esac
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
