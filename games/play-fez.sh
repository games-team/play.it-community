#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Fez
# send your bug reports to contact@dotslashplay.it
###

script_version=20240830.2

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='fez'
GAME_NAME='Fez'

ARCHIVE_BASE_0_NAME='fez-11282016-bin'
ARCHIVE_BASE_0_MD5='333d2e5f55adbd251b09e01d4da213c6'
## This MojoSetup installer is not relying on a Makeself wrapper
ARCHIVE_BASE_0_EXTRACTOR='bsdtar'
ARCHIVE_BASE_0_SIZE='440000'
ARCHIVE_BASE_0_VERSION='1.12-humble161128'
ARCHIVE_BASE_0_URL='https://www.humblebundle.com/store/fez'

CONTENT_PATH_DEFAULT='data'
## Include shipped libraries that can not be replaced by system ones
CONTENT_LIBS_FILES='
libmojoshader.so'
CONTENT_LIBS_LIBS64_PATH="${CONTENT_PATH_DEFAULT}/lib64"
CONTENT_LIBS_LIBS64_FILES="$CONTENT_LIBS_FILES"
CONTENT_LIBS_LIBS32_PATH="${CONTENT_PATH_DEFAULT}/lib"
CONTENT_LIBS_LIBS32_FILES="$CONTENT_LIBS_FILES"
CONTENT_GAME_MAIN_FILES='
Content
monoconfig
FEZ.bmp
Common.dll
ContentSerialization.dll
EasyStorage.dll
FezEngine.dll
FNA.dll
FNA.dll.config
SimpleDefinitionLanguage.dll
XnaWordWrapCore.dll
FEZ.exe
gamecontrollerdb.txt'
CONTENT_DOC_MAIN_FILES='
Linux.README
Changelog.txt
README.txt'

APP_MAIN_EXE='FEZ.exe'
APP_MAIN_ICON='FEZ.bmp'

PACKAGES_LIST='
PKG_MAIN
PKG_LIBS64
PKG_LIBS32'

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_LIBS'
PKG_MAIN_DEPENDENCIES_LIBRARIES='
libogg.so.0
libopenal.so.1
libSDL2-2.0.so.0
libvorbis.so.0
libvorbisfile.so.3'
PKG_MAIN_DEPENDENCIES_MONO_LIBRARIES='
mscorlib.dll
Mono.Posix.dll
Mono.Security.dll
System.dll
System.Configuration.dll
System.Core.dll
System.Data.dll
System.Drawing.dll
System.IO.Compression.dll
System.IO.Compression.FileSystem.dll
System.Security.dll
System.Xml.dll'

PKG_LIBS_ID="${GAME_ID}-libs"
PKG_LIBS64_ID="$PKG_LIBS_ID"
PKG_LIBS32_ID="$PKG_LIBS_ID"
PKG_LIBS_DESCRIPTION='Shipped libraries'
PKG_LIBS64_DESCRIPTION="$PKG_LIBS_DESCRIPTION"
PKG_LIBS32_DESCRIPTION="$PKG_LIBS_DESCRIPTION"
PKG_LIBS64_ARCH='64'
PKG_LIBS32_ARCH='32'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
