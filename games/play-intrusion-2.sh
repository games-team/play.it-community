#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Intrusion 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20231017.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='intrusion-2'
GAME_NAME='Intrusion 2'

ARCHIVE_BASE_0_NAME='intrusion2-1370288626-bin'
ARCHIVE_BASE_0_MD5='de6a30d7fb3d117065f11743b4f0a13a'
ARCHIVE_BASE_0_SIZE='87000'
ARCHIVE_BASE_0_VERSION='1.024-humble1'
ARCHIVE_BASE_0_URL='https://www.humblebundle.com/store/intrusion-2'

FAKE_HOME_PERSISTENT_DIRECTORIES='
intrusion2'

APP_MAIN_EXE='intrusion-2'

PKG_MAIN_ARCH='32'
PKG_MAIN_DEPENDENCIES_LIBRARIES='
libatk-1.0.so.0
libc.so.6
libdl.so.2
libfontconfig.so.1
libfreetype.so.6
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libgdk-x11-2.0.so.0
libgio-2.0.so.0
libglib-2.0.so.0
libgmodule-2.0.so.0
libgobject-2.0.so.0
libgthread-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpango-1.0.so.0
libpangoft2-1.0.so.0
libpthread.so.0
librt.so.1
libSM.so.6
libssl3.so
libstdc++.so.6
libX11.so.6
libXext.so.6
libXinerama.so.1
libXt.so.6
libXtst.so.6'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Check for the presence of extra required archives

ARCHIVE_REQUIRED_GSTREAMER_NAME='gstreamer0.10.tar.gz'
ARCHIVE_REQUIRED_GSTREAMER_MD5='dcd836dfd0dfdf9882557b5a13f89a3c'
ARCHIVE_REQUIRED_GSTREAMER_URL='https://downloads.dotslashplay.it/resources/gstreamer/'
ARCHIVE_REQUIRED_GST_PLUGINS_BASE_NAME='gst-plugins-base0.10.tar.gz'
ARCHIVE_REQUIRED_GST_PLUGINS_BASE_MD5='13e30627414178684de753e5f6105a43'
ARCHIVE_REQUIRED_GST_PLUGINS_BASE_URL='https://downloads.dotslashplay.it/resources/gstreamer/'
archive_initialize_required \
	'ARCHIVE_GSTREAMER' \
	'ARCHIVE_REQUIRED_GSTREAMER'
archive_initialize_required \
	'ARCHIVE_GST_PLUGINS_BASE' \
	'ARCHIVE_REQUIRED_GST_PLUGINS_BASE'

# Check for the presence of extra optional archives

ARCHIVE_OPTIONAL_ICONS_NAME='intrusion-2_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_MD5='f31db2695382a996934f8f8dbb7c0f56'
ARCHIVE_OPTIONAL_ICONS_URL='https://downloads.dotslashplay.it/games/intrusion-2/'
archive_initialize_optional \
	'ARCHIVE_ICONS' \
	'ARCHIVE_OPTIONAL_ICONS'

# Extract game data

archive_extraction 'ARCHIVE_GSTREAMER'
archive_extraction 'ARCHIVE_GST_PLUGINS_BASE'
if archive_is_available 'ARCHIVE_ICONS'; then
	archive_extraction 'ARCHIVE_ICONS'
fi

# Include game data

CONTENT_GSTREAMER_PATH='gstreamer0.10'
CONTENT_GSTREAMER_FILES='
libgstreamer-0.10.so.0
libgstreamer-0.10.so.0.30.0'
CONTENT_GST_PLUGINS_BASE_PATH='gst-plugins-base0.10'
CONTENT_GST_PLUGINS_BASE_FILES='
libgstinterfaces-0.10.so.0
libgstinterfaces-0.10.so.0.25.0'
content_inclusion 'GSTREAMER' "$(current_package)" "$(path_libraries)"
content_inclusion 'GST_PLUGINS_BASE' "$(current_package)" "$(path_libraries)"
if archive_is_available 'ARCHIVE_ICONS'; then
	CONTENT_ICONS_PATH='.'
	CONTENT_ICONS_FILES='
	64x64'
	content_inclusion 'ICONS' "$(current_package)" "$(path_icons)"
fi
install -D --mode=755 \
	"$(archive_path "$(current_archive)")" \
	"$(package_path "$(current_package)")$(path_game_data)/intrusion-2"
rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
