#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Nonsense at Nightfall
# send your bug reports to contact@dotslashplay.it
###

script_version=20240618.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='nonsense-at-nightfall'
GAME_NAME='Nonsense at Nightfall'

ARCHIVE_BASE_0_NAME='Nonsense at Nightfall.zip'
ARCHIVE_BASE_0_MD5='0fc2ea596776428f659a15acfb20dba6'
ARCHIVE_BASE_0_SIZE='41000'
ARCHIVE_BASE_0_VERSION='1.0-itch'
ARCHIVE_BASE_0_URL='https://siegfriedcroes.itch.io/nonsense-at-nightfall'

UNITY3D_NAME='Nonsense at Nightfall'

CONTENT_PATH_DEFAULT='.'

WINE_REGEDIT_PERSISTENT_KEYS='
HKEY_CURRENT_USER\Software\Siegfried Croes\Nonsense at Nightfall'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
