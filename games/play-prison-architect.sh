#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 BetaRays
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Prison Architect
# send your bug reports to contact@dotslashplay.it
###

script_version=20231123.4

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='prison-architect'
GAME_NAME='Prison Architect'

ARCHIVE_BASE_GOG_0_NAME='prison_architect_clink_1_02_30664.sh'
ARCHIVE_BASE_GOG_0_MD5='f261f6121e3fe9ae5023624098d3946d'
ARCHIVE_BASE_GOG_0_SIZE='390000'
ARCHIVE_BASE_GOG_0_VERSION='1.02-gog30664'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/prison_architect'

## This DRM-free archive is no longer available for sell from humblebundle.com,
## they now only sell a Steam key.
ARCHIVE_BASE_HUMBLE_0_NAME='prisonarchitect-clink_1.02-linux.tar.gz'
ARCHIVE_BASE_HUMBLE_0_MD5='ecf4cf68e10069c3c2cb99bcc52ef417'
ARCHIVE_BASE_HUMBLE_0_SIZE='390000'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.02-humble1'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='prisonarchitect-clink_1.0-linux'
CONTENT_LIBS_BIN_FILES='
libpops_api.so'
CONTENT_LIBS_BIN64_PATH_GOG="${CONTENT_PATH_DEFAULT_GOG}/lib64"
CONTENT_LIBS_BIN64_PATH_HUMBLE="${CONTENT_PATH_DEFAULT_HUMBLE}/lib64"
CONTENT_LIBS_BIN64_FILES="$CONTENT_LIBS_BIN_FILES"
CONTENT_LIBS_BIN32_PATH_GOG="${CONTENT_PATH_DEFAULT_GOG}/lib"
CONTENT_LIBS_BIN32_PATH_HUMBLE="${CONTENT_PATH_DEFAULT_HUMBLE}/lib"
CONTENT_LIBS_BIN32_FILES="$CONTENT_LIBS_BIN_FILES"
CONTENT_GAME_BIN64_FILES='
PrisonArchitect.x86_64'
CONTENT_GAME_BIN32_FILES='
PrisonArchitect.i686'
CONTENT_GAME_DATA_FILES='
*.dat'

APP_MAIN_EXE_BIN32='PrisonArchitect.i686'
APP_MAIN_EXE_BIN64='PrisonArchitect.x86_64'
APP_MAIN_ICON_GOG='../support/icon.png'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libGLU.so.1
libm.so.6
libpthread.so.0
libresolv.so.2
libSDL2-2.0.so.0
libstdc++.so.6
libuuid.so.1
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

case "$(current_archive)" in
	('ARCHIVE_BASE_GOG_'*)
		set_current_package 'PKG_DATA'
		# shellcheck disable=SC2119
		icons_inclusion
	;;
	('ARCHIVE_BASE_HUMBLE_'*)
		## No icon is included in the humblebundle.com archive.
	;;
esac
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN64'
# shellcheck disable=SC2119
launchers_write
set_current_package 'PKG_BIN32'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
