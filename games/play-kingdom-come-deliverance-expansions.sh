#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Fabien Givors <captnfab@debian-facile.org>
set -o errexit

###
# Kingdom Come: Deliverance, expansions
# send your bug reports to contact@dotslashplay.it
###

script_version=20240114.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

# Set game-specific variables

GAME_ID='kingdom-come-deliverance'
GAME_NAME='Kingdom Come: Deliverance'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_MAIN_FILES='data
goggame-*'
PKG_MAIN_DEPS="$GAME_ID"

# Archives

## HD Sound Pack
EXPANSION_ID_HD_SOUND="hd-sound"
EXPANSION_NAME_HD_SOUND="HD Sound pack"

ARCHIVE_BASE_HD_SOUND_0_NAME='setup_kingdom_come_deliverance_-_hd_sound_pack_1.9.6-404-504czj3_(62297).exe'
ARCHIVE_BASE_HD_SOUND_0_MD5='2148cf5d68e477f5c4fb32fde94a2cb8'
ARCHIVE_BASE_HD_SOUND_0_TYPE='innosetup'
ARCHIVE_BASE_HD_SOUND_0_SIZE='974736'
ARCHIVE_BASE_HD_SOUND_0_VERSION='1.9.6-404-504czj3'
ARCHIVE_BASE_HD_SOUND_0_URL='https://www.gog.com/fr/game/kingdom_come_deliverance_hd_sound_pack'

## HD Texture Pack
EXPANSION_ID_HD_TEXTURE="hd-texture"
EXPANSION_NAME_HD_TEXTURE="HD Texture pack"

ARCHIVE_BASE_HD_TEXTURE_0_NAME='setup_kingdom_come_deliverance_-_hd_texture_pack_1.9.6-404-504czj3_(62297).exe'
ARCHIVE_BASE_HD_TEXTURE_0_MD5='599a26fd112d65bd0c6bcaeccd47e3c2'
ARCHIVE_BASE_HD_TEXTURE_0_TYPE='innosetup'
ARCHIVE_BASE_HD_TEXTURE_0_SIZE='7089775'
ARCHIVE_BASE_HD_TEXTURE_0_VERSION='1.9.6-404-504czj3'
ARCHIVE_BASE_HD_TEXTURE_0_URL='https://www.gog.com/fr/game/kingdom_come_deliverance_hd_texture_pack'
ARCHIVE_BASE_HD_TEXTURE_0_PART1='setup_kingdom_come_deliverance_-_hd_texture_pack_1.9.6-404-504czj3_(62297)-1.bin'
ARCHIVE_BASE_HD_TEXTURE_0_PART1_MD5='257a19297a6e1ffc202ca9af584dbc17'
ARCHIVE_BASE_HD_TEXTURE_0_PART2='setup_kingdom_come_deliverance_-_hd_texture_pack_1.9.6-404-504czj3_(62297)-2.bin'
ARCHIVE_BASE_HD_TEXTURE_0_PART2_MD5='c3271df85b819e5ed5016e318544fcc5'

## HD Voice FR pack
EXPANSION_ID_HD_VOICE_FR="hd-voice-fr"
EXPANSION_NAME_HD_VOICE_FR="HD Voice pack"

ARCHIVE_BASE_HD_VOICE_FR_0_NAME='setup_kingdom_come_deliverance_-_hd_voice_pack_-_french_1.9.6-404-504czj3_(62297).exe'
ARCHIVE_BASE_HD_VOICE_FR_0_MD5='16178699acfc1ca2b94c385f7e6dc206'
ARCHIVE_BASE_HD_VOICE_FR_0_TYPE='innosetup'
ARCHIVE_BASE_HD_VOICE_FR_0_SIZE='3832027'
ARCHIVE_BASE_HD_VOICE_FR_0_VERSION='1.9.6-404-504czj3'
ARCHIVE_BASE_HD_VOICE_FR_0_URL='https://www.gog.com/fr/game/kingdom_come_deliverance_hd_voice_pack_french'
ARCHIVE_BASE_HD_VOICE_FR_0_PART1='setup_kingdom_come_deliverance_-_hd_voice_pack_-_french_1.9.6-404-504czj3_(62297)-1.bin'
ARCHIVE_BASE_HD_VOICE_FR_0_PART1_MD5='85279184e0e52c810cea55efa9440efc'

## Expansion: A woman's lot
EXPANSION_ID_A_WOMANS_LOT="a-woman-s-lot"
EXPANSION_NAME_A_WOMANS_LOT="A woman's lot"

ARCHIVE_BASE_A_WOMANS_LOT_0_NAME='setup_kingdom_come_deliverance_-_a_womans_lot_1.9.6-404-504czj3_(62297).exe'
ARCHIVE_BASE_A_WOMANS_LOT_0_MD5='08ec99ec6bf0a08bc04004579641da81'
ARCHIVE_BASE_A_WOMANS_LOT_0_TYPE='innosetup'
ARCHIVE_BASE_A_WOMANS_LOT_0_SIZE='8000000'
ARCHIVE_BASE_A_WOMANS_LOT_0_VERSION='1.9.6-404-504czj3'
ARCHIVE_BASE_A_WOMANS_LOT_0_URL='https://www.gog.com/fr/game/kingdom_come_deliverance_a_womans_lot'
ARCHIVE_BASE_A_WOMANS_LOT_0_PART1='setup_kingdom_come_deliverance_-_a_womans_lot_1.9.6-404-504czj3_(62297)-1.bin'
ARCHIVE_BASE_A_WOMANS_LOT_0_PART1_MD5='7cf9ac8abb8e304b756c98a36d6fbc2a'
ARCHIVE_BASE_A_WOMANS_LOT_0_PART2='setup_kingdom_come_deliverance_-_a_womans_lot_1.9.6-404-504czj3_(62297)-2.bin'
ARCHIVE_BASE_A_WOMANS_LOT_0_PART2_MD5='e671d0cfaceb8f5cac910d0e93f2b19f'

## Expansion: Band of bastards
EXPANSION_ID_BAND_OF_BASTARDS="band-of-bastards"
EXPANSION_NAME_BAND_OF_BASTARDS="Band of bastards"

ARCHIVE_BASE_BAND_OF_BASTARDS_0_NAME='setup_kingdom_come_deliverance__band_of_bastards_1.9.6-404-504czj3_(62297).exe'
ARCHIVE_BASE_BAND_OF_BASTARDS_0_MD5='cce9e163f07e2e9fa8be5b6b113fe866'
ARCHIVE_BASE_BAND_OF_BASTARDS_0_TYPE='innosetup'
ARCHIVE_BASE_BAND_OF_BASTARDS_0_SIZE='500000'
ARCHIVE_BASE_BAND_OF_BASTARDS_0_VERSION='1.9.6-404-504czj3'
ARCHIVE_BASE_BAND_OF_BASTARDS_0_URL='https://www.gog.com/fr/game/kingdom_come_deliverance_band_of_bastards'

## Expansion: From the ashes
EXPANSION_ID_FROM_THE_ASHES="from-the-ashes"
EXPANSION_NAME_FROM_THE_ASHES="From the ashes"

ARCHIVE_BASE_FROM_THE_ASHES_0_NAME='setup_kingdom_come_deliverance__from_the_ashes_1.9.6-404-504czj3_(62297).exe'
ARCHIVE_BASE_FROM_THE_ASHES_0_MD5='141360d56c9ba17c72fcac2f50685a7f'
ARCHIVE_BASE_FROM_THE_ASHES_0_TYPE='innosetup'
ARCHIVE_BASE_FROM_THE_ASHES_0_SIZE='100'
ARCHIVE_BASE_FROM_THE_ASHES_0_VERSION='1.9.6-404-504czj3'
ARCHIVE_BASE_FROM_THE_ASHES_0_URL='https://www.gog.com/fr/game/kingdom_come_deliverance_from_the_ashes'

## Expansion: The amorous adventures of bold sir Hans Capon
EXPANSION_ID_HANS_CAPON="amorous-adventures-sir-capon"
EXPANSION_NAME_HANS_CAPON="The amorous adventures of bold sir Hans Capon"

ARCHIVE_BASE_HANS_CAPON_0_NAME='setup_kingdom_come_deliverance__the_amorous_adventures_of_bold_sir_hans_capon_1.9.6-404-504czj3_(62297).exe'
ARCHIVE_BASE_HANS_CAPON_0_MD5='2b41e8147c8046b7b01cfdd3e8079b10'
ARCHIVE_BASE_HANS_CAPON_0_TYPE='innosetup'
ARCHIVE_BASE_HANS_CAPON_0_SIZE='2000000'
ARCHIVE_BASE_HANS_CAPON_0_VERSION='1.9.6-404-504czj3'
ARCHIVE_BASE_HANS_CAPON_0_URL='https://www.gog.com/fr/game/kingdom_come_deliverance_the_amorous_adventures_of_bold_sir_hans_capon'

## Expansion: Theasures of the past
EXPANSION_ID_TREASURES_OF_THE_PAST="treasures-of-the-past"
EXPANSION_NAME_TREASURES_OF_THE_PAST="Treasures of the past"

ARCHIVE_BASE_TREASURES_OF_THE_PAST_0_NAME='setup_kingdom_come_deliverance_treasures_of_the_past_dlc_1.9.6-404-504czj3_(62297).exe'
ARCHIVE_BASE_TREASURES_OF_THE_PAST_0_MD5='4e3bd284e3dc4573690bed957ba78cbe'
ARCHIVE_BASE_TREASURES_OF_THE_PAST_0_TYPE='innosetup'
ARCHIVE_BASE_TREASURES_OF_THE_PAST_0_SIZE='100'
ARCHIVE_BASE_TREASURES_OF_THE_PAST_0_VERSION='1.9.6-404-504czj3'
ARCHIVE_BASE_TREASURES_OF_THE_PAST_0_URL='https://www.gog.com/fr/game/kingdom_come_deliverance_treasures_of_the_past'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
