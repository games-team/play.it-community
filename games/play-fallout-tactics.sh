#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Fallout Tactics
# send your bug reports to contact@dotslashplay.it
###

script_version=20250113.5

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='fallout-tactics'
GAME_NAME='Fallout Tactics'

ARCHIVE_BASE_EN_1_NAME='setup_fallout_tactics_2.1.0.12.exe'
ARCHIVE_BASE_EN_1_MD5='9fc5a3b9e0c6a01187383886776c2889'
ARCHIVE_BASE_EN_1_TYPE='innosetup'
## TODO: Check the actual size, it might differ from the other build
ARCHIVE_BASE_EN_1_SIZE='1827190'
ARCHIVE_BASE_EN_1_VERSION='1.27-gog2.1.0.12'
ARCHIVE_BASE_EN_1_URL='https://www.gog.com/game/fallout_tactics'

ARCHIVE_BASE_EN_0_NAME='setup_fallout_tactics_2.1.0.12.exe'
ARCHIVE_BASE_EN_0_MD5='9cc1d9987d8a2fa6c1cc6cf9837758ad'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_SIZE='1827190'
ARCHIVE_BASE_EN_0_VERSION='1.27-gog2.1.0.12'

ARCHIVE_BASE_FR_0_NAME='setup_fallout_tactics_french_2.1.0.12.exe'
ARCHIVE_BASE_FR_0_MD5='520c29934290910cdeecbc7fcca68f2b'
ARCHIVE_BASE_FR_0_TYPE='innosetup'
ARCHIVE_BASE_FR_0_SIZE='1813158'
ARCHIVE_BASE_FR_0_VERSION='1.27-gog2.1.0.12'
ARCHIVE_BASE_FR_0_URL='https://www.gog.com/game/fallout_tactics'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
bos.exe
bos_hr.exe
fot_hires_patch.exe
ft tools.exe
fttools_hires_patch.exe
tacticslauncher.exe
*.dll
core/*.cfg'
CONTENT_GAME_L10N_FILES='
tactics.cfg
core/campaigns
core/editor
core/entities
core/gui
core/locale
core/movie
core/sound
core/music/custom/readme.txt
core/entities_0.bos
core/gui_0.bos
core/locale_0.bos
core/loc-mis_*.bos
core/mis-core_?.bos'
CONTENT_GAME_DATA_FILES='
bitmaps
miles
fallouttactics.ico
core/missions
core/tables
core/user
core/game.pck
core/*.bos'
CONTENT_DOC_L10N_FILES='
editor_readme.txt
fallout editor end user license agreement.txt
patch readme.txt
readme.txt
*.pdf
*.rtf'

USER_PERSISTENT_DIRECTORIES='
core/user'
USER_PERSISTENT_FILES='
*.cfg'
## The game binaries can be patched by the provided resolution tool.
USER_PERSISTENT_FILES="${USER_PERSISTENT_FILES:-}
bos.exe
ft tools.exe"

## When using a fullscreen resolution higher than the ones supported by the game,
## the mouse cursor is stuck in an invisible box on the top left side of the screen.
## Running the game in a virtual desktop prevents that (WINE 10.0-rc2).
WINE_VIRTUAL_DESKTOP='1024x768'

## FIXME: ./play.it 2.32.0 only builds a list from applications
##        using an identifier matching "APP_xxx",
##        where "xxx" does not include any underscore.
APPLICATIONS_LIST='
APP_MAIN
APP_MAIN_RESOLUTION
APP_TOOLS
APP_TOOLS_RESOLUTION'

APP_MAIN_EXE='bos.exe'
APP_MAIN_ICON='fallouttactics.ico'

APP_MAIN_RESOLUTION_ID="${GAME_ID}-hires"
APP_MAIN_RESOLUTION_NAME="$GAME_NAME - HD patch"
APP_MAIN_RESOLUTION_CAT='Settings'
APP_MAIN_RESOLUTION_EXE='fot_hires_patch.exe'
APP_MAIN_RESOLUTION_ICON='fallouttactics.ico'

APP_TOOLS_ID="${GAME_ID}-tools"
APP_TOOLS_NAME="$GAME_NAME - Toolbox"
APP_TOOLS_EXE='ft tools.exe'
APP_TOOLS_ICON='fallouttactics.ico'

APP_TOOLS_RESOLUTION_ID="${APP_TOOLS_ID}-hires"
APP_TOOLS_RESOLUTION_NAME="$APP_TOOLS_NAME - HD patch"
APP_TOOLS_RESOLUTION_CAT='Settings'
APP_TOOLS_RESOLUTION_EXE='fttools_hires_patch.exe'
APP_TOOLS_RESOLUTION_ICON='fallouttactics.ico'

PACKAGES_LIST='
PKG_BIN
PKG_L10N
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_L10N_BASE
PKG_DATA'

PKG_L10N_BASE_ID="${GAME_ID}-l10n"
PKG_L10N_ID_EN="${PKG_L10N_BASE_ID}-en"
PKG_L10N_ID_FR="${PKG_L10N_BASE_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_BASE_ID"
PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_FR='French localization'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Prevent graphical artifacts on map scrolling (WINE 10.0-rc2)
	sed --in-place --expression='s/{display.bpp} = {.*}/{display.bpp} = {16}/' 'core/bos.cfg'

	# Set the highest supported game resolution by default
	sed_expression='s/{display.height} = {.*}/{display.height} = {768}/'
	sed_expression="${sed_expression};s/{display.width} = {.*}/{display.width} = {1024}/"
	sed --in-place --expression="$sed_expression" 'core/bos.cfg'
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
