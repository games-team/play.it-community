#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Slipways
# send your bug reports to contact@dotslashplay.it
###

script_version=20240325.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='slipways'
GAME_NAME='Slipways'

ARCHIVE_BASE_1_NAME='setup_slipways_1.3-1139-win_(64bit)_(71895).exe'
ARCHIVE_BASE_1_MD5='847e5a084cceb2741bd26684568b4140'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='835016'
ARCHIVE_BASE_1_VERSION='1.3.1139-gog71895'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/slipways'

ARCHIVE_BASE_0_NAME='setup_slipways_1.3-1106-win_(64bit)_(61549).exe'
ARCHIVE_BASE_0_MD5='f14169a08342feaeb87b0420d7b5d65d'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='840000'
ARCHIVE_BASE_0_VERSION='1.3.1106-gog61549'

UNITY3D_NAME='slipways'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME0_DATA_FILES='
data'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Beetlewing/Slipways'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
PKG_BIN_DEPS="${PKG_BIN_DEPS:-} $PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
