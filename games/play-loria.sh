#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Loria
# send your bug reports to contact@dotslashplay.it
###

script_version=20241107.3

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='loria'
GAME_NAME='Loria'

GAME_ID_DEMO="${GAME_ID}-demo"
GAME_NAME_DEMO="$GAME_NAME (demo)"

ARCHIVE_BASE_0_NAME='loria_1_2_0_31834.sh'
ARCHIVE_BASE_0_MD5='3e23b2eeb0df168e73e47c70813e1a75'
ARCHIVE_BASE_0_SIZE='1189146'
ARCHIVE_BASE_0_VERSION='1.2.0-gog31834'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/loria'

ARCHIVE_BASE_DEMO_0_NAME='loria_demo_1_2_0_31834.sh'
ARCHIVE_BASE_DEMO_0_MD5='aeb232dd39397e509b0fab8033108cf4'
ARCHIVE_BASE_DEMO_0_SIZE='948854'
ARCHIVE_BASE_DEMO_0_VERSION='1.2.0-gog31834'
ARCHIVE_BASE_DEMO_0_URL='https://www.gog.com/game/loria_demo'

UNITY3D_NAME='Loria'
UNITY3D_PLUGINS='
ScreenSelector.so'
## TODO: Check if the Steam library is required
UNITY3D_PLUGINS="$UNITY3D_PLUGINS
libsteam_api.so"

CONTENT_PATH_DEFAULT='data/noarch/game'

## Work around a failure to detect the screen resolution on first launch
APP_MAIN_PRERUN='
# Work around a failure to detect the screen resolution on first launch
unity3d_prefs="${HOME}/.config/unity3d/Loria/Loria/prefs"
if [ ! -e "$unity3d_prefs" ]; then
	mkdir --parents "$(dirname "$unity3d_prefs")"
	cat > "$unity3d_prefs" <<- EOF
	<unity_prefs version_major="1" version_minor="1">
	    <pref name="Screenmanager Fullscreen mode" type="int">0</pref>
	    <pref name="Screenmanager Resolution Width" type="int">1024</pref>
	    <pref name="Screenmanager Resolution Height" type="int">768</pref>
	</unity_prefs>
	EOF
fi
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_DEMO="${GAME_ID_DEMO}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
