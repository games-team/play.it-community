#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Starship Titanic
# send your bug reports to contact@dotslashplay.it
###

script_version=20240619.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='starship-titanic'
GAME_NAME='Starship Titanic'

ARCHIVE_BASE_0_NAME='starship_titanic_1_0_53361.sh'
ARCHIVE_BASE_0_MD5='a41b4da053b7d4191e0d864bc52e6a89'
ARCHIVE_BASE_0_SIZE='1400000'
ARCHIVE_BASE_0_VERSION='1.00.42-gog53361'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/starship_titanic'

CONTENT_PATH_DEFAULT='data/noarch/game/data'
CONTENT_GAME_MAIN_FILES='
Assets
newgame.st'
CONTENT_DOC_MAIN_FILES='
*.doc
*.txt'

APP_MAIN_SCUMMID='titanic:titanic'
APP_MAIN_ICON='../../support/icon.png'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
