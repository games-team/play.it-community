#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Overboard
# send your bug reports to contact@dotslashplay.it
###

script_version=20240820.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='overboard'
GAME_NAME='Overboard!'

ARCHIVE_BASE_0_NAME='setup_overboard_1.3.1_(64bit)_(48184).exe'
ARCHIVE_BASE_0_MD5='7b8ad8c5b7325f9d7904880c1b72d064'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='210000'
ARCHIVE_BASE_0_VERSION='1.3.1-gog48184'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/overboard'

UNITY3D_NAME='overboard!'

CONTENT_PATH_DEFAULT='.'

WINE_PERSISTENT_DIRECTORIES='
userdata:users/${USER}/AppData/LocalLow/Inkle Ltd/Overboard!'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
