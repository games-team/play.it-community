#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Strafe
# send your bug reports to contact@dotslashplay.it
###

script_version=20221210.2

GAME_ID='strafe'
GAME_NAME='Strafe'

UNITY3D_NAME='STRAFE'

ARCHIVE_BASE_1='strafe_gold_edition_1_4_38941.sh'
ARCHIVE_BASE_1_MD5='f7e0bd39d0c6527f4205ec9d095de936'
ARCHIVE_BASE_1_TYPE='mojosetup'
ARCHIVE_BASE_1_SIZE='3000000'
ARCHIVE_BASE_1_VERSION='1.4-gog38941'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/strafe'

ARCHIVE_BASE_0='strafe_en_1_2_update_25_04_2018_20318.sh'
ARCHIVE_BASE_0_MD5='3726412cdf951f5d4278a0a557a70db6'
ARCHIVE_BASE_0_TYPE='mojosetup'
ARCHIVE_BASE_0_SIZE='3000000'
ARCHIVE_BASE_0_VERSION='1.2.180425-gog20318'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/strafe'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN_FILES="
${UNITY3D_NAME}_Data/Mono
${UNITY3D_NAME}_Data/Plugins
${UNITY3D_NAME}.x86_64"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data"

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
ld-linux-x86-64.so.2
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXrandr.so.2'

# Load common functions

target_version='2.19'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build package

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
