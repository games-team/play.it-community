#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Janeene "dawnmist" Beeforth
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Surviving Mars expansions:
# - Digital Deluxe Edition Upgrade Pack
# - Stellaris Dome Set
# - Mysteries Resupply Pack
# send your bug reports to contact@dotslashplay.it
###

script_version=20230414.1

GAME_ID='surviving-mars'
GAME_NAME='Surviving Mars'

EXPANSION_ID_DELUXE='digital-deluxe'
EXPANSION_NAME_DELUXE='Digital Deluxe'

EXPANSION_ID_STELLARIS='stellaris-dome-set'
EXPANSION_NAME_STELLARIS='Stellaris Dome Set'

EXPANSION_ID_MYSTERIES='mysteries-resupply-pack'
EXPANSION_NAME_MYSTERIES='Mysteries Resupply Pack'

# Archives

## Digital Deluxe Edition Upgrade Pack

ARCHIVE_BASE_DELUXE_5='surviving_mars_digital_deluxe_edition_upgrade_pack_cernan_update_29871.sh'
ARCHIVE_BASE_DELUXE_5_MD5='d4446a7a747ab2e087b48b241aedc9eb'
ARCHIVE_BASE_DELUXE_5_SIZE='70000'
ARCHIVE_BASE_DELUXE_5_VERSION='1.0-gog29871'
ARCHIVE_BASE_DELUXE_5_URL='https://www.gog.com/game/surviving_mars_digital_deluxe_edition_upgrade_pack'

ARCHIVE_BASE_DELUXE_4='surviving_mars_digital_deluxe_edition_upgrade_pack_sagan_rc3_update_24111.sh'
ARCHIVE_BASE_DELUXE_4_MD5='60cddca455eb1882e0ca7ebf4e26838a'
ARCHIVE_BASE_DELUXE_4_SIZE='66000'
ARCHIVE_BASE_DELUXE_4_VERSION='1.0-gog24111'

ARCHIVE_BASE_DELUXE_3='surviving_mars_digital_deluxe_edition_upgrade_pack_sagan_rc1_update_23676.sh'
ARCHIVE_BASE_DELUXE_3_MD5='7ba5d3ab5626f1a18015b9516adf29af'
ARCHIVE_BASE_DELUXE_3_SIZE='66000'
ARCHIVE_BASE_DELUXE_3_VERSION='1.0-gog23676'

ARCHIVE_BASE_DELUXE_2='surviving_mars_digital_deluxe_edition_upgrade_pack_en_davinci_rc1_22763.sh'
ARCHIVE_BASE_DELUXE_2_MD5='195f0d1a28047112ced2d9cc31df5e52'
ARCHIVE_BASE_DELUXE_2_SIZE='67000'
ARCHIVE_BASE_DELUXE_2_VERSION='1.0-gog22763'

ARCHIVE_BASE_DELUXE_1='surviving_mars_digital_deluxe_edition_upgrade_pack_en_180619_curiosity_hotfix_3_21661.sh'
ARCHIVE_BASE_DELUXE_1_MD5='cef24bda9587c1923139ea0c86df317a'
ARCHIVE_BASE_DELUXE_1_SIZE='66000'
ARCHIVE_BASE_DELUXE_1_VERSION='1.0-gog21661'

ARCHIVE_BASE_DELUXE_0='surviving_mars_digital_deluxe_edition_upgrade_pack_en_180423_opportunity_rc1_20289.sh'
ARCHIVE_BASE_DELUXE_0_MD5='a574de12f4b7f3aa1f285167109bb6a3'
ARCHIVE_BASE_DELUXE_0_SIZE='66000'
ARCHIVE_BASE_DELUXE_0_VERSION='1.0-gog20289'

## Stellaris Dome Set

ARCHIVE_BASE_STELLARIS_2='surviving_mars_stellaris_dome_set_sagan_rc3_update_24111.sh'
ARCHIVE_BASE_STELLARIS_2_MD5='7759d7fa7f9d99a693a828a6c5db601f'
ARCHIVE_BASE_STELLARIS_2_SIZE='4000'
ARCHIVE_BASE_STELLARIS_2_VERSION='1.0-gog24111'
ARCHIVE_BASE_STELLARIS_2_URL='https://www.gog.com/game/surviving_mars_stellaris_dome_set'

ARCHIVE_BASE_STELLARIS_1='surviving_mars_stellaris_dome_set_sagan_rc1_update_23676.sh'
ARCHIVE_BASE_STELLARIS_1_MD5='2b0f7100813779cdd847be15b6599fea'
ARCHIVE_BASE_STELLARIS_1_SIZE='4000'
ARCHIVE_BASE_STELLARIS_1_VERSION='1.0-gog23676'

ARCHIVE_BASE_STELLARIS_0='surviving_mars_stellaris_dome_set_pre_order_dlc_en_180619_curiosity_hotfix_3_21661.sh'
ARCHIVE_BASE_STELLARIS_0_MD5='01ffc529b9a0cc72e5d94830385bf7b9'
ARCHIVE_BASE_STELLARIS_0_SIZE='4000'
ARCHIVE_BASE_STELLARIS_0_VERSION='1.0-gog21661'

## Mysteries Resupply Pack

ARCHIVE_BASE_MYSTERIES_4='surviving_mars_mysteries_resupply_pack_sagan_rc3_update_24111.sh'
ARCHIVE_BASE_MYSTERIES_4_MD5='042fc7152f3ad72e0c121dfb96f617d8'
ARCHIVE_BASE_MYSTERIES_4_SIZE='3100'
ARCHIVE_BASE_MYSTERIES_4_VERSION='1.0-gog24111'
ARCHIVE_BASE_MYSTERIES_4_URL='https://www.gog.com/game/surviving_mars_mysteries_resupply_pack'

ARCHIVE_BASE_MYSTERIES_3='surviving_mars_mysteries_resupply_pack_sagan_rc1_update_23676.sh'
ARCHIVE_BASE_MYSTERIES_3_MD5='e7e96c1384fd795f4a9b69db579524e6'
ARCHIVE_BASE_MYSTERIES_3_SIZE='3100'
ARCHIVE_BASE_MYSTERIES_3_VERSION='1.0-gog23676'

ARCHIVE_BASE_MYSTERIES_2='surviving_mars_mysteries_resupply_pack_en_davinci_rc1_22763.sh'
ARCHIVE_BASE_MYSTERIES_2_MD5='6e83b67c5d368c25092ecb4fd700b5ae'
ARCHIVE_BASE_MYSTERIES_2_SIZE='3100'
ARCHIVE_BASE_MYSTERIES_2_VERSION='1.0-gog22763'

ARCHIVE_BASE_MYSTERIES_1='surviving_mars_mysteries_resupply_pack_en_180619_curiosity_hotfix_3_21661.sh'
ARCHIVE_BASE_MYSTERIES_1_MD5='fd7ef79614de264ac4eb2a1e431d64bf'
ARCHIVE_BASE_MYSTERIES_1_SIZE='2900'
ARCHIVE_BASE_MYSTERIES_1_VERSION='1.0-gog21661'

ARCHIVE_BASE_MYSTERIES_0='surviving_mars_mysteries_resupply_pack_en_curiosity_2_21442.sh'
ARCHIVE_BASE_MYSTERIES_0_MD5='9ca47c2cdb5a41cf8b221dca99783916'
ARCHIVE_BASE_MYSTERIES_0_SIZE='2900'
ARCHIVE_BASE_MYSTERIES_0_VERSION='1.0-gog21442'


CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
DLC'

PKG_MAIN_ID_DELUXE="${GAME_ID}-${EXPANSION_ID_DELUXE}"
PKG_MAIN_ID_STELLARIS="${GAME_ID}-${EXPANSION_ID_STELLARIS}"
PKG_MAIN_ID_MYSTERIES="${GAME_ID}-${EXPANSION_ID_MYSTERIES}"
PKG_MAIN_DESCRIPTION_DELUXE="$EXPANSION_NAME_DELUXE"
PKG_MAIN_DESCRIPTION_STELLARIS="$EXPANSION_NAME_STELLARIS"
PKG_MAIN_DESCRIPTION_MYSTERIES="$EXPANSION_NAME_MYSTERIES"
PKG_MAIN_DEPS="$GAME_ID"

# Load common functions

target_version='2.23'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
