#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Erwann Duclos
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2020 berru <berru@riseup.net>
set -o errexit

###
# Butcher
# send your bug reports to contact@dotslashplay.it
###

script_version=20240528.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='butcher'
GAME_NAME='Butcher'

GAME_ID_DEMO="${GAME_ID}-demo"
GAME_NAME_DEMO="$GAME_NAME (demo)"

## Archives

### Full game

ARCHIVE_BASE_0_NAME='butcher_w_i_m_p_gog_10_33399.sh'
ARCHIVE_BASE_0_MD5='88f2e3d426f8dbcc0b78c53a43063329'
ARCHIVE_BASE_0_SIZE='206121'
ARCHIVE_BASE_0_VERSION='1.0-gog33399'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/butcher'

### Free demo

ARCHIVE_BASE_DEMO_0_NAME='gog_butcher_demo_2.0.0.1.sh'
ARCHIVE_BASE_DEMO_0_MD5='03ed5d89ef38ef10a3318b8da7e62525'
ARCHIVE_BASE_DEMO_0_SIZE='106381'
ARCHIVE_BASE_DEMO_0_VERSION='1.0-gog2.0.0.1'
ARCHIVE_BASE_DEMO_0_URL='https://www.gog.com/game/butcher_demo'


UNITY3D_NAME='Butcher'
UNITY3D_NAME_DEMO='butcher'
UNITY3D_PLUGINS='
libfmod.so
libfmodstudio.so
ScreenSelector.so'
## TODO: Check if the Steam libraries are required.
UNITY3D_PLUGINS="$UNITY3D_PLUGINS
libsteam_api.so
libSteamworksWrapper64.so"

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'
PACKAGES_LIST_DEMO='
PKG_BIN64
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_DEMO="${GAME_ID_DEMO}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPS_DEMO="$PKG_DATA_ID_DEMO"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXrandr.so.2
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

case "$(current_archive)" in
	('ARCHIVE_BASE_DEMO_'*)
		launchers_generation 'PKG_BIN64'
	;;
	(*)
		launchers_generation 'PKG_BIN64'
		launchers_generation 'PKG_BIN32'
	;;
esac

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
