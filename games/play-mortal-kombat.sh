#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Mortal Kombat:
# - Mortal Kombat 1
# - Mortal Kombat 2
# - Mortal Kombat 3
# send your bug reports to contact@dotslashplay.it
###

script_version=20240618.3

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID_MK1='mortal-kombat-1'
GAME_NAME_MK1='Mortal Kombat'

GAME_ID_MK2='mortal-kombat-2'
GAME_NAME_MK2='Mortal Kombat II'

GAME_ID_MK3='mortal-kombat-3'
GAME_NAME_MK3='Mortal Kombat III'

ARCHIVE_BASE_MK1_0_NAME='setup_mortal_kombat_2.0.0.2.exe'
ARCHIVE_BASE_MK1_0_MD5='c4b310a87a7fc021c424e550e979e018'
ARCHIVE_BASE_MK1_0_TYPE='innosetup'
ARCHIVE_BASE_MK1_0_SIZE='37000'
ARCHIVE_BASE_MK1_0_VERSION='1.0-gog2.0.0.2'
ARCHIVE_BASE_MK1_0_URL='https://www.gog.com/game/mortal_kombat_123'

ARCHIVE_BASE_MK2_0_NAME='setup_mortal_kombat2_2.0.0.2.exe'
ARCHIVE_BASE_MK2_0_MD5='fd66922a3c5551077020ba6df2b2446f'
ARCHIVE_BASE_MK2_0_TYPE='innosetup'
ARCHIVE_BASE_MK2_0_SIZE='38000'
ARCHIVE_BASE_MK2_0_VERSION='1.0-gog2.0.0.2'
ARCHIVE_BASE_MK2_0_URL='https://www.gog.com/game/mortal_kombat_123'

ARCHIVE_BASE_MK3_0_NAME='setup_mortal_kombat3_2.0.0.2.exe'
ARCHIVE_BASE_MK3_0_MD5='e9703877b5bd2dfde5b2d65b8586aa6d'
ARCHIVE_BASE_MK3_0_TYPE='innosetup'
ARCHIVE_BASE_MK3_0_SIZE='87000'
ARCHIVE_BASE_MK3_0_VERSION='1.0-gog2.0.0.2'
ARCHIVE_BASE_MK3_0_URL='https://www.gog.com/game/mortal_kombat_123'

CONTENT_PATH_DEFAULT_MK1='app/mk1'
CONTENT_PATH_DEFAULT_MK2='app/mk2'
CONTENT_PATH_DEFAULT_MK3='app/mk3'
CONTENT_GAME_MAIN_FILES_MK1='
graphics
sfx
*.bat
*.dig
*.dat
*.drv
*.exe
*.gra
*.ini
*.lst
*.mk1
*.pif'
CONTENT_GAME_MAIN_FILES_MK2='
fx
sound
cmos
*.bat
*.bin
*.drv
*.exe
*.gra
*.ini'
CONTENT_GAME_MAIN_FILES_MK3='
drivers
image
*.asg
*.bat
*.dat
*.exe
*.ftr
*.ini
*.mk3'
CONTENT_DOC_MAIN_FILES_MK2='
readme.txt'

USER_PERSISTENT_FILES_MK1='
cmos.mk1
*.ini'
USER_PERSISTENT_FILES_MK2='
cmos
*.ini'
USER_PERSISTENT_FILES_MK3='
*.dat
*.ini'

GAME_IMAGE_MK3='image/mk3.cue'

APP_MAIN_EXE_MK1='mk1.exe'
APP_MAIN_EXE_MK2='mk2.exe'
APP_MAIN_EXE_MK3='mk3.exe'
APP_MAIN_ICON_MK1='../goggame-1207667043.ico'
APP_MAIN_ICON_MK2='../goggame-1207667053.ico'
APP_MAIN_ICON_MK3='../goggame-1207667063.ico'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Mortal Kombat 3 - Ensure case consistency between the disk image table of contents and the actual filenames.
	case "$(current_archive)" in
		('ARCHIVE_BASE_MK3_'*)
			sed_pattern='MK3\.GOG'
			sed_replacement='mk3.gog'
			sed_expression="s/${sed_pattern}/${sed_replacement}/"
			sed_pattern='Track'
			sed_replacement='track'
			sed_expression="${sed_expression};s/${sed_pattern}/${sed_replacement}/g"
			sed --in-place --expression="$sed_expression" "$GAME_IMAGE_MK3"
		;;
	esac
)

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

## Work around ./play.it 2.29 lack of context support for $GAME_IMAGE.
GAME_IMAGE=$(context_value 'GAME_IMAGE')
launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
