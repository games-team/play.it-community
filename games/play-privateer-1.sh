#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Privateer
# send your bug reports to contact@dotslashplay.it
###

script_version=20240806.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='privateer-1'
GAME_NAME='Privateer'

ARCHIVE_BASE_1_NAME='setup_wing_commander_privateer_1.0_(28045).exe'
ARCHIVE_BASE_1_MD5='482b990445b335ecf7f47ee18efccc14'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='180000'
ARCHIVE_BASE_1_VERSION='1.0-gog28045'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/wing_commander_privateer'

ARCHIVE_BASE_0_NAME='setup_wing_commander_privateer_2.0.0.9.exe'
ARCHIVE_BASE_0_MD5='53c77040cba69a642ec1302b5cf231b5'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='190000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.0.0.9'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_MAIN_FILES='
GAME.GOG
*.CFG
*.DAT
*.EXE
*.NDA
*.OVL
*.PAK
*.VDA'
CONTENT_DOC_MAIN_FILES='
*.PDF'

GAME_IMAGE='GAME.GOG'
GAME_IMAGE_TYPE='iso'

USER_PERSISTENT_FILES='
*.CFG
*.IFF'

APP_MAIN_EXE='PRIV.EXE'
APP_MAIN_ICON='APP/GOGGAME-1207658938.ICO'
APP_MAIN_ICON_0='GFW_HIGH.ICO'
## Work around sound problems in the intro video
APP_MAIN_PRERUN='
# Work around sound problems in the intro video
export DOSBOX_SBLASTER_IRQ=5
'

APP_RF_ID="${GAME_ID}-righteous-fire"
APP_RF_NAME="${GAME_NAME} - Righteous Fire"
APP_RF_EXE='PRIV.EXE'
APP_RF_EXE_OPTIONS='r'
APP_RF_ICON='APP/GOGGAME-1207658938.ICO'
APP_RF_ICON_0='GFW_HIGH.ICO'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Convert all file paths to upper case
	toupper .
)

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
