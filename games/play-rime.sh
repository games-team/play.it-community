#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Rime
# send your bug reports to contact@dotslashplay.it
###

script_version=20230821.1

GAME_ID='rime'
GAME_NAME='RiME'

ARCHIVE_BASE_1='setup_rime_1.04_(a)_(40568).exe'
ARCHIVE_BASE_1_MD5='04c89d5de10df3c16c5e9b3512d559aa'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1='setup_rime_1.04_(a)_(40568)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='aa40d7bf862088c92b9a0dbe09c42012'
ARCHIVE_BASE_1_PART2='setup_rime_1.04_(a)_(40568)-2.bin'
ARCHIVE_BASE_1_PART2_MD5='c9cad9754731585b3e68a305eef4bd01'
ARCHIVE_BASE_1_SIZE='7800000'
ARCHIVE_BASE_1_VERSION='1.04-gog40568'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/rime'

ARCHIVE_BASE_0='setup_rime_152498_signed_(14865).exe'
ARCHIVE_BASE_0_MD5='303d41314564c753fcef92260c3e20f8'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1='setup_rime_152498_signed_(14865)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='ea9fc9eeaeeb2d7c58eab42cef31bb2e'
ARCHIVE_BASE_0_PART2='setup_rime_152498_signed_(14865)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='b65792a122d267cc799e9b044605c1a1'
ARCHIVE_BASE_0_SIZE='8000000'
ARCHIVE_BASE_0_VERSION='1.04-gog14865'

UNREALENGINE4_NAME='sirengame'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'

APP_MAIN_EXE='sirengame/binaries/win64/rime.exe'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

target_version='2.25'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
