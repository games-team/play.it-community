#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Iconoclasts
# send your bug reports to contact@dotslashplay.it
###

script_version=20240708.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='iconoclasts'
GAME_NAME='Iconoclasts'

ARCHIVE_BASE_0_NAME='iconoclasts_1_15_chinese_24946.sh'
ARCHIVE_BASE_0_MD5='f93af13b81659e76a953dfff584d6fc9'
ARCHIVE_BASE_0_SIZE='187633'
ARCHIVE_BASE_0_VERSION='1.15-gog.24946'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/iconoclasts'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN64_FILES='
bin64'
CONTENT_GAME_BIN32_FILES='
bin32'
CONTENT_GAME_DATA_FILES='
data
Assets.dat
gamecontrollerdb.txt
icon.bmp'

USER_PERSISTENT_FILES='
lang.cfg
data/point
data/settings
data/save*'

APP_MAIN_EXE_BIN32='bin32/Chowdren'
APP_MAIN_EXE_BIN64='bin64/Chowdren'
APP_MAIN_ICON='icon.bmp'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libGL.so.1
libm.so.6
libpthread.so.0'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
