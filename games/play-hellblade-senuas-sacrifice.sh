#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Hellblade: Senua's Sacrifice
# send your bug reports to contact@dotslashplay.it
###

script_version=20240611.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='hellblade-senuas-sacrifice'
GAME_NAME='Hellblade: Senuaʼs Sacrifice'

ARCHIVE_BASE_1_NAME='setup_hellblade_senuas_sacrifice_1.03.1.202112071122_(52018).exe'
ARCHIVE_BASE_1_MD5='56e77d9dfc1a2cc871488a9a92fc2280'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_hellblade_senuas_sacrifice_1.03.1.202112071122_(52018)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='0f8df5e451d8bbe1aea86947f271d2d6'
ARCHIVE_BASE_1_PART2_NAME='setup_hellblade_senuas_sacrifice_1.03.1.202112071122_(52018)-2.bin'
ARCHIVE_BASE_1_PART2_MD5='a06d8b220be8acd257c5f4b2a16332c6'
ARCHIVE_BASE_1_PART3_NAME='setup_hellblade_senuas_sacrifice_1.03.1.202112071122_(52018)-3.bin'
ARCHIVE_BASE_1_PART3_MD5='fd53c212060d9b76de3ad04465cdd980'
ARCHIVE_BASE_1_PART4_NAME='setup_hellblade_senuas_sacrifice_1.03.1.202112071122_(52018)-4.bin'
ARCHIVE_BASE_1_PART4_MD5='f8ed203da482970d81fa0c6e09471a14'
ARCHIVE_BASE_1_SIZE='20000000'
ARCHIVE_BASE_1_VERSION='1.03.1-gog52018'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/hellblade_senuas_sacrifice_pack'

ARCHIVE_BASE_0_NAME='setup_hellblade_senuas_sacrifice_1.03_(25168).exe'
ARCHIVE_BASE_0_MD5='0568c6e5c57dd64cc0a23a77fe54aafd'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_hellblade_senuas_sacrifice_1.03_(25168)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='b01d26d7555d26f2dc1cb8a361564cb7'
ARCHIVE_BASE_0_PART2_NAME='setup_hellblade_senuas_sacrifice_1.03_(25168)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='8e7e4e73fa6a535a4856005be7ea8cbb'
ARCHIVE_BASE_0_PART3_NAME='setup_hellblade_senuas_sacrifice_1.03_(25168)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='fcabee54e6f1072cbdbd46eb2a8ca0f8'
ARCHIVE_BASE_0_PART4_NAME='setup_hellblade_senuas_sacrifice_1.03_(25168)-4.bin'
ARCHIVE_BASE_0_PART4_MD5='6fce92bde8bb15b0e706a7030874a3a9'
ARCHIVE_BASE_0_SIZE='23000000'
ARCHIVE_BASE_0_VERSION='1.03-gog25168'

UNREALENGINE4_NAME='hellbladegame'

CONTENT_PATH_DEFAULT='.'

HUGE_FILES_DATA="
${UNREALENGINE4_NAME}/content/paks/hellbladegame-windowsnoeditor.pak"

APP_MAIN_EXE="${UNREALENGINE4_NAME}.exe"

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
