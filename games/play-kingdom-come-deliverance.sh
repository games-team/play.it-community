#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Fabien Givors <captnfab@debian-facile.org>
set -o errexit

###
# Kingdom Come: Deliverance
# send your bug reports to contact@dotslashplay.it
###

script_version=20240219.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='kingdom-come-deliverance'
GAME_NAME='Kingdom Come: Deliverance'

ARCHIVE_BASE_0_NAME='setup_kingdom_come_deliverance_1.9.6-404-504czj3_(62297).exe'
ARCHIVE_BASE_0_MD5='de458c20cbcaadf8dfc92afe7c2e8284'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='62899673'
ARCHIVE_BASE_0_VERSION='1.9.6-404-504czj3-gog62297'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/kingdom_come_deliverance'
ARCHIVE_BASE_0_PART1='setup_kingdom_come_deliverance_1.9.6-404-504czj3_(62297)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='c18fa417f8486275d78e01c1a62c8204'
ARCHIVE_BASE_0_PART2='setup_kingdom_come_deliverance_1.9.6-404-504czj3_(62297)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='d288e846badd8228ba0f78c6186731f2'
ARCHIVE_BASE_0_PART3='setup_kingdom_come_deliverance_1.9.6-404-504czj3_(62297)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='b6702c24ded1f8851cb297553cc9b71b'
ARCHIVE_BASE_0_PART4='setup_kingdom_come_deliverance_1.9.6-404-504czj3_(62297)-4.bin'
ARCHIVE_BASE_0_PART4_MD5='b33e86210161017d399071fcf6c54f9f'
ARCHIVE_BASE_0_PART5='setup_kingdom_come_deliverance_1.9.6-404-504czj3_(62297)-5.bin'
ARCHIVE_BASE_0_PART5_MD5='379faac2d6128f1a94275b9d9248c0de'
ARCHIVE_BASE_0_PART6='setup_kingdom_come_deliverance_1.9.6-404-504czj3_(62297)-6.bin'
ARCHIVE_BASE_0_PART6_MD5='c5ffb38b7425cb0219fe70497bb09d9a'
ARCHIVE_BASE_0_PART7='setup_kingdom_come_deliverance_1.9.6-404-504czj3_(62297)-7.bin'
ARCHIVE_BASE_0_PART7_MD5='594f01d2cbd5b2c5cc9442d644a8b335'
ARCHIVE_BASE_0_PART8='setup_kingdom_come_deliverance_1.9.6-404-504czj3_(62297)-8.bin'
ARCHIVE_BASE_0_PART8_MD5='93896ac78ea2dddeb433ffa685a561c2'
ARCHIVE_BASE_0_PART9='setup_kingdom_come_deliverance_1.9.6-404-504czj3_(62297)-9.bin'
ARCHIVE_BASE_0_PART9_MD5='b8cd2810be97f2f697f67e92c71c16b8'
ARCHIVE_BASE_0_PART10='setup_kingdom_come_deliverance_1.9.6-404-504czj3_(62297)-10.bin'
ARCHIVE_BASE_0_PART10_MD5='a38ed45d417c2d2cef03b270a21da255'
ARCHIVE_BASE_0_PART11='setup_kingdom_come_deliverance_1.9.6-404-504czj3_(62297)-11.bin'
ARCHIVE_BASE_0_PART11_MD5='671cc5add7a31e5efd46e2a03478cf5d'
ARCHIVE_BASE_0_PART12='setup_kingdom_come_deliverance_1.9.6-404-504czj3_(62297)-12.bin'
ARCHIVE_BASE_0_PART12_MD5='9aec35e9d193044603095eb1961d8647'
ARCHIVE_BASE_0_PART13='setup_kingdom_come_deliverance_1.9.6-404-504czj3_(62297)-13.bin'
ARCHIVE_BASE_0_PART13_MD5='cb65a86dd5cc63a66a18767c3a4c5378'
ARCHIVE_BASE_0_PART14='setup_kingdom_come_deliverance_1.9.6-404-504czj3_(62297)-14.bin'
ARCHIVE_BASE_0_PART14_MD5='701d947164a1f4d541c985a7273b9552'
ARCHIVE_BASE_0_PART15='setup_kingdom_come_deliverance_1.9.6-404-504czj3_(62297)-15.bin'
ARCHIVE_BASE_0_PART15_MD5='e3bb56e79d9ff4f9be2ee2b321f10f68'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
patchversion.txt
whdlversions.txt
bin
engine
system.cfg
goggame-*
'

CONTENT_GAME_L10N_COMMON_FILES='
localization/*_xml.pak
localization/common.pak'
CONTENT_GAME_L10N_VOICES_CZ_FILES='
localization/*czech-*.pak
localization/patch/*czech*.pak'
CONTENT_GAME_L10N_VOICES_JA_FILES='
localization/*japanese-*.pak
localization/patch/*japanese*.pak'
CONTENT_GAME_L10N_VOICES_DE_FILES='
localization/*german.pak
localization/patch/*german*.pak'
CONTENT_GAME_L10N_VOICES_EN_FILES='
localization/*english.pak
localization/patch/*english*.pak'
CONTENT_GAME_L10N_VOICES_FR_FILES='
localization/*french.pak
localization/patch/*french*.pak'

CONTENT_GAME_DATA_BASE_FILES='
data/gamedata.pak
data/pak.cfg'

CONTENT_GAME_DATA_PATCH103_FILES='
data/patch/ipl_patch_010300.pak
data/patch/patch_010300.pak'

CONTENT_GAME_DATA_PATCH104_FILES='
data/patch/ipl_patch_010400.pak
data/patch/ipl_patch_010403.pak'

CONTENT_GAME_DATA_PATCH105_FILES='
data/patch/ipl_patch_010500.pak
data/patch/patch_010500.pak'

CONTENT_GAME_DATA_PATCH106_FILES='
data/patch/ipl_patch_010600.pak
data/patch/ipl_patch_010602.pak
data/patch/patch_010600.pak'

CONTENT_GAME_DATA_PATCH107_FILES='
data/patch/ipl_patch_010700b.pak
data/patch/ipl_patch_010700.pak
data/patch/ipl_patch_010701b.pak
data/patch/ipl_patch_010701.pak
data/patch/patch_010700.pak
data/patch/patch_010701.pak'

CONTENT_GAME_DATA_PATCH108_FILES='
data/patch/ipl_patch_010800.pak
data/patch/patch_010900.pak'

CONTENT_GAME_DATA_PATCH109_FILES='
data/patch/ipl_patch_010900.pak
data/patch/ipl_patch_010902.pak
data/patch/patch_010800.pak'

CONTENT_GAME_DATA_WORLD_FILES='
data/_fastload/startup_graphics.pak
data/buildings.pak
data/engineassets
data/engineassets/sky
data/engineassets/sky/optical.lut
data/geomcaches.pak
data/levels
data/objects.pak
data/scripts.pak
data/tables.pak
data/textures.pak'

CONTENT_GAME_DATA_AUDIO_FILES='
data/music.pak
data/sounds.pak'

CONTENT_GAME_DATA_VIDEOS_FILES='
data/ipl_videos.pak
data/videos-part0.pak
data/videos-part1.pak
data/videos-part2.pak
data/videos-part3.pak
data/videos-part4.pak'

CONTENT_GAME_DATA_VIDEOSB_FILES='
data/ipl_videosb.pak
data/videosb-part0.pak
data/videosb-part1.pak'

CONTENT_GAME_DATA_CINEMATICS_FILES='
data/cinematics.pak
data/ipl_cinematics.pak'

CONTENT_GAME_DATA_CLOTHES_FILES='
data/cloth-part0.pak
data/cloth-part1.pak
data/cloth-part2.pak'

CONTENT_GAME_DATA_CHARACTERS_FILES='
data/_fastload/startup_characters.pak
data/characters.pak
data/heads.pak
data/ipl_heads.pak'

CONTENT_GAME_DATA_ANIMATIONS_FILES='
data/_fastload/startup_animations.pak
data/animations-part0.pak
data/animations-part1.pak'

APP_MAIN_EXE='bin/win64/kingdomcome.exe'
## Run the game binary from its parent directory
APP_MAIN_PRERUN='# Run the game binary from its parent directory
cd "$(dirname "$APP_EXE")"
APP_EXE=$(basename "$APP_EXE")
'

WINE_DIRECT3D_RENDERER='dxvk'

PACKAGES_LIST='
PKG_BIN
PKG_L10N_COMMON
PKG_L10N_VOICES_EN
PKG_L10N_VOICES_FR
PKG_L10N_VOICES_CZ
PKG_L10N_VOICES_JA
PKG_L10N_VOICES_DE
PKG_DATA_BASE
PKG_DATA_PATCH103
PKG_DATA_PATCH104
PKG_DATA_PATCH105
PKG_DATA_PATCH106
PKG_DATA_PATCH107
PKG_DATA_PATCH108
PKG_DATA_PATCH109
PKG_DATA_WORLD
PKG_DATA_AUDIO
PKG_DATA_VIDEOS
PKG_DATA_VIDEOSB
PKG_DATA_ANIMATIONS
PKG_DATA_CINEMATICS
PKG_DATA_CLOTHES
PKG_DATA_CHARACTERS'

# Data package

PKG_DATA_BASE_ID="${GAME_ID}-data-base"
PKG_DATA_BASE_DESCRIPTION='data-base'

PKG_DATA_WORLD_ID="${GAME_ID}-data-world"
PKG_DATA_WORLD_DESCRIPTION='data world'

PKG_DATA_AUDIO_ID="${GAME_ID}-data-audio"
PKG_DATA_AUDIO_DESCRIPTION='data audio'

PKG_DATA_VIDEOS_ID="${GAME_ID}-data-videos"
PKG_DATA_VIDEOS_DESCRIPTION='data videos'

PKG_DATA_VIDEOSB_ID="${GAME_ID}-data-videosb"
PKG_DATA_VIDEOSB_DESCRIPTION='data videosb'

PKG_DATA_CHARACTERS_ID="${GAME_ID}-data-characters"
PKG_DATA_CHARACTERS_DESCRIPTION='data characters'

PKG_DATA_CLOTHES_ID="${GAME_ID}-data-clothes"
PKG_DATA_CLOTHES_DESCRIPTION='data clothes'

PKG_DATA_CINEMATICS_ID="${GAME_ID}-data-cinematics"
PKG_DATA_CINEMATICS_DESCRIPTION='data cinematics'

PKG_DATA_ANIMATIONS_ID="${GAME_ID}-data-animations"
PKG_DATA_ANIMATIONS_DESCRIPTION='data animations'

PKG_DATA_PATCH103_ID="${GAME_ID}-data-patch-103"
PKG_DATA_PATCH103_DESCRIPTION='data patch 103'

PKG_DATA_PATCH104_ID="${GAME_ID}-data-patch-104"
PKG_DATA_PATCH104_DESCRIPTION='data patch 104'

PKG_DATA_PATCH105_ID="${GAME_ID}-data-patch-105"
PKG_DATA_PATCH105_DESCRIPTION='data patch 105'

PKG_DATA_PATCH106_ID="${GAME_ID}-data-patch-106"
PKG_DATA_PATCH106_DESCRIPTION='data patch 106'

PKG_DATA_PATCH107_ID="${GAME_ID}-data-patch-107"
PKG_DATA_PATCH107_DESCRIPTION='data patch 107'

PKG_DATA_PATCH108_ID="${GAME_ID}-data-patch-108"
PKG_DATA_PATCH108_DESCRIPTION='data patch 108'

PKG_DATA_PATCH109_ID="${GAME_ID}-data-patch-109"
PKG_DATA_PATCH109_DESCRIPTION='data patch 109'

PKG_L10N_COMMON_ID="${GAME_ID}-l10n-common"
PKG_L10N_COMMON_DESCRIPTION='localizations - shared files'

PKG_L10N_VOICES_ID="${GAME_ID}-l10n-voices"
PKG_L10N_VOICES_DESCRIPTION="localizations - voices"

PKG_L10N_VOICES_CZ_ID="${PKG_L10N_VOICES_ID}-cz"
PKG_L10N_VOICES_DE_ID="${PKG_L10N_VOICES_ID}-de"
PKG_L10N_VOICES_EN_ID="${PKG_L10N_VOICES_ID}-en"
PKG_L10N_VOICES_FR_ID="${PKG_L10N_VOICES_ID}-fr"
PKG_L10N_VOICES_JA_ID="${PKG_L10N_VOICES_ID}-ja"
PKG_L10N_VOICES_CZ_DESCRIPTION="$PKG_L10N_VOICES_DESCRIPTION - Czech"
PKG_L10N_VOICES_DE_DESCRIPTION="$PKG_L10N_VOICES_DESCRIPTION - Deutch"
PKG_L10N_VOICES_EN_DESCRIPTION="$PKG_L10N_VOICES_DESCRIPTION - English"
PKG_L10N_VOICES_FR_DESCRIPTION="$PKG_L10N_VOICES_DESCRIPTION - French"
PKG_L10N_VOICES_JA_DESCRIPTION="$PKG_L10N_VOICES_DESCRIPTION - Japanese"

# Binaries package

PKG_BIN_ARCH='64'
PKG_L10N_COMMON_DEPS="$PKG_L10N_VOICES_EN_ID $PKG_L10N_VOICES_FR_ID $PKG_L10N_VOICES_CZ_ID $PKG_L10N_VOICES_JA_ID $PKG_L10N_VOICES_DE_ID"
PKG_DATA_BASE_DEPS="$PKG_DATA_PATCH103_ID $PKG_DATA_PATCH104_ID $PKG_DATA_PATCH105_ID $PKG_DATA_PATCH106_ID $PKG_DATA_PATCH107_ID $PKG_DATA_PATCH108_ID $PKG_DATA_PATCH109_ID $PKG_DATA_WORLD_ID $PKG_DATA_AUDIO_ID $PKG_DATA_VIDEOS_ID $PKG_DATA_VIDEOSB_ID $PKG_DATA_ANIMATIONS_ID $PKG_DATA_CINEMATICS_ID $PKG_DATA_CLOTHES_ID $PKG_DATA_CHARACTERS_ID"
PKG_BIN_DEPS="$PKG_L10N_COMMON_ID $PKG_DATA_BASE_ID"

# Use persistent storage for user data

WINE_PERSISTENT_DIRECTORIES='
users/$USER/Saved Games/kingdomcome/profiles
users/$USER/Saved Games/kingdomcome/saves'

USER_PERSISTENT_FILES='
system.cfg
user.cfg.keeptextures
user.cfg.perf
user.cfg.reticle
user.cfg'

USER_PERSISTENT_DIRECTORIES='
mods'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA_BASE'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
