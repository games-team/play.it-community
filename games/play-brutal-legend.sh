#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Brütal Legend
# send your bug reports to contact@dotslashplay.it
###

script_version=20240622.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='brutal-legend'
GAME_NAME='Brütal Legend'

ARCHIVE_BASE_GOG_0_NAME='gog_brutal_legend_2.0.0.3.sh'
ARCHIVE_BASE_GOG_0_MD5='f5927fb8b3959c52e2117584475ffe49'
ARCHIVE_BASE_GOG_0_SIZE='8800000'
ARCHIVE_BASE_GOG_0_VERSION='1.0-gog2.0.0.3'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/brutal_legend'

## This installer used to be sold at Humble Bundle,
## but they now sell Steam keys only.
ARCHIVE_BASE_HUMBLE_0_NAME='BrutalLegend-Linux-2013-06-15-setup.bin'
ARCHIVE_BASE_HUMBLE_0_MD5='cbda6ae12aafe20a76f4d45367430d32'
## The Humble Bundle installer looks like some custom MojoSetup installer,
## but it does not seem to be based on Makeself like the ones we usually work with.
## We force its extraction using unzip, ignoring errors due to the installer not being a real .zip archive.
ARCHIVE_BASE_HUMBLE_0_EXTRACTOR='unzip'
ARCHIVE_BASE_HUMBLE_0_SIZE='8800000'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.0-humble130616'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='data'
CONTENT_LIBS_BIN_PATH_GOG="${CONTENT_PATH_DEFAULT_GOG}/lib"
CONTENT_LIBS_BIN_PATH_HUMBLE="${CONTENT_PATH_DEFAULT_HUMBLE}/lib"
CONTENT_LIBS_BIN_FILES='
libfmodevent-4.42.16.so
libfmodeventnet-4.42.16.so
libfmodex-4.42.16.so
libsteam_api.so'
CONTENT_GAME_BIN_FILES='
Buddha.bin.x86
DFCONFIG'
CONTENT_GAME_AUDIO_FILES='
Win'
CONTENT_GAME_DATA_FILES='
Buddha.png
Data
Linux
OGL'

USER_PERSISTENT_FILES='
DFCONFIG'

APP_MAIN_EXE='Buddha.bin.x86'
APP_MAIN_ICON='Buddha.png'

PACKAGES_LIST='
PKG_AUDIO
PKG_DATA
PKG_BIN'

PKG_AUDIO_ID="${GAME_ID}-audio"
PKG_AUDIO_DESCRIPTION='audio'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_AUDIO_ID $PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libGLU.so.1
libm.so.6
libpthread.so.0
libSDL2-2.0.so.0
libstdc++.so.6
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

case "$(current_archive)" in
	('ARCHIVE_BASE_HUMBLE_'*)
		## The Humble Bundle installer looks like some custom MojoSetup installer,
		## but it does not seem to be based on Makeself like the ones we usually work with.
		## We force its extraction using unzip, ignoring errors due to the installer not being a real .zip archive.
		archive_extraction_default 2>/dev/null || true
	;;
	(*)
		archive_extraction_default
	;;
esac

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
