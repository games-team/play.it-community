#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Daguhh
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Elephantasy
# send your bug reports to contact@dotslashplay.it
###

script_version=20230328.2

GAME_ID='elephantasy'
GAME_NAME='Elephantasy'

ARCHIVE_BASE_0='Elephantasy Release Build_Itch.zip'
ARCHIVE_BASE_0_MD5='d0a7deaccf9b5fbbd3f3c879adb61a6a'
ARCHIVE_BASE_0_SIZE='35000'
ARCHIVE_BASE_0_VERSION='1.6-itch.2021.04.24'
ARCHIVE_BASE_0_URL='https://linker.itch.io/elephantasy'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
d3dx9_43.dll
elephant.exe
options.ini'
CONTENT_GAME_DATA_FILES='
data.win'
CONTENT_DOC_DATA_FILES='
README.txt'

WINE_PERSISTENT_DIRECTORIES='
userdata:users/${USER}/Local Settings/Application Data/Elephant'

APP_MAIN_EXE='elephant.exe'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# The .zip archive contains a .exe file,
# that is actually a cabinet archive.

ARCHIVE_INNER_TYPE='cabinet'

SCRIPT_DEPS="$SCRIPT_DEPS cabextract"

# Load common functions

target_version='2.23'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'
## The .zip archive contains a .exe file,
## that is actually a cabinet archive.
ARCHIVE_INNER="${PLAYIT_WORKDIR}/gamedata/Elephantasy.exe"
archive_extraction 'ARCHIVE_INNER'
rm "$ARCHIVE_INNER"

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
