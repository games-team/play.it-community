#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 BetaRays
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Q.U.B.E. 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20240404.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='qube-2'
GAME_NAME='Q.U.B.E. 2'

ARCHIVE_BASE_1_NAME='setup_q.u.b.e._2_1.8_(64bit)_(33108).exe'
ARCHIVE_BASE_1_MD5='38e53c42845f1ea44ca2939d2d48571b'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1='setup_q.u.b.e._2_1.8_(64bit)_(33108)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='838dbb204cd6eca9488d502ec16f28d0'
ARCHIVE_BASE_1_SIZE='3800000'
ARCHIVE_BASE_1_VERSION='1.8-gog33108'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/qube_2'

ARCHIVE_BASE_0_NAME='setup_q.u.b.e._2_1.6_with_overlay_(64bit)_(23818).exe'
ARCHIVE_BASE_0_MD5='b62f5e18bff2e9abcb591e1921538989'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1='setup_q.u.b.e._2_1.6_with_overlay_(64bit)_(23818)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='7a397b6e7c20a13c00af2ee964952609'
ARCHIVE_BASE_0_SIZE='3700000'
ARCHIVE_BASE_0_VERSION='1.6-gog23818'

UNREALENGINE4_NAME='qube'

CONTENT_PATH_DEFAULT='.'

APP_MAIN_EXE="${UNREALENGINE4_NAME}/binaries/win64/${UNREALENGINE4_NAME}-win64-shipping.exe"

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
## TODO: Check that native libopenal.so.1 is actually required.
PKG_BIN_DEPENDENCIES_LIBRARIES='
libopenal.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

## Display the post-installation warning about wine build flags on Gentoo.
option_package=$(option_value 'package')
case "$option_package" in
	('gentoo'|'egentoo')
		PKG_BIN_POSTINST_WARNINGS_GENTOO='You might need to compile wine with the openal or faudio USE flag'
	;;
esac

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
