#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Pokemon Insurgence
# send your bug reports to contact@dotslashplay.it
###

script_version=20240516.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='pokemon-insurgence'
GAME_NAME='Pokemon Insurgence'

ARCHIVE_BASE_0_NAME='Pokemon Insurgence 1.2.5 Core.zip'
ARCHIVE_BASE_0_MD5='d992c31fe67ffc0a72c4274caaa4a977'
ARCHIVE_BASE_0_SIZE='840000'
ARCHIVE_BASE_0_VERSION='1.2.5-insurgence1'
ARCHIVE_BASE_0_URL='https://p-insurgence.com/'

CONTENT_PATH_DEFAULT='Pokemon Insurgence 1.2.5 Core'
CONTENT_GAME_BIN_FILES='
Game.exe
Game.ini
*.dll'
CONTENT_GAME_DATA_FILES='
Audio
Data
Egglocke
Eggs
Fonts
Graphics
Game.rgssad'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Saved Games/Pokemon Insurgence'
## Ensure availability of fonts expected by the game engine.
WINE_WINETRICKS_VERBS='arial'

APP_MAIN_EXE='Game.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
audio/mpeg, mpegversion=(int)1, layer=(int)3'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
