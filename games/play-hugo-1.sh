#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Anna Lea
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Hugo 1: Hugo's House of Horrors
# send your bug reports to contact@dotslashplay.it
###

script_version=20240811.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='hugo-1'
GAME_NAME='Hugo I: Hugo’s House of Horrors'

ARCHIVE_BASE_0_NAME='the_hugo_trilogy_en_gog_2_19789.sh'
ARCHIVE_BASE_0_MD5='48a853bb5936e29bf22cb1d4ae8fcfdb'
ARCHIVE_BASE_0_SIZE='100000'
ARCHIVE_BASE_0_VERSION='2-gog19789'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/the_hugo_trilogy'

CONTENT_PATH_DEFAULT='data/noarch/data/hwin_1'
CONTENT_GAME_MAIN_FILES='
*.bsf
*.dat'
CONTENT_DOC_MAIN_FILES='
*.txt'
CONTENT_DOC0_MAIN_PATH='data/noarch/docs'
CONTENT_DOC0_MAIN_FILES='
*.txt'

APP_MAIN_SCUMMID='hugo:hugo1'
APP_MAIN_ICON='../../support/icon.png'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Convert all file paths to lowercase.
	tolower .
)

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "${PLAYIT_WORKDIR}"

exit 0
