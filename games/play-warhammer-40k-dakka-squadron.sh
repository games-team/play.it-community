#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Warhammer 40,000: Dakka Squadron
# send your bug reports to contact@dotslashplay.it
###

script_version=20240516.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='warhammer-40k-dakka-squadron'
GAME_NAME='Warhammer 40,000: Dakka Squadron'

ARCHIVE_BASE_0_NAME='setup_warhammer_40000_dakka_squadron_-_flyboyz_edition_1.154277.shipping_(64bit)_(46795).exe'
ARCHIVE_BASE_0_MD5='746ce7f112d089a2aca8dd6d4458dd26'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_warhammer_40000_dakka_squadron_-_flyboyz_edition_1.154277.shipping_(64bit)_(46795)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='f32a0390b27c406063fcf7ad3bff4655'
ARCHIVE_BASE_0_PART2_NAME='setup_warhammer_40000_dakka_squadron_-_flyboyz_edition_1.154277.shipping_(64bit)_(46795)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='9c978a8867a9747039bcfceb462d0a2e'
ARCHIVE_BASE_0_PART3_NAME='setup_warhammer_40000_dakka_squadron_-_flyboyz_edition_1.154277.shipping_(64bit)_(46795)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='ffb1a9b6e5343e6cb8955d6c8814ef97'
ARCHIVE_BASE_0_SIZE='9200000'
ARCHIVE_BASE_0_VERSION='1.154277-gog46795'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/warhammer_40000_dakka_squadron'

UNREALENGINE4_NAME='dakkagame'

CONTENT_PATH_DEFAULT='.'

## Work around a crash related to shaders compilation
## Without these native libraries, the game crashes with the following error:
## SlateD3DShaders::CompileShader() - D3DCompilerFunc Result: DXGI_ERROR_80004005
WINE_WINETRICKS_VERBS='d3dcompiler_43 d3dcompiler_47'

APP_MAIN_EXE='dakkagame/binaries/win64/dakkagame-win64-shipping.exe'
APP_MAIN_ICON_WRESTOOL_OPTIONS='--type=14 --name=123 --language=1033'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
