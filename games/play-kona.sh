#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Kona
# send your bug reports to contact@dotslashplay.it
###

script_version=20240509.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='kona'
GAME_NAME='Kona'

ARCHIVE_BASE_0_NAME='gog_kona_2.8.0.9.sh'
ARCHIVE_BASE_0_MD5='62e924fb4b9cafcb34c58b2fb66bc9f7'
ARCHIVE_BASE_0_TYPE='mojosetup'
ARCHIVE_BASE_0_SIZE='4400000'
ARCHIVE_BASE_0_VERSION='2017.07.07-gog2.8.0.9'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/kona_day_one'

UNITY3D_NAME='Kona'
UNITY3D_PLUGINS='
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'

## Force the game to run in a window on first launch, to prevent display problems.
## TODO: We might have to force windowed mode on every launch, not only the first one.
APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}"'
# Force the game to run in a window on first launch, to prevent display problems
config_file="$HOME/.config/unity3d/Parabole/Kona/prefs"
if [ ! -e "$config_file" ]; then
	mkdir --parents "$(dirname "$config_file")"
	cat > "$config_file" <<- EOF
	<unity_prefs version_major="1" version_minor="1">
	        <pref name="Fullscreen" type="int">0</pref> 
	        <pref name="Screenmanager Is Fullscreen mode" type="int">0</pref>
	</unity_prefs>
	EOF
fi'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
