#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 HS-157
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Binding of Isaac
# send your bug reports to contact@dotslashplay.it
###

script_version=20241106.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='the-binding-of-isaac'
GAME_NAME='The Binding of Isaac: Rebirth'

ARCHIVE_BASE_1_NAME='setup_the_binding_of_isaac_rebirth_1.0.1.0_(54718).exe'
ARCHIVE_BASE_1_MD5='2e30949d6bc26c25692403daec6de446'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='380000'
ARCHIVE_BASE_1_VERSION='1.0.1.0-gog54718'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/the_binding_of_isaac_rebirth'

ARCHIVE_BASE_0_NAME='setup_the_binding_of_isaac_rebirth_1.0.0.0_(52089).exe'
ARCHIVE_BASE_0_MD5='b65dc20d9f6cef55fdecd39a1a59380c'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='360000'
ARCHIVE_BASE_0_VERSION='1.0.0.0-gog52089'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
*.dll
*.exe'
CONTENT_GAME_DATA_FILES='
resources'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/My Games/Binding of Isaac Rebirth (Galaxy)
users/${USER}/Documents/My Games/Binding of Isaac Afterbirth (Galaxy)
users/${USER}/Documents/My Games/Binding of Isaac Afterbirth+ (Galaxy)
users/${USER}/Documents/My Games/Binding of Isaac Repentance (Galaxy)'
## Install required OpenAL32.dll library
WINE_WINETRICKS_VERBS='openal'

APP_MAIN_EXE='isaac-ng_launcher.exe'
APP_MAIN_ICON='isaac-ng_rebirth.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Prevent the inclusion of unwanted files
	## FIXME: This could be avoided by using a more targeted list of .dll files to include
	rm --recursive \
		'__redist' \
		'commonappdata' \
		'tmp'
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
