#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Zodiacats
# send your bug reports to contact@dotslashplay.it
###

script_version=20240808.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='zodiacats'
GAME_NAME='Zodiacats'

ARCHIVE_BASE_0_NAME='setup_zodiacats_1.0003_(64bit)_(62419).exe'
ARCHIVE_BASE_0_MD5='ad9c318bba0464f6bcdf3079ebd8f1c9'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='270000'
ARCHIVE_BASE_0_VERSION='1.0003-gog62419'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/zodiacats'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_MAIN_FILES='
zodiacats.exe'
## TODO: Check if the Steam library is required
CONTENT_GAME0_MAIN_FILES='
steam_api64.dll'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Roaming/Godot/app_userdata/Zodiacats'

APP_MAIN_EXE='zodiacats.exe'
## The .exe file is too big, wrestool fails to handle it with:
## wrestool: (…)/gamedata/zodiacats.exe: premature end
APP_MAIN_ICON='app/goggame-1435098189.ico'

PKG_MAIN_ARCH='64'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
