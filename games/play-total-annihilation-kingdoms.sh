#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 HS-157
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Total Annihilation: Kingdoms
# send your bug reports to contact@dotslashplay.it
###

script_version=20241203.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='total-annihilation-kingdoms'
GAME_NAME='Total Annihilation: Kingdoms'

ARCHIVE_BASE_0_NAME='setup_total_annihilation_kingdoms_2.0.0.22.exe'
ARCHIVE_BASE_0_MD5='206f4b8e9159414ee38ec609831907bb'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='1100000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.0.0.22'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/total_annihilation_kingdoms'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
*.asi
*.dll
*.exe
*.tsk'
CONTENT_GAME_DATA_FILES='
atlas
boneyards
docs
gc
maps
movies
music
*.256
*.esk
*.hpi
*.htm
*.icd
*.id
*.isu
*.key
*.mmz
*.pdf
*.tdf
*.txt'

APP_MAIN_EXE='kingdoms.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
