#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Noita
# send your bug reports to contact@dotslashplay.it
###

script_version=20240110.6

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='noita'
GAME_NAME='Noita'

ARCHIVE_BASE_0_NAME='setup_noita_20231221-_050_(69862).exe'
ARCHIVE_BASE_0_MD5='106a6244f5d69eccfb690e2a5d5e3100'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='1446784'
ARCHIVE_BASE_0_VERSION='2023.12.21-gog69862'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/noita'

CONTENT_PATH_DEFAULT='.'
## TODO: Check if the Galaxy library is required.
CONTENT_GAME_BIN_FILES='
tools_modding
fmod.dll
fmodstudio.dll
galaxy.dll
lua51.dll
msvcp120.dll
msvcr120.dll
sdl2.dll
noita.exe
noita_dev.exe
config.xml'
CONTENT_GAME_DATA_FILES='
data
mods'
CONTENT_DOC_DATA_FILES='
licenses
_branch.txt
_release_notes.txt
_version_hash.txt
readme.txt
screenshot_paths.txt'

USER_PERSISTENT_DIRECTORIES='
mods'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Nolla_Games_Noita'
## The game will fail to start unless Microsoft Visual C++ 2013 libraries are available.
WINE_WINETRICKS_VERBS='vcrun2013'

APP_MAIN_EXE='noita.exe'
APP_MAIN_ICON='data/icon.bmp'
## The game crashes on launch when the Wayland backend of SDL is used.
APP_MAIN_PRERUN='# The game crashes on launch when the Wayland backend of SDL is used
if [ "${SDL_VIDEODRIVER:-}" = "wayland" ]; then
	unset SDL_VIDEODRIVER
fi
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Convert icon to standard resolutions

SCRIPT_DEPS="${SCRIPT_DEPS:-} convert"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Convert icon to standard resolutions.
	icon_source=$(icon_path 'APP_MAIN_ICON')
	convert "$icon_source" -resize 64x64 'icon_64.png'
	convert "$icon_source" -resize 96x96 'icon_96.png'
)

# Include game data

## Convert icon to standard resolutions.
APP_MAIN_ICON_64='icon_64.png'
APP_MAIN_ICON_96='icon_96.png'
APP_MAIN_ICONS_LIST="$(application_icons_list 'APP_MAIN')
APP_MAIN_ICON_64
APP_MAIN_ICON_96"

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
