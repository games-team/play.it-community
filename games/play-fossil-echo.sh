#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Fossil Echo
# send your bug reports to contact@dotslashplay.it
###

script_version=20240808.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='fossil-echo'
GAME_NAME='Fossil Echo'

ARCHIVE_BASE_0_NAME='FossilEcho_setup.zip'
ARCHIVE_BASE_0_MD5='079e62f826016c1d3edd35e8fe40bb3a'
ARCHIVE_BASE_0_SIZE='2900000'
ARCHIVE_BASE_0_VERSION='1.0-itch1'
ARCHIVE_BASE_0_URL='https://awaceb.itch.io/fossilecho'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
datatypes.dll
fmod.dll
fmodstudio.dll
fossilecho.exe'
CONTENT_GAME_DATA_FILES='
content
data
level
media'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/SavedGames/fossilecho'
## Install the Microsoft XNA 4.0 runtime
WINE_WINETRICKS_VERBS='xna40'
## Do not disable the mscoree library, as it is required by the Microsoft XNA 4.0 runtime
WINE_DLLOVERRIDES_DEFAULT='winemenubuilder.exe,mshtml='

USER_PERSISTENT_DIRECTORIES='
data
level'

APP_MAIN_EXE='fossilecho.exe'
## Handle this binary with WINE, not with Mono
APP_MAIN_TYPE='wine'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
ARCHIVE_INNER_PATH="${PLAYIT_WORKDIR}/gamedata/FossilEcho_setup.exe"
ARCHIVE_INNER_TYPE='innosetup'
archive_extraction 'ARCHIVE_INNER'
rm --force "$(archive_path 'ARCHIVE_INNER')"

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
