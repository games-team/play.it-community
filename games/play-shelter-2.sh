#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Shelter 2:
# - Shelter 2 (base game)
# - Shelter 2 + Mountains expansion
# send your bug reports to contact@dotslashplay.it
###

script_version=20240611.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='shelter-2'
GAME_NAME='Shelter 2'

ARCHIVE_BASE_0_NAME='gog_shelter_2_2.5.0.10.sh'
ARCHIVE_BASE_0_MD5='f2bf2e188667133ad117b5bff846e66e'
ARCHIVE_BASE_0_SIZE='2200000'
ARCHIVE_BASE_0_VERSION='20150708-gog2.5.0.10'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/shelter_2'

ARCHIVE_BASE_MOUNTAINS_0_NAME='gog_shelter_2_mountains_dlc_2.0.0.1.sh'
ARCHIVE_BASE_MOUNTAINS_0_MD5='ffe25b4ac5d75b9a30ed983634397d85'
ARCHIVE_BASE_MOUNTAINS_0_SIZE='2500000'
ARCHIVE_BASE_MOUNTAINS_0_VERSION='1.0-gog2.0.0.1'
ARCHIVE_BASE_MOUNTAINS_0_URL='https://www.gog.com/game/shelter_2_mountains'

UNITY3D_NAME='Shelter2'
UNITY3D_PLUGINS='
ScreenSelector.so'
## TODO: Check if the Steam libraries are required.
UNITY3D_PLUGINS="$UNITY3D_PLUGINS
libCSteamworks.so
libsteam_api.so"

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libGLU.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
