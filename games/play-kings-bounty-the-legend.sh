#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# King's Bounty: The Legend
# send your bug reports to contact@dotslashplay.it
###

script_version=20240811.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='kings-bounty-the-legend'
GAME_NAME='King’s Bounty: The Legend'

ARCHIVE_BASE_0_NAME='setup_kings_bounty_the_legend_1.7.35.398_(39945).exe'
ARCHIVE_BASE_0_MD5='1357aae6054406ce9d3d77b0601b045a'
## "--collisions rename-all" is used to extract all localized files, not only the English ones.
ARCHIVE_BASE_0_EXTRACTOR='innoextract'
ARCHIVE_BASE_0_EXTRACTOR_OPTIONS='--collisions rename-all --lowercase'
ARCHIVE_BASE_0_PART1_NAME='setup_kings_bounty_the_legend_1.7.35.398_(39945)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='4647d2dd1486833c135393e2ca1c0d2e'
ARCHIVE_BASE_0_SIZE='6500991'
ARCHIVE_BASE_0_VERSION='1.7.35.398-gog39945'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/kings_bounty_the_legend'

ARCHIVE_BASE_EN_0_NAME='setup_kings_bounty_the_legend_1.7_(15542).exe'
ARCHIVE_BASE_EN_0_MD5='f7a9defe0fd96a7f8d6dff6ed7828242'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_PART1_NAME='setup_kings_bounty_the_legend_1.7_(15542)-1.bin'
ARCHIVE_BASE_EN_0_PART1_MD5='04fb818107e4bfe7aeae449778e88dd9'
ARCHIVE_BASE_EN_0_SIZE='6000000'
ARCHIVE_BASE_EN_0_VERSION='1.7-gog15542'

ARCHIVE_BASE_FR_0_NAME='setup_kings_bounty_the_legend_french_1.7_(15542).exe'
ARCHIVE_BASE_FR_0_MD5='646fdfacadc498826be127fe6703f259'
ARCHIVE_BASE_FR_0_TYPE='innosetup'
ARCHIVE_BASE_FR_0_PART1_NAME='setup_kings_bounty_the_legend_french_1.7_(15542)-1.bin'
ARCHIVE_BASE_FR_0_PART1_MD5='907882679fb7050e172994d36730454a'
ARCHIVE_BASE_FR_0_SIZE='6000000'
ARCHIVE_BASE_FR_0_VERSION='1.7-gog15542'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_EN='app'
CONTENT_PATH_DEFAULT_FR='app'
CONTENT_GAME_BIN_FILES='
kb.exe
data/default.ini
data/game.ini
data/fonts.cfg
*.dll'
CONTENT_GAME_L10N_FILES='
data/app.ini
data/loc_data.kfs
sessions/base/loc_ses.kfs'
CONTENT_GAME_L10N_EN_FILES='
data/app.ini@en-US
data/loc_data.kfs@en-US
sessions/base/loc_ses.kfs@en-US'
CONTENT_GAME_L10N_FR_FILES='
data/app.ini@fr-FR
data/loc_data.kfs@fr-FR
sessions/base/loc_ses.kfs@fr-FR'
CONTENT_GAME_DATA_FILES='
data
sessions
curver.txt'
CONTENT_DOC_L10N_FILES='
readme.rtf
manual.pdf'
CONTENT_DOC_L10N_EN_FILES='
manual.pdf@en-US
readme.rtf@en-US'
CONTENT_DOC_L10N_FR_FILES='
manual.pdf@fr-FR'

USER_PERSISTENT_FILES='
data/*.ini
data/fonts.cfg'

WINE_DIRECT3D_RENDERER='dxvk'

APP_MAIN_EXE='kb.exe'

PACKAGES_LIST='
PKG_BIN
PKG_L10N_EN
PKG_L10N_FR
PKG_DATA'
PACKAGES_LIST_EN='
PKG_BIN
PKG_L10N
PKG_DATA'
PACKAGES_LIST_FR='
PKG_BIN
PKG_L10N
PKG_DATA'

PKG_L10N_ID="${GAME_ID}-l10n"
PKG_L10N_EN_ID="${PKG_L10N_ID}-en"
PKG_L10N_FR_ID="${PKG_L10N_ID}-fr"
PKG_L10N_ID_EN="$PKG_L10N_EN_ID"
PKG_L10N_ID_FR="$PKG_L10N_FR_ID"
PKG_L10N_PROVIDES="
$PKG_L10N_ID"
PKG_L10N_EN_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_FR_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_EN_DESCRIPTION='English localization'
PKG_L10N_FR_DESCRIPTION='French localization'
PKG_L10N_DESCRIPTION_EN="$PKG_L10N_EN_DESCRIPTION"
PKG_L10N_DESCRIPTION_FR="$PKG_L10N_FR_DESCRIPTION"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_L10N_ID $PKG_DATA_ID"

# The "find" and "rename" commands are used to handle some localized files

SCRIPT_DEPS="${SCRIPT_DEPS:-} find rename"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Delete unsupported localizations.
	case "$(current_archive)" in
		('ARCHIVE_BASE_EN_'*|'ARCHIVE_BASE_FR_'*)
			## Old language-specific archives do not use localization suffixes.
		;;
		(*)
			find . -name '*@??-*' ! -name '*@en-US' ! -name '*@fr-FR' -delete
		;;
	esac

	## Rename some localized files that do not differ between the supported localizations.
	case "$(current_archive)" in
		('ARCHIVE_BASE_EN_'*|'ARCHIVE_BASE_FR_'*)
			## Old language-specific archives do not use localization suffixes.
		;;
		(*)
			rename 's/\@en-US$//' \
				'data/fonts.cfg@en-US' \
				'data/video/1c_logo_eng.ogm@en-US' \
				'data/video/kbtl-intro-800x600-eng.ogm@en-US' \
				'kb.exe@en-US'
		;;
	esac

	## Delete some redundant files duplicated between the supported localizations.
	case "$(current_archive)" in
		('ARCHIVE_BASE_EN_'*|'ARCHIVE_BASE_FR_'*)
			## Old language-specific archives do not use localization suffixes.
		;;
		(*)
			rm \
				'data/fonts.cfg@fr-FR' \
				'data/video/1c_logo_eng.ogm@fr-FR' \
				'data/video/kbtl-intro-800x600-eng.ogm@fr-FR' \
				'kb.exe@fr-FR'
		;;
	esac
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default
## Drop the localization suffix from some files.
case "$(current_archive)" in
	('ARCHIVE_BASE_EN_'*|'ARCHIVE_BASE_FR_'*)
		## Old language-specific archives do not use localization suffixes.
	;;
	(*)
		find "$(package_path 'PKG_L10N_EN')$(path_game_data)" -name '*@en-US' -exec rename 's/\@en-US$//' {} +
		find "$(package_path 'PKG_L10N_FR')$(path_game_data)" -name '*@fr-FR' -exec rename 's/\@fr-FR$//' {} +
		find "$(package_path 'PKG_L10N_EN')$(path_documentation)" -name '*@en-US' -exec rename 's/\@en-US$//' {} +
		find "$(package_path 'PKG_L10N_FR')$(path_documentation)" -name '*@fr-FR' -exec rename 's/\@fr-FR$//' {} +
	;;
esac

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
case "$(current_archive)" in
	('ARCHIVE_BASE_EN_'*|'ARCHIVE_BASE_FR_'*)
		print_instructions
	;;
	(*)
		case "$(messages_language)" in
			('fr')
				lang_string='version %s :'
				lang_en='anglaise'
				lang_fr='française'
			;;
			('en'|*)
				lang_string='%s version:'
				lang_en='English'
				lang_fr='French'
			;;
		esac
		printf '\n'
		printf "$lang_string" "$lang_en"
		print_instructions 'PKG_BIN' 'PKG_DATA' 'PKG_L10N_EN'
		printf "$lang_string" "$lang_fr"
		print_instructions 'PKG_BIN' 'PKG_DATA' 'PKG_L10N_FR'
	;;
esac

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
