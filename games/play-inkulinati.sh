#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Fabien Givors <captnfab@debian-facile.org>
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Inkulinati
# send your bug reports to contact@dotslashplay.it
###

script_version=20240524.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='inkulinati'
GAME_NAME='Inkulinati'

GAME_ID_DEMO="${GAME_ID}-demo"
GAME_NAME_DEMO="$GAME_NAME (demo)"

## Archives

### Full game

ARCHIVE_BASE_0_NAME='setup_inkulinati_1.47.2.0_(64bit)_(71246).exe'
ARCHIVE_BASE_0_MD5='099f8976a9142d736282316f37028f60'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='3934716'
ARCHIVE_BASE_0_VERSION='1.47.2.0-gog71246'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/inkulinati'

### Free demo

## The free demo is no longer available from gog.com.
ARCHIVE_BASE_DEMO_0_NAME='setup_inkulinati_demo_1.1_(42755).exe'
ARCHIVE_BASE_DEMO_0_MD5='8e081a8cf9e9457757c1a66b72c9a5f0'
ARCHIVE_BASE_DEMO_0_TYPE='innosetup'
ARCHIVE_BASE_DEMO_0_SIZE='1258482'
ARCHIVE_BASE_DEMO_0_VERSION='1.0-gog42755'


UNITY3D_NAME='inkulinati'

CONTENT_PATH_DEFAULT='.'

WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Yaza Games/Inkulinati'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_DEMO="${GAME_ID_DEMO}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPS_DEMO="$PKG_DATA_ID_DEMO"
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
video/quicktime, variant=(string)iso'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
