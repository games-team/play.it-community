#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# My Time at Portia
# send your bug reports to contact@dotslashplay.it
###

script_version=20241219.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='my-time-at-portia'
GAME_NAME='My Time at Portia'

ARCHIVE_BASE_1_NAME='setup_my_time_at_portia_20210813-1123-141541_(64bit)_(49522).exe'
ARCHIVE_BASE_1_MD5='73890f1c7a86bdcab47a00b72366928a'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_my_time_at_portia_20210813-1123-141541_(64bit)_(49522)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='87fb19e0f0f04a499e81e7a81b49818d'
ARCHIVE_BASE_1_PART2_NAME='setup_my_time_at_portia_20210813-1123-141541_(64bit)_(49522)-2.bin'
ARCHIVE_BASE_1_PART2_MD5='2ef18dcfdc16f1d7f52c2a49cfd63010'
ARCHIVE_BASE_1_SIZE='10038490'
ARCHIVE_BASE_1_VERSION='2021.08.13-gog49522'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/my_time_at_portia'

ARCHIVE_BASE_0_NAME='setup_my_time_at_portia_2.0.141235_(64bit)_(41960).exe'
ARCHIVE_BASE_0_MD5='9f21752466f3ce96719cfea66bce05d5'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_my_time_at_portia_2.0.141235_(64bit)_(41960)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='881179a255b81a165411ca88fc99dcc7'
ARCHIVE_BASE_0_PART2_NAME='setup_my_time_at_portia_2.0.141235_(64bit)_(41960)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='b84720aea74b713c8738197872f4add0'
ARCHIVE_BASE_0_SIZE='9800000'
ARCHIVE_BASE_0_VERSION='2.0.141235-gog41960'

UNITY3D_NAME='portia'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_DATA_ASSETBUNDLES_FILES='
portia_data/streamingassets/assetbundles'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Pathea Games/My Time at Portia'

PACKAGES_LIST='
PKG_BIN
PKG_DATA_ASSETBUNDLES
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPENDENCIES_SIBLINGS='
PKG_DATA_ASSETBUNDLES'

PKG_DATA_ASSETBUNDLES_ID="${PKG_DATA_ID}-asssetbundles"
PKG_DATA_DESCRIPTION="$PKG_DATA_DESCRIPTION - asset bundles"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
