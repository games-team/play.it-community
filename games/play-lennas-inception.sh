#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Lenna's Inception
# send your bug reports to contact@dotslashplay.it
###

script_version=20241103.3

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='lennas-inception'
GAME_NAME='Lennaʼs Inception'

ARCHIVE_BASE_2_NAME='lennas-inception-linux-amd64-stable.zip'
ARCHIVE_BASE_2_MD5='2eea96efe93f463688da6540a3904ce9'
ARCHIVE_BASE_2_VERSION='1.1.8-itch'
ARCHIVE_BASE_2_SIZE='301938'
ARCHIVE_BASE_2_URL='https://tccoxon.itch.io/lennas-inception'

## ./play.it is only going to get support for archives sharing a same name with its 2.31 release,
## cf. https://forge.dotslashplay.it/play.it/play.it/-/issues/539
#ARCHIVE_BASE_1_NAME='lennas-inception-linux-amd64-stable.zip'
#ARCHIVE_BASE_1_MD5='5f7ff7b389777b00519144df1cc98cc9'
#ARCHIVE_BASE_1_VERSION='1.1.5-itch.2020.11.22'
#ARCHIVE_BASE_1_SIZE='310000'

## ./play.it is only going to get support for archives sharing a same name with its 2.31 release,
## cf. https://forge.dotslashplay.it/play.it/play.it/-/issues/539
#ARCHIVE_BASE_0_NAME='lennas-inception-linux-amd64-stable.zip'
#ARCHIVE_BASE_0_MD5='e701126a913c2c63c89e79875dd89e86'
#ARCHIVE_BASE_0_VERSION='1.0.10-itch1'
#ARCHIVE_BASE_0_SIZE='310000'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_MAIN_FILES='
assets
launch-config.json
lib'
CONTENT_DOC_MAIN_FILES='
COPYRIGHT.txt
README.txt'

APP_MAIN_TYPE='java'
APP_MAIN_EXE='lib/libloader.jar'
APP_MAIN_JAVA_OPTIONS='-Dsun.java2d.opengl=True -Djava.library.path=./lib -Xms1024m -Xmx3072m'
APP_MAIN_ICON='icon.png'
## Work around the shipped binaries overuse of file descriptors
APP_MAIN_PRERUN_BIN_SHIPPED='
# Work around the engine overuse of file descriptors
## 4096 is an arbitrary value, 4 times the default (1024), that seems to work for all setups.
if ! ulimit -n 4096; then
	{
		printf "\\n\\033[1;33mWarning:\\033[0m\\n"
		printf "Your current shell interpreter has no support for ulimit -n.\\n"
		printf "This might lead to unending loading screens if the game engine hits the file descriptors use limit.\\n"
		printf "\\n"
	} > /dev/stderr
fi
'

# Since this game seems to be broken on OpenJDK ≥ 11, we build an extra package allowing to use the shipped OpenJDK 1.8

CONTENT_GAME_BIN_SHIPPED_FILES='
launch-config.json
jre'

PACKAGES_LIST='
PKG_MAIN
PKG_BIN_SYSTEM
PKG_BIN_SHIPPED'

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_BIN'

PKG_BIN_ID="${GAME_ID}-bin"
PKG_BIN_PROVIDES="
$PKG_BIN_ID"

PKG_BIN_SYSTEM_ID="${PKG_BIN_ID}-system"
PKG_BIN_SYSTEM_PROVIDES="$PKG_BIN_PROVIDES"
PKG_BIN_SYSTEM_DESCRIPTION='Using system-provided Java'

PKG_BIN_SHIPPED_ID="${PKG_BIN_ID}-shipped"
PKG_BIN_SHIPPED_PROVIDES="$PKG_BIN_PROVIDES"
PKG_BIN_SHIPPED_DESCRIPTION='Using shipped Java binaries'
PKG_BIN_SHIPPED_ARCH='64'
PKG_BIN_SHIPPED_DEPENDENCIES_LIBRARIES='
libasound.so.2
libatk-1.0.so.0
libcairo.so.2
libc.so.6
libdl.so.2
libfontconfig.so.1
libfreetype.so.6
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libgdk-x11-2.0.so.0
libgio-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgmodule-2.0.so.0
libgobject-2.0.so.0
libgthread-2.0.so.0
libgtk-x11-2.0.so.0
libjpeg.so.62
libm.so.6
libpango-1.0.so.0
libpangocairo-1.0.so.0
libpangoft2-1.0.so.0
libpthread.so.0
librt.so.1
libstdc++.so.6
libthread_db.so.1
libX11.so.6
libXext.so.6
libXi.so.6
libxml2.so.2
libXrender.so.1
libxslt.so.1
libXtst.so.6
libXxf86vm.so.1
libz.so.1'

# Extract game icon from the .jar archive

SCRIPT_DEPS="${SCRIPT_DEPS:-} unzip"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Ensure the shipped java binary can be executed.
	chmod 755 'jre/bin/java'

	## Extract game icon from the .jar archive.
	unzip -q -d . 'assets/lennasinception.jar' "$(icon_path 'APP_MAIN_ICON')"
)

# Include game data

content_inclusion_icons 'PKG_MAIN'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN_SYSTEM'

## Use shipped Java binary instead of system-provided one.
game_exec_line() {
	cat <<- EOF
	./jre/bin/java $APP_MAIN_JAVA_OPTIONS -jar "\$APP_EXE" "\$@"
	EOF
}
launchers_generation 'PKG_BIN_SHIPPED'

# Build packages

packages_generation
case "$(messages_language)" in
	('fr')
		message='Utilisation des binaires fournis par %s :'
		bin_shipped='les développeurs'
		bin_system='le système'
	;;
	('en'|*)
		message='Using binaries provided by %s:'
		bin_shipped='the developers'
		bin_system='the system'
	;;
esac
printf '\n'
printf "$message" "$bin_system"
print_instructions 'PKG_MAIN' 'PKG_BIN_SYSTEM'
printf "$message" "$bin_shipped"
print_instructions 'PKG_MAIN' 'PKG_BIN_SHIPPED'

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
