#!/bin/sh
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Giana Sisters series:
# - Giana Sisters: Twisted Dreams
# - Giana Sisters: Rise of the Owlverlord
# send your bug reports to contact@dotslashplay.it
###

script_version=20241210.2

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID_DREAMS='giana-sisters-twisted-dreams'
GAME_NAME_DREAMS='Giana Sisters: Twisted Dreams'

GAME_ID_OWL='giana-sisters-rise-of-the-owlverlord'
GAME_NAME_OWL='Giana Sisters: Rise of the Owlverlord'

# Archives

## Giana Sisters: Twisted Dreams

ARCHIVE_BASE_DREAMS_1_NAME='setup_giana_sisters_twisted_dreams_1.2.1_(19142).exe'
ARCHIVE_BASE_DREAMS_1_MD5='e5605f4890984375192bd37545e51ff8'
ARCHIVE_BASE_DREAMS_1_TYPE='innosetup'
ARCHIVE_BASE_DREAMS_1_SIZE='2800000'
ARCHIVE_BASE_DREAMS_1_VERSION='1.2.1-gog19142'
ARCHIVE_BASE_DREAMS_1_URL='https://www.gog.com/game/giana_sisters_twisted_dreams'

ARCHIVE_BASE_DREAMS_0_NAME='setup_giana_sisters_twisted_dreams_2.2.0.16.exe'
ARCHIVE_BASE_DREAMS_0_MD5='31b2a0431cfd764198834faec314f0b2'
ARCHIVE_BASE_DREAMS_0_TYPE='innosetup'
ARCHIVE_BASE_DREAMS_0_SIZE='2900000'
ARCHIVE_BASE_DREAMS_0_VERSION='1.0-gog2.2.0.16'

## Giana Sisters: Rise of the Owlverlord

ARCHIVE_BASE_OWL_1_NAME='setup_giana_sisters_rise_of_the_owlverlord_2.1.0.8.exe'
ARCHIVE_BASE_OWL_1_MD5='bebb3a09c10b8e34b88b6d9f64dedeab'
ARCHIVE_BASE_OWL_1_TYPE='innosetup'
ARCHIVE_BASE_OWL_1_SIZE='2556034'
ARCHIVE_BASE_OWL_1_VERSION='1.0-gog2.1.0.8'
ARCHIVE_BASE_OWL_1_URL='https://www.gog.com/game/giana_sisters_rise_of_the_owlverlord'


CONTENT_PATH_DEFAULT_DREAMS='.'
CONTENT_PATH_DEFAULT_DREAMS_0='app'
CONTENT_PATH_DEFAULT_OWL='app'
CONTENT_GAME_BIN_FILES='
launcher
7z.dll
gsgameexe.exe
gsgameexe_dx9.exe'
CONTENT_GAME_DATA_FILES='
bundles
added_content
data_common'
CONTENT_GAME0_DATA_PATH='__support/app'
CONTENT_GAME0_DATA_FILES='
data_common'
CONTENT_DOC_DATA_FILES='
7z_copying.txt
7z_license.txt
7z_readme.txt'

WINE_PERSISTENT_DIRECTORIES_DREAMS='
users/${USER}/Documents/Giana Sisters - Twisted Dreams'
WINE_PERSISTENT_DIRECTORIES_OWL='
users/${USER}/Documents/Giana Sisters - Rise of the Owlverlord'
## TODO: Check if these winetricks verbs are still required with current WINE builds.
WINE_WINETRICKS_VERBS='xact wmp9'

APP_MAIN_EXE='gsgameexe.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID_DREAMS="${GAME_ID_DREAMS}-data"
PKG_DATA_ID_OWL="${GAME_ID_OWL}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
