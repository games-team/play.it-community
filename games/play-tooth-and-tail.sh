#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 BetaRays
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Tooth and Tail
# send your bug reports to contact@dotslashplay.it
###

script_version=20230125.2

GAME_ID='tooth-and-tail'
GAME_NAME='Tooth and Tail'

## This DRM-free archive used to be sold by Humble Bundle,
## but they now only provide a Steam key instead.
ARCHIVE_BASE_0='toothandtail_linux.zip'
ARCHIVE_BASE_0_MD5='5a01a61889e795b538d3db288a6f519d'
ARCHIVE_BASE_0_VERSION='1.6.1.1-humble1'
ARCHIVE_BASE_0_SIZE='600000'

CONTENT_PATH_DEFAULT='Release'
CONTENT_GAME_MAIN_FILES='
content
ToothAndTail.exe
ToothAndTail.exe.config
fr/ToothAndTail.resources.dll
FNA.dll
FNA.dll.config
Glide.dll
Newtonsoft.Json.dll'

APP_MAIN_EXE='ToothAndTail.exe'
APP_MAIN_ICON='ToothAndTail.exe'

PKG_MAIN_DEPS='mono'
PKG_MAIN_DEPENDENCIES_LIBRARIES='
libopenal.so.1
libSDL2-2.0.so.0
libSDL2_image-2.0.so.0
libtheora.so.0
libvorbisfile.so.3'
PKG_MAIN_DEPENDENCIES_MONO_LIBRARIES='
mscorlib.dll
Mono.Posix.dll
Mono.Security.dll
System.dll
System.Configuration.dll
System.Core.dll
System.Data.dll
System.Drawing.dll
System.Net.dll
System.Net.Http.dll
System.Runtime.Serialization.dll
System.Security.dll
System.Windows.Forms.dll
System.Xml.dll
System.Xml.Linq.dll'

# Include shipped libraries that can not be replaced by system ones

CONTENT_LIBS_LIBS32_PATH="${CONTENT_PATH_DEFAULT}/lib"
CONTENT_LIBS_LIBS32_FILES='
libmojoshader.so
libtheorafile.so'
CONTENT_LIBS_LIBS64_PATH="${CONTENT_PATH_DEFAULT}/lib64"
CONTENT_LIBS_LIBS64_FILES='
libfmod.so.10
libfmodstudio.so.10
libmojoshader.so
libtheorafile.so'

PACKAGES_LIST='PKG_MAIN PKG_LIBS32 PKG_LIBS64'

PKG_LIBS_ID="${GAME_ID}-libs"
PKG_LIBS32_ID="$PKG_LIBS_ID"
PKG_LIBS32_ARCH='32'
PKG_LIBS64_ID="$PKG_LIBS_ID"
PKG_LIBS64_ARCH='64'

PKG_MAIN_DEPS="$PKG_MAIN_DEPS $PKG_LIBS_ID"

# Load common functions

target_version='2.21'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
