#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 HS-157
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Breach & Clear
# send your bug reports to contact@dotslashplay.it
###

script_version=20240811.1

GAME_ID='breach-and-clear'
GAME_NAME='Breach & Clear'

GAME_ID_DEADLINE="${GAME_ID}-deadline"
GAME_NAME_DEADLINE="$GAME_NAME - DEADline"

UNITY3D_NAME='Bnc'
UNITY3D_NAME_DEADLINE='DEADline'

# Archives

## Breach & Clear (base game)

ARCHIVE_BASE_HUMBLE_0='BncNix_12_4_15_NODRM.rar'
ARCHIVE_BASE_HUMBLE_0_MD5='a2eff200bdce1b96852d2b709614d17a'
ARCHIVE_BASE_HUMBLE_0_SIZE='2500000'
ARCHIVE_BASE_HUMBLE_0_VERSION='2.1.1-humble1'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/breach-clear'

## This archive is no longer available for sale from gog.com, starting on 2024-09-13
## cf. https://www.gog.com/forum/general/delisting_breach_clear_will_be_removed_from_the_store_on_september_13th/post1
ARCHIVE_BASE_GOG_0='gog_breach_clear_2.0.0.3.sh'
ARCHIVE_BASE_GOG_0_MD5='e7f6b59d8ce97c9ab850b47608941b7d'
ARCHIVE_BASE_GOG_0_TYPE='mojosetup'
ARCHIVE_BASE_GOG_0_SIZE='2500000'
ARCHIVE_BASE_GOG_0_VERSION='2.1.1-gog2.0.0.3'

## Breach & Clear: DEADline

ARCHIVE_BASE_DEADLINE_0='gog_breach_and_clear_deadline_2.3.0.8.sh'
ARCHIVE_BASE_DEADLINE_0_MD5='fcf5e77ea47ec4446bd67168372f6e34'
ARCHIVE_BASE_DEADLINE_0_TYPE='mojosetup'
ARCHIVE_BASE_DEADLINE_0_SIZE='3300000'
ARCHIVE_BASE_DEADLINE_0_VERSION='1.05-gog2.3.0.8'
ARCHIVE_BASE_DEADLINE_0_URL='https://www.gog.com/game/breach_clear_deadline'


CONTENT_PATH_DEFAULT_HUMBLE='BncHumboNix'
CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_GAME_BIN_FILES="
${UNITY3D_NAME}_Data/Mono
${UNITY3D_NAME}_Data/Plugins
${UNITY3D_NAME}.x86"
CONTENT_GAME_BIN_FILES_DEADLINE="
${UNITY3D_NAME_DEADLINE}_Data/Mono
${UNITY3D_NAME_DEADLINE}_Data/Plugins
${UNITY3D_NAME_DEADLINE}.x86"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data"
CONTENT_GAME_DATA_FILES_DEADLINE="
${UNITY3D_NAME_DEADLINE}_Data"

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
ld-linux.so.2
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libGLU.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1'

# Enforce a consistent Unity3D name

SCRIPT_DEPS="$SCRIPT_DEPS rename"

# Load common functions

target_version='2.20'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Enforce a consistent Unity3D name
	unity3d_name=$(unity3d_name)
	rename -- "s/.*_Data/${unity3d_name}_Data/" *_Data
	rename -- "s/.*\.x86/${unity3d_name}.x86/" *.x86
)

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build package

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
