#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Hoël Bézier
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Sundered
# send your bug reports to contact@dotslashplay.it
###

script_version=20240614.2

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='sundered'
GAME_NAME='Sundered'

ARCHIVE_BASE_ITCH_0_NAME='sundered-linux.zip'
ARCHIVE_BASE_ITCH_0_MD5='1be5d771f33fa95f3a567aef75314750'
ARCHIVE_BASE_ITCH_0_SIZE='2439430'
ARCHIVE_BASE_ITCH_0_VERSION='1.0-itch.2020.06.07'
ARCHIVE_BASE_ITCH_0_URL='https://thunderlotus.itch.io/sundered'

ARCHIVE_BASE_GOG_0_NAME='sundered_20190404_29317.sh'
ARCHIVE_BASE_GOG_0_MD5='efd81d3e4b14d26cdef2362b888e0a56'
ARCHIVE_BASE_GOG_0_SIZE='2500000'
ARCHIVE_BASE_GOG_0_VERSION='20190404-gog23987'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/sundered'

UNITY3D_NAME='Sundered'
UNITY3D_PLUGINS='
ScreenSelector.so'
## TODO: Check if the Steam libraries are required.
UNITY3D_PLUGINS="$UNITY3D_PLUGINS
libCSteamworks.so
libsteam_api.so"

CONTENT_PATH_DEFAULT_ITCH='linux'
CONTENT_PATH_DEFAULT_GOG='data/noarch/game'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
