#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 VA
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Tetrobot and Co.
# send your bug reports to contact@dotslashplay.it
###

script_version=20240605.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='tetrobot-and-co'
GAME_NAME='Tetrobot and Co.'

ARCHIVE_BASE_1_NAME='gog_tetrobot_and_co_2.1.0.6.sh'
ARCHIVE_BASE_1_MD5='2ad2969e64e19d5753f8822e407c148c'
ARCHIVE_BASE_1_SIZE='523264'
ARCHIVE_BASE_1_VERSION='1.2.1-gog2.1.0.6'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/tetrobot_and_co'

ARCHIVE_BASE_0_NAME='gog_tetrobot_and_co_2.1.0.5.sh'
ARCHIVE_BASE_0_MD5='7d75f9813e1ec154158875c8e69e9dc8'
ARCHIVE_BASE_0_SIZE='530000'
ARCHIVE_BASE_0_VERSION='1.2.1-gog2.1.0.5'

## This game uses an unusual files structure for an Unity3D game,
## so most properties need to be explicitly set.
UNITY3D_NAME='Tetrobot and Co'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN_FILES='
Data/Mono
Tetrobot and Co.x86'
CONTENT_GAME_DATA_FILES='
Data'

APP_MAIN_EXE='Tetrobot and Co.x86'
APP_MAIN_ICON='Data/Resources/UnityPlayer.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libGLU.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXext.so.6'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
