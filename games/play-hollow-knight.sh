#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2018 VA
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Hollow Knight
# send your bug reports to contact@dotslashplay.it
###

script_version=20240623.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='hollow-knight'
GAME_NAME='Hollow Knight'

ARCHIVE_BASE_3_NAME='hollow_knight_1_5_78_11833_51218.sh'
ARCHIVE_BASE_3_MD5='ad36d98ec636b09796667f0b124ff5bb'
ARCHIVE_BASE_3_SIZE='7900000'
ARCHIVE_BASE_3_VERSION='1.5.78-gog51218'
ARCHIVE_BASE_3_URL='https://www.gog.com/game/hollow_knight'

ARCHIVE_BASE_2_NAME='hollow_knight_1_4_3_2_23987.sh'
ARCHIVE_BASE_2_MD5='520af5ebe8ac3977faaafc5d2ed8a779'
ARCHIVE_BASE_2_SIZE='7800000'
ARCHIVE_BASE_2_VERSION='1.4.3.2-gog23987'

ARCHIVE_BASE_1_NAME='hollow_knight_en_1_3_1_5_20240.sh'
ARCHIVE_BASE_1_MD5='197d9ffc7e0be447849e22a04da836e4'
ARCHIVE_BASE_1_SIZE='7100000'
ARCHIVE_BASE_1_VERSION='1.3.1.5-gog20240'

ARCHIVE_BASE_0_NAME='gog_hollow_knight_2.1.0.2.sh'
ARCHIVE_BASE_0_MD5='0d18baf29d5552dc094ca2bfe5fcaae6'
ARCHIVE_BASE_0_SIZE='9200000'
ARCHIVE_BASE_0_VERSION='1.0.3.1-gog2.1.0.2'

UNITY3D_NAME='Hollow Knight'
## Only older builds include the ScreenSelector.so plugin.
UNITY3D_PLUGINS_2='
ScreenSelector.so'
UNITY3D_PLUGINS_1="$UNITY3D_PLUGINS_2"
UNITY3D_PLUGINS_0="$UNITY3D_PLUGINS_2"

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
## Ensure easy upgrade from packages generated using pre-20230406.2 game script.
PKG_DATA_PROVIDES="${PKG_DATA_PROVIDES:-}
hollow-knight-assets"

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libz.so.1'
## Only older builds include the ScreenSelector.so plugin.
PKG_BIN_DEPENDENCIES_LIBRARIES_2='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'
PKG_BIN_DEPENDENCIES_LIBRARIES_1="$PKG_BIN_DEPENDENCIES_LIBRARIES_2"
PKG_BIN_DEPENDENCIES_LIBRARIES_0="$PKG_BIN_DEPENDENCIES_LIBRARIES_2"

# Load common functions

target_version='2.22'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Enforce a consistent Unity3D name.
	if [ -e 'hollow_knight.x86_64' ]; then
		mv 'hollow_knight.x86_64' "$(unity3d_name)"
	fi
	if [ -e 'hollow_knight_Data' ]; then
		mv 'hollow_knight_Data' "$(unity3d_name)_Data"
	fi

	## Drop the "Plugins" directory if it is empty.
	rmdir --ignore-fail-on-non-empty "$(unity3d_name)_Data/Plugins"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
