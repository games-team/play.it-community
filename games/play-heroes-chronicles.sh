#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Heroes Chronicles series:
# - Chapter 1
# - Chapter 2
# - Chapter 3
# - Chapter 4
# - Chapter 5
# - Chapter 6
# - Chapter 7
# - Chapter 8
# send your bug reports to contact@dotslashplay.it
###

script_version=20240505.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='heroes-chronicles'
GAME_NAME='Heroes Chronicles'

GAME_ID_CHAPTER1="${GAME_ID}-1"
GAME_NAME_CHAPTER1="${GAME_NAME}: Chapter 1 - Warlords of the Wasteland"

GAME_ID_CHAPTER2="${GAME_ID}-2"
GAME_NAME_CHAPTER2="${GAME_NAME}: Chapter 2 - Conquest of the Underworld"

GAME_ID_CHAPTER3="${GAME_ID}-3"
GAME_NAME_CHAPTER3="${GAME_NAME}: Chapter 3 - Masters of the Elements"

GAME_ID_CHAPTER4="${GAME_ID}-4"
GAME_NAME_CHAPTER4="${GAME_NAME}: Chapter 4 - Clash of the Dragons"

GAME_ID_CHAPTER5="${GAME_ID}-5"
GAME_NAME_CHAPTER5="${GAME_NAME}: Chapter 5 - The World Tree"

GAME_ID_CHAPTER6="${GAME_ID}-6"
GAME_NAME_CHAPTER6="${GAME_NAME}: Chapter 6 - The Fiery Moon"

GAME_ID_CHAPTER7="${GAME_ID}-7"
GAME_NAME_CHAPTER7="${GAME_NAME}: Chapter 7 - Revolt of the Beastmasters"

GAME_ID_CHAPTER8="${GAME_ID}-8"
GAME_NAME_CHAPTER8="${GAME_NAME}: Chapter 8 - The Sword of Frost"

# Archives

## Chapter 1

ARCHIVE_BASE_CHAPTER1_0_NAME='setup_heroes_chronicles_chapter1_2.1.0.42.exe'
ARCHIVE_BASE_CHAPTER1_0_MD5='f584d6e11ed47d1d40e973a691adca5d'
ARCHIVE_BASE_CHAPTER1_0_TYPE='innosetup'
ARCHIVE_BASE_CHAPTER1_0_SIZE='500000'
ARCHIVE_BASE_CHAPTER1_0_VERSION='1.0-gog2.1.0.42'
ARCHIVE_BASE_CHAPTER1_0_URL='https://www.gog.com/game/heroes_chronicles_all_chapters'

## Chapter 2

ARCHIVE_BASE_CHAPTER2_0_NAME='setup_heroes_chronicles_chapter2_2.1.0.43.exe'
ARCHIVE_BASE_CHAPTER2_0_MD5='0d240bc0309814ba251c2d9b557cf69f'
ARCHIVE_BASE_CHAPTER2_0_TYPE='innosetup'
ARCHIVE_BASE_CHAPTER2_0_SIZE='510000'
ARCHIVE_BASE_CHAPTER2_0_VERSION='1.0-gog2.1.0.43'
ARCHIVE_BASE_CHAPTER2_0_URL='https://www.gog.com/game/heroes_chronicles_all_chapters'

## Chapter 3

ARCHIVE_BASE_CHAPTER3_0_NAME='setup_heroes_chronicles_chapter3_2.1.0.41.exe'
ARCHIVE_BASE_CHAPTER3_0_MD5='cb21751572960d47a259efc17b92c88c'
ARCHIVE_BASE_CHAPTER3_0_TYPE='innosetup'
ARCHIVE_BASE_CHAPTER3_0_SIZE='490000'
ARCHIVE_BASE_CHAPTER3_0_VERSION='1.0-gog2.1.0.41'
ARCHIVE_BASE_CHAPTER3_0_URL='https://www.gog.com/game/heroes_chronicles_all_chapters'

## Chapter 4

ARCHIVE_BASE_CHAPTER4_0_NAME='setup_heroes_chronicles_chapter4_2.1.0.42.exe'
ARCHIVE_BASE_CHAPTER4_0_MD5='922291e16176cb4bd37ca88eb5f3a19e'
ARCHIVE_BASE_CHAPTER4_0_TYPE='innosetup'
ARCHIVE_BASE_CHAPTER4_0_SIZE='490000'
ARCHIVE_BASE_CHAPTER4_0_VERSION='1.0-gog2.1.0.42'
ARCHIVE_BASE_CHAPTER4_0_URL='https://www.gog.com/game/heroes_chronicles_all_chapters'

## Chapter 5

ARCHIVE_BASE_CHAPTER5_0_NAME='setup_heroes_chronicles_chapter5_2.1.0.42.exe'
ARCHIVE_BASE_CHAPTER5_0_MD5='57b3ec588e627a2da30d3bc80ede5b1d'
ARCHIVE_BASE_CHAPTER5_0_TYPE='innosetup'
ARCHIVE_BASE_CHAPTER5_0_SIZE='470000'
ARCHIVE_BASE_CHAPTER5_0_VERSION='1.0-gog2.1.0.42'
ARCHIVE_BASE_CHAPTER5_0_URL='https://www.gog.com/game/heroes_chronicles_all_chapters'

## Chapter 6

ARCHIVE_BASE_CHAPTER6_0_NAME='setup_heroes_chronicles_chapter6_2.1.0.42.exe'
ARCHIVE_BASE_CHAPTER6_0_MD5='64becfde1882eecd93fb02bf215eff11'
ARCHIVE_BASE_CHAPTER6_0_TYPE='innosetup'
ARCHIVE_BASE_CHAPTER6_0_SIZE='470000'
ARCHIVE_BASE_CHAPTER6_0_VERSION='1.0-gog2.1.0.42'
ARCHIVE_BASE_CHAPTER6_0_URL='https://www.gog.com/game/heroes_chronicles_all_chapters'

## Chapter 7

ARCHIVE_BASE_CHAPTER7_0_NAME='setup_heroes_chronicles_chapter7_2.1.0.42.exe'
ARCHIVE_BASE_CHAPTER7_0_MD5='07c189a731886b2d3891ac1c65581d40'
ARCHIVE_BASE_CHAPTER7_0_TYPE='innosetup'
ARCHIVE_BASE_CHAPTER7_0_SIZE='500000'
ARCHIVE_BASE_CHAPTER7_0_VERSION='1.0-gog2.1.0.42'
ARCHIVE_BASE_CHAPTER7_0_URL='https://www.gog.com/game/heroes_chronicles_all_chapters'

## Chapter 8

ARCHIVE_BASE_CHAPTER8_0_NAME='setup_heroes_chronicles_chapter8_2.1.0.42.exe'
ARCHIVE_BASE_CHAPTER8_0_MD5='2b3e4c366db0f7e3e8b15b0935aad528'
ARCHIVE_BASE_CHAPTER8_0_TYPE='innosetup'
ARCHIVE_BASE_CHAPTER8_0_SIZE='480000'
ARCHIVE_BASE_CHAPTER8_0_VERSION='1.0-gog2.1.0.42'
ARCHIVE_BASE_CHAPTER8_0_URL='https://www.gog.com/game/heroes_chronicles_all_chapters'

CONTENT_INNER_PATH_CHAPTER1='warlords of the wasteland'
CONTENT_INNER_PATH_CHAPTER2='conquest of the underworld'
CONTENT_INNER_PATH_CHAPTER3='masters of the elements'
CONTENT_INNER_PATH_CHAPTER4='clash of the dragons'
CONTENT_INNER_PATH_CHAPTER5='the world tree'
CONTENT_INNER_PATH_CHAPTER6='the fiery moon'
CONTENT_INNER_PATH_CHAPTER7='revolt of the beastmasters'
CONTENT_INNER_PATH_CHAPTER8='the sword of frost'
CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES_CHAPTER1="
${CONTENT_INNER_PATH_CHAPTER1}/*.asi
${CONTENT_INNER_PATH_CHAPTER1}/*.dll
${CONTENT_INNER_PATH_CHAPTER1}/*.exe"
CONTENT_GAME_BIN_FILES_CHAPTER2="
${CONTENT_INNER_PATH_CHAPTER2}/*.asi
${CONTENT_INNER_PATH_CHAPTER2}/*.dll
${CONTENT_INNER_PATH_CHAPTER2}/*.exe"
CONTENT_GAME_BIN_FILES_CHAPTER3="
${CONTENT_INNER_PATH_CHAPTER3}/*.asi
${CONTENT_INNER_PATH_CHAPTER3}/*.dll
${CONTENT_INNER_PATH_CHAPTER3}/*.exe"
CONTENT_GAME_BIN_FILES_CHAPTER4="
${CONTENT_INNER_PATH_CHAPTER4}/*.asi
${CONTENT_INNER_PATH_CHAPTER4}/*.dll
${CONTENT_INNER_PATH_CHAPTER4}/*.exe"
CONTENT_GAME_BIN_FILES_CHAPTER5="
${CONTENT_INNER_PATH_CHAPTER5}/*.asi
${CONTENT_INNER_PATH_CHAPTER5}/*.dll
${CONTENT_INNER_PATH_CHAPTER5}/*.exe"
CONTENT_GAME_BIN_FILES_CHAPTER6="
${CONTENT_INNER_PATH_CHAPTER6}/*.asi
${CONTENT_INNER_PATH_CHAPTER6}/*.dll
${CONTENT_INNER_PATH_CHAPTER6}/*.exe"
CONTENT_GAME_BIN_FILES_CHAPTER7="
${CONTENT_INNER_PATH_CHAPTER7}/*.asi
${CONTENT_INNER_PATH_CHAPTER7}/*.dll
${CONTENT_INNER_PATH_CHAPTER7}/*.exe"
CONTENT_GAME_BIN_FILES_CHAPTER8="
${CONTENT_INNER_PATH_CHAPTER8}/*.asi
${CONTENT_INNER_PATH_CHAPTER8}/*.dll
${CONTENT_INNER_PATH_CHAPTER8}/*.exe"
CONTENT_GAME_DATA_FILES_CHAPTER1="
${CONTENT_INNER_PATH_CHAPTER1}/data
${CONTENT_INNER_PATH_CHAPTER1}/games
${CONTENT_INNER_PATH_CHAPTER1}/maps"
CONTENT_GAME_DATA_FILES_CHAPTER2="
${CONTENT_INNER_PATH_CHAPTER2}/data
${CONTENT_INNER_PATH_CHAPTER2}/games
${CONTENT_INNER_PATH_CHAPTER2}/maps"
CONTENT_GAME_DATA_FILES_CHAPTER3="
${CONTENT_INNER_PATH_CHAPTER3}/data
${CONTENT_INNER_PATH_CHAPTER3}/games
${CONTENT_INNER_PATH_CHAPTER3}/maps"
CONTENT_GAME_DATA_FILES_CHAPTER4="
${CONTENT_INNER_PATH_CHAPTER4}/data
${CONTENT_INNER_PATH_CHAPTER4}/games
${CONTENT_INNER_PATH_CHAPTER4}/maps"
CONTENT_GAME_DATA_FILES_CHAPTER5="
${CONTENT_INNER_PATH_CHAPTER5}/data
${CONTENT_INNER_PATH_CHAPTER5}/games
${CONTENT_INNER_PATH_CHAPTER5}/maps"
CONTENT_GAME_DATA_FILES_CHAPTER6="
${CONTENT_INNER_PATH_CHAPTER6}/data
${CONTENT_INNER_PATH_CHAPTER6}/games
${CONTENT_INNER_PATH_CHAPTER6}/maps"
CONTENT_GAME_DATA_FILES_CHAPTER7="
${CONTENT_INNER_PATH_CHAPTER7}/data
${CONTENT_INNER_PATH_CHAPTER7}/games
${CONTENT_INNER_PATH_CHAPTER7}/maps"
CONTENT_GAME_DATA_FILES_CHAPTER8="
${CONTENT_INNER_PATH_CHAPTER8}/data
${CONTENT_INNER_PATH_CHAPTER8}/games
${CONTENT_INNER_PATH_CHAPTER8}/maps"
CONTENT_GAME_COMMON_FILES='
data
mp3'
CONTENT_DOC_DATA_PATH_CHAPTER1="${CONTENT_PATH_DEFAULT}/${CONTENT_INNER_PATH_CHAPTER1}"
CONTENT_DOC_DATA_PATH_CHAPTER2="${CONTENT_PATH_DEFAULT}/${CONTENT_INNER_PATH_CHAPTER2}"
CONTENT_DOC_DATA_PATH_CHAPTER3="${CONTENT_PATH_DEFAULT}/${CONTENT_INNER_PATH_CHAPTER3}"
CONTENT_DOC_DATA_PATH_CHAPTER4="${CONTENT_PATH_DEFAULT}/${CONTENT_INNER_PATH_CHAPTER4}"
CONTENT_DOC_DATA_PATH_CHAPTER5="${CONTENT_PATH_DEFAULT}/${CONTENT_INNER_PATH_CHAPTER5}"
CONTENT_DOC_DATA_PATH_CHAPTER6="${CONTENT_PATH_DEFAULT}/${CONTENT_INNER_PATH_CHAPTER6}"
CONTENT_DOC_DATA_PATH_CHAPTER7="${CONTENT_PATH_DEFAULT}/${CONTENT_INNER_PATH_CHAPTER7}"
CONTENT_DOC_DATA_PATH_CHAPTER8="${CONTENT_PATH_DEFAULT}/${CONTENT_INNER_PATH_CHAPTER8}"
CONTENT_DOC_DATA_FILES='
*.pdf
*.txt'

USER_PERSISTENT_DIRECTORIES_CHAPTER1="
${CONTENT_INNER_PATH_CHAPTER1}/games
${CONTENT_INNER_PATH_CHAPTER1}/maps"
USER_PERSISTENT_DIRECTORIES_CHAPTER2="
${CONTENT_INNER_PATH_CHAPTER2}/games
${CONTENT_INNER_PATH_CHAPTER2}/maps"
USER_PERSISTENT_DIRECTORIES_CHAPTER3="
${CONTENT_INNER_PATH_CHAPTER3}/games
${CONTENT_INNER_PATH_CHAPTER3}/maps"
USER_PERSISTENT_DIRECTORIES_CHAPTER4="
${CONTENT_INNER_PATH_CHAPTER4}/games
${CONTENT_INNER_PATH_CHAPTER4}/maps"
USER_PERSISTENT_DIRECTORIES_CHAPTER5="
${CONTENT_INNER_PATH_CHAPTER5}/games
${CONTENT_INNER_PATH_CHAPTER5}/maps"
USER_PERSISTENT_DIRECTORIES_CHAPTER6="
${CONTENT_INNER_PATH_CHAPTER6}/games
${CONTENT_INNER_PATH_CHAPTER6}/maps"
USER_PERSISTENT_DIRECTORIES_CHAPTER7="
${CONTENT_INNER_PATH_CHAPTER7}/games
${CONTENT_INNER_PATH_CHAPTER7}/maps"
USER_PERSISTENT_DIRECTORIES_CHAPTER8="
${CONTENT_INNER_PATH_CHAPTER8}/games
${CONTENT_INNER_PATH_CHAPTER8}/maps"
USER_PERSISTENT_FILES_CHAPTER1="
data/*.lod
${CONTENT_INNER_PATH_CHAPTER1}/data/*.lod"
USER_PERSISTENT_FILES_CHAPTER2="
data/*.lod
${CONTENT_INNER_PATH_CHAPTER2}/data/*.lod"
USER_PERSISTENT_FILES_CHAPTER3="
data/*.lod
${CONTENT_INNER_PATH_CHAPTER3}/data/*.lod"
USER_PERSISTENT_FILES_CHAPTER4="
data/*.lod
${CONTENT_INNER_PATH_CHAPTER4}/data/*.lod"
USER_PERSISTENT_FILES_CHAPTER5="
data/*.lod
${CONTENT_INNER_PATH_CHAPTER5}/data/*.lod"
USER_PERSISTENT_FILES_CHAPTER6="
data/*.lod
${CONTENT_INNER_PATH_CHAPTER6}/data/*.lod"
USER_PERSISTENT_FILES_CHAPTER7="
data/*.lod
${CONTENT_INNER_PATH_CHAPTER7}/data/*.lod"
USER_PERSISTENT_FILES_CHAPTER8="
data/*.lod
${CONTENT_INNER_PATH_CHAPTER8}/data/*.lod"

WINE_VIRTUAL_DESKTOP='auto'

APP_MAIN_EXE_CHAPTER1="${CONTENT_INNER_PATH_CHAPTER1}/warlords.exe"
APP_MAIN_EXE_CHAPTER2="${CONTENT_INNER_PATH_CHAPTER2}/underworld.exe"
APP_MAIN_EXE_CHAPTER3="${CONTENT_INNER_PATH_CHAPTER3}/elements.exe"
APP_MAIN_EXE_CHAPTER4="${CONTENT_INNER_PATH_CHAPTER4}/dragons.exe"
APP_MAIN_EXE_CHAPTER5="${CONTENT_INNER_PATH_CHAPTER5}/worldtree.exe"
APP_MAIN_EXE_CHAPTER6="${CONTENT_INNER_PATH_CHAPTER6}/fierymoon.exe"
APP_MAIN_EXE_CHAPTER7="${CONTENT_INNER_PATH_CHAPTER7}/beastmaster.exe"
APP_MAIN_EXE_CHAPTER8="${CONTENT_INNER_PATH_CHAPTER8}/sword.exe"
## Run the game binary from its parent directory
APP_MAIN_PRERUN='# Run the game binary from its parent directory
cd "$(dirname "$APP_EXE")"
APP_EXE=$(basename "$APP_EXE")
'

PACKAGES_LIST='
PKG_BIN
PKG_COMMON
PKG_DATA'

PKG_COMMON_ID="${GAME_ID}-common"
PKG_COMMON_DESCRIPTION="data shared between all $GAME_NAME games"

PKG_DATA_ID_CHAPTER1="${GAME_ID_CHAPTER1}-data"
PKG_DATA_ID_CHAPTER2="${GAME_ID_CHAPTER2}-data"
PKG_DATA_ID_CHAPTER3="${GAME_ID_CHAPTER3}-data"
PKG_DATA_ID_CHAPTER4="${GAME_ID_CHAPTER4}-data"
PKG_DATA_ID_CHAPTER5="${GAME_ID_CHAPTER5}-data"
PKG_DATA_ID_CHAPTER6="${GAME_ID_CHAPTER6}-data"
PKG_DATA_ID_CHAPTER7="${GAME_ID_CHAPTER7}-data"
PKG_DATA_ID_CHAPTER8="${GAME_ID_CHAPTER8}-data"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPS="$PKG_COMMON_ID"

PKG_BIN_ARCH='32'
PKG_BIN_DEPS_CHAPTER1="$PKG_DATA_ID_CHAPTER1"
PKG_BIN_DEPS_CHAPTER2="$PKG_DATA_ID_CHAPTER2"
PKG_BIN_DEPS_CHAPTER3="$PKG_DATA_ID_CHAPTER3"
PKG_BIN_DEPS_CHAPTER4="$PKG_DATA_ID_CHAPTER4"
PKG_BIN_DEPS_CHAPTER5="$PKG_DATA_ID_CHAPTER5"
PKG_BIN_DEPS_CHAPTER6="$PKG_DATA_ID_CHAPTER6"
PKG_BIN_DEPS_CHAPTER7="$PKG_DATA_ID_CHAPTER7"
PKG_BIN_DEPS_CHAPTER8="$PKG_DATA_ID_CHAPTER8"

# Allow to skip intro video on first launch + set default settings

registry_dump_settings_file='registry-dumps/default-settings.reg'
registry_dump_settings_content_common='"Animate SpellBook"=dword:00000001
"Autosave"=dword:00000001
"Bink Video"=dword:00000000
"Blackout Computer"=dword:00000000
"Combat Army Info Level"=dword:00000000
"Combat Auto Creatures"=dword:00000001
"Combat Auto Spells"=dword:00000001
"Combat Ballista"=dword:00000001
"Combat Catapult"=dword:00000001
"Combat First Aid Tent"=dword:00000001
"Combat Shade Level"=dword:00000000
"Combat Speed"=dword:00000000
"Computer Walk Speed"=dword:00000003
"First Time"=dword:00000000
"Last Music Volume"=dword:00000005
"Last Sound Volume"=dword:00000005
"Main Game Full Screen"=dword:00000001
"Main Game Show Menu"=dword:00000001
"Main Game X"=dword:0000000a
"Main Game Y"=dword:0000000a
"Move Reminder"=dword:00000001
"Music Volume"=dword:00000005
"Quick Combat"=dword:00000000
"Show Combat Grid"=dword:00000000
"Show Combat Mouse Hex"=dword:00000000
"Show Intro"=dword:00000001
"Show Route"=dword:00000001
"Sound Volume"=dword:00000005
"Test Blit"=dword:00000000
"Test Decomp"=dword:00000000
"Test Read"=dword:00000000
"Town Outlines"=dword:00000001
"Video Subtitles"=dword:00000001
"Walk Speed"=dword:00000002
"Window Scroll Speed"=dword:00000001'
## Multiple levels of escaping are required:
## - app_path_xxx  declaration: "\\\\\\\\" → "\\\\"
## - registry_dump_settings_content_xxx declaration: "\\\\" → "\\"
## We want to get "\\" in the final .reg file.
app_path_CHAPTER1="C:\\\\\\\\${GAME_ID_CHAPTER1}\\\\\\\\${CONTENT_INNER_PATH_CHAPTER1}\\\\\\\\"
app_path_CHAPTER2="C:\\\\\\\\${GAME_ID_CHAPTER2}\\\\\\\\${CONTENT_INNER_PATH_CHAPTER2}\\\\\\\\"
app_path_CHAPTER3="C:\\\\\\\\${GAME_ID_CHAPTER3}\\\\\\\\${CONTENT_INNER_PATH_CHAPTER3}\\\\\\\\"
app_path_CHAPTER4="C:\\\\\\\\${GAME_ID_CHAPTER4}\\\\\\\\${CONTENT_INNER_PATH_CHAPTER4}\\\\\\\\"
app_path_CHAPTER5="C:\\\\\\\\${GAME_ID_CHAPTER5}\\\\\\\\${CONTENT_INNER_PATH_CHAPTER5}\\\\\\\\"
app_path_CHAPTER6="C:\\\\\\\\${GAME_ID_CHAPTER6}\\\\\\\\${CONTENT_INNER_PATH_CHAPTER6}\\\\\\\\"
app_path_CHAPTER7="C:\\\\\\\\${GAME_ID_CHAPTER7}\\\\\\\\${CONTENT_INNER_PATH_CHAPTER7}\\\\\\\\"
app_path_CHAPTER8="C:\\\\\\\\${GAME_ID_CHAPTER8}\\\\\\\\${CONTENT_INNER_PATH_CHAPTER8}\\\\\\\\"
registry_dump_settings_content_CHAPTER1="Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\\Software\\New World Computing\\Heroes Chronicles\\Warlords]
\"AppPath\"=\"${app_path_CHAPTER1}\"
$registry_dump_settings_content_common"
registry_dump_settings_content_CHAPTER2="Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\\Software\\New World Computing\\Heroes Chronicles\\Underworld]
\"AppPath\"=\"${app_path_CHAPTER2}\"
$registry_dump_settings_content_common"
registry_dump_settings_content_CHAPTER3="Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\\Software\\New World Computing\\Heroes Chronicles\\Elements]
\"AppPath\"=\"${app_path_CHAPTER3}\"
$registry_dump_settings_content_common"
registry_dump_settings_content_CHAPTER4="Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\\Software\\New World Computing\\Heroes Chronicles\\Dragons]
\"AppPath\"=\"${app_path_CHAPTER4}\"
$registry_dump_settings_content_common"
registry_dump_settings_content_CHAPTER5="Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\\Software\\New World Computing\\Heroes Chronicles\\WorldTree]
\"AppPath\"=\"${app_path_CHAPTER5}\"
$registry_dump_settings_content_common"
registry_dump_settings_content_CHAPTER6="Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\\Software\\New World Computing\\Heroes Chronicles\\FieryMoon]
\"AppPath\"=\"${app_path_CHAPTER6}\"
$registry_dump_settings_content_common"
registry_dump_settings_content_CHAPTER7="Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\\Software\\New World Computing\\Heroes Chronicles\\Beastmaster]
\"AppPath\"=\"${app_path_CHAPTER7}\"
$registry_dump_settings_content_common"
registry_dump_settings_content_CHAPTER8="Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\\Software\\New World Computing\\Heroes Chronicles\\Sword]
\"AppPath\"=\"${app_path_CHAPTER8}\"
$registry_dump_settings_content_common"
CONTENT_GAME0_BIN_FILES="
$registry_dump_settings_file"
APP_REGEDIT="${APP_REGEDIT:-} $registry_dump_settings_file"
SCRIPT_DEPS="${SCRIPT_DEPS:-} iconv"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Allow to skip intro video on first launch + set default settings.
	registry_dump_settings_content=$(context_value 'registry_dump_settings_content')
	mkdir --parents "$(dirname "$registry_dump_settings_file")"
	printf '%s' "$registry_dump_settings_content" | \
		iconv --from-code=UTF-8 --to-code=UTF-16 \
		--output="$registry_dump_settings_file"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
## Use a common directory and symbolic links to handle data shared between all Heroes Chronicles games.
PATH_GAME_COMMON=$(path_game_data | sed "s/$(game_id)$/${GAME_ID}/")
content_inclusion 'GAME_COMMON' 'PKG_COMMON' "$PATH_GAME_COMMON"
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

## Use a common directory and symbolic links to handle data shared between all Heroes Chronicles games
PKG_DATA_POSTINST_RUN="$(package_postinst_actions 'PKG_DATA')
# Link common files shared by the games series
ln --symbolic '${PATH_GAME_COMMON}/data' '$(path_game_data)'
ln --symbolic '${PATH_GAME_COMMON}/mp3' '$(path_game_data)'"
PKG_DATA_PRERM_RUN="$(package_prerm_actions 'PKG_DATA')
# Delete links to common files shared by the games series
rm '$(path_game_data)/data'
rm '$(path_game_data)/mp3'"

(
	## Use the generic values of GAME_ID and GAME_NAME for the shared data package.
	unset "$(context_name_archive 'GAME_ID')"
	unset "$(context_name_archive 'GAME_NAME')"
	packages_generation 'PKG_COMMON'
)
packages_generation 'PKG_BIN' 'PKG_DATA'
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
