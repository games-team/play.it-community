#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 berru <berru@riseup.net>
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Dicey Dungeons
# send your bug reports to contact@dotslashplay.it
###

script_version=20230108.1

GAME_ID='dicey-dungeons'
GAME_NAME='Dicey Dungeons'

ARCHIVE_BASE_1='dicey-dungeons-linux64.zip'
ARCHIVE_BASE_1_MD5='7561697f602e3a0af054569e3a8114b3'
ARCHIVE_BASE_1_SIZE='110000'
ARCHIVE_BASE_1_VERSION='1.11-itch.2021.03.18'
ARCHIVE_BASE_1_URL='https://terrycavanagh.itch.io/dicey-dungeons'

ARCHIVE_BASE_0='dicey-dungeons-linux64.zip'
ARCHIVE_BASE_0_MD5='14879aa94aef2291d6aec0c4c9e760c5'
ARCHIVE_BASE_0_SIZE='450000'
ARCHIVE_BASE_0_VERSION='1.10-itch.2020.11.05'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
diceydungeons
lime.ndll'
CONTENT_GAME_DATA_FILES='
data
manifest
mods
soundstuff'

APP_MAIN_EXE='diceydungeons'
APP_MAIN_ICON='data/icon.png'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6'

# Build 512×512 icon from the 1024×1024 provided one

SCRIPT_DEPS="$SCRIPT_DEPS convert"

# Load common functions

target_version='2.20'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Build 512×512 icon from the 1024×1024 provided one
	icon_source="$APP_MAIN_ICON"
	icon_destination="${APP_MAIN_ICON%.png}_512.png"
	convert "$icon_source" -resize 512 "$icon_destination"
)

# Include game icons

## Build 512×512 icon from the 1024×1024 provided one
APP_MAIN_ICON_512="${APP_MAIN_ICON%.png}_512.png"
APP_MAIN_ICONS_LIST='APP_MAIN_ICON APP_MAIN_ICON_512'

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
