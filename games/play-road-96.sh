#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Road 96
# send your bug reports to contact@dotslashplay.it
###

script_version=20230709.1

GAME_ID='road-96'
GAME_NAME='Road 96'

ARCHIVE_BASE_0='setup_road_96_1.04_(64bit)_(51993).exe'
ARCHIVE_BASE_0_MD5='f0d88097b5e6c44687e073c4abef816b'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1='setup_road_96_1.04_(64bit)_(51993)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='608d310ebb3154a0e85bad40175f9189'
ARCHIVE_BASE_0_PART2='setup_road_96_1.04_(64bit)_(51993)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='f0d523facddf8c40c18fd6b1721a8855'
ARCHIVE_BASE_0_SIZE='14000000'
ARCHIVE_BASE_0_VERSION='1.04-gog51993'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/road_96'

UNITY3D_NAME='road 96'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES="
${UNITY3D_NAME}.exe
${UNITY3D_NAME}_data/plugins
baselib.dll
gameassembly.dll
unityplayer.dll"
CONTENT_GAME_DATA_RESOURCES_FILES="
${UNITY3D_NAME}_data/*.ress"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_data"

WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/DigixArt/Road 96'

APP_MAIN_TYPE='wine'
APP_MAIN_EXE="${UNITY3D_NAME}.exe"

PACKAGES_LIST='PKG_BIN PKG_DATA_RESOURCES PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_DATA_RESOURCES_ID="${PKG_DATA_ID}-resources"
PKG_DATA_RESOURCES_DESCRIPTION="$PKG_DATA_DESCRIPTION - resources"
PKG_DATA_DEPS="${PKG_DATA_DEPS:-} $PKG_DATA_RESOURCES_ID"

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

target_version='2.24'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Delete unwanted files
	rm --recursive \
		'__redist' \
		'app' \
		'commonappdata' \
		'tmp'
)

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
