#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 BetaRays
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Multiwinia
# send your bug reports to contact@dotslashplay.it
###

script_version=20230817.1

GAME_ID='multiwinia'
GAME_NAME='Multiwinia'

ARCHIVE_BASE_0='gog_multiwinia_2.0.0.5.sh'
ARCHIVE_BASE_0_MD5='ec7f0cc245b4fb4bf85cba5fc4a536ba'
ARCHIVE_BASE_0_SIZE='66000'
ARCHIVE_BASE_0_VERSION='1.3.1-gog2.0.0.5'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/multiwinia'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN32_PATH="${CONTENT_PATH_DEFAULT}/lib"
CONTENT_LIBS_BIN32_FILE='
libpng12.so.0
libpng12.so.0.50.0'
CONTENT_GAME_BIN32_FILES='
multiwinia.bin.x86'
CONTENT_LIBS_BIN64_PATH="${CONTENT_PATH_DEFAULT}/lib64"
CONTENT_LIBS_BIN64_FILE='
libpng12.so.0
libpng12.so.0.50.0'
CONTENT_GAME_BIN64_FILES='
multiwinia.bin.x86_64'
CONTENT_GAME_DATA_FILES='
*.dat
multiwinia.png'
CONTENT_DOC_DATA_PATH="${CONTENT_PATH_DEFAULT}/docs"
CONTENT_DOC_DATA_FILES='
readme.txt
manual.pdf'

APP_MAIN_EXE_BIN32='multiwinia.bin.x86'
APP_MAIN_EXE_BIN64='multiwinia.bin.x86_64'
APP_MAIN_ICON='multiwinia.png'

PACKAGES_LIST='PKG_BIN32 PKG_BIN64 PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN32_ARCH='32'
PKG_BIN64_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libgcc_s.so.1
libGL.so.1
libGLU.so.1
libm.so.6
libogg.so.0
libopenal.so.1
libpthread.so.0
libSDL-1.2.so.0
libstdc++.so.6
libvorbisfile.so.3
libvorbis.so.0
libz.so.1'
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

target_version='2.25'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

for PKG in 'PKG_BIN32' 'PKG_BIN64'; do
	# shellcheck disable=SC2119
	launchers_write
done

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
