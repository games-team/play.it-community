#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 BetaRays
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The King's Bird
# send your bug reports to contact@dotslashplay.it
###

script_version=20230805.1

GAME_ID='the-kings-bird'
GAME_NAME='The Kingʼs Bird'

ARCHIVE_BASE_0='TheKingsBirdDRMFREE_v5.zip'
ARCHIVE_BASE_0_MD5='fccb138fa993f1e983fcc6c08d1b0b96'
ARCHIVE_BASE_0_SIZE='5400000'
ARCHIVE_BASE_0_VERSION='5-itch'
ARCHIVE_BASE_0_URL='https://graffiti-games.itch.io/the-kings-bird'

UNITY3D_NAME='TheKingsBird'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES="
${UNITY3D_NAME}_Data/Mono
${UNITY3D_NAME}_Data/Plugins
${UNITY3D_NAME}.exe"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data"

WINE_PERSISTENT_DIRECTORIES="
users/\${USER}/AppData/LocalLow/Serenity Forge/The King's Bird"

APP_MAIN_EXE='TheKingsBird.exe'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

target_version='2.24'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
