#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Bad Mojo
# send your bug reports to contact@dotslashplay.it
###

script_version=20230803.1

GAME_ID='bad-mojo'
GAME_NAME='Bad Mojo'

ARCHIVE_BASE_0='setup_badmojo_2.0.0.3.exe'
ARCHIVE_BASE_0_MD5='aedcb9e57bf5c6e692b889d3c21c4d12'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='770000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.0.0.3'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/bad_mojo_redux'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
badmojo.exe
launcher2.bat
launcher.exe
qthook.dll
waveout.dll'
CONTENT_GAME_DATA_FILES='
backgnd
btc
cel
extras
mash
movie
over
palette
qtsystem
roachd
save
scribble
script
sound
topo
quicktime.qts'

USER_PERSISTENT_DIRECTORIES='
save'

APP_MAIN_EXE='badmojo.exe'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

target_version='2.24'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Create required configuration file

config_file='badmojo.ini'
config_file_path="$(package_path 'PKG_BIN')$(path_game_data)/${config_file}"
mkdir --parents "$(dirname "$config_file_path")"
game_id=$(game_id)
cat > "$config_file_path" << EOF
[booger]
BACKGROUND=C:\\${game_id}\\OVER;C:\\${game_id}\\BACKGND
TOPO=C:\\${game_id}\\TOPO
BTC=C:\\${game_id}\\BTC
PALETTE=C:\\${game_id}\\PALETTE
SCRIBBLE=C:\\${game_id}\\OVER;C:\\${game_id}\\SCRIBBLE
CEL=C:\\${game_id}\\OVER;C:\\${game_id}\\CEL
SCRIPT=C:\\${game_id}\\SCRIPT
ROACH=C:\\${game_id}\\ROACHD
MOVIE=C:\\${game_id}\\OVER;C:\\${game_id}\\MOVIE
MASH=C:\\${game_id}\\OVER;C:\\${game_id}\\MASH
SOUND=C:\\${game_id}\\SOUND
SAVE=C:\\${game_id}\\SAVE
[SoundMix]
SoundOn=1
BetweenScreens=1
SmallChunkSize=3
BigChunkSize=26
BigChunkMethod=0
DumbSound=0
[badmojo]
Preferences=1111110
EOF
USER_PERSISTENT_FILES="$(persistent_list_files)
$config_file"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
