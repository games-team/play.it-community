#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# A House of Many Doors
# send your bug reports to contact@dotslashplay.it
###

script_version=20240531.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='a-house-of-many-doors'
GAME_NAME='A House of Many Doors'

ARCHIVE_BASE_0_NAME='A House of Many Doors.zip'
ARCHIVE_BASE_0_MD5='bf3714af567c8e6290577af2d744dd0a'
ARCHIVE_BASE_0_SIZE='463417'
ARCHIVE_BASE_0_VERSION='1.2-itch.2017.03.30'
ARCHIVE_BASE_0_URL='https://pixeltrickery.itch.io/a-house-of-many-doors'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
D3DX9_43.dll
A House of Many Doors.exe
HOMD.homd
Game Data.ini
options.ini'
CONTENT_GAME_DATA_FILES='
Images
audiogroup*.dat
*.ogg
penumbra_s.png
data.win'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Local/A_House_of_Many_Doors'

APP_MAIN_EXE='A House of Many Doors.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
