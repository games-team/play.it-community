#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Legend of Grimrock
# send your bug reports to contact@dotslashplay.it
###

script_version=20231108.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='legend-of-grimrock'
GAME_NAME='Legend of Grimrock'

ARCHIVE_BASE_0_NAME='gog_legend_of_grimrock_2.1.0.5.sh'
ARCHIVE_BASE_0_MD5='b63089766247484f5d2b214d924425f6'
ARCHIVE_BASE_0_SIZE='690000'
ARCHIVE_BASE_0_VERSION='1.3.7-gog2.1.0.5'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/legend_of_grimrock'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN64_FILES='
Grimrock.bin.x86_64'
CONTENT_GAME_BIN32_FILES='
Grimrock.bin.x86'
CONTENT_GAME_DATA_FILES='
grimrock.dat
grimrock.png'
CONTENT_DOC_DATA_FILES='
README.linux'

APP_MAIN_EXE_BIN32='Grimrock.bin.x86'
APP_MAIN_EXE_BIN64='Grimrock.bin.x86_64'
APP_MAIN_ICON='grimrock.png'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN32_ARCH='32'
PKG_BIN64_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libfreeimage.so.3
libfreetype.so.6
libgcc_s.so.1
libGL.so.1
libminizip.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libstdc++.so.6
libvorbisfile.so.3
libX11.so.6
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN64'
# shellcheck disable=SC2119
launchers_write
set_current_package 'PKG_BIN32'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
