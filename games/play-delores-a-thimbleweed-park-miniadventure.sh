#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Delores: A Thimbleweed Park Mini-Adventure
# send your bug reports to contact@dotslashplay.it
###

script_version=20240516.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='delores-a-thimbleweed-park-mini-adventure'
GAME_NAME='Delores: A Thimbleweed Park Mini-Adventure'

ARCHIVE_BASE_0_NAME='setup_delores_a_thimbleweed_park_mini-adventure_1.1.191_(64bit)_(38487).exe'
ARCHIVE_BASE_0_MD5='30b609714d47171a84c8d41c1a1c038a'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='77000'
ARCHIVE_BASE_0_VERSION='1.1.1.191-gog38487'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/delores_a_thimbleweed_park_miniadventure'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
*.dll
delores.exe'
CONTENT_GAME_DATA_FILES='
delores.ggpack1
delores.ico
delores.rc'

## The game expects to be running on top of Windows ≥ 10.
WINE_WINETRICKS_VERBS="${WINE_WINETRICKS_VERBS:-} win10"
## A Windows native build of d3dcompiler is required.
WINE_WINETRICKS_VERBS="${WINE_WINETRICKS_VERBS:-} d3dcompiler_47"

APP_MAIN_EXE='delores.exe'
APP_MAIN_ICON='delores.ico'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
