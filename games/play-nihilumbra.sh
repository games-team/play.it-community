#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Nihilumbra
# send your bug reports to contact@dotslashplay.it
###

script_version=20241107.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='nihilumbra'
GAME_NAME='Nihilumbra'

ARCHIVE_BASE_64BIT_0_NAME='Nihilumbra-1.35-linux64.tar.gz'
ARCHIVE_BASE_64BIT_0_MD5='18aa096020cedea4f208ca55f7e5c85f'
ARCHIVE_BASE_64BIT_0_SIZE='2304011'
ARCHIVE_BASE_64BIT_0_VERSION='1.35-humble150122'
ARCHIVE_BASE_64BIT_0_URL='https://www.humblebundle.com/store/nihilumbra'

ARCHIVE_BASE_32BIT_0_NAME='Nihilumbra-1.35-linux32.tar.gz'
ARCHIVE_BASE_32BIT_0_MD5='24ba59112bdb95b05651ebe48ec5882d'
ARCHIVE_BASE_32BIT_0_SIZE='2301254'
ARCHIVE_BASE_32BIT_0_VERSION='1.35-humble150122'
ARCHIVE_BASE_32BIT_0_URL='https://www.humblebundle.com/store/nihilumbra'

UNITY3D_NAME='Nihilumbra'
UNITY3D_PLUGINS='
ScreenSelector.so'

CONTENT_PATH_DEFAULT_64BIT='Nihilumbra-1.35-linux64'
CONTENT_PATH_DEFAULT_32BIT='Nihilumbra-1.35-linux32'

## APP_MAIN_ICON is set implicitly for all Unity3D games
APP_MAIN_ICONS_LIST='APP_MAIN_ICON APP_MAIN_ICON_32 APP_MAIN_ICON_48_PNG APP_MAIN_ICON_48_XPM APP_MAIN_ICON_64 APP_MAIN_ICON_128'
APP_MAIN_ICON_32='icon32x32.png'
APP_MAIN_ICON_48_PNG='icon48x48.png'
APP_MAIN_ICON_48_XPM='icon48x48.xpm'
APP_MAIN_ICON_64='icon64x64.png'
APP_MAIN_ICON_128='icon128x128.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH_32BIT='32'
PKG_BIN_ARCH_64BIT='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libatk-1.0.so.0
libcairo.so.2
libc.so.6
libdl.so.2
libfontconfig.so.1
libfreetype.so.6
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libgdk-x11-2.0.so.0
libgio-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libGLU.so.1
libgmodule-2.0.so.0
libgobject-2.0.so.0
libgthread-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpango-1.0.so.0
libpangocairo-1.0.so.0
libpangoft2-1.0.so.0
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXext.so.6
libXrandr.so.2'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

## Do not disable the MAP_32BIT flag, as this game is not affected by the bug it works around
unity3d_disable_map32bit() { true; }

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
