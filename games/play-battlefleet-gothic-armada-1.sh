#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Battlefleet Gothic: Armada
# send your bug reports to contact@dotslashplay.it
###

script_version=20240611.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='battlefleet-gothic-armada-1'
GAME_NAME='Battlefleet Gothic: Armada'

ARCHIVE_BASE_0_NAME='setup_battlefleet_gothic_armada_1.0.12_(64bit)_(41732).exe'
ARCHIVE_BASE_0_MD5='74271753f6cba4385e50c3f8c05b32da'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_battlefleet_gothic_armada_1.0.12_(64bit)_(41732)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='bea83722e900a3253ca68600723e5853'
ARCHIVE_BASE_0_PART2_NAME='setup_battlefleet_gothic_armada_1.0.12_(64bit)_(41732)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='246e012f15d4bcbbac9a024f1b6816a6'
ARCHIVE_BASE_0_SIZE='14000000'
ARCHIVE_BASE_0_VERSION='1.0.12-gog41732'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/battlefleet_gothic_armada_1'

UNREALENGINE4_NAME='battlefleetgothic'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME0_BIN_FILES="
${UNREALENGINE4_NAME}/version.bfgver"

HUGE_FILES_DATA="
${UNREALENGINE4_NAME}/content/paks/${UNREALENGINE4_NAME}-windowsnoeditor.pak"

APP_MAIN_EXE="${UNREALENGINE4_NAME}.exe"

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
