#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# BIT RAT: Singularity
# send your bug reports to contact@dotslashplay.it
###

script_version=20250303.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='bit-rat-singularity'
GAME_NAME='BIT RAT: Singularity'

ARCHIVE_BASE_0='bit_rat_singularity_linux.zip'
ARCHIVE_BASE_0_MD5='a2780bc1b25a6b07116dc739139bb561'
ARCHIVE_BASE_0_SIZE='33000'
ARCHIVE_BASE_0_VERSION='1.0-itch1'
ARCHIVE_BASE_0_URL='https://bucketdrumgames.itch.io/bitrat'

CONTENT_PATH_DEFAULT='bit_rat_singularity'
CONTENT_GAME_BIN_FILES='
runner'
CONTENT_GAME_DATA_FILES='
assets'

APP_MAIN_EXE='runner'
APP_MAIN_ICON='assets/icon.png'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libGLU.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
librt.so.1
libssl.so.1.0.0
libstdc++.so.6
libX11.so.6
libXext.so.6
libXrandr.so.2
libXxf86vm.so.1
libz.so.1'

# Build 512×512 icon from the 1024×1024 provided one

SCRIPT_DEPS="${SCRIPT_DEPS:-} convert"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game icons

PKG='PKG_DATA'
## Build 512×512 icon from the 1024×1024 provided one
APP_MAIN_ICONS_LIST="$(application_icons_list 'APP_MAIN') APP_MAIN_ICON_512"
APP_MAIN_ICON_512='assets/icon_512.png'
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"
	convert "$APP_MAIN_ICON" -resize 512 "$APP_MAIN_ICON_512"
)
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
