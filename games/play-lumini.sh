#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Lumini
# send your bug reports to contact@dotslashplay.it
###

script_version=20231103.7

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='lumini'
GAME_NAME='Lumini'

## This game is no longer available for sale from humblebundle.com.
ARCHIVE_BASE_0_NAME='Lumini_DRMfree_Linux.zip'
ARCHIVE_BASE_0_MD5='7d70f1824be9ab701cf9d52fb12a039a'
ARCHIVE_BASE_0_SIZE='4700000'
ARCHIVE_BASE_0_VERSION='1.0-humble1'

UNITY3D_NAME='lumini'
UNITY3D_PLUGINS='
libfmod.so
libfmodstudio.so
ScreenSelector.so'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME0_DATA_FILES='
SavesDir'

USER_PERSISTENT_DIRECTORIES='
SavesDir'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXrandr.so.2'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Merge assets directories for the 32-bit and 64-bit builds.
	mkdir "$(unity3d_name)_Data"
	cp --link --recursive --update 'lumini_Linux64_Data/'* "$(unity3d_name)_Data"
	cp --link --recursive --update 'lumini_Linux32_Data/'* "$(unity3d_name)_Data"
	rm --recursive 'lumini_Linux64_Data' 'lumini_Linux32_Data'

	## Rename the binaries.
	mv 'lumini_Linux64.x86_64' "$(unity3d_name).x86_64"
	mv 'lumini_Linux32.x86' "$(unity3d_name).x86"

	## Delete files that are not used by the Linux build.
	rm \
		"$(unity3d_name)_Data/Mono/"*/*.dll \
		"$(unity3d_name)_Data/Mono/"*/*.dll.meta

	## Delete duplicated libraries.
	rm \
		"$(unity3d_name)_Data/Mono/"*/libfmod.so \
		"$(unity3d_name)_Data/Mono/"*/libfmodstudio.so \
		"$(unity3d_name)_Data/Mono/"*/ScreenSelector.so

	## These directories must be deleted to prevent an error during the inclusion of Unity3D plugins.
	rm --recursive "$(unity3d_name)_Data/Plugins/"*.bundle/
)

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN64'
# shellcheck disable=SC2119
launchers_write
set_current_package 'PKG_BIN32'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
