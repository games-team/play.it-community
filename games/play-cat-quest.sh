#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Cat Quest series:
# - Cat Quest 1
# - Cat Quest 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20240707.3

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID_EPISODE1='cat-quest-1'
GAME_NAME_EPISODE1='Cat Quest'

GAME_ID_EPISODE2='cat-quest-2'
GAME_NAME_EPISODE2='Cat Quest II'

ARCHIVE_BASE_EPISODE1_0_NAME='setup_cat_quest_1.2.10.2_(42782).exe'
ARCHIVE_BASE_EPISODE1_0_MD5='496d069ea28098aa6d575fc56f8c71ec'
ARCHIVE_BASE_EPISODE1_0_TYPE='innosetup'
ARCHIVE_BASE_EPISODE1_0_SIZE='334416'
ARCHIVE_BASE_EPISODE1_0_VERSION='1.2.10.2-gog42782'
ARCHIVE_BASE_EPISODE1_0_URL='https://www.gog.com/en/game/cat_quest'

ARCHIVE_BASE_EPISODE2_0_NAME='setup_cat_quest_ii_1.7.7.3_(51746).exe'
ARCHIVE_BASE_EPISODE2_0_MD5='78f1001f07099b398ce66d971bf9b17c'
ARCHIVE_BASE_EPISODE2_0_TYPE='innosetup'
ARCHIVE_BASE_EPISODE2_0_SIZE='511796'
ARCHIVE_BASE_EPISODE2_0_VERSION='1.7.7.3-gog51746'
ARCHIVE_BASE_EPISODE2_0_URL='https://www.gog.com/en/game/cat_quest_ii'

UNITY3D_NAME_EPISODE1='cat quest'
UNITY3D_NAME_EPISODE2='cat quest ii'

CONTENT_PATH_DEFAULT='.'

WINE_PERSISTENT_DIRECTORIES_EPISODE1='
users/${USER}/AppData/LocalLow/The Gentlebros Pte_ Ltd_/Cat Quest'
WINE_PERSISTENT_DIRECTORIES_EPISODE2='
users/${USER}/Documents/Cat Quest II'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID_EPISODE1="${GAME_ID_EPISODE1}-data"
PKG_DATA_ID_EPISODE2="${GAME_ID_EPISODE2}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS_EPISODE1="$PKG_DATA_ID_EPISODE1"
PKG_BIN_DEPS_EPISODE2="$PKG_DATA_ID_EPISODE2"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
