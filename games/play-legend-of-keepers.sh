#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Legend of Keepers
# send your bug reports to contact@dotslashplay.it
###

script_version=20231225.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='legend-of-keepers'
GAME_NAME='Legend of Keepers: Career of a Dungeon Manager'

GAME_ID_DEMO="${GAME_ID}-demo"
GAME_NAME_DEMO='Legend of Keepers: Prologue'

ARCHIVE_BASE_0_NAME='legend_of_keepers_career_of_a_dungeon_manager_1_1_0_2_57134.sh'
ARCHIVE_BASE_0_MD5='a1d7f09037caaa7212cfd8a61a992289'
ARCHIVE_BASE_0_SIZE='1113810'
ARCHIVE_BASE_0_VERSION='1.1.0.2-gog57134'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/legend_of_keepers_career_of_a_dungeon_master'

ARCHIVE_BASE_DEMO_0_NAME='legend_of_keepers_prologue_0_9_2_1_44469.sh'
ARCHIVE_BASE_DEMO_0_MD5='af73802b552219f834b53734b955906b'
ARCHIVE_BASE_DEMO_0_SIZE='1012855'
ARCHIVE_BASE_DEMO_0_VERSION='0.9.2.1-gog44469'
ARCHIVE_BASE_DEMO_0_URL='https://www.gog.com/game/legend_of_keepers_prologue'

UNITY3D_NAME='LegendOfKeepers'
UNITY3D_NAME_DEMO='LegendOfKeepersPrologue'
## TODO: Check if the Steam library is required.
UNITY3D_PLUGINS='
libEOSSDK-Linux-Shipping.so
libfmodstudioL.so
libfmodstudio.so
libresonanceaudio.so
libsteam_api.so'

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_DEMO="${GAME_ID_DEMO}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPS_DEMO="$PKG_DATA_ID_DEMO"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
