#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Obduction
# Send your bug reports to contact@dotslashplay.it
###

script_version=20231018.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='obduction'
GAME_NAME='Obduction'

ARCHIVE_BASE_0_NAME='setup_obduction_1.8.4.1-ssl_(51210).exe'
ARCHIVE_BASE_0_MD5='02477c05bece9e1655adc31bbc5a89fa'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_obduction_1.8.4.1-ssl_(51210)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='834211d7c8218965106369575f72c3ac'
ARCHIVE_BASE_0_PART2_NAME='setup_obduction_1.8.4.1-ssl_(51210)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='73e5a3a0b413f24ea0e7348e86301b11'
ARCHIVE_BASE_0_SIZE='9300000'
ARCHIVE_BASE_0_VERSION='1.8.4.1-gog51210'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/obduction'

UNREALENGINE4_NAME='obduction'

CONTENT_PATH_DEFAULT='.'
CONTENT_DOC_DATA_FILES='
obduction_user_manual.pdf'

APP_MAIN_EXE="${UNREALENGINE4_NAME}.exe"

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
