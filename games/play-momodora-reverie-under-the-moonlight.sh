#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2019 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Momodora: Reverie Under the Moonlight
# send your bug reports to contact@dotslashplay.it
###

script_version=20250303.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='momodora-reverie-under-the-moonlight'
GAME_NAME='Momodora: Reverie Under the Moonlight'

ARCHIVE_BASE_2='momodora_reverie_under_the_moonlight_1_063_43451.sh'
ARCHIVE_BASE_2_MD5='b10b3d929d6ccac6249581cefcc43285'
ARCHIVE_BASE_2_SIZE='330000'
ARCHIVE_BASE_2_VERSION='1.063-gog43451'
ARCHIVE_BASE_2_URL='https://www.gog.com/game/momodora_reverie_under_the_moonlight'

ARCHIVE_BASE_1='momodora_reverie_under_the_moonlight_1_062_24682.sh'
ARCHIVE_BASE_1_MD5='9da233f084d0a86e4068ca90c89e4f05'
ARCHIVE_BASE_1_SIZE='330000'
ARCHIVE_BASE_1_VERSION='1.062-gog24682'

ARCHIVE_BASE_0='momodora_reverie_under_the_moonlight_en_20180418_20149.sh'
ARCHIVE_BASE_0_MD5='5ec0d0e8475ced69fbaf3881652d78c1'
ARCHIVE_BASE_0_SIZE='330000'
ARCHIVE_BASE_0_VERSION='1.02a-gog20149'

CONTENT_PATH_DEFAULT='data/noarch/game/GameFiles'
CONTENT_GAME_BIN_FILES='
MomodoraRUtM'
CONTENT_GAME_DATA_FILES='
assets'
CONTENT_DOC_DATA_PATH='data/noarch/game'
CONTENT_DOC_DATA_FILES='
Installation Notes.pdf
Update.txt'

USER_PERSISTENT_FILES='
assets/*.ini'

APP_MAIN_PRERUN='export LANG=C'
APP_MAIN_EXE='MomodoraRUtM'
APP_MAIN_ICON='assets/icon.png'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libcurl.so.4+CURL_OPENSSL_3
libdl.so.2
libgcc_s.so.1
libGL.so.1
libGLU.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
librt.so.1
libssl.so.1.0.0
libstdc++.so.6
libX11.so.6
libXext.so.6
libXrandr.so.2
libXxf86vm.so.1
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
