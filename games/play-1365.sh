#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# 1365
# send your bug reports to contact@dotslashplay.it
###

script_version=20240504.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='1365-game'
GAME_NAME='1365'

ARCHIVE_BASE_0_NAME='1365 Linux 1.0'
ARCHIVE_BASE_0_MD5='8cb8fb11a2df4af72154f2909238c09c'
ARCHIVE_BASE_0_TYPE='file'
ARCHIVE_BASE_0_SIZE='32000'
ARCHIVE_BASE_0_VERSION='1.0.0-itch1'
ARCHIVE_BASE_0_URL='https://shadybug.itch.io/1365'

## Optional icons pack
ARCHIVE_OPTIONAL_ICONS_NAME='1356_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_MD5='f820888c924fed091c5d64d9aaa2d467'
ARCHIVE_OPTIONAL_ICONS_URL='https://downloads.dotslashplay.it/games/1365/'
CONTENT_ICONS_PATH='.'
CONTENT_ICONS_FILES='
256x256
512x512'

APP_MAIN_EXE='1365 Linux 1.0'

PKG_MAIN_ARCH='32'
PKG_MAIN_DEPENDENCIES_LIBRARIES='
libasound.so.2
libc.so.6
libdl.so.2
libGL.so.1
libm.so.6
libpthread.so.0
libpulse-simple.so.0
libpulse.so.0
libX11.so.6
libXcursor.so.1
libXinerama.so.1
libXi.so.6
libXrandr.so.2'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Include game data

content_inclusion_icons
install -D --mode=755 \
	"$(archive_path "$(current_archive)")" \
	"$(package_path 'PKG_MAIN')$(path_game_data)/$(application_exe 'APP_MAIN')"

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
