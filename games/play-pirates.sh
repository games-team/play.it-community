#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Pirates series:
# - Pirates
# - Pirates Gold
# send your bug reports to contact@dotslashplay.it
###

script_version=20240621.2

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='pirates'
GAME_NAME='Pirates!'

GAME_ID_GOLD='pirates-gold'
GAME_NAME_GOLD='Pirates Gold'

ARCHIVE_BASE_0_NAME='gog_pirates_2.0.0.8.sh'
ARCHIVE_BASE_0_MD5='885cdb8f2119a278fb15a32cafad64a8'
ARCHIVE_BASE_0_SIZE='12000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.0.0.8'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/pirates_gold_plus'

ARCHIVE_BASE_GOLD_1_NAME='gog_pirates_gold_2.0.0.9.sh'
ARCHIVE_BASE_GOLD_1_MD5='1eec642e9d7337b9456775d110760a80'
ARCHIVE_BASE_GOLD_1_SIZE='39000'
ARCHIVE_BASE_GOLD_1_VERSION='1.0-gog2.0.0.9'
ARCHIVE_BASE_GOLD_1_URL='https://www.gog.com/game/pirates_gold_plus'

ARCHIVE_BASE_GOLD_0_NAME='gog_pirates_gold_2.0.0.8.sh'
ARCHIVE_BASE_GOLD_0_MD5='ea602d91950cffa615abae567f498989'
ARCHIVE_BASE_GOLD_0_SIZE='39000'
ARCHIVE_BASE_GOLD_0_VERSION='1.0-gog2.0.0.8'

CONTENT_PATH_DEFAULT='data/noarch/classic/Pirates!'
CONTENT_PATH_DEFAULT_GOLD='data/noarch/data'
## FIXME: An explicit list of files should be set.
CONTENT_GAME_MAIN_FILES='
*'
CONTENT_GAME_MAIN_FILES_GOLD='
MPS
PIRATESC
PIRATESG.CD
DATA.DAT'
CONTENT_DOC_MAIN_PATH_GOLD='data/noarch/docs'
CONTENT_DOC_MAIN_FILES_GOLD='
flags.pdf
manual.pdf'

GAME_IMAGE_GOLD='DATA.DAT'
GAME_IMAGE_TYPE='iso'

APP_MAIN_EXE='pir.exe'
APP_MAIN_EXE_GOLD='PIRATESG.EXE'
APP_MAIN_OPTIONS_GOLD='%2 %3 %4 %5'
APP_MAIN_DOSBOX_PRERUN_GOLD='
d:
cd PIRATESG.CD
lh cdpatch %1
'
APP_MAIN_DOSBOX_POSTRUN_GOLD='
cdpatch x
'
APP_MAIN_ICON='../../support/icon.png'
APP_MAIN_ICON_GOLD='piratesg.ico'
## The type can not be omitted, because the binary is actually on the CD-ROM image.
APP_MAIN_TYPE_GOLD='dosbox'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

case "$(current_archive)" in
	('ARCHIVE_BASE_GOLD_'*)
		## Work around the binary presence check,
		## it is actually included in the CD-ROM image.
		launcher_target_presence_check() { return 0; }
		## Set the context-specific values of some variables.
		GAME_IMAGE=$(context_value 'GAME_IMAGE')
		APP_MAIN_DOSBOS_PRERUN=$(context_value 'APP_MAIN_DOSBOS_PRERUN')
		APP_MAIN_DOSBOS_POSTRUN=$(context_value 'APP_MAIN_DOSBOS_POSTRUN')
	;;
esac
launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
