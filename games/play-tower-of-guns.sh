#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Tower of Guns
# send your bug reports to contact@dotslashplay.it
###

script_version=20230103.1

GAME_ID='tower-of-guns'
GAME_NAME='Tower of Guns'

## This Linux native DRM-free installer is no longer avaiable for sale from humblebundle.com
ARCHIVE_BASE_0='TowerOfGuns-Linux-1.27-2015021101-g_fix.sh'
ARCHIVE_BASE_0_MD5='45fae40e529e678c9129f9ee2dc8694b'
ARCHIVE_BASE_0_TYPE='mojosetup'
ARCHIVE_BASE_0_SIZE='950000'
ARCHIVE_BASE_0_VERSION='1.27-humble160104'

CONTENT_PATH_DEFAULT='data'
CONTENT_LIBS_BIN_PATH="${CONTENT_PATH_DEFAULT}/x86/Binaries/Linux/lib"
CONTENT_LIBS_BIN_FILES='
libPhysXCore.so
libPhysXCooking.so
libSDL2_mixer-2.0.so.0
libtcmalloc.so.0'
CONTENT_GAME_BIN_PATH="${CONTENT_PATH_DEFAULT}/x86"
CONTENT_GAME_BIN_FILES='
Binaries/Linux/UDKGame-Linux'
CONTENT_GAME_DATA_PATH="${CONTENT_PATH_DEFAULT}/noarch"
CONTENT_GAME_DATA_FILES='
Engine
UDKGame
ToGIcon.bmp
TowerOfGunsIcon.png'
CONTENT_DOC_DATA_PATH="${CONTENT_PATH_DEFAULT}/noarch"
CONTENT_DOC_DATA_FILES='
*.txt
README.linux'

APP_MAIN_EXE='Binaries/Linux/UDKGame-Linux'
APP_MAIN_ICON='noarch/TowerOfGunsIcon.png'
APP_MAIN_PRERUN='export LANG=C'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libogg.so.0
libopenal.so.1
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libstdc++.so.6
libvorbisfile.so.3
libvorbis.so.0'

# Run the game binary from its parent directory

APP_MAIN_PRERUN="$APP_MAIN_PRERUN"'
# Run the game binary from its parent directory
cd "$(dirname "$APP_EXE")"
APP_EXE=$(basename "$APP_EXE")'

# Load common functions

target_version='2.20'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game date

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build package

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
