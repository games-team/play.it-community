#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 berru <berru@riseup.net>
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Invisible Inc expansions:
# - Contingency Plan
# send your bug reports to contact@dotslashplay.it
###

script_version=20230414.1

GAME_ID='invisible-inc'
GAME_NAME='Invisible Inc.'

EXPANSION_ID_CONTINGENCY='contingency-plan'
EXPANSION_NAME_CONTINGENCY='Contigency Plan'


ARCHIVE_BASE_0='invisible_inc_contingency_plan_dlc_en_n_a_15873.sh'
ARCHIVE_BASE_0_MD5='adfe52f0e0c3400c3026fca66acf5acb'
ARCHIVE_BASE_0_SIZE='2700'
ARCHIVE_BASE_0_VERSION='1.0-gog15873'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/invisible_inc_contingency_plan'

CONTENT_GAME_MAIN_PATH='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
*'

PKG_MAIN_ID_CONTINGENCY="${GAME_ID}-${EXPANSION_ID_CONTINGENCY}"
PKG_MAIN_DESCRIPTION_CONTINGENCY="$EXPANSION_NAME_CONTINGENCY"
PKG_MAIN_DEPS="$GAME_ID"

# Load common functions

target_version='2.23'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
