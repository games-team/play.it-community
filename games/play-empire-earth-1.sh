#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Empire Earth
# send your bug reports to contact@dotslashplay.it
###

script_version=20241217.2

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='empire-earth-1'
GAME_NAME='Empire Earth'

GAME_ID_CONQUEST='the-art-of-conquest'
GAME_NAME_CONQUEST='The Art of Conquest'

ARCHIVE_BASE_1_NAME='setup_empire_earth_gold_2.0.0.2974_gog_v3_(78415).exe'
ARCHIVE_BASE_1_MD5='fcad43c5606236f5fe5023a40f6b539e'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='703925'
ARCHIVE_BASE_1_VERSION='2.0.0.2974-gog78415'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/empire_earth_gold_edition'

ARCHIVE_BASE_0_NAME='setup_empire_earth_gold_2.0.0.2974_(25522).exe'
ARCHIVE_BASE_0_MD5='e8b22cdc9520aefab49e21349b20b731'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='710000'
ARCHIVE_BASE_0_VERSION='2.0.0.2974-gog25522'

# Archive content

CONTENT_PATH_DEFAULT='.'

## Empire Earth - base game

CONTENT_GAME_BIN_RELATIVE_PATH='empire earth'
CONTENT_GAME_BIN_FILES='
redist
*.cfg
*.dll
*.exe
*.inf'
CONTENT_GAME_DATA_RELATIVE_PATH='empire earth'
CONTENT_GAME_DATA_FILES='
data'
CONTENT_DOC_DATA_FILES='
manual.pdf
scenario_editor_manual.pdf
technology_tree.pdf'

## The Art of Conquest expansion

CONTENT_GAME_BIN_CONQUEST_RELATIVE_PATH='empire earth - the art of conquest'
CONTENT_GAME_BIN_CONQUEST_FILES='
redist
*.cfg
*.dll
*.exe
*.inf'
CONTENT_GAME_DATA_CONQUEST_RELATIVE_PATH='empire earth - the art of conquest'
CONTENT_GAME_DATA_CONQUEST_FILES='
data'
CONTENT_DOC_DATA_CONQUEST_FILES='
technology_tree_aoc.pdf
manual_aoc.pdf'

# Applications

## Ensure that background music is played during gameplay (WINE 9.0)
## Without these verbs, music is only played in the game menus.
## TODO: Check if all of these verbs are required
WINE_WINETRICKS_VERBS='dmusic dmloader dmsynth dmime dmband dmcompos dmstyle'

## Empire Earth - base game

APP_MAIN_EXE='empire earth.exe'
APP_MAIN_ICON='empire earth/empire earth.exe'

USER_PERSISTENT_DIRECTORIES="
${GAME_ID}/data/Scenarios
${GAME_ID}/data/Saved Games
${GAME_ID}/Users"

## The Art of Conquest expansion

APP_CONQUEST_ID="${GAME_ID}-${GAME_ID_CONQUEST}"
APP_CONQUEST_NAME="$GAME_NAME - $GAME_NAME_CONQUEST"
APP_CONQUEST_EXE='ee-aoc.exe'
APP_CONQUEST_ICON='empire earth - the art of conquest/ee-aoc.exe'

USER_PERSISTENT_DIRECTORIES="$USER_PERSISTENT_DIRECTORIES
${GAME_ID_CONQUEST}/data/Scenarios
${GAME_ID_CONQUEST}/data/Saved Games
${GAME_ID_CONQUEST}/Users"

# Packages

PACKAGES_LIST='
PKG_BIN
PKG_DATA
PKG_BIN_CONQUEST
PKG_DATA_CONQUEST'

## Empire Earth - base game

PKG_BIN_ID="$GAME_ID"
PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

## The Art of Conquest expansion

PKG_BIN_CONQUEST_ID="${GAME_ID}-${GAME_ID_CONQUEST}"
PKG_BIN_CONQUEST_ARCH='32'
PKG_BIN_CONQUEST_DESCRIPTION="$GAME_NAME_CONQUEST"
PKG_BIN_CONQUEST_DEPENDENCIES_SIBLINGS='
PKG_DATA_CONQUEST'

PKG_DATA_CONQUEST_ID="${GAME_ID}-${GAME_ID_CONQUEST}-data"
PKG_DATA_CONQUEST_DESCRIPTION="$GAME_NAME_CONQUEST - data"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA' 'APP_MAIN'
content_inclusion_icons 'PKG_DATA_CONQUEST' 'APP_CONQUEST'
content_inclusion 'GAME_BIN'  'PKG_BIN'  "$(path_game_data)/$(game_id)"
content_inclusion 'GAME_DATA' 'PKG_DATA' "$(path_game_data)/$(game_id)"
content_inclusion 'DOC_DATA'  'PKG_DATA' "$(path_documentation)/$(game_id)"
content_inclusion 'GAME_BIN_CONQUEST'  'PKG_BIN_CONQUEST'  "$(path_game_data)/$(game_id)-${GAME_ID_CONQUEST}"
content_inclusion 'GAME_DATA_CONQUEST' 'PKG_DATA_CONQUEST' "$(path_game_data)/$(game_id)-${GAME_ID_CONQUEST}"
content_inclusion 'DOC_DATA_CONQUEST'  'PKG_DATA_CONQUEST' "$(path_documentation)/$(game_id)-${GAME_ID_CONQUEST}"
rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

APP_MAIN_EXE="$(game_id)/$(application_exe 'APP_MAIN')"
## Run the game binary from its parent directory
game_exec_line() {
	cat <<- EOF
	cd "$(dirname "$APP_MAIN_EXE")"
	\$(wine_command) "$(basename "$APP_MAIN_EXE")" "\$@"
	EOF
}
launchers_generation 'PKG_BIN' 'APP_MAIN'

APP_CONQUEST_EXE="$(game_id)-${GAME_ID_CONQUEST}/$(application_exe 'APP_CONQUEST')"
## Run the game binary from its parent directory
game_exec_line() {
	cat <<- EOF
	cd "$(dirname "$APP_CONQUEST_EXE")"
	\$(wine_command) "$(basename "$APP_CONQUEST_EXE")" "\$@"
	EOF
}
launchers_generation 'PKG_BIN_CONQUEST' 'APP_CONQUEST'

# Build package

packages_generation
print_instructions 'PKG_BIN' 'PKG_DATA'
(
	GAME_NAME="$GAME_NAME - $GAME_NAME_CONQUEST"
	print_instructions 'PKG_BIN_CONQUEST' 'PKG_DATA_CONQUEST'
)

# Clean up

working_directory_cleanup

exit 0
