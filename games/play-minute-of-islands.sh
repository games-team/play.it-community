#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Mopi
set -o errexit

###
# Minute of Islands
# send your bug reports to contact@dotslashplay.it
###

script_version=20230716.2

GAME_ID='minute-of-islands'
GAME_NAME='Minute of Islands'

ARCHIVE_BASE_0='setup_minute_of_islands_1.0_(64bit)_(46572).exe'
ARCHIVE_BASE_0_MD5='637bf2569af75d362de916fdf4ed570a'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1='setup_minute_of_islands_1.0_(64bit)_(46572)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='612f7cd620f9055a8f45b408b06a0a8e'
ARCHIVE_BASE_0_SIZE='2400000'
ARCHIVE_BASE_0_VERSION='1.0-gog46572'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/minute_of_islands'

UNITY3D_NAME='minute of islands'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES="
${UNITY3D_NAME}.exe
${UNITY3D_NAME}_data/plugins
monobleedingedge
*.dll"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_data"

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Studio Fizbin/Minute of Islands'

APP_MAIN_TYPE='wine'
APP_MAIN_EXE="${UNITY3D_NAME}.exe"

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

target_version='2.24'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Delete unwanted files
	rm --recursive \
		'__redist' \
		'app' \
		'commonappdata' \
		'tmp'
)

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
