#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The World Begins With You
# send your bug reports to contact@dotslashplay.it
###

script_version=20240516.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='the-world-begins-with-you'
GAME_NAME='The World Begins With You'

ARCHIVE_BASE_0_NAME='TheWorldBeginsWithYou_v1_0_1_WIN.zip'
ARCHIVE_BASE_0_MD5='ae72427190719de31f59b2cc1ed4baa3'
ARCHIVE_BASE_0_SIZE='730000'
ARCHIVE_BASE_0_VERSION='1.0.1-itch'
ARCHIVE_BASE_0_URL='https://fabiandenter.itch.io/the-world-begins-with-you'

UNITY3D_NAME='TheWorldBeginsWithYou_v1_0_1'

CONTENT_PATH_DEFAULT='.'

WINE_VIRTUAL_DESKTOP='auto'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
