#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Phil Morrell
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2020 Mopi
set -o errexit

###
# Anno 1404
# send your bug reports to contact@dotslashplay.it
###

script_version=20241124.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='anno-1404'
GAME_NAME='Anno 1404'

GAME_ID_VENICE="${GAME_ID}-venice"
GAME_NAME_VENICE="$GAME_NAME - Venice"

# Archives

## Single language archives

ARCHIVE_BASE_EN_0_NAME='setup_anno_1404_2.01_v2_(30326).exe'
ARCHIVE_BASE_EN_0_MD5='5b92b95ddd3a60bff25afaca6531dab3'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_PART1_NAME='setup_anno_1404_2.01_v2_(30326)-1.bin'
ARCHIVE_BASE_EN_0_PART1_MD5='3bf8dd4469d43392617df7737cebad04'
ARCHIVE_BASE_EN_0_SIZE='4100000'
ARCHIVE_BASE_EN_0_VERSION='2.01.5010-gog30326'
ARCHIVE_BASE_EN_0_URL='https://www.gog.com/game/anno_1404_gold_edition'

ARCHIVE_BASE_FR_0_NAME='setup_anno_1404_2.01_v2_(french)_(30326).exe'
ARCHIVE_BASE_FR_0_MD5='24be40c2f1ce714dcc9c505ed62fcdb2'
ARCHIVE_BASE_FR_0_TYPE='innosetup'
ARCHIVE_BASE_FR_0_PART1_NAME='setup_anno_1404_2.01_v2_(french)_(30326)-1.bin'
ARCHIVE_BASE_FR_0_PART1_MD5='d2bbff77601562218b62b53901edd5e7'
ARCHIVE_BASE_FR_0_SIZE='4000000'
ARCHIVE_BASE_FR_0_VERSION='2.01.5010-gog30326'
ARCHIVE_BASE_FR_0_URL='https://www.gog.com/game/anno_1404_gold_edition'

## Multi-languages archives

ARCHIVE_BASE_MULTILANG_0_NAME='setup_anno_1404_gold_edition_2.01.5010_(13111).exe'
ARCHIVE_BASE_MULTILANG_0_MD5='b19333f57c1c15b788e29ff6751dac20'
ARCHIVE_BASE_MULTILANG_0_TYPE='innosetup'
ARCHIVE_BASE_MULTILANG_0_PART1_NAME='setup_anno_1404_gold_edition_2.01.5010_(13111)-1.bin'
ARCHIVE_BASE_MULTILANG_0_PART1_MD5='17933b44bdb2a26d8d82ffbfdc494210'
ARCHIVE_BASE_MULTILANG_0_PART2_NAME='setup_anno_1404_gold_edition_2.01.5010_(13111)-2.bin'
ARCHIVE_BASE_MULTILANG_0_PART2_MD5='2f71f5378b5f27a84a41cc481a482bd6'
ARCHIVE_BASE_MULTILANG_0_SIZE='6200000'
ARCHIVE_BASE_MULTILANG_0_VERSION='2.01.5010-gog13111'


# Archive contents

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_MULTILANG='app'

## Anno 1404 (base game)

CONTENT_GAME_BIN_FILES='
bin
tools
anno4.exe'
CONTENT_GAME_L10N_EN_FILES='
maindata/eng0.rda'
CONTENT_GAME_L10N_FR_FILES='
maindata/fra0.rda'
CONTENT_GAME_DATA_FILES='
data
maindata
resources'
CONTENT_GAME0_DATA_PATH="${CONTENT_PATH_DEFAULT}/__support/add"
CONTENT_GAME0_DATA_PATH_MULTILANG="${CONTENT_PATH_DEFAULT_MULTILANG}/__support/add"
CONTENT_GAME0_DATA_FILES='
engine.ini'
CONTENT_DOC_DATA_FILES='
manual.pdf'

## Venice expansion

CONTENT_GAME_BIN_VENICE_FILES='
addon.exe'
CONTENT_GAME_L10N_EN_VENICE_FILES='
addon/eng0.rda'
CONTENT_GAME_L10N_FR_VENICE_FILES='
addon/fra0.rda'
CONTENT_GAME_DATA_VENICE_FILES='
addon'
CONTENT_DOC_DATA_FILES='
manual addon.pdf'


WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Roaming/Ubisoft/Anno1404
users/${USER}/Documents/Anno 1404'

APP_MAIN_EXE='anno4.exe'

APP_VENICE_ID="$GAME_ID_VENICE"
APP_VENICE_NAME="$GAME_NAME_VENICE"
APP_VENICE_EXE='addon.exe'

# Packages

## Anno 1404 (base game)

PACKAGES_LIST_EN='
PKG_BIN
PKG_L10N_EN
PKG_DATA'
PACKAGES_LIST_FR='
PKG_BIN
PKG_L10N_FR
PKG_DATA'
PACKAGES_LIST_MULTILANG='
PKG_BIN
PKG_L10N_EN
PKG_L10N_FR
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_L10N_ID="${GAME_ID}-l10n"
PKG_L10N_EN_ID="${PKG_L10N_ID}-en"
PKG_L10N_FR_ID="${PKG_L10N_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_ID"
PKG_L10N_EN_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_FR_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_DESCRIPTION='localization'
PKG_L10N_EN_DESCRIPTION="${PKG_L10N_DESCRIPTION} - English"
PKG_L10N_FR_DESCRIPTION="${PKG_L10N_DESCRIPTION} - French"

PKG_BIN_ID="$GAME_ID"
PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID $PKG_L10N_ID"

## Venice expansion

PACKAGES_LIST_EN="$PACKAGES_LIST_EN
PKG_BIN_VENICE
PKG_L10N_EN_VENICE
PKG_DATA_VENICE"
PACKAGES_LIST_FR="$PACKAGES_LIST_FR
PKG_BIN_VENICE
PKG_L10N_FR_VENICE
PKG_DATA_VENICE"
PACKAGES_LIST_MULTILANG="$PACKAGES_LIST_MULTILANG
PKG_BIN_VENICE
PKG_L10N_EN_VENICE
PKG_L10N_FR_VENICE
PKG_DATA_VENICE"

PKG_DATA_VENICE_ID="${GAME_ID_VENICE}-data"
PKG_DATA_VENICE_DESCRIPTION="$GAME_NAME_VENICE - data"

PKG_L10N_VENICE_ID="${GAME_ID_VENICE}-l10n"
PKG_L10N_EN_VENICE_ID="${PKG_L10N_VENICE_ID}-en"
PKG_L10N_FR_VENICE_ID="${PKG_L10N_VENICE_ID}-fr"
PKG_L10N_VENICE_PROVIDES="
$PKG_L10N_VENICE_ID"
PKG_L10N_EN_VENICE_PROVIDES="$PKG_L10N_VENICE_PROVIDES"
PKG_L10N_FR_VENICE_PROVIDES="$PKG_L10N_VENICE_PROVIDES"
PKG_L10N_VENICE_DESCRIPTION="$GAME_NAME_VENICE - localization"
PKG_L10N_EN_VENICE_DESCRIPTION="$PKG_L10N_VENICE_DESCRIPTION - English"
PKG_L10N_FR_VENICE_DESCRIPTION="$PKG_L10N_VENICE_DESCRIPTION - French"

PKG_BIN_VENICE_ID="$GAME_ID_VENICE"
PKG_BIN_VENICE_ARCH='32'
PKG_BIN_VENICE_DESCRIPTION="$GAME_NAME_VENICE"
PKG_BIN_VENICE_DEPS="$PKG_BIN_ID $PKG_DATA_VENICE_ID $PKG_L10N_VENICE_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Work around immediate crash on launch.
	config_file='engine.ini'
	if [ -e "$config_file" ]; then
		sed_pattern='2i<DirectXVersion>9</DirectXVersion>'
		sed --in-place "$sed_pattern" "$config_file"
	else
		cat > "$config_file" <<- 'EOF'
		<InitFile>
		<DirectXVersion>9</DirectXVersion>
		</InitFile>
		EOF
	fi
)

# Include game data

set_current_package 'PKG_DATA'
icons_inclusion 'APP_MAIN'
set_current_package 'PKG_DATA_VENICE'
icons_inclusion 'APP_VENICE'
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
launchers_write 'APP_MAIN'
set_current_package 'PKG_BIN_VENICE'
launchers_write 'APP_VENICE'

# Build packages

packages_generation
case "$(current_archive)" in
	('ARCHIVE_BASE_MULTILANG_'*)
		case "${LANG%_*}" in
			('fr')
				lang_string='version %s :'
				lang_en='anglaise'
				lang_fr='française'
			;;
			('en'|*)
				lang_string='%s version:'
				lang_en='English'
				lang_fr='French'
			;;
		esac
		printf '\n'
		printf "$lang_string" "$lang_en"
		print_instructions 'PKG_BIN' 'PKG_L10N_EN' 'PKG_DATA'
		printf "$lang_string" "$lang_fr"
		print_instructions 'PKG_BIN' 'PKG_L10N_FR' 'PKG_DATA'
	;;
	('ARCHIVE_BASE_FR_'*)
		print_instructions 'PKG_BIN' 'PKG_L10N_FR' 'PKG_DATA'
	;;
	('ARCHIVE_BASE_EN_'*|*)
		print_instructions 'PKG_BIN' 'PKG_L10N_EN' 'PKG_DATA'
	;;
esac
GAME_NAME="$GAME_NAME_VENICE"
case "$(current_archive)" in
	('ARCHIVE_BASE_MULTILANG_'*)
		case "${LANG%_*}" in
			('fr')
				lang_string='version %s :'
				lang_en='anglaise'
				lang_fr='française'
			;;
			('en'|*)
				lang_string='%s version:'
				lang_en='English'
				lang_fr='French'
			;;
		esac
		printf '\n'
		printf "$lang_string" "$lang_en"
		print_instructions 'PKG_BIN_VENICE' 'PKG_L10N_EN_VENICE' 'PKG_DATA_VENICE'
		printf "$lang_string" "$lang_fr"
		print_instructions 'PKG_BIN_VENICE' 'PKG_L10N_FR_VENICE' 'PKG_DATA_VENICE'
	;;
	('ARCHIVE_BASE_FR_'*)
		print_instructions 'PKG_BIN_VENICE' 'PKG_L10N_FR_VENICE' 'PKG_DATA_VENICE'
	;;
	('ARCHIVE_BASE_EN_'*|*)
		print_instructions 'PKG_BIN_VENICE' 'PKG_L10N_EN_VENICE' 'PKG_DATA_VENICE'
	;;
esac

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
