#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Fabien Givors <captnfab@debian-facile.org>
set -o errexit

###
# Medieval Dynasty
# send your bug reports to contact@dotslashplay.it
###

script_version=20240831.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='medieval-dynasty'
GAME_NAME='Medieval Dynasty'

## Archives

### Full game

ARCHIVE_BASE_0_NAME='setup_medieval_dynasty_2.1.1.3_(64bit)_(75182).exe'
ARCHIVE_BASE_0_MD5='e1db5aac7e6bcde369e6d36f3cd76fcc'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1='setup_medieval_dynasty_2.1.1.3_(64bit)_(75182)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='591f1146a80fd822511799fc6aeaf197'
ARCHIVE_BASE_0_PART2='setup_medieval_dynasty_2.1.1.3_(64bit)_(75182)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='c0728d685a4c914fff0716fef9e1f2c3'
ARCHIVE_BASE_0_SIZE='10550253'
ARCHIVE_BASE_0_VERSION='2.1.1.3-gog75182'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/medieval_dynasty'

UNREALENGINE4_NAME='medieval_dynasty'

CONTENT_PATH_DEFAULT='.'

CONTENT_GAME_MOVIES_FILES="
${UNREALENGINE4_NAME}/content/movies"

CONTENT_GAME_EDITOR_FILES="
${UNREALENGINE4_NAME}/content/paks/medieval_dynasty-windowsnoeditor.pak
${UNREALENGINE4_NAME}/content/paks/medieval_dynasty-windowsnoeditor.sig"


APP_MAIN_EXE="${UNREALENGINE4_NAME}/binaries/win64/${UNREALENGINE4_NAME}-win64-shipping.exe"
APP_MAIN_ICON="app/goggame-1224667888.ico"

PACKAGES_LIST='
PKG_BIN
PKG_MOVIES
PKG_EDITOR
PKG_DATA'

PKG_MOVIES_ID="${GAME_ID}-movies"
PKG_MOVIES_DESCRIPTION='movies'

PKG_EDITOR_ID="${GAME_ID}-editor"
PKG_EDITOR_DESCRIPTION='editor'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPENDENCIES_SIBLINGS='
PKG_MOVIES
PKG_EDITOR'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
