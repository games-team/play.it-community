#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Cats are Liquid series:
# - Cats are Liquid 1
# - Cats are Liquid 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20240619.3

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID_EPISODE1='cats-are-liquid-1'
GAME_NAME_EPISODE1='Cats are Liquid - A Light in the Shadows'

GAME_ID_EPISODE2='cats-are-liquid-2'
GAME_NAME_EPISODE2='Cats are Liquid - A Better Place'

ARCHIVE_BASE_EPISODE1_0_NAME='CatsAreLiquidLinux.zip'
ARCHIVE_BASE_EPISODE1_0_MD5='eb01e4f7c44543051e95bc78b777bb5b'
ARCHIVE_BASE_EPISODE1_0_SIZE='171981'
ARCHIVE_BASE_EPISODE1_0_VERSION='1.0-itch1'
ARCHIVE_BASE_EPISODE1_0_URL='https://lastquarterstudios.itch.io/cats-are-liquid-a-light-in-the-shadows'

ARCHIVE_BASE_EPISODE2_0_NAME='CaL-ABP-Linux.zip'
ARCHIVE_BASE_EPISODE2_0_MD5='a61524e809870b026dc8a993b177b9b9'
ARCHIVE_BASE_EPISODE2_0_SIZE='437281'
ARCHIVE_BASE_EPISODE2_0_VERSION='1.0-itch1'
ARCHIVE_BASE_EPISODE2_0_URL='https://lastquarterstudios.itch.io/cats-are-liquid-a-better-place'

UNITY3D_NAME_EPISODE1='CatsAreLiquidLinux'
UNITY3D_NAME_EPISODE2='CaL-ABP-Linux'
UNITY3D_PLUGINS='
ScreenSelector.so'
## TODO: Check if the Steam libraries are required.
UNITY3D_PLUGINS_EPISODE1="$UNITY3D_PLUGINS
libCSteamworks.so
libsteam_api.so"
UNITY3D_PLUGINS_EPISODE2="$UNITY3D_PLUGINS
libsteam_api.so"

CONTENT_PATH_DEFAULT_EPISODE1='CatsAreLiquidLinux'
CONTENT_PATH_DEFAULT_EPISODE2='Linux'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID_EPISODE1="${GAME_ID_EPISODE1}-data"
PKG_DATA_ID_EPISODE2="${GAME_ID_EPISODE2}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH_EPISODE1='32'
PKG_BIN_ARCH_EPISODE2='64'
PKG_BIN_DEPS_EPISODE1="$PKG_DATA_ID_EPISODE1"
PKG_BIN_DEPS_EPISODE2="$PKG_DATA_ID_EPISODE2"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
