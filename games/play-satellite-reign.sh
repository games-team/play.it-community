#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Satellite Reign
# send your bug reports to contact@dotslashplay.it
###

script_version=20231104.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='satellite-reign'
GAME_NAME='Satellite Reign'

ARCHIVE_BASE_1_NAME='gog_satellite_reign_2.8.0.10.sh'
ARCHIVE_BASE_1_MD5='5a22cdb3e721bbed7bc5836e769caa0a'
ARCHIVE_BASE_1_SIZE='1400000'
ARCHIVE_BASE_1_VERSION='1.13.06-gog2.8.0.10'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/satellite_reign'

ARCHIVE_BASE_0_NAME='gog_satellite_reign_2.6.0.8.sh'
ARCHIVE_BASE_0_MD5='0e050bac1c75632b094097132fa6be72'
ARCHIVE_BASE_0_SIZE='1400000'
ARCHIVE_BASE_0_VERSION='1.07.08-gog2.6.0.8'

UNITY3D_NAME='SatelliteReignLinux'
## TODO: Check if the Steam libraries can be dropped.
UNITY3D_PLUGINS='
libCSteamworks.so
libsteam_api.so
RakNet.so
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_DOC_DATA_FILES='
Linux_GPU_DriverInfo.txt'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXrandr.so.2'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Delete unwanted empty Mac OS directories.
	rm --recursive \
		"$(unity3d_name)_Data/Plugins/CSteamworks.bundle" \
		"$(unity3d_name)_Data/Plugins/ShroudUnityPlugin.bundle"
)

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN64'
# shellcheck disable=SC2119
launchers_write
set_current_package 'PKG_BIN32'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
