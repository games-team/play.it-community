#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2021 Anna Lea
set -o errexit

###
# Afterlife
# send your bug reports to contact@dotslashplay.it
###

script_version=20241023.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='afterlife'
GAME_NAME='Afterlife'

ARCHIVE_BASE_EN_0_NAME='gog_afterlife_2.2.0.8.sh'
ARCHIVE_BASE_EN_0_MD5='3aca0fac1b93adec5aff39d395d995ab'
ARCHIVE_BASE_EN_0_SIZE='260000'
ARCHIVE_BASE_EN_0_VERSION='1.1-gog2.2.0.8'
ARCHIVE_BASE_EN_0_URL='https://www.gog.com/game/afterlife'

ARCHIVE_BASE_FR_0_NAME='gog_afterlife_french_2.2.0.8.sh'
ARCHIVE_BASE_FR_0_MD5='56b3efee60bc490c68f8040587fc1878'
ARCHIVE_BASE_FR_0_SIZE='250000'
ARCHIVE_BASE_FR_0_VERSION='1.1-gog2.2.0.8'
ARCHIVE_BASE_FR_0_URL='https://www.gog.com/game/afterlife'

ARCHIVE_BASE_DE_0_NAME='gog_afterlife_german_2.2.0.8.sh'
ARCHIVE_BASE_DE_0_MD5='441b57901235584ff851da8fe316f0b3'
ARCHIVE_BASE_DE_0_SIZE='310000'
ARCHIVE_BASE_DE_0_VERSION='1.1-gog2.2.0.8'
ARCHIVE_BASE_DE_0_URL='https://www.gog.com/game/afterlife'

ARCHIVE_BASE_ES_0_NAME='gog_afterlife_spanish_2.2.0.8.sh'
ARCHIVE_BASE_ES_0_MD5='43deb4aeeaa3b9ede542f1a0718b15ee'
ARCHIVE_BASE_ES_0_SIZE='250000'
ARCHIVE_BASE_ES_0_VERSION='1.1-gog2.2.0.8'
ARCHIVE_BASE_ES_0_URL='https://www.gog.com/game/afterlife'

ARCHIVE_BASE_IT_0_NAME='gog_afterlife_italian_2.2.0.8.sh'
ARCHIVE_BASE_IT_0_MD5='cb60193a32d29babfc203412df00b21e'
ARCHIVE_BASE_IT_0_SIZE='250000'
ARCHIVE_BASE_IT_0_VERSION='1.1-gog2.2.0.8'
ARCHIVE_BASE_IT_0_URL='https://www.gog.com/game/afterlife'

CONTENT_PATH_DEFAULT='data/noarch/data'
CONTENT_GAME_MAIN_FILES='
alife.*
alife
*.asc
*.exe
*.ini'
CONTENT_DOC_MAIN_FILES='
readme.txt'
CONTENT_DOC0_MAIN_PATH="${CONTENT_PATH_DEFAULT}/../docs"
CONTENT_DOC0_MAIN_FILES='
afterlife - reference guide.pdf'

USER_PERSISTENT_FILES='
*.ini'
USER_PERSISTENT_DIRECTORIES='
save'

APP_MAIN_TYPE='dosbox'
APP_MAIN_EXE='alife/afterdos.bat'
APP_MAIN_ICON='../support/icon.png'

PKG_MAIN_ID="$GAME_ID"
PKG_MAIN_ID_EN="${PKG_MAIN_ID}-en"
PKG_MAIN_ID_FR="${PKG_MAIN_ID}-fr"
PKG_MAIN_ID_DE="${PKG_MAIN_ID}-de"
PKG_MAIN_ID_ES="${PKG_MAIN_ID}-es"
PKG_MAIN_ID_IT="${PKG_MAIN_ID}-it"
PKG_MAIN_PROVIDES="
$PKG_MAIN_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Convert all file paths to lowercase.
	tolower .
)

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

## Run the game binary from its parent directory
game_exec_line() {
	cat <<- 'EOF'
	cd alife
	afterdos.bat $@
	EOF
}

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
