#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# AER: Memories of Old
# send your bug reports to contact@dotslashplay.it
###

script_version=20240623.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='aer-memories-of-old'
GAME_NAME='AER: Memories of Old'

ARCHIVE_BASE_1_NAME='aer_memories_of_old_1_0_4_2_34743.sh'
ARCHIVE_BASE_1_MD5='275fbe912805fde9ed3d6d2a25d61b7b'
ARCHIVE_BASE_1_TYPE='mojosetup'
ARCHIVE_BASE_1_SIZE='1400000'
ARCHIVE_BASE_1_VERSION='1.0.4.2-gog34743'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/aer'

ARCHIVE_BASE_0_NAME='aer_memories_of_old_en_v1_0_4_1_19457.sh'
ARCHIVE_BASE_0_MD5='ca59918433567c974b4985767c4af576'
ARCHIVE_BASE_0_TYPE='mojosetup'
ARCHIVE_BASE_0_SIZE='1400000'
ARCHIVE_BASE_0_VERSION='1.0.4.1-gog19475'

UNITY3D_NAME='AER'
UNITY3D_PLUGINS='
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

## Force the game to run in windowed mode.
case "$(current_archive)" in
	('ARCHIVE_BASE_0')
		unity3d_prefs_path='${HOME}/.config/unity3d/Daedalic Entertainment GmbH/AERMemoriesofOld/prefs'
	;;
	(*)
		unity3d_prefs_path='${HOME}/.config/unity3d/Daedalic Entertainment GmbH/AER_ Memories of Old/prefs'
	;;
esac
APP_MAIN_PRERUN="$(application_prerun 'APP_MAIN')"'
# Force the game to run in windowed mode
config_file="'"$unity3d_prefs_path"'"
if [ ! -f "$config_file" ] ; then
	mkdir --parents "$(dirname "$config_file")"
	cat > "$config_file" <<- EOF
	<unity_prefs version_major="1" version_minor="1">
	<pref name="Screenmanager Is Fullscreen mode" type="int">0</pref>
	<pref name="UnitySelectMonitor" type="int">0</pref>
	</unity_prefs>
	EOF
else
	sed_pattern='\''<pref name=\"Screenmanager Is Fullscreen mode\" type=\"int\">.</pref>'\''
	sed_replacement='\''<pref name=\"Screenmanager Is Fullscreen mode\" type=\"int\">0</pref>'\''
	sed --in-place \
		"s#${sed_pattern}#${sed_replacement}#" \
		"$config_file"
fi
'
launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
