#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Knights of Pen and Paper
# send your bug reports to contact@dotslashplay.it
###

script_version=20231017.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='knights-of-pen-and-paper'
GAME_NAME='Knights of Pen and Paper'

# Archives

## Kinghts of Pen and Paper (base game)

ARCHIVE_BASE_0_NAME='gog_knights_of_pen_and_paper_1_edition_2.0.0.1.sh'
ARCHIVE_BASE_0_MD5='1f387b78bfe426b9396715fbfe3499b9'
ARCHIVE_BASE_0_VERSION='2.34c-gog2.0.0.1'
ARCHIVE_BASE_0_SIZE='120000'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/knights_of_pen_and_paper_1_deluxier_edition'

## Deluxier Edition

## TODO: The expansion content should be installed using a dedicated package.
ARCHIVE_OPTIONAL_DELUXIER_0_NAME='gog_knights_of_pen_and_paper_1_deluxier_edition_upgrade_2.0.0.1.sh'
ARCHIVE_OPTIONAL_DELUXIER_0_MD5='b3033693afd93cc885883aede7ede4b0'


UNITY3D_NAME='knightspp'
## TODO: Check if the Steam libraries can be dropped.
UNITY3D_PLUGINS='
libCSteamworks.so
libsteam_api.so
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libGLU.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Check for the presence of optional extra archives

archive_initialize_optional \
	'ARCHIVE_DELUXIER' \
	'ARCHIVE_OPTIONAL_DELUXIER_0'

# Extract game data

archive_extraction_default
if archive_is_available 'ARCHIVE_DELUXIER' ; then
	archive_extraction 'ARCHIVE_DELUXIER'
fi

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
