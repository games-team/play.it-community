#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Chaos Reborn
# send your bug reports to contact@dotslashplay.it
###

script_version=20230226.3

GAME_ID='chaos-reborn'
GAME_NAME='Chaos Reborn'

ARCHIVE_BASE_4='chaos_reborn_en_1_13_3_23004.sh'
ARCHIVE_BASE_4_MD5='283d60b5adf91640679d2fe1c7962300'
ARCHIVE_BASE_4_TYPE='mojosetup'
ARCHIVE_BASE_4_SIZE='1700000'
ARCHIVE_BASE_4_VERSION='1.13.3-gog23004'
ARCHIVE_BASE_4_URL='https://www.gog.com/game/chaos_reborn'

ARCHIVE_BASE_3='chaos_reborn_en_1_13_2_17223.sh'
ARCHIVE_BASE_3_MD5='edb60d98710c87c0adea06f55be99567'
ARCHIVE_BASE_3_TYPE='mojosetup'
ARCHIVE_BASE_3_SIZE='1700000'
ARCHIVE_BASE_3_VERSION='1.13.2-gog17223'

ARCHIVE_BASE_2='chaos_reborn_en_1_131_14290.sh'
ARCHIVE_BASE_2_MD5='fcfea11ad6a6cbdda2290c4f29bbeb2b'
ARCHIVE_BASE_2_TYPE='mojosetup'
ARCHIVE_BASE_2_SIZE='1700000'
ARCHIVE_BASE_2_VERSION='1.13.1-gog14290'

ARCHIVE_BASE_1='gog_chaos_reborn_2.14.0.16.sh'
ARCHIVE_BASE_1_MD5='97dbfc0a679a7fd104c744b6aa46db36'
ARCHIVE_BASE_1_TYPE='mojosetup'
ARCHIVE_BASE_1_SIZE='1700000'
ARCHIVE_BASE_1_VERSION='1.13-gog2.14.0.16'

ARCHIVE_BASE_0='gog_chaos_reborn_2.13.0.15.sh'
ARCHIVE_BASE_0_MD5='a2abf12572eea8b43059a9bb8d5d3171'
ARCHIVE_BASE_0_TYPE='mojosetup'
ARCHIVE_BASE_0_SIZE='1700000'
ARCHIVE_BASE_0_VERSION='1.12.2-gog2.13.0.15'

UNITY3D_NAME='ChaosRebornLinux'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN32_PATH="${CONTENT_PATH_DEFAULT}/${UNITY3D_NAME}_Data/Plugins/x86"
CONTENT_LIBS_BIN32_FILES='
ScreenSelector.so'
CONTENT_GAME_BIN32_FILES="
${UNITY3D_NAME}.x86
${UNITY3D_NAME}_Data/Mono/x86"
CONTENT_LIBS_BIN64_PATH="${CONTENT_PATH_DEFAULT}/${UNITY3D_NAME}_Data/Plugins/x86_64"
CONTENT_LIBS_BIN64_FILES='
ScreenSelector.so'
CONTENT_GAME_BIN64_FILES="
${UNITY3D_NAME}.x86_64
${UNITY3D_NAME}_Data/Mono/x86_64"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data/Managed
${UNITY3D_NAME}_Data/Mono/etc
${UNITY3D_NAME}_Data/Resources
${UNITY3D_NAME}_Data/globalgamemanagers
${UNITY3D_NAME}_Data/ScreenSelector.png
${UNITY3D_NAME}_Data/level?
${UNITY3D_NAME}_Data/level??
${UNITY3D_NAME}_Data/*.assets
${UNITY3D_NAME}_Data/*.assets.resS
${UNITY3D_NAME}_Data/*.resource"

PACKAGES_LIST='PKG_BIN32 PKG_BIN64 PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN32_ARCH='32'
PKG_BIN64_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Enforce windowed mode on first launch

APP_MAIN_PRERUN="$APP_MAIN_PRERUN"'
# Enforce windowed mode on first launch
config_file="$HOME/.config/unity3d/Snapshot Games Inc_/Chaos Reborn/prefs"
if [ ! -e "$config_file" ]; then
	mkdir --parents "$(dirname "$config_file")"
	cat > "$config_file" <<- EOF
		<unity_prefs version_major="1" version_minor="1">
		<pref name="Screenmanager Is Fullscreen mode" type="int">0</pref>
		</unity_prefs>
	EOF
fi'

# Load common functions

target_version='2.21'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

for PKG in 'PKG_BIN32' 'PKG_BIN64'; do
	# shellcheck disable=SC2119
	launchers_write
done

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
