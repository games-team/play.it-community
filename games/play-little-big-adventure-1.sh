#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Dominique Derrier
# SPDX-FileCopyrightText: © 2020 macaron
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Little Big Adventure 1
# send your bug reports to contact@dotslashplay.it
###

script_version=20230420.1

GAME_ID='little-big-adventure-1'
GAME_NAME='Little Big Adventure'

ARCHIVE_BASE_1='setup_little_big_adventure_1.0_(28186).exe'
ARCHIVE_BASE_1_MD5='43d4926dc8a56a95800e746ac9797201'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='510000'
ARCHIVE_BASE_1_VERSION='1.0-gog28186'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/little_big_adventure'

ARCHIVE_BASE_0='setup_lba_2.1.0.22.exe'
ARCHIVE_BASE_0_MD5='c40177522adcbe50ea52590be57045f8'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='510000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.1.0.22'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_MAIN_FILES='
VOX
SETUP.LST
LBA.DAT
LBA.GOG
SETSOUND.BAT
SAMPLE.*
*.CFG
*.DLL
*.EXE
*.HQR
*.INI'
CONTENT_DOC_MAIN_FILES='
*.PDF
*.TXT'

GAME_IMAGE='LBA.DAT'

USER_PERSISTENT_FILES='
*.CFG
*.INI
*.LBA'

APP_MAIN_EXE='RELENT.EXE'
APP_MAIN_ICON='APP/GOGGAME-1207658971.ICO'
APP_MAIN_ICON_0='GOGGAME-1207658971.ICO'

APP_SETUP_ID="${GAME_ID}-setup"
APP_SETUP_NAME="$GAME_NAME - Setup"
APP_SETUP_EXE='SETUP.EXE'
APP_SETUP_CAT='Settings'
APP_SETUP_ICON='APP/GOGGAME-1207658971.ICO'
APP_SETUP_ICON_0='GOGGAME-1207658971.ICO'

# Ensure easy upgrade from packages generated with pre-20200210.3 scripts

PKG_MAIN_PROVIDES="$PKG_MAIN_PROVIDES
little-big-adventure-1-data"

# Load common functions

target_version='2.23'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Convert all file names to upper case
	toupper .
)

# Include game icons

# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
