#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Ravenous Devils
# send your bug reports to contact@dotslashplay.it
###

script_version=20240522.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='ravenous-devils'
GAME_NAME='Ravenous Devils'

ARCHIVE_BASE_1_NAME='setup_ravenous_devils_1.0.2_(64bit)_(59454).exe'
ARCHIVE_BASE_1_MD5='efe01e3e15054928ea1b91777d0481bf'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='1800000'
ARCHIVE_BASE_1_VERSION='1.0.2-gog59454'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/ravenous_devils'

ARCHIVE_BASE_0_NAME='setup_ravenous_devils_1.0.1_(64bit)_(55400).exe'
ARCHIVE_BASE_0_MD5='e18ae8a3932ebcfeaa742cd9f849af4c'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='1800000'
ARCHIVE_BASE_0_VERSION='1.0.1-gog55400'

UNREALENGINE4_NAME='ravenous_devils'

CONTENT_PATH_DEFAULT='.'

APP_MAIN_EXE='ravenous_devils.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
