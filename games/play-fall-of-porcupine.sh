#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Fall of Porcupine
# send your bug reports to contact@dotslashplay.it
###

script_version=20240119.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='fall-of-porcupine'
GAME_NAME='Fall of Porcupine'

ARCHIVE_BASE_0_NAME='setup_fall_of_porcupine_1.1.12_(65935).exe'
ARCHIVE_BASE_0_MD5='6b3cb17a1f869630d16ca633dc176382'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_fall_of_porcupine_1.1.12_(65935)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='8a32270570176b36192ac3ff8bcf7715'
ARCHIVE_BASE_0_SIZE='4168652'
ARCHIVE_BASE_0_VERSION='1.1.12-gog65935'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/fall_of_porcupine'

UNITY3D_NAME='fallofporcupine'

CONTENT_PATH_DEFAULT='.'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Buntspecht Film und Digitales GmbH/FallOfPorcupine'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
video/quicktime, variant=(string)iso'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

## TODO: Investigate a hashsum mismatch during data extraction:
##       Warning: Output checksum mismatch for fallofporcupine_data/sharedassets11.assets.ress:
##       ├─ actual:   MD5 6f4cb6264ae05e5a039650fb7fd2d078
##       └─ expected: MD5 2212bc181be9503f339e23573e1c0256
archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
