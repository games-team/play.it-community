#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 BetaRays
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Wing Commander 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20230103.1

GAME_ID='wing-commander-2'
GAME_NAME='Wing Commander Ⅱ'

ARCHIVE_BASE_1='setup_wing_commander_ii_1.0_(28045).exe'
ARCHIVE_BASE_1_MD5='f5288f26451f7c1c860a269a8198f554'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='46000'
ARCHIVE_BASE_1_VERSION='1.0-gog28045'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/wing_commander_1_2'

ARCHIVE_BASE_0='setup_wing_commander2_2.1.0.18.exe'
ARCHIVE_BASE_0_MD5='f94a7eb75e4ed454108d13189d003e9f'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='49000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.1.0.18'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_MAIN_FILES='
wc2.exe
so1.exe
so2.exe
gamedat
*.cfg'
CONTENT_DOC_MAIN_FILES='
*.pdf'

APP_MAIN_EXE='wc2.exe'
APP_MAIN_ICON='app/goggame-1207662653.ico'
APP_MAIN_ICON_0='goggame-1207662653.ico'

APP_SO1_ID="${GAME_ID}-special-operations-1"
APP_SO1_NAME="$GAME_NAME - Special Operations 1"
APP_SO1_EXE='so1.exe'
APP_SO1_ICON='app/goggame-1207662653.ico'
APP_SO1_ICON_0='goggame-1207662653.ico'

APP_SO2_ID="${GAME_ID}-special-operations-2"
APP_SO2_NAME="$GAME_NAME - Special Operations 2"
APP_SO2_EXE='so2.exe'
APP_SO2_ICON='app/goggame-1207662653.ico'
APP_SO2_ICON_0='goggame-1207662653.ico'

USER_PERSISTENT_FILES='
*.cfg'
USER_PERSISTENT_DIRECTORIES='
gamedat'

PKG_MAIN_DEPS='dosbox'

# Work around performance issues

APP_MAIN_DOSBOX_PRERUN="$APP_MAIN_DOSBOX_PRERUN"'
config -set cpu cycles=fixed 8000
loadfix -32'
APP_SO1_DOSBOX_PRERUN="$APP_MAIN_DOSBOX_PRERUN"
APP_SO2_DOSBOX_PRERUN="$APP_MAIN_DOSBOX_PRERUN"

# Work around sound issues that can lock the game during the first intro speech

APP_MAIN_PRERUN="$APP_MAIN_PRERUN"'
# Work around sound issues that can lock the game during the first intro speech
export DOSBOX_SBLASTER_IRQ=5'
APP_SO1_PRERUN="$APP_MAIN_PRERUN"
APP_SO2_PRERUN="$APP_MAIN_PRERUN"

# Load common functions

target_version='2.20'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

launchers_write 'APP_MAIN' 'APP_SO1' 'APP_SO2'

# Build package

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
