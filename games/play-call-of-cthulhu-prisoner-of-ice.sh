#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 macaron
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Call of Cthulhu: Prisoner of Ice
# send your bug reports to contact@dotslashplay.it
###

script_version=20240505.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='call-of-cthulhu-prisoner-of-ice'
GAME_NAME='Call of Cthulhu: Prisoner of Ice'

ARCHIVE_BASE_EN_0_NAME='call_of_cthulhu_prisoner_of_ice_en_gog_5_17654.sh'
ARCHIVE_BASE_EN_0_MD5='c3f64c02981cfacefd3b3f8d0d504ac3'
ARCHIVE_BASE_EN_0_SIZE='310000'
ARCHIVE_BASE_EN_0_VERSION='1.0-gog17654'
ARCHIVE_BASE_EN_0_URL='https://www.gog.com/game/call_of_cthulhu_prisoner_of_ice'

ARCHIVE_BASE_FR_0_NAME='call_of_cthulhu_prisoner_of_ice_fr_gog_5_17654.sh'
ARCHIVE_BASE_FR_0_MD5='da1f4dad3ee3817a026390fa28320284'
ARCHIVE_BASE_FR_0_SIZE='350000'
ARCHIVE_BASE_FR_0_VERSION='1.0-gog17654'
ARCHIVE_BASE_FR_0_URL='https://www.gog.com/game/call_of_cthulhu_prisoner_of_ice'

CONTENT_PATH_DEFAULT='data/noarch/data'
CONTENT_GAME_MAIN_FILES='
ice
cd'
CONTENT_DOC_MAIN_FILES='
*.txt'
CONTENT_DOC0_MAIN_PATH='data/noarch/docs'
CONTENT_DOC0_MAIN_FILES='
*.pdf
*.txt'
CONTENT_DOC1_MAIN_PATH_EN='data/noarch/docs/english'
CONTENT_DOC1_MAIN_PATH_FR='data/noarch/docs/french'
CONTENT_DOC1_MAIN_FILES='
*.pdf
*.txt'

GAME_IMAGE='cd'
GAME_IMAGE_TYPE='cdrom'

APP_MAIN_EXE='ice640.exe'
APP_MAIN_DOSBOX_PRERUN='d:'
APP_MAIN_ICON='../support/icon.png'
## The type can not be omitted, because the binary is actually on the CD-ROM image.
APP_MAIN_TYPE='dosbox'

USER_PERSISTENT_FILES='
ice/*.cfg
ice/*.ice
ice/*.pck'

PKG_MAIN_ID="$GAME_ID"
PKG_MAIN_ID_EN="${PKG_MAIN_ID}-en"
PKG_MAIN_ID_FR="${PKG_MAIN_ID}-fr"
PKG_MAIN_PROVIDES="
$PKG_MAIN_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

## Work around the binary presence check,
## it is actually included in the CD-ROM image.
launcher_target_presence_check() { return 0; }
launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
