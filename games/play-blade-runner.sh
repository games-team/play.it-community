#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 macaron
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Blade Runner
# send your bug reports to contact@dotslashplay.it
###

script_version=20240624.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='blade-runner'
GAME_NAME='Blade Runner'

# Archives

## Blade Runner

ARCHIVE_BASE_EN_0_NAME='blade_runner_1_0_svm_src_34722.sh'
ARCHIVE_BASE_EN_0_MD5='d9dd6f98eb8dc9401d5499a1d48e5d76'
## TODO: Check if it is required to force the extraction using unzip.
ARCHIVE_BASE_EN_0_EXTRACTOR='unzip'
ARCHIVE_BASE_EN_0_SIZE='1700000'
ARCHIVE_BASE_EN_0_VERSION='1.0-gog34722'
ARCHIVE_BASE_EN_0_URL='https://www.gog.com/game/blade_runner'

ARCHIVE_BASE_FR_0_NAME='blade_runner_french_1_0_svm_src_34722.sh'
ARCHIVE_BASE_FR_0_MD5='98ee3723ac6114a6b464af0b07f76757'
## TODO: Check if it is required to force the extraction using unzip.
ARCHIVE_BASE_FR_0_EXTRACTOR='unzip'
ARCHIVE_BASE_FR_0_SIZE='1700000'
ARCHIVE_BASE_FR_0_VERSION='1.0-gog34722'
ARCHIVE_BASE_FR_0_URL='https://www.gog.com/game/blade_runner'

## Optional subtitles

ARCHIVE_OPTIONAL_SUBTITLES_NAME='Blade_Runner_Subtitles-v6.zip'
ARCHIVE_OPTIONAL_SUBTITLES_MD5='f9b4e5738d3c6092cac3485c41f47cb2'
ARCHIVE_OPTIONAL_SUBTITLES_URL='https://www.scummvm.org/games/#bladerunner'

# Archive contents

## Blade Runner

CONTENT_PATH_DEFAULT='data/noarch/game/data'
CONTENT_GAME_MAIN_FILES='
*.dat
*.mix
*.tlk'
CONTENT_DOC_MAIN_FILES='
*.pdf'
CONTENT_DOC0_MAIN_PATH='data/noarch/docs'
CONTENT_DOC0_MAIN_FILES='
*.txt'

## Optional subtitles

CONTENT_GAME_SUBTITLES_PATH='.'
CONTENT_GAME_SUBTITLES_FILES='
subtitles.mix'
CONTENT_DOC_SUBTITLES_PATH='.'
CONTENT_DOC_SUBTITLES_FILES='
readme.txt'


APP_MAIN_SCUMMID='bladerunner:bladerunner'
APP_MAIN_ICON='../../support/icon.png'

APP_RESTORED_SCUMMID='bladerunner:bladerunner-final'
APP_RESTORED_ID="${GAME_ID}-restored"
APP_RESTORED_NAME="$GAME_NAME with restored content"
APP_RESTORED_ICON='../../support/icon.png'

PKG_MAIN_ID="$GAME_ID"
PKG_MAIN_ID_EN="${GAME_ID}-en"
PKG_MAIN_ID_FR="${GAME_ID}-fr"
PKG_MAIN_PROVIDES="
$PKG_MAIN_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Check for the presence of optional extra archives

## Optional subtitles
archive_initialize_optional \
	'ARCHIVE_SUBTITLES' \
	'ARCHIVE_OPTIONAL_SUBTITLES'

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'
## Optional subtitles
if archive_is_available 'ARCHIVE_SUBTITLES'; then
	archive_extraction 'ARCHIVE_SUBTITLES'
fi
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Convert all file paths to lowercase.
	tolower .
)

# Include game data

content_inclusion_icons
## Optional subtitles
if archive_is_available 'ARCHIVE_SUBTITLES'; then
	content_inclusion 'GAME_SUBTITLES' 'PKG_MAIN' "$(path_game_data)"
	content_inclusion 'DOC_SUBTITLES' 'PKG_MAIN' "$(path_documentation)/subtitles"
fi
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
