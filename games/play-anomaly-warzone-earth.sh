#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Anomaly: Warzone Earth
# send your bug reports to contact@dotslashplay.it
###

script_version=20240503.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='anomaly-warzone-earth'
GAME_NAME='Anomaly: Warzone Earth'

ARCHIVE_BASE_GOG_0_NAME='gog_anomaly_warzone_earth_2.0.0.3.sh'
ARCHIVE_BASE_GOG_0_MD5='37b96b44b2dafde64ab364a11dcf9ad8'
ARCHIVE_BASE_GOG_0_SIZE='440000'
ARCHIVE_BASE_GOG_0_VERSION='1.0-gog2.0.0.3'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/anomaly_warzone_earth'

ARCHIVE_BASE_HUMBLE_1_NAME='AnomalyWarzoneEarth-Installer_Humble_Linux_1364850491.zip'
ARCHIVE_BASE_HUMBLE_1_MD5='acf5147293b42c7625aea6ad0e56afc4'
ARCHIVE_BASE_HUMBLE_1_SIZE='870000'
ARCHIVE_BASE_HUMBLE_1_VERSION='1.0-humble2'
ARCHIVE_BASE_HUMBLE_1_URL='https://www.humblebundle.com/store/anomaly-warzone-earth'

ARCHIVE_BASE_HUMBLE_0_NAME='AnomalyWarzoneEarth-Installer'
ARCHIVE_BASE_HUMBLE_0_MD5='36d3fb101ab7c674d475b8f0b59d5e68'
ARCHIVE_BASE_HUMBLE_0_SIZE='440000'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.0-humble1'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='data'
CONTENT_GAME_BIN_FILES='
AnomalyWarzoneEarth'
CONTENT_GAME_DATA_FILES='
icon.png
*.idx
*.dat'
CONTENT_DOC_DATA_FILES='
README
Copyright*'

APP_MAIN_EXE='AnomalyWarzoneEarth'
APP_MAIN_ICON='icon.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libgcc_s.so.1
libGL.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6'

PRELOAD_HACKS_LIST='
HACK_LOADING_TIME'

# LD_PRELOAD shim working around the infinite loading times

HACK_LOADING_TIME_NAME='finite-loading-time'
HACK_LOADING_TIME_DESCRIPTION='LD_PRELOAD shim working around the infinite loading times'
HACK_LOADING_TIME_PACKAGE='PKG_BIN'
HACK_LOADING_TIME_SOURCE='
#define _GNU_SOURCE
#include <dlfcn.h>
#include <semaphore.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

static int (*_realSemTimedWait)(sem_t *, const struct timespec *) = NULL;

int sem_timedwait(sem_t *sem, const struct timespec *abs_timeout) {
	if (abs_timeout->tv_nsec >= 1000000000) {
		((struct timespec *)abs_timeout)->tv_nsec -= 1000000000;
		((struct timespec *)abs_timeout)->tv_sec++;
	}
	return _realSemTimedWait(sem, abs_timeout);
}
__attribute__((constructor)) void init(void) {
	_realSemTimedWait = dlsym(RTLD_NEXT, "sem_timedwait");
}
'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Build and include the LD_PRELOAD shims

hacks_inclusion_default

# Extract game data

archive_extraction_default
case "$(current_archive)" in
	('ARCHIVE_BASE_HUMBLE_1')
		ARCHIVE_INNER_PATH="${PLAYIT_WORKDIR}/gamedata/AnomalyWarzoneEarth-Installer"
		## This MojoSetup installer is not using a Makeself wrapper.
		ARCHIVE_INNER_EXTRACTOR='bsdtar'
		archive_extraction 'ARCHIVE_INNER'
		rm "$(archive_path 'ARCHIVE_INNER')"
	;;
esac

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
