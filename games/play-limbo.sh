#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 BetaRays
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Limbo
# send your bug reports to contact@dotslashplay.it
###

script_version=20240623.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='limbo'
GAME_NAME='Limbo'

ARCHIVE_BASE_1_NAME='Limbo-Linux-2014-06-18.zip'
ARCHIVE_BASE_1_MD5='5bb49941722ff6d4d6a494e064fd197e'
ARCHIVE_BASE_1_SIZE='190000'
ARCHIVE_BASE_1_VERSION='1.3-humble2'
ARCHIVE_BASE_1_URL='https://www.humblebundle.com/store/limbo'

ARCHIVE_BASE_0_NAME='Limbo-Linux-2014-06-18.sh'
ARCHIVE_BASE_0_MD5='9b453abcb859c31cc645a7207de08329'
ARCHIVE_BASE_0_TYPE='mojosetup'
ARCHIVE_BASE_0_SIZE='120000'
ARCHIVE_BASE_0_VERSION='1.3-humble1'

CONTENT_PATH_DEFAULT='data'
CONTENT_GAME_BIN_FILES='
limbo'
CONTENT_GAME_DATA_FILES='
data
settings.txt
limbo.png
*.pkg'
CONTENT_DOC_DATA_FILES='
README-linux.txt'

USER_PERSISTENT_FILES='
settings.txt'

APP_MAIN_EXE='limbo'
APP_MAIN_ICON='limbo.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
libSDL2-2.0.so.0
libstdc++.so.6'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'
case "$(current_archive)" in
	('ARCHIVE_BASE_1')
		ARCHIVE_INNER_PATH="${PLAYIT_WORKDIR}/gamedata/Limbo-Linux-2014-06-18.sh"
		archive_extraction 'ARCHIVE_INNER'
		rm "$ARCHIVE_INNER_PATH"
	;;
esac

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
