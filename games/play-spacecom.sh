#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Spacecom
# send your bug reports to contact@dotslashplay.it
###

script_version=20230212.1

GAME_ID='spacecom'
GAME_NAME='Spacecom'

ARCHIVE_BASE_HUMBLE_0='spacecomLinux0.9.1035.zip'
ARCHIVE_BASE_HUMBLE_0_MD5='a89515caa9e79a36b1769fbecc71e3d8'
ARCHIVE_BASE_HUMBLE_0_SIZE='230000'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.0.9.1035-humble.2014.11.03'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/spacecom'

ARCHIVE_BASE_GOG_0='gog_spacecom_2.4.0.9.sh'
ARCHIVE_BASE_GOG_0_MD5='ee70b48ffa207bc1c668d50753ec24b5'
ARCHIVE_BASE_GOG_0_TYPE='mojosetup'
ARCHIVE_BASE_GOG_0_SIZE='230000'
ARCHIVE_BASE_GOG_0_VERSION='1.0.9.1035-gog.2.4.0.9'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/spacecom'

UNITY3D_NAME='SPACECOM'

CONTENT_PATH_DEFAULT_HUMBLE='Linux'
CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_LIBS_BIN_PATH_HUMBLE="${CONTENT_PATH_DEFAULT_HUMBLE}/${UNITY3D_NAME}/Plugins/x86"
CONTENT_LIBS_BIN_PATH_GOG="${CONTENT_PATH_DEFAULT_GOG}/${UNITY3D_NAME}/Plugins/x86"
CONTENT_LIBS_BIN_FILES='
ScreenSelector.so'
CONTENT_GAME_BIN_FILES="
${UNITY3D_NAME}.x86
${UNITY3D_NAME}_Data/Mono/x86"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data/Managed
${UNITY3D_NAME}_Data/Mono/etc
${UNITY3D_NAME}_Data/Resources
${UNITY3D_NAME}_Data/mainData
${UNITY3D_NAME}_Data/level?
${UNITY3D_NAME}_Data/*.assets
${UNITY3D_NAME}_Data/*.png"
CONTENT_DOC_DATA_FILES='
SPACECOM Game Manual.pdf'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="$GAME_ID-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libatk-1.0.so.0
libcairo.so.2
libc.so.6
libdl.so.2
libfontconfig.so.1
libfreetype.so.6
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libgdk-x11-2.0.so.0
libgio-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libGLU.so.1
libgmodule-2.0.so.0
libgobject-2.0.so.0
libgthread-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpango-1.0.so.0
libpangocairo-1.0.so.0
libpangoft2-1.0.so.0
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXext.so.6
libXrandr.so.2'

# Load common functions

target_version='2.21'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"
	set_standard_permissions .
)

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packageS

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
