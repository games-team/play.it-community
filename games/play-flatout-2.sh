#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Flatout 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20231213.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='flatout-2'
GAME_NAME='FlatOut 2'

ARCHIVE_BASE_2_NAME='flatout_2_gog_3_23461.sh'
ARCHIVE_BASE_2_MD5='5529dcd679eae03f23d9807efd22a182'
ARCHIVE_BASE_2_SIZE='3800000'
ARCHIVE_BASE_2_VERSION='1.2-gog23461'
ARCHIVE_BASE_2_URL='https://www.gog.com/game/flatout_2'

ARCHIVE_BASE_1_NAME='gog_flatout_2_2.1.0.6.sh'
ARCHIVE_BASE_1_MD5='77cbd07105aa202ef808edebda15833a'
ARCHIVE_BASE_1_SIZE='3400000'
ARCHIVE_BASE_1_VERSION='1.2-gog2.1.0.6'

ARCHIVE_BASE_0_NAME='gog_flatout_2_2.0.0.4.sh'
ARCHIVE_BASE_0_MD5='cdc453f737159ac62bd9f59540002610'
ARCHIVE_BASE_0_SIZE='3600000'
ARCHIVE_BASE_0_VERSION='1.2-gog2.0.0.4'

CONTENT_PATH_DEFAULT='data/noarch/prefix/drive_c/GOG Games/FlatOut 2'
CONTENT_GAME_BIN_FILES='
flatout2.exe'
CONTENT_GAME_DATA_FILES='
filesystem
patch
flatout2.ico
*.bfs'
CONTENT_DOC_DATA_FILES='
readme.htm
*.pdf'

USER_PERSISTENT_DIRECTORIES='
savegame'

## TODO: Check if this winetricks verb is required.
WINE_WINETRICKS_VERBS='d3dx9'

APP_MAIN_EXE='flatout2.exe'
APP_MAIN_ICON='flatout2.ico'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Convert file paths to lower case.
	tolower .
)

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
