#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Out There
# send your bug reports to contact@dotslashplay.it
###

script_version=20240623.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='out-there'
GAME_NAME='Out There: Ω Edition'

ARCHIVE_BASE_LINUX_0_NAME='OutThere2-4-2Linux.zip'
ARCHIVE_BASE_LINUX_0_MD5='8ea51a42c9ad221e3d258e404c7106b0'
ARCHIVE_BASE_LINUX_0_SIZE='340000'
ARCHIVE_BASE_LINUX_0_VERSION='2.4.2-humble170213'
ARCHIVE_BASE_LINUX_0_URL='https://www.humblebundle.com/store/out-there-edition'

## Support for the Windows version is included because of poor mouse support in the Linux build.
ARCHIVE_BASE_WINDOWS_0_NAME='OutThere2-4-2.zip'
ARCHIVE_BASE_WINDOWS_0_MD5='8b23dde3778ade4db73a3ed76c4134cd'
ARCHIVE_BASE_WINDOWS_0_SIZE='300000'
ARCHIVE_BASE_WINDOWS_0_VERSION='2.4.2-humble170213'
ARCHIVE_BASE_WINDOWS_0_URL='https://www.humblebundle.com/store/out-there-edition'

UNITY3D_NAME_LINUX='OutThereOmega'
UNITY3D_NAME_WINDOWS='outthereomega'
UNITY3D_PLUGINS='
ScreenSelector.so'

CONTENT_PATH_DEFAULT='.'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Local Settings/Application Data/MiClos Studio/OutThereOmegaEdition'

PACKAGES_LIST_LINUX='
PKG_BIN64
PKG_BIN32
PKG_DATA'
PACKAGES_LIST_WINDOWS='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_LINUX="${PKG_DATA_ID}-linux"
PKG_DATA_ID_WINDOWS="${PKG_DATA_ID}-windows"
PKG_DATA_PROVIDES="
$PKG_DATA_ID"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ID="$GAME_ID"
PKG_BIN_PROVIDES="
$PKG_BIN_ID"
PKG_BIN64_PROVIDES="$PKG_BIN_PROVIDES"
PKG_BIN32_PROVIDES="$PKG_BIN_PROVIDES"
PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'

PKG_BIN_ID_LINUX="${GAME_ID}-linux"
PKG_BIN64_ID_LINUX="$PKG_BIN_ID_LINUX"
PKG_BIN32_ID_LINUX="$PKG_BIN_ID_LINUX"
PKG_BIN_DEPS_LINUX="$PKG_DATA_ID_LINUX"
PKG_BIN64_DEPS_LINUX="$PKG_BIN_DEPS_LINUX"
PKG_BIN32_DEPS_LINUX="$PKG_BIN_DEPS_LINUX"
PKG_BIN_DEPENDENCIES_LIBRARIES_LINUX='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXrandr.so.2'
PKG_BIN64_DEPENDENCIES_LIBRARIES_LINUX="$PKG_BIN_DEPENDENCIES_LIBRARIES_LINUX"
PKG_BIN32_DEPENDENCIES_LIBRARIES_LINUX="$PKG_BIN_DEPENDENCIES_LIBRARIES_LINUX"

PKG_BIN_ID_WINDOWS="${PKG_BIN_ID}-windows"
PKG_BIN_ARCH_WINDOWS='32'
PKG_BIN_DEPS_WINDOWS="$PKG_DATA_ID_WINDOWS"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Delete Apple archives junk.
	find . -name '*.rsrc' -delete
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

case "$(current_archive)" in
	('ARCHIVE_BASE_LINUX_'*)
		launchers_generation 'PKG_BIN64'
		launchers_generation 'PKG_BIN32'
	;;
	('ARCHIVE_BASE_WINDOWS_'*)
		launchers_generation 'PKG_BIN'
	;;
esac

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
