#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# A Hat in Time
# send your bug reports to contact@dotslashplay.it
###

script_version=20240522.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='a-hat-in-time'
GAME_NAME='A Hat in Time'

ARCHIVE_BASE_0_NAME='setup_a_hat_in_time_nyakuza_metro_1.12_(64bit)_(35607).exe'
ARCHIVE_BASE_0_MD5='b488a803f372891556adde2589b87643'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_a_hat_in_time_nyakuza_metro_1.12_(64bit)_(35607)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='c0b7186f04d9d07baa835cd4186a5fb9'
ARCHIVE_BASE_0_PART2_NAME='setup_a_hat_in_time_nyakuza_metro_1.12_(64bit)_(35607)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='7ce5f0d9d40007cb4c12e9312f0cfc57'
ARCHIVE_BASE_0_SIZE='8000000'
ARCHIVE_BASE_0_VERSION='1.12-gog35607'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/a_hat_in_time'

## Despite the similar directories structure,
## this is not an Unreal Engine 4 game but an Unreal Engine 3 one.
CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
binaries
engine'
CONTENT_GAME_DATA_FILES='
hatintimegame'
CONTENT_GAME0_BIN_PATH='app'
CONTENT_GAME0_BIN_FILES='
engine'
CONTENT_GAME0_DATA_PATH='app'
CONTENT_GAME0_DATA_FILES='
hatintimegame'

## .NET Framework 4 is required for controller support.
## TODO: Check if Mono could be used instead.
WINE_DLLOVERRIDES_DEFAULT='winemenubuilder.exe,mshtml='
WINE_WINETRICKS_VERBS='dotnet40'

APP_MAIN_EXE='binaries/win64/hatintimegame.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
