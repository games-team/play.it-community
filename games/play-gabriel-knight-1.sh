#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Gabriel Knight 1
# send your bug reports to contact@dotslashplay.it
###

script_version=20240430.2

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='gabriel-knight-1'
GAME_NAME='Gabriel Knight 1: Sins of the Fathers'

ARCHIVE_BASE_0_NAME='setup_gabriel_knight_-_sins_of_the_fathers_20th_anniversary_edition_2.02_(16216).exe'
ARCHIVE_BASE_0_MD5='ac1879d3217f7ce12403e51ccb463792'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_gabriel_knight_-_sins_of_the_fathers_20th_anniversary_edition_2.02_(16216)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='7d97631d1b1d636faa4eefd7ae49c16c'
ARCHIVE_BASE_0_SIZE='3016016'
ARCHIVE_BASE_0_VERSION='2.02-gog16216'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/gabriel_knight_sins_of_the_fathers_20th_anniversary_edition'

UNITY3D_NAME='gk1'

CONTENT_PATH_DEFAULT='app'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Phoenix Online Studios/GK1'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
