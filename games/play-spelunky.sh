#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Spelunky
# send your bug reports to contact@dotslashplay.it
###

script_version=20240701.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='spelunky'
GAME_NAME='Spelunky'

ARCHIVE_BASE_0_NAME='setup_spelunky_2.1.0.9.exe'
ARCHIVE_BASE_0_MD5='bff7c275053137881c9f2f6df16ee4b3'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='180000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.1.0.9'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/spelunky'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
fmodex.dll
spelunky.exe'
CONTENT_GAME_DATA_FILES='
data'

USER_PERSISTENT_FILES='
data/config.txt
data/spelunky_save.sav
data/spelunky_save.bak'

APP_MAIN_EXE='spelunky.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
