#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Turnip Boy Commits Tax Evasion
# send your bug reports to contact@dotslashplay.it
###

script_version=20241218.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='turnip-boy-commits-tax-evasion'
GAME_NAME='Turnip Boy Commits Tax Evasion'

ARCHIVE_BASE_1_NAME='setup_turnip_boy_commits_tax_evasion_v1.1.3f1_(64bit)_(58989).exe'
ARCHIVE_BASE_1_MD5='89752267d5f53df91532cdb925d39ace'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='672316'
ARCHIVE_BASE_1_VERSION='1.1.3f1-gog58989'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/turnip_boy_commits_tax_evasion'

ARCHIVE_BASE_0_NAME='setup_turnip_boy_commits_tax_evasion_v1.1.0f2_(64bit)_(49701).exe'
ARCHIVE_BASE_0_MD5='8253f3bec3d84f71f9fdf5e5ec1e5a18'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='650000'
ARCHIVE_BASE_0_VERSION='1.1.0f2-gog49701'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/turnip_boy_commits_tax_evasion'

UNITY3D_NAME='turnip boy commits tax evasion'

CONTENT_PATH_DEFAULT='.'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Snoozy Kazoo/Turnip Boy Commits Tax Evasion'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
