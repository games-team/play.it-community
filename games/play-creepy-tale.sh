#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Creepy Tale series:
# - Creepy Tale 1
# - Creepy Tale 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20240811.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID_EPISODE1='creepy-tale-1'
GAME_NAME_EPISODE1='Creepy Tale'

GAME_ID_EPISODE2='creepy-tale-2'
GAME_NAME_EPISODE2='Creepy Tale 2'

ARCHIVE_BASE_EPISODE1_0_NAME='creepy_tale_english_1_0_2c_42446.sh'
ARCHIVE_BASE_EPISODE1_0_MD5='727b15afedd0ef2e58565b40419e76de'
ARCHIVE_BASE_EPISODE1_0_SIZE='180064'
ARCHIVE_BASE_EPISODE1_0_VERSION='1.0.2c-gog42446'
ARCHIVE_BASE_EPISODE1_0_URL='https://www.gog.com/game/creepy_tale'

ARCHIVE_BASE_EPISODE2_0_NAME='creepy_tale_2_1_0_0a_53727.sh'
ARCHIVE_BASE_EPISODE2_0_MD5='e2626eaebe690d4b8cbf7506ff78fff9'
ARCHIVE_BASE_EPISODE2_0_SIZE='560000'
ARCHIVE_BASE_EPISODE2_0_VERSION='2.1.0.0a-gog53727'
ARCHIVE_BASE_EPISODE2_0_URL='https://www.gog.com/game/creepy_tale_2'

UNITY3D_NAME_EPISODE1='creepy_tale'
UNITY3D_NAME_EPISODE2='CreepyTale2'
## TODO: Check if the Steam library is required.
UNITY3D_PLUGINS_EPISODE1='
libsteam_api.so'
UNITY3D_PLUGINS_EPISODE2='
libfmodstudio.so
libresonanceaudio.so'
## TODO: Check if the Steam library is required.
UNITY3D_PLUGINS_EPISODE2="$UNITY3D_PLUGINS_EPISODE2
libsteam_api.so"

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID_EPISODE1="${GAME_ID_EPISODE1}-data"
PKG_DATA_ID_EPISODE2="${GAME_ID_EPISODE2}-data"
PKG_DATA_DESCRIPTION='data'
## Ensure easy upgrade from packages generated with pre-20230414.1 game scripts.
PKG_DATA_PROVIDES_EPISODE1="${PKG_DATA_PROVIDES_EPISODE1:-}
creepy-tale-data"

PKG_BIN_ARCH='64'
PKG_BIN_DEPS_EPISODE1="$PKG_DATA_ID_EPISODE1"
PKG_BIN_DEPS_EPISODE2="$PKG_DATA_ID_EPISODE2"
PKG_BIN_DEPENDENCIES_LIBRARIES_EPISODE1='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libz.so.1'
PKG_BIN_DEPENDENCIES_LIBRARIES_EPISODE2='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'
## Ensure easy upgrade from packages generated with pre-20230414.1 game scripts.
PKG_BIN_PROVIDES_EPISODE1="${PKG_BIN_PROVIDES_EPISODE1:-}
creepy-tale-data"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
