#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Spiritfarer
# send your bug reports to contact@dotslashplay.it
###

script_version=20241209.3

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='spiritfarer'
GAME_NAME='Spiritfarer'

GAME_ID_DEMO="${GAME_ID}-demo"
GAME_NAME_DEMO="$GAME_NAME (demo)"

ARCHIVE_BASE_4_NAME='spiritfarer_35325c_linux_60321.sh'
ARCHIVE_BASE_4_MD5='6c01cdd8cd04d23b46153367682722fb'
ARCHIVE_BASE_4_SIZE='6308582'
ARCHIVE_BASE_4_VERSION='35325c-gog60321'
ARCHIVE_BASE_4_URL='https://www.gog.com/game/spiritfarer'

ARCHIVE_BASE_3_NAME='spiritfarer_35325c_linux_54594.sh'
ARCHIVE_BASE_3_MD5='39f342a2616f42406e8fd4d3f4576acb'
ARCHIVE_BASE_3_SIZE='6400000'
ARCHIVE_BASE_3_VERSION='35325c-gog54594'

ARCHIVE_BASE_2_NAME='spiritfarer_34556_linux_50344.sh'
ARCHIVE_BASE_2_MD5='7dd310caa09790c0b56d4a9a67e7f9dc'
ARCHIVE_BASE_2_SIZE='7500000'
ARCHIVE_BASE_2_VERSION='34556-gog50344'

ARCHIVE_BASE_1_NAME='spiritfarer_32698_linux_42758.sh'
ARCHIVE_BASE_1_MD5='2f1c327bd020676eabd78f99124a786f'
ARCHIVE_BASE_1_SIZE='6900000'
ARCHIVE_BASE_1_VERSION='32698-gog42758'

ARCHIVE_BASE_0_NAME='spiritfarer_32513_linux_41748.sh'
ARCHIVE_BASE_0_MD5='8e6e49ce7e7571091f91d82706d2df8e'
ARCHIVE_BASE_0_SIZE='6900000'
ARCHIVE_BASE_0_VERSION='32513-gog41748'

ARCHIVE_BASE_DEMO_0_NAME='spiritfarer_demo_demo_20200525a_linux_38575.sh'
ARCHIVE_BASE_DEMO_0_MD5='952e425281143b061462e9122e638531'
ARCHIVE_BASE_DEMO_0_SIZE='2782689'
ARCHIVE_BASE_DEMO_0_VERSION='2020.05.25-gog38575'
ARCHIVE_BASE_DEMO_0_URL='https://www.gog.com/game/spiritfarer_demo'

UNITY3D_NAME='spiritfarer'
UNITY3D_NAME_DEMO='Spiritfarer'
## TODO: Check if the shipped Galaxy libraries are required.
UNITY3D_PLUGINS='
libGalaxy64.so
libGalaxyCSharpGlue.so'
UNITY3D_PLUGINS_DEMO='
ScreenSelector.so'
## TODO: Check if the shipped Steam libraries are required.
UNITY3D_PLUGINS_DEMO="$UNITY3D_PLUGINS_DEMO
libCSteamworks.so
libsteam_api.so"

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libz.so.1'
PKG_BIN_DEPENDENCIES_LIBRARIES_DEMO='
libatk-1.0.so.0
libcairo.so.2
libc.so.6
libdl.so.2
libfontconfig.so.1
libfreetype.so.6
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libgdk-x11-2.0.so.0
libgio-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpango-1.0.so.0
libpangocairo-1.0.so.0
libpangoft2-1.0.so.0
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_DEMO="${GAME_ID_DEMO}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# GOG build 50344 - Check for the presence of required extra archives

case "$(current_archive)" in
	('ARCHIVE_BASE_2')
		ARCHIVE_REQUIRED_MISSING_PLUGINS_NAME='spiritfarer_missing-plugins.tar.xz'
		ARCHIVE_REQUIRED_MISSING_PLUGINS_MD5='0fe61aeadf3066e724dca3db627d6a49'
		ARCHIVE_REQUIRED_MISSING_PLUGINS_URL='https://downloads.dotslashplay.it/games/spiritfarer/'
		archive_initialize_required \
			'ARCHIVE_MISSING_PLUGINS' \
			'ARCHIVE_REQUIRED_MISSING_PLUGINS'
	;;
esac

# Extract game data

archive_extraction_default
if archive_is_available 'ARCHIVE_MISSING_PLUGINS'; then
	archive_extraction 'ARCHIVE_MISSING_PLUGINS'
fi
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Enforce a consistent Unity3D name.
	unity3d_name=$(unity3d_name)
	alternative_name='Spiritfarer'
	### Old builds ship the binary with a .exe file extension.
	if [ -e "${alternative_name}.exe" ]; then
		mv "${alternative_name}.exe" "${unity3d_name}.x86_64"
		mv "${alternative_name}_Data" "${unity3d_name}_Data"
	fi
)

# Include game data

content_inclusion_icons 'PKG_DATA'
## GOG build 50344 - Include missing plugins.
if archive_is_available 'ARCHIVE_MISSING_PLUGINS'; then
	CONTENT_GAME_MISSING_PLUGINS_BIN_PATH='.'
	CONTENT_GAME_MISSING_PLUGINS_BIN_FILES='
	libGalaxy64.so
	libGalaxyCSharpGlue.so'
	content_inclusion 'GAME_MISSING_PLUGINS_BIN' 'PKG_BIN' "$(path_game_data)/$(unity3d_name)_Data/Plugins"
fi
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
