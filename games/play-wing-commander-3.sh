#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Wing Commander 3
# send your bug reports to contact@dotslashplay.it
###

script_version=20240428.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='wing-commander-3'
GAME_NAME='Wing Commander Ⅲ'

ARCHIVE_BASE_1_NAME='setup_wing_commander_iii_1.4_(28045).exe'
ARCHIVE_BASE_1_MD5='9418288d818315fbbede459bef76b82c'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_wing_commander_iii_1.4_(28045)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='1caaf5ba29075e67a00b8009bc53e463'
ARCHIVE_BASE_1_SIZE='1900000'
ARCHIVE_BASE_1_VERSION='1.4-gog28045'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/wing_commander_3_heart_of_the_tiger'

ARCHIVE_BASE_0_NAME='setup_wing_commander3_2.1.0.7.exe'
ARCHIVE_BASE_0_MD5='c9c9b539e6e1f0b0509b6f777878d91e'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='1900000'
ARCHIVE_BASE_0_VERSION='1.4-gog2.1.0.7'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_MAIN_FILES='
wc3
data.dat
objects.tre'
CONTENT_DOC_MAIN_FILES='
*.pdf'

GAME_IMAGE='data.dat'

USER_PERSISTENT_DIRECTORIES='
wc3'
USER_PERSISTENT_FILES='
*.WSG'

APP_MAIN_EXE='wc3.exe'
APP_MAIN_ICON='app/goggame-1207658966.ico'
APP_MAIN_ICON_0='goggame-1207658966.ico'
## The type can not be omitted, because the binary is actually on the CD-ROM image.
APP_MAIN_TYPE='dosbox'
## Run the game from the mounted CD-ROM image.
APP_MAIN_DOSBOX_PRERUN='d:'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

## Work around the binary presence check,
## it is actually included in the CD-ROM image.
launcher_target_presence_check() { return 0; }
launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
