#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Lords of Xulima
# send your bug reports to contact@dotslashplay.it
###

script_version=20240504.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='lords-of-xulima'
GAME_NAME='Lords of Xulima'

ARCHIVE_BASE_0_NAME='gog_lords_of_xulima_2.3.0.9.sh'
ARCHIVE_BASE_0_MD5='480abf8d929da622eacd69595a4ebc80'
ARCHIVE_BASE_0_SIZE='1700000'
ARCHIVE_BASE_0_VERSION='2.1.1-gog2.3.0.9'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/lords_of_xulima'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN_PATH="${CONTENT_PATH_DEFAULT}/Linux/lib"
CONTENT_LIBS_BIN_FILES='
libmonoboehm-2.0.a
libmonoboehm-2.0.so.1
libmono-profiler-aot.so.0.0.0
libmono-profiler-cov.so.0.0.0
libmono-profiler-iomap.so.0.0.0
libmono-profiler-log.so.0.0.0
libmonosgen-2.0.so.1
libmonosgen-2.0.so.1.0.0
libMonoPosixHelper.so
libMonoSupportW.so'
CONTENT_LIBS0_BIN_FILES='
libbass.so'
CONTENT_GAME_BIN_FILES='
Linux/etc/mono
Linux/lib/mono
*.conf
*.config
*.cpp
*.dll
*.exe
*.hqx
*.manifest
LOXLinux
MusicPlayer'
CONTENT_FONTS_DATA_FILES='
FoL_BB_RU.ttf
FoL_CB_RU.ttf
FoL_MB_RU.ttf
LOXBold.ttf
LOXCondensed.ttf
LOXMedium.ttf
WenQuanYiZenHei.ttf'
CONTENT_GAME_DATA_FILES='
DXApp_*
SOL_*
Cinematic
CSteamworks.bundle
Manual
Resources
*.dx*
*.fx
*.glsl
*.icns
*.jx*
*.sqlite
*.suo
*.txt
SOL'

FAKE_HOME_PERSISTENT_DIRECTORIES='
My Games/Lords of Xulima'

## TODO: Check if the system-provided Mono runtime could be used instead of the shipped binaries.
APP_MAIN_EXE='LOXLinux'
APP_MAIN_OPTIONS='--gc=sgen'
APP_MAIN_ICON='LoX.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgdiplus.so
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

## Apply common workarounds for Mono games.
APP_MAIN_PRERUN="$(application_prerun 'APP_MAIN')
$(mono_launcher_tweaks)
"
launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
