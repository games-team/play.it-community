#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2021 Anna Lea
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Beholder series:
# - Beholder
# - Beholder 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20241124.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID_BEHOLDER1='beholder-1'
GAME_NAME_BEHOLDER1='Beholder'

GAME_ID_BEHOLDER2='beholder-2'
GAME_NAME_BEHOLDER2='Beholder 2'

## This DRM-free archive is no longer available for sale from Humble Bundle
ARCHIVE_BASE_BEHOLDER1_0_NAME='BeholderLinux_v1.3.0.9120.zip'
ARCHIVE_BASE_BEHOLDER1_0_MD5='580a52329b99c701acf3b3665068c6c6'
ARCHIVE_BASE_BEHOLDER1_0_SIZE='1781531'
ARCHIVE_BASE_BEHOLDER1_0_VERSION='1.3.0.9120-humble1'

ARCHIVE_BASE_BEHOLDER2_0_NAME='beholder_2_1_1_13306_25983.sh'
ARCHIVE_BASE_BEHOLDER2_0_MD5='af3cb735132ee4fbc17079b932d72dd6'
ARCHIVE_BASE_BEHOLDER2_0_SIZE='3700000'
ARCHIVE_BASE_BEHOLDER2_0_VERSION='1.1.13306-gog25983'
ARCHIVE_BASE_BEHOLDER2_0_URL='https://www.gog.com/game/beholder_2'

UNITY3D_NAME_BEHOLDER1='Beholder'
UNITY3D_NAME_BEHOLDER2='Beholder2'
UNITY3D_PLUGINS_BEHOLDER1='
ScreenSelector.so'
## TODO: Check if the Steam libraries are required
UNITY3D_PLUGINS_BEHOLDER1="$UNITY3D_PLUGINS_BEHOLDER1
libCSteamworks.so
libsteam_api.so"

CONTENT_PATH_DEFAULT_BEHOLDER1='BeholderLinux_v1.3.0.9120'
## FIXME: The list of Unity3D plugins to include should be set with UNITY3D_PLUGINS
CONTENT_GAME0_BIN64_FILES_BEHOLDER2="
${UNITY3D_NAME_BEHOLDER2}_Data/Plugins/x86_64"
CONTENT_GAME0_BIN32_FILES_BEHOLDER2="
${UNITY3D_NAME_BEHOLDER2}_Data/Plugins/x86"
CONTENT_GAME0_DATA_FILES_BEHOLDER2='
hashes.txt'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES_BEHOLDER1='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXrandr.so.2'
## TODO: This list of dependencies should be completed
PKG_BIN_DEPENDENCIES_LIBRARIES_BEHOLDER2='
libc.so.6
libstdc++.so.6
libgdk-x11-2.0.so.0
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES_BEHOLDER1="$PKG_BIN_DEPENDENCIES_LIBRARIES_BEHOLDER1"
PKG_BIN64_DEPENDENCIES_LIBRARIES_BEHOLDER2="$PKG_BIN_DEPENDENCIES_LIBRARIES_BEHOLDER2"
PKG_BIN32_DEPENDENCIES_LIBRARIES_BEHOLDER1="$PKG_BIN_DEPENDENCIES_LIBRARIES_BEHOLDER1"
PKG_BIN32_DEPENDENCIES_LIBRARIES_BEHOLDER2="$PKG_BIN_DEPENDENCIES_LIBRARIES_BEHOLDER2"
## Ensure easy upgrades from packages generated with pre-20241111.1 game scripts
PKG_BIN_PROVIDES_BEHOLDER1="${PKG_BIN_PROVIDES_BEHOLDER1:-}
beholder"
PKG_BIN64_PROVIDES_BEHOLDER1="$PKG_BIN_PROVIDES_BEHOLDER1"
PKG_BIN32_PROVIDES_BEHOLDER1="$PKG_BIN_PROVIDES_BEHOLDER1"

PKG_DATA_ID_BEHOLDER1="${GAME_ID_BEHOLDER1}-data"
PKG_DATA_ID_BEHOLDER2="${GAME_ID_BEHOLDER2}-data"
PKG_DATA_DESCRIPTION='data'
## Ensure easy upgrades from packages generated with pre-20241111.1 game scripts
PKG_DATA_PROVIDES_BEHOLDER1="${PKG_DATA_PROVIDES_BEHOLDER1:-}
beholder-data"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
