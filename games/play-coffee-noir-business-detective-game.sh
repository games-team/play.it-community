#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Coffee Noir - Business Detective Game
# send your bug reports to contact@dotslashplay.it
###

script_version=20240615.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='coffee-noir-business-detective-game'
GAME_NAME='Coffee Noir - Business Detective Game'

GAME_ID_DEMO="${GAME_ID}-demo"
GAME_NAME_DEMO="$GAME_NAME (demo)"

ARCHIVE_BASE_0_NAME='coffee_noir_business_detective_game_1_0_1_50385.sh'
ARCHIVE_BASE_0_MD5='dc158baf4bf3d80524603582ee3be780'
ARCHIVE_BASE_0_SIZE='1459648'
ARCHIVE_BASE_0_VERSION='1.0.1-gog50385'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/coffee_noir_business_detective_game'

ARCHIVE_BASE_DEMO_0_NAME='coffee_noir_business_detective_game_demo_2_0_0_50174.sh'
ARCHIVE_BASE_DEMO_0_MD5='7e3ffe0fbc1b0992a5e241432c85da29'
ARCHIVE_BASE_DEMO_0_SIZE='591420'
ARCHIVE_BASE_DEMO_0_VERSION='2.0.0-gog50174'
ARCHIVE_BASE_DEMO_0_URL='https://www.gog.com/game/coffee_noir_business_detective_game_demo'

UNITY3D_NAME='CoffeeNoir'
UNITY3D_NAME_DEMO='CoffeeNoirDemo'
UNITY3D_PLUGINS='
libSQLite-Interop.so'

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_DEMO="${GAME_ID_DEMO}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPS_DEMO="$PKG_DATA_ID_DEMO"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
