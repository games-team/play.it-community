#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2015 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Worms 1
# send your bug reports to contact@dotslashplay.it
###

script_version=20240720.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='worms-1'
GAME_NAME='Worms United'

ARCHIVE_BASE_1_NAME='setup_worms_united_1.0_(28045).exe'
ARCHIVE_BASE_1_MD5='fd76a7ea0a94f0e3e9cdd2b3b3f9fdd4'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='206589'
ARCHIVE_BASE_1_VERSION='1.0-gog28045'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/worms_united'

ARCHIVE_BASE_0_NAME='setup_worms_united_2.0.0.20.exe'
ARCHIVE_BASE_0_MD5='619421cafa20f478d19222e3f49d77b6'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='220000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.0.0.20'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_MAIN_FILES='
bin
batch
data
extras
dos4gw.exe
worms.cfg
worms.dat
worms.gog
worms*.ogg'
CONTENT_DOC_MAIN_FILES='
docs
manual.pdf'

GAME_IMAGE='worms.dat'

USER_PERSISTENT_FILES='
worms.cfg'
USER_PERSISTENT_DIRECTORIES='
data'

APP_MAIN_TYPE='dosbox'
APP_MAIN_EXE='bin\wrms.exe'
APP_MAIN_ICON='goggame-1207658991.ico'
APP_MAIN_ICON_0='gfw_high.ico'
## Set required environment variables.
APP_MAIN_DOSBOX_PRERUN='
SET wormscfg=C:\\worms.cfg
SET wormscd=D:'
## Play introduction videos
APP_MAIN_DOSBOX_PRERUN="${APP_MAIN_DOSBOX_PRERUN:-}"'
D:\\fmv\\play /modex D:\\fmv\\logo2.avi
D:\\fmv\\play /modex D:\\fmv\\logo1.avi
D:\\fmv\\play /modex D:\\fmv\\cinadd.avi
D:\\fmv\\play /modex D:\\fmv\\armup.avi
bin\\black.exe'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

## Work around the binary presence check.
## The regular check would fail due to "\" being used as a path separator.
launcher_target_presence_check() {
	local application_exe application_exe_path
	application_exe=$(application_exe "$application" | sed 's#\\#/#')
	application_exe_path=$(application_exe_path "$application_exe")
	test -f "$application_exe_path"
}
launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
