#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 BetaRays
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Rogue Legacy
# send your bug reports to contact@dotslashplay.it
###

script_version=20240831.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='rogue-legacy'
GAME_NAME='Rogue Legacy'

ARCHIVE_BASE_GOG_1_NAME='rogue_legacy_en_1_4_0_22617.sh'
ARCHIVE_BASE_GOG_1_MD5='60a1a7a7ff84a50e2ac52f2e44dce92d'
ARCHIVE_BASE_GOG_1_SIZE='366454'
ARCHIVE_BASE_GOG_1_VERSION='1.4.0-gog22617'
ARCHIVE_BASE_GOG_1_URL='https://www.gog.com/game/rogue_legacy'

ARCHIVE_BASE_GOG_0_NAME='gog_rogue_legacy_2.0.0.2.sh'
ARCHIVE_BASE_GOG_0_MD5='1b99d6122f0107b420cad9547efefc5e'
ARCHIVE_BASE_GOG_0_SIZE='240000'
ARCHIVE_BASE_GOG_0_VERSION='1.2.0b-gog2.0.0.2'

ARCHIVE_BASE_HUMBLE_0_NAME='roguelegacy-12282013-bin'
ARCHIVE_BASE_HUMBLE_0_MD5='b2a18745b911ed87a048440c2f8a0404'
ARCHIVE_BASE_HUMBLE_0_SIZE='240000'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.2.0b-humble131228'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/rogue-legacy'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='data'
CONTENT_LIBS_FILES='
libmojoshader.so'
## The shipped build of OpenAL must be used to prevent a crash on launch
CONTENT_LIBS_FILES="$CONTENT_LIBS_FILES
libopenal.so.1"
CONTENT_LIBS_LIBS64_PATH_GOG="${CONTENT_PATH_DEFAULT_GOG}/lib64"
CONTENT_LIBS_LIBS64_PATH_HUMBLE="${CONTENT_PATH_DEFAULT_HUMBLE}/lib64"
CONTENT_LIBS_LIBS64_FILES="$CONTENT_LIBS_FILES"
CONTENT_LIBS_LIBS32_PATH_GOG="${CONTENT_PATH_DEFAULT_GOG}/lib"
CONTENT_LIBS_LIBS32_PATH_HUMBLE="${CONTENT_PATH_DEFAULT_HUMBLE}/lib"
CONTENT_LIBS_LIBS32_FILES="$CONTENT_LIBS_FILES"
CONTENT_GAME_MAIN_FILES='
RogueLegacy.exe
RogueCastle.exe
Rogue Legacy.bmp
Content
mono*
DS2DEngine.dll
FNA.dll
FNA.dll.config
InputSystem.dll
SpriteSystem.dll
Tweener.dll'
CONTENT_DOC_MAIN_FILES='
Linux.README'

APP_MAIN_EXE_GOG='RogueLegacy.exe'
APP_MAIN_EXE_GOG_0='RogueCastle.exe'
APP_MAIN_EXE_HUMBLE='RogueCastle.exe'
APP_MAIN_ICON='Rogue Legacy.bmp'

PACKAGES_LIST='
PKG_MAIN
PKG_LIBS64
PKG_LIBS32'

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_LIBS'
PKG_MAIN_DEPENDENCIES_LIBRARIES='
libSDL2-2.0.so.0'
PKG_MAIN_DEPENDENCIES_MONO_LIBRARIES='
mscorlib.dll
Mono.Posix.dll
Mono.Security.dll
System.dll
System.Configuration.dll
System.Core.dll
System.Data.dll
System.Drawing.dll
System.Numerics.dll
System.Runtime.Serialization.dll
System.Security.dll
System.Xml.dll
System.Xml.Linq.dll'

PKG_LIBS_ID="${GAME_ID}-libs"
PKG_LIBS64_ID="$PKG_LIBS_ID"
PKG_LIBS32_ID="$PKG_LIBS_ID"
PKG_LIBS_DESCRIPTION='shipped libraries'
PKG_LIBS64_DESCRIPTION="$PKG_LIBS_DESCRIPTION"
PKG_LIBS32_DESCRIPTION="$PKG_LIBS_DESCRIPTION"
PKG_LIBS64_ARCH='64'
PKG_LIBS32_ARCH='32'
PKG_LIBS_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libm.so.6
libpthread.so.0
librt.so.1'
PKG_LIBS64_DEPENDENCIES_LIBRARIES="$PKG_LIBS_DEPENDENCIES_LIBRARIES"
PKG_LIBS32_DEPENDENCIES_LIBRARIES="$PKG_LIBS_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
