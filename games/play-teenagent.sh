#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Teen Agent
# send your bug reports to contact@dotslashplay.it
###

script_version=20240619.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='teenagent'
GAME_NAME='Teen Agent'

ARCHIVE_BASE_0_NAME='setup_teenagent_1.0_(15595).exe'
ARCHIVE_BASE_0_MD5='95126a8eba61b60e12535ebaa020c01b'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='32000'
ARCHIVE_BASE_0_VERSION='1.0-gog15595'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/teenagent'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_MAIN_FILES='
*.res'

APP_MAIN_SCUMMID='teenagent:teenagent'
APP_MAIN_ICON='goggame-1207658753.ico'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
