#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# A Boy and His Blob
# send your bug reports to contact@dotslashplay.it
###

script_version=20231017.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='a-boy-and-his-blob'
GAME_NAME='A Boy and His Blob'

ARCHIVE_BASE_ZOOM_0_NAME='A Boy and His Blob.tar.gz'
ARCHIVE_BASE_ZOOM_0_MD5='4e56d18404f82a2c6f6489661df807c8'
ARCHIVE_BASE_ZOOM_0_SIZE='1300000'
ARCHIVE_BASE_ZOOM_0_VERSION='2016.04.21-zoom1'
ARCHIVE_BASE_ZOOM_0_URL='https://www.zoom-platform.com/product/a-boy-and-his-blob'

ARCHIVE_BASE_GOG_0_NAME='gog_a_boy_and_his_blob_2.1.0.2.sh'
ARCHIVE_BASE_GOG_0_MD5='7025963a3a26f838877374f72ce3760d'
ARCHIVE_BASE_GOG_0_SIZE='1300000'
ARCHIVE_BASE_GOG_0_VERSION='2016.04.21-gog2.1.0.2'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/a_boy_and_his_blob'

CONTENT_PATH_DEFAULT_ZOOM='A Boy And His Blob/game'
CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_LIBS_BIN_FILES='
libfmod.so.7
libGLEW.so.1.10'
CONTENT_GAME_BIN_FILES='
Blob'
CONTENT_GAME_DATA_FILES='
content'

APP_MAIN_EXE='Blob'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libstdc++.so.6'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Check for the presence of optional extra archives

ARCHIVE_OPTIONAL_ICONS_0_NAME='a-boy-and-his-blob_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_0_MD5='2a555c1f6b02a45b8932c8e72a9c1dd6'
ARCHIVE_OPTIONAL_ICONS_0_URL='https://downloads.dotslashplay.it/game/a-boy-and-his-blob/'
archive_initialize_optional \
	'ARCHIVE_ICONS' \
	'ARCHIVE_OPTIONAL_ICONS_0'

# Extract game data

archive_extraction_default
if archive_is_available 'ARCHIVE_ICONS'; then
	archive_extraction 'ARCHIVE_ICONS'
fi

# Include game data

if archive_is_available 'ARCHIVE_ICONS'; then
	CONTENT_ICONS_DATA_PATH='.'
	CONTENT_ICONS_DATA_FILES='
	16x16
	32x32
	48x48
	64x64'
	content_inclusion 'ICONS_DATA' 'PKG_DATA' "$(path_icons)"
fi
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
