#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Disney 1994 games:
# - Aladdin
# - The Jungle Book
# - The Lion King
# send your bug reports to contact@dotslashplay.it
###

script_version=20240622.2

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID_ALADDIN='aladdin'
GAME_NAME_ALADDIN='Aladdin'

GAME_ID_JUNGLE='the-jungle-book'
GAME_NAME_JUNGLE='The Jungle Book'

GAME_ID_LION='the-lion-king'
GAME_NAME_LION='The Lion King'

## This installer is no longer sold by gog.com.
ARCHIVE_BASE_ALADDIN_0_NAME='gog_disney_s_aladdin_2.0.0.2.sh'
ARCHIVE_BASE_ALADDIN_0_MD5='9dd6d84c2276809c5630320335e3415b'
ARCHIVE_BASE_ALADDIN_0_SIZE='18000'
ARCHIVE_BASE_ALADDIN_0_VERSION='1.0-gog2.0.0.2'

## This installer is no longer sold by gog.com.
ARCHIVE_BASE_JUNGLE_0_NAME='gog_disney_s_the_jungle_book_2.0.0.2.sh'
ARCHIVE_BASE_JUNGLE_0_MD5='bcb57f4ff5cb1662ba3d4a9e34f263ad'
ARCHIVE_BASE_JUNGLE_0_SIZE='15000'
ARCHIVE_BASE_JUNGLE_0_VERSION='1.0-gog2.0.0.2'

## This installer is no longer sold by gog.com.
ARCHIVE_BASE_LION_0_NAME='gog_the_lion_king_2.0.0.2.sh'
ARCHIVE_BASE_LION_0_MD5='3b4f1118785e1f1cc769ae41379b7940'
ARCHIVE_BASE_LION_0_SIZE='16000'
ARCHIVE_BASE_LION_0_VERSION='1.0-gog2.0.0.2'

CONTENT_PATH_DEFAULT='data/noarch/data'
## FIXME: An explicit list of files should be set.
CONTENT_GAME_MAIN_FILES='
*'
CONTENT_DOC_MAIN_PATH='data/noarch/docs'
CONTENT_DOC_MAIN_FILES='
Manual.pdf'

USER_PERSISTENT_FILES='
*.cfg'

APP_MAIN_EXE_ALADDIN='ALADDIN.EXE'
APP_MAIN_EXE_JUNGLE='JUNGLE.EXE'
APP_MAIN_EXE_LION='LIONKING.EXE'
APP_MAIN_ICON='../support/icon.png'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
