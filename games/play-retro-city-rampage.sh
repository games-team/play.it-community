#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 BetaRays
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Retro City Rampage
# send your bug reports to contact@dotslashplay.it
###

script_version=20231001.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='retro-city-rampage'
GAME_NAME='Retro City Rampage'

ARCHIVE_BASE_32BIT_0_NAME='retrocityrampage-1.0-linux.i386.bin'
ARCHIVE_BASE_32BIT_0_MD5='35c776fa33af850158b0d6a886dfe2a0'
ARCHIVE_BASE_32BIT_0_EXTRACTOR='bsdtar'
ARCHIVE_BASE_32BIT_0_SIZE='17000'
ARCHIVE_BASE_32BIT_0_VERSION='1.53-humble1'
ARCHIVE_BASE_32BIT_0_URL='https://www.humblebundle.com/store/retro-city-rampage-dx'

ARCHIVE_BASE_64BIT_0='retrocityrampage-1.0-linux.x86_64.bin'
ARCHIVE_BASE_64BIT_0_MD5='4fc25ab742d5bd389bd4a76eb6ec987f'
ARCHIVE_BASE_64BIT_0_EXTRACTOR='bsdtar'
ARCHIVE_BASE_64BIT_0_SIZE='17000'
ARCHIVE_BASE_64BIT_0_VERSION='1.53-humble1'
ARCHIVE_BASE_64BIT_0_URL='https://www.humblebundle.com/store/retro-city-rampage-dx'

CONTENT_PATH_DEFAULT='data'
CONTENT_GAME_BIN_FILES='
retrocityrampage'
CONTENT_GAME_DATA_FILES='
audio_music_Linux.bap
audio_sfx_Linux.bap
gamedata_sdl.bfp
icon.png'

USER_PERSISTEN_FILES='
rcr_sdl.cfg
*.rsv'

APP_MAIN_EXE='retrocityrampage'
APP_MAIN_ICON='icon.png'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH_32BIT='32'
PKG_BIN_ARCH_64BIT='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
