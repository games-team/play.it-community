#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Shelter 1
# send your bug reports to contact@dotslashplay.it
###

script_version=20230918.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='shelter-1'
GAME_NAME='Shelter'

ARCHIVE_BASE_HUMBLE_0_NAME='Shelter_PC_Gold_v5.zip'
ARCHIVE_BASE_HUMBLE_0_MD5='32b84f87d7c24f57809670742adad3f7'
ARCHIVE_BASE_HUMBLE_0_SIZE='550000'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.0-humble1'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/shelter'

ARCHIVE_BASE_GOG_1_NAME='setup_shelter_1.0_(18565).exe'
ARCHIVE_BASE_GOG_1_MD5='7d1a2df224b43ce5a80008af13edde1d'
ARCHIVE_BASE_GOG_1_TYPE='innosetup'
ARCHIVE_BASE_GOG_1_SIZE='560000'
ARCHIVE_BASE_GOG_1_VERSION='1.0-gog18565'
ARCHIVE_BASE_GOG_1_URL='https://www.gog.com/game/shelter'

ARCHIVE_BASE_GOG_0_NAME='setup_shelter_2.0.0.6.exe'
ARCHIVE_BASE_GOG_0_MD5='06860af1df9a8120bce4e97d899b3edd'
ARCHIVE_BASE_GOG_0_TYPE='innosetup'
ARCHIVE_BASE_GOG_0_SIZE='570000'
ARCHIVE_BASE_GOG_0_VERSION='1.0-gog2.0.0.6'

CONTENT_PATH_DEFAULT_HUMBLE='.'
CONTENT_PATH_DEFAULT_GOG='app'

UNITY3D_NAME_HUMBLE='Shelter PC Gold v5'
UNITY3D_NAME_GOG='shelter'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Roaming/Might and Delight/Shelter'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
## Ensure smooth upgrade from previous game id
PKG_DATA_PROVIDES='
shelter-data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
## Ensure smooth upgrade from previous game id
PKG_BIN_PROVIDES='
shelter'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
