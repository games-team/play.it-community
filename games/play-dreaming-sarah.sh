#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Dreaming Sarah
# send your bug reports to contact@dotslashplay.it
###

script_version=20240329.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='dreaming-sarah'
GAME_NAME='Dreaming Sarah'

ARCHIVE_BASE_32BIT_0_NAME='DreamingSarah-linux32_1.3.zip'
ARCHIVE_BASE_32BIT_0_MD5='73682a545e979ad9a2b6123222ddb517'
ARCHIVE_BASE_32BIT_0_SIZE='200000'
ARCHIVE_BASE_32BIT_0_VERSION='1.3-humble1'
ARCHIVE_BASE_32BIT_0_URL='https://www.humblebundle.com/store/dreaming-sarah'

ARCHIVE_BASE_64BIT_0_NAME='DreamingSarah-linux64_1.3.zip'
ARCHIVE_BASE_64BIT_0_MD5='a68f3956eb09ea7b34caa20f6e89b60c'
ARCHIVE_BASE_64BIT_0_SIZE='200000'
ARCHIVE_BASE_64BIT_0_VERSION='1.3-humble1'
ARCHIVE_BASE_64BIT_0_URL='https://www.humblebundle.com/store/dreaming-sarah'

CONTENT_PATH_DEFAULT_32BIT='DreamingSarah-linux32'
CONTENT_PATH_DEFAULT_64BIT='DreamingSarah-linux64'
CONTENT_GAME_BIN_FILES='
nw
nacl_helper*'
CONTENT_LIBS_BIN_PATH_32BIT="${CONTENT_PATH_DEFAULT_32BIT}/lib"
CONTENT_LIBS_BIN_PATH_64BIT="${CONTENT_PATH_DEFAULT_64BIT}/lib"
CONTENT_LIBS_BIN_FILES='
libffmpeg.so
libnode.so
libnw.so'
CONTENT_GAME_DATA_FILES='
locales
*.bin
*.dat
*.nw
*.pak'

APP_MAIN_EXE='nw'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH_32BIT='32'
PKG_BIN_ARCH_64BIT='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libasound.so.2
libatk-1.0.so.0
libcairo.so.2
libc.so.6
libcups.so.2
libdbus-1.so.3
libdl.so.2
libexpat.so.1
libfontconfig.so.1
libfreetype.so.6
libgcc_s.so.1
libgconf-2.so.4
libgdk_pixbuf-2.0.so.0
libgdk-x11-2.0.so.0
libgio-2.0.so.0
libglib-2.0.so.0
libgmodule-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libnspr4.so
libnss3.so
libnssutil3.so
libpango-1.0.so.0
libpangocairo-1.0.so.0
libpthread.so.0
librt.so.1
libsmime3.so
libstdc++.so.6
libX11.so.6
libXcomposite.so.1
libXcursor.so.1
libXdamage.so.1
libXext.so.6
libXfixes.so.3
libXi.so.6
libXrandr.so.2
libXrender.so.1
libXss.so.1
libXtst.so.6'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
