#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Hob
# send your bug reports to contact@dotslashplay.it
###

script_version=20240810.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='hob-game'
GAME_NAME='Hob'

ARCHIVE_BASE_0_NAME='setup_hob_1.17.3.0_(16560).exe'
ARCHIVE_BASE_0_MD5='e99ca23c70ea3bfd4335fee7ca5a618e'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_hob_1.17.3.0_(16560)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='25455a965280f7ac6f94f4ff7d96b852'
ARCHIVE_BASE_0_SIZE='2800000'
ARCHIVE_BASE_0_VERSION='1.17.3.0-gog16560'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/hob'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
*.cfg
*.dll
hob.exe
hoblauncher.exe'
CONTENT_GAME_DATA_FILES='
audiobanks
media
music
paks'

WINE_DIRECT3D_RENDERER='dxvk'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/My Games/Runic Games/HOB'

APP_MAIN_EXE='hob.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
