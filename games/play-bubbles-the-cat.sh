#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Bubbles the Cat
# send your bug reports to contact@dotslashplay.it
###

script_version=20240808.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='bubbles-the-cat'
GAME_NAME='Bubbles the Cat'

ARCHIVE_BASE_0_NAME='BubblesTheCat._v1016_Standalone.zip'
ARCHIVE_BASE_0_MD5='75c5130b47cd1bb4cf3f9f9d8239ba31'
ARCHIVE_BASE_0_SIZE='160000'
ARCHIVE_BASE_0_VERSION='1016-itch'
ARCHIVE_BASE_0_URL='https://teamcatsandbears.itch.io/bubbles-the-cat'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
BubblesTheCat.exe'
CONTENT_GAME_DATA_FILES='
data.win'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Local/BubblesTheCat'

APP_MAIN_EXE='BubblesTheCat.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Use icotool for the .ico → .png icon conversion

SCRIPT_DEPS="${SCRIPT_DEPS:-} icotool"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

## Use icotool for the .ico → .png icon conversion
## TODO: Check why convert from ImageMagick can not be used
icon_extract_png_from_ico() {
	local icon destination
	icon="$1"
	destination="$2"

	local icon_file
	icon_file=$(icon_full_path "$icon")
	icotool --extract --output="$destination" "$icon_file"

	# Generate smaller variants of the icon from the original 1024×1024 one
	local png_original
	png_original=$(find "$destination" -name '*.png')
	convert "$png_original" -resize 512 "${png_original%.png}_512.png"
	convert "$png_original" -resize 256 "${png_original%.png}_256.png"
	convert "$png_original" -resize 128 "${png_original%.png}_128.png"
	convert "$png_original" -resize 64 "${png_original%.png}_64.png"
}

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
