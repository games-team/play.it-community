#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Garden In
# send your bug reports to contact@dotslashplay.it
###

script_version=20241105.2

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='garden-in'
GAME_NAME='Garden In!'

ARCHIVE_BASE_1_NAME='setup_garden_in_1.3.6.1_(64bit)_(73207).exe'
ARCHIVE_BASE_1_MD5='dc2cacf13ccf7e758463af3039c1d565'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='917331'
ARCHIVE_BASE_1_VERSION='1.3.6.1-gog73207'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/garden_in'

ARCHIVE_BASE_0_NAME='setup_garden_in_1.0.6.2_(64bit)_(63850).exe'
ARCHIVE_BASE_0_MD5='1ed753d195f563e2a530f07743ae7a4f'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='780000'
ARCHIVE_BASE_0_VERSION='1.0.6.2-gog63850'

UNREALENGINE4_NAME='gardenin'

CONTENT_PATH_DEFAULT='windowsnoeditor'

APP_MAIN_EXE="${UNREALENGINE4_NAME}/binaries/win64/${UNREALENGINE4_NAME}-win64-shipping.exe"
## The --name=101 wrestool option, default for UE4 games, should not be used here
APP_MAIN_ICON_WRESTOOL_OPTIONS='--type=14'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
