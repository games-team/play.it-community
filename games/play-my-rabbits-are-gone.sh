#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# My Rabbits Are Gone
# send your bug reports to contact@dotslashplay.it
###

script_version=20250303.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='my-rabbits-are-gone'
GAME_NAME='Oh Jeez, Oh No, My Rabbits Are Gone !!!'

ARCHIVE_BASE_1='oh-jeez-oh-no-my-rabbits-are-gone-linux.zip'
ARCHIVE_BASE_1_MD5='5fd3335ca1a517bd733d2692bda6dd35'
ARCHIVE_BASE_1_SIZE='210000'
ARCHIVE_BASE_1_VERSION='1.3.1.2a-itch1'
ARCHIVE_BASE_1_URL='https://studionevermore.itch.io/oh-jeez-oh-no-my-rabbits-are-gone'

ARCHIVE_BASE_0='MyRabbitsAreGone1.1.0.5ItchLinux.zip'
ARCHIVE_BASE_0_MD5='4666fb50124c7076b1a9e493b5ff4883'
ARCHIVE_BASE_0_SIZE='640000'
ARCHIVE_BASE_0_VERSION='1.1.5-itch1'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
MyRabbitsAreGone'
CONTENT_GAME_DATA_FILES='
assets'

USER_PERSISTENT_FILES='
assets/options.ini'

APP_MAIN_EXE='MyRabbitsAreGone'
APP_MAIN_ICON='assets/icon.png'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES_COMMON='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libGLU.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXext.so.6
libXrandr.so.2
libXxf86vm.so.1
libz.so.1'
PKG_BIN_DEPENDENCIES_LIBRARIES="
$PKG_BIN_DEPENDENCIES_LIBRARIES_COMMON
libcurl.so.4
libssl.so.1.1"
PKG_BIN_DEPENDENCIES_LIBRARIES_0="
$PKG_BIN_DEPENDENCIES_LIBRARIES_COMMON
libcurl-gnutls.so.4
libssl.so.1.0.0"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
