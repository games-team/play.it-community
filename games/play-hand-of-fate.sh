#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 mortalius
# SPDX-FileCopyrightText: © 2019 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Hand Of Fate
# send your bug reports to contact@dotslashplay.it
###

script_version=20231102.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='hand-of-fate'
GAME_NAME='Hand Of Fate'

ARCHIVE_BASE_2_NAME='hand_of_fate_1_3_20_25356.sh'
ARCHIVE_BASE_2_MD5='f0a1b47a6e4b5cb7a9f1494b55982b54'
ARCHIVE_BASE_2_SIZE='2800000'
ARCHIVE_BASE_2_VERSION='1.3.20-gog25356'
ARCHIVE_BASE_2_URL='https://www.gog.com/game/hand_of_fate'

ARCHIVE_BASE_1_NAME='hand_of_fate_en_1_3_19_21087.sh'
ARCHIVE_BASE_1_MD5='5895ccaf640afff877b6cab5fa348748'
ARCHIVE_BASE_1_SIZE='2800000'
ARCHIVE_BASE_1_VERSION='1.3.19-gog21087'

ARCHIVE_BASE_0_NAME='gog_hand_of_fate_2.12.0.16.sh'
ARCHIVE_BASE_0_MD5='54c61dce76b1281b4161d53d096d6ffe'
ARCHIVE_BASE_0_SIZE='2800000'
ARCHIVE_BASE_0_VERSION='1.3.17-gog2.12.0.16'

UNITY3D_NAME='Hand of Fate'
## TODO: Check if the Steam libraries could be dropped.
UNITY3D_PLUGINS='
libCSteamworks.so
libsteam_api.so
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='PKG_BIN32 PKG_BIN64 PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN32_ARCH='32'
PKG_BIN64_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXrandr.so.2'
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Write launchers

PKG='PKG_BIN32'
# shellcheck disable=SC2119
launchers_write
PKG='PKG_BIN64'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
