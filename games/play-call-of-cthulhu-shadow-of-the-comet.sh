#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 macaron
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Call of Cthulhu: Shadow of the Comet
# send your bug reports to contact@dotslashplay.it
###

script_version=20240505.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='call-of-cthulhu-shadow-of-the-comet'
GAME_NAME='Call of Cthulhu: Shadow of the Comet'

ARCHIVE_BASE_0_NAME='gog_call_of_cthulhu_shadow_of_the_comet_2.0.0.4.sh'
ARCHIVE_BASE_0_MD5='18c4f78b766e8e1d638e4ac32df0be60'
ARCHIVE_BASE_0_SIZE='150000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.0.0.4'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/call_of_cthulhu_shadow_of_the_comet'

CONTENT_PATH_DEFAULT='data/noarch/data'
CONTENT_GAME_MAIN_FILES='
infogram
cd'
CONTENT_DOC_MAIN_PATH='data/noarch/docs'
CONTENT_DOC_MAIN_FILES='
*.pdf
*.txt'

GAME_IMAGE='cd'
GAME_IMAGE_TYPE='cdrom'

APP_MAIN_EXE='shadow.exe'
APP_MAIN_DOSBOX_PRERUN='d:
config -set cpu cycles=fixed 13000'
APP_MAIN_ICON='../support/icon.png'
## The type can not be omitted, because the binary is actually on the CD-ROM image.
APP_MAIN_TYPE='dosbox'

APP_MUSEUM_ID="${GAME_ID}-museum"
APP_MUSEUM_NAME="$GAME_NAME - Lovecraft Museum"
APP_MUSEUM_EXE='museum.exe'
APP_MUSEUM_DOSBOX_PRERUN='d:
config -set cpu cycles=fixed 13000'
APP_MUSEUM_ICON='../support/icon.png'
## The type can not be omitted, because the binary is actually on the CD-ROM image.
APP_MUSEUM_TYPE='dosbox'

APP_SETUP_ID="${GAME_ID}-setup"
APP_SETUP_NAME="$GAME_NAME - Setup"
APP_SETUP_CAT='Settings'
APP_SETUP_EXE='install.exe'
APP_SETUP_DOSBOX_PRERUN='d:'
APP_SETUP_ICON="$APP_MAIN_ICON"
## The type can not be omitted, because the binary is actually on the CD-ROM image.
APP_SETUP_TYPE='dosbox'

USER_PERSISTENT_FILES='
infogram/shadow.cd/*.cfg
infogram/shadow.cd/*.opt
infogram/shadow.cd/*.sav'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

## Work around the binary presence check,
## it is actually included in the CD-ROM image.
launcher_target_presence_check() { return 0; }
launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
