#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Puddle
# send your bug reports to contact@dotslashplay.it
###

script_version=20240501.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='puddle'
GAME_NAME='Puddle'

ARCHIVE_BASE_0_NAME='setup_puddle_2.1.0.6.exe'
ARCHIVE_BASE_0_MD5='e1773a7bd360125ad67ce864963be586'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_VERSION='1.0-gog2.1.0.6'
ARCHIVE_BASE_0_SIZE='700000'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/puddle'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
*.exe
*.dll'
CONTENT_GAME_DATA_FILES='
data
de
es
fr
it'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/Puddle'
WINE_WINEPREFIX_TWEAKS='mono'

APP_MAIN_EXE='puddle.exe'
APP_MAIN_ICON='puddle.exe'

APP_SETTINGS_ID="${GAME_ID}-settings"
APP_SETTINGS_NAME="$GAME_NAME - Settings"
APP_SETTINGS_CAT='Settings'
APP_SETTINGS_EXE='launcher.exe'
APP_SETTINGS_ICON='puddle.exe'
## Type must be set explicitly,
## or it will be wrongly identified as a Mono application.
APP_SETTINGS_TYPE='wine'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Automatically spawn game settings window on first launch

## Using desktop_field_exec here ensures that we get a path already escaped if required.
settings_cmd=$(desktop_field_exec 'APP_SETTINGS')
APP_MAIN_PRERUN="$(application_prerun 'APP_MAIN')"'
# Automatically spawn game settings window on first launch
settings_file="${WINEPREFIX}/drive_c/users/${USER}/Documents/Puddle/Settings.dat"
if [ ! -e "$settings_file" ]; then
	'"${settings_cmd}"'
	exit 0
fi'

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
