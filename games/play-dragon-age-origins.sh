#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Dragon Age: Origins
# send your bug reports to contact@dotslashplay.it
###

script_version=20241209.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='dragon-age-origins'
GAME_NAME='Dragon Age: Origins'

ARCHIVE_BASE_2_NAME='setup_dragon_age_origins_-_ultimate_edition_1.05_gog_0.8_(78205).exe'
ARCHIVE_BASE_2_MD5='18e2e1f3fba4184ca15d6170d0a2ce51'
ARCHIVE_BASE_2_TYPE='innosetup'
ARCHIVE_BASE_2_PART1_NAME='setup_dragon_age_origins_-_ultimate_edition_1.05_gog_0.8_(78205)-1.bin'
ARCHIVE_BASE_2_PART1_MD5='0735b7a919fe9b1a7e7d31cfea156ee5'
ARCHIVE_BASE_2_PART2_NAME='setup_dragon_age_origins_-_ultimate_edition_1.05_gog_0.8_(78205)-2.bin'
ARCHIVE_BASE_2_PART2_MD5='c7f59542be194793d3f1664b2b079d64'
ARCHIVE_BASE_2_PART3_NAME='setup_dragon_age_origins_-_ultimate_edition_1.05_gog_0.8_(78205)-3.bin'
ARCHIVE_BASE_2_PART3_MD5='87ef8829f56c5b985b7f3eb1893249de'
ARCHIVE_BASE_2_PART4_NAME='setup_dragon_age_origins_-_ultimate_edition_1.05_gog_0.8_(78205)-4.bin'
ARCHIVE_BASE_2_PART4_MD5='04f49cccc87d268fef2a7a4cafce27e8'
ARCHIVE_BASE_2_PART5_NAME='setup_dragon_age_origins_-_ultimate_edition_1.05_gog_0.8_(78205)-5.bin'
ARCHIVE_BASE_2_PART5_MD5='e44d862bd7221d065be30cd80de4d568'
ARCHIVE_BASE_2_PART6_NAME='setup_dragon_age_origins_-_ultimate_edition_1.05_gog_0.8_(78205)-6.bin'
ARCHIVE_BASE_2_PART6_MD5='e97793771570a38174e202704297fe37'
ARCHIVE_BASE_2_SIZE='38342773'
ARCHIVE_BASE_2_VERSION='1.05-gog78205'
ARCHIVE_BASE_2_URL='https://www.gog.com/game/dragon_age_origins'

ARCHIVE_BASE_1_NAME='setup_dragon_age_origins_-_ultimate_edition_1.05_gog_0.5_(76885).exe'
ARCHIVE_BASE_1_MD5='fa14b944ca5ce04e462afe61e932f1b0'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_dragon_age_origins_-_ultimate_edition_1.05_gog_0.5_(76885)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='910ff0ff7db44dd44c9b7fbc01439226'
ARCHIVE_BASE_1_PART2_NAME='setup_dragon_age_origins_-_ultimate_edition_1.05_gog_0.5_(76885)-2.bin'
ARCHIVE_BASE_1_PART2_MD5='317484fbbe084fdfac86af933a3827d3'
ARCHIVE_BASE_1_PART3_NAME='setup_dragon_age_origins_-_ultimate_edition_1.05_gog_0.5_(76885)-3.bin'
ARCHIVE_BASE_1_PART3_MD5='c7a236056afe1e24070e747760cab8d9'
ARCHIVE_BASE_1_PART4_NAME='setup_dragon_age_origins_-_ultimate_edition_1.05_gog_0.5_(76885)-4.bin'
ARCHIVE_BASE_1_PART4_MD5='0b08303e31e29708cc679d92b5b9c607'
ARCHIVE_BASE_1_PART5_NAME='setup_dragon_age_origins_-_ultimate_edition_1.05_gog_0.5_(76885)-5.bin'
ARCHIVE_BASE_1_PART5_MD5='dead382df73a84c02ee54a5b34050fcc'
ARCHIVE_BASE_1_PART6_NAME='setup_dragon_age_origins_-_ultimate_edition_1.05_gog_0.5_(76885)-6.bin'
ARCHIVE_BASE_1_PART6_MD5='9c94a20453a52d5284c89aa4507949e0'
ARCHIVE_BASE_1_SIZE='38343327'
ARCHIVE_BASE_1_VERSION='1.05-gog76885'

ARCHIVE_BASE_0_NAME='setup_dragon_age_origins_ultimate_2.1.0.4.exe'
ARCHIVE_BASE_0_MD5='2bfdbc94523ef4c21476f64ef8029479'
ARCHIVE_BASE_0_EXTRACTOR='innoextract'
ARCHIVE_BASE_0_EXTRACTOR_OPTIONS='--lowercase --gog'
ARCHIVE_BASE_0_PART1_NAME='setup_dragon_age_origins_ultimate_2.1.0.4-1.bin'
ARCHIVE_BASE_0_PART1_MD5='b6e68b1b3b11fdddea809a5f11368036'
ARCHIVE_BASE_0_PART1_TYPE='rar'
ARCHIVE_BASE_0_PART2_NAME='setup_dragon_age_origins_ultimate_2.1.0.4-2.bin'
ARCHIVE_BASE_0_PART2_MD5='71d813d6827941a90422a40088d64b78'
ARCHIVE_BASE_0_PART3_NAME='setup_dragon_age_origins_ultimate_2.1.0.4-3.bin'
ARCHIVE_BASE_0_PART3_MD5='2ff9cc2bb41435429ee6277106a6a568'
ARCHIVE_BASE_0_PART4_NAME='setup_dragon_age_origins_ultimate_2.1.0.4-4.bin'
ARCHIVE_BASE_0_PART4_MD5='a25c58b43a2e468fcf72446f57542115'
ARCHIVE_BASE_0_PART5_NAME='setup_dragon_age_origins_ultimate_2.1.0.4-5.bin'
ARCHIVE_BASE_0_PART5_MD5='4ce5f6dceb01c9a1fc85c759c436b7b2'
ARCHIVE_BASE_0_PART6_NAME='setup_dragon_age_origins_ultimate_2.1.0.4-6.bin'
ARCHIVE_BASE_0_PART6_MD5='e2d13b236af30f210e0eb65aec5d137e'
ARCHIVE_BASE_0_SIZE='39000000'
ARCHIVE_BASE_0_VERSION='1.05-gog2.1.0.4'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='game'
CONTENT_GAME_BIN_FILES='
bin_ship
daoriginslauncher.exe'
## TODO: The add-ons could be moved to a dedicated package
CONTENT_GAME_DATA_FILES='
addins
data
modules
offers
packages'
CONTENT_GAME_ENVIRONMENT_FILES='
addins/*/*/env
packages/*/env'
CONTENT_GAME_MOVIES_FILES='
addins/*/core/data/movies
modules/*/data/movies
packages/*/data/movies'
CONTENT_GAME_L10N_DE_FILES='
*/de-de
*/*_de-de*'
CONTENT_GAME_L10N_EN_FILES='
*/en-us
*/*_en-us*'
CONTENT_GAME_L10N_FR_FILES='
*/fr-fr
*/*_fr-fr*'
CONTENT_GAME_L10N_RU_FILES='
*/ru-ru
*/*_ru-ru*'
CONTENT_GAME_L10N_PL_FILES='
*/pl-pl
*/*_pl-pl*'
CONTENT_GAME_L10N_CS_FILES='
*/cs-cz
*/*_cs-cz*'
CONTENT_GAME_L10N_ES_FILES='
*/es-es
*/*_es-es*'
CONTENT_GAME_L10N_HU_FILES='
*/hu-hu
*/*_hu-hu*'
CONTENT_GAME_L10N_IT_FILES='
*/it-it
*/*_it-it*'
CONTENT_DOC_DATA_FILES='
manual.pdf'
## TODO: This should be split into the localization packages
CONTENT_DOC0_DATA_RELATIVE_PATH='docs'
CONTENT_DOC0_DATA_FILES='
*'
CONTENT_SETTINGS_RELATIVE_PATH='__support/userdocs'
CONTENT_SETTINGS_RELATIVE_PATH_0='../userdocs/bioware/dragon age/settings'
CONTENT_SETTINGS_FILES='
addins.xml'
CONTENT_PHYSX_RELATIVE_PATH='redist'
CONTENT_PHYSX_FILES='
physx_9.09.0408_systemsoftware.exe'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/BioWare/Dragon Age'

APP_MAIN_EXE='bin_ship/daorigins.exe'
## Install shipped build of PhysX
APP_MAIN_PRERUN='
# Install shipped build of PhysX
if [ ! -e physx/installed ]; then
	$(wine_command) physx/physx_9.09.0408_systemsoftware.exe
	touch physx/installed
fi
'

APP_CONFIG_ID="${GAME_ID}-config"
APP_CONFIG_NAME="$GAME_NAME - Configuration"
APP_CONFIG_CAT='Settings'
APP_CONFIG_EXE='bin_ship/daoriginsconfig.exe'

PACKAGES_LIST='
PKG_BIN
PKG_L10N_DE
PKG_L10N_EN
PKG_L10N_FR
PKG_L10N_RU
PKG_L10N_PL
PKG_L10N_CS
PKG_L10N_ES
PKG_L10N_HU
PKG_L10N_IT
PKG_ENVIRONMENT
PKG_MOVIES
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_ENVIRONMENT
PKG_MOVIES
PKG_DATA
PKG_L10N'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_ENVIRONMENT_ID="${GAME_ID}-environment"
PKG_ENVIRONMENT_DESCRIPTION='environment'

PKG_MOVIES_ID="${GAME_ID}-movies"
PKG_MOVIES_DESCRIPTION='movies'

PKG_L10N_ID="${GAME_ID}-l10n"
PKG_L10N_DE_ID="${PKG_L10N_ID}-de"
PKG_L10N_EN_ID="${PKG_L10N_ID}-en"
PKG_L10N_FR_ID="${PKG_L10N_ID}-fr"
PKG_L10N_RU_ID="${PKG_L10N_ID}-ru"
PKG_L10N_PL_ID="${PKG_L10N_ID}-pl"
PKG_L10N_CS_ID="${PKG_L10N_ID}-cs"
PKG_L10N_ES_ID="${PKG_L10N_ID}-es"
PKG_L10N_HU_ID="${PKG_L10N_ID}-hu"
PKG_L10N_IT_ID="${PKG_L10N_ID}-it"
PKG_L10N_PROVIDES="
$PKG_L10N_ID"
PKG_L10N_DE_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_EN_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_FR_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_RU_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_PL_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_CS_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_ES_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_HU_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_IT_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_DE_DESCRIPTION='German localization'
PKG_L10N_EN_DESCRIPTION='English localization'
PKG_L10N_FR_DESCRIPTION='French localization'
PKG_L10N_RU_DESCRIPTION='Russian localization'
PKG_L10N_PL_DESCRIPTION='Polish localization'
PKG_L10N_CS_DESCRIPTION='Czech localization'
PKG_L10N_ES_DESCRIPTION='Spanish localization'
PKG_L10N_HU_DESCRIPTION='Hungarian localization'
PKG_L10N_IT_DESCRIPTION='Italian localization'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# innoextract --lowercase has no effect on the content of the .bin RAR archives
	if [ "$(archive_type "$(current_archive)_PART1")" = 'rar' ]; then
		tolower .
	fi
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion 'SETTINGS' 'PKG_DATA' "$(path_game_data)/settings"
content_inclusion 'PHYSX' 'PKG_BIN' "$(path_game_data)/physx"
content_inclusion_default

# Write launchers

## Include the shipped settings
## This is required to unlock the included expansions
## TODO: This override could be mught lighter if a function
##       wineprefix_init_actions_custom was provided by the library
##       cf. https://forge.dotslashplay.it/play.it/play.it/-/issues/548
wine_wineprefix_init_actions() {
	local game_id
	game_id=$(game_id)
	cat <<- EOF
	    ## Link the game prefix into the WINE prefix
	    ln --symbolic \
	        "\$PATH_PREFIX" \
	        "\${WINEPREFIX}/drive_c/${game_id}"

	EOF
	cat <<- 'EOF'
	    ## Remove most links pointing outside of the WINE prefix
	    rm "$WINEPREFIX/dosdevices/z:"
	    find "$WINEPREFIX/drive_c/users/$(whoami)" -type l | while read -r directory; do
	        rm "$directory"
	        mkdir "$directory"
	    done
	    unset directory

	    ## Set symbolic links to the legacy paths
	    wineprefix_legacy_link 'AppData/Roaming' 'Application Data'
	    wineprefix_legacy_link 'AppData/Local' 'Local Settings/Application Data'
	    wineprefix_legacy_link 'Documents' 'My Documents'

	    ## Include the shipped settings
	    settings_path="${PATH_PERSISTENT}/wineprefix/users/${USER}/Documents/BioWare/Dragon Age/Settings"
	    if [ ! -e "$settings_path" ]; then
	        mkdir --parents "$(dirname "$settings_path")"
	        cp --recursive "${PATH_GAME_DATA}/settings" "$settings_path"
	    fi
	EOF
}

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
case "$(messages_language)" in
	('fr')
		lang_string='version %s :'
		lang_de='allemande'
		lang_en='anglaise'
		lang_fr='française'
		lang_ru='russe'
		lang_pl='polonaise'
		lang_cs='tchèque'
		lang_es='espagnole'
		lang_hu='hongroise'
		lang_it='italienne'
	;;
	('en'|*)
		lang_string='%s version:'
		lang_de='German'
		lang_en='English'
		lang_fr='French'
		lang_ru='Russian'
		lang_pl='Polish'
		lang_cs='Czech'
		lang_es='Spanish'
		lang_hu='Hungarian'
		lang_it='Italian'
	;;
esac
printf '\n'
printf "$lang_string" "$lang_de"
print_instructions 'PKG_BIN' 'PKG_L10N_DE' 'PKG_DATA' 'PKG_ENVIRONMENT' 'PKG_MOVIES'
printf "$lang_string" "$lang_en"
print_instructions 'PKG_BIN' 'PKG_L10N_EN' 'PKG_DATA' 'PKG_ENVIRONMENT' 'PKG_MOVIES'
printf "$lang_string" "$lang_fr"
print_instructions 'PKG_BIN' 'PKG_L10N_FR' 'PKG_DATA' 'PKG_ENVIRONMENT' 'PKG_MOVIES'
printf "$lang_string" "$lang_ru"
print_instructions 'PKG_BIN' 'PKG_L10N_RU' 'PKG_DATA' 'PKG_ENVIRONMENT' 'PKG_MOVIES'
printf "$lang_string" "$lang_pl"
print_instructions 'PKG_BIN' 'PKG_L10N_PL' 'PKG_DATA' 'PKG_ENVIRONMENT' 'PKG_MOVIES'
printf "$lang_string" "$lang_cs"
print_instructions 'PKG_BIN' 'PKG_L10N_CS' 'PKG_DATA' 'PKG_ENVIRONMENT' 'PKG_MOVIES'
printf "$lang_string" "$lang_es"
print_instructions 'PKG_BIN' 'PKG_L10N_ES' 'PKG_DATA' 'PKG_ENVIRONMENT' 'PKG_MOVIES'
printf "$lang_string" "$lang_hu"
print_instructions 'PKG_BIN' 'PKG_L10N_HU' 'PKG_DATA' 'PKG_ENVIRONMENT' 'PKG_MOVIES'
printf "$lang_string" "$lang_it"
print_instructions 'PKG_BIN' 'PKG_L10N_IT' 'PKG_DATA' 'PKG_ENVIRONMENT' 'PKG_MOVIES'

# Clean up

working_directory_cleanup

exit 0
