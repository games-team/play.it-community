#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Darwinia
# send your bug reports to contact@dotslashplay.it
###

script_version=20240602.2

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='darwinia'
GAME_NAME='Darwinia'

ARCHIVE_BASE_0_NAME='gog_darwinia_2.0.0.5.sh'
ARCHIVE_BASE_0_MD5='ef55064ab82a64324e295f2ea96239d6'
ARCHIVE_BASE_0_SIZE='45000'
ARCHIVE_BASE_0_VERSION='1.4.2-gog2.0.0.5'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/darwinia'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN64_FILES='
darwinia.bin.x86_64'
CONTENT_GAME_BIN32_FILES='
darwinia.bin.x86'
CONTENT_GAME_DATA_FILES='
darwinian.png
*.dat'
CONTENT_DOC_DATA_FILES='
*.txt'

FAKE_HOME_PERSISTENT_DIRECTORIES='
.darwinia'

APP_MAIN_EXE_BIN64='darwinia.bin.x86_64'
APP_MAIN_EXE_BIN32='darwinia.bin.x86'
APP_MAIN_ICON='darwinian.png'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libgcc_s.so.1
libGL.so.1
libGLU.so.1
libm.so.6
libogg.so.0
libpthread.so.0
libSDL-1.2.so.0
libstdc++.so.6
libvorbisfile.so.3
libvorbis.so.0'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Convert the shipped icon into a standard size

SCRIPT_DEPS="${SCRIPT_DEPS:-} convert"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Convert the shipped icon into a standard size.
	icon_source=$(icon_path 'APP_MAIN_ICON')
	convert \
		-background none -gravity center "$icon_source" \
		-resize 128x128 -extent 128x128 "${icon_source}_resized.png"
	mv "${icon_source}_resized.png" "$icon_source"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
