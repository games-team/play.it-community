#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Helium Rain
# send your bug reports to contact@dotslashplay.it
###

script_version=20231213.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='helium-rain'
GAME_NAME='Helium Rain'

ARCHIVE_BASE_GOG_0_NAME='helium_rain_1_3_6_27662.sh'
ARCHIVE_BASE_GOG_0_MD5='6e430338f7292ebe9c864603034fb9f1'
ARCHIVE_BASE_GOG_0_SIZE='3930976'
ARCHIVE_BASE_GOG_0_VERSION='1.3.6-gog27662'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/helium_rain'

ARCHIVE_BASE_ITCH_0_NAME='helium-rain-linux.zip'
ARCHIVE_BASE_ITCH_0_MD5='8d43439154be0d3de242b39f63decd7c'
ARCHIVE_BASE_ITCH_0_SIZE='3930288'
ARCHIVE_BASE_ITCH_0_VERSION='1.3.6-itch.2019.02.09'
ARCHIVE_BASE_ITCH_0_URL='https://deimos-games.itch.io/helium-rain'

UNREALENGINE4_NAME='HeliumRain'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_ITCH='.'

APP_MAIN_EXE="${UNREALENGINE4_NAME}/Binaries/Linux/${UNREALENGINE4_NAME}-Linux-Shipping"
APP_MAIN_ICON_GOG='../support/icon.png'
APP_MAIN_ICONS_LIST_ITCH='APP_MAIN_ICON_ITCH_16 APP_MAIN_ICON_ITCH_24 APP_MAIN_ICON_ITCH_32 APP_MAIN_ICON_ITCH_64 APP_MAIN_ICON_ITCH_96 APP_MAIN_ICON_ITCH_128 APP_MAIN_ICON_ITCH_256 APP_MAIN_ICON_ITCH_512'
APP_MAIN_ICON_ITCH_16='Icons/16x16.png'
APP_MAIN_ICON_ITCH_24='Icons/24x24.png'
APP_MAIN_ICON_ITCH_32='Icons/32x32.png'
APP_MAIN_ICON_ITCH_64='Icons/64x64.png'
APP_MAIN_ICON_ITCH_96='Icons/96x96.png'
APP_MAIN_ICON_ITCH_128='Icons/128x128.png'
APP_MAIN_ICON_ITCH_256='Icons/256x256.png'
APP_MAIN_ICON_ITCH_512='Icons/512x512.png'
## The following hacks can not be avoided by forcing the use of system-provided SDL,
## because the game crashes on launch when it is used instead of the shipped SDL build.
### The game crashes on launch when the Wayland backend of SDL is used.
APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}"'
# The game crashes on launch when the Wayland backend of SDL is used
if [ "${SDL_VIDEODRIVER:-}" = "wayland" ]; then
	unset SDL_VIDEODRIVER
fi
'
### It seems that the shipped SDL build does not support the alsa backend.
### When it is used, no error is triggered but no sound is played either.
APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}"'
# The game does not output any sound when the ALSA backend of SDL is used
if [ "${SDL_AUDIODRIVER:-}" = "alsa" ]; then
	unset SDL_AUDIODRIVER
fi
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
