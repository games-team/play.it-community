#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Race The Sun expansions:
# - Sunrise
# send your bug reports to contact@dotslashplay.it
###

script_version=20241109.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='race-the-sun'
GAME_NAME='Race The Sun'

EXPANSION_ID_SUNRISE='sunrise'
EXPANSION_NAME_SUNRISE='Sunrise'

ARCHIVE_BASE_SUNRISE_0_NAME='gog_race_the_sun_sunrise_dlc_2.0.0.1.sh'
ARCHIVE_BASE_SUNRISE_0_MD5='5af9dee7941f63c310d83ac771d26884'
ARCHIVE_BASE_SUNRISE_0_SIZE='1100'
ARCHIVE_BASE_SUNRISE_0_VERSION='1.0-gog2.0.0.1'
ARCHIVE_BASE_SUNRISE_0_URL='https://www.gog.com/game/sunrise'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
SunriseDLC'

PKG_PARENT_ID="$GAME_ID"

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_PARENT'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
