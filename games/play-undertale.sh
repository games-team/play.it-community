#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Undertale
# send your bug reports to contact@dotslashplay.it
###

script_version=20250303.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='undertale'
GAME_NAME='Undertale'

ARCHIVE_BASE_2='undertale_en_1_08_18328.sh'
ARCHIVE_BASE_2_MD5='b134d85dd8bf723a74498336894ca723'
ARCHIVE_BASE_2_SIZE='160000'
ARCHIVE_BASE_2_VERSION='1.08-gog18328'
ARCHIVE_BASE_2_URL='https://www.gog.com/game/undertale'

ARCHIVE_BASE_1='undertale_en_1_06_15928.sh'
ARCHIVE_BASE_1_MD5='54f9275d3def027e9f3f65a61094a662'
ARCHIVE_BASE_1_SIZE='160000'
ARCHIVE_BASE_1_VERSION='1.06-gog15928'

ARCHIVE_BASE_0='gog_undertale_2.0.0.1.sh'
ARCHIVE_BASE_0_MD5='e740df4e15974ad8c21f45ebe8426fb0'
ARCHIVE_BASE_0_SIZE='160000'
ARCHIVE_BASE_0_VERSION='1.001-gog2.0.0.1'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN_FILES='
runner'
CONTENT_GAME_DATA_FILES='
assets'

APP_MAIN_EXE='runner'
APP_MAIN_ICON='assets/icon.png'
## Work around Mesa-related startup crash
APP_MAIN_PRERUN='# Work around Mesa-related startup crash
# cf. https://gitlab.freedesktop.org/mesa/mesa/issues/1310
export radeonsi_sync_compile=true
'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libGLU.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
librt.so.1
libssl.so.1.0.0
libstdc++.so.6
libX11.so.6
libXext.so.6
libXrandr.so.2
libXxf86vm.so.1
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Enforce the binary name
	if [ -e 'UNDERTALE' ]; then
		mv 'UNDERTALE' "$(application_exe 'APP_MAIN')"
	fi
)

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
