#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Epistory - Typing Chronicles
# send your bug reports to contact@dotslashplay.it
###

script_version=20241020.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='epistory-typing-chronicles'
GAME_NAME='Epistory - Typing Chronicles'

ARCHIVE_BASE_1_NAME='epistory_typing_chronicles_en_1_4_0_21518.sh'
ARCHIVE_BASE_1_MD5='bf54d1235b4b02be0a90eeccce64e9a5'
ARCHIVE_BASE_1_SIZE='1300000'
ARCHIVE_BASE_1_VERSION='1.4.0-gog21518'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/epistory_typing_chronicles'

ARCHIVE_BASE_0_NAME='gog_epistory_typing_chronicles_2.2.0.3.sh'
ARCHIVE_BASE_0_MD5='8db1f835a9189099e57c174ba2353f53'
ARCHIVE_BASE_0_SIZE='1300000'
ARCHIVE_BASE_0_VERSION='1.3.5-gog2.2.0.3'

UNITY3D_NAME='Epistory'
UNITY3D_PLUGINS='
libfmod.so
libfmodstudio.so
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXrandr.so.2'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

## Link ScreenSelector.so in the game data path as the game engine fails to find it otherwise
file_name='ScreenSelector.so'
file_source="$(path_libraries)/${file_name}"
file_destination_32="$(package_path 'PKG_BIN32')$(path_game_data)/$(unity3d_name)_Data/Plugins/x86/${file_name}"
file_destination_64="$(package_path 'PKG_BIN64')$(path_game_data)/$(unity3d_name)_Data/Plugins/x86_64/${file_name}"
mkdir --parents \
	"$(dirname "$file_destination_32")" \
	"$(dirname "$file_destination_64")"
ln --symbolic "$file_source" "$file_destination_32"
ln --symbolic "$file_source" "$file_destination_64"

# Write launchers

## Do not override the system locale, as it prevents the game from starting.
launcher_unity3d_force_locale() { return 0 ; }

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

## Do not include a package for the 64-bit build, as running it seems to trigger a severe memory leak
PACKAGES_LIST='
PKG_BIN32
PKG_DATA'

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
