#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Beacon Pines
# send your bug reports to contact@dotslashplay.it
###

script_version=20240315.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='beacon-pines'
GAME_NAME='Beacon Pines'

ARCHIVE_BASE_0_NAME='setup_beacon_pines_1.1.1_(63135).exe'
ARCHIVE_BASE_0_MD5='a19d066a3d9c7df55a1db27e47e7852f'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='1943267'
ARCHIVE_BASE_0_VERSION='1.1.1-gog63135'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/beacon_pines'

UNITY3D_NAME='beacon pines'

CONTENT_PATH_DEFAULT='.'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Hiding Spot/Beacon Pines'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
