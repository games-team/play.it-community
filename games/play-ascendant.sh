#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Ascendant
# send your bug reports to contact@dotslashplay.it
###

script_version=20231015.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='ascendant'
GAME_NAME='Ascendant'

ARCHIVE_BASE_0_NAME='gog_ascendant_2.2.0.7.sh'
ARCHIVE_BASE_0_MD5='8cdcd59a2f8363b7237e9cbe2675adda'
ARCHIVE_BASE_0_SIZE='2400000'
ARCHIVE_BASE_0_VERSION='1.2.2-gog2.2.0.7'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/ascendant'

UNITY3D_NAME='Ascendant'
UNITY3D_PLUGINS='
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='PKG_BIN32 PKG_BIN64 PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN32_ARCH='32'
PKG_BIN64_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libatk-1.0.so.0
libcairo.so.2
libc.so.6
libdl.so.2
libfontconfig.so.1
libfreetype.so.6
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libgdk-x11-2.0.so.0
libgio-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libGLU.so.1
libgmodule-2.0.so.0
libgobject-2.0.so.0
libgthread-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpango-1.0.so.0
libpangocairo-1.0.so.0
libpangoft2-1.0.so.0
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXext.so.6
libXrandr.so.2'
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "$PLAYIT_WORKDIR/gamedata/$(content_path_default)"

	# Work around the massive files duplication between the 32-bit and 64-bit builds
	## Merge assets directories for the 32-bit and 64-bit builds
	UNITY3D_NAME_64BIT='Ascendant_64'
	cp --force --recursive --link "${UNITY3D_NAME_64BIT}_Data"/* "$(unity3d_name)_Data"
	rm --force --recursive "${UNITY3D_NAME_64BIT}_Data"
	## Rename the 64-bit binary
	mv "${UNITY3D_NAME_64BIT}.x86_64" "$(unity3d_name).x86_64"
)

# Include game data

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

PKG='PKG_BIN32'
# shellcheck disable=SC2119
launchers_write
PKG='PKG_BIN64'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
