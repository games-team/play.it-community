#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Headspun
# send your bug reports to contact@dotslashplay.it
###

script_version=20240611.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='headspun'
GAME_NAME='Headspun'

ARCHIVE_BASE_0_NAME='headspun-dazed-edition-win.zip'
ARCHIVE_BASE_0_MD5='0e0621486930ba3a0dda64727423c603'
ARCHIVE_BASE_0_SIZE='4600000'
ARCHIVE_BASE_0_VERSION='1.0-itch'
ARCHIVE_BASE_0_URL='https://svperstring.itch.io/headspun-dazed-edition'

UNITY3D_NAME='Headspun'

CONTENT_PATH_DEFAULT='Headspun/Headspun'
CONTENT_GAME0_DATA_FILES='
Subtitles
Videos'

USER_PERSISTENT_FILES="
${UNITY3D_NAME}_Data/OptionsData.txt
${UNITY3D_NAME}_Data/PlayerData.txt"

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
video/quicktime, variant=(string)iso'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
