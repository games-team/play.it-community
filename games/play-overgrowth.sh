#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 BetaRays
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Overgrowth
# send your bug reports to contact@dotslashplay.it
###

script_version=20230930.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='overgrowth'
GAME_NAME='Overgrowth'

ARCHIVE_BASE_0='overgrowth-1.4.0_build-5584-linux64.zip'
ARCHIVE_BASE_0_MD5='748f6888386d842193218c53396ac844'
ARCHIVE_BASE_0_SIZE='22000000'
ARCHIVE_BASE_0_VERSION='1.4.0.5584-humble'
ARCHIVE_BASE_0_URL='https://www.humblebundle.com/store/overgrowth'

CONTENT_PATH_DEFAULT='Overgrowth'
CONTENT_LIBS_BIN_PATH="${CONTENT_PATH_DEFAULT}/lib64"
## The game binary is linked against libsteam_api.so.
CONTENT_LIBS_BIN_FILES='
libsteam_api.so'
CONTENT_GAME_BIN_FILES='
Overgrowth.bin.x86_64'
CONTENT_GAME_TEXTURES_TERRAIN_FILES='
Data/Textures/Terrain'
CONTENT_GAME_TEXTURES_FILES='
Data/Textures'
CONTENT_GAME_DATA_FILES='
Data'

USER_PERSISTENT_DIRECTORIES='
Data/Levels
Data/Mods
Data/Scripts'

APP_MAIN_EXE='Overgrowth.bin.x86_64'
APP_MAIN_ICON='Data/Textures/ui/ogicon.png'

PACKAGES_LIST='PKG_BIN PKG_TEXTURES_TERRAIN PKG_TEXTURES PKG_DATA'

PKG_TEXTURES_TERRAIN_ID="${GAME_ID}-textures-terrain"
PKG_TEXTURES_TERRAIN_DESCRIPTION='textures - terrain'

PKG_TEXTURES_ID="${GAME_ID}-textures"
PKG_TEXTURES_DESCRIPTION='textures'
PKG_TEXTURES_DEPS="$PKG_TEXTURES_TERRAIN_ID"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPS="$PKG_TEXTURES_ID"

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libfreeimage.so.3
libgcc_s.so.1
libglib-2.0.so.0
libGL.so.1
libGLU.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libopenal.so.1
libpthread.so.0
libSDL2-2.0.so.0
libSDL2_net-2.0.so.0
libstdc++.so.6
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Write launchers

PKG='PKG_BIN32'
# shellcheck disable=SC2119
launchers_write
PKG='PKG_BIN64'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
