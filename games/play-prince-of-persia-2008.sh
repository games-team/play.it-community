#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Prince of Persia (2008)
# send your bug reports to contact@dotslashplay.it
###

script_version=20240530.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='prince-of-persia-2008'
GAME_NAME='Prince of Persia'

ARCHIVE_BASE_0_NAME='setup_prince_of_persia_1.0_v2_(28572).exe'
ARCHIVE_BASE_0_MD5='65143fe6dfb653d5fa95bb79c8022630'
## Conversion of file paths to lowercase should be skipped, to prevent a game crash on launch.
ARCHIVE_BASE_0_EXTRACTOR='innoextract'
ARCHIVE_BASE_0_EXTRACTOR_OPTIONS=' '
ARCHIVE_BASE_0_PART1_NAME='setup_prince_of_persia_1.0_v2_(28572)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='caab8ceb59b30c75533ebd29ccd7a199'
ARCHIVE_BASE_0_PART2_NAME='setup_prince_of_persia_1.0_v2_(28572)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='ddc7e15fd6daf47e3085e2c66a4ca6cd'
ARCHIVE_BASE_0_SIZE='8237644'
ARCHIVE_BASE_0_VERSION='1.0-gog28572'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/prince_of_persia'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
Launcher/regvs.cmd
Launcher/detectionapi_rd.dll
Launcher/directx9tests_rd.dll
Launcher/local.dll
Launcher/Launcher.exe
Launcher/gameinterpreters_rd.intr
Launcher/directxtests_rd.tst
Launcher/systemtests_rd.tst
Support
binkw32.dll
eax.dll
Prince of Persia.exe
PrinceOfPersia_Launcher.exe'
CONTENT_GAME_L10N_EN_FILES='
Launcher/localization/EN/interpreter_local.ini
Resources/us/GameUpdate.us
DataPC_StreamedSoundsEng.forge'
CONTENT_GAME_L10N_FR_FILES='
Launcher/localization/FR/interpreter_local.ini
Resources/fr/GameUpdate.fr
Videos/fre
DataPC_StreamedSoundsFre.forge'
CONTENT_GAME_DATA_FILES='
Videos/ArtGallery.bik
Videos/ArtGallery_CityofLight.bik
Videos/ArtGallery_DesertTemple.bik
Videos/ArtGallery_RoyalePalace.bik
Videos/ArtGallery_RuinedCitadel.bik
Videos/ArtGallery_Vale.bik
Videos/AttractMode.bik
Videos/ComboList.bik
Videos/Controls.bik
Videos/Credits.bik
Videos/DE1_001_CINV_GameIntro.bik
Videos/Display.bik
Videos/Extras.bik
Videos/LoadGame.bik
Videos/MainMenuE3.bik
Videos/Options.bik
Videos/SkinsManager.bik
Videos/Sound.bik
Videos/StartScreenE3.bik
Videos/Ubi_Logo.bik
Videos/VideoBlack.bik
Videos/Vision1.bik
Videos/Vision2.bik
Videos/Vision3.bik
Videos/Vision4.bik
Videos/Vision5.bik
ekshaderspccompress.bin
DataPC.forge
DataPC_Default.forge
DataPC_DE.forge
DataPC_Ext.forge
DataPC_HC.forge
DataPC_JCT.forge
DataPC_lod.forge
DataPC_LR.forge
DataPC_OB.forge
DataPC_POP0WORLD_Ext.forge
DataPC_POP0WORLD.forge
DataPC_RC.forge
DataPC_StreamedSounds.forge'
CONTENT_DOC_DATA_FILES='
ubisoft.html
manual.pdf
ReadMe.txt'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/Prince of Persia'

APP_MAIN_EXE='Prince of Persia.exe'

PACKAGES_LIST='
PKG_BIN
PKG_L10N_EN
PKG_L10N_FR
PKG_DATA'

PKG_L10N_ID="${GAME_ID}-l10n"
PKG_L10N_EN_ID="${PKG_L10N_ID}-en"
PKG_L10N_FR_ID="${PKG_L10N_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_ID"
PKG_L10N_EN_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_FR_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_EN_DESCRIPTION='English localization'
PKG_L10N_FR_DESCRIPTION='French localization'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_L10N_ID $PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Set mandatory registry keys

registry_dump_init_file='registry-dumps/init.reg'
APP_REGEDIT="${APP_REGEDIT:-} $registry_dump_init_file"
SCRIPT_DEPS="${SCRIPT_DEPS:-} iconv"
check_deps

## English localization

registry_dump_init_file_en='registry-dumps/init-en.reg'
# shellcheck disable=SC1003
registry_dump_init_content_en='Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\Software\Ubisoft\Prince of Persia]
"Executable"="C:\\'"$(game_id)"'\\PrinceOfPersia_Launcher.exe"
"InstallDir"="C:\\'"$(game_id)"'"
"Language"="English"

[HKEY_LOCAL_MACHINE\Software\Ubisoft\Prince of Persia\1.0\Engine]
"LNG_Language"=dword:00000001

[HKEY_LOCAL_MACHINE\Software\Ubisoft\Prince of Persia\GameUpdate]
"execPath"="C:\\'"$(game_id)"'\\Prince of Persia.exe"
"info"="08a0f1c01d540ee143f7ee48a91898fa"
"installdir"="C:\\'"$(game_id)"'"
"language"="us"
'
CONTENT_GAME_L10N_EN_FILES="$(content_files 'GAME_L10N_EN')
$registry_dump_init_file_en"

## French localization

registry_dump_init_file_fr='registry-dumps/init-fr.reg'
# shellcheck disable=SC1003
registry_dump_init_content_fr='Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\Software\Ubisoft\Prince of Persia]
"Executable"="C:\\'"$(game_id)"'\\PrinceOfPersia_Launcher.exe"
"InstallDir"="C:\\'"$(game_id)"'"
"Language"="French"

[HKEY_LOCAL_MACHINE\Software\Ubisoft\Prince of Persia\1.0\Engine]
"LNG_Language"=dword:00000002

[HKEY_LOCAL_MACHINE\Software\Ubisoft\Prince of Persia\GameUpdate]
"execPath"="C:\\'"$(game_id)"'\\Prince of Persia.exe"
"info"="08a0f1c01d540ee143f7ee48a91898fa"
"installdir"="C:\\'"$(game_id)"'"
"language"="fr"
'
CONTENT_GAME_L10N_FR_FILES="$(content_files 'GAME_L10N_FR')
$registry_dump_init_file_fr"

# Set default settings

registry_dump_settings_file='registry-dumps/settings.reg'
APP_REGEDIT="${APP_REGEDIT:-} $registry_dump_settings_file"
SCRIPT_DEPS="${SCRIPT_DEPS:-} iconv"
check_deps

## English localization

registry_dump_settings_file_en='registry-dumps/settings-en.reg'
registry_dump_settings_content_en='Windows Registry Editor Version 5.00

[HKEY_CURRENT_USER\Software\Ubisoft\Prince of Persia\1.0\Engine]
"AlternateVerticalSync"=dword:00000001
"AlternateVerticalSyncThreshold"=dword:00000000
"Antialiasing"=dword:00000000
"AspectRatioOverride"=dword:000000a6
"DegradedTextures"=dword:00000000
"LNG_Language"=dword:00000001
"ParticleEffectNumber"=dword:00000064
"PostEffects"=dword:00000002
"ScreenResolutionHeight"=dword:00000300
"ScreenResolutionWidth"=dword:00000500
"Shadows"=dword:00000002
"VerticalSync"=dword:00000001

[HKEY_CURRENT_USER\Software\Ubisoft\Prince of Persia\1.0\Launcher]
"AspectRatioOverrideCHKB"=dword:00000000
"DefaultSetting"=dword:00000001
"LauncherVersion"=dword:00000004
"LNG_Language"=dword:00000001
"Texture_lvl"=dword:00000001
"VerticalSync_lvl"=dword:00000001
"VisualQualityLvl"=dword:00000002
'
CONTENT_GAME_L10N_EN_FILES="$(content_files 'GAME_L10N_EN')
$registry_dump_settings_file_en"

## French localization

registry_dump_settings_file_fr='registry-dumps/settings-fr.reg'
registry_dump_settings_content_fr='Windows Registry Editor Version 5.00

[HKEY_CURRENT_USER\Software\Ubisoft\Prince of Persia\1.0\Engine]
"AlternateVerticalSync"=dword:00000001
"AlternateVerticalSyncThreshold"=dword:00000000
"Antialiasing"=dword:00000000
"AspectRatioOverride"=dword:000000a6
"DegradedTextures"=dword:00000000
"LNG_Language"=dword:00000002
"ParticleEffectNumber"=dword:00000064
"PostEffects"=dword:00000002
"ScreenResolutionHeight"=dword:00000300
"ScreenResolutionWidth"=dword:00000500
"Shadows"=dword:00000002
"VerticalSync"=dword:00000001

[HKEY_CURRENT_USER\Software\Ubisoft\Prince of Persia\1.0\Launcher]
"AspectRatioOverrideCHKB"=dword:00000000
"DefaultSetting"=dword:00000001
"LauncherVersion"=dword:00000004
"LNG_Language"=dword:00000002
"Texture_lvl"=dword:00000001
"VerticalSync_lvl"=dword:00000001
"VisualQualityLvl"=dword:00000002
'
CONTENT_GAME_L10N_FR_FILES="$(content_files 'GAME_L10N_FR')
$registry_dump_settings_file_fr"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Set mandatory registry keys
	mkdir --parents "$(dirname "$registry_dump_init_file_en")"
	printf '%s' "$registry_dump_init_content_en" | \
		iconv --from-code=UTF-8 --to-code=UTF-16 --output="$registry_dump_init_file_en"
	mkdir --parents "$(dirname "$registry_dump_init_file_fr")"
	printf '%s' "$registry_dump_init_content_fr" | \
		iconv --from-code=UTF-8 --to-code=UTF-16 --output="$registry_dump_init_file_fr"

	## Set default settings
	mkdir --parents "$(dirname "$registry_dump_settings_file_en")"
	printf '%s' "$registry_dump_settings_content_en" | \
		iconv --from-code=UTF-8 --to-code=UTF-16 --output="$registry_dump_settings_file_en"
	mkdir --parents "$(dirname "$registry_dump_settings_file_fr")"
	printf '%s' "$registry_dump_settings_content_fr" | \
		iconv --from-code=UTF-8 --to-code=UTF-16 --output="$registry_dump_settings_file_fr"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default
## Set mandatory registry keys
(
	cd "$(package_path 'PKG_L10N_EN')$(path_game_data)"
	mv "$registry_dump_init_file_en" "$registry_dump_init_file"
)
(
	cd "$(package_path 'PKG_L10N_FR')$(path_game_data)"
	mv "$registry_dump_init_file_fr" "$registry_dump_init_file"
)
## Set default settings
(
	cd "$(package_path 'PKG_L10N_EN')$(path_game_data)"
	mv "$registry_dump_settings_file_en" "$registry_dump_settings_file"
)
(
	cd "$(package_path 'PKG_L10N_FR')$(path_game_data)"
	mv "$registry_dump_settings_file_fr" "$registry_dump_settings_file"
)

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
case "${LANG%_*}" in
	('fr')
		lang_string='version %s :'
		lang_en='anglaise'
		lang_fr='française'
	;;
	('en'|*)
		lang_string='%s version:'
		lang_en='English'
		lang_fr='French'
	;;
esac
printf '\n'
printf "$lang_string" "$lang_en"
print_instructions 'PKG_L10N_EN' 'PKG_DATA' 'PKG_BIN'
printf "$lang_string" "$lang_fr"
print_instructions 'PKG_L10N_FR' 'PKG_DATA' 'PKG_BIN'

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
