#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Jacek Szafarkiewicz
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Hard West
# send your bug reports to contact@dotslashplay.it
###

script_version=20241112.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='hard-west'
GAME_NAME='Hard West'

# Archives

## Hard West (base game)

ARCHIVE_BASE_0_NAME='gog_hard_west_2.7.0.8.sh'
ARCHIVE_BASE_0_MD5='de81eb547f089d8bdb96b7a2fe38e8c0'
ARCHIVE_BASE_0_SIZE='5688858'
ARCHIVE_BASE_0_VERSION='1.5-gog2.7.0.8'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/hard_west'

## Scars of Freedom

## TODO: Support for the expansion should be moved to a dedicated script
ARCHIVE_OPTIONAL_SCARSOFFREEDOM_0_NAME='gog_hard_west_scars_of_freedom_dlc_2.3.0.4.sh'
ARCHIVE_OPTIONAL_SCARSOFFREEDOM_0_MD5='bb4368afaf670f0d8ebc09f7bb1f3713'
ARCHIVE_OPTIONAL_SCARSOFFREEDOM_0_SIZE='200000'
ARCHIVE_OPTIONAL_SCARSOFFREEDOM_0_URL='https://www.gog.com/game/hard_west_scars_of_freedom'


UNITY3D_NAME='HardWest'
UNITY3D_PLUGINS='
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME0_DATA_FILES='
Data
Data_dlc1'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXrandr.so.2'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Load Scars of Freedom archive if it is available

archive_initialize_optional \
	'ARCHIVE_SCARSOFFREEDOM' \
	'ARCHIVE_OPTIONAL_SCARSOFFREEDOM_0'

# Extract game data

archive_extraction_default
if archive_is_available 'ARCHIVE_SCARSOFFREEDOM'; then
	archive_extraction 'ARCHIVE_SCARSOFFREEDOM'
fi

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build package

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
