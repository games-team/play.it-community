#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Monster Prom expansions:
# - Second Term
# send your bug reports to contact@dotslashplay.it
###

script_version=20241210.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='monster-prom'
GAME_NAME='Monster Prom'

EXPANSION_ID_SECOND='second-term'
EXPANSION_NAME_SECOND='Second Term'

ARCHIVE_BASE_SECOND_0_NAME='monster_prom_second_term_v6_8b_71317.sh'
ARCHIVE_BASE_SECOND_0_MD5='a27aaf8e07389026802946206a9bcd6c'
ARCHIVE_BASE_SECOND_0_SIZE='1078'
ARCHIVE_BASE_SECOND_0_VERSION='6.8b-gog71317'
ARCHIVE_BASE_SECOND_0_URL='https://www.gog.com/game/monster_prom_second_term'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
MonsterProm_Data/globalgamemanagerd'

PKG_PARENT_ID="$GAME_ID"

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_PARENT'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
