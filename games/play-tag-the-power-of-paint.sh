#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 BetaRays
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Tag: The Power of Paint
# send your bug reports to contact@dotslashplay.it
###

script_version=20231110.3

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='tag-the-power-of-paint'
GAME_NAME='Tag: The Power of Paint'

ARCHIVE_BASE_0_NAME='Tag_setup.exe'
ARCHIVE_BASE_0_MD5='e46933ffbdc3fdf0816b3ebdd6c326d5'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='80000'
ARCHIVE_BASE_0_VERSION='1.0-digipen'
ARCHIVE_BASE_0_URL='https://arcade.digipen.edu/games/tag-the-power-of-paint'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
fmodex.dll
tag.exe'
CONTENT_GAME_DATA_FILES='
assets
game data'

USER_PERSISTENT_FILES='
game data/*.ini
game data/*.xml'

## Native d3dx9_35.dll is required to prevent a crash on launch with WINE 8.0:
## Unhandled exception: unimplemented function d3dx9_35.dll.D3DXComputeTangent called in 32-bit code (0x7b012866).
WINE_WINETRICKS_VERBS='d3dx9_35'

APP_MAIN_EXE='tag.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
