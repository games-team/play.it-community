#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Little Nightmares 1
# send your bug reports to contact@dotslashplay.it
###

script_version=20240403.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='little-nightmares-1'
GAME_NAME='Little Nightmares 1'

ARCHIVE_BASE_0_NAME='setup_little_nightmares_1.0.43.1_(18471).exe'
ARCHIVE_BASE_0_MD5='6e1ac1107650aeb03abc17cf0e614e4c'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1='setup_little_nightmares_1.0.43.1_(18471)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='0732121bf0da7db6dc6f5e5a3bc8705b'
ARCHIVE_BASE_0_SIZE='9500000'
ARCHIVE_BASE_0_VERSION='1.0.43.1-gog18471'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/little_nightmares'

UNREALENGINE4_NAME='atlas'

CONTENT_PATH_DEFAULT='.'

USER_PERSISTENT_DIRECTORIES="
${UNREALENGINE4_NAME}/Saved"

APP_MAIN_EXE="${UNREALENGINE4_NAME}/binaries/win64/littlenightmares.exe"

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
