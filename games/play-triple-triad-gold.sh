#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Triple Triad Gold
# send your bug reports to contact@dotslashplay.it
###

script_version=20230821.1

GAME_ID='triple-triad-gold'
GAME_NAME='Triple Triad Gold'

ARCHIVE_BASE_0='ttg09.zip'
ARCHIVE_BASE_0_MD5='ec517c23236f2ad9217e1488f27b3dab'
ARCHIVE_BASE_0_SIZE='4400'
ARCHIVE_BASE_0_VERSION='0.9-qhimm1'
ARCHIVE_BASE_0_URL='https://ttg.qhimm.com/'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
*.dll
*.exe'
CONTENT_GAME_DATA_FILES='
*.dat'

USER_PERSISTENT_FILES='
settings.dat'
WINE_WINETRICKS_VERBS='mfc42'

APP_MAIN_EXE='Triple Triad Gold.exe'
APP_MAIN_ICON_WRESTOOL_OPTIONS='--type=14 --name=128'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

target_version='2.25'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
