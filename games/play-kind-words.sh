#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Kind Words
# send your bug reports to contact@dotslashplay.it
###

script_version=20241111.2

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='kind-words'
GAME_NAME='Kind Words'

ARCHIVE_BASE_2_NAME='kind-words-linux-20240804-1.zip'
ARCHIVE_BASE_2_MD5='6e2b736846cc7335be26b1f901c88535'
ARCHIVE_BASE_2_SIZE='371697'
ARCHIVE_BASE_2_VERSION='2024.08.04-itch'
ARCHIVE_BASE_2_URL='https://popcannibal.itch.io/kindwords'

ARCHIVE_BASE_1_NAME='kind-words-linux-64-67.zip'
ARCHIVE_BASE_1_MD5='c4cfc5e184cba2bb19964ae429c92966'
ARCHIVE_BASE_1_SIZE='285035'
ARCHIVE_BASE_1_VERSION='2022.04.01-itch.67'

ARCHIVE_BASE_0_NAME='kind-words-linux-64-66.zip'
ARCHIVE_BASE_0_MD5='e15ca776e2c5da2f3e33f03aea754730'
ARCHIVE_BASE_0_SIZE='290000'
ARCHIVE_BASE_0_VERSION='2021.09.10-itch.66'

UNITY3D_NAME='Kind Words'
## TODO: Check if the Steam library is required
UNITY3D_PLUGINS='
libsteam_api.so'

CONTENT_PATH_DEFAULT='Kind Words'
CONTENT_PATH_DEFAULT_1='.'
CONTENT_PATH_DEFAULT_0='.'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libz.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
