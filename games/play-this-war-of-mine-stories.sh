#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# This War of Mine: Stories:
# - Father's Promise
# - The Last Broadcast
# - Fading Embers
# send your bug reports to contact@dotslashplay.it
###

script_version=20240623.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='this-war-of-mine'
GAME_NAME='This War of Mine'

EXPANSION_ID_PROMISE='fathers-promise'
EXPANSION_NAME_PROMISE='Fatherʼs Promise'

EXPANSION_ID_BROADCAST='the-last-broadcast'
EXPANSION_NAME_BROADCAST='The Last Broadcast'

EXPANSION_ID_EMBERS='fading-embers'
EXPANSION_NAME_EMBERS='Fading Embers'

# Archives

## Father's Promise

ARCHIVE_BASE_PROMISE_0_NAME='this_war_of_mine_stories_father_s_promise_6_0_8_a_34693.sh'
ARCHIVE_BASE_PROMISE_0_MD5='edd88a0589e73cee7f2ad575bd87f933'
ARCHIVE_BASE_PROMISE_0_SIZE='1300'
ARCHIVE_BASE_PROMISE_0_VERSION='6.0.8a-gog34693'
ARCHIVE_BASE_PROMISE_0_URL='https://www.gog.com/game/this_war_of_mine_stories_fathers_promise'

## The Last Broadcast

ARCHIVE_BASE_BROADCAST_0_NAME='this_war_of_mine_stories_the_last_broadcast_6_0_8_a_34693.sh'
ARCHIVE_BASE_BROADCAST_0_MD5='daff8c5453365f234fbcfa5e2ad11448'
ARCHIVE_BASE_BROADCAST_0_SIZE='3900'
ARCHIVE_BASE_BROADCAST_0_VERSION='6.0.8a-gog34693'
ARCHIVE_BASE_BROADCAST_0_URL='https://www.gog.com/game/this_war_of_mine_stories_the_last_broadcast'

## Fading Embers

ARCHIVE_BASE_EMBERS_0_NAME='this_war_of_mine_stories_fading_embers_6_0_8_a_34693.sh'
ARCHIVE_BASE_EMBERS_0_MD5='1684ebcdd6be5f11e69830da349549aa'
ARCHIVE_BASE_EMBERS_0_SIZE='4100'
ARCHIVE_BASE_EMBERS_0_VERSION='6.0.8a-gog34693'
ARCHIVE_BASE_EMBERS_0_URL='https://www.gog.com/game/this_war_of_mine_stories_fading_embers'


CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
gog?.idx
gog?.dat'

PKG_MAIN_DEPS="$GAME_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
