#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Everspace
# send your bug reports to contact@dotslashplay.it
###

script_version=20230712.2

GAME_ID='everspace'
GAME_NAME='Everspace'

ARCHIVE_BASE_3='everspace_1_3_5_3655_32896.sh'
ARCHIVE_BASE_3_MD5='55c9230b3805c54d13371f940860d4a9'
ARCHIVE_BASE_3_SIZE='11000000'
ARCHIVE_BASE_3_VERSION='1.3.5-gog32896'
ARCHIVE_BASE_3_URL='https://www.gog.com/game/everspace'

ARCHIVE_BASE_2='everspace_1_3_4_29339.sh'
ARCHIVE_BASE_2_MD5='2010b839534fb5a265eea6116b9193ae'
ARCHIVE_BASE_2_SIZE='11000000'
ARCHIVE_BASE_2_VERSION='1.3.4-gog29339'

ARCHIVE_BASE_1='everspace_1_3_3_25886.sh'
ARCHIVE_BASE_1_MD5='df8f210059a515ef738f247bfcd61bb2'
ARCHIVE_BASE_1_SIZE='11000000'
ARCHIVE_BASE_1_VERSION='1.3.3-gog25886'

ARCHIVE_BASE_0='everspace_en_1_3_2_3_22978.sh'
ARCHIVE_BASE_0_MD5='4290b47c1396f140198f45a74bf53abf'
ARCHIVE_BASE_0_SIZE='11000000'
ARCHIVE_BASE_0_VERSION='1.3.2.3-gog22978'

UNREALENGINE4_NAME='RSG'

CONTENT_PATH_DEFAULT='data/noarch/game'

APPLICATIONS_PREFIX_TYPE='none'

APP_MAIN_EXE="${UNREALENGINE4_NAME}/Binaries/Linux/${UNREALENGINE4_NAME}-Linux-Shipping"
APP_MAIN_ICON='../support/icon.png'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6'

# Load common functions

target_version='2.25'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
