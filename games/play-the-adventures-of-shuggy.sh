#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Adventures of Shuggy
# send your bug reports to contact@dotslashplay.it
###

script_version=20240603.3

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='the-adventures-of-shuggy'
GAME_NAME='The Adventures of Shuggy'

ARCHIVE_BASE_0_NAME='gog_the_adventures_of_shuggy_2.0.0.2.sh'
ARCHIVE_BASE_0_MD5='7d031b4cbbbf88beb5bdaa077892215d'
ARCHIVE_BASE_0_SIZE='109602'
ARCHIVE_BASE_0_VERSION='1.10.10222015-gog2.0.0.2'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/the_adventures_of_shuggy'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_FILES='
libmojoshader.so
libtheoraplay.so'
## The game audio does not work if the shipped build of OpenAL is not used.
CONTENT_LIBS_FILES="$CONTENT_LIBS_FILES
libopenal.so.1"
CONTENT_LIBS_LIBS64_PATH="${CONTENT_PATH_DEFAULT}/lib64"
CONTENT_LIBS_LIBS64_FILES="$CONTENT_LIBS_FILES"
CONTENT_LIBS_LIBS32_PATH="${CONTENT_PATH_DEFAULT}/lib"
CONTENT_LIBS_LIBS32_FILES="$CONTENT_LIBS_FILES"
CONTENT_GAME_MAIN_FILES='
deadunderground
fx
gateways
gfx
growingpains
maps
recordings
sfx
text
video
Shuggy.exe
Shuggy.bmp
GameFont.xnb
FNA.dll
FNA.dll.config
Lidgren.Network.dll
MonoGame.Framework.Net.dll
SpriteSheetRuntime.dll'
CONTENT_DOC_MAIN_FILES='
info.txt
Linux.README'

APP_MAIN_EXE='Shuggy.exe'
APP_MAIN_ICON='Shuggy.bmp'

PACKAGES_LIST='
PKG_MAIN
PKG_LIBS64
PKG_LIBS32'

PKG_MAIN_DEPENDENCIES_LIBRARIES='
libopenal.so.1
libSDL2-2.0.so.0'
PKG_MAIN_DEPENDENCIES_MONO_LIBRARIES='
mscorlib.dll
Mono.Posix.dll
Mono.Security.dll
System.dll
System.Configuration.dll
System.Core.dll
System.Data.dll
System.Drawing.dll
System.Security.dll
System.Xml.dll'
## Ensure easy upgrades from packages generated with pre-20240603.2 game scripts.
PKG_MAIN_PROVIDES="${PKG_MAIN_PROVIDES:-}
the-adventures-of-shuggy-data"

PKG_LIBS_ID="${GAME_ID}-libs"
PKG_LIBS64_ID="$PKG_LIBS_ID"
PKG_LIBS64_ARCH='64'
PKG_LIBS32_ID="$PKG_LIBS_ID"
PKG_LIBS32_ARCH='32'
PKG_LIBS_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libm.so.6
libogg.so.0
libpthread.so.0
librt.so.1
libtheoradec.so.1
libvorbis.so.0'
PKG_LIBS64_DEPENDENCIES_LIBRARIES="$PKG_LIBS_DEPENDENCIES_LIBRARIES"
PKG_LIBS32_DEPENDENCIES_LIBRARIES="$PKG_LIBS_DEPENDENCIES_LIBRARIES"
PKG_MAIN_DEPS="${PKG_MAIN_DEPS:-} $PKG_LIBS_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_MAIN'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_MAIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
