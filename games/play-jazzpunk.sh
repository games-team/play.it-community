#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Jazzpunk
# send your bug reports to contact@dotslashplay.it
###

script_version=20231117.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='jazzpunk'
GAME_NAME='Jazzpunk'

## This DRM-free build is no longer available for sale from Humble Bundle
ARCHIVE_BASE_0_NAME='Jazzpunk-Oct-30-2017-Linux.zip'
ARCHIVE_BASE_0_MD5='e8ecf692ded05cea80701d417fa565c1'
ARCHIVE_BASE_0_SIZE='2800000'
ARCHIVE_BASE_0_VERSION='2017.10.30-humble1'

ARCHIVE_BASE_MULTIARCH_0_NAME='Jazzpunk-July6-2014-Linux.zip'
ARCHIVE_BASE_MULTIARCH_0_MD5='50ad5722cafe16dc384e83a4a4e19480'
ARCHIVE_BASE_MULTIARCH_0_SIZE='1600000'
ARCHIVE_BASE_MULTIARCH_0_VERSION='2014.07.06-humble1'

UNITY3D_NAME='Jazzpunk'
UNITY3D_PLUGINS='
ScreenSelector.so'
## The game does not start if the CSteamworks library is not provided.
UNITY3D_PLUGINS="${UNITY3D_PLUGINS:-}
libCSteamworks.so
libsteam_api.so"

CONTENT_PATH_DEFAULT='.'

PACKAGES_LIST='
PKG_BIN32
PKG_DATA'
PACKAGES_LIST_MULTIARCH='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libGLU.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Check for the presence of optional extra archives

ARCHIVE_OPTIONAL_ICONS_0_NAME='jazzpunk_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_0_MD5='d1fe700322ad08f9ac3dec1c29512f94'
ARCHIVE_OPTIONAL_ICONS_0_URL='https://downloads.dotslashplay.it/games/jazzpunk'

archive_initialize_optional \
	'ARCHIVE_ICONS' \
	'ARCHIVE_OPTIONAL_ICONS'

# Extract game data

archive_extraction_default
if archive_is_available 'ARCHIVE_ICONS'; then
	archive_extraction 'ARCHIVE_ICONS'
fi

# Include game data

if archive_is_available 'ARCHIVE_ICONS'; then
	CONTENT_ICONS_DATA_PATH='.'
	CONTENT_ICONS_DATA_FILES='
	16x16
	32x32
	48x48
	128x128
	256x256'
	content_inclusion 'ICONS_DATA' 'PKG_DATA' "$(path_icons)"
else
	set_current_package 'PKG_DATA'
	# shellcheck disable=SC2119
	icons_inclusion
fi
content_inclusion_default

# Write launchers

case "$(current_archive)" in
	('ARCHIVE_BASE_MULTIARCH_'*)
		set_current_package 'PKG_BIN32'
		# shellcheck disable=SC2119
		launchers_write
		set_current_package 'PKG_BIN64'
		# shellcheck disable=SC2119
		launchers_write
	;;
	(*)
		set_current_package 'PKG_BIN32'
		# shellcheck disable=SC2119
		launchers_write
	;;
esac

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
