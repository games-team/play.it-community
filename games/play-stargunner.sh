#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Stargunner
# send your bug reports to contact@dotslashplay.it
###

script_version=20230428.5

GAME_ID='stargunner'
GAME_NAME='Stargunner'

ARCHIVE_BASE_1='gog_stargunner_2.0.0.10.sh'
ARCHIVE_BASE_1_MD5='d04655b120bce07d7c840f49e89e6a83'
ARCHIVE_BASE_1_SIZE='58000'
ARCHIVE_BASE_1_VERSION='1.0b-gog2.0.0.10'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/stargunner'

ARCHIVE_BASE_0='gog_stargunner_2.0.0.9.sh'
ARCHIVE_BASE_0_MD5='4e90175d15754e05ad6cb0a0fa1af413'
ARCHIVE_BASE_0_SIZE='57000'
ARCHIVE_BASE_0_VERSION='1.0b-gog2.0.0.9'

CONTENT_PATH_DEFAULT='data/noarch/data'
CONTENT_GAME_MAIN_FILES='
*'
CONTENT_DOC_MAIN_PATH='data/noarch/docs'
CONTENT_DOC_MAIN_FILES='
*.pdf'

USER_PERSISTENT_FILES='
STARGUN.CFG
STARGUN.HI
STARGUN.SAV'

APP_MAIN_EXE='STARGUN.EXE'
APP_MAIN_ICON='../support/icon.png'

APP_HELP_ID="${GAME_ID}-help"
APP_HELP_NAME="$GAME_NAME - help"
APP_HELP_EXE='STARHELP.EXE'
APP_HELP_ICON='../support/icon.png'

APP_SETUP_ID="${GAME_ID}-setup"
APP_SETUP_NAME="$GAME_NAME - setup"
APP_SETUP_CAT='Settings'
APP_SETUP_EXE='SETUP.EXE'
APP_SETUP_ICON='../support/icon.png'

# Prevent the game from running too fast

APP_MAIN_DOSBOX_PRERUN="$APP_MAIN_DOSBOX_PRERUN"'
config -set cpu cycles=fixed 25000'

# Load common functions

target_version='2.23'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Generate a smaller icon from the shipped one

APP_MAIN_ICON_512="${APP_MAIN_ICON%.png}_512.png"
APP_MAIN_ICONS_LIST="$(application_icons_list 'APP_MAIN')
APP_MAIN_ICON_512"
APP_HELP_ICONS_LIST="$(application_icons_list 'APP_HELP')
APP_MAIN_ICON_512"
APP_SETUP_ICONS_LIST="$(application_icons_list 'APP_SETUP')
APP_MAIN_ICON_512"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Generate a smaller icon from the shipped one
	convert "$APP_MAIN_ICON" -resize 512x512 "$APP_MAIN_ICON_512"
)
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path 'DOC_MAIN')"

	# Convert documentation file names to lower case
	tolower .
)

# Include game icons

# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
