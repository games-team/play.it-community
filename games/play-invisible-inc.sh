#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Invisible Inc.
# send your bug reports to contact@dotslashplay.it
###

script_version=20240504.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='invisible-inc'
GAME_NAME='Invisible Inc.'

ARCHIVE_BASE_2_NAME='invisible_inc_en_281021_22858.sh'
ARCHIVE_BASE_2_MD5='bfb1493931172a9f71c95a6861af97ee'
ARCHIVE_BASE_2_SIZE='1200000'
ARCHIVE_BASE_2_VERSION='2021.10.28-gog22858'
ARCHIVE_BASE_2_URL='https://www.gog.com/game/invisible_inc'

ARCHIVE_BASE_1_NAME='invisible_inc_en_8_07_2017_15873.sh'
ARCHIVE_BASE_1_MD5='b3acb8f72cf01f71b0ddcb4355543a16'
ARCHIVE_BASE_1_SIZE='1200000'
ARCHIVE_BASE_1_VERSION='2017.07.08-gog15873'

ARCHIVE_BASE_0_NAME='gog_invisible_inc_2.6.0.11.sh'
ARCHIVE_BASE_0_MD5='97e6efdc9237ec17deb02b5cf5185cf5'
ARCHIVE_BASE_0_SIZE='1200000'
ARCHIVE_BASE_0_VERSION='2016.04.13-gog2.6.0.11'

## Optional icons pack
ARCHIVE_OPTIONAL_ICONS_NAME='invisible-inc_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_URL='https://downloads.dotslashplay.it/resources/invisible-inc/'
ARCHIVE_OPTIONAL_ICONS_MD5='37a62fed1dc4185e95db3e82e6695c1d'
CONTENT_ICONS_PATH='.'
CONTENT_ICONS_FILES='
16x16
32x32
64x64
128x128
256x256'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN64_PATH='data/noarch/game/lib64'
CONTENT_LIBS_BIN64_FILES='
libfmodevent64-4.44.47.so
libfmodevent64.so
libfmodex64-4.44.47.so
libfmodex64.so'
CONTENT_LIBS_BIN32_PATH='data/noarch/game/lib32'
CONTENT_LIBS_BIN32_FILES='
libfmodevent-4.44.47.so
libfmodevent.so
libfmodex-4.44.47.so
libfmodex.so'
CONTENT_GAME_BIN64_FILES='
InvisibleInc64'
CONTENT_GAME_BIN32_FILES='
InvisibleInc32'
CONTENT_GAME_DATA_FILES='
*.kwad
*.lua
hashes.dat
scripts.zip'
CONTENT_DOC_DATA_FILES='
LICENSE'

APP_MAIN_EXE_BIN64='InvisibleInc64'
APP_MAIN_EXE_BIN32='InvisibleInc32'
APP_MAIN_ICON='../support/icon.png'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libstdc++.so.6'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
