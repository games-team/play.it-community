#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# CrossCode
# send your bug reports to contact@dotslashplay.it
###

script_version=20241201.3

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='crosscode'
GAME_NAME='CrossCode'

GAME_ID_DEMO="${GAME_ID}-demo"
GAME_NAME_DEMO="$GAME_NAME (demo)"

ARCHIVE_BASE_64BIT_0_NAME='crosscode-new-linux64.zip'
ARCHIVE_BASE_64BIT_0_MD5='2fece5dc78bf6bfdde7e6b3239da73b6'
ARCHIVE_BASE_64BIT_0_SIZE='1019175'
ARCHIVE_BASE_64BIT_0_VERSION='1.4.2-itch.2022.04.19'
ARCHIVE_BASE_64BIT_0_URL='https://radicalfishgames.itch.io/crosscode'

ARCHIVE_BASE_32BIT_0_NAME='crosscode-new-linux32.zip'
ARCHIVE_BASE_32BIT_0_MD5='630cfd22cd5e9e168f833dfa3fce9931'
ARCHIVE_BASE_32BIT_0_SIZE='1021105'
ARCHIVE_BASE_32BIT_0_VERSION='1.4.2-itch.2022.04.19'
ARCHIVE_BASE_32BIT_0_URL='https://radicalfishgames.itch.io/crosscode'

ARCHIVE_BASE_DEMO_64BIT_0_NAME='crosscode-demo-linux64.zip'
ARCHIVE_BASE_DEMO_64BIT_0_MD5='242e4cc9f48bd70a04f2d5d5e3258207'
ARCHIVE_BASE_DEMO_64BIT_0_SIZE='211392'
ARCHIVE_BASE_DEMO_64BIT_0_VERSION='1.0-itch1'
ARCHIVE_BASE_DEMO_64BIT_0_URL='https://radicalfishgames.itch.io/crosscode'

ARCHIVE_BASE_DEMO_32BIT_0_NAME='crosscode-demo-linux32.zip'
ARCHIVE_BASE_DEMO_32BIT_0_MD5='6b6c85bcd7ad315ee42a4c785e3e85a6'
ARCHIVE_BASE_DEMO_32BIT_0_SIZE='221220'
ARCHIVE_BASE_DEMO_32BIT_0_VERSION='1.0-itch1'
ARCHIVE_BASE_DEMO_32BIT_0_URL='https://radicalfishgames.itch.io/crosscode'

CONTENT_PATH_DEFAULT='.'
CONTENT_LIBS_BIN_RELATIVE_PATH='lib'
CONTENT_LIBS_BIN_FILES='
libffmpeg.so
libnode.so
libnw.so'
CONTENT_LIBS0_BIN_RELATIVE_PATH='swiftshader'
CONTENT_LIBS0_BIN_FILES='
libEGL.so
libGLESv2.so'
CONTENT_GAME_BIN_FILES='
CrossCode'
## TODO: Check if the Steam id file is required
CONTENT_GAME_DATA_FILES='
assets
locales
natives_blob.bin
v8_context_snapshot.bin
icudtl.dat
credits.html
package.json
favicon.png
nw_100_percent.pak
nw_200_percent.pak
resources.pak
steam_appid.txt'

APP_MAIN_EXE='CrossCode'
APP_MAIN_ICON='favicon.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH_64BIT='64'
PKG_BIN_ARCH_32BIT='32'
PKG_BIN_ARCH_DEMO_64BIT='64'
PKG_BIN_ARCH_DEMO_32BIT='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libasound.so.2
libatk-1.0.so.0
libatspi.so.0
libcairo.so.2
libc.so.6
libcups.so.2
libdbus-1.so.3
libdl.so.2
libexpat.so.1
libgcc_s.so.1
libgdk-3.so.0
libgdk_pixbuf-2.0.so.0
libgio-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-3.so.0
libm.so.6
libnspr4.so
libnss3.so
libnssutil3.so
libpango-1.0.so.0
libpangocairo-1.0.so.0
libpthread.so.0
librt.so.1
libsmime3.so
libuuid.so.1
libX11.so.6
libX11-xcb.so.1
libxcb.so.1
libXcomposite.so.1
libXcursor.so.1
libXdamage.so.1
libXext.so.6
libXfixes.so.3
libXi.so.6
libXrandr.so.2
libXrender.so.1
libXss.so.1
libXtst.so.6'
PKG_BIN_DEPENDENCIES_LIBRARIES_DEMO='
libasound.so.2
libatk-1.0.so.0
libcairo.so.2
libc.so.6
libcups.so.2
libdbus-1.so.3
libdl.so.2
libexpat.so.1
libfontconfig.so.1
libfreetype.so.6
libgcc_s.so.1
libgconf-2.so.4
libgdk_pixbuf-2.0.so.0
libgdk-x11-2.0.so.0
libgio-2.0.so.0
libglib-2.0.so.0
libgmodule-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libnspr4.so
libnss3.so
libnssutil3.so
libpango-1.0.so.0
libpangocairo-1.0.so.0
libpthread.so.0
librt.so.1
libsmime3.so
libstdc++.so.6
libX11.so.6
libXcomposite.so.1
libXcursor.so.1
libXdamage.so.1
libXext.so.6
libXfixes.so.3
libXi.so.6
libXrandr.so.2
libXrender.so.1
libXss.so.1
libXtst.so.6'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_DEMO="${GAME_ID_DEMO}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

case "$(current_archive)" in
	('ARCHIVE_BASE_DEMO_'*)
		# No icon is provided with the demo builds
	;;
	(*)
		content_inclusion_icons 'PKG_DATA'
	;;
esac
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
