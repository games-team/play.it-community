#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Dungeons 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20240615.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='dungeons-2'
GAME_NAME='Dungeons 2'

ARCHIVE_BASE_0_NAME='gog_dungeons_2_2.0.0.1.sh'
ARCHIVE_BASE_0_MD5='990a4ded53d43f28e4e16c0885f515cd'
ARCHIVE_BASE_0_SIZE='5100000'
ARCHIVE_BASE_0_VERSION='1.6.1.29-gog2.0.0.1'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/dungeons_2'

UNITY3D_NAME='Dungeons2'
UNITY3D_PLUGINS='
libRakNet.so
ScreenSelector.so'
## TODO: Check if the Steam are required.
UNITY3D_PLUGINS="$UNITY3D_PLUGINS
libCSteamworks.so
libsteam_api.so"

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME0_BIN_FILES='
mono
server'
CONTENT_GAME0_DATA_FILES='
AssetBundles
mapexports
*.block
*.txt'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXrandr.so.2'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Set required execution permission on the shipped Mono binary.
	chmod 755 'mono/bin/mono'
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
