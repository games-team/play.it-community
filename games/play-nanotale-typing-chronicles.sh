#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Nanotale - Typing Chronicles
# send your bug reports to contact@dotslashplay.it
###

script_version=20230811.2

GAME_ID='nanotale-typing-chronicles'
GAME_NAME='Nanotale - Typing Chronicles'

ARCHIVE_BASE_1='nanotale_typing_chronicles_1_96_51035.sh'
ARCHIVE_BASE_1_MD5='fee2152ca68930869172c1bb6785d37d'
ARCHIVE_BASE_1_SIZE='5200000'
ARCHIVE_BASE_1_VERSION='1.96-gog51035'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/nanotale_typing_chronicles'

ARCHIVE_BASE_0='nanotale_typing_chronicles_1_95_50826.sh'
ARCHIVE_BASE_0_MD5='a77a001896b0c742feddd24ce594bc3e'
ARCHIVE_BASE_0_SIZE='4600000'
ARCHIVE_BASE_0_VERSION='1.95-gog50826'

UNITY3D_NAME='Nanotale'
UNITY3D_PLUGINS='
libfmodstudioL.so
libfmodstudio.so
libresonanceaudio.so
sqlite3.so
UnityFbxSdkNative.so'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN_FILES="
${UNITY3D_NAME}_Data/MonoBleedingEdge
${UNITY3D_NAME}
UnityPlayer.so"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data"

APP_MAIN_EXE="$UNITY3D_NAME"

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

# Load common functions

target_version='2.24'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icon

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
