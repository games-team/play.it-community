#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 BetaRays
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Populous series:
# - Populous 1
# - Populous 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20240605.2

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID_EPISODE1='populous-1'
GAME_NAME_EPISODE1='Populous: Promised Lands'

GAME_ID_EPISODE2='populous-2'
GAME_NAME_EPISODE2='Populous 2'

ARCHIVE_BASE_EPISODE1_0_NAME='setup_populous_promised_lands_2.0.0.3.exe'
ARCHIVE_BASE_EPISODE1_0_MD5='4c1844f4077b46925a57abca8111f26a'
ARCHIVE_BASE_EPISODE1_0_TYPE='innosetup'
ARCHIVE_BASE_EPISODE1_0_VERSION='1.0-gog2.0.0.3'
ARCHIVE_BASE_EPISODE1_0_SIZE='28000'
ARCHIVE_BASE_EPISODE1_0_URL='https://www.gog.com/game/populous'

ARCHIVE_BASE_EPISODE2_0_NAME='setup_populous2_2.0.0.2.exe'
ARCHIVE_BASE_EPISODE2_0_MD5='4f3b46cbadcd44821c212d96ef02cec5'
ARCHIVE_BASE_EPISODE2_0_TYPE='innosetup'
ARCHIVE_BASE_EPISODE2_0_SIZE='31000'
ARCHIVE_BASE_EPISODE2_0_VERSION='1.0-gog2.0.0.2'
ARCHIVE_BASE_EPISODE2_0_URL='https://www.gog.com/game/populous_2'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_MAIN_FILES_EPISODE1='
data
populous.exe'
CONTENT_GAME_MAIN_FILES_EPISODE2='
data
sound
pop2.exe
intro.exe'
CONTENT_DOC_MAIN_FILES='
*.pdf'

USER_PERSISTENT_DIRECTORIES='
data
sound
popsave'

APP_MAIN_EXE_EPISODE1='populous.exe'
APP_MAIN_EXE_EPISODE2='pop2.exe'
APP_MAIN_ICON='gfw_high.ico'
APP_MAIN_DOSBOX_PRERUN_EPISODE2='
intro.exe'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

## TODO: APP_xxx_DOSBOX_PRERUN should be context-sensitive.
APP_MAIN_DOSBOX_PRERUN=$(context_value 'APP_MAIN_DOSBOX_PRERUN')
launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
