#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 BetaRays
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Minecraft 4K
# send your bug reports to contact@dotslashplay.it
###

script_version=20240404.2

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='minecraft-4k'
GAME_NAME='Minecraft 4K'

ARCHIVE_BASE_0_NAME='4K.exe'
ARCHIVE_BASE_0_MD5='c566f5be102e1e1afc153690e47173e5'
ARCHIVE_BASE_0_EXTRACTOR='unzip'
ARCHIVE_BASE_0_SIZE='20'
ARCHIVE_BASE_0_VERSION='1.0-archiveorg180513'
ARCHIVE_BASE_0_URL='https://archive.org/details/Minecraft4K'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_MAIN_FILES='
M.class'

APP_MAIN_TYPE='java'
APP_MAIN_EXE='M'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# The launcher target is not a .jar archive

launcher_target_presence_check() { true ; }
game_exec_line() {
	cat <<- 'EOF'
	java "$APP_EXE" "$@"
	EOF
}

# Extract game data

## Errors during the data extraction from the archive are expected,
## they should be ignored.
archive_extraction_default || true

# Include game data

content_inclusion_default

# Write launchers

# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
