#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# GreedCorp
# send your bug reports to contact@dotslashplay.it
###

script_version=20241203.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='greedcorp'
GAME_NAME='GreedCorp'

# This DRM-free archive is no longer available for sale from Humble Bundle.
ARCHIVE_BASE_0_NAME='greedcorp_linux.tar.gz'
ARCHIVE_BASE_0_MD5='c1cffb847bf65caf8abd4c589813884a'
ARCHIVE_BASE_0_SIZE='205594'
ARCHIVE_BASE_0_VERSION='1.0-humble'

UNITY3D_NAME='GreedCorp'
UNITY3D_PLUGINS='
liblibAI.so'

CONTENT_PATH_DEFAULT='GreedCorp'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libGLU.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXext.so.6'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

## Work around the engine looking for a library in an hardcoded path
library_source="$(path_libraries)/liblibAI.so"
library_destination="$(package_path 'PKG_BIN64')$(path_game_data)/$(unity3d_name)_Data/Plugins/x86_64/liblibAI.so"
mkdir --parents "$(dirname "$library_destination")"
ln --symbolic "$library_source"	"$library_destination"
library_destination="$(package_path 'PKG_BIN32')$(path_game_data)/$(unity3d_name)_Data/Plugins/x86/liblibAI.so"
mkdir --parents "$(dirname "$library_destination")"
ln --symbolic "$library_source"	"$library_destination"

# Write launchers

## Do not disable the MAP_32BIT flag, as it would lead to a crash on launch
unity3d_disable_map32bit() { return 0 ; }

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
