#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Unsung Warriors
# send your bug reports to contact@dotslashplay.it
###

script_version=20240617.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='unsung-warriors-prologue'
GAME_NAME='Unsung Warriors Prologue'

GAME_ID_PROLOGUE="${GAME_ID}-prologue"
GAME_NAME_PROLOGUE="$GAME_NAME Prologue"

ARCHIVE_BASE_PROLOGUE_0_NAME='unsung-warriors-prologue-linux.zip'
ARCHIVE_BASE_PROLOGUE_0_MD5='09fd165d947d2eb02bc51a04b06e415a'
ARCHIVE_BASE_PROLOGUE_0_SIZE='306303'
ARCHIVE_BASE_PROLOGUE_0_VERSION='1.0.5.1-itch1'
ARCHIVE_BASE_PROLOGUE_0_URL='https://unsungwarriors.itch.io/unsung-warriors-prologue'

UNITY3D_NAME_PROLOGUE='UnsungWarriorsPrologue'
UNITY3D_PLUGINS='
ScreenSelector.so'
## TODO: Check is the Steam libraey is required.
UNITY3D_PLUGINS="$UNITY3D_PLUGINS
libsteam_api.so"

CONTENT_PATH_DEFAULT='.'
CONTENT_DOC_DATA_FILES='readme.txt'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID_PROLOGUE="${GAME_ID_PROLOGUE}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS_PROLOGUE="$PKG_DATA_ID_PROLOGUE"
PKG_BIN64_DEPS_PROLOGUE="$PKG_BIN_DEPS_PROLOGUE"
PKG_BIN32_DEPS_PROLOGUE="$PKG_BIN_DEPS_PROLOGUE"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
