#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Master of Orion 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20240624.2

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='master-of-orion-2'
GAME_NAME='Master of Orion Ⅱ: Battle at Antares'

ARCHIVE_BASE_EN_0_NAME='gog_master_of_orion_2_2.0.0.6.sh'
ARCHIVE_BASE_EN_0_MD5='51529fd6734bc12f1ac36fea5fc547f8'
ARCHIVE_BASE_EN_0_SIZE='350000'
ARCHIVE_BASE_EN_0_VERSION='1.31-gog2.0.0.6'
ARCHIVE_BASE_EN_0_URL='https://www.gog.com/game/master_of_orion_1_2'

ARCHIVE_BASE_FR_0_NAME='gog_master_of_orion_2_french_2.0.0.6.sh'
ARCHIVE_BASE_FR_0_MD5='06d643ee04387914738707d435e8f7a6'
ARCHIVE_BASE_FR_0_SIZE='370000'
ARCHIVE_BASE_FR_0_VERSION='1.31-gog2.0.0.6'
ARCHIVE_BASE_FR_0_URL='https://www.gog.com/game/master_of_orion_1_2'

CONTENT_PATH_DEFAULT='data/noarch/data'
CONTENT_GAME_L10N_FILES='
*.BAT
*.COM
*.DLL
*.SET
ANWINFIN.LBX
CMBTSHP.LBX
COUNCIL.LBX
DIPLOMAT.LBX
DIPLOMSF.LBX
DIPLOMSG.LBX
DIPLOMSI.LBX
DIPLOMSS.LBX
ESTRFREN.LBX
ESTRGERM.LBX
ESTRITAL.LBX
ESTRPOLI.LBX
ESTRSPAN.LBX
EVENTMSF.LBX
EVENTMSG.LBX
EVENTMSI.LBX
EVENTMSS.LBX
FILEDATA.LBX
FONTSF.LBX
FONTSG.LBX
FONTSI.LBX
FONTSS.LBX
FRECRDTS.LBX
FRE_HELP.LBX
FRESKLLS.LBX
FRETECD.LBX
GERCRDTS.LBX
GER_HELP.LBX
GERSKLLS.LBX
GERTECD.LBX
HELP.LBX
HERODATF.LBX
HERODATG.LBX
HERODATI.LBX
HERODATS.LBX
HESTRNGS.LBX
HFSTRNGS.LBX
HGSTRNGS.LBX
HISTRNGS.LBX
HSSTRNGS.LBX
IFONTS.LBX
INSTALL.LBX
ITACRDTS.LBX
ITA_HELP.LBX
ITASKLLS.LBX
ITATECD.LBX
LANGUAGE.INI
LOSERFIN.LBX
MAINFREN.LBX
MAINGERM.LBX
MAINITAL.LBX
MAINSPAN.LBX
ORION2.EXE
ORION95.EXE
RSTRING1.LBX
RSTRING2.LBX
RSTRING3.LBX
RSTRING4.LBX
RSTRINGS.LBX
SIMTEX.LBX
SPACRDTS.LBX
SPA_HELP.LBX
SPASKLLS.LBX
SPATECD.LBX
STREAMHD.LBX
TANM_114.LBX
WARNING.LBX'
CONTENT_GAME_MAIN_FILES='
*.AD
*.BNK
*.CAT
*.DIG
*.EXE
*.INI
*.LBX
*.LST
*.MDI
*.MT
*.OPL'
CONTENT_DOC_L10N_FILES='
*.TXT'
CONTENT_DOC_MAIN_PATH='data/noarch/docs'
CONTENT_DOC_MAIN_FILES='
*.pdf'

APP_MAIN_EXE='ORION2.EXE'
APP_MAIN_ICON='../support/icon.png'

USER_PERSISTENT_FILES='
MOX.SET
SOUND.LBX
*.INI
*.GAM'

PACKAGES_LIST='
PKG_L10N
PKG_MAIN'

PKG_L10N_ID="${GAME_ID}-l10n"
PKG_L10N_ID_EN="${PKG_L10N_ID}-en"
PKG_L10N_ID_FR="${PKG_L10N_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_ID"
PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_FR='French localization'

PKG_MAIN_DEPS="$PKG_L10N_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Convert all file paths to uppercase.
	toupper .
)

# Include game data

content_inclusion_icons 'PKG_MAIN'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_MAIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
