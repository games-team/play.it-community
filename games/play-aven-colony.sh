#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Aven Colony
# send your bug reports to contact@dotslashplay.it
###

script_version=20230814.1

GAME_ID='aven-colony'
GAME_NAME='Aven Colony'

ARCHIVE_BASE_0='setup_aven_colony_1.0.1_(64bit)_(25959).exe'
ARCHIVE_BASE_0_MD5='959a44c1bb18e8c3342b10d396ae8a53'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1='setup_aven_colony_1.0.1_(64bit)_(25959)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='4f24fb8c38d99916a623c46aafc4eeeb'
ARCHIVE_BASE_0_PART2='setup_aven_colony_1.0.1_(64bit)_(25959)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='db96cef3e5ce8e2f160b703d6d81777b'
ARCHIVE_BASE_0_PART3='setup_aven_colony_1.0.1_(64bit)_(25959)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='0ab31c5b95332e6ab160f55c5fbe0db3'
ARCHIVE_BASE_0_PART4='setup_aven_colony_1.0.1_(64bit)_(25959)-4.bin'
ARCHIVE_BASE_0_PART4_MD5='2537b2db5c65a3b1ded96e938a864c52'
ARCHIVE_BASE_0_SIZE='15000000'
ARCHIVE_BASE_0_VERSION='1.0.25697-gog25959'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/aven_colony'

UNREALENGINE4_NAME='avencolony'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_DATA_TILESETS_1_FILES="
${UNREALENGINE4_NAME}/content/granitesdk/importedtilesets/city-*"
CONTENT_GAME_DATA_TILESETS_2_FILES="
${UNREALENGINE4_NAME}/content/granitesdk/importedtilesets"

APP_MAIN_EXE="${UNREALENGINE4_NAME}.exe"

PACKAGES_LIST='PKG_BIN PKG_DATA_TILESETS_1 PKG_DATA_TILESETS_2 PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_DATA_TILESETS_ID="${PKG_DATA_ID}-tilesets"
PKG_DATA_TILESETS_1_ID="${PKG_DATA_TILESETS_ID}-1"
PKG_DATA_TILESETS_2_ID="${PKG_DATA_TILESETS_ID}-2"
PKG_DATA_TILESETS_DESCRIPTION="$PKG_DATA_DESCRIPTION - tilesets"
PKG_DATA_TILESETS_1_DESCRIPTION="$PKG_TILESETS_DESCRIPTION - 1"
PKG_DATA_TILESETS_2_DESCRIPTION="$PKG_TILESETS_DESCRIPTION - 2"
PKG_DATA_DEPS="${PKG_DATA_DEPS:-} $PKG_DATA_TILESETS_1_ID $PKG_DATA_TILESETS_2_ID"

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

target_version='2.25'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
