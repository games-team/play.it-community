#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Arcanum: Of Steamworks and Magick Obscura
# send your bug reports to contact@dotslashplay.it
###

script_version=20250113.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='arcanum'
GAME_NAME='Arcanum: Of Steamworks and Magick Obscura'

ARCHIVE_BASE_3_NAME='setup_arcanum_-_of_steamworks_and_magick_obscura_1.0.7.4_ddrawfix_(77721).exe'
ARCHIVE_BASE_3_MD5='7a00ac83478518ae287e4f532f5c84ab'
ARCHIVE_BASE_3_TYPE='innosetup'
ARCHIVE_BASE_3_SIZE='1177294'
ARCHIVE_BASE_3_VERSION='1.0.7.4-gog77721'
ARCHIVE_BASE_3_URL='https://www.gog.com/game/arcanum_of_steamworks_and_magick_obscura'

ARCHIVE_BASE_2_NAME='setup_arcanum_-_of_steamworks_and_magick_obscura_1.0.7.4_hotfix_(24155).exe'
ARCHIVE_BASE_2_MD5='6d14d07f7cc8c9823cba5322cf2336f4'
ARCHIVE_BASE_2_TYPE='innosetup'
ARCHIVE_BASE_2_SIZE='1200000'
ARCHIVE_BASE_2_VERSION='1.0.7.4-gog24155'

ARCHIVE_BASE_1_NAME='setup_arcanum_-_of_steamworks_and_magick_obscura_1.0.7.4_(19476).exe'
ARCHIVE_BASE_1_MD5='298a3315baebf40f3cc6cee4acae9947'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='1200000'
ARCHIVE_BASE_1_VERSION='1.0.7.4-gog19476'

ARCHIVE_BASE_0_NAME='setup_arcanum_2.0.0.15.exe'
ARCHIVE_BASE_0_MD5='c09523c61edd18abb97da97463e07a88'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='1200000'
ARCHIVE_BASE_0_VERSION='1.0.7.4-gog2.0.0.15'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_BIN_FILES='
sierraup.cfg
binkw32.dll
ddraw.dll
mm_won.dll
mss32.dll
sierrapt.dll
arcanum.exe
msiexec.exe
removeprotos.exe
sierra.inf
*.asi'
CONTENT_GAME_DATA_FILES='
data
modules
*.dat'
CONTENT_DOC_DATA_FILES='
documents
eula.doc
customer_support.htm
manual.pdf
eula.txt
readme.txt
version.txt'
CONTENT_DOC0_DATA_PATH='__support/app'
CONTENT_DOC0_DATA_FILES='
eula.*'

USER_PERSISTENT_DIRECTORIES='
data
modules/arcanum/maps
modules/arcanum/saves'
USER_PERSISTENT_FILES='
*.cfg'

## TODO: Check if this is still required with current WINE
WINE_VIRTUAL_DESKTOP='auto'

APP_MAIN_EXE='arcanum.exe'
## Disable 3D acceleration
## cf. https://bugs.winehq.org/show_bug.cgi?id=23676 - Arcanum essentially unusable due to graphics errors
## TODO: Check if this is still required with current WINE
APP_MAIN_OPTIONS='-no3d'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# In some archives, the game binary is compressed using UPX

REQUIREMENTS_LIST="${REQUIREMENTS_LIST:-}
upx"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Decompress UPX-packed game binary
	binary_file=$(application_exe 'APP_MAIN')
	if upx -t -qqq "$binary_file"; then
		upx -d -qqq "$binary_file"
	fi
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
