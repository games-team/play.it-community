#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Dream
# send your bug reports to contact@dotslashplay.it
###

script_version=20240501.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='dream'
GAME_NAME='Dream'

ARCHIVE_BASE_0_NAME='setup_dream_1.12_(20822).exe'
ARCHIVE_BASE_0_MD5='a4e75ba9d21e146fff1a9cfca2674657'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1='setup_dream_1.12_(20822)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='944eee69b9e0a90a0d03dd2e83c2644e'
ARCHIVE_BASE_0_SIZE='3100000'
ARCHIVE_BASE_0_VERSION='1.12-gog20822'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/dream'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
binaries
engine'
CONTENT_GAME_DATA_FILES='
udkgame'

USER_PERSISTENT_DIRECTORIES='
udkgame/config
Saves'

WINE_WINEPREFIX_TWEAKS='mono'

APP_MAIN_EXE='binaries/win32/dream.exe'
## The type must be set explicitly, or it will be wrongly identified as a Mono application.
APP_MAIN_TYPE='wine'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
