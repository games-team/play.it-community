#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Symphony
# send your bug reports to contact@dotslashplay.it
###

script_version=20241111.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='symphony'
GAME_NAME='Symphony'

## This installer used to be available for free from Humble Bundle,
## during some time-limited event.
ARCHIVE_BASE_0_NAME='Symphony-Linux-2014-07-15.sh'
ARCHIVE_BASE_0_MD5='9218c0b803baf90c34bed2ac1501efa6'
ARCHIVE_BASE_0_SIZE='405705'
ARCHIVE_BASE_0_VERSION='2014.07.15-humble1'

CONTENT_PATH_DEFAULT='data'
CONTENT_LIBS_BIN_FILES='
libfmodex-4.44.20.so'
CONTENT_LIBS_BIN64_RELATIVE_PATH='x86_64/lib64'
CONTENT_LIBS_BIN64_FILES="$CONTENT_LIBS_BIN_FILES"
CONTENT_LIBS_BIN32_RELATIVE_PATH='x86/lib'
CONTENT_LIBS_BIN32_FILES="$CONTENT_LIBS_BIN_FILES"
CONTENT_GAME_BIN64_RELATIVE_PATH='x86_64'
CONTENT_GAME_BIN64_FILES='
Symphony.bin.x86_64'
CONTENT_GAME_BIN32_RELATIVE_PATH='x86'
CONTENT_GAME_BIN32_FILES='
Symphony.bin.x86'
CONTENT_GAME_DATA_RELATIVE_PATH='noarch'
CONTENT_GAME_DATA_FILES='
Symphony.png
Symphony soundtrack
*.mdf
*.mtc'
CONTENT_DOC_DATA_RELATIVE_PATH='noarch'
CONTENT_DOC_DATA_FILES='
README.linux'

APP_MAIN_EXE_BIN64='Symphony.bin.x86_64'
APP_MAIN_EXE_BIN32='Symphony.bin.x86'
APP_MAIN_ICON='noarch/Symphony.png'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libstdc++.so.6'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
