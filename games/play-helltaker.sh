#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Emmanuel Gil Peyrot <linkmauve@linkmauve.fr>
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Helltaker
# send your bug reports to contact@dotslashplay.it
###

script_version=20230324.1

GAME_ID='helltaker'
GAME_NAME='Helltaker'

ARCHIVE_BASE_0='helltaker_lnx.zip'
ARCHIVE_BASE_0_MD5='114d9b631637057f1e0674615754bc50'
ARCHIVE_BASE_0_SIZE='350000'
ARCHIVE_BASE_0_VERSION='1.0-itch.2021.06.01'
ARCHIVE_BASE_0_URL='https://vanripper.itch.io/helltaker'

UNITY3D_NAME='helltaker_lnx'

CONTENT_PATH_DEFAULT='.'
CONTENT_LIBS_BIN_PATH="${CONTENT_PATH_DEFAULT}/${UNITY3D_NAME}_Data/Plugins/x86_64"
CONTENT_LIBS_BIN_FILES='
ScreenSelector.so'
CONTENT_GAME_BIN_FILES="
${UNITY3D_NAME}_Data/MonoBleedingEdge/x86_64
${UNITY3D_NAME}.x86_64"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data/Managed
${UNITY3D_NAME}_Data/MonoBleedingEdge/etc
${UNITY3D_NAME}_Data/Resources
${UNITY3D_NAME}_Data/app.info
${UNITY3D_NAME}_Data/boot.config
${UNITY3D_NAME}_Data/globalgamemanagers
${UNITY3D_NAME}_Data/level?
${UNITY3D_NAME}_Data/level??
${UNITY3D_NAME}_Data/*.assets
${UNITY3D_NAME}_Data/*.assets.resS
${UNITY3D_NAME}_Data/*.png
${UNITY3D_NAME}_Data/*.resource
local
localHM"

## Work around automatic application type detection failure,
## cf. https://forge.dotslashplay.it/play.it/scripts/-/merge_requests/1707
APP_MAIN_EXE="${UNITY3D_NAME}.x86_64"

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libatk-1.0.so.0
libcairo.so.2
libc.so.6
libdl.so.2
libfontconfig.so.1
libfreetype.so.6
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libgdk-x11-2.0.so.0
libgio-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpango-1.0.so.0
libpangocairo-1.0.so.0
libpangoft2-1.0.so.0
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

# Load common functions

target_version='2.22'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
