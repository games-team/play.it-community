#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Hive Time
# send your bug reports to contact@dotslashplay.it
###

script_version=20241215.3

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='hive-time'
GAME_NAME='Hive Time'

ARCHIVE_BASE_1_NAME='hive-time-linux-v1.2.zip'
ARCHIVE_BASE_1_MD5='e6766ce08338257a4207ef0ee306f63b'
ARCHIVE_BASE_1_SIZE='202987'
ARCHIVE_BASE_1_VERSION='1.2-itch'
ARCHIVE_BASE_1_URL='https://cheeseness.itch.io/hive-time'

ARCHIVE_BASE_0_NAME='hive-time-linux-v1.1.zip'
ARCHIVE_BASE_0_MD5='5d63b490982d96620b0bbb7b6522be56'
ARCHIVE_BASE_0_SIZE='180000'
ARCHIVE_BASE_0_VERSION='1.1-itch'

CONTENT_PATH_DEFAULT='hive-time_linux_v1.2'
CONTENT_PATH_DEFAULT_0='.'
CONTENT_GAME_BIN_FILES='
hive-time_linux_v1.2.x86_64'
CONTENT_GAME_BIN_FILES_0='
hive-time_linux_v1.1.x86_64'
## TODO: Move the shipped fonts to system paths
##       This would require support for OpenType fonts in addition to TrueType ones.
CONTENT_GAME_DATA_FILES='
fonts
json'
CONTENT_GAME0_DATA_FILES='
hive-time_linux_v1.2.pck'
CONTENT_GAME0_DATA_FILES_0='
hive-time_linux_v1.1.pck'

## TODO: Check if we can replace the shipped binary with system-provided Godot
APP_MAIN_EXE='hive-time_linux_v1.2.x86_64'
APP_MAIN_EXE_0='hive-time_linux_v1.1.x86_64'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libGL.so.1
libm.so.6
libpthread.so.0
libX11.so.6
libXcursor.so.1
libXext.so.6
libXinerama.so.1
libXi.so.6
libXrandr.so.2
libXrender.so.1'
PKG_BIN_DEPENDENCIES_LIBRARIES_0='
libasound.so.2
libc.so.6
libdl.so.2
libGL.so.1
libm.so.6
libpthread.so.0
libpulse.so.0
libX11.so.6
libXcursor.so.1
libXinerama.so.1
libXi.so.6
libXrandr.so.2
libXrender.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
