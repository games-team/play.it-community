#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 mortalius
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Door Kickers
# send your bug reports to contact@dotslashplay.it
###

script_version=20240404.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='door-kickers'
GAME_NAME='Door Kickers'

ARCHIVE_BASE_GOG_0_NAME='gog_door_kickers_2.7.0.11.sh'
ARCHIVE_BASE_GOG_0_MD5='29efa58e4a61060b0b1211dddd2476a1'
ARCHIVE_BASE_GOG_0_SIZE='1400000'
ARCHIVE_BASE_GOG_0_VERSION='1.0.9-gog2.7.0.11'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/door_kickers'

ARCHIVE_BASE_HUMBLE_0_NAME='DoorKickers1448920440.tar.gz'
ARCHIVE_BASE_HUMBLE_0_MD5='0126db31867ae0e7a7eceee54de4a177'
ARCHIVE_BASE_HUMBLE_0_SIZE='1400000'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.0.9-humble151130'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/door-kickers'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='DoorKickers'
CONTENT_LIBS_BIN_PATH_GOG="${CONTENT_PATH_DEFAULT_GOG}/linux_libs"
CONTENT_LIBS_BIN_PATH_HUMBLE="${CONTENT_PATH_DEFAULT_HUMBLE}/linux_libs"
## Shipped libavdevice.so.55 is not actually used.
CONTENT_LIBS_BIN_FILES='
libavcodec.so.55
libavformat.so.55
libavutil.so.52
libswscale.so.2'
CONTENT_GAME_BIN_FILES='
DoorKickers'
CONTENT_GAME_DATA_FILES='
data
mods'
CONTENT_DOC_DATA_FILES='
*.txt'

APP_MAIN_EXE='DoorKickers'
APP_MAIN_ICON_GOG='../support/icon.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
librt.so.1
libstdc++.so.6
libtheoradec.so.1
libtheoraenc.so.1
libX11.so.6
libXxf86vm.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

case "$(current_archive)" in
	('ARCHIVE_BASE_GOG_'*)
		set_current_package 'PKG_DATA'
		# shellcheck disable=SC2119
		icons_inclusion
	;;
esac
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
