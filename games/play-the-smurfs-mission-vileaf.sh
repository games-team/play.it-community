#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Smurfs: Mission Vileaf
# send your bug reports to contact@dotslashplay.it
###

script_version=20240319.3

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='the-smurfs-mission-vileaf'
GAME_NAME='The Smurfs: Mission Vileaf'

ARCHIVE_BASE_0_NAME='setup_the_smurfs_-_mission_vileaf_1.0.19.3_(68727).exe'
ARCHIVE_BASE_0_MD5='086cdfa541244a81b9e00e3c55daad1b'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_the_smurfs_-_mission_vileaf_1.0.19.3_(68727)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='1f36f6b263cb1e983d8f6608e252671d'
ARCHIVE_BASE_0_SIZE='5283712'
ARCHIVE_BASE_0_VERSION='1.0.19.3-gog68727'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/the_smurfs_mission_vileaf'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
fmod.dll
fmodstudio.dll
libscepad.dll
thesmurfsmissionvileaf.exe'
## thesmurfsmissionvileaf.exe is linked against Galaxy64.dll
CONTENT_GAME0_BIN_FILES='
galaxy64.dll'
CONTENT_GAME_DATA_FILES='
smurfsresources'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Saved Games/Smurfs'

APP_MAIN_EXE='thesmurfsmissionvileaf.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
PKG_BIN_DEPS="${PKG_BIN_DEPS:-} $PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
