#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Dracula: Love Kills
# send your bug reports to contact@dotslashplay.it
###

script_version=20230328.2

GAME_ID='dracula-love-kills'
GAME_NAME='Dracula: Love Kills'

ARCHIVE_BASE_0='setup_dracula_love_kills_1.0_(31713).exe'
ARCHIVE_BASE_0_MD5='df1dc685677ed169914c9a1d96f47e32'
ARCHIVE_BASE_0_SIZE='470000'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_VERSION='1.0-gog31713'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/dracula_love_kills'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
d3dx9_42.dll
msvcr71.dll
dracula_love_kills.exe'
CONTENT_GAME_DATA_FILES='
000
branding
cursor
shaders.0050
game.ini
user.ini
data.txt'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Roaming/Frogwares/Dracula. Love Kills'

APP_MAIN_EXE='dracula_love_kills.exe'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

target_version='2.23'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
