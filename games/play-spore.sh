#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Spore
# send your bug reports to contact@dotslashplay.it
###

script_version=20241026.2

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='spore'
GAME_NAME='Spore'

ARCHIVE_BASE_1_NAME='setup_sporetm_collection_3.1.0.29_(77221).exe'
ARCHIVE_BASE_1_MD5='a84931fcbdda5dda58dcc5cd9d5b906f'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_sporetm_collection_3.1.0.29_(77221)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='da8282841877bdfeb9ba3dbbaff03e1e'
ARCHIVE_BASE_1_PART2_NAME='setup_sporetm_collection_3.1.0.29_(77221)-2.bin'
ARCHIVE_BASE_1_PART2_MD5='e5290fb4975109b9229d3494c4c5ccd9'
ARCHIVE_BASE_1_SIZE='6223301'
ARCHIVE_BASE_1_VERSION='3.1.0.29-gog77221'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/spore_collection'

ARCHIVE_BASE_0_NAME='setup_spore_3.1.0.22_(10834).exe'
ARCHIVE_BASE_0_MD5='195652ca3eb62fe725f7b334cb27874e'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_spore_3.1.0.22_(10834)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='384354f468d981965d45f437e54a13ab'
ARCHIVE_BASE_0_PART2_NAME='setup_spore_3.1.0.22_(10834)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='885466a612c4243ef10758c7c6cfcfbb'
ARCHIVE_BASE_0_SIZE='6300000'
ARCHIVE_BASE_0_VERSION='3.1.0.22-gog10834'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_BIN_FILES='
sporebin'
CONTENT_GAME_EP1_BIN_FILES='
sporebinep1'
CONTENT_GAME_DATA_FILES='
data'
CONTENT_GAME_EP1_DATA_FILES='
dataep1'
CONTENT_GAME_BP1_FILES='
bp1content'
CONTENT_GAME0_DATA_PATH="${CONTENT_PATH_DEFAULT}/__support/add"
CONTENT_GAME0_DATA_PATH_0="${CONTENT_PATH_DEFAULT_0}/__support/add"
CONTENT_GAME0_DATA_FILES='
login.prop'
CONTENT_DOC_DATA_FILES='
support'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Roaming/Spore
users/${USER}/Documents/My Spore Creations'

APP_MAIN_EXE='sporebin/sporeapp.exe'
## Include the login workaround
## TODO: With ./play.it 2.31, it could be done by overriding wine_wineprefix_init_actions
##       cf. https://forge.dotslashplay.it/play.it/play.it/-/issues/536
APP_MAIN_PRERUN='
# Include the login workaround
login_file_destination="${WINEPREFIX}/drive_c/users/${USER}/AppData/Roaming/Spore/Preferences/login.prop"
if [ ! -e "$login_file_destination" ]; then
	install -D --mode=644 \
		"${PATH_GAME_DATA}/login.prop" \
		"$login_file_destination"
fi
'

APP_EP1_ID="${GAME_ID}-galactic-adventures"
APP_EP1_NAME="$GAME_NAME - Galactic Adventures"
APP_EP1_EXE='sporebinep1/sporeapp.exe'
## Include the login workaround
APP_EP1_PRERUN="$APP_MAIN_PRERUN"

PACKAGES_LIST='
PKG_BIN
PKG_DATA
PKG_EP1_BIN
PKG_EP1_DATA
PKG_BP1'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_EP1_BIN_ID="${GAME_ID}-galactic-adventures"
PKG_EP1_BIN_DESCRIPTION='Galactic Adventures'
PKG_EP1_BIN_ARCH='32'
PKG_EP1_BIN_DEPENDENCIES_SIBLINGS='
PKG_BIN
PKG_EP1_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_EP1_DATA_ID="${PKG_EP1_BIN_ID}-data"
PKG_EP1_DATA_DESCRIPTION="$PKG_EP1_BIN_DESCRIPTION - data"

PKG_BP1_ID="${GAME_ID}-creepy-and-cute-parts-pack"
PKG_BP1_DESCRIPTION='Creepy & Cute Parts Pack'
PKG_BP1_DEPENDENCIES_SIBLINGS='
PKG_BIN'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Set required registry keys

game_id=$(game_id)
# shellcheck disable=SC1003
installloc='C:\\'"${game_id}"
# shellcheck disable=SC1003
datadir='C:\\'"${game_id}"'\\data'
# shellcheck disable=SC1003
datadir_ep1='C:\\'"${game_id}"'\\dataep1\\'

registry_dump_init_file='registry-dumps/init.reg'
registry_dump_init_content='Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\Software\electronic arts\spore]
"appdir"="Spore"
"installcompleted"=dword:00000001
"locale"="en-us"
"playerdir"="My Spore Creations"
"reindexyesorno"=dword:00000000
"installloc"="'"${installloc}"'"
"datadir"="'"${datadir}"'"

[HKEY_LOCAL_MACHINE\Software\electronic arts\spore_EP1]
"AddOnID"=dword:00000002
"PackID"=dword:07a7f786
"DataDir"="'"${datadir_ep1}"'"'
CONTENT_GAME_BIN_FILES="${CONTENT_GAME_BIN_FILES:-}
$registry_dump_init_file"
APP_REGEDIT="${APP_REGEDIT:-} $registry_dump_init_file"
SCRIPT_DEPS="${SCRIPT_DEPS:-} iconv"
check_deps

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Set required registry keys
	mkdir --parents "$(dirname "$registry_dump_init_file")"
	printf '%s' "$registry_dump_init_content" | \
		iconv --from-code=UTF-8 --to-code=UTF-16 --output="$registry_dump_init_file"
)

# Include game data

content_inclusion_icons 'PKG_DATA' 'APP_MAIN'
content_inclusion_icons 'PKG_EP1_DATA' 'APP_EP1'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN' 'APP_MAIN'
launchers_generation 'PKG_EP1_BIN' 'APP_EP1'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
