#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# No Man's Sky
# send your bug reports to contact@dotslashplay.it
###

script_version=20230731.3

GAME_ID='no-mans-sky'
GAME_NAME='No Manʼs Sky'

ARCHIVE_BASE_0='setup_no_mans_sky_experimental_54853_(34267).exe'
ARCHIVE_BASE_0_MD5='c9a4884cf0158412f5feb2f34006b69d'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1='setup_no_mans_sky_experimental_54853_(34267)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='60db3050ffeb6db013c1170e6641d734'
ARCHIVE_BASE_0_PART2='setup_no_mans_sky_experimental_54853_(34267)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='91569361327142cc6855b46450f0f44b'
ARCHIVE_BASE_0_PART3='setup_no_mans_sky_experimental_54853_(34267)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='a75187db1de93e9bd66fa0b7d1ac7ef7'
ARCHIVE_BASE_0_VERSION='54853-gog34267'
ARCHIVE_BASE_0_SIZE='9600000'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/no_mans_sky'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
binaries
languagesetup.exe'
CONTENT_GAME_DATA_FILES='
gamedata'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Roaming/HelloGames/NMS'

USER_PERSISTENT_DIRECTORIES='
binaries/settings'

APP_MAIN_EXE='binaries/nms.exe'

APP_LANGUAGE_ID="${GAME_ID}-language-selector"
APP_LANGUAGE_NAME="$GAME_NAME - Language Selector"
APP_LANGUAGE_CAT='Settings'
APP_LANGUAGE_EXE='languagesetup.exe'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

target_version='2.24'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build package

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
