#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Rebel Galaxy
# send your bug reports to contact@dotslashplay.it
###

script_version=20240328.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='rebel-galaxy'
GAME_NAME='Rebel Galaxy'

ARCHIVE_BASE_0_NAME='setup_rebel_galaxy_1.08(hotifx2)_(23097).exe'
ARCHIVE_BASE_0_MD5='9746494be23b83bc2a44d8da6cb6e311'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_rebel_galaxy_1.08(hotifx2)_(23097)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='e8e5d4450b5ad8f9cc757cc4153ba13c'
ARCHIVE_BASE_0_SIZE='2500000'
ARCHIVE_BASE_0_VERSION='1.08-gog23097'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/rebel_galaxy'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
*.cfg
*.dll
*.exe'
CONTENT_GAME_DATA_FILES='
media
music
paks
video'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/My Games/Double Damage Games/RebelGalaxy'
## Microsoft Visual C++ 2012 Runtime is required by the settings window.
WINE_WINETRICKS_VERBS='vcrun2012'

APP_MAIN_EXE='rebelgalaxygog.exe'

APP_SETTINGS_ID="${GAME_ID}-settings"
APP_SETTINGS_NAME="${GAME_NAME} - Settings"
APP_SETTINGS_CAT='Settings'
APP_SETTINGS_EXE='goglauncher.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Disable Direct3D 11 on first launch to prevent a game crash.
## TODO: Check if this is sitll required with current WINE builds.

APP_PRERUN_NOD3D11='
# Disable Direct3D 11 on first launch to prevent a game crash
settings_file="${WINEPREFIX}/drive_c/users/${USER}/Documents/My Games/Double Damage Games/RebelGalaxy/local_settings.txt"
if [ ! -e "$settings_file" ]; then
	mkdir --parents "$(dirname "$settings_file")"
	cat > "$settings_file" << EOF
[SETTINGS]
	<INTEGER>OPENGL:1
	<INTEGER>DX11:0
[/SETTINGS]
EOF
fi
'
APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}
$APP_PRERUN_NOD3D11"
APP_SETTINGS_PRERUN="${APP_SETTINGS_PRERUN:-}
$APP_PRERUN_NOD3D11"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
