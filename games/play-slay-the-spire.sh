#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Slay the Spire
# send your bug reports to contact@dotslashplay.it
###

script_version=20240218.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='slay-the-spire'
GAME_NAME='Slay the Spire'

ARCHIVE_BASE_0_NAME='slay_the_spire_2020_12_15_8735c9fe3cc2280b76aa3ec47c953352a7df1f65_43444.sh'
ARCHIVE_BASE_0_MD5='1989ebecb2434f0480b42c4e576dfc78'
ARCHIVE_BASE_0_SIZE='570000'
ARCHIVE_BASE_0_VERSION='2.2-gog43444'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/slay_the_spire'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
desktop-1.0.jar
mod-uploader.jar
mts-launcher.jar'
CONTENT_DOC_MAIN_FILES='
README.md'

USER_PERSISTENT_DIRECTORIES='
betaPreferences
runs
saves'
USER_PERSISTENT_FILES='
info.displayconfig'

APP_MAIN_TYPE='java'
APP_MAIN_EXE='desktop-1.0.jar'
APP_MAIN_ICON='../support/icon.png'

## Ensure smooth upgrades from packages generated with pre-20231107.1 game scripts.
PKG_MAIN_PROVIDES="${PKG_MAIN_PROVIDES:-}
slay-the-spire-bin
slay-the-spire-data"

PKG_MAIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXext.so.6
libXrandr.so.2
libXxf86vm.so.1'
## Prevent a crash on launch due to a failure to get the display dimensions.
##
## Exception in thread "LWJGL Application" java.lang.ExceptionInInitializerError
##         at com.badlogic.gdx.backends.lwjgl.LwjglGraphics.setVSync(LwjglGraphics.java:558)
##         at com.badlogic.gdx.backends.lwjgl.LwjglApplication$1.run(LwjglApplication.java:124)
## Caused by: java.lang.ArrayIndexOutOfBoundsException: Index 0 out of bounds for length 0
##         at org.lwjgl.opengl.LinuxDisplay.getAvailableDisplayModes(LinuxDisplay.java:954)
##         at org.lwjgl.opengl.LinuxDisplay.init(LinuxDisplay.java:738)
##         at org.lwjgl.opengl.Display.<clinit>(Display.java:138)
##         ... 2 more
PKG_MAIN_DEPENDENCIES_COMMANDS="${PKG_MAIN_DEPENDENCIES_COMMANDS:-}"'
xrandr'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
