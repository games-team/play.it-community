#!/bin/sh
# SPDX-FileCopyrightText: © 2017 Jacek Szafarkiewicz
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Anno 1503
# send your bug reports to contact@dotslashplay.it
###

script_version=20240522.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='anno-1503'
GAME_NAME='Anno 1503'

ARCHIVE_BASE_EN_0_NAME='setup_anno_1503_2.0.0.5.exe'
ARCHIVE_BASE_EN_0_MD5='a7b6aeb2c5f96e2fab12d1ef12f3b4af'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_SIZE='1600000'
ARCHIVE_BASE_EN_0_VERSION='3.0.43-gog2.0.0.5'
ARCHIVE_BASE_EN_0_URL='https://www.gog.com/game/anno_1503_ad'

ARCHIVE_BASE_FR_0_NAME='setup_anno_1503_french_2.0.0.5.exe'
ARCHIVE_BASE_FR_0_MD5='b2ebc98d0dc97350ede75098633bfbd8'
ARCHIVE_BASE_FR_0_TYPE='innosetup'
ARCHIVE_BASE_FR_0_SIZE='1500000'
ARCHIVE_BASE_FR_0_VERSION='3.0.43-gog2.0.0.5'
ARCHIVE_BASE_FR_0_URL='https://www.gog.com/game/anno_1503_ad'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
*.exe
*.dll'
CONTENT_GAME_L10N_FILES='
texte.dat
toolgfx/bodenschtze.inc
toolgfx/bodenschtze.tex
toolgfx/mainmedium8.zei
toolgfx/mainmedium.zei.old
toolgfx/mainsmall8.zei
toolgfx/pk_*.tex
toolgfx/startup.tex
videos/1001.bik
videos/1002.bik
videos/1004.bik
videos/1005.bik
videos/1006.bik
videos/1007.bik
videos/1009.bik
help
samples
scenes
speech'
CONTENT_GAME_DATA_FILES='
anlagen.dat
figuren*.dat
instanthelplinks.txt
videos/1000.bik
videos/1003.bik
videos/1008.bik
data
music
profiles
textures
toolgfx'
CONTENT_DOC_L10N_FILES='
manual.pdf'

USER_PERSISTENT_DIRECTORIES='
SaveGame'

## Prevent a failure to render the intro videos, and display problems on some resolutions.
WINE_VIRTUAL_DESKTOP='auto'
## Work around missing background music.
## cf. https://bugs.winehq.org/show_bug.cgi?id=16096
WINE_WINETRICKS_VERBS='directmusic'

APP_MAIN_EXE='1503startup.exe'

PACKAGES_LIST='
PKG_BIN
PKG_L10N
PKG_DATA'

PKG_L10N_ID="${GAME_ID}-l10n"
PKG_L10N_ID_EN="${PKG_L10N_ID}-en"
PKG_L10N_ID_FR="${PKG_L10N_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_ID"
PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_FR='French localization'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID $PKG_L10N_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
