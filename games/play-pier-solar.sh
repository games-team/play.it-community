#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 HS-157
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Pier Solar and the Great Architects
# send your bug reports to contact@dotslashplay.it
###

script_version=20231108.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='pier-solar'
GAME_NAME='Pier Solar and the Great Architects'

ARCHIVE_BASE_GOG_0_NAME='gog_pier_solar_and_the_great_architects_2.1.0.4.sh'
ARCHIVE_BASE_GOG_0_MD5='2de03fb6d69944e3f204d5ae45147a3e'
ARCHIVE_BASE_GOG_0_SIZE='2400000'
ARCHIVE_BASE_GOG_0_VERSION='1.3.2-gog2.1.0.4'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/pier_solar_and_the_great_architects'

ARCHIVE_BASE_HUMBLE_0_NAME='PierSolar_linux.zip'
ARCHIVE_BASE_HUMBLE_0_MD5='e5ceda3a75cab3fe9b1ad1cbaf2d4a1d'
ARCHIVE_BASE_HUMBLE_0_SIZE='2400000'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.3.2-humble1'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/pier-solar-and-the-great-architects-special-edition'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='PierSolar_linux'
CONTENT_GAME_BIN64_FILES='
pshd.linux64'
CONTENT_GAME_BIN32_FILES='
pshd.linux32'
CONTENT_GAME_DATA_FILES='
data
icon.png'
CONTENT_GAME_DOC_FILES='
README.txt'

APP_MAIN_EXE_BIN64='pshd.linux64'
APP_MAIN_EXE_BIN32='pshd.linux32'
APP_MAIN_ICON='icon.png'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
## TODO: Update the list of required native libraries.
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libGL.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN64'
# shellcheck disable=SC2119
launchers_write
set_current_package 'PKG_BIN32'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
