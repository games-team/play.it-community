#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Great Perhaps
# send your bug reports to contact@dotslashplay.it
###

script_version=20230805.2

GAME_ID='the-great-perhaps'
GAME_NAME='The Great Perhaps'

ARCHIVE_BASE_0='the_great_perhaps_1_32_14_31714.sh'
ARCHIVE_BASE_0_MD5='4f675ed32851ec790cd010aaba6dd4b7'
ARCHIVE_BASE_0_SIZE='4900000'
ARCHIVE_BASE_0_VERSION='1.32.14-gog31714'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/the_great_perhaps'

UNITY3D_NAME='TheGreatPerhaps'
UNITY3D_PLUGINS='
libfmodL.so
libfmod.so
libfmodstudioL.so
libfmodstudio.so
libgvraudio.so
libresonanceaudio.so
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN_FILES="
${UNITY3D_NAME}_Data/Mono
${UNITY3D_NAME}.x86_64"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data"

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

# Force the use of the Vulkan renderer,
# as most text strings fail to render using the default one (OpenGL?).

APP_MAIN_OPTIONS="${APP_MAIN_OPTIONS:-} -force-vulkan"

# Load common functions

target_version='2.24'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build package

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
