#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Zniw Adventure
# send your bug reports to contact@dotslashplay.it
###

script_version=20240624.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='zniw-adventure'
GAME_NAME='Zniw Adventure'

ARCHIVE_BASE_0_NAME='zniw_adventure_1_3_4_1l_gog_60121.sh'
ARCHIVE_BASE_0_MD5='37fb48f55d4e36d6692a8e13b7f07623'
ARCHIVE_BASE_0_SIZE='2300000'
ARCHIVE_BASE_0_VERSION='1.3.4.1l-gog60121'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/zniw_adventure'

CONTENT_PATH_DEFAULT='data/noarch/game/data'
CONTENT_GAME_MAIN_FILES='
acsetup.cfg
CTGAME.???
*.cdf
*.cvf
*.ogv
*.tra
*.vox'
CONTENT_DOC_MAIN_FILES='
licenses'
CONTENT_DOC0_MAIN_PATH='data/noarch/game'
CONTENT_DOC0_MAIN_FILES='
manualEn.pdf
manualPl.pdf
refCardLinEnglish.pdf
refCardLinPolish.pdf'
CONTENT_DOC1_MAIN_PATH='data/noarch/game/Walkthrough'
CONTENT_DOC1_MAIN_FILES='
SpoilerFreeWalkthrough.pdf'

APP_MAIN_SCUMMID='ags:zniwadventure'
APP_MAIN_ICON='../../support/icon.png'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
