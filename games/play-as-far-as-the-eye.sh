#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# As Far As The Eye
# send your bug reports to contact@dotslashplay.it
###

script_version=20231112.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='as-far-as-the-eye'
GAME_NAME='As Far As The Eye'

ARCHIVE_BASE_0_NAME='as_far_as_the_eye_1_1_1_44514.sh'
ARCHIVE_BASE_0_MD5='577c7175488d34cbcf374bbbce720921'
ARCHIVE_BASE_0_SIZE='890000'
ARCHIVE_BASE_0_VERSION='1.1.1-gog44514'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/as_far_as_the_eye'

UNITY3D_NAME='AFATE'
## No shipped plugin should be included.
UNITY3D_PLUGINS='
'

CONTENT_PATH_DEFAULT='data/noarch/game'

FAKE_HOME_PERSISTENT_DIRECTORIES='
My Games/A FATE'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
