#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Dawnmaker
# send your bug reports to contact@dotslashplay.it
###

script_version=20241026.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='dawnmaker'
GAME_NAME='Dawnmaker'

ARCHIVE_BASE_3_NAME='Dawnmaker-linux-x64-1.2.0.zip'
ARCHIVE_BASE_3_MD5='23b01fda61bae2c0606e89350eb3f5c1'
ARCHIVE_BASE_3_SIZE='514262'
ARCHIVE_BASE_3_VERSION='1.2.0-itch.2024.10.10'
ARCHIVE_BASE_3_URL='https://arpentor.itch.io/dawnmaker'

## Support for multiple archive sharing a same name is not available before ./play.it 2.31
## cf. https://forge.dotslashplay.it/play.it/play.it/-/issues/355
#ARCHIVE_BASE_2_NAME='Dawnmaker-linux-x64-1.2.0.zip'
#ARCHIVE_BASE_2_MD5='9cdf881907da6b58e77a5e41e7e557f1'
#ARCHIVE_BASE_2_SIZE='514261'
#ARCHIVE_BASE_2_VERSION='1.2.0-itch.2024.10.07'

ARCHIVE_BASE_1_NAME='Dawnmaker-linux-x64-1.1.0.zip'
ARCHIVE_BASE_1_MD5='2c0aa2792cd83b61ae1316cb00fc9304'
ARCHIVE_BASE_1_SIZE='506494'
ARCHIVE_BASE_1_VERSION='1.1.0-itch.2024.08.12'

ARCHIVE_BASE_0_NAME='Dawnmaker-linux-x64-1.0.0.zip'
ARCHIVE_BASE_0_MD5='8b2e787dafb8e92fb5714e907108f509'
ARCHIVE_BASE_0_SIZE='505449'
ARCHIVE_BASE_0_VERSION='1.0.0-itch.2024.07.31'

CONTENT_PATH_DEFAULT='Dawnmaker-linux-x64'
CONTENT_LIBS_BIN_FILES='
libEGL.so
libffmpeg.so
libGLESv2.so
libvk_swiftshader.so'
CONTENT_GAME_BIN_FILES='
chrome_crashpad_handler
chrome-sandbox
Dawnmaker'
CONTENT_GAME_DATA_FILES='
locales
resources
snapshot_blob.bin
v8_context_snapshot.bin
icudtl.dat
vk_swiftshader_icd.json
chrome_100_percent.pak
chrome_200_percent.pak
resources.pak
version'
CONTENT_DOC_DATA_FILES='
LICENSE
LICENSES.chromium.html'

APP_MAIN_EXE='Dawnmaker'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libasound.so.2
libatk-1.0.so.0
libatk-bridge-2.0.so.0
libatspi.so.0
libcairo.so.2
libc.so.6
libcups.so.2
libdbus-1.so.3
libdl.so.2
libdrm.so.2
libexpat.so.1
libgbm.so.1
libgcc_s.so.1
libgio-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-3.so.0
libm.so.6
libnspr4.so
libnss3.so
libnssutil3.so
libpango-1.0.so.0
libpthread.so.0
libsmime3.so
libvulkan.so.1
libX11.so.6
libxcb.so.1
libXcomposite.so.1
libXdamage.so.1
libXext.so.6
libXfixes.so.3
libxkbcommon.so.0
libXrandr.so.2'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

## Link some libraries in the game data path, as the engine fails to find them even when they are in LD_LIBRARY_PATH
path_libraries=$(set_current_package 'PKG_BIN';path_libraries)
path_data="$(package_path 'PKG_BIN')$(path_game_data)"
for library in \
	'libGLESv2.so' \
	'libEGL.so'
do
	ln --symbolic "${path_libraries}/${library}" "$path_data"
done

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
