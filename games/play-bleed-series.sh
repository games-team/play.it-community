#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Bleed series:
# - Bleed
# - Bleed 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20241124.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID_BLEED1='bleed-1'
GAME_NAME_BLEED1='Bleed'

GAME_ID_BLEED2='bleed-2'
GAME_NAME_BLEED2='Bleed 2'

# Archives

## Bleed 1

ARCHIVE_BASE_BLEED1_0_NAME='bleed-linux-05052016-bin'
ARCHIVE_BASE_BLEED1_0_MD5='a4522f679d7e7038e0085aaf1319f41f'
## This archive is a MojoSetup installer, but it is not based on Makeself.
ARCHIVE_BASE_BLEED1_0_EXTRACTOR='bsdtar'
ARCHIVE_BASE_BLEED1_0_SIZE='110000'
ARCHIVE_BASE_BLEED1_0_VERSION='1.7.0-itch.2016.05.05'
ARCHIVE_BASE_BLEED1_0_URL='https://bootdiskrevolution.itch.io/bleed'

## Bleed 2

ARCHIVE_BASE_BLEED2_0_NAME='bleed2-02112018-bin'
ARCHIVE_BASE_BLEED2_0_MD5='756324f1843c289719c6630a834e8f59'
## This archive is a MojoSetup installer, but it is not based on Makeself.
ARCHIVE_BASE_BLEED2_0_EXTRACTOR='bsdtar'
ARCHIVE_BASE_BLEED2_0_SIZE='350000'
ARCHIVE_BASE_BLEED2_0_VERSION='1.0-itch.2018.02.11'
ARCHIVE_BASE_BLEED2_0_URL='https://bootdiskrevolution.itch.io/bleed-2'

# Archives content

CONTENT_PATH_DEFAULT='data'
CONTENT_LIBS_LIBS64_RELATIVE_PATH='lib64'
CONTENT_LIBS_LIBS32_RELATIVE_PATH='lib'
CONTENT_GAME_MAIN_FILES='
Content
monoconfig
FNA.dll
FNA.dll.config'
CONTENT_DOC_MAIN_FILES='
Linux.README'

## Bleed 1

## Shipped OpenAL library must be included, or the game will crash on launch.
CONTENT_LIBS_LIBS_FILES_BLEED1='
libmojoshader.so
libopenal.so.1'
CONTENT_LIBS_LIBS64_FILES_BLEED1="$CONTENT_LIBS_LIBS_FILES_BLEED1"
CONTENT_LIBS_LIBS32_FILES_BLEED1="$CONTENT_LIBS_LIBS_FILES_BLEED1"
CONTENT_GAME_MAIN_FILES_BLEED1="$CONTENT_GAME_MAIN_FILES
Bleed.exe"

## Bleed 2

## Shipped OpenAL library must be included, or the game will crash on launch.
## Shipped Steamworks library must be included, or the game will crash on launch.
CONTENT_LIBS_LIBS_FILES_BLEED2='
libCSteamworks.so
libmojoshader.so
libopenal.so.1
libsteam_api.so'
CONTENT_LIBS_LIBS64_FILES_BLEED2="$CONTENT_LIBS_LIBS_FILES_BLEED2"
CONTENT_LIBS_LIBS32_FILES_BLEED2="$CONTENT_LIBS_LIBS_FILES_BLEED2"
## The game will crash on launch if the Steamworks library is omitted.
CONTENT_GAME_MAIN_FILES_BLEED2="$CONTENT_GAME_MAIN_FILES
Bleed2.exe
Steamworks.NET.dll"


APP_MAIN_EXE_BLEED1='Bleed.exe'
APP_MAIN_EXE_BLEED2='Bleed2.exe'
APP_MAIN_ICON_BLEED1='Bleed.bmp'
APP_MAIN_ICON_BLEED2='Bleed 2.bmp'

PACKAGES_LIST='
PKG_MAIN
PKG_LIBS64
PKG_LIBS32'

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_LIBS'
PKG_MAIN_DEPENDENCIES_LIBRARIES='
libopenal.so.1
libSDL2-2.0.so.0'
PKG_MAIN_DEPENDENCIES_MONO_LIBRARIES='
mscorlib.dll
Mono.Posix.dll
Mono.Security.dll
System.dll
System.Configuration.dll
System.Core.dll
System.Data.dll
System.Drawing.dll
System.Security.dll
System.Xml.dll'

PKG_LIBS_ID_BLEED1="${GAME_ID_BLEED1}-libs"
PKG_LIBS_ID_BLEED2="${GAME_ID_BLEED2}-libs"
PKG_LIBS64_ID_BLEED1="$PKG_LIBS_ID_BLEED1"
PKG_LIBS64_ID_BLEED2="$PKG_LIBS_ID_BLEED2"
PKG_LIBS32_ID_BLEED1="$PKG_LIBS_ID_BLEED1"
PKG_LIBS32_ID_BLEED2="$PKG_LIBS_ID_BLEED2"
PKG_LIBS64_ARCH='64'
PKG_LIBS32_ARCH='32'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_MAIN'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_MAIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
