#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Etherlords
# send your bug reports to contact@dotslashplay.it
###

script_version=20241203.2

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='etherlords-1'
GAME_NAME='Etherlords'

ARCHIVE_BASE_0_NAME='setup_etherlords_2.0.0.10.exe'
ARCHIVE_BASE_0_MD5='13f406358ed79d49754ef3a48de6c2db'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='1458432'
ARCHIVE_BASE_0_VERSION='1.07-gog2.0.0.10'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/etherlords'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
mp3dec.asi
2dintmmx.dll
3dfpfpu.dll
binkw32.dll
dsetup.dll
mss32.dll
etherlords.exe
combatcamera.ini
settings.ini'
CONTENT_GAME_DATA_FILES='
campaigns
duel
maps
resources
halloffame.hl
ar_text.res'
CONTENT_DOC_DATA_FILES='
manual.pdf
readme.txt
readme 1.07.txt'

USER_PERSISTENT_DIRECTORIES='
campaign
duel
save'
USER_PERSISTENT_FILES='
halloffame.hl
*.ini'

## The game does not seem to support resolutions higher than 1024×768
WINE_VIRTUAL_DESKTOP='1024x768'

APP_MAIN_EXE='etherlords.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
