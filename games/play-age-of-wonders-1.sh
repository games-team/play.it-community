#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 BetaRays
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Age of Wonders 1
# send your bug reports to contact@dotslashplay.it
###

script_version=20240516.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='age-of-wonders-1'
GAME_NAME='Age of Wonders'

ARCHIVE_BASE_1_NAME='setup_age_of_wonders_1.36.0053_(22161).exe'
ARCHIVE_BASE_1_MD5='e9c045b1a915d0af6f838ec0ad27d910'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='320000'
ARCHIVE_BASE_1_VERSION='1.36.0053-gog22161'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/age_of_wonders'

ARCHIVE_BASE_0='setup_age_of_wonders_2.0.0.13.exe'
ARCHIVE_BASE_0_MD5='9ee2ccc5223c41306cf6695fc09a5634'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='330000'
ARCHIVE_BASE_0_VERSION='1.36.0053-gog2.0.0.13'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_BIN_FILES='
aow.exe
aowed.exe'
CONTENT_GAME_DATA_FILES='
*.wav
*.dpl
int
dict
edimages
images
release
scenario
sfx
songs
tcmaps'
CONTENT_DOC_DATA_FILES='
readme.txt
quickstart.pdf'

USER_PERSISTENT_DIRECTORIES='
emailin
emailout
save
user'

## "icodecs" winetricks verb is required for correct intro movie playback.
WINE_WINETRICKS_VERBS='icodecs'

APP_MAIN_EXE='aow.exe'

APP_EDITOR_ID="${GAME_ID}-editor"
APP_EDITOR_EXE='aowed.exe'
APP_EDITOR_NAME="$GAME_NAME - editor"

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
