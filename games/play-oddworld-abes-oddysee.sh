#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Oddworld: Abe's Oddysee
# send your bug reports to contact@dotslashplay.it
###

script_version=20240707.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='oddworld-abes-oddysee'
GAME_NAME='Oddworld: Abeʼs Oddysee'

ARCHIVE_BASE_1_NAME='setup_abes_oddysee_1.0_(19071).exe'
ARCHIVE_BASE_1_MD5='1c60cd9f43cc6392fc7c5185580eb048'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='650000'
ARCHIVE_BASE_1_VERSION='1.0-gog19071'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/oddworld_abes_oddysee'

ARCHIVE_BASE_0_NAME='setup_abes_oddysee_2.0.0.4.exe'
ARCHIVE_BASE_0_MD5='c22a44d208e524dc2760ea6ce57829d5'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='660000'
ARCHIVE_BASE_0_VERSION='2.1-gog2.0.0.4'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
abewin.exe'
CONTENT_GAME_DATA_FILES='
*.ddv
*.lvl'
CONTENT_DOC_DATA_FILES='
*.txt
*.pdf'

USER_PERSISTENT_DIRECTORIES='
save'
USER_PERSISTENT_FILES='
*.ini'

APP_MAIN_EXE='abewin.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
