#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Litil Divil
# send your bug reports to contact@dotslashplay.it
###

script_version=20231109.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='litil-divil'
GAME_NAME='Litil Divil'

ARCHIVE_BASE_1_NAME='gog_litil_divil_2.0.0.22.sh'
ARCHIVE_BASE_1_MD5='89a1a0cedbf13d8e6aed285780b69def'
ARCHIVE_BASE_1_SIZE='45000'
ARCHIVE_BASE_1_VERSION='1.0-gog2.0.0.22'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/litil_divil'

ARCHIVE_BASE_0_NAME='gog_litil_divil_2.0.0.21.sh'
ARCHIVE_BASE_0_MD5='1258be406cb4b40c912c4846df2ac92b'
ARCHIVE_BASE_0_TYPE='mojosetup'
ARCHIVE_BASE_0_SIZE='44000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.0.0.21'

CONTENT_PATH_DEFAULT='data/noarch/data'
CONTENT_GAME_MAIN_FILES='
data
gfx
*.cfg
*.exe'
CONTENT_DOC_MAIN_FILES='
config.doc'
CONTENT_DOC0_MAIN_PATH="${CONTENT_PATH_DEFAULT}/../docs"
CONTENT_DOC0_MAIN_FILES='
*.txt
*.pdf'

USER_PERSISTENT_FILES='
divils.cfg'

GAME_IMAGE='data'
GAME_IMAGE_TYPE='cdrom'

APP_MAIN_EXE='data/divil.exe'
APP_MAIN_OPTIONS='c:'
APP_MAIN_ICON='../support/icon.png'
## Run the game binary from the CD-ROM directory.
APP_MAIN_PRERUN='# Run the game binary from the CD-ROM directory
APP_EXE=$(basename "$APP_EXE")
'
APP_MAIN_DOSBOX_PRERUN='
d:
'

APP_CONFIG_ID="${GAME_ID}-config"
APP_CONFIG_NAME="$GAME_NAME - configuration"
APP_CONFIG_CAT='Settings'
APP_CONFIG_EXE='config.exe'
APP_CONFIG_ICON='../support/icon.png'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Convert all file paths to lowercase.
	tolower .
)

# Include game data

# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
