#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2018 BetaRays
# SPDX-FileCopyrightText: © 2021 dany_wilde
set -o errexit

###
# Fallout classic games:
# - Fallout 1
# - Fallout 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20240422.3

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID_FALLOUT1='fallout-1'
GAME_NAME_FALLOUT1='Fallout'

GAME_ID_FALLOUT2='fallout-2'
GAME_NAME_FALLOUT2='Fallout 2'

# Archives

## Fallout 1

ARCHIVE_BASE_FALLOUT1_EN_1_NAME='setup_fallout_1.2_(27130).exe'
ARCHIVE_BASE_FALLOUT1_EN_1_MD5='2cd1bb09f241c286498ea834480852ec'
ARCHIVE_BASE_FALLOUT1_EN_1_TYPE='innosetup'
ARCHIVE_BASE_FALLOUT1_EN_1_PART1_NAME='setup_fallout_1.2_(27130)-1.bin'
ARCHIVE_BASE_FALLOUT1_EN_1_PART1_MD5='b9a0a59bc1426df4cc9588fdd5a8d736'
ARCHIVE_BASE_FALLOUT1_EN_1_SIZE='600000'
ARCHIVE_BASE_FALLOUT1_EN_1_VERSION='1.2-gog27130'
ARCHIVE_BASE_FALLOUT1_EN_1_URL='https://www.gog.com/game/fallout'

ARCHIVE_BASE_FALLOUT1_FR_1_NAME='setup_fallout_1.2_(french)_(27130).exe'
ARCHIVE_BASE_FALLOUT1_FR_1_MD5='2c0d7a347a903bb52ed1d70305038e9c'
ARCHIVE_BASE_FALLOUT1_FR_1_TYPE='innosetup'
ARCHIVE_BASE_FALLOUT1_FR_1_PART1_NAME='setup_fallout_1.2_(french)_(27130)-1.bin'
ARCHIVE_BASE_FALLOUT1_FR_1_PART1_MD5='7db5f755168b89cc38b6e090130b0e1a'
ARCHIVE_BASE_FALLOUT1_FR_1_SIZE='600000'
ARCHIVE_BASE_FALLOUT1_FR_1_VERSION='1.2-gog27130'
ARCHIVE_BASE_FALLOUT1_FR_1_URL='https://www.gog.com/game/fallout'

ARCHIVE_BASE_FALLOUT1_EN_0_NAME='setup_fallout_2.1.0.18.exe'
ARCHIVE_BASE_FALLOUT1_EN_0_MD5='47b7b3c059d92c0fd6db5881635277ea'
ARCHIVE_BASE_FALLOUT1_EN_0_TYPE='innosetup'
ARCHIVE_BASE_FALLOUT1_EN_0_SIZE='600000'
ARCHIVE_BASE_FALLOUT1_EN_0_VERSION='1.2-gog2.1.0.18'

ARCHIVE_BASE_FALLOUT1_FR_0_NAME='setup_fallout_french_2.1.0.18.exe'
ARCHIVE_BASE_FALLOUT1_FR_0_MD5='12ba5bb0489b5bafb777c8d07717b020'
ARCHIVE_BASE_FALLOUT1_FR_0_TYPE='innosetup'
ARCHIVE_BASE_FALLOUT1_FR_0_SIZE='600000'
ARCHIVE_BASE_FALLOUT1_FR_0_VERSION='1.2-gog2.1.0.18'

## Fallout 2

ARCHIVE_BASE_FALLOUT2_EN_1_NAME='setup_fallout2_2.1.0.18.exe'
ARCHIVE_BASE_FALLOUT2_EN_1_MD5='b20e9a133c23bf308b8460272fd32d2b'
ARCHIVE_BASE_FALLOUT2_EN_1_TYPE='innosetup'
ARCHIVE_BASE_FALLOUT2_EN_1_SIZE='740000'
ARCHIVE_BASE_FALLOUT2_EN_1_VERSION='1.3-gog2.1.0.18'
ARCHIVE_BASE_FALLOUT2_EN_1_URL='https://www.gog.com/game/fallout_2'

ARCHIVE_BASE_FALLOUT2_EN_0_NAME='setup_fallout2_2.1.0.17.exe'
ARCHIVE_BASE_FALLOUT2_EN_0_MD5='b40a8f2e1ff9216e25b8f09577c27f33'
ARCHIVE_BASE_FALLOUT2_EN_0_TYPE='innosetup'
ARCHIVE_BASE_FALLOUT2_EN_0_SIZE='740000'
ARCHIVE_BASE_FALLOUT2_EN_0_VERSION='1.3-gog2.1.0.17'

ARCHIVE_BASE_FALLOUT2_FR_0_NAME='setup_fallout2_french_2.1.0.17.exe'
ARCHIVE_BASE_FALLOUT2_FR_0_MD5='7df6f834b480873bea2f8593254b1960'
ARCHIVE_BASE_FALLOUT2_FR_0_TYPE='innosetup'
ARCHIVE_BASE_FALLOUT2_FR_0_SIZE='740000'
ARCHIVE_BASE_FALLOUT2_FR_0_VERSION='1.3-gog2.1.0.17'
ARCHIVE_BASE_FALLOUT2_FR_0_URL='https://www.gog.com/game/fallout_2'

# Archives content

## Fallout 1

CONTENT_PATH_DEFAULT_FALLOUT1='.'
CONTENT_PATH_DEFAULT_FALLOUT1_EN_0='app'
CONTENT_PATH_DEFAULT_FALLOUT1_FR_0='app'
CONTENT_GAME_BIN_FILES_FALLOUT1='
falloutw.exe
f1_res.dll
f1_res_config.exe'
CONTENT_GAME_L10N_PATH_FALLOUT1='__support/app'
CONTENT_GAME_L10N_PATH_FALLOUT1_EN_0='app'
CONTENT_GAME_L10N_PATH_FALLOUT1_FR_0='app'
CONTENT_GAME_L10N_FILES_FALLOUT1='
fallout.cfg
f1_res.ini'
CONTENT_GAME_DATA_FILES_FALLOUT1='
critter.dat
master.dat
data
extras
fallout.ico'
CONTENT_DOC_L10N_FILES_FALLOUT1='
readme.txt
manual.pdf'
CONTENT_DOC_DATA_FILES_FALLOUT1='
refcard.pdf
readme.rtf
f1_res_readme.rtf'

## Fallout 2

CONTENT_PATH_DEFAULT_FALLOUT2='app'
CONTENT_GAME_BIN_FILES_FALLOUT2='
fallout2.exe
f2_res.dll
f2_res_config.exe
f2_res.ini'
CONTENT_GAME_L10N_FILES_FALLOUT2='
critter.dat
master.dat
translations
fallout2.cfg'
CONTENT_GAME_DATA_FILES_FALLOUT2='
data
extras
fallout2.ico
sound'
CONTENT_DOC_L10N_FILES_FALLOUT2='
manual.pdf'
CONTENT_DOC_DATA_FILES_FALLOUT2='
refcard.pdf
readme.rtf
f2_res_readme.rtf
faq.txt
licence.txt
f2_res_change.log'

# Launchers

USER_PERSISTENT_FILES='
*.cfg
*.ini'
USER_PERSISTENT_DIRECTORIES='
data/savegame'

## Disable WINE csmt to avoid performance issues
WINE_WINETRICKS_VERBS='csmt=off'

APP_RES_CAT='Settings'

## Fallout 1

## The resolution tweaking tool expects write access to the game binary.
USER_PERSISTENT_FILES_FALLOUT1="${USER_PERSISTENT_FILES:-}
falloutw.exe"

APP_MAIN_EXE_FALLOUT1='falloutw.exe'
APP_MAIN_ICON_FALLOUT1='fallout.ico'

APP_RES_ID_FALLOUT1="${GAME_ID_FALLOUT1}-resolution"
APP_RES_NAME_FALLOUT1="$GAME_NAME_FALLOUT1 - resolution"
APP_RES_EXE_FALLOUT1='f1_res_config.exe'

## Fallout 2

## The resolution tweaking tool expects write access to the game binary.
USER_PERSISTENT_FILES_FALLOUT2="${USER_PERSISTENT_FILES:-}
fallout2.exe"

APP_MAIN_EXE_FALLOUT2='fallout2.exe'
APP_MAIN_ICON_FALLOUT2='fallout2.ico'

APP_RES_ID_FALLOUT2="${GAME_ID_FALLOUT2}-resolution"
APP_RES_NAME_FALLOUT2="$GAME_NAME_FALLOUT2 - resolution"
APP_RES_EXE_FALLOUT2='f2_res_config.exe'

# Packages

PACKAGES_LIST='
PKG_BIN
PKG_L10N
PKG_DATA'

PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_FR='French localization'

PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'

## Fallout 1

PKG_L10N_ID_FALLOUT1="${GAME_ID_FALLOUT1}-l10n"
PKG_L10N_ID_FALLOUT1_EN="${PKG_L10N_ID_FALLOUT1}-en"
PKG_L10N_ID_FALLOUT1_FR="${PKG_L10N_ID_FALLOUT1}-fr"
PKG_L10N_PROVIDES_FALLOUT1="
$PKG_L10N_ID_FALLOUT1"
## Easier upgrade from packages generated with pre-20190116.1 scripts
PKG_L10N_PROVIDES_FALLOUT1="${PKG_L10N_PROVIDES_FALLOUT1}
fallout-l10n"

PKG_DATA_ID_FALLOUT1="${GAME_ID_FALLOUT1}-data"
## Easier upgrade from packages generated with pre-20190116.1 scripts
PKG_DATA_PROVIDES_FALLOUT1="${PKG_DATA_PROVIDES_FALLOUT1}
fallout-data"

PKG_BIN_DEPS_FALLOUT1="$PKG_L10N_ID_FALLOUT1 $PKG_DATA_ID_FALLOUT1"
## Easier upgrade from packages generated with pre-20190116.1 scripts
PKG_BIN_PROVIDES_FALLOUT1="${PKG_BIN_PROVIDES_FALLOUT1}
fallout"

## Fallout 2

PKG_L10N_ID_FALLOUT2="${GAME_ID_FALLOUT2}-l10n"
PKG_L10N_ID_FALLOUT2_EN="${PKG_L10N_ID_FALLOUT2}-en"
PKG_L10N_ID_FALLOUT2_FR="${PKG_L10N_ID_FALLOUT2}-fr"
PKG_L10N_PROVIDES_FALLOUT2="
$PKG_L10N_ID_FALLOUT2"

PKG_DATA_ID_FALLOUT2="${GAME_ID_FALLOUT2}-data"

PKG_BIN_DEPS_FALLOUT2="$PKG_L10N_ID_FALLOUT2 $PKG_DATA_ID_FALLOUT2"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

## Drop the easter egg icon.
case "$(current_archive)" in
	('ARCHIVE_BASE_FALLOUT1_'*)
		icon_extract_png_from_ico() {
			local icon destinaton
			icon="$1"
			destination="$2"

			icon_convert_to_png "$icon" "$destination"

			rm --force "${destination}/fallout-4.png"
		}
	;;
	('ARCHIVE_BASE_FALLOUT2_'*)
		icon_extract_png_from_ico() {
			local icon destinaton
			icon="$1"
			destination="$2"

			icon_convert_to_png "$icon" "$destination"

			rm --force "${destination}/fallout2-4.png"
		}
	;;
esac
set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
