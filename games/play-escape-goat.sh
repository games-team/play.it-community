#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Escape Goat series:
# - Escape Goat 1
# - Escape Goat 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20240728.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID_EPISODE1='escape-goat-1'
GAME_ID_EPISODE2='escape-goat-2'

GAME_NAME_EPISODE1='Escape Goat'
GAME_NAME_EPISODE2='Escape Goat 2'

# Archives

## Escape Goat

ARCHIVE_BASE_EPISODE1_0_NAME='gog_escape_goat_2.0.0.8.sh'
ARCHIVE_BASE_EPISODE1_0_MD5='772eda5c48d59b7528a9d85b3f1f84e3'
ARCHIVE_BASE_EPISODE1_0_SIZE='110000'
ARCHIVE_BASE_EPISODE1_0_VERSION='1.0.6-gog2.0.0.8'
ARCHIVE_BASE_EPISODE1_0_URL='https://www.gog.com/game/escape_goat'

## Escapt Goat 2

ARCHIVE_BASE_EPISODE2_0_NAME='gog_escape_goat_2_2.0.0.11.sh'
ARCHIVE_BASE_EPISODE2_0_MD5='50e77abfe8737c6d0e1e37e8ad2460cc'
ARCHIVE_BASE_EPISODE2_0_SIZE='260000'
ARCHIVE_BASE_EPISODE2_0_VERSION='1.1.0-gog2.0.0.11'
ARCHIVE_BASE_EPISODE2_0_URL='https://www.gog.com/game/escape_goat_2'


CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES_EPISODE1='
Content
fr
EscapeGoat.exe
Escape Goat.bmp
Common.dll
Lidgren.Network.dll
MonoGame.Framework.dll
Physics.dll
SDL2#.dll
SDL2#.dll.config'
CONTENT_GAME_MAIN_FILES_EPISODE2='
Content
fr
EscapeGoat2.exe
Escape Goat 2.bmp
Common.dll
EG2.ICSharpCode.SharpZipLib.dll
EG2.Newtonsoft.Json.dll
EscapeGoat2.resources.dll
Illuminant.dll
MonoGame.Framework.dll
Physics.dll
SDL2-CS.dll
SDL2-CS.dll.config
Squared.*.dll'
CONTENT_DOC_MAIN_FILES='
Linux.README
ReadMe.txt'

APP_MAIN_EXE_EPISODE1='EscapeGoat.exe'
APP_MAIN_EXE_EPISODE2='EscapeGoat2.exe'
APP_MAIN_ICON_EPISODE1='Escape Goat.bmp'
APP_MAIN_ICON_EPISODE2='Escape Goat 2.bmp'

PKG_MAIN_DEPENDENCIES_LIBRARIES='
libopenal.so.1
libSDL2-2.0.so.0'
PKG_MAIN_DEPENDENCIES_MONO_LIBRARIES_EPISODE1='
mscorlib.dll
Mono.Posix.dll
Mono.Security.dll
System.dll
System.Configuration.dll
System.Core.dll
System.Data.dll
System.Drawing.dll
System.Runtime.Serialization.dll
System.Security.dll
System.Xml.dll'
PKG_MAIN_DEPENDENCIES_MONO_LIBRARIES_EPISODE2='
mscorlib.dll
I18N.dll
I18N.West.dll
Mono.Posix.dll
Mono.Security.dll
System.dll
System.Configuration.dll
System.Core.dll
System.Data.dll
System.Drawing.dll
System.Runtime.Serialization.dll
System.Security.dll
System.Xml.dll
System.Xml.Linq.dll'
## Ensure easy upgrades from packages generated from pre-20240603.1 game scripts.
PKG_MAIN_PROVIDES_EPISODE1="${PKG_MAIN_PROVIDES_EPISODE1:-}
escape-goat
escapt-goat-data"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
