#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Elder Scrolls 1: Arena
# send your bug reports to contact@dotslashplay.it
###

script_version=20240505.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='the-elder-scrolls-1-arena'
GAME_NAME='The Elder Scrolls: Arena'

ARCHIVE_BASE_1_NAME='setup_the_elder_scrolls_arena_1.07_(28043).exe'
ARCHIVE_BASE_1_MD5='53a12a1cc3955776f06fba5906cdc6fe'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='120000'
ARCHIVE_BASE_1_VERSION='1.07-gog28043'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/the_elder_scrolls_iii_morrowind_goty_edition'

ARCHIVE_BASE_0_NAME='setup_tes_arena_2.0.0.5.exe'
ARCHIVE_BASE_0_MD5='ca5a894aa852f9dbb3ede787e51ec828'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='130000'
ARCHIVE_BASE_0_VERSION='1.07-gog2.0.0.5'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_MAIN_FILES='
*.cfg
*.exe
*.inf
*.ini
*.65
*.ad
*.adv
*.bak
*.bnk
*.bsa
*.cel
*.cif
*.clr
*.col
*.cpy
*.dat
*.flc
*.gld
*.img
*.lgt
*.lst
*.me
*.mif
*.mnu
*.ntz
*.opl
*.rci
*.txt
*.voc
*.xfm
cityintr
citytxt
extra
speech'
CONTENT_GAME0_MAIN_PATH='__support'
CONTENT_GAME0_MAIN_PATH_0='app/__support'
CONTENT_GAME0_MAIN_FILES='
save'
CONTENT_DOC_MAIN_FILES='
*.pdf
readme.txt'

GAME_IMAGE='.'
GAME_IMAGE_TYPE='cdrom'

USER_PERSISTENT_DIRECTORIES='
save
arena_cd'

APP_MAIN_EXE='acd.exe'
APP_MAIN_OPTIONS='-Ssbpdig.adv -IOS220 -IRQS7 -DMAS1 -Mgenmidi.adv -IOM330 -IRQM2 -DMAM1'
APP_MAIN_ICON='app/goggame-1435828982.ico'
APP_MAIN_DOSBOX_PRERUN='config -set cpu cycles=fixed 20000
d:'
## The type can not be omitted, because the binary is actually on the CD-ROM image.
APP_MAIN_TYPE='dosbox'

# Easier upgrade from packages generated with pre-20190302.3 scripts
PKG_MAIN_PROVIDES="${PKG_MAIN_PROVIDES:-}
the-elder-scrolls-1-arena-data"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

## Work around the binary presence check,
## it is actually included in the CD-ROM image.
launcher_target_presence_check() { return 0; }
launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
