#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Long Live the Queen
# send your bug reports to contact@dotslashplay.it
###

script_version=20231114.3

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='long-live-the-queen'
GAME_NAME='Long Live the Queen'

ARCHIVE_BASE_0_NAME='gog_long_live_the_queen_2.4.0.11.sh'
ARCHIVE_BASE_0_MD5='654ccbbdde2071c4f13b5f80378582c2'
ARCHIVE_BASE_0_SIZE='63000'
ARCHIVE_BASE_0_VERSION='1.3.23.4-gog2.4.0.11'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/long_live_the_queen'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN_PATH="${CONTENT_PATH_DEFAULT}/lib/linux-x86/lib"
CONTENT_LIBS_BIN_FILES='
libavcodec.so.52
libavformat.so.52
libavutil.so.50
libGLEW.so.1.5
libpython2.6.so.1.0
libswscale.so.0'
CONTENT_GAME_BIN_FILES='
lib/linux-x86/lib/python2.6
lib/linux-x86/python.real'
CONTENT_GAME_DATA_FILES='
common
game
renpy
LongLiveTheQueen.py'
CONTENT_FONTS_PATH="${CONTENT_PATH_DEFAULT}/game"
CONTENT_FONTS_FILES='
DejaVuSans-Oblique.ttf'
CONTENT_DOC_DATA_FILES='
LICENSE.txt'

APP_MAIN_EXE='lib/linux-x86/python.real'
APP_MAIN_OPTIONS='-OO LongLiveTheQueen.py'
APP_MAIN_ICON='../support/icon.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libEGL.so.1
libfreetype.so.6
libfribidi.so.0
libGL.so.1
libGLU.so.1
libm.so.6
libpthread.so.0
libSDL-1.2.so.0
libSDL_ttf-2.0.so.0
libutil.so.1
libX11.so.6
libXext.so.6
libXi.so.6
libXmu.so.6
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Link fonts to the game directory, or the engine would fail to find them.

PATH_FONTS="$(option_value 'prefix')/share/fonts/truetype/$(game_id)"
game_data_path="$(package_path 'PKG_DATA')$(path_game_data)/game"
mkdir --parents "$game_data_path"
ln --symbolic \
	"${PATH_FONTS}/DejaVuSans-Oblique.ttf" \
	"$game_data_path"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Add required execution permissions
	chmod 755 'lib/linux-x86/python.real'
)

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion 'FONTS' 'PKG_DATA' "$PATH_FONTS"
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
