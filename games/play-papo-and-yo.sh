#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Papo and Yo
# send your bug reports to contact@dotslashplay.it
###

script_version=20221210.2

GAME_ID='papo-and-yo'
GAME_NAME='Papo and Yo'

ARCHIVE_BASE_0='PapoYo_linux_1389070953.sh'
ARCHIVE_BASE_0_MD5='d8222b87222f4eb05025584bf923da41'
ARCHIVE_BASE_0_TYPE='mojosetup'
ARCHIVE_BASE_0_SIZE='2000000'
ARCHIVE_BASE_0_VERSION='2014010601-humble1'
ARCHIVE_BASE_0_URL='https://www.humblebundle.com/store/papo-yo'

CONTENT_PATH_DEFAULT='data/noarch'
CONTENT_GAME_BIN_PATH='data/x86'
CONTENT_GAME_BIN_FILES='
Binaries/Linux/steam_appid.txt
Binaries/Linux/PYGame-Linux'
CONTENT_LIBS_BIN_PATH='data/x86/Binaries/Linux/lib'
CONTENT_LIBS_BIN_FILES='
libsteam_api.so
libPhysXCooking.so
libPhysXCore.so
libtcmalloc.so.0'
CONTENT_GAME_DATA_FILES='
Engine
PYGame
PapoYoIcon.bmp
PapoYoIcon.png'
CONTENT_DOC_DATA_FILES='
about.html
BuildVersion.txt
README.linux
UpdateLog.txt'

APP_MAIN_EXE='Binaries/Linux/PYGame-Linux'
APP_MAIN_ICON='PapoYoIcon.png'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
ld-linux.so.2
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libstdc++.so.6'

# Run the game binary from its parent directory

APP_MAIN_PRERUN="$APP_MAIN_PRERUN"'
# Run the game binary from its parent directory
cd "$(dirname "$APP_EXE")"
APP_EXE=$(basename "$APP_EXE")'

# Load common functions

target_version='2.19'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
