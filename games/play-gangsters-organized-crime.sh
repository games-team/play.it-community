#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Gangsters: Organized Crime
# send your bug reports to contact@dotslashplay.it
###

script_version=20240610.2

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='gangsters-organized-crime'
GAME_NAME='Gangsters: Organized Crime'

ARCHIVE_BASE_0_NAME='setup_gangsters_2.0.0.15.exe'
ARCHIVE_BASE_0_MD5='794561f7b449e3bfce82611f11f8bc9b'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='330000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.0.0.15'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/gangsters_organized_crime'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
gangsters.exe
*.dll
*.ini'
CONTENT_GAME_DATA_FILES='
data
graphics
help
multiplayer scenarios
music
samples
scenarios
sound
tutorials
video
security.key'
CONTENT_DOC_DATA_FILES='
eula
*.pdf
*.txt
*.wri'

USER_PERSISTENT_DIRECTORIES='
multiplayer saved games
saved games'

APP_MAIN_EXE='gangsters.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
application/x-id3'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
