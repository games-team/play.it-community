#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Farabel
# send your bug reports to contact@dotslashplay.it
###

script_version=20231115.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='farabel'
GAME_NAME='Farabel'

ARCHIVE_BASE_1_NAME='Farabel_Linux_1.2.1.zip'
ARCHIVE_BASE_1_MD5='383d59983be2cedd151006d8f932e5ff'
ARCHIVE_BASE_1_SIZE='430000'
ARCHIVE_BASE_1_VERSION='1.2.1-humble181130'
ARCHIVE_BASE_1_URL='https://www.humblebundle.com/store/farabel'

ARCHIVE_BASE_0_NAME='Farabel1.2Linux.zip'
ARCHIVE_BASE_0_MD5='f2bd82b7a9578e8d7f084286cdb5943f'
ARCHIVE_BASE_0_SIZE='430000'
ARCHIVE_BASE_0_VERSION='1.2-humble181031'

UNITY3D_NAME='Farabel'
UNITY3D_PLUGINS='
ScreenSelector.so'

CONTENT_PATH_DEFAULT='Farabel'
CONTENT_PATH_DEFAULT_0='.'

FAKE_HOME_PERSISTENT_DIRECTORIES='
Frogames/Farabel'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN32'
# shellcheck disable=SC2119
launchers_write
set_current_package 'PKG_BIN64'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
