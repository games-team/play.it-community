#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Nowhere Prophet
# send your bug reports to contact@dotslashplay.it
###

script_version=20230129.1

GAME_ID='nowhere-prophet'
GAME_NAME='Nowhere Prophet'

UNITY3D_NAME='NowhereProphet'

ARCHIVE_BASE_0='nowhere_prophet_1_07_013_ichcha_49715.sh'
ARCHIVE_BASE_0_MD5='a88b8aaf466ffffb0f9515b5e53d2366'
ARCHIVE_BASE_0_TYPE='mojosetup'
ARCHIVE_BASE_0_SIZE='660000'
ARCHIVE_BASE_0_VERSION='1.07.013-gog49715'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/nowhere_prophet'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN32_PATH="${CONTENT_PATH_DEFAULT}/${UNITY3D_NAME}_Data/Plugins/x86"
CONTENT_LIBS_BIN32_FILES='
ScreenSelector.so'
CONTENT_GAME_BIN32_FILES="
${UNITY3D_NAME}.x86
${UNITY3D_NAME}_Data/MonoBleedingEdge/x86"
CONTENT_LIBS_BIN64_PATH="${CONTENT_PATH_DEFAULT}/${UNITY3D_NAME}_Data/Plugins/x86_64"
CONTENT_LIBS_BIN64_FILES='
ScreenSelector.so'
CONTENT_GAME_BIN64_FILES="
${UNITY3D_NAME}.x86_64
${UNITY3D_NAME}_Data/MonoBleedingEdge/x86_64"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data/Managed
${UNITY3D_NAME}_Data/MonoBleedingEdge/etc
${UNITY3D_NAME}_Data/Resources
${UNITY3D_NAME}_Data/StreamingAssets
${UNITY3D_NAME}_Data/boot.config
${UNITY3D_NAME}_Data/globalgamemanagers
${UNITY3D_NAME}_Data/ScreenSelector.png
${UNITY3D_NAME}_Data/level?
${UNITY3D_NAME}_Data/*.assets
${UNITY3D_NAME}_Data/*.assets.resS
${UNITY3D_NAME}_Data/*.resource"

PACKAGES_LIST='PKG_BIN32 PKG_BIN64 PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN32_ARCH='32'
PKG_BIN32_DEPS="$PKG_DATA_ID"
PKG_BIN32_DEPENDENCIES_LIBRARIES='
ld-linux-x86-64.so.2
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

PKG_BIN64_ARCH='64'
PKG_BIN64_DEPS="$PKG_BIN32_DEPS"
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN32_DEPENDENCIES_LIBRARIES"

# Load common functions

target_version='2.21'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

for PKG in 'PKG_BIN32' 'PKG_BIN64'; do
	# shellcheck disable=SC2119
	launchers_write
done

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
