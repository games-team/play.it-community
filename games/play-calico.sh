#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Calico
# send your bug reports to contact@dotslashplay.it
###

script_version=20240807.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

## The command `calico` is already provided by an IRC bouncer:
## https://git.causal.agency/pounce/about/calico.1
GAME_ID='calico-game'
GAME_NAME='Calico'

ARCHIVE_BASE_0_NAME='setup_calico_1.0.5_(45142).exe'
ARCHIVE_BASE_0_MD5='f2b7aa8e3944f26a02d1ea19afa8e4f0'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='930000'
ARCHIVE_BASE_0_VERSION='1.0.5-gog45142'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/calico'

UNITY3D_NAME='calico'

CONTENT_PATH_DEFAULT='.'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Peachy Keen Games/Calico'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
