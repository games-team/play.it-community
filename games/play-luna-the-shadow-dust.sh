#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Luna The Shadow Dust
# send your bug reports to contact@dotslashplay.it
###

script_version=20231018.2

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='luna-the-shadow-dust'
GAME_NAME='LUNA The Shadow Dust'

ARCHIVE_BASE_0_NAME='luna_the_shadow_dust_1_0_2_36448.sh'
ARCHIVE_BASE_0_MD5='394463d5b600dbf8fe0619d5ffe944f1'
ARCHIVE_BASE_0_SIZE='789052'
ARCHIVE_BASE_0_VERSION='1.0.2-gog36448'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/luna_the_shadow_dust'

UNITY3D_NAME='Luna'
UNITY3D_PLUGINS='
libfmodL.so
libfmod.so
libfmodstudioL.so
libfmodstudio.so
libgvraudio.so
libresonanceaudio.so
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'

## The game crashes on launch when the Wayland backend of SDL is used,
## even when forcing the use of system SDL.
APP_MAIN_PRERUN='# The game crashes on launch when the Wayland backend of SDL is used
if [ "${SDL_VIDEODRIVER:-}" = "wayland" ]; then
	unset SDL_VIDEODRIVER
fi
'

## PKG_BIN64 must be listed before PKG_BIN32,
## because some 64-bit plugins are included at the root of the Plugins directory.
PACKAGES_LIST='PKG_BIN64 PKG_BIN32 PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

PKG='PKG_BIN64'
# shellcheck disable=SC2119
launchers_write
PKG='PKG_BIN32'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
