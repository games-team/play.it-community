#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Sunlight
# send your bug reports to contact@dotslashplay.it
###

script_version=20231006.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='sunlight'
GAME_NAME='Sunlight'

ARCHIVE_BASE_0_NAME='Sunlight v1.1.5RC 211801 Windows.zip'
ARCHIVE_BASE_0_MD5='fbe2840776c841d4582f40e68acfe8aa'
ARCHIVE_BASE_0_SIZE='980000'
ARCHIVE_BASE_0_VERSION='1.1.5RC.211801-itch1'
ARCHIVE_BASE_0_URL='https://krillbitestudio.itch.io/sunlight'

UNITY3D_NAME='Sunlight'

CONTENT_PATH_DEFAULT='Sunlight v1.1.5RC 211801 Windows'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
