#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Codemancer
# send your bug reports to contact@dotslashplay.it
###

script_version=20230328.2

GAME_ID='codemancer'
GAME_NAME='Codemancer'

ARCHIVE_BASE_0='Codemancer.msi'
ARCHIVE_BASE_0_MD5='727dfcf3640ce064f455a023c1c50e1e'
ARCHIVE_BASE_0_SIZE='120000'
ARCHIVE_BASE_0_VERSION='1.0.2-itch.2020.03.17'
ARCHIVE_BASE_0_URL='https://importantlittlegames.itch.io/codemancer-ch-1'

CONTENT_PATH_DEFAULT='appdir:.'
CONTENT_GAME_BIN_FILES='
adobe air
codemancer chapter 1.exe'
CONTENT_GAME_DATA_FILES='
assets
meta-inf
mimetype
icon.ico
*.swf'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Roaming/CodemancerChapter1'

APP_MAIN_EXE='codemancer chapter 1.exe'
APP_MAIN_ICON='icon.ico'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

target_version='2.23'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
