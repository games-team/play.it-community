#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Book of Demons
# send your bug reports to contact@dotslashplay.it
###

script_version=20240325.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='book-of-demons'
GAME_NAME='Book of Demons'

ARCHIVE_BASE_3_NAME='setup_book_of_demons_1.05.240321_(72057).exe'
ARCHIVE_BASE_3_MD5='d569fc4dddcec04aadea70ef49207566'
ARCHIVE_BASE_3_TYPE='innosetup'
ARCHIVE_BASE_3_SIZE='946740'
ARCHIVE_BASE_3_VERSION='1.05.240321-gog72057'
ARCHIVE_BASE_3_URL='https://www.gog.com/game/book_of_demons'

ARCHIVE_BASE_2_NAME='setup_book_of_demons_1.05.221221_(61311).exe'
ARCHIVE_BASE_2_MD5='fc7148454bba56db0bb1b8942d1bfa85'
ARCHIVE_BASE_2_TYPE='innosetup'
ARCHIVE_BASE_2_SIZE='950000'
ARCHIVE_BASE_2_VERSION='1.05.221221-gog61311'

ARCHIVE_BASE_1_NAME='setup_book_of_demons_1.05.220217_(53747).exe'
ARCHIVE_BASE_1_MD5='6d33342267e0b05af5f493339d450d60'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='900000'
ARCHIVE_BASE_1_VERSION='1.05.220217-gog53747'

ARCHIVE_BASE_0_NAME='setup_book_of_demons_1.05.211021_(50897).exe'
ARCHIVE_BASE_0_MD5='ef098c3f8dd9bc7c82ec2e8ebd6c8ec5'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='900000'
ARCHIVE_BASE_0_VERSION='1.05.211021-gog50897'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
*.dll
*.xml
r2g_launcher.exe'
CONTENT_GAME_DATA_FILES='
*.pac
supporters.utf8'
CONTENT_DOC_DATA_FILES='
changelog.txt'

USER_PERSISTENT_FILES='
custom_settings.txt
*.xml'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Roaming/R2G_Common_WIP1/GOG
users/${USER}/AppData/Roaming/Return2Games_WIP21/GOG'

APP_MAIN_EXE='r2g_launcher.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
