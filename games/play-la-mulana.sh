#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# La-Mulana
# send your bug reports to contact@dotslashplay.it
###

script_version=20230216.2

GAME_ID='la-mulana'
GAME_NAME='La•Mulana'

## This DRM-free build is no longer available for sale from Humble Bundle,
## they now only sell Steam keys instead.
ARCHIVE_BASE_0='20170404_LaMulana_Linux.zip'
ARCHIVE_BASE_0_MD5='e7a597ea2588ae975a7cc7b59c17d50d'
ARCHIVE_BASE_0_SIZE='120000'
ARCHIVE_BASE_0_VERSION='1.6.6-humble180409'

CONTENT_PATH_DEFAULT='data'
CONTENT_GAME_BIN_PATH='data/x86'
CONTENT_GAME_BIN_FILES='
LaMulana.bin.x86'
CONTENT_GAME_DATA_PATH='data/noarch'
CONTENT_GAME_DATA_FILES='
data
*.bmp
*.png'
CONTENT_DOC_DATA_PATH='data/noarch'
CONTENT_DOC_DATA_FILES='
README.linux'
CONTENT_DOC0_DATA_PATH='.'
CONTENT_DOC0_DATA_FILES='
License
Manual
ReadMe_??.txt'

APP_MAIN_EXE='LaMulana.bin.x86'
APP_MAIN_ICON='noarch/Icon.png'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libgcc_s.so.1
libGL.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
libSDL2-2.0.so.0
libstdc++.so.6'

# The .zip archive includes a MojoSetup installer.

SCRIPT_DEPS="$SCRIPT_DEPS bsdtar"

# Load common functions

target_version='2.21'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'
INNER_ARCHIVE="${PLAYIT_WORKDIR}/gamedata/LaMulanaSetup-2017-01-27.sh"
INNER_ARCHIVE_TYPE='mojosetup'
archive_extraction 'INNER_ARCHIVE'
rm "$INNER_ARCHIVE"

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game content

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
