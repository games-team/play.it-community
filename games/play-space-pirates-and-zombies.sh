#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Space Pirates and Zombies
# send your bug reports to contact@dotslashplay.it
###

script_version=20230206.1

GAME_ID='space-pirates-and-zombies'
GAME_NAME='Space Pirates and Zombies'

ARCHIVE_BASE_0='gog_space_pirates_and_zombies_2.0.0.4.sh'
ARCHIVE_BASE_0_MD5='46da2a84e78f8016e35f7c0e63e28581'
ARCHIVE_BASE_0_TYPE='mojosetup'
ARCHIVE_BASE_0_SIZE='180000'
ARCHIVE_BASE_0_VERSION='1.605-gog2.0.0.4'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/space_pirates_and_zombies'

CONTENT_PATH_DEFAULT='data/noarch/game'
## Despite its name, "audio.so" is not an ELF library.
CONTENT_GAME_BIN_FILES='
audio.so
SPAZ'
CONTENT_GAME_DATA_FILES='
common
game
mods
icon.bmp
SPAZ.png'
CONTENT_DOC_DATA_FILES='
README-linux.txt'

APP_MAIN_EXE='SPAZ'
APP_MAIN_ICON='SPAZ.png'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
libSDL-1.2.so.0
libstdc++.so.6'

# Load common functions

target_version='2.21'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "$PLAYIT_WORKDIR/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
