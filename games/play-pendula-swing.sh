#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Pendula Swing
# send your bug reports to contact@dotslashplay.it
###

script_version=20230811.3

GAME_ID='pendula-swing'
GAME_NAME='Pendula Swing: The Complete Journey'

ARCHIVE_BASE_0='PendulaSwingTheCompleJourney.zip'
ARCHIVE_BASE_0_MD5='afbec54341d36c29baa08feb538b9613'
ARCHIVE_BASE_0_SIZE='3500000'
ARCHIVE_BASE_0_VERSION='3.1.4-itch.2020.06.08'
ARCHIVE_BASE_0_URL='https://valiant-game-studio.itch.io/pendula-swing-the-complete-journey'

UNITY3D_NAME='PendulaSwing'

CONTENT_PATH_DEFAULT='Build'
CONTENT_GAME_BIN_FILES="
${UNITY3D_NAME}.exe
${UNITY3D_NAME}_Data/Plugins
MonoBleedingEdge
UnityPlayer.dll"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data"

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Valiant Game Studio/PendulaSwing'

APP_MAIN_EXE="${UNITY3D_NAME}.exe"

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
video/quicktime, variant=(string)iso'

# Load common functions

target_version='2.24'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icon

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
