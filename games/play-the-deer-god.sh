#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Deer God
# send your bug reports to contact@dotslashplay.it
###

script_version=20240806.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='the-deer-god'
GAME_NAME='The Deer God'

ARCHIVE_BASE_1_NAME='DeerGodBeta02.zip'
ARCHIVE_BASE_1_MD5='84dda0d5e6708ddb5e7ceee307671f2c'
ARCHIVE_BASE_1_SIZE='150000'
ARCHIVE_BASE_1_VERSION='1.0~beta2-itch'
ARCHIVE_BASE_1_URL='https://marumari.itch.io/the-deer-god'

ARCHIVE_BASE_0_NAME='thedeergod_beta1.zip'
ARCHIVE_BASE_0_MD5='e722cdbd4cc93c5d655735c6a1285cc0'
ARCHIVE_BASE_0_SIZE='140000'
ARCHIVE_BASE_0_VERSION='1.0~beta1-itch'
ARCHIVE_BASE_0_URL='https://marumari.itch.io/the-deer-god'

UNITY3D_NAME='DeerGodBeta'

CONTENT_PATH_DEFAULT='.'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Cinopt Studios LLC/The Deer God'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Enforce a consistent Unity3D name
	unity3d_name=$(unity3d_name)
	old_name='thedeergod_beta1'
	if [ -e "${old_name}.exe" ]; then
		mv "${old_name}.exe" "${unity3d_name}.exe"
		mv "${old_name}_Data" "${unity3d_name}_Data"
	fi
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
