#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Peck N Run
# send your bug reports to contact@dotslashplay.it
###

script_version=20240404.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='peck-n-run'
GAME_NAME='Peck N Run'

ARCHIVE_BASE_0_NAME='peck-n-run-windows-release.zip'
ARCHIVE_BASE_0_MD5='2b355dea05894f71c248cf0a4583cfb0'
ARCHIVE_BASE_0_SIZE='410000'
ARCHIVE_BASE_0_VERSION='1.10-itch'
ARCHIVE_BASE_0_URL='https://spiltmilkstudios.itch.io/peck-n-run'

UNITY3D_NAME='PeckNRun'

CONTENT_PATH_DEFAULT='.'

WINE_DIRECT3D_RENDERER='dxvk'
WINE_REGEDIT_PERSISTENT_KEYS='
HKEY_CURRENT_USER\Software\Spilt Milk Studios\BirdGamePrototype'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
