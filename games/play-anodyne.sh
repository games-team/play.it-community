#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Anodyne
# send your bug reports to contact@dotslashplay.it
###

script_version=20240806.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='anodyne'
GAME_NAME='Anodyne'

ARCHIVE_BASE_0_NAME='1_6winanodyne.zip'
ARCHIVE_BASE_0_MD5='5e7be131731a8073f3dbf4489d7e414a'
ARCHIVE_BASE_0_SIZE='110000'
ARCHIVE_BASE_0_VERSION='1.60-itch1'
ARCHIVE_BASE_0_URL='https://han-tani.itch.io/anodyne'

CONTENT_PATH_DEFAULT='windows'
CONTENT_DOC_DATA_FILES='
README.txt'
CONTENT_GAME_BIN_FILES='
Adobe AIR
*.dll
*.exe'
CONTENT_GAME_DATA_FILES='
icons
js
META-INF
mimetype
*.der
*.dylib
*.html
*.png
*.ssp4
*.swf
*.txt'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/.Anodyne'

APP_MAIN_EXE='Anodyne.exe'
APP_MAIN_ICONS_LIST='APP_MAIN_ICON_29 APP_MAIN_ICON_32 APP_MAIN_ICON_48 APP_MAIN_ICON_57 APP_MAIN_ICON_128 APP_MAIN_ICON_512'
APP_MAIN_ICON_29='icons/IntraAIR29.png'
APP_MAIN_ICON_32='icons/IntraAIR32.png'
APP_MAIN_ICON_48='icons/IntraAIR48.png'
APP_MAIN_ICON_57='icons/IntraAIR57.png'
APP_MAIN_ICON_512='icons/IntraAIR512.png'
APP_MAIN_ICON_128='icons/IntraAIR128.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
