#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Anna Lea
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Amnesia: The Dark Descent
# send your bug reports to contact@dotslashplay.it
###

script_version=20231107.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='amnesia-1-the-dark-descent'
GAME_NAME='Amnesia: The Dark Descent'

ARCHIVE_BASE_0_NAME='amnesia_the_dark_descent_1_41_a_40599.sh'
ARCHIVE_BASE_0_MD5='6ef56415f02d611f83f12a064f144379'
ARCHIVE_BASE_0_SIZE='2500000'
ARCHIVE_BASE_0_VERSION='1.41a-gog40599'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/amnesia_the_dark_descent'

CONTENT_PATH_DEFAULT='data/noarch/game'
## TODO: Check if the Steam library can be dropped.
## TODO: Check if some shipped libraries can be replaced with system ones.
CONTENT_LIBS_BIN_FILES='
libIL.so.1
libSDL2-2.0.so.0
libsteam_api.so'
CONTENT_LIBS_BIN64_PATH="${CONTENT_PATH_DEFAULT}/lib64"
CONTENT_LIBS_BIN64_FILES="$CONTENT_LIBS_BIN_FILES"
CONTENT_LIBS_BIN32_PATH="${CONTENT_PATH_DEFAULT}/lib"
CONTENT_LIBS_BIN32_FILES="$CONTENT_LIBS_BIN_FILES"
CONTENT_GAME_BIN64_FILES='
Amnesia.bin.x86_64
Launcher.bin.x86_64'
CONTENT_GAME_BIN32_FILES='
Amnesia.bin.x86
Launcher.bin.x86'
CONTENT_GAME_DATA_FILES='
billboards
commentary
config
core
custom_stories
data
entities
flashbacks
fonts
graphics
gui
lang
launcher
lights
main_menu
maps
misc
models
music
particles
shaders
sounds
static_objects
textures
materials.cfg
ptest_materials.cfg
resources.cfg
super_secret.rar'
CONTENT_DOC_DATA_FILES='
Manual_en.pdf
Remember - Short Story Collection.pdf
EULA_en.rtf
1.3_ChangeLog.txt'

APP_MAIN_EXE_BIN32='Amnesia.bin.x86'
APP_MAIN_EXE_BIN64='Amnesia.bin.x86_64'
APP_MAIN_ICON='../support/icon.png'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
## Ensure smooth upgrades from packages generated with pre-20231107.1 game scripts.
PKG_DATA_PROVIDES="${PKG_DATA_PROVIDES:-}
amnesia-1-data"

PKG_BIN32_ARCH='32'
PKG_BIN64_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libfontconfig.so.1
libGL.so.1
libGLU.so.1
libIL.so.1
libogg.so.0
libopenal.so.1
libSDL2-2.0.so.0
libstdc++.so.6
libtheora.so.0
libvorbisfile.so.3
libvorbis.so.0
libX11.so.6
libXext.so.6
libXft.so.2
libz.so.1'
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
## Ensure smooth upgrades from packages generated with pre-20231107.1 game scripts.
PKG_BIN64_PROVIDES="${PKG_BIN64_PROVIDES:-}
amnesia-1"
PKG_BIN32_PROVIDES="${PKG_BIN32_PROVIDES:-}
amnesia-1"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN64'
# shellcheck disable=SC2119
launchers_write
set_current_package 'PKG_BIN32'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
