#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# GemCraft - Frostborn Wrath
# send your bug reports to contact@dotslashplay.it
###

script_version=20240618.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='gemcraft'
GAME_NAME='GemCraft - Frostborn Wrath'

ARCHIVE_BASE_1_NAME='setup_gemcraft_-_frostborn_wrath_v.1.2.1a_(45694).exe'
ARCHIVE_BASE_1_MD5='8b04168cdb93ed44440fe0ed4181085d'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='220000'
ARCHIVE_BASE_1_VERSION='1.2.1a-gog45694'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/gemcraft_frostborn_wrath'

ARCHIVE_BASE_0_NAME='setup_gemcraft_-_frostborn_wrath_v.1.1.2b_(38400).exe'
ARCHIVE_BASE_0_MD5='3810dc7a58febb3120047a0a041f1b60'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='220000'
ARCHIVE_BASE_0_VERSION='1.1.2b-gog38400'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
gemcraft frostborn wrath.exe
adobe air'
CONTENT_GAME_DATA_FILES='
icons
meta-inf
mimetype
*.swf'

WINE_PERSISTENT_DIRCTORIES='
users/${USER}/AppData/Roaming/com.giab.games.gcfw
users/${USER}/Documents/GCFW-backup1'

APP_MAIN_EXE='gemcraft frostborn wrath.exe'
APP_MAIN_ICON='icons/gcfw-icon-128x128tr.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
