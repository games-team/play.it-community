#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Flashout 3D: Enhanced Edition
# send your bug reports to contact@dotslashplay.it
###

script_version=20230213.1

GAME_ID='flashout-3d-enhanced-edition'
GAME_NAME='Flashout 3D: Enhanced Edition'

ARCHIVE_BASE_0='setup_flashout_3d_enhanced_edition_1.0.4_(58979).exe'
ARCHIVE_BASE_0_MD5='f1a898a6f1a5d50f1f482170221557a5'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='830000'
ARCHIVE_BASE_0_VERSION='1.0.4-gog58979'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/flashout_3d_enhanced_edition'

UNITY3D_NAME='flashout3d-ee'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES="
${UNITY3D_NAME}.exe
${UNITY3D_NAME}_data/mono
${UNITY3D_NAME}_data/plugins
*.dll"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_data"

APP_MAIN_EXE="${UNITY3D_NAME}.exe"

WINE_REGEDIT_PERSISTENT_KEYS='
HKEY_CURRENT_USER\Software\Jujubee S.A.\Flashout3D-EE'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

target_version='2.22'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Delete unwanted files
	rm --force --recursive \
		'commonappdata' \
		'tmp'
)

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
