#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Us Lovely Corpses
# send your bug reports to contact@dotslashplay.it
###

script_version=20240622.2

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='us-lovely-corpses'
GAME_NAME='Us Lovely Corpses'

ARCHIVE_BASE_0_NAME='UsLovelyCorpses-1.0-linux.tar.bz2'
ARCHIVE_BASE_0_MD5='2df30ef88e4cc99662510f7e8b9efd3d'
ARCHIVE_BASE_0_SIZE='136948'
ARCHIVE_BASE_0_VERSION='1.0-itch.2018.03.08'
ARCHIVE_BASE_0_URL='https://dmarielicea.itch.io/uslovelycorpses'

CONTENT_PATH_DEFAULT='UsLovelyCorpses-1.0-linux/game'
CONTENT_GAME_MAIN_FILES='
cache
gui
images
script_version.txt
*.ogg
*.rpy
*.rpyc'
CONTENT_DOC_MAIN_PATH='UsLovelyCorpses-1.0-linux'
CONTENT_DOC_MAIN_FILES='
README.html'

APP_MAIN_TYPE='renpy'

## Ensure easy upgrade from packages generated with pre-20240622.1 game scripts.
PKG_MAIN_PROVIDES="${PKG_MAIN_PROVIDES:-}
us-lovely-corpses
us-lovely-corpses-data"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Use the default Ren'Py icon, as none is provided

desktop_field_icon() { printf 'renpy'; }

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
