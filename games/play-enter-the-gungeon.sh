#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Daguhh
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Enter the Gungeon
# send your bug reports to contact@dotslashplay.it
###

script_version=20231122.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='enter-the-gungeon'
GAME_NAME='Enter the Gungeon'

ARCHIVE_BASE_0_NAME='enter_the_gungeon_2_1_9_33951.sh'
ARCHIVE_BASE_0_MD5='5d4c174aa3b9bb57faa19e44872794e5'
ARCHIVE_BASE_0_SIZE='360000'
ARCHIVE_BASE_0_VERSION='2.1.9-gog33951'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/enter_the_gungeon'

UNITY3D_NAME='EtG'

CONTENT_PATH_DEFAULT='data/noarch/game'
## TODO: Add an explicit list of plugins to include.
CONTENT_GAME0_BIN32_FILES="
${UNITY3D_NAME}_Data/Plugins/x86"
CONTENT_GAME0_BIN64_FILES="
${UNITY3D_NAME}_Data/Plugins/x86_64"

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
## TODO: Complete the dependencies list.
##       cf. https://forge.dotslashplay.it/play.it/scripts/-/snippets/28
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libSDL2-2.0.so.0
libstdc++.so.6'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN32'
# shellcheck disable=SC2119
launchers_write
set_current_package 'PKG_BIN64'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
