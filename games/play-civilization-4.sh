#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Civilization 4
# send your bug reports to contact@dotslashplay.it
###

script_version=20231117.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='civilization-4'
GAME_NAME='Civilization Ⅳ'

ARCHIVE_BASE_0_NAME='setup_civilization4_complete_2.0.0.4.exe'
ARCHIVE_BASE_0_MD5='8ddc5e0129d216cb397e8da5f95a8c92'
ARCHIVE_BASE_0_EXTRACTOR='innoextract'
ARCHIVE_BASE_0_EXTRACTOR_OPTIONS='--gog'
ARCHIVE_BASE_0_PART1_NAME='setup_civilization4_complete_2.0.0.4-1.bin'
ARCHIVE_BASE_0_PART1_MD5='dbf971c1acbad3648697c7c343f14aad'
ARCHIVE_BASE_0_PART1_EXTRACTOR='unar'
ARCHIVE_BASE_0_SIZE='4800000'
ARCHIVE_BASE_0_VERSION='1.7.4.0-gog2.0.0.4'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/sid_meiers_civilization_iv_the_complete_edition'

CONTENT_PATH_DEFAULT='game/Civ4'
CONTENT_GAME_BIN_WARLORDS_FILES='
Warlords/Assets/Python/System
Warlords/CvGameCoreDLL
Warlords/*.dll
Warlords/*.exe'
CONTENT_GAME_BIN_SWORD_FILES='
Beyond the Sword/CvGameCoreDLL
Beyond the Sword/*.dll
Beyond the Sword/*.exe'
CONTENT_GAME_BIN_FILES='
Assets/Python/System
CvGameCoreDLL
Miles
*.dll
*.exe'
CONTENT_GAME_DATA_WARLORDS_FILES='
Warlords/Assets
Warlords/Mods
Warlords/PublicMaps
Warlords/Resource
Warlords/Shaders'
CONTENT_GAME_DATA_SWORD_FILES='
Beyond the Sword/Assets
Beyond the Sword/Mods
Beyond the Sword/PublicMaps
Beyond the Sword/Resource
Beyond the Sword/Shaders'
CONTENT_GAME_DATA_FILES='
Assets
Mods
PublicMaps
Resource
Shaders'
CONTENT_DOC_DATA_WARLORDS_PATH="${CONTENT_PATH_DEFAULT}/.."
CONTENT_DOC_DATA_WARLORDS_FILES="
Sid Meier's Civilization 4 - Warlords.pdf
Sid Meier's Civilization 4 - Warlords (FR).pdf"
CONTENT_DOC_DATA_SWORD_PATH="${CONTENT_PATH_DEFAULT}/.."
CONTENT_DOC_DATA_SWORD_FILES="
Sid Meier's Civilization 4 - Beyond the Sword.pdf
Sid Meier's Civilization 4 - Beyond the Sword (FR).pdf"
CONTENT_DOC_DATA_PATH="${CONTENT_PATH_DEFAULT}/.."
CONTENT_DOC_DATA_FILES="
Sid Meier's Civilization 4.pdf
Sid Meier's Civilization 4 (FR).pdf"
CONTENT_DOC0_DATA_PATH="${CONTENT_PATH_DEFAULT}/../Readme"
CONTENT_DOC0_DATA_FILES='
*/Readme.htm'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Local/My Games/civilization-4
users/${USER}/Documents/My Games/civilization-4
users/${USER}/AppData/Local/My Games/Warlords
users/${USER}/Documents/My Games/Warlords
users/${USER}/AppData/Local/My Games/Beyond the Sword
users/${USER}/Documents/My Games/Beyond the Sword'

APP_MAIN_EXE='Civilization4.exe'
APP_MAIN_ICON='Assets/res/Civ4Game.ico'

APP_WARLORDS_ID="${GAME_ID}-warlords"
APP_WARLORDS_NAME="$GAME_NAME - Warlords"
APP_WARLORDS_EXE='Warlords/Civ4Warlords.exe'
APP_WARLORDS_ICONS_LIST='APP_WARLORDS_ICON16 APP_WARLORDS_ICON32 APP_WARLORDS_ICON64'
APP_WARLORDS_ICON16='Warlords/Assets/res/CIV_Warlords_16.ico'
APP_WARLORDS_ICON32='Warlords/Assets/res/CIV_Warlords_32.ico'
APP_WARLORDS_ICON64='Warlords/Assets/res/CIV_Warlords_64.ico'

APP_SWORD_ID="${GAME_ID}-beyond-the-sword"
APP_SWORD_NAME="$GAME_NAME - Beyond the Sword"
APP_SWORD_EXE='Beyond the Sword/Civ4BeyondSword.exe'
APP_SWORD_ICON='Beyond the Sword/Assets/res/Civ4BtS.ico'

PACKAGES_LIST='
PKG_BIN_WARLORDS
PKG_BIN_SWORD
PKG_BIN
PKG_DATA_WARLORDS
PKG_DATA_SWORD
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_DATA_WARLORDS_ID="${GAME_ID}-warlords-data"
PKG_DATA_WARLORDS_DESCRIPTION='Warlords - data'
PKG_DATA_WARLORDS_DEPS="$PKG_DATA_ID"

PKG_DATA_SWORD_ID="${GAME_ID}-beyond-the-sword-data"
PKG_DATA_SWORD_DESCRIPTION='Beyond the Sword - data'
PKG_DATA_SWORD_DEPS="$PKG_DATA_ID"

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

PKG_BIN_WARLORDS_ID="${GAME_ID}-warlords"
PKG_BIN_WARLORDS_DESCRIPTION='Warlords'
PKG_BIN_WARLORDS_ARCH='32'
PKG_BIN_WARLORDS_DEPS="$PKG_DATA_WARLORDS_ID"

PKG_BIN_SWORD_ID="${GAME_ID}-beyond-the-sword"
PKG_BIN_SWORD_DESCRIPTION='Beyond the Sword'
PKG_BIN_SWORD_ARCH='32'
PKG_BIN_SWORD_DEPS="$PKG_DATA_SWORD_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
icons_inclusion 'APP_MAIN'
set_current_package 'PKG_DATA_WARLORDS'
icons_inclusion 'APP_WARLORDS'
set_current_package 'PKG_DATA_SWORD'
icons_inclusion 'APP_SWORD'
content_inclusion_default

# Ensure fonts expected by the game are available

## TODO: The library should provide a function path_fonts (or path_fonts_ttf?) returning this path.
PATH_FONTS="$(option_value 'PREFIX')/share/fonts/truetype/$(game_id)"
fonts_path_system="$(package_path 'PKG_DATA')${PATH_FONTS}"
fonts_path_game="$(package_path 'PKG_DATA')$(path_game_data)"
mkdir --parents "$fonts_path_system"
for font_file in \
	'Assets/res/Fonts/sylfaen.ttf' \
	'Resource/Civ4/sylfaenbi.ttf' \
	'Resource/Civ4/sylfaenb.ttf' \
	'Resource/Civ4/sylfaeni.ttf' \
	'Resource/Civ4/sylfaen.ttf' \
	'Resource/Fonts/arial.ttf' \
	'Resource/Fonts/sylfaen.ttf'
do
	mv --force "${fonts_path_game}/${font_file}" "${fonts_path_system}/$(basename "$font_file")"
	ln --symbolic "${PATH_FONTS}/$(basename "$font_file")" "${fonts_path_game}/${font_file}"
done

# Write launchers

set_current_package 'PKG_BIN'
launchers_write 'APP_MAIN'
set_current_package 'PKG_BIN_WARLORDS'
launchers_write 'APP_WARLORDS'
set_current_package 'PKG_BIN_SWORD'
launchers_write 'APP_SWORD'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
