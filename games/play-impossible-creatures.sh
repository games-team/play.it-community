#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2015 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Impossible Creatures
# send your bug reports to contact@dotslashplay.it
###

script_version=20230418.1

GAME_ID='impossible-creatures'
GAME_NAME='Impossible Creatures'

ARCHIVE_BASE_EN_0='setup_impossible_creatures_2.2.0.5.exe'
ARCHIVE_BASE_EN_0_MD5='1b7223749a9f1fef2aac8213db2023da'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_SIZE='1400000'
ARCHIVE_BASE_EN_0_VERSION='1.1.3-gog2.2.0.5'
ARCHIVE_BASE_EN_0_URL='https://www.gog.com/game/impossible_creatures'

ARCHIVE_BASE_FR_0='setup_impossible_creatures_french_2.2.0.5.exe'
ARCHIVE_BASE_FR_0_MD5='ff7366c991de12339c0063817b2194da'
ARCHIVE_BASE_FR_0_TYPE='innosetup'
ARCHIVE_BASE_FR_0_SIZE='1500000'
ARCHIVE_BASE_FR_0_VERSION='1.1.3-gog2.2.0.5'
ARCHIVE_BASE_FR_0_URL='https://www.gog.com/game/impossible_creatures'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
drivers
rdnplugins
rdntools
*.asi
*.dll
*.exe
*.flt
*.ini
*.m3d'
CONTENT_GAME_L10N_FILES='
locale
movies'
CONTENT_GAME_DATA_FILES='
data
datasrc
ic
insect
rankedgaming
rdnmod
sdk
ebusetup.sem
ic_stats.ico
*.module
*.sga'
CONTENT_DOC_DATA_FILES='
news.txt'
CONTENT_DOC0_DATA_PATH="${CONTENT_PATH_DEFAULT}/docs"
CONTENT_DOC0_DATA_FILES='*'
CONTENT_DOC1_DATA_PATH="${CONTENT_PATH_DEFAULT}/documents"
CONTENT_DOC1_DATA_FILES='*'

APP_MAIN_EXE='ic.exe'

APP_EDIT_ID="${GAME_ID}-mission-editor"
APP_EDIT_NAME="$GAME_NAME - Mission editor"
APP_EDIT_EXE='missioneditor.exe'

APP_CONFIG_ID="${GAME_ID}-configuration"
APP_CONFIG_NAME="$GAME_NAME - Configuration"
APP_CONFIG_CAT='Settings'
APP_CONFIG_EXE='icconfig.exe'

USER_PERSISTENT_DIRECTORIES='
playback
profiles
screenshots
stats'
USER_PERSISTENT_FILES='
*.ini'

PACKAGES_LIST='PKG_BIN PKG_L10N PKG_DATA'

PKG_L10N_ID="${GAME_ID}-l10n"
PKG_L10N_ID_EN="${PKG_L10N_ID}-en"
PKG_L10N_ID_FR="${PKG_L10N_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_ID"
PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_FR='French localization'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID $PKG_L10N_ID"

# Load common functions

target_version='2.24'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
