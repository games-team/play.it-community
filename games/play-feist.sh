#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Feist
# send your bug reports to contact@dotslashplay.it
###

script_version=20241123.2

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='feist'
GAME_NAME='Feist'

ARCHIVE_BASE_0_NAME='feist_en_1_3_0_22450.sh'
ARCHIVE_BASE_0_MD5='f9561b4faddb9789a5c97833af935d09'
ARCHIVE_BASE_0_SIZE='186520'
ARCHIVE_BASE_0_VERSION='1.3.0-gog22450'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/feist'

ARCHIVE_BASE_MULTIARCH_0_NAME='gog_feist_2.4.0.7.sh'
ARCHIVE_BASE_MULTIARCH_0_MD5='ce0b128c23defc946535ec180dcef121'
ARCHIVE_BASE_MULTIARCH_0_SIZE='459990'
ARCHIVE_BASE_MULTIARCH_0_VERSION='1.1.2-gog2.4.0.7'

UNITY3D_NAME='Feist'
UNITY3D_PLUGINS='
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='
PKG_BIN64
PKG_DATA'
PACKAGES_LIST_MULTIARCH='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN_DEPENDENCIES_LIBRARIES_MUTLIARCH='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libGLU.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES_MUTLIARCH="$PKG_BIN_DEPENDENCIES_LIBRARIES_MUTLIARCH"
PKG_BIN32_DEPENDENCIES_LIBRARIES_MUTLIARCH="$PKG_BIN_DEPENDENCIES_LIBRARIES_MUTLIARCH"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
case "$(current_archive)" in
	('ARCHIVE_BASE_MULTIARCH_'*)
		launchers_generation 'PKG_BIN32'
	;;
esac

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
