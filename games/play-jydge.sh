#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Jydge
# send your bug reports to contact@dotslashplay.it
###

script_version=20230216.2

GAME_ID='jydge'
GAME_NAME='JYDGE'

ARCHIVE_BASE_0='JYDGE'
ARCHIVE_BASE_0_MD5='3b8c8a14b7d7bc7c059c479910eb449d'
ARCHIVE_BASE_0_TYPE='zip'
ARCHIVE_BASE_0_SIZE='310000'
ARCHIVE_BASE_0_VERSION='1.2.0.6-humble1'
ARCHIVE_BASE_0_URL='https://www.humblebundle.com/store/jydge'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
JYDGE'
CONTENT_GAME_DATA_FILES='
*.pak
*.xml'

APP_MAIN_EXE='JYDGE'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libgcc_s.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
libSDL2-2.0.so.0
libstdc++.so.6'

# Load common functions

target_version='2.21'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
