#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Batman: Arkham Knight
# send your bug reports to contact@dotslashplay.it
###

script_version=20241215.3

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='batman-arkham-4'
GAME_NAME='Batman: Arkham Knight'

ARCHIVE_BASE_1_NAME='setup_batman_arkham_knight_1.999_(74718).exe'
ARCHIVE_BASE_1_MD5='5cbbe0e90a025b815cbd41ee424306f0'
ARCHIVE_BASE_1_EXTRACTOR='innoextract'
## Do not convert file paths to lower case
ARCHIVE_BASE_1_EXTRACTOR_OPTIONS=' '
ARCHIVE_BASE_1_PART1_NAME='setup_batman_arkham_knight_1.999_(74718)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='a65b7f94f1742b520eba4fe1833dea10'
ARCHIVE_BASE_1_PART2_NAME='setup_batman_arkham_knight_1.999_(74718)-2.bin'
ARCHIVE_BASE_1_PART2_MD5='8ec21e3f6d589b435741a7c479d6dc2c'
ARCHIVE_BASE_1_PART3_NAME='setup_batman_arkham_knight_1.999_(74718)-3.bin'
ARCHIVE_BASE_1_PART3_MD5='df3dc59d8c937e5ecc82bbf7cdb569a3'
ARCHIVE_BASE_1_PART4_NAME='setup_batman_arkham_knight_1.999_(74718)-4.bin'
ARCHIVE_BASE_1_PART4_MD5='f2ca1ca49a6ec9f889bb7fe512d33656'
ARCHIVE_BASE_1_PART5_NAME='setup_batman_arkham_knight_1.999_(74718)-5.bin'
ARCHIVE_BASE_1_PART5_MD5='36cb7c2a6a3312c14f50e8a0ed62d368'
ARCHIVE_BASE_1_PART6_NAME='setup_batman_arkham_knight_1.999_(74718)-6.bin'
ARCHIVE_BASE_1_PART6_MD5='615673312299f977c4c224538915382e'
ARCHIVE_BASE_1_PART7_NAME='setup_batman_arkham_knight_1.999_(74718)-7.bin'
ARCHIVE_BASE_1_PART7_MD5='106965231ab2888f9ecf59df76238366'
ARCHIVE_BASE_1_PART8_NAME='setup_batman_arkham_knight_1.999_(74718)-8.bin'
ARCHIVE_BASE_1_PART8_MD5='19849206fc9004fd7e662d6fb2d3fa8e'
ARCHIVE_BASE_1_PART9_NAME='setup_batman_arkham_knight_1.999_(74718)-9.bin'
ARCHIVE_BASE_1_PART9_MD5='15578850acef4289fc0ea670803fa28b'
ARCHIVE_BASE_1_PART10_NAME='setup_batman_arkham_knight_1.999_(74718)-10.bin'
ARCHIVE_BASE_1_PART10_MD5='e9d915384e205b4005b0152da61e4d7c'
ARCHIVE_BASE_1_PART11_NAME='setup_batman_arkham_knight_1.999_(74718)-11.bin'
ARCHIVE_BASE_1_PART11_MD5='1bb779b16400790d1ac09a224712f9bb'
ARCHIVE_BASE_1_PART12_NAME='setup_batman_arkham_knight_1.999_(74718)-12.bin'
ARCHIVE_BASE_1_PART12_MD5='ba347af6291479505292b7214c0821b2'
ARCHIVE_BASE_1_SIZE='52303886'
ARCHIVE_BASE_1_VERSION='1.999-gog74718'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/batman_arkham_knight'

ARCHIVE_BASE_0_NAME='setup_batman_arkham_knight_1.98_(37902).exe'
ARCHIVE_BASE_0_MD5='73307c30caaf63052132968a923f13ec'
ARCHIVE_BASE_0_EXTRACTOR='innoextract'
## Do not convert file paths to lower case
ARCHIVE_BASE_0_EXTRACTOR_OPTIONS=' '
ARCHIVE_BASE_0_PART1_NAME='setup_batman_arkham_knight_1.98_(37902)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='82a14c0140c2d3bfb587d55559edd3ab'
ARCHIVE_BASE_0_PART2_NAME='setup_batman_arkham_knight_1.98_(37902)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='075b116c6f6c79bc682a1dcd020e4910'
ARCHIVE_BASE_0_PART3_NAME='setup_batman_arkham_knight_1.98_(37902)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='ccf091fe25df898fb692854ffba23e95'
ARCHIVE_BASE_0_PART4_NAME='setup_batman_arkham_knight_1.98_(37902)-4.bin'
ARCHIVE_BASE_0_PART4_MD5='128dcddb67ed6c9838e802a82d29267f'
ARCHIVE_BASE_0_PART5_NAME='setup_batman_arkham_knight_1.98_(37902)-5.bin'
ARCHIVE_BASE_0_PART5_MD5='e4a186a946e10780ddc58e8ca79d6794'
ARCHIVE_BASE_0_PART6_NAME='setup_batman_arkham_knight_1.98_(37902)-6.bin'
ARCHIVE_BASE_0_PART6_MD5='ea31bab1b5ff9410a30663f2cd8ca35f'
ARCHIVE_BASE_0_PART7_NAME='setup_batman_arkham_knight_1.98_(37902)-7.bin'
ARCHIVE_BASE_0_PART7_MD5='e7726a1f658c9b29387bad9b10e8e4a3'
ARCHIVE_BASE_0_PART8_NAME='setup_batman_arkham_knight_1.98_(37902)-8.bin'
ARCHIVE_BASE_0_PART8_MD5='647176434aa98a19cce3edf625ef8631'
ARCHIVE_BASE_0_PART9_NAME='setup_batman_arkham_knight_1.98_(37902)-9.bin'
ARCHIVE_BASE_0_PART9_MD5='c8f4833b789186d3b03ff44af93b883b'
ARCHIVE_BASE_0_PART10_NAME='setup_batman_arkham_knight_1.98_(37902)-10.bin'
ARCHIVE_BASE_0_PART10_MD5='389d3f38c987268dfdb85c489b2e4831'
ARCHIVE_BASE_0_PART11_NAME='setup_batman_arkham_knight_1.98_(37902)-11.bin'
ARCHIVE_BASE_0_PART11_MD5='18947e9bd6b4a0b4ec35998f78961c88'
ARCHIVE_BASE_0_PART12_NAME='setup_batman_arkham_knight_1.98_(37902)-12.bin'
ARCHIVE_BASE_0_PART12_MD5='d67afeb3f4a687255d79ad1c1f45516e'
ARCHIVE_BASE_0_SIZE='53000000'
ARCHIVE_BASE_0_VERSION='1.98-gog37902'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
Binaries
Engine
BmGame/Config'
CONTENT_GAME_TEXTURES1_FILES='
BmGame/CookedPCConsole/Chunk0.tfc
BmGame/CookedPCConsole/Chunk1.tfc
BmGame/CookedPCConsole/Chunk2.tfc
BmGame/CookedPCConsole/Chunk3.tfc
BmGame/CookedPCConsole/Chunk4.tfc
BmGame/CookedPCConsole/Chunk5.tfc'
CONTENT_GAME_TEXTURES2_FILES='
BmGame/CookedPCConsole/Chunk6.tfc
BmGame/CookedPCConsole/Chunk7.tfc
BmGame/CookedPCConsole/Chunk8.tfc
BmGame/CookedPCConsole/Textures.tfc'
CONTENT_GAME_PACKAGES1_FILES='
BmGame/CookedPCConsole/City*.upk'
CONTENT_GAME_PACKAGES2_FILES='
BmGame/CookedPCConsole/*.upk'
CONTENT_GAME_MOVIES_FILES='
BmGame/Movies'
CONTENT_GAME_DATA_FILES='
BmGame/Splash
BmGame/CookedPCConsole/SFX
BmGame/CookedPCConsole/*.bin
BmGame/CookedPCConsole/English(US)
BmGame/Localization/INT'

USER_PERSISTENT_DIRECTORIES='
BmGame/Config
BmGame/SaveData'

WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/WB Games/Batman Arkham Knight'
## TODO: Check why a virtual desktop is required
WINE_VIRTUAL_DESKTOP='auto'
WINE_WINETRICKS_VERBS='physx'

APP_MAIN_EXE='Binaries/Win64/BatmanAK.exe'

PACKAGES_LIST='
PKG_BIN
PKG_TEXTURES1
PKG_TEXTURES2
PKG_PACKAGES1
PKG_PACKAGES2
PKG_MOVIES
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPENDENCIES_SIBLINGS='
PKG_TEXTURES1
PKG_TEXTURES2
PKG_PACKAGES1
PKG_PACKAGES2
PKG_MOVIES'

PKG_TEXTURES_ID="${GAME_ID}-textures"
PKG_TEXTURES1_ID="${PKG_TEXTURES_ID}-1"
PKG_TEXTURES2_ID="${PKG_TEXTURES_ID}-2"
PKG_TEXTURES_DESCRIPTION='textures'
PKG_TEXTURES1_DESCRIPTION="$PKG_TEXTURES_DESCRIPTION - 1"
PKG_TEXTURES2_DESCRIPTION="$PKG_TEXTURES_DESCRIPTION - 2"

PKG_PACKAGES_ID="${GAME_ID}-packages"
PKG_PACKAGES1_ID="${PKG_PACKAGES_ID}-1"
PKG_PACKAGES2_ID="${PKG_PACKAGES_ID}-2"
PKG_PACKAGES_DESCRIPTION='packages'
PKG_PACKAGES1_DESCRIPTION="$PKG_PACKAGES_DESCRIPTION - 1"
PKG_PACKAGES2_DESCRIPTION="$PKG_PACKAGES_DESCRIPTION - 2"

PKG_MOVIES_ID="${GAME_ID}-movies"
PKG_MOVIES_DESCRIPTION='movies'

# Ensure ability fo fully control the camera with the mouse

registry_dump_mouse_grab_file='registry-dumps/mouse-grab.reg'
registry_dump_mouse_grab_content='Windows Registry Editor Version 5.00

[HKEY_CURRENT_USER\Software\Wine\X11 Driver]
"GrabFullscreen"="Y"'
CONTENT_GAME_BIN_FILES="${CONTENT_GAME_BIN_FILES:-}
$registry_dump_mouse_grab_file"
APP_REGEDIT="${APP_REGEDIT:-} $registry_dump_mouse_grab_file"
REQUIREMENTS_LIST="${REQUIREMENTS_LIST:-}
iconv"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Ensure ability fo fully control the camera with the mouse
	mkdir --parents "$(dirname "$registry_dump_mouse_grab_file")"
	printf '%s' "$registry_dump_mouse_grab_content" |
		iconv --from-code=UTF-8 --to-code=UTF-16 --output="$registry_dump_mouse_grab_file"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

## Run the game binary from its parent directory
game_exec_line() {
	cat <<- 'EOF'
	cd Binaries/Win64
	$(wine_command) BatmanAK.exe "$@"
	EOF
}

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
