#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Anomaly Defenders
# send your bug reports to contact@dotslashplay.it
###

script_version=20240502.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='anomaly-defenders'
GAME_NAME='Anomaly Defenders'

ARCHIVE_BASE_ZIP_0_NAME='AnomalyDefenders_Linux_1402514865.zip'
ARCHIVE_BASE_ZIP_0_MD5='430367c8228077e4c8af2f5e7f164f4e'
ARCHIVE_BASE_ZIP_0_SIZE='640000'
ARCHIVE_BASE_ZIP_0_VERSION='1.0-humble'
ARCHIVE_BASE_ZIP_0_URL='https://www.humblebundle.com/store/anomaly-defenders'

ARCHIVE_BASE_TARGZ_0_NAME='AnomalyDefenders_Linux_1402512837.tar.gz'
ARCHIVE_BASE_TARGZ_0_MD5='35ccd57e8650dd53a09b1f1e088307cc'
ARCHIVE_BASE_TARGZ_0_SIZE='640000'
ARCHIVE_BASE_TARGZ_0_VERSION='1.0-humble'
ARCHIVE_BASE_TARGZ_0_URL='https://www.humblebundle.com/store/anomaly-defenders'

CONTENT_PATH_DEFAULT='AnomalyDefenders'
CONTENT_LIBS_BIN_FILES='
libOpenAL.so'
CONTENT_GAME_BIN_FILES='
AnomalyDefenders'
CONTENT_GAME_DATA_FILES='
icon.png
*.dat
*.idx'
CONTENT_DOC_DATA_FILES='
README
Copyright license*'

APP_MAIN_EXE='AnomalyDefenders'
APP_MAIN_ICON='icon.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6'

# LD_PRELOAD shim working around infinite loading times

PRELOAD_HACKS_LIST='
HACK_TIMERFIX'

HACK_TIMERFIX_NAME='timerfix'
HACK_TIMERFIX_DESCRIPTION='LD_PRELOAD shim working around infinite loading times'
HACK_TIMERFIX_PACKAGE='PKG_BIN'
HACK_TIMERFIX_SOURCE='
#define _GNU_SOURCE
#include <dlfcn.h>
#include <semaphore.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

static int (*_realSemTimedWait)(sem_t *, const struct timespec *) = NULL;

int sem_timedwait(sem_t *sem, const struct timespec *abs_timeout) {
	if (abs_timeout->tv_nsec >= 1000000000) {
		((struct timespec *)abs_timeout)->tv_nsec -= 1000000000;
		((struct timespec *)abs_timeout)->tv_sec++;
	}
	return _realSemTimedWait(sem, abs_timeout);
}
__attribute__((constructor)) void init(void) {
	_realSemTimedWait = dlsym(RTLD_NEXT, "sem_timedwait");
}
'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Build and include the LD_PRELOAD shims

hacks_inclusion_default

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
