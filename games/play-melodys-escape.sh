#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Melody's Escape
# send your bug reports to contact@dotslashplay.it
###

script_version=20230209.1

GAME_ID='melodys-escape'
GAME_NAME='Melodyʼs Escape'

ARCHIVE_BASE_0='Melodys_Escape_Linux.zip'
ARCHIVE_BASE_0_MD5='4d463482418c2d9917c56df3bbde6eea'
ARCHIVE_BASE_0_SIZE='60000'
ARCHIVE_BASE_0_VERSION='1.0-humble160601'
ARCHIVE_BASE_0_URL='https://www.humblebundle.com/store/melodys-escape'

CONTENT_PATH_DEFAULT="Melody's Escape"
CONTENT_GAME_MAIN_FILES='
BundledMusic
Calibration
Content
Mods
mono
MelodysEscape.exe
Bass.Net.Linux.dll
FNA.dll
FNA.dll.config
MelodyEngine.dll
MelodyReactor.dll
tar-cs.dll'
CONTENT_DOC_MAIN_FILES='
Licenses
README.txt'

APP_MAIN_EXE='MelodysEscape.exe'

PKG_MAIN_DEPS='mono'
PKG_MAIN_DEPENDENCIES_LIBRARIES='
libopenal.so.1
libSDL2-2.0.so.0
libSDL2_image-2.0.so.0'
PKG_MAIN_DEPENDENCIES_MONO_LIBRARIES='
mscorlib.dll
Mono.Posix.dll
Mono.Security.dll
System.dll
System.Configuration.dll
System.Core.dll
System.Data.dll
System.Drawing.dll
System.Runtime.Serialization.dll
System.Security.dll
System.Xml.dll'

# Patch shipped libraries, to ensure they work with system-provided Mono

SCRIPT_DEPS="$SCRIPT_DEPS xdelta3"

# Include shipped libraries that can not be replaced by system ones

CONTENT_LIBS_LIBS32_PATH="${CONTENT_PATH_DEFAULT}/lib"
CONTENT_LIBS_LIBS32_FILES='
libbass.so
libbassmix.so
libmojoshader.so'
CONTENT_LIBS0_LIBS32_PATH="${CONTENT_PATH_DEFAULT}/BassPlugins"
CONTENT_LIBS0_LIBS32_FILES='
libbassflac.so'
CONTENT_LIBS_LIBS64_PATH='lib64'
CONTENT_LIBS_LIBS64_FILES='
libbass.so
libbassmix.so
libmojoshader.so'
CONTENT_LIBS0_LIBS64_PATH='BassPlugins'
CONTENT_LIBS0_LIBS64_FILES='
libbassflac.so'

PACKAGES_LIST='PKG_MAIN PKG_LIBS32 PKG_LIBS64'

PKG_LIBS_ID="${GAME_ID}-libs"
PKG_LIBS32_ID="$PKG_LIBS_ID"
PKG_LIBS64_ID="$PKG_LIBS_ID"
PKG_LIBS32_ARCH='32'
PKG_LIBS64_ARCH='64'
PKG_LIBS_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libm.so.6
libpthread.so.0
librt.so.1'
PKG_LIBS32_DEPENDENCIES_LIBRARIES="$PKG_LIBS_DEPENDENCIES_LIBRARIES"
PKG_LIBS64_DEPENDENCIES_LIBRARIES="$PKG_LIBS_DEPENDENCIES_LIBRARIES"

PKG_MAIN_DEPS="$PKG_MAIN_DEPS $PKG_LIBS_ID"

# Load common functions

target_version='2.21'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Check for presence of required library patches

ARCHIVE_REQUIRED_LIB_PATCHES='melodys-escape_lib-patches.tar.gz'
ARCHIVE_REQUIRED_LIB_PATCHES_MD5='6aee21776f44df0d927babcddfa3c386'
ARCHIVE_REQUIRED_LIB_PATCHES_URL='https://downloads.dotslashplay.it/games/melodys-escape/'

archive_initialize_required \
	'ARCHIVE_LIB_PATCHES' \
	'ARCHIVE_REQUIRED_LIB_PATCHES'

# Check for presence of optional 64-bit libraries

ARCHIVE_OPTIONAL_LIB64='melodys-escape_lib64.tar.gz'
ARCHIVE_OPTIONAL_LIB64_MD5='a77c6b3acd5910bd874a1ca4b7d0c53c'
ARCHIVE_OPTIONAL_LIB64_URL='https://downloads.dotslashplay.it/games/melodys-escape/'

archive_initialize_optional \
	'ARCHIVE_LIB64' \
	'ARCHIVE_OPTIONAL_LIB64'

if [ -z "$ARCHIVE_LIB64" ]; then
	# Exclude 64-bit libraries from the packages to generate
	# if the archive has not been provided.
	PACKAGES_LIST='PKG_MAIN PKG_LIBS32'
fi

# Link libbassflac.so in the game data path
# as the game engine fails to find it otherwise,
# even if it is in LD_LIBRARY_PATH.

file_name='libbassflac.so'
file_source="$(path_libraries)/${file_name}"
for file_destination in \
	"$(package_path 'PKG_LIBS32')$(path_game_data)/BassPlugins/${file_name}" \
	"$(package_path 'PKG_LIBS64')$(path_game_data)/BassPlugins/${file_name}"
do
	mkdir --parents "$(dirname "$file_destination")"
	ln --symbolic "$file_source" "$file_destination"
done

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'
archive_extraction 'ARCHIVE_LIB_PATCHES'
if [ -n "$ARCHIVE_LIB64" ]; then
	archive_extraction 'ARCHIVE_LIB64'
fi
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Patch shipped libraries, to ensure they work with system-provided Mono
	for file in 'MelodyReactor.dll' 'MelodysEscape.exe'; do
		patch="${PLAYIT_WORKDIR}/gamedata/${file}.delta"
		xdelta3 decode -f -s "$file" "$patch" "$file"
		rm "$patch"
	done
)

# Include game icons

ARCHIVE_OPTIONAL_ICONS='melodys-escape_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_MD5='656fce13728d399e557fd72c3a6bc244'
ARCHIVE_OPTIONAL_ICONS_URL='https://downloads.dotslashplay.it/games/melodys-escape/'

CONTENT_ICONS_PATH='.'
CONTENT_ICONS_FILES='
16x16
32x32
48x48
64x64
128x128
256x256'

archive_initialize_optional \
	'ARCHIVE_ICONS' \
	'ARCHIVE_OPTIONAL_ICONS'

if [ -n "$ARCHIVE_ICONS" ]; then
	archive_extraction 'ARCHIVE_ICONS'
	content_inclusion 'ICONS' 'PKG_MAIN' "$(path_icons)"
fi

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
