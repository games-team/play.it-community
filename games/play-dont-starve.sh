#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2018 BetaRays
# SPDX-FileCopyrightText: © 2021 Hoël Bézier
set -o errexit

###
# Don't Starve
# send your bug reports to contact@dotslashplay.it
###

script_version=20231112.4

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='dont-starve'
GAME_NAME='Donʼt Starve'

ARCHIVE_BASE_1_NAME='don_t_starve_554439_66995.sh'
ARCHIVE_BASE_1_MD5='783646e973fdcc1fefad470da14f6855'
ARCHIVE_BASE_1_SIZE='660000'
ARCHIVE_BASE_1_VERSION='554439-gog66995'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/dont_starve'

ARCHIVE_BASE_0_NAME='don_t_starve_4294041_41439.sh'
ARCHIVE_BASE_0_MD5='05baa7fb659f79d4676a59bf3a64fc76'
ARCHIVE_BASE_0_SIZE='950000'
ARCHIVE_BASE_0_VERSION='4294041-gog41439'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN_PATH="${CONTENT_PATH_DEFAULT}/bin/lib64"
CONTENT_LIBS_BIN_FILES='
libfmodevent64.so
libfmodevent64-4.44.??.so
libfmodex64.so
libfmodex64-4.44.??.so'
## Using system-provided SDL, no mouse cursor is displayed and mouse inputs are ignored.
CONTENT_LIBS_BIN_FILES="${CONTENT_LIBS_BIN_FILES:-}
libSDL2.so
libSDL2-2.0.so.0
libSDL2-2.0.so.0.0.0"
CONTENT_GAME_BIN_FILES='
bin/dontstarve
*.json'
CONTENT_GAME_DATA_FILES='
data
mods
dontstarve.xpm'

USER_PERSISTENT_DIRECTORIES='
mods'

APP_MAIN_EXE='bin/dontstarve'
APP_MAIN_ICON='dontstarve.xpm'
## The shipped build of SDL2 has no support for the wayland backend.
APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}"'
# The shipped build of SDL2 has no support for the wayland backend
if [ "${SDL_VIDEODRIVER:-}" = "wayland" ]; then
	unset SDL_VIDEODRIVER
fi
'
## Run the game binary from its parent directory
APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}"'
# Run the game binary from its parent directory
cd "$(dirname "$APP_EXE")"
APP_EXE=$(basename "$APP_EXE")
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libcurl-gnutls.so.4
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
