#!/bin/sh
# SPDX-FileCopyrightText: © 2018 BetaRays
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Pink Hour
# send your bug reports to contact@dotslashplay.it
###

script_version=20230625.1

GAME_ID='pink-hour'
GAME_NAME='Pink Hour'

## This archive is no longer available from the Playism store,
## as they turned into yet another Steam keys reseller.
ARCHIVE_BASE_0='PinkHourEn-v1430a.zip'
ARCHIVE_BASE_0_MD5='7cd38735bf02634474eb8bf5a39439b2'
ARCHIVE_BASE_0_SIZE='15000'
ARCHIVE_BASE_0_VERSION='1.43-playism1430a'

CONTENT_PATH_DEFAULT='PinkHourEn'
CONTENT_GAME_BIN_FILES='
PinkHour.exe'
CONTENT_GAME_DATA_FILES='
rsc_p'
CONTENT_DOC_DATA_FILES='
ReadmeEn.txt'

APP_MAIN_EXE='PinkHour.exe'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

target_version='2.23'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
