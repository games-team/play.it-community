#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Encodya
# send your bug reports to contact@dotslashplay.it
###

script_version=20230731.1

GAME_ID='encodya'
GAME_NAME='Encodya'

GAME_ID_DEMO="${GAME_ID}-demo"
GAME_NAME_DEMO="$GAME_NAME (demo)"

# Archives

## Full game (gog.com)

ARCHIVE_BASE_0='encodya_1_1_46120.sh'
ARCHIVE_BASE_0_MD5='6fdadc42c828896389de2a9fd3a355fb'
ARCHIVE_BASE_0_SIZE='3300000'
ARCHIVE_BASE_0_VERSION='1.1-gog46120'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/encodya'

## Demo (itch.io)

## This free demo has been removed from itch.io since the release of the full game.
ARCHIVE_BASE_DEMO_0='ENCODYA_DemoV1linux.zip'
ARCHIVE_BASE_DEMO_0_MD5='4b3dfbbd37585615791acf13b4bb7ecd'
ARCHIVE_BASE_DEMO_0_SIZE='1100000'
ARCHIVE_BASE_DEMO_0_VERSION='1-itch'


UNITY3D_NAME='ENCODYA'
UNITY3D_NAME_DEMO='ENCODYA_DemoV1'
UNITY3D_PLUGINS='
abci.so
libsteam_api.so
UnityFbxSdkNative.so'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_PATH_DEFAULT_DEMO='ENCODYA_DemoV1'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_DEMO="${GAME_ID_DEMO}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPS_DEMO="$PKG_DATA_ID_DEMO"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libxml2.so.2
libz.so.1'

# Load common functions

target_version='2.25'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build package

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
