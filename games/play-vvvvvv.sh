#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 BetaRays
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# VVVVVV
# send your bug reports to contact@dotslashplay.it
###

script_version=20240505.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='vvvvvv'
GAME_NAME='VVVVVV'

ARCHIVE_BASE_HUMBLE_0_NAME='vvvvvv-10202016-bin'
ARCHIVE_BASE_HUMBLE_0_MD5='2a67882173f36c685f532e3cce0607af'
## This MojoSetup installer is not using a Makeself wrapper.
ARCHIVE_BASE_HUMBLE_0_EXTRACTOR='bsdtar'
ARCHIVE_BASE_HUMBLE_0_SIZE='110000'
ARCHIVE_BASE_HUMBLE_0_VERSION='2.2-humble.2016.10.20'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/vvvvvv'

ARCHIVE_BASE_GOG_0_NAME='vvvvvv_2_3_4_49985.sh'
ARCHIVE_BASE_GOG_0_MD5='76177a8ae3dede7c5cac1a90a00c0ca7'
ARCHIVE_BASE_GOG_0_SIZE='67000'
ARCHIVE_BASE_GOG_0_VERSION='2.3.4-gog49985'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/vvvvvv'

ARCHIVE_BASE_GOG_MULTIARCH_0_NAME='gog_vvvvvv_2.0.0.2.sh'
ARCHIVE_BASE_GOG_MULTIARCH_0_MD5='f25b5dd11ea1778d17d4b2e0b54c7eed'
ARCHIVE_BASE_GOG_MULTIARCH_0_SIZE='74000'
ARCHIVE_BASE_GOG_MULTIARCH_0_VERSION='2.2-gog2.0.0.2'

CONTENT_PATH_DEFAULT_HUMBLE='data'
CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_GAME_BIN_FILES='
VVVVVV'
CONTENT_GAME_BIN64_FILES='
x86_64/vvvvvv.x86_64'
CONTENT_GAME_BIN32_FILES='
x86/vvvvvv.x86'
CONTENT_GAME_DATA_FILES='
data.zip
VVVVVV.png'
CONTENT_DOC_DATA_FILES='
Linux.README'

APP_MAIN_EXE='VVVVVV'
APP_MAIN_EXE_BIN64='x86_64/vvvvvv.x86_64'
APP_MAIN_EXE_BIN32='x86/vvvvvv.x86'
APP_MAIN_ICON='VVVVVV.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'
PACKAGES_LIST_HUMBLE='
PKG_BIN64
PKG_BIN32
PKG_DATA'
PACKAGES_LIST_GOG_MULTIARCH='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libgcc_s.so.1
libm.so.6
libpthread.so.0
libSDL2-2.0.so.0
libSDL2_mixer-2.0.so.0
libstdc++.so.6'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

case "$(current_archive)" in
	('ARCHIVE_BASE_HUMBLE_'*)
		launchers_generation 'PKG_BIN64'
		launchers_generation 'PKG_BIN32'
	;;
	('ARCHIVE_BASE_GOG_MULTIARCH_'*)
		launchers_generation 'PKG_BIN64'
		launchers_generation 'PKG_BIN32'
	;;
	(*)
		launchers_generation 'PKG_BIN'
	;;
esac

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
