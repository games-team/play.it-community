#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 HS-157
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Just Cause 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20231005.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='just-cause-2'
GAME_NAME='Just Cause 2'

ARCHIVE_BASE_0_NAME='setup_just_cause_2_-_complete_edition_1.0.0.2_(50335).exe'
ARCHIVE_BASE_0_MD5='32cc043750be9e09354a3605f05f7de4'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_just_cause_2_-_complete_edition_1.0.0.2_(50335)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='4f100ae3a4ba93b72de4247aa7a61f29'
ARCHIVE_BASE_0_PART2_NAME='setup_just_cause_2_-_complete_edition_1.0.0.2_(50335)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='338797d683e65f69c834731f41855a12'
ARCHIVE_BASE_0_SIZE='9100000'
ARCHIVE_BASE_0_VERSION='1.0.0.2-gog50335'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/just_cause_2_complete_edition'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
*.dll
*.exe'
CONTENT_GAME_L10N_EN_FILES='
sound/fmod/dialog_mission?specific_eng.fsb
sound/fmod/dialog_pa_eng.fsb'
CONTENT_GAME_L10N_FR_FILES='
sound/fmod/dialog_mission?specific_fre.fsb
sound/fmod/dialog_pa_fre.fsb'
CONTENT_DOC_L10N_EN_FILES='
jc2 readme_en.rtf'
CONTENT_DOC_L10N_FR_FILES='
jc2 readme_fr.rtf'
CONTENT_GAME_DATA_FILES='
archives_win32
dlc
dx10_shaders_f.shader_bundle
dx10_specialshaders_f.shader_bundle
sound/fmod/cutscenes_common.fsb
sound/fmod/music_common.fsb
sound/fmod/music_stereo.fsb'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/Square Enix/Just Cause 2
users/${USER}/Documents/My Games/Just Cause 2'
## Work around a failure to start the game.
WINE_WINETRICKS_VERBS='d3dcompiler_47'

APP_MAIN_EXE='justcause2.exe'
## Switch French keyboard layout to us-azerty to provide direct access to digits.
## TODO: Check that this works expected on Wayland (it probably does not).
APP_MAIN_PRERUN='# Switch French keyboard layout to us-azerty to provide direct access to digits.
KEYBOARD_RESTORE_VARIANT=0
KEYBOARD_LAYOUT=$(LANG=C setxkbmap -query | awk "/layout:/ {print \$2}")
if [ "$KEYBOARD_LAYOUT" = "fr" ]; then
	KEYBOARD_VARIANT=$(LANG=C setxkbmap -query | awk "/variant:/ {print \$2}")
	if [ "$KEYBOARD_VARIANT" != "us-azerty" ]; then
		KEYBOARD_RESTORE_VARIANT=1
		setxkbmap -variant us-azerty
	fi
fi
'
APP_MAIN_POSTRUN='# Restore the keyboard variant, if it has previously been switched to us-azerty.
if [ $KEYBOARD_RESTORE_VARIANT -eq 1 ]; then
	setxkbmap -variant "$KEYBOARD_VARIANT"
fi
'

PACKAGES_LIST='PKG_BIN PKG_L10N_EN PKG_L10N_FR PKG_DATA'

PKG_L10N_ID="${GAME_ID}-l10n"
PKG_L10N_EN_ID="${PKG_L10N_ID}-en"
PKG_L10N_FR_ID="${PKG_L10N_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_ID"
PKG_L10N_EN_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_FR_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_EN_DESCRIPTION='English localization'
PKG_L10N_FR_DESCRIPTION='French localization'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID $PKG_L10N_ID"
## Switch French keyboard layout to us-azerty to provide direct access to digits.
PKG_BIN_DEPENDENCIES_COMMANDS='
setxkbmap'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Set game language

## English
l10n_conf_file_en="$(package_path 'PKG_L10N_EN')$(path_game_data)/goggame-1833855510.info"
mkdir --parents "$(dirname "$l10n_conf_file_en")"
## TODO: Check if we can drop some fields.
cat > "$l10n_conf_file_en" << EOF
{
    "clientId": "54243798143089551",
    "gameId": "1833855510",
    "language": "English",
    "languages": [
        "en-US"
    ],
    "name": "Just Cause 2 - Complete Edition",
    "playTasks": [
        {
            "category": "game",
            "isPrimary": true,
            "languages": [
                "en-US",
                "ru-RU",
                "es-ES",
                "it-IT",
                "de-DE",
                "fr-FR",
                "pl-PL"
            ],
            "name": "Just Cause 2 - Complete Edition",
            "path": "JustCause2.exe",
            "type": "FileTask"
        }
    ],
    "rootGameId": "1833855510",
    "version": 1
}
EOF
## French
l10n_conf_file_fr="$(package_path 'PKG_L10N_FR')$(path_game_data)/goggame-1833855510.info"
mkdir --parents "$(dirname "$l10n_conf_file_fr")"
## TODO: Check if we can drop some fields.
cat > "$l10n_conf_file_fr" << EOF
{
    "clientId": "54243798143089551",
    "gameId": "1833855510",
    "language": "French",
    "languages": [
        "fr-FR"
    ],
    "name": "Just Cause 2 - Complete Edition",
    "playTasks": [
        {
            "category": "game",
            "isPrimary": true,
            "languages": [
                "en-US",
                "ru-RU",
                "es-ES",
                "it-IT",
                "de-DE",
                "fr-FR",
                "pl-PL"
            ],
            "name": "Just Cause 2 - Complete Edition",
            "path": "JustCause2.exe",
            "type": "FileTask"
        }
    ],
    "rootGameId": "1833855510",
    "version": 1
}
EOF

# Extract game data

archive_extraction_default

# Include game data

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
case "${LANG%_*}" in
	('fr')
		lang_string='version %s :'
		lang_en='anglaise'
		lang_fr='française'
	;;
	('en'|*)
		lang_string='%s version:'
		lang_en='English'
		lang_fr='French'
	;;
esac
printf '\n'
printf "$lang_string" "$lang_en"
print_instructions 'PKG_L10N_EN' 'PKG_DATA' 'PKG_BIN'
printf "$lang_string" "$lang_fr"
print_instructions 'PKG_L10N_FR' 'PKG_DATA' 'PKG_BIN'

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
