#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2020 Mopi
set -o errexit

###
# Divine Divinity
# send your bug reports to contact@dotslashplay.it
###

script_version=20241216.3

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='divine-divinity'
GAME_NAME='Divine Divinity'

ARCHIVE_BASE_HUMBLE_EN_0_NAME='DivineDivinity.zip'
ARCHIVE_BASE_HUMBLE_EN_0_MD5='01bd76cb8e3054d7a32dda383b96fa73'
ARCHIVE_BASE_HUMBLE_EN_0_SIZE='4066351'
ARCHIVE_BASE_HUMBLE_EN_0_VERSION='1.0062a-humble1'
ARCHIVE_BASE_HUMBLE_EN_0_URL='https://www.humblebundle.com/store/divine-divinity'

ARCHIVE_BASE_GOG_EN_0_NAME='setup_divine_divinity_2.0.0.21.exe'
ARCHIVE_BASE_GOG_EN_0_MD5='3798d48f04a7a8444fd9f4c32b75b41d'
ARCHIVE_BASE_GOG_EN_0_TYPE='innosetup'
ARCHIVE_BASE_GOG_EN_0_SIZE='2400000'
ARCHIVE_BASE_GOG_EN_0_VERSION='1.0062a-gog2.0.0.21'
ARCHIVE_BASE_GOG_EN_0_URL='https://www.gog.com/game/divine_divinity'

ARCHIVE_BASE_GOG_FR_0_NAME='setup_divine_divinity_french_2.1.0.32.exe'
ARCHIVE_BASE_GOG_FR_0_MD5='f755d69ad7d319fb70298844dcb3861a'
ARCHIVE_BASE_GOG_FR_0_TYPE='innosetup'
ARCHIVE_BASE_GOG_FR_0_SIZE='2400000'
ARCHIVE_BASE_GOG_FR_0_VERSION='1.0062a-gog2.1.0.32'
ARCHIVE_BASE_GOG_FR_0_URL='https://www.gog.com/game/divine_divinity'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
sound.cfg
binkw32.dll
divdialogsystem.dll
drvmgt.dll
fmod.dll
nlseng.dll
nlsfr.dll
nlsger.dll
osirisdll.dll
slash1.dll
slash2.dll
slash3.dll
slash4.dll
unrar.dll
configtool.exe
div.exe
secdrv.sys'
CONTENT_GAME_L10N_FILES='
localizations
dat/english
dat/french
config.lcl'
CONTENT_GAME_DATA_FILES='
capture
dat
fonts
global
main
sound
static
divinityevent.dat
testimage.tga
keylist.txt'
CONTENT_DOC_DATA_FILES='
*.pdf'

USER_PERSISTENT_DIRECTORIES='
dynamic
global
savegames'
USER_PERSISTENT_FILES='
persist.dat
config.div
config.lcl
keylist.txt
dat/usernotes.bin
static/imagelists/collide.*
*.000
*.cfg'

APP_MAIN_EXE='div.exe'

APP_CONFIG_ID="${GAME_ID}_configuration"
APP_CONFIG_NAME="${GAME_NAME} - Configuration"
APP_CONFIG_CAT='Settings'
APP_CONFIG_EXE='configtool.exe'

PACKAGES_LIST='
PKG_BIN
PKG_L10N
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA
PKG_L10N'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_L10N_ID="${GAME_ID}-l10n"
PKG_L10N_ID_EN="${PKG_L10N_ID}-en"
PKG_L10N_ID_FR="${PKG_L10N_ID}-fr"
PKG_L10N_ID_HUMBLE_EN="$PKG_L10N_ID_EN"
PKG_L10N_ID_GOG_EN="$PKG_L10N_ID_EN"
PKG_L10N_ID_GOG_FR="$PKG_L10N_ID_FR"
PKG_L10N_PROVIDES="
$PKG_L10N_ID"
PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_FR='French localization'
PKG_L10N_DESCRIPTION_HUMBLE_EN="$PKG_L10N_DESCRIPTION_EN"
PKG_L10N_DESCRIPTION_GOG_EN="$PKG_L10N_DESCRIPTION_EN"
PKG_L10N_DESCRIPTION_GOG_FR="$PKG_L10N_DESCRIPTION_FR"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Check for the presence of extra dependency required by the Humble Bundle archive

## TODO: This could be avoided if REQUIREMENTS_LIST had support for contextual values.
##       cf. https://forge.dotslashplay.it/play.it/play.it/-/issues/558
case "$(current_archive)" in
	('ARCHIVE_BASE_HUMBLE_'*)
		REQUIREMENTS_LIST="${REQUIREMENTS_LIST:-}
		innoextract"
		requirements_check
	;;
esac

# Extract game data

archive_extraction_default
case "$(current_archive)" in
	('ARCHIVE_BASE_HUMBLE_EN_'*)
		ARCHIVE_INNER_PATH="${PLAYIT_WORKDIR}/gamedata/Build/DivInstaller_EN.exe"
		ARCHIVE_INNER_TYPE='innosetup'
		archive_extraction 'ARCHIVE_INNER'
		rm --recursive "${PLAYIT_WORKDIR}/gamedata/Build"
	;;
esac

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
