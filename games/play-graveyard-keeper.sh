#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Graveyard Keeper
# send your bug reports to contact@dotslashplay.it
###

script_version=20231112.3

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='graveyard-keeper'
GAME_NAME='Graveyard Keeper'

ARCHIVE_BASE_1_NAME='graveyard_keeper_1_405_55214.sh'
ARCHIVE_BASE_1_MD5='6d5461b5f7571dd5bed0561d26f189b3'
ARCHIVE_BASE_1_SIZE='1800000'
ARCHIVE_BASE_1_VERSION='1.405-gog55214'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/graveyard_keeper'

ARCHIVE_BASE_0_NAME='graveyard_keeper_1_402_51180.sh'
ARCHIVE_BASE_0_MD5='4095c374cade58a5ce4f6a80377cbcb7'
ARCHIVE_BASE_0_SIZE='1800000'
ARCHIVE_BASE_0_VERSION='1.402-gog51180'

UNITY3D_NAME='Graveyard Keeper'
UNITY3D_PLUGINS='
sqlite3.so'
## The game crashes on launch if the steam_api library is not available.
UNITY3D_PLUGINS="${UNITY3D_PLUGINS:-}
libsteam_api.so"

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
