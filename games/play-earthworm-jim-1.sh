#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Earthworm Jim 1
# send your bug reports to contact@dotslashplay.it
###

script_version=20241130.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='earthworm-jim-1'
GAME_NAME='Earthworm Jim'

ARCHIVE_BASE_0_NAME='earthworm_jim_en_1_0_16595.sh'
ARCHIVE_BASE_0_MD5='87b487f85654763347433dc0952b0118'
ARCHIVE_BASE_0_SIZE='720000'
ARCHIVE_BASE_0_VERSION='1.0-gog16595'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/earthworm_jim_1_2'

CONTENT_PATH_DEFAULT='data/noarch/data'
CONTENT_GAME_MAIN_FILES='
*.cfg
*.dat
*.exe
*.gog
*.hlp
*.ico
*.id
*.inst'
CONTENT_DOC_MAIN_FILES='
readme.txt'

GAME_IMAGE='ewj1.inst'

USER_PERSISTENT_FILES='
*.cfg'

APP_MAIN_EXE='ewj1.exe'
APP_MAIN_OPTIONS='320x224'
APP_MAIN_ICON='../support/icon.png'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Convert all file paths to lowercase
	tolower .
)

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
