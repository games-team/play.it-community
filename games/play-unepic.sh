#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 BetaRays
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# unEpic
# send your bug reports to contact@dotslashplay.it
###

script_version=20231112.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='unepic'
GAME_NAME='unEpic'

ARCHIVE_BASE_HUMBLE_0_NAME='unepic-15005.run'
ARCHIVE_BASE_HUMBLE_0_MD5='940824c4de6e48522845f63423e87783'
## This MojoSetup installer does not seem to be based on Makeself,
## so our regular type detection and extraction code do not work.
ARCHIVE_BASE_HUMBLE_0_EXTRACTOR='bsdtar'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.50.05-humble141208'
ARCHIVE_BASE_HUMBLE_0_SIZE='360000'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/unepic'

ARCHIVE_BASE_GOG_1_NAME='unepic_en_1_51_01_20608.sh'
ARCHIVE_BASE_GOG_1_MD5='88d98eb09d235fe3ca00f35ec0a014a3'
ARCHIVE_BASE_GOG_1_VERSION='1.51.01-gog20608'
ARCHIVE_BASE_GOG_1_SIZE='380000'
ARCHIVE_BASE_GOG_1_URL='https://www.gog.com/game/unepic'

ARCHIVE_BASE_GOG_0_NAME='gog_unepic_2.1.0.4.sh'
ARCHIVE_BASE_GOG_0_MD5='341556e144d5d17ae23d2b0805c646a1'
ARCHIVE_BASE_GOG_0_SIZE='380000'
ARCHIVE_BASE_GOG_0_VERSION='1.50.05-gog2.1.0.4'

CONTENT_PATH_DEFAULT_HUMBLE='data'
CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_GAME_BIN64_FILES='
unepic64'
CONTENT_GAME_BIN32_FILES='
unepic32'
CONTENT_GAME_DATA_FILES='
data
image
sound
voices
omaps
dictios_pc
unepic.png'

APP_MAIN_EXE_BIN64='unepic64'
APP_MAIN_EXE_BIN32='unepic32'
APP_MAIN_ICON_HUMBLE='unepic.png'
APP_MAIN_ICON_GOG='../support/icon.png'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libGL.so.1
libm.so.6
libSDL2-2.0.so.0
libSDL2_mixer-2.0.so.0
libstdc++.so.6
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN64'
# shellcheck disable=SC2119
launchers_write
set_current_package 'PKG_BIN32'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
