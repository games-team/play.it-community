#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Fear Equation
# send your bug reports to contact@dotslashplay.it
###

script_version=20241203.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='fear-equation'
GAME_NAME='Fear Equation'

ARCHIVE_BASE_0_NAME='gog_fear_equation_2.1.0.2.sh'
ARCHIVE_BASE_0_MD5='5bf5e55b87d8a9a2256a0a56531692c7'
ARCHIVE_BASE_0_SIZE='898207'
ARCHIVE_BASE_0_VERSION='2.0.2-gog2.1.0.2'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/fear_equation'

UNITY3D_NAME='FearEquation'
UNITY3D_PLUGINS='
libsqlite3.so
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libGLU.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

## Work around the engine looking for libsqlite3.so in an hardcoded path
library_source="$(path_libraries)/libsqlite3.so"
library_destination="$(package_path 'PKG_BIN64')$(path_game_data)/$(unity3d_name)_Data/Plugins/x86_64/libsqlite3.so"
mkdir --parents "$(dirname "$library_destination")"
ln --symbolic "$library_source"	"$library_destination"
library_destination="$(package_path 'PKG_BIN32')$(path_game_data)/$(unity3d_name)_Data/Plugins/x86/libsqlite3.so"
mkdir --parents "$(dirname "$library_destination")"
ln --symbolic "$library_source"	"$library_destination"

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
