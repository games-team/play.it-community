#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Age of Wonders: Planetfall
# send your bug reports to contact@dotslashplay.it
###

script_version=20230814.1

GAME_ID='age-of-wonders-planetfall'
GAME_NAME='Age of Wonders: Planetfall'

ARCHIVE_BASE_0='setup_age_of_wonders_planetfall_1.4.0.4c_(64bit)_(47648).exe'
ARCHIVE_BASE_0_MD5='a92c35dc7da9d98ec81124fe8d9e4d7d'
## Do not use innoextract --lowercase option,
## as it leads to voice overs not playing.
ARCHIVE_BASE_0_EXTRACTOR='innoextract'
ARCHIVE_BASE_0_EXTRACTOR_OPTIONS=' '
ARCHIVE_BASE_0_PART1='setup_age_of_wonders_planetfall_1.4.0.4c_(64bit)_(47648)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='cca141657c572c23851bf614988106c8'
ARCHIVE_BASE_0_PART2='setup_age_of_wonders_planetfall_1.4.0.4c_(64bit)_(47648)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='acf2bd9ce2f2e776f60b163e95628795'
ARCHIVE_BASE_0_SIZE='17000000'
ARCHIVE_BASE_0_VERSION='1.404.43827-gog47648'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/age_of_wonders_planetfall'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
Bin
*.dll
*.exe'
CONTENT_GAME_DATA_LIBRARIES_1_FILES='
Content/*/Libraries/Strategic
Content/*/Libraries/Tactical'
CONTENT_GAME_DATA_LIBRARIES_2_FILES='
Content/*/Libraries'
CONTENT_GAME_DATA_FILES='
Content
Development
Language
Debug.ttf'
CONTENT_DOC_DATA_FILES='
OpenSource
Readme.txt
ThirdPartyLicenses.txt'

WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/Paradox Interactive/Age of Wonders Planetfall'

APP_MAIN_EXE='AowPF.exe'

PACKAGES_LIST='PKG_BIN PKG_DATA_LIBRARIES_1 PKG_DATA_LIBRARIES_2 PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_DATA_LIBRARIES_ID="${PKG_DATA_ID}-libraries"
PKG_DATA_LIBRARIES_1_ID="${PKG_DATA_LIBRARIES_ID}-1"
PKG_DATA_LIBRARIES_2_ID="${PKG_DATA_LIBRARIES_ID}-2"
PKG_DATA_LIBRARIES_DESCRIPTION="$PKG_DATA_DESCRIPTION - libraries"
PKG_DATA_LIBRARIES_1_DESCRIPTION="$PKG_LIBRARIES_DESCRIPTION - 1"
PKG_DATA_LIBRARIES_2_DESCRIPTION="$PKG_LIBRARIES_DESCRIPTION - 2"
PKG_DATA_DEPS="${PKG_DATA_DEPS:-} $PKG_DATA_LIBRARIES_1_ID $PKG_DATA_LIBRARIES_2_ID"

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

target_version='2.25'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Delete unwanted files
	rm --recursive \
		'__redist' \
		'app' \
		'commonappdata' \
		'Launcher' \
		'launcher-installer-windows.msi' \
		'launcher-settings.json' \
		'tmp'
)

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build package

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
