#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Mirror's Edge
# send your bug reports to contact@dotslashplay.it
###

script_version=20241106.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='mirrors-edge'
GAME_NAME='Mirrorʼs Edge'

ARCHIVE_BASE_2_NAME='setup_mirrors_edge_1.01_jpfix_(41238).exe'
ARCHIVE_BASE_2_MD5='58078b7594476adf72d58dc01e024327'
ARCHIVE_BASE_2_TYPE='innosetup'
ARCHIVE_BASE_2_PART1_NAME='setup_mirrors_edge_1.01_jpfix_(41238)-1.bin'
ARCHIVE_BASE_2_PART1_MD5='9a202aeca8eaa63d18c24fcdc62a7890'
ARCHIVE_BASE_2_PART2_NAME='setup_mirrors_edge_1.01_jpfix_(41238)-2.bin'
ARCHIVE_BASE_2_PART2_MD5='3ee19c143f7d85e5393fc51d705ed61c'
ARCHIVE_BASE_2_PART3_NAME='setup_mirrors_edge_1.01_jpfix_(41238)-3.bin'
ARCHIVE_BASE_2_PART3_MD5='2a961e5b6f2ce2ef42169c0a023e5c4c'
ARCHIVE_BASE_2_SIZE='12000000'
ARCHIVE_BASE_2_VERSION='1.01-gog41238'
ARCHIVE_BASE_2_URL='https://www.gog.com/game/mirrors_edge'

ARCHIVE_BASE_1_NAME='setup_mirrors_edge_1.01_(40566).exe'
ARCHIVE_BASE_1_MD5='f0faffb76a2b46c3aca8342265a5f41b'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_mirrors_edge_1.01_(40566)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='cdfea6d6412de910eae6748eeb6af54e'
ARCHIVE_BASE_1_PART2_NAME='setup_mirrors_edge_1.01_(40566)-2.bin'
ARCHIVE_BASE_1_PART2_MD5='6b4a66c9ac31c8c2f4e647957c64370a'
ARCHIVE_BASE_1_PART3_NAME='setup_mirrors_edge_1.01_(40566)-3.bin'
ARCHIVE_BASE_1_PART3_MD5='8529e17c8faa54be07219106ffe00774'
ARCHIVE_BASE_1_SIZE='12000000'
ARCHIVE_BASE_1_VERSION='1.01-gog40566'

ARCHIVE_BASE_0_NAME='setup_mirrors_edge_2.0.0.3.exe'
ARCHIVE_BASE_0_MD5='89381d67169f5c6f8f300e172a64f99c'
ARCHIVE_BASE_0_EXTRACTOR='innoextract'
ARCHIVE_BASE_0_EXTRACTOR_OPTIONS='--gog'
ARCHIVE_BASE_0_PART1_NAME='setup_mirrors_edge_2.0.0.3-1.bin'
ARCHIVE_BASE_0_PART1_MD5='406b99108e1edd17fc60435d1f2c27f9'
ARCHIVE_BASE_0_PART1_TYPE='rar'
ARCHIVE_BASE_0_PART2_NAME='setup_mirrors_edge_2.0.0.3-2.bin'
ARCHIVE_BASE_0_PART2_MD5='18f2bd62201904c8e98a4b805a90ab2d'
ARCHIVE_BASE_0_PART2_TYPE='rar'
ARCHIVE_BASE_0_SIZE='7700000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.0.0.3'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='game'
CONTENT_GAME_BIN_FILES='
binaries
engine'
CONTENT_GAME_L10N_FILES='
tdgame/localization
tdgame/cookedpc/audio/???
tdgame/cookedpc/startup_???.upk
tdgame/cookedpc/ts_loc_???.upk
tdgame/cookedpc/ui/ui_fonts_final_???.upk
tdgame/cookedpc/maps/sp??/*_loc_???.upk'
CONTENT_GAME_MAPS_FILES='
tdgame/cookedpc/maps'
CONTENT_GAME_DATA_FILES='
me_icon.ico
tdgame'

USER_PERSISTENT_DIRECTORIES='
tdgame/config
tdgame/savefiles'

## TODO: Check if the inclusion of this library is required
WINE_WINETRICKS_VERBS='physx'

APP_MAIN_EXE='binaries/mirrorsedge.exe'
APP_MAIN_ICON='me_icon.ico'

PACKAGES_LIST='
PKG_BIN
PKG_L10N
PKG_MAPS
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_L10N
PKG_MAPS
PKG_DATA'

## TODO: Check if the localizations package could be splitted into lang-specific ones
PKG_L10N_ID="${GAME_ID}-l10n"
PKG_L10N_DESCRIPTION='localizations'

PKG_MAPS_ID="${GAME_ID}-maps"
PKG_MAPS_DESCRIPTION='maps'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
