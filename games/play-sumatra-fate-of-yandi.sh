#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Sumatra: Fate of Yandi
# send your bug reports to contact@dotslashplay.it
###

script_version=20230406.1

GAME_ID='sumatra-fate-of-yandi'
GAME_NAME='Sumatra: Fate of Yandi'

ARCHIVE_BASE_0='setup_sumatra_fate_of_yandi_1.2_(56037).exe'
ARCHIVE_BASE_0_MD5='60b1734aa9e06bb5077f48200c0106bf'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='290000'
ARCHIVE_BASE_0_VERSION='1.2-gog56037'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/sumatra_fate_of_yandi'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_MAIN_FILES='
*.cfg
*.exe
*.tra
*.vox'

APP_MAIN_TYPE='scummvm'
APP_MAIN_SCUMMID='ags:sumatra'
APP_MAIN_ICON='sumatra fate of yandi.exe'

PKG_MAIN_DEPS='scummvm'

# Load common functions

target_version='2.22'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
