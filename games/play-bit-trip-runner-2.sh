#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Bit.Trip Runner 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20240623.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='bit-trip-runner-2'
GAME_NAME='Bit.Trip Presents… Runner2: Future Legend of Rhythm Alien'

ARCHIVE_BASE_GOG_0_NAME='gog_bit_trip_presents_runner2_future_legend_of_rhythm_alien_2.0.0.1.sh'
ARCHIVE_BASE_GOG_0_MD5='28c003bae691aa46d841547da8e3def6'
ARCHIVE_BASE_GOG_0_SIZE='1600000'
ARCHIVE_BASE_GOG_0_VERSION='1.0-gog2.0.0.1'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/bittrip_presents_runner2_future_legend_of_rhythm_alien'

## These native Linux DRM-free builds are no longer available from the Humble Store.
## Only builds for Windows and MacOS are provided.
ARCHIVE_BASE_HUMBLE_32BIT_0_NAME='runner2_i386_1388171186.tar.gz'
ARCHIVE_BASE_HUMBLE_32BIT_0_MD5='ea105bdcd486879fb99889b87e90eed5'
ARCHIVE_BASE_HUMBLE_32BIT_0_SIZE='770000'
ARCHIVE_BASE_HUMBLE_32BIT_0_VERSION='1.0-humble1388171186'

## These native Linux DRM-free builds are no longer available from the Humble Store.
## Only builds for Windows and MacOS are provided.
ARCHIVE_BASE_HUMBLE_64BIT_0_NAME='runner2_amd64_1388171186.tar.gz'
ARCHIVE_BASE_HUMBLE_64BIT_0_MD5='2f7ccdb675a63a5fc152514682e97480'
ARCHIVE_BASE_HUMBLE_64BIT_0_SIZE='770000'
ARCHIVE_BASE_HUMBLE_64BIT_0_VERSION='1.0-humble1388171186'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game/runner2-1.0-32/runner2'
CONTENT_PATH_DEFAULT_GOG_BIN32='data/noarch/game/runner2-1.0-32/runner2'
CONTENT_PATH_DEFAULT_GOG_BIN64='data/noarch/game/runner2-1.0-64/runner2'
CONTENT_PATH_DEFAULT_HUMBLE='runner2-1.0/runner2'
CONTENT_LIBS_BIN64_PATH_GOG="$CONTENT_PATH_DEFAULT_GOG_BIN64"
CONTENT_LIBS_BIN64_FILES='
libfmodevent64.so
libfmodevent64-4.44.07.so
libfmodex64.so
libfmodex64-4.44.07.so'
CONTENT_LIBS_BIN32_PATH_GOG="$CONTENT_PATH_DEFAULT_GOG_BIN32"
CONTENT_LIBS_BIN32_FILES='
libfmodevent.so
libfmodevent-4.44.08.so
libfmodex.so
libfmodex-4.44.08.so'
CONTENT_GAME_BIN64_PATH_GOG="$CONTENT_PATH_DEFAULT_GOG_BIN64"
CONTENT_GAME_BIN64_FILES='
runner2'
CONTENT_GAME_BIN32_PATH_GOG="$CONTENT_PATH_DEFAULT_GOG_BIN32"
CONTENT_GAME_BIN32_FILES='
runner2'
CONTENT_GAME_DATA_FILES='
Effects
Fonts
Gameplay
Graphics
Menus
Models
Shaders
Sounds
Textures
package.toc
Runner2.png'
CONTENT_DOC_DATA_PATH_GOG="${CONTENT_PATH_DEFAULT_GOG}/.."
CONTENT_DOC_DATA_PATH_HUMBLE="${CONTENT_PATH_DEFAULT_HUMBLE}/.."
CONTENT_DOC_DATA_FILES='
README
README.html'

APP_MAIN_EXE='runner2'
APP_MAIN_ICON='Runner2.png'

PACKAGES_LIST_GOG='
PKG_BIN64
PKG_BIN32
PKG_DATA'
PACKAGES_LIST_HUMBLE_32BIT='
PKG_BIN32
PKG_DATA'
PACKAGES_LIST_HUMBLE_64BIT='
PKG_BIN64
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
## Ensure easy upgrade from packages generated using pre-20211125.2 game scripts.
PKG_DATA_PROVIDES="${PKG_DATA_PROVIDES:-}
runner-2-data"

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
librt.so.1
libSDL-1.2.so.0
libstdc++.so.6
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
## Ensure easy upgrade from packages generated using pre-20211125.2 game script
PKG_BIN_PROVIDES="${PKG_BIN_PROVIDES:-}
runner-2"
PKG_BIN32_PROVIDES="$PKG_BIN_PROVIDES"
PKG_BIN64_PROVIDES="$PKG_BIN_PROVIDES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

case "$(current_archive)" in
	('ARCHIVE_BASE_HUMBLE_32BIT_'*)
		launchers_generation 'PKG_BIN32'
	;;
	('ARCHIVE_BASE_HUMBLE_64BIT_'*)
		launchers_generation 'PKG_BIN64'
	;;
	('ARCHIVE_BASE_GOG_'*)
		launchers_generation 'PKG_BIN64'
		launchers_generation 'PKG_BIN32'
	;;
esac

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
