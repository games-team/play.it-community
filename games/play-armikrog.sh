#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2022 Anna Lea
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Armikrog
# send your bug reports to contact@dotslashplay.it
###

script_version=20240623.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='armikrog'
GAME_NAME='Armikrog'

ARCHIVE_BASE_LINUX_0_NAME='gog_armikrog_2.5.0.6.sh'
ARCHIVE_BASE_LINUX_0_MD5='4ca9ac6d05ac66c18229e41da6fd89dd'
ARCHIVE_BASE_LINUX_0_SIZE='2900000'
ARCHIVE_BASE_LINUX_0_VERSION='2016.01.11-gog2.5.0.6'
ARCHIVE_BASE_LINUX_0_URL='https://www.gog.com/game/armikrog'

## Support for the Windows build is provided to work around a double cursor display bug on the Linux build.
## cf. https://forge.dotslashplay.it/play.it/games/-/issues/778
ARCHIVE_BASE_WINDOWS_0_NAME='setup_armikrog_1.05_(20650).exe'
ARCHIVE_BASE_WINDOWS_0_MD5='9b18bfd56117bcdbd95434203303b941'
ARCHIVE_BASE_WINDOWS_0_TYPE='innosetup'
ARCHIVE_BASE_WINDOWS_0_SIZE='2900000'
ARCHIVE_BASE_WINDOWS_0_VERSION='1.05-gog20650'
ARCHIVE_BASE_WINDOWS_0_URL='https://www.gog.com/game/armikrog'

UNITY3D_NAME_LINUX='Armikrog'
UNITY3D_NAME_WINDOWS='armikrog'
UNITY3D_PLUGINS='
ScreenSelector.so'

CONTENT_PATH_DEFAULT_LINUX='data/noarch/game'
CONTENT_PATH_DEFAULT_WINDOWS='.'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/PencilTestStudios/Armikrog'

# Packages

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_PROVIDES="
$PKG_DATA_ID"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ID="$GAME_ID"
PKG_BIN_PROVIDES="
$PKG_BIN_ID"
PKG_BIN64_PROVIDES="$PKG_BIN_PROVIDES"
PKG_BIN32_PROVIDES="$PKG_BIN_PROVIDES"

## Linux build

PACKAGES_LIST_LINUX='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID_LINUX="${PKG_DATA_ID}-linux"

PKG_BIN64_ID_LINUX="${PKG_BIN_ID}-linux"
PKG_BIN32_ID_LINUX="${PKG_BIN_ID}-linux"
PKG_BIN64_ARCH_LINUX='64'
PKG_BIN32_ARCH_LINUX='32'
PKG_BIN_DEPS_LINUX="$PKG_DATA_ID_LINUX"
PKG_BIN64_DEPS_LINUX="$PKG_BIN_DEPS_LINUX"
PKG_BIN32_DEPS_LINUX="$PKG_BIN_DEPS_LINUX"
PKG_BIN_DEPENDENCIES_LIBRARIES_LINUX='
libatk-1.0.so.0
libcairo.so.2
libc.so.6
libdl.so.2
libfontconfig.so.1
libfreetype.so.6
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libgdk-x11-2.0.so.0
libgio-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libGLU.so.1
libgmodule-2.0.so.0
libgobject-2.0.so.0
libgthread-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpango-1.0.so.0
libpangocairo-1.0.so.0
libpangoft2-1.0.so.0
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXext.so.6
libXrandr.so.2'
PKG_BIN64_DEPENDENCIES_LIBRARIES_LINUX="$PKG_BIN_DEPENDENCIES_LIBRARIES_LINUX"
PKG_BIN32_DEPENDENCIES_LIBRARIES_LINUX="$PKG_BIN_DEPENDENCIES_LIBRARIES_LINUX"

## Windows build

PACKAGES_LIST_WINDOWS='
PKG_BIN
PKG_DATA'

PKG_DATA_ID_WINDOWS="${PKG_DATA_ID}-windows"

PKG_BIN_ID_WINDOWS="${PKG_BIN_ID}-windows"
PKG_BIN_ARCH_WINDOWS='32'
PKG_BIN_DEPS_WINDOWS="$PKG_DATA_ID_WINDOWS"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

case "$(current_archive)" in
	('ARCHIVE_BASE_LINUX_'*)
		launchers_generation 'PKG_BIN64'
		launchers_generation 'PKG_BIN32'
	;;
	('ARCHIVE_BASE_WINDOWS_'*)
		launchers_generation 'PKG_BIN'
	;;
esac

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
