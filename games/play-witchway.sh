#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Witchway
# send your bug reports to contact@dotslashplay.it
###

script_version=20240515.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='witchway'
GAME_NAME='WitchWay'

ARCHIVE_BASE_0_NAME='WitchWay 1-12 WIN .zip'
ARCHIVE_BASE_0_MD5='70bc22f82227be0008332b5f5ce10585'
ARCHIVE_BASE_0_SIZE='25000'
ARCHIVE_BASE_0_VERSION='1.12-itch'
ARCHIVE_BASE_0_URL='https://gleeson.itch.io/witchway'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_MAIN_FILES='
WitchWay.exe'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Local/WitchWay'
## Work around a crash related to shaders compilation.
WINE_WINETRICKS_VERBS='d3dcompiler_43'

APP_MAIN_EXE='WitchWay.exe'

PKG_MAIN_ARCH='32'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
