#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Mark of the Ninja
# send your bug reports to contact@dotslashplay.it
###

script_version=20230905.1

GAME_ID='mark-of-the-ninja'
GAME_NAME='Mark of the Ninja'

## This Linux build is no longer available for sale from GOG, they now only sell a Windows build.
ARCHIVE_BASE_GOG_0='gog_mark_of_the_ninja_2.0.0.4.sh'
ARCHIVE_BASE_GOG_0_MD5='126ded567b38580f574478fd994e3728'
ARCHIVE_BASE_GOG_0_TYPE='mojosetup'
ARCHIVE_BASE_GOG_0_SIZE='2200000'
ARCHIVE_BASE_GOG_0_VERSION='1.0-gog2.0.0.4'

## This game is no longer available for sale from Humble Store.
ARCHIVE_BASE_HUMBLE_0='markoftheninja_linux38_1380755375.zip'
ARCHIVE_BASE_HUMBLE_0_MD5='7871a48068ef43e93916325eedd6913e'
ARCHIVE_BASE_HUMBLE_0_SIZE='2300000'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.0-humble130310'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='.'
CONTENT_LIBS_LIBS32_PATH_GOG="${CONTENT_PATH_DEFAULT_GOG}/bin/lib32"
CONTENT_LIBS_LIBS32_PATH_HUMBLE="${CONTENT_PATH_DEFAULT_HUMBLE}/bin/lib32"
CONTENT_LIBS_LIBS32_FILES='
libfmodevent.so
libfmodevent-4.44.14.so
libfmodex.so
libfmodex-4.44.14.so'
CONTENT_LIBS_LIBS64_PATH_GOG="${CONTENT_PATH_DEFAULT_GOG}/bin/lib64"
CONTENT_LIBS_LIBS64_PATH_HUMBLE="${CONTENT_PATH_DEFAULT_HUMBLE}/bin/lib64"
CONTENT_LIBS_LIBS64_FILES='
libfmodevent64.so
libfmodevent64-4.44.14.so
libfmodex64.so
libfmodex64-4.44.14.so'
CONTENT_GAME_BIN32_FILES='
bin/ninja-bin32'
CONTENT_GAME_BIN64_FILES='
bin/ninja-bin64'
CONTENT_GAME_DATA_FILES='
data
data-pc
bin/*.xpm'
CONTENT_DOC_DATA_PATH_GOG="${CONTENT_PATH_DEFAULT_GOG}/bin"
CONTENT_DOC_DATA_PATH_HUMBLE="${CONTENT_PATH_DEFAULT_HUMBLE}/bin"
CONTENT_DOC_DATA_FILES='
motn_readme.txt'

APP_MAIN_EXE_BIN32='bin/ninja-bin32'
APP_MAIN_EXE_BIN64='bin/ninja-bin64'
APP_MAIN_ICON='bin/motn_icon.xpm'
## Run the game binary from its parent directory
APP_MAIN_PRERUN='# Run the game binary from its parent directory
cd "$(dirname "$APP_EXE")"
APP_EXE=$(basename "$APP_EXE")
'
## Work around a rendering failure with sdl12-compat
## cf. https://github.com/libsdl-org/sdl12-compat/issues/315
APP_MAIN_PRERUN="$APP_MAIN_PRERUN"'
# Work around a rendering failure with sdl12-compat
# cf. https://github.com/libsdl-org/sdl12-compat/issues/315
export SDL12COMPAT_OPENGL_SCALING=0
'

PACKAGES_LIST='PKG_BIN32 PKG_BIN64 PKG_LIBS32 PKG_LIBS64 PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_LIBS_ID="${GAME_ID}-libs"
PKG_LIBS32_ID="$PKG_LIBS_ID"
PKG_LIBS64_ID="$PKG_LIBS_ID"
PKG_LIBS32_ARCH='32'
PKG_LIBS64_ARCH='64'
PKG_LIBS_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
libstdc++.so.6'
PKG_LIBS32_DEPENDENCIES_LIBRARIES="$PKG_LIBS_DEPENDENCIES_LIBRARIES"
PKG_LIBS64_DEPENDENCIES_LIBRARIES="$PKG_LIBS_DEPENDENCIES_LIBRARIES"

PKG_BIN32_ARCH='32'
PKG_BIN64_ARCH='64'
PKG_BIN_DEPS="$PKG_LIBS_ID $PKG_DATA_ID"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
libSDL-1.2.so.0
libstdc++.so.6'
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

target_version='2.25'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

for PKG in 'PKG_BIN32' 'PKG_BIN64'; do
	# shellcheck disable=SC2119
	launchers_write
done

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
