#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# A Musical Story
# send your bug reports to contact@dotslashplay.it
###

script_version=20221231.1

GAME_ID='a-musical-story'
GAME_NAME='A Musical Story'

UNITY3D_NAME='MusicalStory'

ARCHIVE_BASE_0='a_musical_story_1_0_5_53408.sh'
ARCHIVE_BASE_0_MD5='028d274ff2f597d5fb5ed0116bee990a'
ARCHIVE_BASE_0_TYPE='mojosetup'
ARCHIVE_BASE_0_SIZE='2400000'
ARCHIVE_BASE_0_VERSION='1.0.5-gog53408'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN_FILES="
GameAssembly.so
UnityPlayer.so
${UNITY3D_NAME}_Data/Plugins
${UNITY3D_NAME}.x86_64"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data"

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID pulseaudio"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libgcc_s.so.1
libm.so.6
libpthread.so.0
libstdc++.so.6'

# Load common functions

target_version='2.20'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build package

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
