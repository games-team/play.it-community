#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Mon-cuties for All
# send your bug reports to contact@dotslashplay.it
###

script_version=20240428.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='mon-cuties-for-all'
GAME_NAME='Mon-cuties for All'

ARCHIVE_BASE_0_NAME='Mon-cuties-0.5-pc.zip'
ARCHIVE_BASE_0_MD5='5100ee55608ea481ee820e12f6bda4c9'
ARCHIVE_BASE_0_SIZE='440000'
ARCHIVE_BASE_0_VERSION='0.5-itch1'
ARCHIVE_BASE_0_URL='https://reineworks.itch.io/moncuties-for-all'

CONTENT_PATH_DEFAULT='Mon-cuties-0.5-pc/game'
CONTENT_GAME_MAIN_FILES='
cache
gui
main
sound
archive.rpa'
CONTENT_DOC_MAIN_PATH="${CONTENT_PATH_DEFAULT}/.."
CONTENT_DOC_MAIN_FILES='
README.html'

APP_MAIN_TYPE='renpy'
APP_MAIN_ICON='../Mon-cuties.exe'

## Ensure easy upgrades from packages generated with pre-20240428.1 game scripts.
PKG_MAIN_PROVIDES="${PKG_MAIN_PROVIDES:-}
mon-cuties-for-all-data"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
