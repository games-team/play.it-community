#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Westport Independent
# send your bug reports to contact@dotslashplay.it
###

script_version=20231004.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='the-westport-independent'
GAME_NAME='The Westport Independent'

## This game used to be sold by gog.com, but is no longer available for sale there.
ARCHIVE_BASE_0_NAME='gog_the_westport_independent_2.0.0.1.sh'
ARCHIVE_BASE_0_MD5='7032059f085e94f52444e9bf4ed0195c'
ARCHIVE_BASE_0_SIZE='130000'
ARCHIVE_BASE_0_VERSION='1.0.0-gog2.0.0.1'

CONTENT_PATH_DEFAULT='data/noarch/game/linux32'
CONTENT_GAME_BIN32_FILES='
the_westport_independent'
CONTENT_GAME_BIN64_PATH="${CONTENT_PATH_DEFAULT}/../linux64"
CONTENT_GAME_BIN64_FILES='
the_westport_independent'
CONTENT_GAME_DATA_FILES='
config.json
assets'
CONTENT_DOC_DATA_FILES='
*.txt'

USER_PERSISTENT_FILES='
config.json'

APP_MAIN_EXE='the_westport_independent'

PACKAGES_LIST='PKG_BIN32 PKG_BIN64 PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN32_ARCH='32'
PKG_BIN64_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
librt.so.1
libstdc++.so.6'
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
ARCHIVE_OPTIONAL_ICONS_0_NAME='the-westport-independent_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_0_MD5='054b51fe4c02f256b8130b40e93d28ae'
ARCHIVE_OPTIONAL_ICONS_0_URL='https://downloads.dotslashplay.it/games/the-westport-independent'
archive_initialize_optional \
	'ARCHIVE_ICONS' \
	'ARCHIVE_OPTIONAL_ICONS'
## TODO: The library should provide a function to check if a given optional archive has been provided.
if [ -n "$(archive_path 'ARCHIVE_ICONS' 2>/dev/null || true)" ]; then
	archive_extraction 'ARCHIVE_ICONS'
fi

# Include game data

## TODO: The library should provide a function to check if a given optional archive has been provided.
if [ -n "$(archive_path 'ARCHIVE_ICONS' 2>/dev/null || true)" ]; then
	## Include the original game icons from the optional archive…
	CONTENT_ICONS_DATA_PATH='.'
	CONTENT_ICONS_DATA_FILES='
	16x16
	32x32
	48x48
	256x256'
	content_inclusion 'ICONS_DATA' 'PKG_DATA' "$(path_icons)"
else
	## …or fall back on the GOG-specific icon.
	PKG='PKG_DATA'
	APP_MAIN_ICON='../../support/icon.png'
	# shellcheck disable=SC2119
	icons_inclusion
fi
content_inclusion_default

# Write launchers

PKG='PKG_BIN32'
# shellcheck disable=SC2119
launchers_write
PKG='PKG_BIN64'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
