#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Bit.Trip: Runner 3
# send your bug reports to contact@dotslashplay.it
###

script_version=20240424.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='bit-trip-runner-3'
GAME_NAME='Bit.Trip: Runner 3'

ARCHIVE_BASE_0_NAME='Runner3WIN.zip'
ARCHIVE_BASE_0_MD5='d13f6fa59e9b55a259a078328a87c9f9'
ARCHIVE_BASE_0_SIZE='2700000'
ARCHIVE_BASE_0_VERSION='2018.11.12-itch1'
ARCHIVE_BASE_0_URL='https://choice-provisions.itch.io/runner3'

UNITY3D_NAME='Runner3'

CONTENT_PATH_DEFAULT='.'

## Despite the unusual path for a Unity3D game, saved games are .sav files at the root of the game directory.
USER_PERSISTENT_FILES='
*.sav'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
