#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Stanley Parable
# send your bug reports to contact@dotslashplay.it
###

script_version=20231114.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='the-stanley-parable'
GAME_NAME='The Stanley Parable'

ARCHIVE_BASE_0_NAME='The_Stanley_Parable_Setup.tar'
ARCHIVE_BASE_0_MD5='10a98d7fb93017eb666281bf2d3da28d'
ARCHIVE_BASE_0_SIZE='2100000'
ARCHIVE_BASE_0_VERSION='1.0-humble161007'
ARCHIVE_BASE_0_URL='https://www.humblebundle.com/store/the-stanley-parable'

CONTENT_PATH_DEFAULT='data'
CONTENT_LIBS_BIN_PATH="${CONTENT_PATH_DEFAULT}/bin"
CONTENT_LIBS_BIN_FILES='
bsppack.so
crashhandler.so
datacache.so
dedicated.so
engine.so
filesystem_stdio.so
inputsystem.so
launcher.so
libtier0.so
libtogl.so
libvpx.so.1
libvpx.so.1.0
libvpx.so.1.0.0
libvstdlib.so
localize.so
materialsystem.so
puzzlemaker_dll.so
scenefilecache.so
shaderapidx9.so
shaderapiempty.so
soundemittersystem.so
stdshader_dx9.so
studiorender.so
valve_avi.so
vaudio_speex.so
vgui2.so
vguimatsurface.so
vphysics.so
vrad_dll.so
vscript.so
vvis_dll.so'
## Several shipped libraries rely on the Steam libraries.
CONTENT_LIBS_BIN_FILES="${CONTENT_LIBS_BIN_FILES:-}
libsteam_api_linux.so
libsteam_api.so
libsteam.so"
CONTENT_GAME_BIN_FILES='
bin/*.bin
bin/*.cfg
bin/*.dat
bin/*.fgd
bin/*.so
bin/*.txt
bin/vbsp_linux
bin/vrad_linux
bin/vvis_linux
thestanleyparable/bin
stanley_linux'
CONTENT_GAME_DATA_FILES='
platform
stanley.png
thestanleyparable'
CONTENT_DOC_DATA_FILES='
thirdpartylegalnotices.doc'

USER_PERSISTENT_DIRECTORIES='
thestanleyparable/cfg
thestanleyparable/save'
USER_PERSISTENT_FILES='
thestanleyparable/stats.vdf'

APP_MAIN_EXE='stanley_linux'
APP_MAIN_ICON='stanley.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libfontconfig.so.1
libfreetype.so.6
libgcc_s.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libstdc++.so.6
libtcmalloc_minimal.so.4
libuuid.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Some libraries are only found by the game engine if they are in a hardcoded path.

libraries_path_source=$(path_libraries)
libraries_path_destination="$(package_path 'PKG_BIN')$(path_game_data)/bin"
mkdir --parents "$libraries_path_destination"
ln --symbolic \
	"${libraries_path_source}/launcher.so" \
	"${libraries_path_source}/filesystem_stdio.so" \
	"$libraries_path_destination"

# Extract game data

archive_extraction_default
ARCHIVE_INNER_1_PATH="${PLAYIT_WORKDIR}/gamedata/The Stanley Parable Setup"
## This MojoSetup installer is not based on Makeself.
ARCHIVE_INNER_1_EXTRACTOR='bsdtar'
archive_extraction 'ARCHIVE_INNER_1'
rm "$(archive_path 'ARCHIVE_INNER_1')"
ARCHIVE_INNER_2_PATH="${PLAYIT_WORKDIR}/gamedata/data/tsp.tar"
archive_extraction 'ARCHIVE_INNER_2'
rm "$(archive_path 'ARCHIVE_INNER_2')"

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
