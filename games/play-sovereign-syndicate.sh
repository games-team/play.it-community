#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Sovereign Syndicate
# send your bug reports to contact@dotslashplay.it
###

script_version=20241124.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='sovereign-syndicate'
GAME_NAME='Sovereign Syndicate'

ARCHIVE_BASE_0_NAME='setup_sovereign_syndicate_1.0.36_(64bit)_(72092).exe'
ARCHIVE_BASE_0_MD5='266029b31e13dc11b808cd87b4b31eb9'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_sovereign_syndicate_1.0.36_(64bit)_(72092)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='8872971818c95eee27f80f7684209c1c'
ARCHIVE_BASE_0_PART2_NAME='setup_sovereign_syndicate_1.0.36_(64bit)_(72092)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='3de81fad53bfddf5c3eaa6c6598af2b7'
ARCHIVE_BASE_0_SIZE='11071403'
ARCHIVE_BASE_0_VERSION='1.0.36-gog72092'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/sovereign_syndicate'

UNITY3D_NAME='sovereign syndicate'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_DATA_SHAREDASSETS_FILES="
${UNITY3D_NAME}_data/sharedassets*"

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Crimson Herring Studios/Sovereign Syndicate'

PACKAGES_LIST='
PKG_BIN
PKG_DATA_SHAREDASSETS
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPENDENCIES_SIBLINGS='
PKG_DATA_SHAREDASSETS'

PKG_DATA_SHAREDASSETS_ID="${PKG_DATA_ID}-sharedassets"
PKG_DATA_SHAREDASSETS_DESCRIPTION="$PKG_DATA_DESCRIPTION - shared assets"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
