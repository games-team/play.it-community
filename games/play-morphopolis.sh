#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Morphopolis
# send your bug reports to contact@dotslashplay.it
###

script_version=20240728.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='morphopolis'
GAME_NAME='Morphopolis'

ARCHIVE_BASE_0_NAME='setup_morphopolis_1.0_(22453).exe'
ARCHIVE_BASE_0_MD5='325f1bbbcc1c7a657530d70db9f1bb1a'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='1200000'
ARCHIVE_BASE_0_VERSION='1.0-gog22453'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/morphopolis'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
irrklang.dll
project.exe
app.config.txt
app.icf'
CONTENT_GAME_DATA_FILES='
anims
font
sounds
textures
*.xml'

USER_PERSISTENT_FILES='
app.config.txt'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Roaming/Morphopolis'

APP_MAIN_EXE='project.exe'
APP_MAIN_ICON='app/goggame-1559432711.ico'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# icotool is used for icon .ico → .png conversion

SCRIPT_DEPS="${SCRIPT_DEPS:-} icotool"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

## Work around "insufficient image data" issue with convert from imagemagick.
icon_extract_png_from_ico() {
	local icon destination
	icon="$1"
	destination="$2"

	local icon_file
	icon_file=$(icon_full_path "$icon")
	icotool --extract --output="$destination" "$icon_file"
}
content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
