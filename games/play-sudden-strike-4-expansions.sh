#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Sudden Strike 4 expansions:
# - Battle of Kursk
# - Road to Dunkirk
# - The Pacific War
# - Finland: Winter Storm
# - Africa: Desert War
# send your bug reports to contact@dotslashplay.it
###

script_version=20240331.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='sudden-strike-4'
GAME_NAME='Sudden Strike 4'

EXPANSION_ID_KURSK='battle-of-kursk'
EXPANSION_NAME_KURSK='Battle of Kursk'

EXPANSION_ID_DUNKIRK='road-to-dunkirk'
EXPANSION_NAME_DUNKIRK='Road to Dunkirk'

EXPANSION_ID_PACIFIC='the-pacific-war'
EXPANSION_NAME_PACIFIC='The Pacific War'

EXPANSION_ID_FINLAND='finland-winter-storm'
EXPANSION_NAME_FINLAND='Finland: Winter Storm'

EXPANSION_ID_AFRICA='africa-desert-war'
EXPANSION_NAME_AFRICA='Africa: Desert War'

# Archives

## Battle of Kursk

ARCHIVE_BASE_KURSK_0_NAME='sudden_strike_4_battle_of_kursk_1_15_hotfix_28522.sh'
ARCHIVE_BASE_KURSK_0_MD5='adaf1b27f954cc90c32c951334c45916'
ARCHIVE_BASE_KURSK_0_SIZE='56000'
ARCHIVE_BASE_KURSK_0_VERSION='1.15-gog28522'

## Road to Dunkirk

ARCHIVE_BASE_DUNKIRK_0_NAME='sudden_strike_4_road_to_dunkirk_1_15_hotfix_28522.sh'
ARCHIVE_BASE_DUNKIRK_0_MD5='cd07c173ad84e83db50dd3fce310d5a5'
ARCHIVE_BASE_DUNKIRK_0_SIZE='160000'
ARCHIVE_BASE_DUNKIRK_0_VERSION='1.15-gog28522'
ARCHIVE_BASE_DUNKIRK_0_URL='https://www.gog.com/game/sudden_strike_4_road_to_dunkirk'

## The Pacific War

ARCHIVE_BASE_PACIFIC_0_NAME='sudden_strike_4_the_pacific_war_1_15_hotfix_28522.sh'
ARCHIVE_BASE_PACIFIC_0_MD5='9a5a4c8ca66814bfee11203dd877193c'
ARCHIVE_BASE_PACIFIC_0_SIZE='560000'
ARCHIVE_BASE_PACIFIC_0_VERSION='1.15-gog28522'
ARCHIVE_BASE_PACIFIC_0_URL='https://www.gog.com/game/sudden_strike_4_the_pacific_war'

## Finland: Winter Storm

ARCHIVE_BASE_FINLAND_0_NAME='sudden_strike_4_finland_winter_storm_1_15_hotfix_28522.sh'
ARCHIVE_BASE_FINLAND_0_MD5='d51ded65499136f36e613ca35cb583d8'
ARCHIVE_BASE_FINLAND_0_SIZE='330000'
ARCHIVE_BASE_FINLAND_0_VERSION='1.15-gog28522'
ARCHIVE_BASE_FINLAND_0_URL='https://www.gog.com/game/sudden_strike_4_the_pacific_war'

## Africa: Desert War

ARCHIVE_BASE_AFRICA_0_NAME='sudden_strike_4_africa_desert_war_1_15_hotfix_28522.sh'
ARCHIVE_BASE_AFRICA_0_MD5='9fd0469913dda4a63c6dd7d87736c936'
ARCHIVE_BASE_AFRICA_0_SIZE='370000'
ARCHIVE_BASE_AFRICA_0_VERSION='1.15-gog28522'
ARCHIVE_BASE_AFRICA_0_URL='https://www.gog.com/game/sudden_strike_4_africa_desert_war'


CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
AssetBundles'

PKG_MAIN_DEPS="$GAME_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
