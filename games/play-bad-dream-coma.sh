#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Bad Dream: Coma
# send your bug reports to contact@dotslashplay.it
###

script_version=20231011.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='bad-dream-coma'
GAME_NAME='Bad Dream: Coma'

ARCHIVE_BASE_0_NAME='setup_bad_dream_coma_1.0_(26985).exe'
ARCHIVE_BASE_0_MD5='e4f0655ee22d99bdf09d0ee7a6980f23'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='4100000'
ARCHIVE_BASE_0_VERSION='1.0-gog26985'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/bad_dream_coma'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
bad dream coma.exe
d3dx9_43.dll
data.win
options.ini'
CONTENT_GAME_DATA_FILES='
*.dat
*.ogg'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Local Settings/Application Data/Bad_Dream_Coma'

APP_MAIN_EXE='bad dream coma.exe'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
