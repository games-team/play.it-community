#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2018 Andrey Butirsky
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2020 macaron
set -o errexit

###
# Little Big Adventure 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20230420.1

GAME_ID='little-big-adventure-2'
GAME_NAME='Little Big Adventure 2'

ARCHIVE_BASE_1='setup_little_big_adventure_2_1.0_(28192).exe'
ARCHIVE_BASE_1_MD5='80b95bb8faa2353284b321748021da16'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='750000'
ARCHIVE_BASE_1_VERSION='1.0-gog28192'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/little_big_adventure_2'

ARCHIVE_BASE_0='setup_lba2_2.1.0.8.exe'
ARCHIVE_BASE_0_MD5='9909163b7285bd37417f6d3c1ccfa3ee'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='750000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.1.0.8'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_MAIN_FILES='
DRIVERS
LBA2.DAT
LBA2.GOG
LBA2.OGG
*.BAT
*.CFG
*.DOS
*.EXE
*.INI
*.HQR
*.ILE
*.OBL'
CONTENT_DOC_MAIN_FILES='
*.PDF
*.TXT'

GAME_IMAGE='LBA2.DAT'

USER_PERSISTENT_FILES='
*.CFG'
USER_PERSISTENT_DIRECTORIES='
SAVE'

APP_MAIN_EXE='LBA2.EXE'
APP_MAIN_ICON='APP/GOGGAME-1207658974.ICO'
APP_MAIN_ICON_0='GOGGAME-1207658974.ICO'
## Force application type, as LBA2.EXE is detected as a Windows executable.
APP_MAIN_TYPE='dosbox'

APP_SETUP_ID="${GAME_ID}-setup"
APP_SETUP_NAME="$GAME_NAME - Setup"
APP_SETUP_EXE='SETUP.EXE'
APP_SETUP_CAT='Settings'
APP_SETUP_ICON='APP/GOGGAME-1207658974.ICO'
APP_SETUP_ICON_0='GOGGAME-1207658974.ICO'

# Load common functions

target_version='2.23'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Convert all file names to upper case
	toupper .

	# Store voices outside of the game image
	sed_pattern='s/\(FlagKeepVoice:\) OFF/\1 ON/'
	sed --in-place "$sed_pattern" 'LBA2.CFG'
)

# Include game icons

# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
