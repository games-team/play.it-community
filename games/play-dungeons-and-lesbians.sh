#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Dungeons and Lesbians
# send your bug reports to contact@dotslashplay.it
###

script_version=20240328.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='dungeons-and-lesbians'
GAME_NAME='Dungeons & Lesbians'

ARCHIVE_BASE_0_NAME='DungeonsAndLesbians-1.2-linux.tar.bz2'
ARCHIVE_BASE_0_MD5='31015bbbc250043dca3e77c96941655b'
ARCHIVE_BASE_0_SIZE='200000'
ARCHIVE_BASE_0_VERSION='1.2-itch1'
ARCHIVE_BASE_0_URL='https://noeybodys.itch.io/dungeonsandlesbians'

CONTENT_PATH_DEFAULT='DungeonsAndLesbians-1.2-linux'
CONTENT_LIBS_BIN_FILES='
libavcodec.so.57
libavformat.so.57
libavresample.so.3
libavutil.so.55
libGLEW.so.1.7
libpython2.7.so.1.0
libswresample.so.2
libswscale.so.4'
## libpng12.so.0 is not only a dependency of the shipped SDL2 build,
## it is actually required to prevent a crash on launch.
CONTENT_LIBS_BIN_FILES="${CONTENT_LIBS_BIN_FILES:-}
libpng12.so.0"
CONTENT_LIBS_BIN64_PATH="${CONTENT_PATH_DEFAULT}/lib/linux-x86_64"
CONTENT_LIBS_BIN64_FILES="$CONTENT_LIBS_BIN_FILES"
CONTENT_LIBS_BIN32_PATH="${CONTENT_PATH_DEFAULT}/lib/linux-i686"
CONTENT_LIBS_BIN32_FILES="$CONTENT_LIBS_BIN_FILES"
CONTENT_GAME_BIN64_FILES='
lib/linux-x86_64/eggs
lib/linux-x86_64/lib
lib/linux-x86_64/python
lib/linux-x86_64/pythonw
lib/linux-x86_64/zsync
lib/linux-x86_64/zsyncmake
lib/linux-x86_64/DungeonsAndLesbians'
CONTENT_GAME_BIN32_FILES='
lib/linux-i686/eggs
lib/linux-i686/lib
lib/linux-i686/python
lib/linux-i686/pythonw
lib/linux-i686/zsync
lib/linux-i686/zsyncmake
lib/linux-i686/DungeonsAndLesbians'
CONTENT_GAME_DATA_FILES='
game
renpy
lib/pythonlib2.7
DungeonsAndLesbians.py'

APP_MAIN_EXE_BIN64='lib/linux-x86_64/DungeonsAndLesbians'
APP_MAIN_EXE_BIN32='lib/linux-i686/DungeonsAndLesbians'
APP_MAIN_OPTIONS='-EO DungeonsAndLesbians.py'
APP_MAIN_ICONS_LIST='APP_MAIN_ICON_ICO APP_MAIN_ICON_PNG'
APP_MAIN_ICON_PNG='game/icon.png'
APP_MAIN_ICON_ICO='game/icon.ico'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libGL.so.1
libGLU.so.1
libm.so.6
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libSDL2_image-2.0.so.0
libSDL2_ttf-2.0.so.0
libutil.so.1
libX11.so.6
libXext.so.6
libXi.so.6
libXmu.so.6
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN64'
# shellcheck disable=SC2119
launchers_write
set_current_package 'PKG_BIN32'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
