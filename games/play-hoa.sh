#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Hoa
# send your bug reports to contact@dotslashplay.it
###

script_version=20240707.2

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='hoa-game'
GAME_NAME='Hoa'

ARCHIVE_BASE_0_NAME='setup_hoa_1.03_(49429).exe'
ARCHIVE_BASE_0_MD5='d713ecdb56059e5b4ee430d2f0e2731d'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='3200000'
ARCHIVE_BASE_0_VERSION='1.03-gog49429'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/hoa'

UNITY3D_NAME='hoa'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME0_BIN_FILES='
fmodstudio.dll
xinputinterface32.dll'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/PMStudioGoG/Hoa'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
## Ensure easy upgrades from packages generated with pre-20240707.2 game scripts.
PKG_DATA_PROVIDES="${PKG_DATA_PROVIDES:-}
hoa-data"

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
## Ensure easy upgrades from packages generated with pre-20240707.2 game scripts.
PKG_BIN_PROVIDES="${PKG_BIN_PROVIDES:-}
hoa"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
