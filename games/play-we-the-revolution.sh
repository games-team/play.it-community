#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# We. The Revolution
# send your bug reports to contact@dotslashplay.it
###

script_version=20241107.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='we-the-revolution'
GAME_NAME='We. The Revolution'

ARCHIVE_BASE_0_NAME='we_the_revolution_1_3_0_31907.sh'
ARCHIVE_BASE_0_MD5='fafc1df1317fdfc73223367ed19eea52'
ARCHIVE_BASE_0_SIZE='2997578'
ARCHIVE_BASE_0_VERSION='1.3.0-gog31907'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/we_the_revolution'

UNITY3D_NAME='We.TheRevolution'
UNITY3D_PLUGINS='
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

## Include the Steam libraries explicitly, as UNITY3D_PLUGINS can not handle a mix of architectures in a single path
## TODO: Check if the inclusion of the Steam libraries is required
install -D --mode=644 \
	"${PLAYIT_WORKDIR}/gamedata/$(content_path_default)/$(unity3d_name)_Data/Plugins/libsteam_api64.so" \
	"$(package_path 'PKG_BIN64')$(path_libraries)/libsteam_api64.so"
install -D --mode=644 \
	"${PLAYIT_WORKDIR}/gamedata/$(content_path_default)/$(unity3d_name)_Data/Plugins/libsteam_api.so" \
	"$(package_path 'PKG_BIN32')$(path_libraries)/libsteam_api.so"
rm \
	"${PLAYIT_WORKDIR}/gamedata/$(content_path_default)/$(unity3d_name)_Data/Plugins/libsteam_api64.so" \
	"${PLAYIT_WORKDIR}/gamedata/$(content_path_default)/$(unity3d_name)_Data/Plugins/libsteam_api.so"

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
