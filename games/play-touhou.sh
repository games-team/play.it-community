#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 BetaRays
# SPDX-FileCopyrightText: © 2019 Emmanuel Gil Peyrot <linkmauve@linkmauve.fr>
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Touhou series:
# - Touhou 6
# - Touhou 9
# - Touhou 11
# - Touhou 12
# - Touhou 13
# - Touhou 14
# - Touhou 16.5
# send your bug reports to contact@dotslashplay.it
###

script_version=20241103.2

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID_EPISODE6='touhou-6'
GAME_NAME_EPISODE6='Touhou Koumakyou ~ the Embodiment of Scarlet Devil'

GAME_ID_EPISODE6_DEMO="${GAME_ID_EPISODE6}-demo"
GAME_NAME_EPISODE6_DEMO="$GAME_NAME_EPISODE6 (demo)"

GAME_ID_EPISODE9='touhou-9'
GAME_NAME_EPISODE9='Touhou Kaeizuka ~ Phantasmagoria of Flower View'

GAME_ID_EPISODE9_DEMO="${GAME_ID_EPISODE9}-demo"
GAME_NAME_EPISODE9_DEMO="$GAME_NAME_EPISODE9 (demo)"

GAME_ID_EPISODE11='touhou-11'
GAME_NAME_EPISODE11='Touhou Chireiden ~ Subterranean Animism'

GAME_ID_EPISODE11_DEMO="${GAME_ID_EPISODE11}-demo"
GAME_NAME_EPISODE11_DEMO="$GAME_NAME_EPISODE11 (demo)"

GAME_ID_EPISODE12='touhou-12'
GAME_NAME_EPISODE12='Touhou Seirensen ~ Undefined Fantastic Object'

GAME_ID_EPISODE12_DEMO="${GAME_ID_EPISODE12}-demo"
GAME_NAME_EPISODE12_DEMO="$GAME_NAME_EPISODE12 (demo)"

GAME_ID_EPISODE13='touhou-13'
GAME_NAME_EPISODE13='Touhou Touhou Shinreibyou ~ Ten Desires'

GAME_ID_EPISODE13_DEMO="${GAME_ID_EPISODE13}-demo"
GAME_NAME_EPISODE13_DEMO="$GAME_NAME_EPISODE13 (demo)"

GAME_ID_EPISODE14='touhou-14'
GAME_NAME_EPISODE14='Touhou Kishinjou ~ Double Dealing Character'

GAME_ID_EPISODE14_DEMO="${GAME_ID_EPISODE14}-demo"
GAME_NAME_EPISODE14_DEMO="$GAME_NAME_EPISODE14 (demo)"

GAME_ID_EPISODE165='touhou-16-5'
GAME_NAME_EPISODE165='Touhou Hifuu Nightmare Diary ~ Violet Detector'

ARCHIVE_BASE_EPISODE6_DEMO_0_NAME='kouma_tr013.lzh'
ARCHIVE_BASE_EPISODE6_DEMO_0_MD5='7ea4be414a7f256429a2c5e4666c9881'
ARCHIVE_BASE_EPISODE6_DEMO_0_TYPE='lha'
ARCHIVE_BASE_EPISODE6_DEMO_0_SIZE='4500'
ARCHIVE_BASE_EPISODE6_DEMO_0_VERSION='0.13-zun1'
ARCHIVE_BASE_EPISODE6_DEMO_0_URL='http://www16.big.or.jp/~zun/html/th06.html'

## All download links behind the provided URL seem to be dead,
## despite the download page itself still being available.
ARCHIVE_BASE_EPISODE9_DEMO_0_NAME='kaei_ver002.lzh'
ARCHIVE_BASE_EPISODE9_DEMO_0_MD5='e07878f414404ba2157c4f646ccf3708'
ARCHIVE_BASE_EPISODE9_DEMO_0_TYPE='lha'
ARCHIVE_BASE_EPISODE9_DEMO_0_SIZE='29000'
ARCHIVE_BASE_EPISODE9_DEMO_0_VERSION='0.02a-zun1'
ARCHIVE_BASE_EPISODE9_DEMO_0_URL='https://www16.big.or.jp/~zun/html/th09dl.html'

ARCHIVE_BASE_EPISODE11_DEMO_0_NAME='th11tr002a_setup.exe'
ARCHIVE_BASE_EPISODE11_DEMO_0_MD5='9b7c092a529fcc1f48590f0a2b3cca87'
## Do not try to convert file paths to lowercase.
ARCHIVE_BASE_EPISODE11_DEMO_0_EXTRACTOR='innoextract'
ARCHIVE_BASE_EPISODE11_DEMO_0_EXTRACTOR_OPTIONS=' '
ARCHIVE_BASE_EPISODE11_DEMO_0_SIZE='140000'
ARCHIVE_BASE_EPISODE11_DEMO_0_VERSION='0.02a-zun1'
ARCHIVE_BASE_EPISODE11_DEMO_0_URL='https://www16.big.or.jp/~zun/html/th11top.html'

ARCHIVE_BASE_EPISODE12_DEMO_0_NAME='th12tr002a_setup.exe'
ARCHIVE_BASE_EPISODE12_DEMO_0_MD5='61a77c94c2ef64d7afda477dda0594eb'
ARCHIVE_BASE_EPISODE12_DEMO_0_TYPE='innosetup'
ARCHIVE_BASE_EPISODE12_DEMO_0_SIZE='151620'
ARCHIVE_BASE_EPISODE12_DEMO_0_VERSION='0.02a-zun1'
ARCHIVE_BASE_EPISODE12_DEMO_0_URL='http://www16.big.or.jp/~zun/html/th12dl.html'

ARCHIVE_BASE_EPISODE13_DEMO_0_NAME='th13tr001a_setup.exe'
ARCHIVE_BASE_EPISODE13_DEMO_0_MD5='5336b10545fd0b6cb0eb38c97199e9bc'
ARCHIVE_BASE_EPISODE13_DEMO_0_TYPE='innosetup'
ARCHIVE_BASE_EPISODE13_DEMO_0_SIZE='190000'
ARCHIVE_BASE_EPISODE13_DEMO_0_VERSION='0.01a-zun1'
ARCHIVE_BASE_EPISODE13_DEMO_0_URL='http://www16.big.or.jp/~zun/html/th13dl.html'

## This DRM-free archive is no longer available from Playism store,
## since their owners closed it down in favour of Steam.
ARCHIVE_BASE_EPISODE14_DEMO_0_NAME='DoubleDealingCharacterDemo.zip'
ARCHIVE_BASE_EPISODE14_DEMO_0_MD5='76a751e8becb51689c2256d218cda788'
ARCHIVE_BASE_EPISODE14_DEMO_0_SIZE='190000'
ARCHIVE_BASE_EPISODE14_DEMO_0_VERSION='0.01b-playism'

ARCHIVE_BASE_EPISODE165_DISC_0_NAME='violet-detector.iso'
ARCHIVE_BASE_EPISODE165_DISC_0_MD5='d6198341c3c92befbeb713fdccc189e7'
ARCHIVE_BASE_EPISODE165_DISC_0_VERSION='1.00a-disc'
ARCHIVE_BASE_EPISODE165_DISC_0_SIZE='370000'

ARCHIVE_BASE_EPISODE165_DISC_RAW_0_NAME='violet-detector-raw.iso'
ARCHIVE_BASE_EPISODE165_DISC_RAW_0_MD5='7bbcf834290e33c0bb656a43a9d39ffe'
ARCHIVE_BASE_EPISODE165_DISC_RAW_0_VERSION='1.00a-disc'
ARCHIVE_BASE_EPISODE165_DISC_RAW_0_SIZE='370000'

CONTENT_PATH_DEFAULT_EPISODE6_DEMO='東方紅魔郷　体験版'
CONTENT_PATH_DEFAULT_EPISODE9_DEMO='kaei'
CONTENT_PATH_DEFAULT_EPISODE11_DEMO='app'
CONTENT_PATH_DEFAULT_EPISODE12_DEMO='app'
CONTENT_PATH_DEFAULT_EPISODE13_DEMO='app'
CONTENT_PATH_DEFAULT_EPISODE14_DEMO='Double Dealing Character DEMO (Touhou14)'
CONTENT_PATH_DEFAULT_EPISODE165='th165'
CONTENT_GAME_BIN_FILES='
*.exe'
CONTENT_GAME_DATA_FILES='
*.dat
*.DAT'
CONTENT_DOC_DATA_FILES='
html
manual
マニュアル
index.html
*.txt'

USER_PERSISTENT_DIRECTORIES_EPISODE6_DEMO='
replay'
USER_PERSISTENT_DIRECTORIES_EPISODE9_DEMO='
replay'
USER_PERSISTENT_DIRECTORIES_EPISODE12_DEMO='
replay'
USER_PERSISTENT_DIRECTORIES_EPISODE13_DEMO='
replay'
USER_PERSISTENT_FILES_EPISODE6_DEMO='
東方紅魔郷.cfg
score.dat
log.txt'
USER_PERSISTENT_FILES_EPISODE9_DEMO='
th09tr.cfg
score.dat
log.txt'
USER_PERSISTENT_FILES_EPISODE11_DEMO='
h11.cfg
scoreth11.dat
log.txt'
USER_PERSISTENT_FILES_EPISODE12_DEMO='
th12.cfg
score.dat
log.txt'
USER_PERSISTENT_FILES_EPISODE13_DEMO='
th13.cfg
score.dat
log.txt'

WINE_PERSISTENT_DIRECTORIES_EPISODE14_DEMO='
users/${USER}/AppData/Roaming/ShanghaiAlice/th14tr'
WINE_PERSISTENT_DIRECTORIES_EPISODE165='
users/${USER}/AppData/Roaming/ShanghaiAlice/th165'

APP_MAIN_EXE_EPISODE6_DEMO='東方紅魔郷.exe'
APP_MAIN_EXE_EPISODE9_DEMO='th09tr.exe'
APP_MAIN_EXE_EPISODE11_DEMO='th11.exe'
APP_MAIN_EXE_EPISODE12_DEMO='th12.exe'
APP_MAIN_EXE_EPISODE13_DEMO='th13.exe'
APP_MAIN_EXE_EPISODE14_DEMO='th14.exe'
APP_MAIN_EXE_EPISODE165='th165.exe'
APP_MAIN_PRERUN='export LANG=ja_JP.UTF-8'

APP_CONFIG_ID_EPISODE6_DEMO="${GAME_ID_EPISODE6_DEMO}-config"
APP_CONFIG_ID_EPISODE9_DEMO="${GAME_ID_EPISODE9_DEMO}-config"
APP_CONFIG_ID_EPISODE11_DEMO="${GAME_ID_EPISODE11_DEMO}-config"
APP_CONFIG_ID_EPISODE12_DEMO="${GAME_ID_EPISODE12_DEMO}-config"
APP_CONFIG_ID_EPISODE13_DEMO="${GAME_ID_EPISODE13_DEMO}-config"
APP_CONFIG_ID_EPISODE14_DEMO="${GAME_ID_EPISODE14_DEMO}-config"
APP_CONFIG_ID_EPISODE165="${GAME_ID_EPISODE165}-config"
APP_CONFIG_NAME_EPISODE6_DEMO="$GAME_NAME_EPISODE6_DEMO - configuration"
APP_CONFIG_NAME_EPISODE9_DEMO="$GAME_NAME_EPISODE9_DEMO - configuration"
APP_CONFIG_NAME_EPISODE11_DEMO="$GAME_NAME_EPISODE11_DEMO - configuration"
APP_CONFIG_NAME_EPISODE12_DEMO="$GAME_NAME_EPISODE12_DEMO - configuration"
APP_CONFIG_NAME_EPISODE13_DEMO="$GAME_NAME_EPISODE13_DEMO - configuration"
APP_CONFIG_NAME_EPISODE14_DEMO="$GAME_NAME_EPISODE14_DEMO - configuration"
APP_CONFIG_NAME_EPISODE165="$GAME_NAME_EPISODE165 - configuration"
APP_CONFIG_CAT='Settings'
APP_CONFIG_EXE='custom.exe'
APP_CONFIG_PRERUN='export LANG=ja_JP.UTF-8'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
## TODO: A dependency on the wqy-microhei.ttc font should be added.

PKG_DATA_ID_EPISODE6_DEMO="${GAME_ID_EPISODE6_DEMO}-data"
PKG_DATA_ID_EPISODE9_DEMO="${GAME_ID_EPISODE9_DEMO}-data"
PKG_DATA_ID_EPISODE11_DEMO="${GAME_ID_EPISODE11_DEMO}-data"
PKG_DATA_ID_EPISODE12_DEMO="${GAME_ID_EPISODE12_DEMO}-data"
PKG_DATA_ID_EPISODE13_DEMO="${GAME_ID_EPISODE13_DEMO}-data"
PKG_DATA_ID_EPISODE14_DEMO="${GAME_ID_EPISODE14_DEMO}-data"
PKG_DATA_ID_EPISODE165="${GAME_ID_EPISODE165}-data"
PKG_DATA_DESCRIPTION='data'

# Convert the text files to UTF-8 encoding

SCRIPT_DEPS="${SCRIPT_DEPS:-} iconv"
SCRIPT_DEPS_EPISODE6_DEMO="${SCRIPT_DEPS:-} convmv"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Check for the presence of extra requirements

SCRIPT_DEPS=$(context_value 'SCRIPT_DEPS')
check_deps

# Extract game data

archive_extraction_default
(
	## Not going into $CONTENT_PATH_DEFAULT is expected.
	cd "${PLAYIT_WORKDIR}/gamedata"

	## Convert all file paths to UTF-8 encoding.
	case "$(current_archive)" in
		('ARCHIVE_BASE_EPISODE6_DEMO_'*)
			convmv -f CP932 -t UTF-8 --notest -r . >/dev/null
		;;
	esac
)
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Convert the text files to UTF-8 encoding.
	find . \( -name '*.txt' -o -name '*.html' \) -exec \
		sh -c 'contents=$(iconv --from-code CP932 --to-code UTF-8 "$1"); printf "%s" "$contents" > "$1"' -- '{}' \;
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
