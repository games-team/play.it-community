#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Fall
# send your bug reports to contact@dotslashplay.it
###

script_version=20241130.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='the-fall'
GAME_NAME='The Fall'

ARCHIVE_BASE_1_NAME='TheFall_Linux_2_5.zip'
ARCHIVE_BASE_1_MD5='5493c159ce23d13d68b60f064ab37297'
ARCHIVE_BASE_1_SIZE='345335'
ARCHIVE_BASE_1_VERSION='2.5-humble171207'
ARCHIVE_BASE_1_URL='https://www.humblebundle.com/store/the-fall'

ARCHIVE_BASE_0_NAME='TheFall_2_31_Linux.rar'
ARCHIVE_BASE_0_MD5='ffac594dc2c9b9e446da5fa375aac6fa'
ARCHIVE_BASE_0_SIZE='340000'
ARCHIVE_BASE_0_VERSION='2.31-humble161116'

UNITY3D_NAME='TheFall'
UNITY3D_PLUGINS='
ScreenSelector.so'

CONTENT_PATH_DEFAULT='.'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
