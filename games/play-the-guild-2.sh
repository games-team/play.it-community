#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Guild 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20231109.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='the-guild-2'
GAME_NAME='The Guild 2'

ARCHIVE_BASE_0_NAME='setup_the_guild2_2.0.0.4.exe'
ARCHIVE_BASE_0_MD5='2b14191f4831b3486e95d52245d6cc64'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='2400000'
ARCHIVE_BASE_0_VERSION='1.4-gog2.0.0.4'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/the_guild_2'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
*.dll
*.exe
*.ini'
CONTENT_GAME_DATA_FILES='
*.dat
camerapaths
db
editor
gui
movie
msx
objects
particles
resource
savegames
scenes
scripts
sfx
shader
shots
textures
worlds'
CONTENT_DOC_DATA_FILES='
*.pdf
*.txt'

USER_PERSISTENT_FILES='
*.ini'

APP_MAIN_EXE='guildii.exe'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

## TODO: Setting up a WINE virtual desktop on first launch might prevent display problems on some setups.

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Disable intro movie
	ini_file='config.ini'
	ini_field='SplashScreen'
	ini_value='0'
	sed_expression="s/^${ini_field}=.*$/${ini_field}=${ini_value}/"
	sed --in-place --expression="$sed_expression" "$ini_file"
)

# Include game data

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
