#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Cotrio
# send your bug reports to contact@dotslashplay.it
###

script_version=20221030.2

GAME_ID='cotrio'
GAME_NAME='Cotrio'

ARCHIVE_BASE_0='Cotrio by Bart Bialek PC x64.zip'
ARCHIVE_BASE_0_MD5='6773aa0537f1d53b319817763db7f0e0'
ARCHIVE_BASE_0_SIZE='160000'
ARCHIVE_BASE_0_VERSION='1.0-itch.2018.10.28'
ARCHIVE_BASE_0_URL='https://bialek.itch.io/cotrio'

UNITY3D_NAME='Cotrio'

CONTENT_PATH_DEFAULT='PCx64'
CONTENT_GAME_BIN_FILES="
Mono
UnityPlayer.dll
${UNITY3D_NAME}.exe"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data"

APP_MAIN_TYPE='wine'
APP_MAIN_EXE="${UNITY3D_NAME}.exe"
APP_MAIN_ICON="$APP_MAIN_EXE"

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Use persistent storage for registry keys

WINE_REGEDIT_PERSISTENT_KEYS='
HKEY_CURRENT_USER\Software\Bkbl Bartosz Białek\Cotrio'

# Load common functions

target_version='2.18'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build package

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
