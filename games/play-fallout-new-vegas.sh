#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Fallout: New Vegas
# send your bug reports to contact@dotslashplay.it
###

script_version=20250113.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='fallout-new-vegas'
GAME_NAME='Fallout: New Vegas'

ARCHIVE_BASE_EN_2_NAME='setup_fallout_new_vegas_1.4.0.525(a)_(55068).exe'
ARCHIVE_BASE_EN_2_MD5='54d7878228b86284d360e8672ce5e85c'
ARCHIVE_BASE_EN_2_TYPE='innosetup'
ARCHIVE_BASE_EN_2_PART1_NAME='setup_fallout_new_vegas_1.4.0.525(a)_(55068)-1.bin'
ARCHIVE_BASE_EN_2_PART1_MD5='7ce668d5d084250b7ea445447b05d9b1'
ARCHIVE_BASE_EN_2_PART2_NAME='setup_fallout_new_vegas_1.4.0.525(a)_(55068)-2.bin'
ARCHIVE_BASE_EN_2_PART2_MD5='6d86a620514632e5d8c453e27e32dce1'
ARCHIVE_BASE_EN_2_SIZE='9879738'
ARCHIVE_BASE_EN_2_VERSION='1.4.0.525-gog55068'
ARCHIVE_BASE_EN_2_URL='https://www.gog.com/game/fallout_new_vegas_ultimate_edition'

ARCHIVE_BASE_FR_2_NAME='setup_fallout_new_vegas_1.4.0.525(a)_(french)_(55068).exe'
ARCHIVE_BASE_FR_2_MD5='301a3885d574a88ebbe8147ce8f28cd0'
ARCHIVE_BASE_FR_2_TYPE='innosetup'
ARCHIVE_BASE_FR_2_PART1_NAME='setup_fallout_new_vegas_1.4.0.525(a)_(french)_(55068)-1.bin'
ARCHIVE_BASE_FR_2_PART1_MD5='58eb0e07dc5eb8c1f1bc78dbfa1f9cfb'
ARCHIVE_BASE_FR_2_PART2_NAME='setup_fallout_new_vegas_1.4.0.525(a)_(french)_(55068)-2.bin'
ARCHIVE_BASE_FR_2_PART2_MD5='e57e233aea17d8f4ee23bf90d0318a38'
ARCHIVE_BASE_FR_2_PART3_NAME='setup_fallout_new_vegas_1.4.0.525(a)_(french)_(55068)-3.bin'
ARCHIVE_BASE_FR_2_PART3_MD5='c2020bf115acc116204e43ca09a58ef0'
ARCHIVE_BASE_FR_2_SIZE='9999844'
ARCHIVE_BASE_FR_2_VERSION='1.4.0.525-gog55068'
ARCHIVE_BASE_FR_2_URL='https://www.gog.com/game/fallout_new_vegas_ultimate_edition'

ARCHIVE_BASE_EN_1_NAME='setup_fallout_new_vegas_1.4.0.525(a)_(55068).exe'
ARCHIVE_BASE_EN_1_MD5='2381656fd552e5b93c143336fedbc21d'
ARCHIVE_BASE_EN_1_TYPE='innosetup'
ARCHIVE_BASE_EN_1_PART1_NAME='setup_fallout_new_vegas_1.4.0.525(a)_(55068)-1.bin'
ARCHIVE_BASE_EN_1_PART1_MD5='e0979d7e1d0145cd7bb6d1ad62419a17'
ARCHIVE_BASE_EN_1_PART2_NAME='setup_fallout_new_vegas_1.4.0.525(a)_(55068)-2.bin'
ARCHIVE_BASE_EN_1_PART2_MD5='730ae6ea0eb5d41773be6bb19bb15d57'
ARCHIVE_BASE_EN_1_SIZE='9900000'
ARCHIVE_BASE_EN_1_VERSION='1.4.0.525-gog55068'

ARCHIVE_BASE_FR_1_NAME='setup_fallout_new_vegas_1.4.0.525(a)_(french)_(55068).exe'
ARCHIVE_BASE_FR_1_MD5='841d652c40c7c4bb54830ba768bf48a7'
ARCHIVE_BASE_FR_1_TYPE='innosetup'
ARCHIVE_BASE_FR_1_PART1_NAME='setup_fallout_new_vegas_1.4.0.525(a)_(french)_(55068)-1.bin'
ARCHIVE_BASE_FR_1_PART1_MD5='054a674e5246f8b8c78c02f3ee2095dc'
ARCHIVE_BASE_FR_1_PART2_NAME='setup_fallout_new_vegas_1.4.0.525(a)_(french)_(55068)-2.bin'
ARCHIVE_BASE_FR_1_PART2_MD5='a3d4bbfd30d2d60389ed2d2b4b5aed07'
ARCHIVE_BASE_FR_1_SIZE='10000000'
ARCHIVE_BASE_FR_1_VERSION='1.4.0.525-gog55068'

ARCHIVE_BASE_EN_0_NAME='setup_fallout_new_vegas_1.4.0.525_(12010).exe'
ARCHIVE_BASE_EN_0_MD5='be32894fe423302d299fa532e5641079'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_PART1_NAME='setup_fallout_new_vegas_1.4.0.525_(12010)-1.bin'
ARCHIVE_BASE_EN_0_PART1_MD5='245661b2e1435c530763ae281ccecd9f'
ARCHIVE_BASE_EN_0_PART2_NAME='setup_fallout_new_vegas_1.4.0.525_(12010)-2.bin'
ARCHIVE_BASE_EN_0_PART2_MD5='705e7097b9c18836118c2e9eb42b19ed'
ARCHIVE_BASE_EN_0_SIZE='11000000'
ARCHIVE_BASE_EN_0_VERSION='1.4.0.525-gog12010'

ARCHIVE_BASE_FR_0_NAME='setup_fallout_new_vegas_french_1.4.0.525_(12010).exe'
ARCHIVE_BASE_FR_0_MD5='da79e8756efb16b211a76756cd8865b3'
ARCHIVE_BASE_FR_0_TYPE='innosetup'
ARCHIVE_BASE_FR_0_PART1_NAME='setup_fallout_new_vegas_french_1.4.0.525_(12010)-1.bin'
ARCHIVE_BASE_FR_0_PART1_MD5='245661b2e1435c530763ae281ccecd9f'
ARCHIVE_BASE_FR_0_PART2_NAME='setup_fallout_new_vegas_french_1.4.0.525_(12010)-2.bin'
ARCHIVE_BASE_FR_0_PART2_MD5='e148a49b1bbcfa4b2662e45691ae606e'
ARCHIVE_BASE_FR_0_SIZE='11000000'
ARCHIVE_BASE_FR_0_VERSION='1.4.0.525-gog12010'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_EN_0='app'
CONTENT_PATH_DEFAULT_FR_0='app'
CONTENT_GAME_BIN_FILES='
atimgpud.dll
binkw32.dll
gdffalloutnv.dll
libvorbis.dll
libvorbisfile.dll
ssce5432.dll
falloutnv.exe
geck.exe
low.ini
medium.ini
high.ini
veryhigh.ini'
## TODO: Check if the Galaxy libraries are required
CONTENT_GAME0_BIN_FILES='
galaxy.dll
galaxywrp.dll'
CONTENT_GAME_L10N_FILES='
falloutnvlauncher.exe
fallout_default.ini
data/credits.txt
data/creditswacky.txt
data/fallout - voices1.bsa
data/falloutnv.esm
data/video/fnvintro.bik'
CONTENT_GAME_DLC1_FILES='
data/deadmoney*'
CONTENT_GAME_DLC2_FILES='
data/honesthearts*'
CONTENT_GAME_DLC3_FILES='
data/oldworldblues*'
CONTENT_GAME_DLC4_FILES='
data/lonesomeroad*'
CONTENT_GAME_DLC5_FILES='
data/gunrunnersarsenal*'
CONTENT_GAME_DLC6_FILES='
data/caravanpack*'
CONTENT_GAME_DLC7_FILES='
data/classicpack*'
CONTENT_GAME_DLC8_FILES='
data/mercenarypack*'
CONTENT_GAME_DLC9_FILES='
data/tribalpack*'
CONTENT_GAME_DATA_FILES='
data
geckicon.ico
falloutnv.ico
maintitle.wav'
CONTENT_DOC_L10N_FILES='
*.txt'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/My Games/FalloutNV'

APP_MAIN_EXE='falloutnvlauncher.exe'
APP_MAIN_ICON='falloutnv.ico'

APP_EDITOR_ID="${GAME_ID}-editor"
APP_EDITOR_NAME="${GAME_NAME} - Editor"
APP_EDITOR_EXE='geck.exe'
APP_EDITOR_ICON='geckicon.ico'

PACKAGES_LIST='
PKG_BIN
PKG_L10N
PKG_DLC1
PKG_DLC2
PKG_DLC3
PKG_DLC4
PKG_DLC5
PKG_DLC6
PKG_DLC7
PKG_DLC8
PKG_DLC9
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_L10N_BASE
PKG_DATA'
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
application/x-id3'

PKG_L10N_BASE_ID="${GAME_ID}-l10n"
PKG_L10N_ID_EN="${PKG_L10N_BASE_ID}-en"
PKG_L10N_ID_FR="${PKG_L10N_BASE_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_BASE_ID"
PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_FR='French localization'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_DLC1_ID="${GAME_ID}-dlc-dead-money"
PKG_DLC1_ID_EN="${PKG_DLC1_ID}-en"
PKG_DLC1_ID_FR="${PKG_DLC1_ID}-fr"
PKG_DLC1_PROVIDES="
$PKG_DLC1_ID"
PKG_DLC1_DESCRIPTION='Dead Money'
PKG_DLC1_DESCRIPTION_EN="${PKG_DLC1_DESCRIPTION} - English version"
PKG_DLC1_DESCRIPTION_FR="${PKG_DLC1_DESCRIPTION} - French version"
PKG_DLC1_DEPENDENCIES_SIBLINGS='
PKG_BIN'

PKG_DLC2_ID="${GAME_ID}-dlc-honest-hearts"
PKG_DLC2_ID_EN="${PKG_DLC2_ID}-en"
PKG_DLC2_ID_FR="${PKG_DLC2_ID}-fr"
PKG_DLC2_PROVIDES="
$PKG_DLC2_ID"
PKG_DLC2_DESCRIPTION='Honest Hearts'
PKG_DLC2_DESCRIPTION_EN="${PKG_DLC2_DESCRIPTION} - English version"
PKG_DLC2_DESCRIPTION_FR="${PKG_DLC2_DESCRIPTION} - French version"
PKG_DLC2_DEPENDENCIES_SIBLINGS='
PKG_BIN'

PKG_DLC3_ID="${GAME_ID}-dlc-old-world-blues"
PKG_DLC3_ID_EN="${PKG_DLC3_ID}-en"
PKG_DLC3_ID_FR="${PKG_DLC3_ID}-fr"
PKG_DLC3_PROVIDES="
$PKG_DLC3_ID"
PKG_DLC3_DESCRIPTION='Old World Blues'
PKG_DLC3_DESCRIPTION_EN="${PKG_DLC3_DESCRIPTION} - English version"
PKG_DLC3_DESCRIPTION_FR="${PKG_DLC3_DESCRIPTION} - French version"
PKG_DLC3_DEPENDENCIES_SIBLINGS='
PKG_BIN'

PKG_DLC4_ID="${GAME_ID}-dlc-lonesome-road"
PKG_DLC4_ID_EN="${PKG_DLC4_ID}-en"
PKG_DLC4_ID_FR="${PKG_DLC4_ID}-fr"
PKG_DLC4_PROVIDES="
$PKG_DLC4_ID"
PKG_DLC4_DESCRIPTION='Lonesome Road'
PKG_DLC4_DESCRIPTION_EN="${PKG_DLC4_DESCRIPTION} - English version"
PKG_DLC4_DESCRIPTION_FR="${PKG_DLC4_DESCRIPTION} - French version"
PKG_DLC4_DEPENDENCIES_SIBLINGS='
PKG_BIN'

PKG_DLC5_ID="${GAME_ID}-dlc-gun-runners-arsenal"
PKG_DLC5_ID_EN="${PKG_DLC5_ID}-en"
PKG_DLC5_ID_FR="${PKG_DLC5_ID}-fr"
PKG_DLC5_PROVIDES="
$PKG_DLC5_ID"
PKG_DLC5_DESCRIPTION='Gun Runnersʼ Arsenal'
PKG_DLC5_DESCRIPTION_EN="${PKG_DLC5_DESCRIPTION} - English version"
PKG_DLC5_DESCRIPTION_FR="${PKG_DLC5_DESCRIPTION} - French version"
PKG_DLC5_DEPENDENCIES_SIBLINGS='
PKG_BIN'

PKG_DLC6_ID="${GAME_ID}-dlc-caravan-pack"
PKG_DLC6_ID_EN="${PKG_DLC6_ID}-en"
PKG_DLC6_ID_FR="${PKG_DLC6_ID}-fr"
PKG_DLC6_PROVIDES="
$PKG_DLC6_ID"
PKG_DLC6_DESCRIPTION='Caravan Pack'
PKG_DLC6_DESCRIPTION_EN="${PKG_DLC6_DESCRIPTION} - English version"
PKG_DLC6_DESCRIPTION_FR="${PKG_DLC6_DESCRIPTION} - French version"
PKG_DLC6_DEPENDENCIES_SIBLINGS='
PKG_BIN'

PKG_DLC7_ID="${GAME_ID}-dlc-classic-pack"
PKG_DLC7_ID_EN="${PKG_DLC7_ID}-en"
PKG_DLC7_ID_FR="${PKG_DLC7_ID}-fr"
PKG_DLC7_PROVIDES="
$PKG_DLC7_ID"
PKG_DLC7_DESCRIPTION='Classic Pack'
PKG_DLC7_DESCRIPTION_EN="${PKG_DLC7_DESCRIPTION} - English version"
PKG_DLC7_DESCRIPTION_FR="${PKG_DLC7_DESCRIPTION} - French version"
PKG_DLC7_DEPENDENCIES_SIBLINGS='
PKG_BIN'

PKG_DLC8_ID="${GAME_ID}-dlc-mercenary-pack"
PKG_DLC8_ID_EN="${PKG_DLC8_ID}-en"
PKG_DLC8_ID_FR="${PKG_DLC8_ID}-fr"
PKG_DLC8_PROVIDES="
$PKG_DLC8_ID"
PKG_DLC8_DESCRIPTION='Mercenary Pack'
PKG_DLC8_DESCRIPTION_EN="${PKG_DLC8_DESCRIPTION} - English version"
PKG_DLC8_DESCRIPTION_FR="${PKG_DLC8_DESCRIPTION} - French version"
PKG_DLC8_DEPENDENCIES_SIBLINGS='
PKG_BIN'

PKG_DLC9_ID="${GAME_ID}-dlc-tribal-pack"
PKG_DLC9_ID_EN="${PKG_DLC9_ID}-en"
PKG_DLC9_ID_FR="${PKG_DLC9_ID}-fr"
PKG_DLC9_PROVIDES="
$PKG_DLC9_ID"
PKG_DLC9_DESCRIPTION='Tribal Pack'
PKG_DLC9_DESCRIPTION_EN="${PKG_DLC9_DESCRIPTION} - English version"
PKG_DLC9_DESCRIPTION_FR="${PKG_DLC9_DESCRIPTION} - French version"
PKG_DLC9_DEPENDENCIES_SIBLINGS='
PKG_BIN'

# Set required registry key

installed_path="C:\\\\${GAME_ID}/"
registry_dump_installed_path_file='registry-dumps/installed-path.reg'
registry_dump_installed_path_content='Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\Software\Bethesda Softworks\FalloutNV]
"Installed Path"="'"${installed_path}"'"'
CONTENT_GAME_BIN_FILES="${CONTENT_GAME_BIN_FILES:-}
$registry_dump_installed_path_file"
APP_REGEDIT="${APP_REGEDIT:-} $registry_dump_installed_path_file"
REQUIREMENTS_LIST="${REQUIREMENTS_LIST:-}
iconv"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Set required registry key
	mkdir --parents "$(dirname "$registry_dump_installed_path_file")"
	printf '%s' "$registry_dump_installed_path_content" |
		iconv \
		--from-code=UTF-8 --to-code=UTF-16 \
		--output="$registry_dump_installed_path_file"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions 'PKG_BIN' 'PKG_L10N' 'PKG_DATA'
(
	GAME_NAME="$PKG_DLC1_DESCRIPTION"
	set_current_package 'PKG_DLC1'
	print_instructions 'PKG_DLC1'
)
(
	GAME_NAME="$PKG_DLC2_DESCRIPTION"
	set_current_package 'PKG_DLC2'
	print_instructions 'PKG_DLC2'
)
(
	GAME_NAME="$PKG_DLC3_DESCRIPTION"
	set_current_package 'PKG_DLC3'
	print_instructions 'PKG_DLC3'
)
(
	GAME_NAME="$PKG_DLC4_DESCRIPTION"
	set_current_package 'PKG_DLC4'
	print_instructions 'PKG_DLC4'
)
(
	GAME_NAME="$PKG_DLC5_DESCRIPTION"
	set_current_package 'PKG_DLC5'
	print_instructions 'PKG_DLC5'
)
(
	GAME_NAME="$PKG_DLC6_DESCRIPTION"
	set_current_package 'PKG_DLC6'
	print_instructions 'PKG_DLC6'
)
(
	GAME_NAME="$PKG_DLC7_DESCRIPTION"
	set_current_package 'PKG_DLC7'
	print_instructions 'PKG_DLC7'
)
(
	GAME_NAME="$PKG_DLC8_DESCRIPTION"
	set_current_package 'PKG_DLC8'
	print_instructions 'PKG_DLC8'
)
(
	GAME_NAME="$PKG_DLC9_DESCRIPTION"
	set_current_package 'PKG_DLC9'
	print_instructions 'PKG_DLC9'
)

# Clean up

working_directory_cleanup

exit 0
