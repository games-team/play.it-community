#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 BetaRays
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Kerbal Space Program
# send your bug reports to contact@dotslashplay.it
###

script_version=20241216.3

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='kerbal-space-program'
GAME_NAME='Kerbal Space Program'

ARCHIVE_BASE_0_NAME='kerbal_space_program_1_12_5_03190_61624.sh'
ARCHIVE_BASE_0_MD5='0dc96f1a59f1f8b8cb43b3e8ed58b05a'
ARCHIVE_BASE_0_SIZE='4873634'
ARCHIVE_BASE_0_VERSION='1.12.5-gog61624'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/kerbal_space_program'

ARCHIVE_BASE_OLDLIBS_3_NAME='kerbal_space_program_1_10_1_02939_41941.sh'
ARCHIVE_BASE_OLDLIBS_3_MD5='fe71449cc6478ae7cbfbf0a718c9daa3'
ARCHIVE_BASE_OLDLIBS_3_SIZE='4000000'
ARCHIVE_BASE_OLDLIBS_3_VERSION='1.10.1-gog41941'

ARCHIVE_BASE_OLDLIBS_2_NAME='kerbal_space_program_1_10_0_02917_39410.sh'
ARCHIVE_BASE_OLDLIBS_2_MD5='40d7ea6e6c112a95954b3178030599b0'
ARCHIVE_BASE_OLDLIBS_2_SIZE='4000000'
ARCHIVE_BASE_OLDLIBS_2_VERSION='1.10.0-gog39410'

ARCHIVE_BASE_OLDLIBS_1_NAME='kerbal_space_program_1_9_1_02788_36309.sh'
ARCHIVE_BASE_OLDLIBS_1_MD5='6157d3ebad90960893e4aa177b8518de'
ARCHIVE_BASE_OLDLIBS_1_SIZE='3700000'
ARCHIVE_BASE_OLDLIBS_1_VERSION='1.9.1-gog36309'

ARCHIVE_BASE_OLDLIBS_0_NAME='kerbal_space_program_1_8_1_02694_33460.sh'
ARCHIVE_BASE_OLDLIBS_0_MD5='195abb84de4a916192190858d0796c50'
ARCHIVE_BASE_OLDLIBS_0_SIZE='3400000'
ARCHIVE_BASE_OLDLIBS_0_VERSION='1.8.1-gog33460'

UNITY3D_NAME='KSP'
UNITY3D_PLUGINS='
libkeyboard.so
liblingoona.grammar.kerbal.so'
UNITY3D_PLUGINS_OLDLIBS='
libkeyboard.so
liblingoona.grammar.kerbal.so
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME0_DATA_FILES='
GameData
Internals
Missions
Parts
Resources
Ships
saves
sounds
*.cfg'
CONTENT_DOC_DATA_FILES='
LegalNotice.txt
readme.txt'

USER_PERSISTENT_DIRECTORIES='
saves
sounds'
## The game fails to load if it does not have write access to GameData/Squad/KSPedia files.
USER_PERSISTENT_DIRECTORIES="${USER_PERSISTENT_DIRECTORIES:-}
GameData/Squad/KSPedia"
USER_PERSISTENT_FILES='
*.cfg
*.mu'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'
PKG_BIN_DEPENDENCIES_LIBRARIES_OLDLIBS='
libatk-1.0.so.0
libcairo.so.2
libc.so.6
libdl.so.2
libfontconfig.so.1
libfreetype.so.6
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libgdk-x11-2.0.so.0
libgio-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpango-1.0.so.0
libpangocairo-1.0.so.0
libpangoft2-1.0.so.0
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
