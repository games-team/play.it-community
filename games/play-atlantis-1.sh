#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Atlantis 1
# send your bug reports to contact@dotslashplay.it
###

script_version=20230929.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='atlantis-1'
GAME_NAME='Atlantis: The Lost Tales'

ARCHIVE_BASE_EN_0_NAME='setup_atlantis_the_lost_tales_2.0.0.15.exe'
ARCHIVE_BASE_EN_0_MD5='287170bea9041b4e29888d97f87eb9fc'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_SIZE='1900000'
ARCHIVE_BASE_EN_0_VERSION='1.0-gog2.0.0.15'
ARCHIVE_BASE_EN_0_URL='https://www.gog.com/game/atlantis_the_lost_tales'

ARCHIVE_BASE_FR_0_NAME='setup_atlantis_the_lost_tales_french_2.1.0.15.exe'
ARCHIVE_BASE_FR_0_MD5='0cb6b037a457d35dacd23e1f22aea57b'
ARCHIVE_BASE_FR_0_TYPE='innosetup'
ARCHIVE_BASE_FR_0_SIZE='1900000'
ARCHIVE_BASE_FR_0_VERSION='1.0-gog2.1.0.15'
ARCHIVE_BASE_FR_0_URL='https://www.gog.com/game/atlantis_the_lost_tales'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
*.exe
*.dll'
CONTENT_GAME_L10N_FILES='
images/end.tga
scenar
sprlist
wav'
CONTENT_GAME_DATA_FILES='
*.big
cyclo
dialog
images
puzzles
sprite
ubb_vue
wam'
CONTENT_DOC_DATA_FILES='
*.pdf'

APP_MAIN_EXE='atlantis.exe'

PACKAGES_LIST='PKG_BIN PKG_L10N PKG_DATA'

PKG_L10N_ID="${GAME_ID}-l10n"
PKG_L10N_ID_EN="${PKG_L10N_ID}-en"
PKG_L10N_ID_FR="${PKG_L10N_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_ID"
PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_FR='French localization'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_ID="$GAME_ID"
PKG_BIN_ID_EN="${PKG_BIN_ID}-en"
PKG_BIN_ID_FR="${PKG_BIN_ID}-fr"
PKG_BIN_PROVIDES="
$PKG_BIN_ID"
PKG_BIN_DESCRIPTION_EN='English version'
PKG_BIN_DESCRIPTION_FR='French version'
PKG_BIN_DEPS="$PKG_L10N_ID $PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Set game directory in the registry

# shellcheck disable=SC1003
game_directory='C:\\'"$(game_id)"'\\'
registry_dump_game_directory_file='registry-dumps/game-directory.reg'
registry_dump_game_directory_content='Windows Registry Editor Version 5.00

[HKEY_CURRENT_USER\Software\CRYO\Atlantis\GameDirectory]
@="'"${game_directory}"'"'
CONTENT_GAME_BIN_FILES="${CONTENT_GAME_BIN_FILES:-}
$registry_dump_game_directory_file"
APP_REGEDIT="${APP_REGEDIT:-} $registry_dump_game_directory_file"
SCRIPT_DEPS="${SCRIPT_DEPS:-} iconv"
check_deps

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Set game directory in the registry
	mkdir --parents "$(dirname "$registry_dump_game_directory_file")"
	printf '%s' "$registry_dump_game_directory_content" | \
		iconv --from-code=UTF-8 --to-code=UTF-16 --output="$registry_dump_game_directory_file"
)

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
