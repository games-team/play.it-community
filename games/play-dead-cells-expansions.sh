#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Hoël Bézier
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Dead Cells expansions:
# - Rise of the Giant
# - The Bad Seed
# send your bug reports to contact@dotslashplay.it
###

script_version=20240615.2

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='dead-cells'
GAME_NAME='Dead Cells'

EXPANSION_ID_GIANT='rise-of-the-giant'
EXPANSION_NAME_GIANT='Rise of the Giant'

EXPANSION_ID_SEED='the-bad-seed'
EXPANSION_NAME_SEED='The Bad Seed'

ARCHIVE_BASE_GIANT_1_NAME='dead_cells_rise_of_the_giant_1_9_1_39495.sh'
ARCHIVE_BASE_GIANT_1_MD5='c1d123c88c3e1f85a11ae9d090f626b0'
ARCHIVE_BASE_GIANT_1_SIZE='1100'
ARCHIVE_BASE_GIANT_1_VERSION='1.9.1-gog39495'
ARCHIVE_BASE_GIANT_1_URL='https://www.gog.com/game/dead_cells_rise_of_the_giant'

ARCHIVE_BASE_GIANT_0='dead_cells_rise_of_the_giant_1_8_0_37766.sh'
ARCHIVE_BASE_GIANT_0_MD5='855ef837a9766e5b30ee34d212e5b16b'
ARCHIVE_BASE_GIANT_0_SIZE='1100'
ARCHIVE_BASE_GIANT_0_VERSION='1.8.0-gog37766'

ARCHIVE_BASE_SEED_1_NAME='dead_cells_the_bad_seed_1_9_1_39495.sh'
ARCHIVE_BASE_SEED_1_MD5='745aa46480a45d4bf4904361a4d06110'
ARCHIVE_BASE_SEED_1_SIZE='1100'
ARCHIVE_BASE_SEED_1_VERSION='1.9.1-gog39495'
ARCHIVE_BASE_SEED_1_URL='https://www.gog.com/game/dead_cells_the_bad_seed'

ARCHIVE_BASE_SEED_0='dead_cells_the_bad_seed_1_8_0_37766.sh'
ARCHIVE_BASE_SEED_0_MD5='3ab8b8c71c1a9079be384a7788592d83'
ARCHIVE_BASE_SEED_0_SIZE='1100'
ARCHIVE_BASE_SEED_0_VERSION='1.8.0-gog37766'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
goggame-*.info'

PKG_MAIN_DEPS="$GAME_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
