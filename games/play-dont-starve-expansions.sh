#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 mortalius
# SPDX-FileCopyrightText: © 2019 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Don't Starve expansions:
# - Reign of Giants
# - Shipwrecked
# send your bug reports to contact@dotslashplay.it
###

script_version=20230901.2

GAME_ID='dont-starve'
GAME_NAME='Donʼt Starve'

EXPANSION_ID_GIANTS='reign-of-giants'
EXPANSION_NAME_GIANTS='Reign of Giants'

EXPANSION_ID_SHIPWRECKED='shipwrecked'
EXPANSION_NAME_SHIPWRECKED='Shipwrecked'

# Archives

## Reign of Giants

ARCHIVE_BASE_GIANTS_1='don_t_starve_reign_of_giants_554439_66995.sh'
ARCHIVE_BASE_GIANTS_1_MD5='93cbeafae249e5a5af4f25afb91ca9a5'
ARCHIVE_BASE_GIANTS_1_SIZE='630000'
ARCHIVE_BASE_GIANTS_1_VERSION='554439-gog66995'
ARCHIVE_BASE_GIANTS_1_URL='https://www.gog.com/game/dont_starve_reign_of_giants'

ARCHIVE_BASE_GIANTS_0='don_t_starve_reign_of_giants_dlc_4294041_41439.sh'
ARCHIVE_BASE_GIANTS_0_MD5='8949aed4ca7eddd8179ad768dd223681'
ARCHIVE_BASE_GIANTS_0_SIZE='620000'
ARCHIVE_BASE_GIANTS_0_VERSION='4294041-gog41439'

ARCHIVE_BASE_GIANTS_MULTIARCH_1='don_t_starve_reign_of_giants_dlc_en_20171215_17628.sh'
ARCHIVE_BASE_GIANTS_MULTIARCH_1_MD5='47084ab8d5b36437e1bcb899c35bfe00'
ARCHIVE_BASE_GIANTS_MULTIARCH_1_SIZE='400000'
ARCHIVE_BASE_GIANTS_MULTIARCH_1_VERSION='246924-gog17628'

ARCHIVE_BASE_GIANTS_MULTIARCH_0='gog_don_t_starve_reign_of_giants_dlc_2.0.0.3.sh'
ARCHIVE_BASE_GIANTS_MULTIARCH_0_MD5='bd505adc70ed478a92669bc8c1c3a127'
ARCHIVE_BASE_GIANTS_MULTIARCH_0_SIZE='400000'
ARCHIVE_BASE_GIANTS_MULTIARCH_0_VERSION='1.0-gog2.0.0.3'

## Shipwrecked

ARCHIVE_BASE_SHIPWRECKED_1='don_t_starve_shipwrecked_554439_66995.sh'
ARCHIVE_BASE_SHIPWRECKED_1_MD5='0f1fcee5e80a2c7a081a0d4b39043010'
ARCHIVE_BASE_SHIPWRECKED_1_SIZE='860000'
ARCHIVE_BASE_SHIPWRECKED_1_VERSION='554439-gog66995'
ARCHIVE_BASE_SHIPWRECKED_1_URL='https://www.gog.com/game/dont_starve_shipwrecked'

ARCHIVE_BASE_SHIPWRECKED_0='don_t_starve_shipwrecked_dlc_4294041_41439.sh'
ARCHIVE_BASE_SHIPWRECKED_0_MD5='ff762f9b6a3ef71e6b802fe596196925'
ARCHIVE_BASE_SHIPWRECKED_0_SIZE='830000'
ARCHIVE_BASE_SHIPWRECKED_0_VERSION='4294041-gog41439'

ARCHIVE_BASE_SHIPWRECKED_MULTIARCH_1='don_t_starve_shipwrecked_dlc_en_20171215_17628.sh'
ARCHIVE_BASE_SHIPWRECKED_MULTIARCH_1_MD5='463825173d76f294337f0ae7043d7cf6'
ARCHIVE_BASE_SHIPWRECKED_MULTIARCH_1_SIZE='1200000'
ARCHIVE_BASE_SHIPWRECKED_MULTIARCH_1_VERSION='246924-gog17628'

ARCHIVE_BASE_SHIPWRECKED_MULTIARCH_0='gog_don_t_starve_shipwrecked_dlc_2.0.0.2.sh'
ARCHIVE_BASE_SHIPWRECKED_MULTIARCH_0_MD5='b1d4152639a272a959d36eacf8cb859e'
ARCHIVE_BASE_SHIPWRECKED_MULTIARCH_0_SIZE='1200000'
ARCHIVE_BASE_SHIPWRECKED_MULTIARCH_0_VERSION='1.0-gog2.0.0.2'


CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_PATH_DEFAULT_MULTIARCH='data/noarch/game/dontstarve32'
CONTENT_PATH_DEFAULT_GIANTS_MULTIARCH="$CONTENT_PATH_DEFAULT_MULTIARCH"
CONTENT_PATH_DEFAULT_SHIPWRECKED_MULTIARCH="$CONTENT_PATH_DEFAULT_MULTIARCH"
CONTENT_GAME_MAIN_FILES='
data
manifest_dlc????.json'

PKG_MAIN_DEPS="$GAME_ID"

# Load common functions

target_version='2.25'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
