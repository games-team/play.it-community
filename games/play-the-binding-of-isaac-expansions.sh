#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 HS-157
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Binding of Isaac expansions:
# - Afterbirth
# - Afterbirth +
# - Repentance
# send your bug reports to contact@dotslashplay.it
###

script_version=20230723.1

GAME_ID='the-binding-of-isaac'
GAME_NAME='The Binding of Isaac'

EXPANSION_ID_AFTERBIRTH='afterbirth'
EXPANSION_NAME_AFTERBIRTH='Afterbirth'

EXPANSION_ID_AFTERBIRTHPLUS='afterbirth-plus'
EXPANSION_NAME_AFTERBIRTHPLUS='Afterbirth +'

EXPANSION_ID_REPENTANCE='repentance'
EXPANSION_NAME_REPENTANCE='Repentance'

# Archives

## Afterbirth

ARCHIVE_BASE_AFTERBIRTH_1='setup_the_binding_of_isaac_afterbirth_1.0.1.0_(54718).exe'
ARCHIVE_BASE_AFTERBIRTH_1_MD5='9cdb71b69a9b25e69df3f9dcdab2de9f'
ARCHIVE_BASE_AFTERBIRTH_1_TYPE='innosetup'
ARCHIVE_BASE_AFTERBIRTH_1_SIZE='170000'
ARCHIVE_BASE_AFTERBIRTH_1_VERSION='1.0.1.0-gog54718'
ARCHIVE_BASE_AFTERBIRTH_1_URL='https://www.gog.com/game/the_binding_of_isaac_afterbirth'

ARCHIVE_BASE_AFTERBIRTH_0='setup_the_binding_of_isaac_afterbirth_1.0.0.0_(52089).exe'
ARCHIVE_BASE_AFTERBIRTH_0_MD5='f72a7c3d8f23627b6e4eaf1b0e71c304'
ARCHIVE_BASE_AFTERBIRTH_0_TYPE='innosetup'
ARCHIVE_BASE_AFTERBIRTH_0_SIZE='170000'
ARCHIVE_BASE_AFTERBIRTH_0_VERSION='1.0.0.0-gog52089'

## Afterbirth +

ARCHIVE_BASE_AFTERBIRTHPLUS_1='setup_the_binding_of_isaac_afterbirth__1.0.1.0_(54718).exe'
ARCHIVE_BASE_AFTERBIRTHPLUS_1_MD5='23558f5eb97895659b5e594595aad247'
ARCHIVE_BASE_AFTERBIRTHPLUS_1_TYPE='innosetup'
ARCHIVE_BASE_AFTERBIRTHPLUS_1_SIZE='110000'
ARCHIVE_BASE_AFTERBIRTHPLUS_1_VERSION='1.0.1.0-gog54718'
ARCHIVE_BASE_AFTERBIRTHPLUS_1_URL='https://www.gog.com/game/the_binding_of_isaac_afterbirth_plus'

ARCHIVE_BASE_AFTERBIRTHPLUS_0='setup_the_binding_of_isaac_afterbirth__1.0.0.0_(52089).exe'
ARCHIVE_BASE_AFTERBIRTHPLUS_0_MD5='ca0d4b261b16ffca26d7f2f723bd8031'
ARCHIVE_BASE_AFTERBIRTHPLUS_0_TYPE='innosetup'
ARCHIVE_BASE_AFTERBIRTHPLUS_0_SIZE='110000'
ARCHIVE_BASE_AFTERBIRTHPLUS_0_VERSION='1.0.0.0-gog52089'

## Repentance

ARCHIVE_BASE_REPENTANCE_1='setup_the_binding_of_isaac_repentance_1.0.1.0_(54718).exe'
ARCHIVE_BASE_REPENTANCE_1_MD5='b8eeaeb46b3b2ddb5aa6e04729d8085e'
ARCHIVE_BASE_REPENTANCE_1_TYPE='innosetup'
ARCHIVE_BASE_REPENTANCE_1_SIZE='490000'
ARCHIVE_BASE_REPENTANCE_1_VERSION='1.0.1.0-gog54718'
ARCHIVE_BASE_REPENTANCE_1_URL='https://www.gog.com/game/the_binding_of_isaac_repentance'

ARCHIVE_BASE_REPENTANCE_0='setup_the_binding_of_isaac_repentance_1.0.0.0_(52089).exe'
ARCHIVE_BASE_REPENTANCE_0_MD5='b569c8fce392e5ec1083e5418d9a36d0'
ARCHIVE_BASE_REPENTANCE_0_TYPE='innosetup'
ARCHIVE_BASE_REPENTANCE_0_SIZE='440000'
ARCHIVE_BASE_REPENTANCE_0_VERSION='1.0.0.0-gog52089'


CONTENT_GAME_MAIN_PATH='.'
CONTENT_GAME_MAIN_FILES='
resources'

PKG_MAIN_DEPS="$GAME_ID"

# Load common functions

target_version='2.24'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
