#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 macaron
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Full Throttle Remastered
# send your bug reports to contact@dotslashplay.it
###

script_version=20230623.1

GAME_ID='full-throttle-remastered'
GAME_NAME='Full Throttle Remastered'

ARCHIVE_BASE_0='full_throttle_remastered_en_gog_3_20993.sh'
ARCHIVE_BASE_0_MD5='2f4bf151e225e7dfa9bec31ed49b721c'
ARCHIVE_BASE_0_SIZE='5400000'
ARCHIVE_BASE_0_VERSION='1.0-gog20993'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/full_throttle_remastered'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN_PATH='data/noarch/game/lib'
CONTENT_LIBS_BIN_FILES='
libfmod.so.8'
CONTENT_GAME_BIN_FILES='
Throttle
gamecontrollerdb.txt'
CONTENT_GAME_DATA_FILES='
full.data'
CONTENT_DOC_DATA_FILES='
readme.txt'

APP_MAIN_EXE='Throttle'
APP_MAIN_ICON='../support/icon.png'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6'

# Load common functions

target_version='2.20'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE' 2>/dev/null
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Restore the full data file from its parts
	mv 'full.data.split00' 'full.data'
	cat 'full.data.split01' >> 'full.data'
	rm 'full.data.split01'
)

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
