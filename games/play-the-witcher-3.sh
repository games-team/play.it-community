#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Mopi
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Witcher 3
# send your bug reports to contact@dotslashplay.it
###

script_version=20241230.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='the-witcher-3'
GAME_NAME='The Witcher 3: Wild Hunt'

ARCHIVE_BASE_0_NAME='setup_the_witcher_3_wild_hunt_goty_1.31_(a)_(9709).exe'
ARCHIVE_BASE_0_MD5='321ed8cc0faedb903190a708686a1b50'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_the_witcher_3_wild_hunt_goty_1.31_(a)_(9709)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='8aab6124c22f1360585ee2285ea6d8f7'
ARCHIVE_BASE_0_PART2_NAME='setup_the_witcher_3_wild_hunt_goty_1.31_(a)_(9709)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='3966e84f941b9fba384eb4a2b0b23c65'
ARCHIVE_BASE_0_PART3_NAME='setup_the_witcher_3_wild_hunt_goty_1.31_(a)_(9709)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='bc80aa6b1538ecf757e7db6f3723e056'
ARCHIVE_BASE_0_PART4_NAME='setup_the_witcher_3_wild_hunt_goty_1.31_(a)_(9709)-4.bin'
ARCHIVE_BASE_0_PART4_MD5='71660541a0c358ef40802ba62a2c3c09'
ARCHIVE_BASE_0_PART5_NAME='setup_the_witcher_3_wild_hunt_goty_1.31_(a)_(9709)-5.bin'
ARCHIVE_BASE_0_PART5_MD5='9ad56c2efc7b09f480f7f8c7922c8b3f'
ARCHIVE_BASE_0_PART6_NAME='setup_the_witcher_3_wild_hunt_goty_1.31_(a)_(9709)-6.bin'
ARCHIVE_BASE_0_PART6_MD5='bd3699654b2e34668445219f2bbbc793'
ARCHIVE_BASE_0_PART7_NAME='setup_the_witcher_3_wild_hunt_goty_1.31_(a)_(9709)-7.bin'
ARCHIVE_BASE_0_PART7_MD5='1beb5a622e6695d0dd65cac5fab08793'
ARCHIVE_BASE_0_PART8_NAME='setup_the_witcher_3_wild_hunt_goty_1.31_(a)_(9709)-8.bin'
ARCHIVE_BASE_0_PART8_MD5='121adc43111562e354d9841800d0c613'
ARCHIVE_BASE_0_PART9_NAME='setup_the_witcher_3_wild_hunt_goty_1.31_(a)_(9709)-9.bin'
ARCHIVE_BASE_0_PART9_MD5='0f5329306515d6f41a1b4a7b2d38ad74'
ARCHIVE_BASE_0_SIZE='38000000'
ARCHIVE_BASE_0_VERSION='1.31a-gog9709'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/the_witcher_3_wild_hunt_game_of_the_year_edition'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
bin'
CONTENT_GAME_MOVIES_FILES='
content/*/bundles/movies.bundle'
CONTENT_GAME_BUNDLES_FILES='
content/*/bundles'
CONTENT_GAME_VOICES_FILES='
content/*/soundspc.cache'
CONTENT_GAME_DATA_FILES='
content'
CONTENT_GAME_DLC_HEARTS_OF_STONE_FILES='
dlc/ep1'
CONTENT_GAME_DLC_BLOOD_AND_WINE_FILES='
dlc/bob'
CONTENT_GAME_DLC_FREE_PACK_FILES='
dlc/dlc*'

WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/The Witcher 3'

APP_MAIN_EXE='bin/x64/witcher3.exe'
## Run the game binary from its parent directory.
APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}"'
# Run the game binary from its parent directory
cd "$(dirname "$APP_EXE")"
APP_EXE=$(basename "$APP_EXE")'

PACKAGES_LIST='
PKG_BIN
PKG_MOVIES
PKG_BUNDLES
PKG_VOICES
PKG_DLC_HEARTS_OF_STONE
PKG_DLC_BLOOD_AND_WINE
PKG_DLC_FREE_PACK
PKG_DATA'

PKG_BUNDLES_ID="${GAME_ID}-bundles"
PKG_BUNDLES_DESCRIPTION='bundles'

PKG_MOVIES_ID="${GAME_ID}-movies"
PKG_MOVIES_DESCRIPTION='movies'
PKG_BUNDLES_DEPS="${PKG_BUNDLES_DEPS:-} $PKG_MOVIES_ID"

PKG_VOICES_ID="${GAME_ID}-voices"
PKG_VOICES_DESCRIPTION='voices'

PKG_DLC_HEARTS_OF_STONE_ID="${GAME_ID}-hearts-of-stone"
PKG_DLC_HEARTS_OF_STONE_DESCRIPTION='Hearts of Stone'
PKG_DLC_HEARTS_OF_STONE_DEPS="$GAME_ID"

PKG_DLC_BLOOD_AND_WINE_ID="${GAME_ID}-blood-and-wine"
PKG_DLC_BLOOD_AND_WINE_DESCRIPTION='Blood and Wine'
PKG_DLC_BLOOD_AND_WINE_DEPS="$GAME_ID"

PKG_DLC_FREE_PACK_ID="${GAME_ID}-free-dlc-pack"
PKG_DLC_FREE_PACK_DESCRIPTION='Free DLC pack'
PKG_DLC_FREE_PACK_DEPS="$GAME_ID"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPS="$PKG_BUNDLES_ID $PKG_VOICES_ID"

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions 'PKG_MOVIES' 'PKG_BUNDLES' 'PKG_VOICES' 'PKG_DATA' 'PKG_BIN'
GAME_NAME=$(package_description 'PKG_DLC_HEARTS_OF_STONE')
print_instructions 'PKG_DLC_HEARTS_OF_STONE'
GAME_NAME=$(package_description 'PKG_DLC_BLOOD_AND_WINE')
print_instructions 'PKG_DLC_BLOOD_AND_WINE'
GAME_NAME=$(package_description 'PKG_DLC_FREE_PACK')
print_instructions 'PKG_DLC_FREE_PACK'

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
