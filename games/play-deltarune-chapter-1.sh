#!/bin/sh
# SPDX-FileCopyrightText: © 2018 BetaRays
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Deltarune - Chapter 1
# send your bug reports to contact@dotslashplay.it
###

script_version=20240704.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='deltarune-chapter-1'
GAME_NAME='Deltarune - Chapter 1'

ARCHIVE_BASE_0_NAME='SURVEY_PROGRAM_WINDOWS_ENGLISH.exe'
ARCHIVE_BASE_0_MD5='2f92f4ad09d41287b36650aaf1e5359e'
ARCHIVE_BASE_0_TYPE='nullsoft-installer'
ARCHIVE_BASE_0_VERSION='1.0-deltarune'
ARCHIVE_BASE_0_SIZE='95000'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
deltarune.exe'
CONTENT_GAME_DATA_FILES='
mus
lang
data.win
*.dat
*.ogg'
CONTENT_DOC_DATA_FILES='
license.txt'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Local/DELTARUNE'

APP_MAIN_EXE='deltarune.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Convert all file paths to lowercase.
	tolower .
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
