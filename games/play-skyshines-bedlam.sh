#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Skyshine's Bedlam
# send your bug reports to contact@dotslashplay.it
###

script_version=20230803.2

GAME_ID='skyshines-bedlam'
GAME_NAME='Skyshineʼs Bedlam'

## This game is no longer available for sale from GOG.com
ARCHIVE_BASE_0='setup_skyshines_bedlam_redux_2.0.0.2.exe'
ARCHIVE_BASE_0_MD5='9fa7fdb957941eb0b9e6b53f6a9ea4a3'
ARCHIVE_BASE_0_EXTRACTOR='innoextract'
ARCHIVE_BASE_0_EXTRACTOR_OPTIONS='--gog'
ARCHIVE_BASE_0_PART1='setup_skyshines_bedlam_redux_2.0.0.2-1.bin'
ARCHIVE_BASE_0_PART1_MD5='30cdb581d87cc26574c5bbb935e897fa'
ARCHIVE_BASE_0_PART1_TYPE='rar'
ARCHIVE_BASE_0_SIZE='2800000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.0.0.2'

CONTENT_PATH_DEFAULT='game'
CONTENT_GAME_BIN_FILES='
win32'
CONTENT_GAME_DATA_FILES='
assets'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Roaming/SkyshinesBedlam'

APP_MAIN_EXE='win32/Skyshines BEDLAM.exe'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

target_version='2.24'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
