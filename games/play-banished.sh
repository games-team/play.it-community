#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Banished
# send your bug reports to contact@dotslashplay.it
###

script_version=20240331.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='banished'
GAME_NAME='Banished'

ARCHIVE_BASE_32BIT_0_NAME='setup_banished_32_1.0.7_(14938).exe'
ARCHIVE_BASE_32BIT_0_MD5='43042701a692f186d467b97e966fb846'
ARCHIVE_BASE_32BIT_0_TYPE='innosetup'
ARCHIVE_BASE_32BIT_0_SIZE='190000'
ARCHIVE_BASE_32BIT_0_VERSION='1.0.7-gog14938'
ARCHIVE_BASE_32BIT_0_URL='https://www.gog.com/game/banished'

ARCHIVE_BASE_64BIT_0_NAME='setup_banished_64_1.0.7_(14938).exe'
ARCHIVE_BASE_64BIT_0_MD5='463b2720c5c88c28f24de9176b8b1ec4'
ARCHIVE_BASE_64BIT_0_TYPE='innosetup'
ARCHIVE_BASE_64BIT_0_SIZE='190000'
ARCHIVE_BASE_64BIT_0_VERSION='1.0.7-gog14938'
ARCHIVE_BASE_64BIT_0_URL='https://www.gog.com/game/banished'

CONTENT_PATH_DEFAULT='app'
## TODO: Use a more targeted list of libraries.
CONTENT_GAME_BIN_FILES='
*.dll
*.exe'
CONTENT_GAME_DATA_FILES='
windata'

APP_MAIN_EXE_64BIT='application-x64.exe'
APP_MAIN_EXE_32BIT='application-x32.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH_64BIT='64'
PKG_BIN_ARCH_32BIT='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
