#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Potion Craft: Alchemy Simulator
# send your bug reports to contact@dotslashplay.it
###

script_version=20241216.2

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='potion-craft'
GAME_NAME='Potion Craft: Alchemy Simulator'

GAME_ID_DEMO="${GAME_ID}-demo"
GAME_NAME_DEMO="$GAME_NAME (demo)"

ARCHIVE_BASE_1_NAME='setup_potion_craft_alchemist_simulator_1.1.0.0_(64bit)_(69392).exe'
ARCHIVE_BASE_1_MD5='f92044dc682cf903e88ed8aa8a00d665'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='1669279'
ARCHIVE_BASE_1_VERSION='1.1.0.0-gog64bit'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/potion_craft_alchemist_simulator'

ARCHIVE_BASE_0_NAME='setup_potion_craft_alchemist_simulator_1.0_(64bit)_(60776).exe'
ARCHIVE_BASE_0_MD5='fa4aeefeba0f8cec711f4e22f9369e6c'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='1200000'
ARCHIVE_BASE_0_VERSION='1.0.2-gog60776'

## This installer is no longer available from gog.com.
ARCHIVE_BASE_DEMO_0_NAME='setup_potion_craft_demo_21.01.30_(46141).exe'
ARCHIVE_BASE_DEMO_0_MD5='ce1bfadccaf2821692dd61e74bdb1e1a'
ARCHIVE_BASE_DEMO_0_TYPE='innosetup'
ARCHIVE_BASE_DEMO_0_SIZE='1400000'
ARCHIVE_BASE_DEMO_0_VERSION='2021.01.30-gog46141'

UNITY3D_NAME='potion craft'

CONTENT_PATH_DEFAULT='.'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/niceplay games/Potion Craft'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_DEMO="${GAME_ID_DEMO}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
