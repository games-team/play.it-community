#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 BetaRays
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Duet
# send your bug reports to contact@dotslashplay.it
###

script_version=20231111.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='duet'
GAME_NAME='Duet'

# This DRM-free build is no longer available for sale from humblebundle.com
ARCHIVE_BASE_0_NAME='Duet-Build1006023-Linux64.zip'
ARCHIVE_BASE_0_MD5='b9c34c29da94c199ee75a5e71272a1eb'
ARCHIVE_BASE_0_SIZE='210000'
ARCHIVE_BASE_0_VERSION='1.0-humble1006023'

CONTENT_PATH_DEFAULT='.'
CONTENT_LIBS_BIN_FILES='
libsfml-audio.so.2.3
libsfml-graphics.so.2.3
libsfml-system.so.2.3
libsfml-window.so.2.3'
## The game binary is linked against libsteam_api.so, so it can not be dropped.
CONTENT_LIBS_BIN_FILES="${CONTENT_LIBS_BIN_FILES:-}
libsteam_api.so"
CONTENT_LIBS0_BIN_PATH='steam-runtime/amd64/usr/lib/x86_64-linux-gnu'
CONTENT_LIBS0_BIN_FILES='
libjpeg.so.8
libjpeg.so.8.0.2'
CONTENT_GAME_BIN_FILES='
Duet'
CONTENT_GAME_DATA_FILES='
Media'

APP_MAIN_EXE='Duet'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libFLAC.so.8
libfreetype.so.6
libgcc_s.so.1
libGL.so.1
libm.so.6
libogg.so.0
libopenal.so.1
libpthread.so.0
librt.so.1
libstdc++.so.6
libudev.so.0
libvorbisenc.so.2
libvorbisfile.so.3
libvorbis.so.0
libX11.so.6
libX11-xcb.so.1
libxcb-randr.so.0
libxcb.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Check for the presence of optional extra archives

ARCHIVE_OPTIONAL_ICONS_0_NAME='duet_icons.tar.xz'
ARCHIVE_OPTIONAL_ICONS_0_MD5='57d79a524801768a742405a7a5310e08'
ARCHIVE_OPTIONAL_ICONS_0_URL='https://downloads.dotslashplay.it/games/duet'
archive_initialize_optional \
	'ARCHIVE_ICONS' \
	'ARCHIVE_OPTIONAL_ICONS_0'

# Extract game data

archive_extraction_default
if archive_is_available 'ARCHIVE_ICONS'; then
	archive_extraction 'ARCHIVE_ICONS'
fi

# Include game data

if archive_is_available 'ARCHIVE_ICONS'; then
	CONTENT_ICONS_DATA_PATH='.'
	CONTENT_ICONS_DATA_FILES='
	16x16
	32x32
	48x48
	128x128
	256x256'
	content_inclusion 'ICONS_DATA' 'PKG_DATA' "$(path_icons)"
fi
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
