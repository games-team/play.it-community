#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Heroes of Might and Magic
# send your bug reports to contact@dotslashplay.it
###

script_version=20250114.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='heroes-of-might-and-magic-1'
GAME_NAME='Heroes of Might and Magic: A Strategic Quest'

ARCHIVE_BASE_EN_2_NAME='setup_heroes_of_might_and_magic_1.5_editor_fix_(77097).exe'
ARCHIVE_BASE_EN_2_MD5='77e51f82f815601154ed29649609c31e'
ARCHIVE_BASE_EN_2_TYPE='innosetup'
ARCHIVE_BASE_EN_2_SIZE='623025'
ARCHIVE_BASE_EN_2_VERSION='1.5-gog77097'
ARCHIVE_BASE_EN_2_URL='https://www.gog.com/game/heroes_of_might_and_magic'

ARCHIVE_BASE_FR_2_NAME='setup_heroes_of_might_and_magic_1.5_editor_fix_(french)_(77097).exe'
ARCHIVE_BASE_FR_2_MD5='4df415227a134721a41c92a8e4ceed4b'
ARCHIVE_BASE_FR_2_TYPE='innosetup'
ARCHIVE_BASE_FR_2_SIZE='622364'
ARCHIVE_BASE_FR_2_VERSION='1.5-gog77097'
ARCHIVE_BASE_FR_2_URL='https://www.gog.com/game/heroes_of_might_and_magic'

ARCHIVE_BASE_EN_1_NAME='setup_heroes_of_might_and_magic_1.2_(1.1)_(33754).exe'
ARCHIVE_BASE_EN_1_MD5='f3100c6547ef1bb82af6dd6fec66bcbf'
ARCHIVE_BASE_EN_1_TYPE='innosetup'
ARCHIVE_BASE_EN_1_SIZE='630000'
ARCHIVE_BASE_EN_1_VERSION='1.2-gog33754'

ARCHIVE_BASE_FR_1_NAME='setup_heroes_of_might_and_magic_1.2_(1.1)_(french)_(33754).exe'
ARCHIVE_BASE_FR_1_MD5='ed647dbfc98cd59dba885dc4fd005a62'
ARCHIVE_BASE_FR_1_TYPE='innosetup'
ARCHIVE_BASE_FR_1_SIZE='630000'
ARCHIVE_BASE_FR_1_VERSION='1.2-gog33754'

ARCHIVE_BASE_EN_0_NAME='setup_heroes_of_might_and_magic_2.3.0.45.exe'
ARCHIVE_BASE_EN_0_MD5='2cae1821085090e30e128cd0a76b0d21'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_SIZE='530000'
ARCHIVE_BASE_EN_0_VERSION='1.0-gog2.3.0.45'

ARCHIVE_BASE_FR_0_NAME='setup_heroes_of_might_and_magic_french_2.3.0.45.exe'
ARCHIVE_BASE_FR_0_MD5='9ec736a2a1b97dc36257f583f42864ac'
ARCHIVE_BASE_FR_0_TYPE='innosetup'
ARCHIVE_BASE_FR_0_SIZE='530000'
ARCHIVE_BASE_FR_0_VERSION='1.0-gog2.3.0.45'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_EN_0='app'
CONTENT_PATH_DEFAULT_FR_0='app'
CONTENT_GAME_MAIN_FILES='
wail32.dll
wing.32
data/campaign.hs
data/heroes.agg
data/standard.hs
games
maps/*.map
*.exe
*.cfg'
CONTENT_GAME0_MAIN_PATH='sys'
CONTENT_GAME0_MAIN_FILES='
wing32.dll'
CONTENT_GAME_COMMON_FILES='
data
homm1.gog
maps/*.cmp'
CONTENT_DOC_COMMON_FILES='
help
*.pdf
*.txt'

GAME_IMAGE='homm1.gog'
GAME_IMAGE_TYPE='iso'

USER_PERSISTENT_DIRECTORIES='
games
maps'
USER_PERSISTENT_FILES='
*.cfg'

APP_MAIN_EXE='heroes.exe'
APP_MAIN_ICON='app/goggame-1207658748.ico'
APP_MAIN_ICON_EN_0='goggame-1207658748.ico'
APP_MAIN_ICON_FR_0='goggame-1207658748.ico'

PACKAGES_LIST='
PKG_MAIN
PKG_COMMON'

PKG_MAIN_ID="$GAME_ID"
PKG_MAIN_ID_EN="${PKG_MAIN_ID}-en"
PKG_MAIN_ID_FR="${PKG_MAIN_ID}-fr"
PKG_MAIN_PROVIDES="
$PKG_MAIN_ID"
PKG_MAIN_DESCRIPTION_EN='English version'
PKG_MAIN_DESCRIPTION_FR='French version'
PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_COMMON'

PKG_COMMON_ID="${GAME_ID}-common"
PKG_COMMON_DESCRIPTION='common data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_COMMON'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_MAIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
