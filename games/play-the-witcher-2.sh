#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Mopi
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Witcher 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20241124.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='the-witcher-2'
GAME_NAME='The Witcher 2: Assassins of Kings'

ARCHIVE_BASE_1_NAME='the_witcher_2_assassins_of_kings_enhanced_edition_en_release_3_20150306204412_20992.sh'
ARCHIVE_BASE_1_MD5='fd7b85d44e3da7fdf860ab4267574b36'
ARCHIVE_BASE_1_SIZE='24000000'
ARCHIVE_BASE_1_VERSION='1.3-gog.20992'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/the_witcher_2'

ARCHIVE_BASE_0_NAME='gog_the_witcher_2_assassins_of_kings_enhanced_edition_2.2.0.8.sh'
ARCHIVE_BASE_0_MD5='3fff5123677a7be2023ecdb6af3b82b6'
ARCHIVE_BASE_0_SIZE='24000000'
ARCHIVE_BASE_0_VERSION='1.3-gog.2.2.0.8'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN_FILES='
libopenal-eon.so.1'
CONTENT_GAME_BIN_FILES='
bin
configurator
eONprecompiledShaders32.dat
sdlinput
witcher2'
CONTENT_GAME_VOICES_DE_FILES='
CookedPC/de0.w2speech'
CONTENT_GAME_VOICES_EN_FILES='
CookedPC/en0.w2speech'
CONTENT_GAME_VOICES_FR_FILES='
CookedPC/fr0.w2speech'
CONTENT_GAME_VOICES_PL_FILES='
CookedPC/pl0.w2speech'
CONTENT_GAME_VOICES_RU_FILES='
CookedPC/ru0.w2speech'
CONTENT_GAME_DATA_PACK1_FILES='
CookedPC/pack0.dzip.split00'
CONTENT_GAME_DATA_PACK2_FILES='
CookedPC/pack0.dzip.split01
CookedPC/pack0.dzip.split02'
CONTENT_GAME_DATA_FILES='
CookedPC
fontconfig
linux
icudt52l.dat
SDLGamepad.config
VPFS_registry.vpfsdb
witcher2.vpfs'
CONTENT_DOC_DATA_FILES='
*.rtf
*.txt'

APP_MAIN_EXE='witcher2'
APP_MAIN_ICON='linux/icons/witcher2-icon.png'

APP_CONFIG_ID="${GAME_ID}-config"
APP_CONFIG_NAME="$GAME_NAME - configuration"
APP_CONFIG_CAT='Settings'
APP_CONFIG_EXE='configurator'
APP_CONFIG_ICON='linux/icons/witcher2-configurator.png'

PACKAGES_LIST='
PKG_BIN
PKG_VOICES_DE
PKG_VOICES_EN
PKG_VOICES_FR
PKG_VOICES_PL
PKG_VOICES_RU
PKG_DATA_PACK1
PKG_DATA_PACK2
PKG_DATA'

PKG_VOICES_ID="${GAME_ID}-voices"
PKG_VOICES_DE_ID="${PKG_VOICES_ID}-de"
PKG_VOICES_EN_ID="${PKG_VOICES_ID}-en"
PKG_VOICES_FR_ID="${PKG_VOICES_ID}-fr"
PKG_VOICES_PL_ID="${PKG_VOICES_ID}-pl"
PKG_VOICES_RU_ID="${PKG_VOICES_ID}-ru"
PKG_VOICES_PROVIDES="
$PKG_VOICES_ID"
PKG_VOICES_DE_PROVIDES="$PKG_VOICES_PROVIDES"
PKG_VOICES_EN_PROVIDES="$PKG_VOICES_PROVIDES"
PKG_VOICES_FR_PROVIDES="$PKG_VOICES_PROVIDES"
PKG_VOICES_PL_PROVIDES="$PKG_VOICES_PROVIDES"
PKG_VOICES_RU_PROVIDES="$PKG_VOICES_PROVIDES"
PKG_VOICES_DESCRIPTION='voices'
PKG_VOICES_DE_DESCRIPTION="$PKG_VOICES_DESCRIPTION - German"
PKG_VOICES_EN_DESCRIPTION="$PKG_VOICES_DESCRIPTION - English"
PKG_VOICES_FR_DESCRIPTION="$PKG_VOICES_DESCRIPTION - French"
PKG_VOICES_PL_DESCRIPTION="$PKG_VOICES_DESCRIPTION - Polish"
PKG_VOICES_RU_DESCRIPTION="$PKG_VOICES_DESCRIPTION - Russian"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_PACK1_ID="${PKG_DATA_ID}-pack1"
PKG_DATA_PACK2_ID="${PKG_DATA_ID}-pack2"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_PACK1_DESCRIPTION="$PKG_DATA_DESCRIPTION - pack0, part 1"
PKG_DATA_PACK2_DESCRIPTION="$PKG_DATA_DESCRIPTION - pack0, part 2"
PKG_DATA_DEPS="$PKG_DATA_PACK1_ID $PKG_DATA_PACK2_ID"

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_VOICES_ID $PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libfreetype.so.6
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libSDL2_image-2.0.so.0
libstdc++.so.6
libX11.so.6'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

## Set package scripts to rebuild the full files from their chunks.
file_path='CookedPC/pack0.dzip'
PKG_DATA_POSTINST_RUN="${PKG_DATA_POSTINST_RUN:-}
$(huge_file_concatenate "$file_path")"
PKG_DATA_PRERM_RUN="${PKG_DATA_PRERM_RUN:-}
$(huge_file_delete "$file_path")"

packages_generation
case "$(messages_language)" in
	('fr')
		lang_string='voix %s :'
		lang_de='allemandes'
		lang_en='anglaises'
		lang_fr='françaises'
		lang_pl='polonaises'
		lang_ru='russes'
	;;
	('en'|*)
		lang_string='%s voices:'
		lang_de='German'
		lang_en='English'
		lang_fr='French'
		lang_pl='Polish'
		lang_ru='Russian'
	;;
esac
printf '\n'
printf "$lang_string" "$lang_de"
print_instructions 'PKG_BIN' 'PKG_VOICES_DE' 'PKG_DATA' 'PKG_DATA_PACK1' 'PKG_DATA_PACK2'
printf "$lang_string" "$lang_en"
print_instructions 'PKG_BIN' 'PKG_VOICES_EN' 'PKG_DATA' 'PKG_DATA_PACK1' 'PKG_DATA_PACK2'
printf "$lang_string" "$lang_fr"
print_instructions 'PKG_BIN' 'PKG_VOICES_FR' 'PKG_DATA' 'PKG_DATA_PACK1' 'PKG_DATA_PACK2'
printf "$lang_string" "$lang_pl"
print_instructions 'PKG_BIN' 'PKG_VOICES_PL' 'PKG_DATA' 'PKG_DATA_PACK1' 'PKG_DATA_PACK2'
printf "$lang_string" "$lang_ru"
print_instructions 'PKG_BIN' 'PKG_VOICES_RU' 'PKG_DATA' 'PKG_DATA_PACK1' 'PKG_DATA_PACK2'

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
