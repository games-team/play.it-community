#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Master of Magic
# send your bug reports to contact@dotslashplay.it
###

script_version=20240525.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='master-of-magic'
GAME_NAME='Master of Magic'

ARCHIVE_BASE_EN_0_NAME='gog_master_of_magic_2.0.0.3.sh'
ARCHIVE_BASE_EN_0_MD5='149cfca1851126d15b098c0d77f6a6cc'
ARCHIVE_BASE_EN_0_SIZE='36715'
ARCHIVE_BASE_EN_0_VERSION='1.0-gog2.0.0.3'
ARCHIVE_BASE_EN_0_URL='https://www.gog.com/game/master_of_magic_classic'

ARCHIVE_BASE_FR_0_NAME='gog_master_of_magic_french_2.0.0.3.sh'
ARCHIVE_BASE_FR_0_MD5='f42da1e4e55951cd7cd859328419fced'
ARCHIVE_BASE_FR_0_SIZE='207248'
ARCHIVE_BASE_FR_0_VERSION='1.0-gog2.0.0.3'
ARCHIVE_BASE_FR_0_URL='https://www.gog.com/game/master_of_magic_classic'

CONTENT_PATH_DEFAULT='data/noarch/data'
CONTENT_GAME_L10N_FILES='
ITEMMAKE.EXE
MAGIC.EXE
WIZARDS.EXE
BUILDDAT.LBX
BUILDESC.LBX
CITYNAME.LBX
DESC.LBX
DIPLOMSG.LBX
EVENTMSG.LBX
HELP.LBX
INTRO.LBX
ITEMDATA.LBX
ITEMPOW.LBX
LISTDAT.LBX
LOSE.LBX
MESSAGE.LBX
README.TXT
SOUNDFX.LBX
SPELLDAT.LBX
SPELLSCR.LBX
MAGIC.SET'
CONTENT_GAME_MAIN_FILES='
*.AD
*.BNK
*.CAT
*.COM
*.DAT
*.EXE
*.GIF
*.LBX
*.MOM
*.MT
*.OPL'
CONTENT_DOC_L10N_FILES='
spellbook.pdf'
CONTENT_DOC0_L10N_PATH='data/noarch/docs'
CONTENT_DOC0_L10N_FILES='
*.pdf'

USER_PERSISTENT_FILES='
MAGIC.SET
*.GAM'

APP_MAIN_EXE='MAGIC.EXE'
APP_MAIN_ICON='../support/icon.png'

PACKAGES_LIST='
PKG_L10N
PKG_MAIN'

PKG_L10N_ID="${GAME_ID}-l10n"
PKG_L10N_ID_EN="${PKG_L10N_ID}-en"
PKG_L10N_ID_FR="${PKG_L10N_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_ID"
PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_FR='French localization'

PKG_MAIN_DEPS="$PKG_L10N_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
