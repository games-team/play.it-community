#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Plant Daddy
# send your bug reports to contact@dotslashplay.it
###

script_version=20231002.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='plant-daddy'
GAME_NAME='Plant Daddy'

ARCHIVE_BASE_2='plantdaddy-linux-1.2.2.zip'
ARCHIVE_BASE_2_MD5='e64ec41dec67d889cfac4763147af384'
ARCHIVE_BASE_2_SIZE='130000'
ARCHIVE_BASE_2_VERSION='1.2.2-itch1'
ARCHIVE_BASE_2_URL='https://overfull.itch.io/plantdaddy'

ARCHIVE_BASE_1='plantdaddy-linux-1.2.1.zip'
ARCHIVE_BASE_1_MD5='1a863b49a4fe6e4c5ad136d808912058'
ARCHIVE_BASE_1_SIZE='140000'
ARCHIVE_BASE_1_VERSION='1.2.1-itch1'

ARCHIVE_BASE_0='plantdaddy-linux-1.2.0.zip'
ARCHIVE_BASE_0_MD5='b626d5a0512cc0334749de3ec137c492'
ARCHIVE_BASE_0_SIZE='140000'
ARCHIVE_BASE_0_VERSION='1.2.0-itch1'

UNITY3D_NAME='plantdaddy'
UNITY3D_NAME_0='plantdaddy-linux-1.2'
UNITY3D_PLUGINS='
libStandaloneFileBrowser.so'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES_0="
${UNITY3D_NAME}_Data/MonoBleedingEdge
UnityPlayer.so
plantdaddy-linux-1.2.0"

APP_MAIN_EXE_0='plantdaddy-linux-1.2.0'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-3.so.0
libm.so.6
libpthread.so.0
librt.so.1
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
