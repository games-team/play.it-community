#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2019 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Stardew Valley
# send your bug reports to contact@dotslashplay.it
###

script_version=20240516.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='stardew-valley'
GAME_NAME='Stardew Valley'

# Archives

## MonoGame builds

ARCHIVE_BASE_MONOGAME_4_NAME='stardew_valley_1_6_8_24119_6732702600_72964.sh'
ARCHIVE_BASE_MONOGAME_4_MD5='8a67d105c8e7b801ce950afa121aa139'
ARCHIVE_BASE_MONOGAME_4_SIZE='677398'
ARCHIVE_BASE_MONOGAME_4_VERSION='1.6.8-gog72964'
ARCHIVE_BASE_MONOGAME_4_URL='https://www.gog.com/game/stardew_valley'

ARCHIVE_BASE_MONOGAME_3_NAME='stardew_valley_1_6_3_24087_6495347694_72214.sh'
ARCHIVE_BASE_MONOGAME_3_MD5='3b37af7f3ccc2975699439ff09163fc5'
ARCHIVE_BASE_MONOGAME_3_SIZE='666325'
ARCHIVE_BASE_MONOGAME_3_VERSION='1.6.3-gog72214'

ARCHIVE_BASE_MONOGAME_2_NAME='stardew_valley_1_6_2_24081_6443062877_72053.sh'
ARCHIVE_BASE_MONOGAME_2_MD5='6718fd050430e14de9bf8e7dca7c86b8'
ARCHIVE_BASE_MONOGAME_2_SIZE='666402'
ARCHIVE_BASE_MONOGAME_2_VERSION='1.6.2-gog72053'

ARCHIVE_BASE_MONOGAME_1_NAME='stardew_valley_1_6_0_24079_6430277568_72019.sh'
ARCHIVE_BASE_MONOGAME_1_MD5='482db1736a6462600245fdd4f6bbd60a'
ARCHIVE_BASE_MONOGAME_1_SIZE='666456'
ARCHIVE_BASE_MONOGAME_1_VERSION='1.6.0-gog72019'

ARCHIVE_BASE_MONOGAME_0_NAME='stardew_valley_1_5_6_1988831614_53040.sh'
ARCHIVE_BASE_MONOGAME_0_MD5='c45810aeb8d52d30cab4d1cc7ff8ce64'
ARCHIVE_BASE_MONOGAME_0_SIZE='670000'
ARCHIVE_BASE_MONOGAME_0_VERSION='1.5.6-gog53040'

## XNA builds

ARCHIVE_BASE_XNA_11_NAME='stardew_valley_1_5_4_981587505_44377.sh'
ARCHIVE_BASE_XNA_11_MD5='ffeb52df688e169950d2b9d883c5e390'
ARCHIVE_BASE_XNA_11_SIZE='650000'
ARCHIVE_BASE_XNA_11_VERSION='1.5.4-gog44377'

ARCHIVE_BASE_XNA_10_NAME='stardew_valley_1_5_3_967165180_44219.sh'
ARCHIVE_BASE_XNA_10_MD5='c87b0beffe3236dd8545274037754c18'
ARCHIVE_BASE_XNA_10_SIZE='650000'
ARCHIVE_BASE_XNA_10_VERSION='1.5.3-gog44219'

ARCHIVE_BASE_XNA_9_NAME='stardew_valley_1_5_2_952803924_44046.sh'
ARCHIVE_BASE_XNA_9_MD5='56eb232720c737cb025f404ee74801ce'
ARCHIVE_BASE_XNA_9_SIZE='650000'
ARCHIVE_BASE_XNA_9_VERSION='1.5.2-gog44046'

ARCHIVE_BASE_XNA_8_NAME='stardew_valley_1_5_1_931692592_43684.sh'
ARCHIVE_BASE_XNA_8_MD5='8d9e0b2df18acbc1b83cd10b05ca9196'
ARCHIVE_BASE_XNA_8_SIZE='650000'
ARCHIVE_BASE_XNA_8_VERSION='1.5.1-gog43684'

ARCHIVE_BASE_XNA_7_NAME='stardew_valley_1_5_928320980_43631.sh'
ARCHIVE_BASE_XNA_7_MD5='50e1bbad197664a5b3511cba978c2fde'
ARCHIVE_BASE_XNA_7_SIZE='650000'
ARCHIVE_BASE_XNA_7_VERSION='1.5-gog43631'

ARCHIVE_BASE_XNA_6_NAME='stardew_valley_1_5_926914271_43619.sh'
ARCHIVE_BASE_XNA_6_MD5='cb92377270ecb859117e41dce26eeb69'
ARCHIVE_BASE_XNA_6_SIZE='650000'
ARCHIVE_BASE_XNA_6_VERSION='1.5-gog43619'

ARCHIVE_BASE_XNA_5_NAME='stardew_valley_1_4_5_433754439_36068.sh'
ARCHIVE_BASE_XNA_5_MD5='8d4f4fcef669a08a39e105f0eda790f4'
ARCHIVE_BASE_XNA_5_SIZE='1100000'
ARCHIVE_BASE_XNA_5_VERSION='1.4.5-gog36068'

ARCHIVE_BASE_XNA_4_NAME='stardew_valley_1_4_3_379_34693.sh'
ARCHIVE_BASE_XNA_4_MD5='07875ca8c7823f48a7eb533ac157a9da'
ARCHIVE_BASE_XNA_4_SIZE='1700000'
ARCHIVE_BASE_XNA_4_VERSION='1.4.3-gog34693'

ARCHIVE_BASE_XNA_3_NAME='stardew_valley_1_4_1_367430508_34378.sh'
ARCHIVE_BASE_XNA_3_MD5='2fc31edf997230c90c90c33e096d5762'
ARCHIVE_BASE_XNA_3_SIZE='1700000'
ARCHIVE_BASE_XNA_3_VERSION='1.4.1-gog34378'

ARCHIVE_BASE_XNA_2_NAME='stardew_valley_1_3_36_27827.sh'
ARCHIVE_BASE_XNA_2_MD5='8dd18eb151471a5901592188dfecb8a3'
ARCHIVE_BASE_XNA_2_SIZE='990000'
ARCHIVE_BASE_XNA_2_VERSION='1.3.36-gog27827'

ARCHIVE_BASE_XNA_1_NAME='stardew_valley_en_1_3_28_22957.sh'
ARCHIVE_BASE_XNA_1_MD5='e1e98cc3e891f5aafc23fb6617d6bc05'
ARCHIVE_BASE_XNA_1_SIZE='970000'
ARCHIVE_BASE_XNA_1_VERSION='1.3.28-gog22957'

ARCHIVE_BASE_XNA_0_NAME='gog_stardew_valley_2.8.0.10.sh'
ARCHIVE_BASE_XNA_0_MD5='27c84537bee1baae4e3c2f034cb0ff2d'
ARCHIVE_BASE_XNA_0_SIZE='490000'
ARCHIVE_BASE_XNA_0_VERSION='1.2.33-gog2.8.0.10'

# Archives contents

CONTENT_PATH_DEFAULT='data/noarch/game'

## MonoGame builds

CONTENT_LIBS_BIN_FILES_MONOGAME='
libclrjit.so
libcoreclr.so
libcoreclrtraceptprovider.so
libdbgshim.so
libGalaxy64.so
libGalaxyCSharpGlue.so
libhostfxr.so
libhostpolicy.so
liblwjgl_lz4.so
libmscordaccore.so
libmscordbi.so
libSkiaSharp.so
libSystem.Globalization.Native.so
libSystem.IO.Compression.Native.so
libSystem.Native.so
libSystem.Net.Security.Native.so
libSystem.Security.Cryptography.Native.OpenSsl.so
libSystem.*.a'
## The shipped SDL library must be included, to avoid the following crash on launch:
## Unhandled exception. System.Exception: SDL 2.0.4 does not support changing resizable parameter of the window after it's already been created, please use a newer version of it.
##    at Microsoft.Xna.Framework.SdlGameWindow.set_AllowUserResizing(Boolean value)
##    at StardewValley.GameRunner..ctor()
##    at StardewValley.Program.Main(String[] args)
CONTENT_LIBS0_BIN_FILES_MONOGAME='
libSDL2-2.0.so.0'
## The shipped FAudio library must be included, to prevent missing audio:
## FAudio: Failed to initialize.
## Game.Initialize() caught exception initializing XACT:
## Microsoft.Xna.Framework.Audio.NoAudioHardwareException (0x80004005)
##    at Microsoft.Xna.Framework.Audio.SoundEffect.Device()
##    at Microsoft.Xna.Framework.Audio.SoundEffect.Initialize()
##    at Microsoft.Xna.Framework.Audio.SoundEffect..ctor(MiniFormatTag codec, Byte[] buffer, Int32 channels, Int32 sampleRate, Int32 blockAlignment, Int32 loopStart, Int32 loopLength)
##    at Microsoft.Xna.Framework.Audio.WaveBank..ctor(AudioEngine audioEngine, String waveBankFilename, Boolean streaming, Int32 offset, Int32 packetsize)
##    at StardewValley.Game1.Initialize()
CONTENT_LIBS1_BIN_FILES_MONOGAME='
libFAudio.so.0'
CONTENT_GAME_BIN_FILES_MONOGAME='
createdump
Stardew Valley
*.dll
*.json
*.xml'
CONTENT_GAME_DATA_FILES_MONOGAME='
Content'
CONTENT_DOC_DATA_FILES_MONOGAME='
lwjgl_license.txt
lz4_license.txt'

## XNA builds

CONTENT_GAME_MAIN_FILES_XNA='
Content
mono
monoconfig
StardewValley.exe
BmFont.dll
GalaxyCSharp.dll
GalaxyCSharp.dll.config
libSkiaSharp.dll
Lidgren.Network.dll
MonoGame.Framework.dll
MonoGame.Framework.dll.config
SkiaSharp.dll
StardewValley.GameData.dll
xTile.dll
xTilePipeline.dll'

# Launchers

APP_MAIN_ICON='../support/icon.png'

## MonoGame builds

APP_MAIN_EXE_MONOGAME='Stardew Valley'
## If run from a symlinks prefix, the game process escapes it and actually runs from the system path.
APP_MAIN_PREFIX_TYPE_MONOGAME='none'

## XNA builds

APP_MAIN_EXE_XNA='StardewValley.exe'

# Packages

## MonoGame builds

PACKAGES_LIST_MONOGAME='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH_MONOGAME='64'
PKG_BIN_DEPENDENCIES_LIBRARIES_MONOGAME='
libc.so.6
libdl.so.2
libfontconfig.so.1
libgcc_s.so.1
liblttng-ust.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'
## Old MonoGame builds have some extra dependencies.
PKG_BIN_DEPENDENCIES_LIBRARIES_MONOGAME_0="${PKG_BIN_DEPENDENCIES_LIBRARIES_MONOGAME:-}
libgssapi_krb5.so.2
libssl.so.1.1"

PKG_DATA_ID_MONOGAME="${GAME_ID}-data"
PKG_DATA_DESCRIPTION_MONOGAME='data'
PKG_BIN_DEPS_MONOGAME="${PKG_BIN_DEPS_MONOGAME:-} $PKG_DATA_ID_MONOGAME"

## XNA builds

PKG_MAIN_DEPENDENCIES_LIBRARIES_XNA='
libGL.so.1
libopenal.so.1
libSDL2-2.0.so.0'
PKG_MAIN_DEPENDENCIES_MONO_LIBRARIES_XNA='
mscorlib.dll
Mono.Posix.dll
Mono.Security.dll
System.dll
System.Configuration.dll
System.Core.dll
System.Data.dll
System.Drawing.dll
System.Runtime.Serialization.dll
System.Security.dll
System.Xml.dll
System.Xml.Linq.dll
WindowsBase.dll'

# XNA builds - The game manager is provided as an ELF binary

CONTENT_GAME_BIN64_FILES_XNA='
mcs.bin.x86_64'
CONTENT_GAME_BIN32_FILES_XNA='
mcs.bin.x86'

PACKAGES_LIST_XNA='
PKG_MAIN
PKG_BIN64
PKG_BIN32'

PKG_BIN_ID_XNA="${GAME_ID}-bin"
PKG_BIN64_ID_XNA="$PKG_BIN_ID_XNA"
PKG_BIN32_ID_XNA="$PKG_BIN_ID_XNA"
PKG_BIN64_ARCH_XNA='64'
PKG_BIN32_ARCH_XNA='32'
PKG_BIN_DESCRIPTION_XNA='game manager binary'
PKG_BIN64_DESCRIPTION_XNA="$PKG_BIN_DESCRIPTION_XNA"
PKG_BIN32_DESCRIPTION_XNA="$PKG_BIN_DESCRIPTION_XNA"
PKG_BIN_DEPENDENCIES_LIBRARIES_XNA='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES_XNA="$PKG_BIN_DEPENDENCIES_LIBRARIES_XNA"
PKG_BIN32_DEPENDENCIES_LIBRARIES_XNA="$PKG_BIN_DEPENDENCIES_LIBRARIES_XNA"

PKG_MAIN_DEPS_XNA="${PKG_MAIN_DEPS_XNA:-} $PKG_BIN_ID_XNA"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# MonoGame builds - The game fails to load some libraries if they are not found in the game path.

case "$(current_archive)" in
	('ARCHIVE_BASE_MONOGAME_'*)
		mkdir --parents "$(package_path 'PKG_BIN')$(path_game_data)"
		for library_file in \
			'libclrjit.so' \
			'libcoreclr.so' \
			'libcoreclrtraceptprovider.so' \
			'libdbgshim.so' \
			'libhostfxr.so' \
			'libhostpolicy.so' \
			'libmscordaccore.so' \
			'libmscordbi.so' \
			'libSkiaSharp.so' \
			'libSystem.IO.Compression.Native.a' \
			'libSystem.IO.Compression.Native.so' \
			'libSystem.Native.a' \
			'libSystem.Native.so' \
			'libSystem.Net.Security.Native.a' \
			'libSystem.Net.Security.Native.so' \
			'libSystem.Security.Cryptography.Native.OpenSsl.a' \
			'libSystem.Security.Cryptography.Native.OpenSsl.so' \
			'libGalaxy64.so' \
			'libGalaxyCSharpGlue.so' \
			'libSDL2-2.0.so.0' \
			'libFAudio.so.0'
		do
			ln --symbolic \
				"$(path_libraries)/${library_file}" \
				"$(package_path 'PKG_BIN')$(path_game_data)/${library_file}"
		done
	;;
esac

# XNA builds - The game fails to load OpenAL and SDL2 libraries if they are not inside some hardcoded path.

case "$(current_archive)" in
	('ARCHIVE_BASE_XNA_'*)
		case "$(option_value 'package')" in
			('arch')
				library_source_directory_32bit='/usr/lib32'
				library_source_directory_64bit='/usr/lib'
			;;
			('deb')
				library_source_directory_32bit='/usr/lib/i386-linux-gnu'
				library_source_directory_64bit='/usr/lib/x86_64-linux-gnu'
			;;
			('gentoo'|'egentoo')
				library_source_directory_32bit='/usr/lib'
				library_source_directory_64bit='/usr/lib64'
			;;
		esac
		for library_file in \
			'libopenal.so.1' \
			'libSDL2-2.0.so.0'
		do
			library_source_32bit="${library_source_directory_32bit}/${library_file}"
			library_source_64bit="${library_source_directory_64bit}/${library_file}"
			library_destination_32bit="$(package_path 'PKG_BIN32')$(path_game_data)/lib/${library_file}"
			library_destination_64bit="$(package_path 'PKG_BIN64')$(path_game_data)/lib64/${library_file}"
			mkdir --parents \
				"$(dirname "$library_destination_32bit")" \
				"$(dirname "$library_destination_64bit")"
			ln --symbolic "$library_source_32bit" "$library_destination_32bit"
			ln --symbolic "$library_source_64bit" "$library_destination_64bit"
		done
	;;
esac

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## XNA builds - Ensure game manager binaries are executable.
	case "$(current_archive)" in
		('ARCHIVE_BASE_XNA_'*)
			chmod 755 'mcs.bin.x86' 'mcs.bin.x86_64'
		;;
	esac
)

# Include game icons

case "$(current_archive)" in
	('ARCHIVE_BASE_XNA_'*)
		content_inclusion_icons
	;;
	('ARCHIVE_BASE_MONOGAME_'*)
		content_inclusion_icons 'PKG_DATA'
	;;
esac

# Include game data

content_inclusion_default

# Write launchers

case "$(current_archive)" in
	('ARCHIVE_BASE_XNA_'*)
		launchers_generation
	;;
	('ARCHIVE_BASE_MONOGAME_'*)
		launchers_generation 'PKG_BIN'
	;;
esac

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
