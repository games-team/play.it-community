#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Toren
# send your bug reports to contact@dotslashplay.it
###

script_version=20240622.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='toren'
GAME_NAME='Toren'

## This free installer is no longer available for sale from gog.com.
ARCHIVE_BASE_0_NAME='setup_toren_gog-1_(17651).exe'
ARCHIVE_BASE_0_MD5='4785b339bb5918722ff6a9f30f9d5c04'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_toren_gog-1_(17651)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='877ca6f3271cb03b46b5de71a6ff37de'
ARCHIVE_BASE_0_SIZE='2400000'
ARCHIVE_BASE_0_VERSION='1.0-gog17651'

UNITY3D_NAME='toren'

CONTENT_PATH_DEFAULT='app'

USER_PERSISTENT_FILES='
*.sav'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
