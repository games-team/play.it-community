#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Ultima 4
# send your bug reports to contact@dotslashplay.it
###

script_version=20250115.2

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='ultima-4'
GAME_NAME='Ultima IV: Quest of the Avatar'

ARCHIVE_BASE_1_NAME='setup_ultima_iv_-_quest_of_the_avatar_1.0_cs_(28045).exe'
ARCHIVE_BASE_1_MD5='f4c943b03e576557a962bcd88470509f'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='16714'
ARCHIVE_BASE_1_VERSION='1.0-gog28045'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/ultima_4'

ARCHIVE_BASE_0_NAME='setup_ultima_iv_-_quest_of_the_avatar_1.0_cs_(28045).exe'
ARCHIVE_BASE_0_MD5='2c8adeb4cacfba84b611e9c28ddf12e0'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='17589'
ARCHIVE_BASE_0_VERSION='1.0-gog28045'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_MAIN_FILES='
ultima.com
avatar.exe
title.exe
ultima4.txt
*.cga
*.con
*.dng
*.drv
*.ega
*.egz
*.map
*.new
*.pic
*.tlk
*.ult'
CONTENT_GAME0_MAIN_PATH='__support/save'
CONTENT_GAME0_MAIN_FILES='
*.sav'
CONTENT_DOC_MAIN_FILES='
manual.pdf
readme.rtf'

USER_PERSISTENT_FILES='
*.sav'

APP_MAIN_EXE='ultima.com'
APP_MAIN_ICON='app/goggame-1207658962.ico'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
