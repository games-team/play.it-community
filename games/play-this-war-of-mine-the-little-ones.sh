#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# This War of Mine: The Little Ones
# send your bug reports to contact@dotslashplay.it
###

script_version=20240623.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='this-war-of-mine'
GAME_NAME='Thie War of Mine'

EXPANSION_ID='the-little-ones'
EXPANSION_NAME='The Little Ones'

ARCHIVE_BASE_0_NAME='this_war_of_mine_the_little_ones_6_0_8_a_34693.sh'
ARCHIVE_BASE_0_MD5='7e2a1b8e0cc51d7a3f56f6591a0f6f21'
ARCHIVE_BASE_0_SIZE='18000'
ARCHIVE_BASE_0_VERSION='6.0.8a-gog34693'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/this_war_of_mine_the_little_ones'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
gog.idx
gog.dat'

PKG_MAIN_ID="${GAME_ID}-data-gog-${EXPANSION_ID}"
PKG_MAIN_PROVIDES="
${GAME_ID}-data-gog"
PKG_MAIN_DESCRIPTION="data - GOG-specific files ($EXPANSION_NAME)"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
