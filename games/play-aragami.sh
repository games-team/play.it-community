#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Jacek Szafarkiewicz
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Aragami
# send your bug reports to contact@dotslashplay.it
###

script_version=20231017.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='aragami'
GAME_NAME='Aragami'

# Archives

## Base game

ARCHIVE_BASE_GOG_1_NAME='aragami_en_01_09_20943.sh'
ARCHIVE_BASE_GOG_1_MD5='46c8315350a4036dcc09b6c3946ddc02'
ARCHIVE_BASE_GOG_1_SIZE='6800000'
ARCHIVE_BASE_GOG_1_VERSION='01.09-gog20943'
ARCHIVE_BASE_GOG_1_URL='https://www.gog.com/game/aragami'

ARCHIVE_BASE_GOG_0_NAME='gog_aragami_2.9.0.12.sh'
ARCHIVE_BASE_GOG_0_MD5='42d0952de9b0373786f2902aa596b4ff'
ARCHIVE_BASE_GOG_0_SIZE='6800000'
ARCHIVE_BASE_GOG_0_VERSION='01.08-gog2.9.0.12'

## This installer used to be sold at humblebundle.com, but they now sell Steam keys only.
ARCHIVE_BASE_HUMBLE_0_NAME='aragami_01_08_Linux.zip'
ARCHIVE_BASE_HUMBLE_0_MD5='4be0b7f674eec62184df216fcaba77b5'
ARCHIVE_BASE_HUMBLE_0_SIZE='6800000'
ARCHIVE_BASE_HUMBLE_0_VERSION='01.08-humble170503'

## Nightfall expansion

## TODO: The expansion content should be installed using a dedicated package.
ARCHIVE_OPTIONAL_NIGHTFALL_0_NAME='aragami_nightfall_dlc_en_gog_1_21278.sh'
ARCHIVE_OPTIONAL_NIGHTFALL_0_MD5='40920a28f030147a524e5ac89e2cf14b'
ARCHIVE_OPTIONAL_NIGHTFALL_0_SIZE='7400000'
ARCHIVE_OPTIONAL_NIGHTFALL_0_URL='https://www.gog.com/game/aragami_nightfall'


UNITY3D_NAME='Aragami'
UNITY3D_PLUGINS='
ScreenSelector.so'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='.'

PACKAGES_LIST='PKG_BIN32 PKG_BIN64 PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN32_ARCH='32'
PKG_BIN64_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXrandr.so.2
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Check for the presence of optional extra archives

archive_initialize_optional \
	'ARCHIVE_NIGHTFALL' \
	'ARCHIVE_OPTIONAL_NIGHTFALL_0'

# Extract game data

archive_extraction_default
if archive_is_available 'ARCHIVE_NIGHTFALL' ; then
	archive_extraction 'ARCHIVE_NIGHTFALL'
fi

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN32'
# shellcheck disable=SC2119
launchers_write
set_current_package 'PKG_BIN64'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
