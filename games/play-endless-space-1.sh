#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Endless Space 1
# send your bug reports to contact@dotslashplay.it
###

script_version=20230716.4

GAME_ID='endless-space-1'
GAME_NAME='Endless Space'

ARCHIVE_BASE_0='setup_endlesstm_space_-_definitive_edition_1.0.0._(64185).exe'
ARCHIVE_BASE_0_MD5='84ccf58a734694a63db59e599344a254'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1='setup_endlesstm_space_-_definitive_edition_1.0.0._(64185)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='65a88affbdbff4aa78808b941184051b'
ARCHIVE_BASE_0_SIZE='3600000'
ARCHIVE_BASE_0_VERSION='1.1.59-gog64185'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/endless_space_definitive_edition'

UNITY3D_NAME='endless space'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES="
${UNITY3D_NAME}_data/plugins
${UNITY3D_NAME}.exe
*.dll"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_data
public
public_xp1"

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/Endless Space'

APP_MAIN_EXE="${UNITY3D_NAME}.exe"

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
deinterlace
video/x-ms-asf'

# The game engine expects write access to XML files.

USER_PERSISTENT_FILES='
*.xml'

# Skip playing the logo movie on launch,
# as it prevents reaching the game menu.

APP_MAIN_OPTIONS="${APP_MAIN_OPTIONS:-} -nologo"

# Load common functions

target_version='2.24'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Delete unwanted files
	rm --force --recursive \
		'__redist' \
		'app' \
		'commonappdata' \
		'tmp'
)

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
