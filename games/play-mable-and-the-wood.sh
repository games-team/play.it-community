#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Mable and the Wood
# send your bug reports to contact@dotslashplay.it
###

script_version=20250303.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='mable-and-the-wood'
GAME_NAME='Mable & the Wood'

ARCHIVE_BASE_0='mable_the_wood_1_7_37606.sh'
ARCHIVE_BASE_0_MD5='d88e40a5927c67c3974becd19d64c940'
ARCHIVE_BASE_0_SIZE='570000'
ARCHIVE_BASE_0_VERSION='1.7-gog37606'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/mable_the_wood'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN_FILES='
Mable_PC'
CONTENT_GAME_DATA_FILES='
assets'

USER_PERSISTENT_FILES='
assets/config.ini'

APP_MAIN_EXE='Mable_PC'
APP_MAIN_ICON='assets/icon.png'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libcurl-gnutls.so.4
libdl.so.2
libgcc_s.so.1
libGL.so.1
libGLU.so.1
libm.so.6
libpthread.so.0
librt.so.1
libssl.so.1.0.0
libstdc++.so.6
libX11.so.6
libXext.so.6
libXrandr.so.2
libXxf86vm.so.1
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
