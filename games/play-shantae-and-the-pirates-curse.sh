#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2022 Mopi
set -o errexit

###
# Shantae and the Pirate's Curse
# send your bug reports to contact@dotslashplay.it
###

script_version=20240706.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='shantae-and-the-pirates-curse'
GAME_NAME='Shantae and the Pirateʼs Curse'

ARCHIVE_BASE_0_NAME='setup_shantae_and_the_pirates_curse_1.04g_(18994).exe'
ARCHIVE_BASE_0_MD5='66e16f3a97328c2ba08c19a64d88c16c'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='470000'
ARCHIVE_BASE_0_VERSION='1.04g-gog18994'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/shantae_and_the_pirates_curse'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES="
shantae and the pirate's curse.exe"
CONTENT_GAME_DATA_FILES='
shantaecurse.data'

USER_PERSISTENT_FILES='
options.txt
*.save'

APP_MAIN_EXE="shantae and the pirate's curse.exe"

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
