#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Wayward Manor
# send your bug reports to contact@dotslashplay.it
###

script_version=20240516.2

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='wayward-manor'
GAME_NAME='Wayward Manor'

ARCHIVE_BASE_0_NAME='WaywardManorWin78Version1.01.zip'
ARCHIVE_BASE_0_MD5='1a832bc0fbf67f5c4675f47988ec2243'
ARCHIVE_BASE_0_SIZE='2500000'
ARCHIVE_BASE_0_VERSION='1.01-humble.2014.07.24'
ARCHIVE_BASE_0_URL='https://www.humblebundle.com/store/wayward-manor'

UNITY3D_NAME='waywardmanor'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME0_BIN_FILES='
msvcr100d.dll'

WINE_REGEDIT_PERSISTENT_KEYS='HKEY_CURRENT_USER\Software\TheOddGentlemen\Wayward Manor'
WINE_VIRTUAL_DESKTOP='auto'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_COMMANDS='
dos2unix
sed'

# The inner archive is based on InnoSetup

SCRIPT_DEPS='innoextract'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
ARCHIVE_INNER_PATH="${PLAYIT_WORKDIR}/gamedata/Wayward Manor Win 7 & 8 Version 1.01/WMSetup.exe"
ARCHIVE_INNER_TYPE='innosetup'
archive_extraction 'ARCHIVE_INNER'
rm "$(archive_path 'ARCHIVE_INNER')"

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
