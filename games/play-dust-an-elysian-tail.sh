#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Dust: An Elysian Tail
# send your bug reports to contact@dotslashplay.it
###

script_version=20230101.1

GAME_ID='dust-an-elysian-tail'
GAME_NAME='Dust: An Elysian Tail'

ARCHIVE_BASE_GOG_0='gog_dust_an_elysian_tail_2.0.0.1.sh'
ARCHIVE_BASE_GOG_0_MD5='57299e2e4af0283e68a7aa5e8b390697'
ARCHIVE_BASE_GOG_0_TYPE='mojosetup'
ARCHIVE_BASE_GOG_0_SIZE='1500000'
ARCHIVE_BASE_GOG_0_VERSION='1.04-gog2.0.0.1'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/dust_an_elysian_tail'

ARCHIVE_BASE_HUMBLE_0='dustaet_05042016-bin'
ARCHIVE_BASE_HUMBLE_0_MD5='6844c82f233b47417620be0bef8b140c'
ARCHIVE_BASE_HUMBLE_0_TYPE='mojosetup'
ARCHIVE_BASE_HUMBLE_0_SIZE='1500000'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.04-humble160504'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/dust-an-elysian-tail'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='data'
CONTENT_GAME_MAIN_FILES='
Content
data
de
es
fr
it
ja
DustAET.exe
Dust An Elysian Tail.bmp
monoconfig
FNA.dll
FNA.dll.config
MonoGame.Framework.Net.dll'
CONTENT_DOC_MAIN_FILES='
Linux.README'

APP_MAIN_TYPE='mono'
APP_MAIN_EXE='DustAET.exe'
APP_MAIN_ICON='Dust An Elysian Tail.bmp'

PACKAGES_LIST='PKG_MAIN'

PKG_MAIN_DEPS='mono'
PKG_MAIN_DEPENDENCIES_LIBRARIES='
libopenal.so.1
libSDL2-2.0.so.0
libtheoradec.so.1
libvorbisfile.so.3'

# Include shipped libraries that can not be replaced by system ones

CONTENT_GAME_LIBS32_PATH_GOG="${CONTENT_PATH_DEFAULT_GOG}/lib"
CONTENT_GAME_LIBS32_PATH_HUMBLE="${CONTENT_PATH_DEFAULT_HUMBLE}/lib"
CONTENT_GAME_LIBS32_FILES='
libmojoshader.so
libtheoraplay.so'
CONTENT_GAME_LIBS64_PATH_GOG="${CONTENT_PATH_DEFAULT_GOG}/lib64"
CONTENT_GAME_LIBS64_PATH_HUMBLE="${CONTENT_PATH_DEFAULT_HUMBLE}/lib64"
CONTENT_GAME_LIBS64_FILES='
libmojoshader.so
libtheoraplay.so'

PACKAGES_LIST="$PACKAGES_LIST PKG_LIBS32 PKG_LIBS64"

PKG_LIBS_ID="${GAME_ID}-libs"
PKG_LIBS32_ID="$PKG_LIBS_ID"
PKG_LIBS32_ARCH='32'
PKG_LIBS64_ID="$PKG_LIBS_ID"
PKG_LIBS64_ARCH='64'

PKG_MAIN_DEPS="$PKG_MAIN_DEPS $PKG_LIBS_ID"

# Load common functions

target_version='2.20'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Disable language override

mono_launcher_tweaks() {
	cat <<- 'EOF'
	## Work around terminfo Mono bug,
	## cf. https://github.com/mono/mono/issues/6752
	export TERM="${TERM%-256color}"
	EOF
}

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_MAIN'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_MAIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0

