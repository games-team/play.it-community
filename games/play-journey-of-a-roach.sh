#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Journey of a Roach
# send your bug reports to contact@dotslashplay.it
###

script_version=20231103.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='journey-of-a-roach'
GAME_NAME='Journey of a Roach'

ARCHIVE_BASE_GOG_0_NAME='setup_journey_of_a_roach_2.1.0.5.exe'
ARCHIVE_BASE_GOG_0_MD5='98b4f1c3efc2a789530ffc1f8f1c2b92'
ARCHIVE_BASE_GOG_0_TYPE='innosetup'
ARCHIVE_BASE_GOG_0_SIZE='1600000'
ARCHIVE_BASE_GOG_0_VERSION='1.30.011-gog2.1.0.5'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/journey_of_a_roach'

ARCHIVE_BASE_HUMBLE_0_NAME='JoaR_1.3_PC_Full_Multi_Daedalic_ESD.exe'
ARCHIVE_BASE_HUMBLE_0_MD5='e349a84f7b8ac095b06edea521cade8f'
ARCHIVE_BASE_HUMBLE_0_TYPE='innosetup'
ARCHIVE_BASE_HUMBLE_0_SIZE='1600000'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.30.011-humble160915'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/journey-of-a-roach'

UNITY3D_NAME='joar'

CONTENT_PATH_DEFAULT='app'

CONTENT_DOC_DATA_PATH="${CONTENT_PATH_DEFAULT}/documents"
## TODO: This glob should be replaced with an explicit list of files.
CONTENT_DOC_DATA_FILES='
*'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Local Settings/Application Data/Daedalic Entertainment/Journey of a Roach'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
