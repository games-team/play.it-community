#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Nyx Quest
# send your bug reports to contact@dotslashplay.it
###

script_version=20240812.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='nyx-quest'
GAME_NAME='NyxQuest: Kindred Spirits'

ARCHIVE_BASE_0_NAME='setup_nyxquest_kindred_spirits_1.3_(36614).exe'
ARCHIVE_BASE_0_MD5='eb9e6d7fd8136bbf581ab350fe7bb85b'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='160000'
ARCHIVE_BASE_0_VERSION='1.3-gog36614'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/nyxquest_kindred_spirits'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
devil.dll
openal32.dll
wrap_oal.dll
nyxquest.exe'
CONTENT_GAME_DATA_FILES='
datapc
nyx_icon.ico'
CONTENT_DOC_DATA_FILES='
manual.pdf'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/Over the Top Games/NyxQuest'

APP_MAIN_EXE='nyxquest.exe'
APP_MAIN_ICON='nyx_icon.ico'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
