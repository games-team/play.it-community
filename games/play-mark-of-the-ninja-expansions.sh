#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Mark of the Ninja expansions:
# - Special Edition
# send your bug reports to contact@dotslashplay.it
###

script_version=20230902.1

GAME_ID='mark-of-the-ninja'
GAME_NAME='Mark of the Ninja'

EXPANSION_ID='special-edition'
EXPANSION_NAME='Special Edition'

## This Linux build is no longer available for sale from GOG, they now only sell a Windows build.
ARCHIVE_BASE_0='gog_mark_of_the_ninja_special_edition_dlc_2.0.0.4.sh'
ARCHIVE_BASE_0_MD5='bbce70b80932ec9c14fbedf0b6b33eb1'
ARCHIVE_BASE_0_SIZE='250000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.0.0.4'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN32_FILES='
ninja-bin32'
CONTENT_GAME_BIN64_FILES='
ninja-bin64'
CONTENT_GAME_DATA_FILES='
dlc'

PACKAGES_LIST='PKG_BIN32 PKG_BIN64 PKG_DATA'

PKG_DATA_ID="${GAME_ID}-${EXPANSION_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN32_ARCH='32'
PKG_BIN64_ARCH='64'
PKG_BIN_PROVIDES="
$GAME_ID"
PKG_BIN32_PROVIDES="$PKG_BIN_PROVIDES"
PKG_BIN64_PROVIDES="$PKG_BIN_PROVIDES"
PKG_BIN_DEPS="$PKG_DATA_ID ${GAME_ID}-libs ${GAME_ID}-data"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
libSDL-1.2.so.0
libstdc++.so.6'
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

target_version='2.25'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

for PKG in 'PKG_BIN32' 'PKG_BIN64'; do
	# shellcheck disable=SC2119
	launchers_write
done

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
