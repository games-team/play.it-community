#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Anna Lea
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Bad Dream Fever
# send your bug reports to contact@dotslashplay.it
###

script_version=20240705.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='bad-dream-fever'
GAME_NAME='Bad Dream Fever'

ARCHIVE_BASE_0_NAME='setup_bad_dream_fever_1.0_(26985).exe'
ARCHIVE_BASE_0_MD5='19336f3d5f3920873c7f111f9bda3f8b'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='490000'
ARCHIVE_BASE_0_VERSION='1.0-gog26985'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/bad_dream_fever'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
d3dx9_43.dll
bad dream fever.exe
options.ini'
## TODO: Check if the shipped build of DirectX is required.
CONTENT_GAME0_BIN_FILES='
__redist/directx'
CONTENT_GAME_DATA_FILES='
language
splash.png
data.win
*.dat
*.ogg'

## FIXME: Use persistent storage for game progress and settings.

APP_MAIN_EXE='bad dream fever.exe'
## TODO: Check if the shipped build of DirectX is required.
APP_MAIN_PRERUN='
# Install shipped DirectX on first launch
if [ ! -e directx9_installed ]; then
	## TODO: Check if this delay is required.
	sleep 3s
	wine __redist/directx/dxsetup.exe /silent
	touch directx9_installed
fi
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
