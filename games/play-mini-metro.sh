#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2020 Alarig Le Lay
set -o errexit

###
# Mini Metro
# send your bug reports to contact@dotslashplay.it
###

script_version=20230726.2

GAME_ID='mini-metro'
GAME_NAME='Mini Metro'

ARCHIVE_BASE_1='MiniMetro-release-53-linux.tar.gz'
ARCHIVE_BASE_1_MD5='67f52adb9cacd494bb79c76cfb36931d'
ARCHIVE_BASE_1_SIZE='330000'
ARCHIVE_BASE_1_VERSION='53-humble1'
ARCHIVE_BASE_1_URL='https://www.humblebundle.com/store/mini-metro'

ARCHIVE_BASE_0='MiniMetro-release-50a-linux.tar.gz'
ARCHIVE_BASE_0_MD5='93577776ea002a6e0a8dc482486ff174'
ARCHIVE_BASE_0_SIZE='320000'
ARCHIVE_BASE_0_VERSION='50a-humble1'

UNITY3D_NAME='Mini Metro'
UNITY3D_PLUGINS='
libminimetrox.so'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES="
${UNITY3D_NAME}
${UNITY3D_NAME}_Data/MonoBleedingEdge
UnityPlayer.so"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data"

APP_MAIN_EXE="$UNITY3D_NAME"

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libz.so.1'

# Load common functions

target_version='2.24'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
