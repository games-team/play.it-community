#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 BetaRays
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Proteus
# send your bug reports to contact@dotslashplay.it
###

script_version=20240830.2

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='proteus'
GAME_NAME='Proteus'

ARCHIVE_BASE_0_NAME='proteus-05162014-bin'
ARCHIVE_BASE_0_MD5='8a5911751382bcfb91483f52f781e283'
## This MojoSetup installer is not relying on a Makeself wrapper
ARCHIVE_BASE_0_EXTRACTOR='bsdtar'
ARCHIVE_BASE_0_VERSION='1.0-humble140516'
ARCHIVE_BASE_0_SIZE='130000'
ARCHIVE_BASE_0_URL='https://www.humblebundle.com/store/proteus'

CONTENT_PATH_DEFAULT='data'
CONTENT_GAME_MAIN_FILES='
resources
Proteus.exe
Proteus.png
KopiLua.dll
KopiLuaDll.dll
KopiLuaInterface.dll
SDL2-CS.dll
SDL2-CS.dll.config
Tao.OpenGl.dll
Tao.OpenGl.dll.config
Tianxia.dll
Wuwei.dll'
CONTENT_DOC_MAIN_FILES='
Linux.README'

APP_MAIN_EXE='Proteus.exe'
APP_MAIN_ICON='Proteus.png'

PKG_MAIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libSDL2_image-2.0.so.0
libSDL2_mixer-2.0.so.0
libstdc++.so.6
libz.so.1'
PKG_MAIN_DEPENDENCIES_MONO_LIBRARIES='
mscorlib.dll
Mono.Posix.dll
Mono.Security.dll
System.dll
System.Configuration.dll
System.Core.dll
System.Data.dll
System.Drawing.dll
System.Security.dll
System.Xml.dll'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
