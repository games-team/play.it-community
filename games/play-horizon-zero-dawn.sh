#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Horizon Zero Dawn
# send your bug reports to contact@dotslashplay.it
###

script_version=20241124.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='horizon-zero-dawn'
GAME_NAME='Horizon Zero Dawn'

ARCHIVE_BASE_1_NAME='setup_horizon_zero_dawntm_complete_edition_7517962_(64bit)_(53037).exe'
ARCHIVE_BASE_1_MD5='cbcd8e966a6ffd5881d9ad5f21896171'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_horizon_zero_dawntm_complete_edition_7517962_(64bit)_(53037)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='9a34a22b24fb0f0e44c17afccec79fdf'
ARCHIVE_BASE_1_PART2_NAME='setup_horizon_zero_dawntm_complete_edition_7517962_(64bit)_(53037)-2.bin'
ARCHIVE_BASE_1_PART2_MD5='0e961289ae2e6a84440d354d98a74325'
ARCHIVE_BASE_1_PART3_NAME='setup_horizon_zero_dawntm_complete_edition_7517962_(64bit)_(53037)-3.bin'
ARCHIVE_BASE_1_PART3_MD5='f7c23754103e3bcf02eec7216b9e94e3'
ARCHIVE_BASE_1_PART4_NAME='setup_horizon_zero_dawntm_complete_edition_7517962_(64bit)_(53037)-4.bin'
ARCHIVE_BASE_1_PART4_MD5='4ed606b3ac83c644dab85bbc1938057a'
ARCHIVE_BASE_1_PART5_NAME='setup_horizon_zero_dawntm_complete_edition_7517962_(64bit)_(53037)-5.bin'
ARCHIVE_BASE_1_PART5_MD5='0a08cc1604d50b5c971bfaa80008c9d9'
ARCHIVE_BASE_1_PART6_NAME='setup_horizon_zero_dawntm_complete_edition_7517962_(64bit)_(53037)-6.bin'
ARCHIVE_BASE_1_PART6_MD5='8d7b79803c494726889d1a21ae55ae63'
ARCHIVE_BASE_1_PART7_NAME='setup_horizon_zero_dawntm_complete_edition_7517962_(64bit)_(53037)-7.bin'
ARCHIVE_BASE_1_PART7_MD5='81bdcd0eb672b0e600af02724745a942'
ARCHIVE_BASE_1_PART8_NAME='setup_horizon_zero_dawntm_complete_edition_7517962_(64bit)_(53037)-8.bin'
ARCHIVE_BASE_1_PART8_MD5='1643dff8d1c8e52506e48549554a14da'
ARCHIVE_BASE_1_PART9_NAME='setup_horizon_zero_dawntm_complete_edition_7517962_(64bit)_(53037)-9.bin'
ARCHIVE_BASE_1_PART9_MD5='7a7133c16ac38ce9b4e9abfddea62f89'
ARCHIVE_BASE_1_PART10_NAME='setup_horizon_zero_dawntm_complete_edition_7517962_(64bit)_(53037)-10.bin'
ARCHIVE_BASE_1_PART10_MD5='f9df3bdac52376d3054f379e2792307c'
ARCHIVE_BASE_1_PART11_NAME='setup_horizon_zero_dawntm_complete_edition_7517962_(64bit)_(53037)-11.bin'
ARCHIVE_BASE_1_PART11_MD5='f4b7607bec174dad7f8452d6c5b0009a'
ARCHIVE_BASE_1_PART12_NAME='setup_horizon_zero_dawntm_complete_edition_7517962_(64bit)_(53037)-12.bin'
ARCHIVE_BASE_1_PART12_MD5='7681476938581baf216a54aa45d98fc4'
ARCHIVE_BASE_1_PART13_NAME='setup_horizon_zero_dawntm_complete_edition_7517962_(64bit)_(53037)-13.bin'
ARCHIVE_BASE_1_PART13_MD5='ea24163717d52a3b1df0673711088143'
ARCHIVE_BASE_1_PART14_NAME='setup_horizon_zero_dawntm_complete_edition_7517962_(64bit)_(53037)-14.bin'
ARCHIVE_BASE_1_PART14_MD5='550d3b76dfa565f632b0eddea24bbcf3'
ARCHIVE_BASE_1_PART15_NAME='setup_horizon_zero_dawntm_complete_edition_7517962_(64bit)_(53037)-15.bin'
ARCHIVE_BASE_1_PART15_MD5='581a66858d19636df0e073929c778f0a'
ARCHIVE_BASE_1_PART16_NAME='setup_horizon_zero_dawntm_complete_edition_7517962_(64bit)_(53037)-16.bin'
ARCHIVE_BASE_1_PART16_MD5='057b513a63935e79e20b6fad76d1e05d'
ARCHIVE_BASE_1_PART17_NAME='setup_horizon_zero_dawntm_complete_edition_7517962_(64bit)_(53037)-17.bin'
ARCHIVE_BASE_1_PART17_MD5='67b713b3f6482b43a493f724c3d99e71'
ARCHIVE_BASE_1_PART18_NAME='setup_horizon_zero_dawntm_complete_edition_7517962_(64bit)_(53037)-18.bin'
ARCHIVE_BASE_1_PART18_MD5='91eedb3db10cdaaa63f930891a3780f2'
ARCHIVE_BASE_1_SIZE='76000000'
ARCHIVE_BASE_1_VERSION='1.11.7517962-gog53037'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/horizon_zero_dawn_complete_edition'

ARCHIVE_BASE_0_NAME='setup_horizon_zero_dawntm_complete_edition_6278995_(64bit)_(44600).exe'
ARCHIVE_BASE_0_MD5='f14a4c244306f169896e168f99d7a719'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_horizon_zero_dawntm_complete_edition_6278995_(64bit)_(44600)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='26260f87628d433912c4155481e7aa48'
ARCHIVE_BASE_0_PART2_NAME='setup_horizon_zero_dawntm_complete_edition_6278995_(64bit)_(44600)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='4dedb5420d45ff65ce0ddd857d2a6cad'
ARCHIVE_BASE_0_PART3_NAME='setup_horizon_zero_dawntm_complete_edition_6278995_(64bit)_(44600)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='79aee3ddc0ce308683a6715521b0daa2'
ARCHIVE_BASE_0_PART4_NAME='setup_horizon_zero_dawntm_complete_edition_6278995_(64bit)_(44600)-4.bin'
ARCHIVE_BASE_0_PART4_MD5='e2386e5e027c44e9f923fb027ad0f391'
ARCHIVE_BASE_0_PART5_NAME='setup_horizon_zero_dawntm_complete_edition_6278995_(64bit)_(44600)-5.bin'
ARCHIVE_BASE_0_PART5_MD5='032be65a01c84f934edfbfa326258e90'
ARCHIVE_BASE_0_PART6_NAME='setup_horizon_zero_dawntm_complete_edition_6278995_(64bit)_(44600)-6.bin'
ARCHIVE_BASE_0_PART6_MD5='3b42edf5efe308cf3c1db72bc1ef9021'
ARCHIVE_BASE_0_PART7_NAME='setup_horizon_zero_dawntm_complete_edition_6278995_(64bit)_(44600)-7.bin'
ARCHIVE_BASE_0_PART7_MD5='f6216943ef02d605752a1156b0559df3'
ARCHIVE_BASE_0_PART8_NAME='setup_horizon_zero_dawntm_complete_edition_6278995_(64bit)_(44600)-8.bin'
ARCHIVE_BASE_0_PART8_MD5='703d33ab31b1ce23587cae3f8eef1453'
ARCHIVE_BASE_0_PART9_NAME='setup_horizon_zero_dawntm_complete_edition_6278995_(64bit)_(44600)-9.bin'
ARCHIVE_BASE_0_PART9_MD5='27f01f0545e732d61a9ceb05495ab9ef'
ARCHIVE_BASE_0_PART10_NAME='setup_horizon_zero_dawntm_complete_edition_6278995_(64bit)_(44600)-10.bin'
ARCHIVE_BASE_0_PART10_MD5='25c10d76ad8399802cdec42230a129bf'
ARCHIVE_BASE_0_PART11_NAME='setup_horizon_zero_dawntm_complete_edition_6278995_(64bit)_(44600)-11.bin'
ARCHIVE_BASE_0_PART11_MD5='7daf2cf2f17e5bd5aa22707dc690fc4f'
ARCHIVE_BASE_0_PART12_NAME='setup_horizon_zero_dawntm_complete_edition_6278995_(64bit)_(44600)-12.bin'
ARCHIVE_BASE_0_PART12_MD5='5a23643c5a474808503cb30d057010f8'
ARCHIVE_BASE_0_PART13_NAME='setup_horizon_zero_dawntm_complete_edition_6278995_(64bit)_(44600)-13.bin'
ARCHIVE_BASE_0_PART13_MD5='98bab66a848465dab195fe2c292a5283'
ARCHIVE_BASE_0_PART14_NAME='setup_horizon_zero_dawntm_complete_edition_6278995_(64bit)_(44600)-14.bin'
ARCHIVE_BASE_0_PART14_MD5='e9817e15706c72ad447881898abe0379'
ARCHIVE_BASE_0_PART15_NAME='setup_horizon_zero_dawntm_complete_edition_6278995_(64bit)_(44600)-15.bin'
ARCHIVE_BASE_0_PART15_MD5='f456a7bfed43b135fb8682481e284cb1'
ARCHIVE_BASE_0_PART16_NAME='setup_horizon_zero_dawntm_complete_edition_6278995_(64bit)_(44600)-16.bin'
ARCHIVE_BASE_0_PART16_MD5='398b6c64e1087a6f3deab3104c10a153'
ARCHIVE_BASE_0_PART17_NAME='setup_horizon_zero_dawntm_complete_edition_6278995_(64bit)_(44600)-17.bin'
ARCHIVE_BASE_0_PART17_MD5='9d0616c36329c3346bad3bdf55ead984'
ARCHIVE_BASE_0_SIZE='71000000'
ARCHIVE_BASE_0_VERSION='1.10.6278995-gog44600'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
localcachedx12
tools
*.dll
*.exe'
CONTENT_GAME_L10N_EN_FILES='
packed_dx12/initial_english.bin
packed_dx12/remainder_english.bin
packed_dx12/dlc1_english.bin'
CONTENT_GAME_L10N_FR_FILES='
packed_dx12/initial_french.bin
packed_dx12/remainder_french.bin
packed_dx12/dlc1_french.bin'
CONTENT_GAME_MOVIES_FILES='
movies'
CONTENT_GAME_DATA_FILES='
packed_dx12/fgrwin32.bin
packed_dx12/patch.bin'
CONTENT_GAME_DATA_DLC1_FILES='
packed_dx12/dlc1.bin'

HUGE_FILES_DATA='
packed_dx12/initial.bin
packed_dx12/remainder.bin'

WINE_DIRECT3D_RENDERER='vkd3d'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/Horizon Zero Dawn'
WINE_WINETRICKS_VERBS='mfc140'

APP_MAIN_EXE='horizonzerodawn.exe'

PACKAGES_LIST='
PKG_BIN
PKG_L10N_EN
PKG_L10N_FR
PKG_MOVIES
PKG_DATA_DLC1
PKG_DATA'

PKG_L10N_ID="${GAME_ID}-l10n"
PKG_L10N_EN_ID="${PKG_L10N_ID}-en"
PKG_L10N_FR_ID="${PKG_L10N_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_ID"
PKG_L10N_EN_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_FR_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_EN_DESCRIPTION='English localization'
PKG_L10N_FR_DESCRIPTION='French localization'

PKG_MOVIES_ID="${GAME_ID}-movies"
PKG_MOVIES_DESCRIPTION='movies'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_DATA_DLC1_ID="${PKG_DATA_ID}-dlc1"
PKG_DATA_DLC1_DESCRIPTION="$PKG_DATA_DESCRIPTION - dlc1"
PKG_DATA_DEPS="$PKG_DATA_DEPS $PKG_DATA_DLC1_ID"

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_L10N_ID $PKG_MOVIES_ID $PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
case "$(messages_language)" in
	('fr')
		message='version %s :'
		lang_en='anglaise'
		lang_fr='française'
	;;
	('en'|*)
		message='%s version:'
		lang_en='English'
		lang_fr='French'
	;;
esac
printf '\n'
## English localization
printf "$message" "$lang_en"
## Silence a ShellCheck false positive, word splitting is expected here
## SC2046 (warning): Quote this to prevent word splitting.
# shellcheck disable=SC2046
print_instructions $(packages_list | sed 's/PKG_L10N_FR//')
## French localization
printf "$message" "$lang_fr"
## Silence a ShellCheck false positive, word splitting is expected here
## SC2046 (warning): Quote this to prevent word splitting.
# shellcheck disable=SC2046
print_instructions $(packages_list | sed 's/PKG_L10N_EN//')

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
