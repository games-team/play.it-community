#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Night Fisherman
# send your bug reports to contact@dotslashplay.it
###

script_version=20230221.1

GAME_ID='the-night-fisherman'
GAME_NAME='The Night Fisherman'

ARCHIVE_BASE_0='The Night Fisherman WIN.zip'
ARCHIVE_BASE_0_MD5='b7df1ae2b6665229870c2f8610ccd2c9'
ARCHIVE_BASE_0_SIZE='300000'
ARCHIVE_BASE_0_VERSION='1.0-itch1'
ARCHIVE_BASE_0_URL='https://farfewgiants.itch.io/night-fisherman'

UNITY3D_NAME='The Night Fisherman'

CONTENT_PATH_DEFAULT='The Night Fisherman WIN'
CONTENT_GAME_BIN_FILES="
MonoBleedingEdge
UnityCrashHandler64.exe
UnityPlayer.dll
${UNITY3D_NAME}.exe
${UNITY3D_NAME}_Data/Plugins
${UNITY3D_NAME}_Data/Managed"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data"

APP_MAIN_TYPE='wine'
APP_MAIN_EXE="${UNITY3D_NAME}.exe"

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

target_version='2.21'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
