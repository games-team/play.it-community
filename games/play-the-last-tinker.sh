#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Last Tinker
# send your bug reports to contact@dotslashplay.it
###

script_version=20241130.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='the-last-tinker'
GAME_NAME='The Last Tinker: City of Colors'

ARCHIVE_BASE_LINUX_0_NAME='last_tinker_city_of_colors_the_en_23_02_2018_18902.sh'
ARCHIVE_BASE_LINUX_0_MD5='d0dbfac723aee309869c2404d88b4eb4'
ARCHIVE_BASE_LINUX_0_SIZE='2100000'
ARCHIVE_BASE_LINUX_0_VERSION='2018.02.23-gog18902'
ARCHIVE_BASE_LINUX_0_URL='https://www.gog.com/game/the_last_tinker_city_of_colors'

ARCHIVE_BASE_WINDOWS_1_NAME='setup_the_last_tinker_-_city_of_colors_23.02.2018_(18831).exe'
ARCHIVE_BASE_WINDOWS_1_MD5='ec303722fba022e2b1d04f69091213d9'
ARCHIVE_BASE_WINDOWS_1_TYPE='innosetup'
ARCHIVE_BASE_WINDOWS_1_PART1_NAME='setup_the_last_tinker_-_city_of_colors_23.02.2018_(18831)-1.bin'
ARCHIVE_BASE_WINDOWS_1_PART1_MD5='91e843b4d7be0d842bf3dc1f9930f11d'
ARCHIVE_BASE_WINDOWS_1_SIZE='2000000'
ARCHIVE_BASE_WINDOWS_1_VERSION='2018.02.23-gog18831'
ARCHIVE_BASE_WINDOWS_1_URL='https://www.gog.com/game/the_last_tinker_city_of_colors'

ARCHIVE_BASE_WINDOWS_0_NAME='setup_the_last_tinker_2.0.0.2.exe'
ARCHIVE_BASE_WINDOWS_0_MD5='7afa966efb4beb5535e19f2d69b245ae'
ARCHIVE_BASE_WINDOWS_0_TYPE='innosetup'
ARCHIVE_BASE_WINDOWS_0_SIZE='2100000'
ARCHIVE_BASE_WINDOWS_0_VERSION='1.0-gog2.0.0.2'

UNITY3D_NAME='the last tinker'
UNITY3D_PLUGINS='
libRenderingPlugin.so
ScreenSelector.so'

CONTENT_PATH_DEFAULT_WINDOWS='app'
CONTENT_PATH_DEFAULT_LINUX='data/noarch/game'
## TODO: Check if the Steam libraries can be dropped
CONTENT_LIBS_BIN_FILES='
libsteam_api.so
libSteamworksNative.so'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ID="$GAME_ID"
PKG_BIN_ID_LINUX="${PKG_BIN_ID}-linux"
PKG_BIN_ID_WINDOWS="${PKG_BIN_ID}-windows"
PKG_BIN_PROVIDES="
$PKG_BIN_ID"
PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES_LINUX='
libatk-1.0.so.0
libcairo.so.2
libc.so.6
libdl.so.2
libfontconfig.so.1
libfreetype.so.6
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libgdk-x11-2.0.so.0
libgio-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libGLU.so.1
libgmodule-2.0.so.0
libgobject-2.0.so.0
libgthread-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpango-1.0.so.0
libpangocairo-1.0.so.0
libpangoft2-1.0.so.0
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXext.so.6'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_LINUX="${PKG_DATA_ID}-linux"
PKG_DATA_ID_WINDOWS="${PKG_DATA_ID}-windows"
PKG_DATA_PROVIDES="
$PKG_DATA_ID"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
