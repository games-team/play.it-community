#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# My Memory of Us
# send your bug reports to contact@dotslashplay.it
###

script_version=20240812.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='my-memory-of-us'
GAME_NAME='My Memory of Us'

ARCHIVE_BASE_0_NAME='setup_my_memory_of_us_1.13057.1_(64bit)_(25548).exe'
ARCHIVE_BASE_0_MD5='44d0900cd3677e8811eea558ed237c56'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_my_memory_of_us_1.13057.1_(64bit)_(25548)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='a2e2e199681aaa136cc109df60b6eefc'
ARCHIVE_BASE_0_SIZE='5000000'
ARCHIVE_BASE_0_VERSION='1.13057.1-gog25548'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/my_memory_of_us'

UNITY3D_NAME='mmou'

CONTENT_PATH_DEFAULT='.'

WINE_REGEDIT_PERSISTENT_KEYS='
HKEY_CURRENT_USER\Software\Juggler Games\My Memory of Us'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Juggler Games/My Memory of Us'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
video/quicktime, variant=(string)iso'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
