#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Pid
# send your bug reports to contact@dotslashplay.it
###

script_version=20240728.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='pid'
GAME_NAME='Pid'

ARCHIVE_BASE_1_NAME='setup_pid_gog-1_(18421).exe'
ARCHIVE_BASE_1_MD5='d6cd6899df3b2ad13071f58d0362ab3a'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='1300000'
ARCHIVE_BASE_1_VERSION='1.0-gog18421'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/pid'

ARCHIVE_BASE_0=_NAME'setup_pid_2.2.0.8.exe'
ARCHIVE_BASE_0_MD5='02ebb3db7572b1e6c28dce439a76df9a'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='1300000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.2.0.8'

UNITY3D_NAME='pid'

CONTENT_PATH_DEFAULT='app'

USER_PERSISTENT_DIRECTORIES='
saves'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
