#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Bio Menace
# send your bug reports to contact@dotslashplay.it
###

script_version=20240422.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='bio-menace'
GAME_NAME='Bio Menace'

ARCHIVE_BASE_0_NAME='gog_bio_menace_2.0.0.2.sh'
ARCHIVE_BASE_0_MD5='75167ee3594dd44ec8535b29b90fe4eb'
ARCHIVE_BASE_0_SIZE='14000'
ARCHIVE_BASE_0_VERSION='1.1-gog2.0.0.2'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/bio_menace'

CONTENT_PATH_DEFAULT='data/noarch/data'
CONTENT_GAME_MAIN_FILES='
*.bm?
*.conf
*.exe
biopatch.zip'
CONTENT_DOC_MAIN_FILES='
*.txt'
CONTENT_DOC0_MAIN_PATH='data/noarch/docs'
CONTENT_DOC0_MAIN_FILES='
*.pdf
*.txt'

USER_PERSISTENT_FILES='
*.conf
config.bm?
SAVEGAM*'

APP_1_ID="${GAME_ID}-1"
APP_1_NAME="$GAME_NAME - Episode 1: Dr. Mangle’s Lab"
APP_1_EXE='bmenace1.exe'
APP_1_ICON='../support/icon.png'

APP_2_ID="${GAME_ID}-2"
APP_2_NAME="$GAME_NAME - Episode 2: The Hidden Lab"
APP_2_EXE='bmenace2.exe'
APP_2_ICON='../support/icon.png'

APP_3_ID="${GAME_ID}-3"
APP_3_NAME="$GAME_NAME - Episode 3: Master Cain"
APP_3_EXE='bmenace3.exe'
APP_3_ICON='../support/icon.png'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Convert all file paths to lowercase.
	tolower .
)

# Include game data

# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
