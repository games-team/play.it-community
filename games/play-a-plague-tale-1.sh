#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# A Plague Tale: Innocence
# send your bug reports to contact@dotslashplay.it
###

script_version=20240812.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='a-plague-tale-1'
GAME_NAME='A Plague Tale: Innocence'

ARCHIVE_BASE_0_NAME='setup_a_plague_tale_innocence_1.07_(64bit)_(34188).exe'
ARCHIVE_BASE_0_MD5='002096bcb7eac4d9aa111b692aab6f07'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_a_plague_tale_innocence_1.07_(64bit)_(34188)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='7a16ee379e97a11a56a60bb77f645c50'
ARCHIVE_BASE_0_PART2_NAME='setup_a_plague_tale_innocence_1.07_(64bit)_(34188)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='c7f7e9b44ecd895ef80cea83aba52ae7'
ARCHIVE_BASE_0_PART3_NAME='setup_a_plague_tale_innocence_1.07_(64bit)_(34188)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='dd99c6c9beb00ef3357622c1782d6406'
ARCHIVE_BASE_0_PART4_NAME='setup_a_plague_tale_innocence_1.07_(64bit)_(34188)-4.bin'
ARCHIVE_BASE_0_PART4_MD5='03b15312c67fe6e5c65b8889056632c1'
ARCHIVE_BASE_0_PART5_NAME='setup_a_plague_tale_innocence_1.07_(64bit)_(34188)-5.bin'
ARCHIVE_BASE_0_PART5_MD5='a8b63b353fa0b59a10895daad970058c'
ARCHIVE_BASE_0_PART6_NAME='setup_a_plague_tale_innocence_1.07_(64bit)_(34188)-6.bin'
ARCHIVE_BASE_0_PART6_MD5='b6f49327c40dda4d25dd78ea2ea47317'
ARCHIVE_BASE_0_PART7_NAME='setup_a_plague_tale_innocence_1.07_(64bit)_(34188)-7.bin'
ARCHIVE_BASE_0_PART7_MD5='d5288ea2b0992bb0f440e8a4dfbe6a0f'
ARCHIVE_BASE_0_PART8_NAME='setup_a_plague_tale_innocence_1.07_(64bit)_(34188)-8.bin'
ARCHIVE_BASE_0_PART8_MD5='e426750fae1e573b1acfbd5301b8a90d'
ARCHIVE_BASE_0_PART9_NAME='setup_a_plague_tale_innocence_1.07_(64bit)_(34188)-9.bin'
ARCHIVE_BASE_0_PART9_MD5='a0612cdb2a6d619588c8ded2622751f5'
ARCHIVE_BASE_0_SIZE='44000000'
ARCHIVE_BASE_0_VERSION='1.07-gog34188'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/a_plague_tale_innocence'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
amd_ags_x64.dll
anselsdk64.dll
binkw64.dll
common64.dll
galaxy64.dll
gfsdk_aftermath_lib.x64.dll
sdkencryptedappticket64.dll
wwiselibpcx64r.dll
zlibd.dll
zlib.dll
aplaguetaleinnocence_x64.exe'
CONTENT_GAME_DATA_LEVELS1_FILES='
levels/battlefield
levels/battlefield2
levels/cathedral
levels/corrupted_domain'
CONTENT_GAME_DATA_LEVELS2_FILES='
levels/domain
levels/epilogue
levels/farm
levels/illusion'
CONTENT_GAME_DATA_LEVELS3_FILES='
levels/inquisition
levels/menu
levels/shelter_forest
levels/shelter_morning
levels/shelter_safe
levels/shelter_siege'
CONTENT_GAME_DATA_LEVELS4_FILES='
levels/university
levels/university2
levels/village
levels/village2'
CONTENT_GAME_DATA_FILES='
datas
font
input
rtc
shaders
soundbanks
trtext
videos
all.psc
initfont.tsc
langdef.tsc'

WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/My Games/A Plague Tale Innocence'

APP_MAIN_EXE='aplaguetaleinnocence_x64.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA_LEVELS1
PKG_DATA_LEVELS2
PKG_DATA_LEVELS3
PKG_DATA_LEVELS4
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPENDENCIES_SIBLINGS='
PKG_DATA_LEVELS1
PKG_DATA_LEVELS2
PKG_DATA_LEVELS3
PKG_DATA_LEVELS4'

PKG_DATA_LEVELS_ID="${PKG_DATA_ID}-levels"
PKG_DATA_LEVELS1_ID="${PKG_DATA_LEVELS_ID}-1"
PKG_DATA_LEVELS2_ID="${PKG_DATA_LEVELS_ID}-2"
PKG_DATA_LEVELS3_ID="${PKG_DATA_LEVELS_ID}-3"
PKG_DATA_LEVELS4_ID="${PKG_DATA_LEVELS_ID}-4"
PKG_DATA_LEVELS_DESCRIPTION="$PKG_DATA_DESCRIPTION - levels"
PKG_DATA_LEVELS1_DESCRIPTION="$PKG_DATA_LEVELS_DESCRIPTION - 1"
PKG_DATA_LEVELS2_DESCRIPTION="$PKG_DATA_LEVELS_DESCRIPTION - 2"
PKG_DATA_LEVELS3_DESCRIPTION="$PKG_DATA_LEVELS_DESCRIPTION - 4"
PKG_DATA_LEVELS4_DESCRIPTION="$PKG_DATA_LEVELS_DESCRIPTION - 5"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
