#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Duke Nukem 3D
# send your bug reports to contact@dotslashplay.it
###

script_version=20230418.1

GAME_ID='duke-nukem-3d'
GAME_NAME='Duke Nukem 3D'

# This game installer is no longer available for sale from gog.com
ARCHIVE_BASE_0='gog_duke_nukem_3d_atomic_edition_2.0.0.9.sh'
ARCHIVE_BASE_0_MD5='a51283d3bbc2db62b75c6c62824f5e46'
ARCHIVE_BASE_0_SIZE='63000'
ARCHIVE_BASE_0_VERSION='1.5-gog2.0.0.9'

CONTENT_PATH_DEFAULT='data/noarch/data'
CONTENT_GAME_MAIN_FILES='
*.BAT
*.CFG
*.CON
*.DAT
*.DMO
*.DOC
*.EXE
*.GIF
*.GRP
*.H
*.HLP
*.INI
*.MAP
*.PCK
*.RTS
*.TXT'
CONTENT_DOC_MAIN_FILES='
license.txt'

APP_MAIN_EXE='DUKE3D.EXE'
APP_MAIN_ICON='duke3d.ico'

USER_PERSISTENT_FILES='
DUKE3D.GRP
*.CFG
*.SAV'

# Enforce required audio setting

APP_MAIN_PRERUN="$APP_MAIN_PRERUN"'
# Enforce required audio setting
export DOSBOX_SBLASTER_IRQ=5'

# Load common functions

target_version='2.24'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

# shellcheck disable=SC2119
launchers_write

# Build package

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
