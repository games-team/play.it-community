#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Softporn Adventure
# send your bug reports to contact@dotslashplay.it
###

script_version=20240621.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='softporn-adventure'
GAME_NAME='Softporn Adventure'

## This installer is no longer distributed by gog.com.
ARCHIVE_BASE_0_NAME='gog_softporn_adventure_2.0.0.1.sh'
ARCHIVE_BASE_0_MD5='1e51094f928757140c5a0dacc70773c0'
ARCHIVE_BASE_0_SIZE='11000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.0.0.1'

CONTENT_PATH_DEFAULT='data/noarch/data'
CONTENT_GAME_MAIN_FILES='
FILE_ID.DIZ
SOFTPORN.BIN
SOFTPORN.EXE
SOFTPORN.LOC
SOFTPORN.TXT'
CONTENT_DOC_MAIN_PATH='data/noarch/docs'
CONTENT_DOC_MAIN_FILES='
SOFTPORN.DOC'

USER_PERSISTENT_FILES='
*.GAM'

APP_MAIN_EXE='SOFTPORN.EXE'
APP_MAIN_ICON='../support/icon.png'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
