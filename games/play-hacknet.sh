#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 VA
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Hacknet
# send your bug reports to contact@dotslashplay.it
###

script_version=20240603.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='hacknet'
GAME_NAME='Hacknet'

ARCHIVE_BASE_0_NAME='hacknet_en_5_069_15083.sh'
ARCHIVE_BASE_0_MD5='305d230cad47d696e4141320189cd4bc'
ARCHIVE_BASE_0_SIZE='350000'
ARCHIVE_BASE_0_VERSION='5.069-gog15083'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/hacknet'

CONTENT_PATH_DEFAULT='data/noarch/game'
## TODO: Check if some shipped libraries could be dropped in favour of system ones.
CONTENT_LIBS_FILES='
libcef.so
libjpeg.so.62
libmojoshader.so
libmono-2.0.so.1
libogg.so.0
libopenal.so.1
libpng15.so.15
libtheoradec.so.1
libtheoraplay.so
libvorbisfile.so.3
libvorbis.so.0
libXNAWebRenderer.so'
## TODO: Check if the Steam libraries are actually required.
CONTENT_LIBS_FILES="${CONTENT_LIBS_FILES:-}"'
libCSteamworks.so
libsteam_api.so'
CONTENT_LIBS_BIN64_PATH="${CONTENT_PATH_DEFAULT}/lib64"
CONTENT_LIBS_BIN64_FILES="$CONTENT_LIBS_FILES"
CONTENT_LIBS_BIN32_PATH="${CONTENT_PATH_DEFAULT}/lib"
CONTENT_LIBS_BIN32_FILES="$CONTENT_LIBS_FILES"
CONTENT_GAME_BIN64_FILES='
cefprocess.bin.x86_64
Hacknet.bin.x86_64'
CONTENT_GAME_BIN32_FILES='
cefprocess.bin.x86
Hacknet.bin.x86'
CONTENT_GAME_DATA_FILES='
Content
locales
Extensions
mono
Hacknet.exe
Hacknet.bmp
icudtl.dat
natives_blob.bin
snapshot_blob.bin
FNA.dll.config
*.dll
*.pak'

## Using the system-provided Mono runtime instead of the shipped binaries leads to a crash on launch:
##
## [0603/211754:FATAL:content_main_runner.cc(711)] Check failed: base::i18n::InitializeICU().
## #0 0x7fe45fadf56e <unknown>
## #1 0x7fe45faf4d6b <unknown>
## #2 0x7fe4626eb398 <unknown>
## #3 0x7fe45f9f14df <unknown>
## #4 0x7fe45f9f133b <unknown>
## #5 0x7fe45f99efe8 cef_initialize
## #6 0x7fe48c42ec33 CefInitialize()
## #7 0x7fe48c42924f XNAWR_Initialize
## #8 0x000041323cfe <unknown>
APP_MAIN_EXE_BIN32='Hacknet.bin.x86'
APP_MAIN_EXE_BIN64='Hacknet.bin.x86_64'
APP_MAIN_ICON='Hacknet.bmp'

USER_PERSISTENT_DIRECTORIES='
Content/People'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libasound.so.2
libatk-1.0.so.0
libcairo.so.2
libc.so.6
libcups.so.2
libdbus-1.so.3
libdl.so.2
libexpat.so.1
libfontconfig.so.1
libfreetype.so.6
libgcc_s.so.1
libgconf-2.so.4
libglib-2.0.so.0
libgmodule-2.0.so.0
libgobject-2.0.so.0
libm.so.6
libnspr4.so
libnss3.so
libnssutil3.so
libpango-1.0.so.0
libpangocairo-1.0.so.0
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libSDL2_image-2.0.so.0
libsmime3.so
libstdc++.so.6
libX11.so.6
libXcomposite.so.1
libXcursor.so.1
libXdamage.so.1
libXext.so.6
libXfixes.so.3
libXi.so.6
libXrandr.so.2
libXrender.so.1
libXss.so.1
libXtst.so.6
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Ensure that Chromium Embedded Framework is functional.
	chmod 755 \
		'cefprocess.bin.x86_64' \
		'cefprocess.bin.x86'
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

## Apply common Mono tweaks
APP_MAIN_PRERUN="$(application_prerun 'APP_MAIN')
$(mono_launcher_tweaks)"

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
