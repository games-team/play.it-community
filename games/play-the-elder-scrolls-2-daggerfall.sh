#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Elder Scrolls 2: Daggerfall
# send your bug reports to contact@dotslashplay.it
###

script_version=20240604.2

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='the-elder-scrolls-2-daggerfall'
GAME_NAME='The Elder Scrolls II: Daggerfall'

ARCHIVE_BASE_1_NAME='setup_the_elder_scrolls_ii_daggerfall_1.07_(28043).exe'
ARCHIVE_BASE_1_MD5='94acfb7acfe2242241d4355ada481d98'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='560000'
ARCHIVE_BASE_1_VERSION='1.07.213-gog28043'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/the_elder_scrolls_iii_morrowind_goty_edition'

ARCHIVE_BASE_0='setup_tes_daggerfall_2.0.0.4.exe'
ARCHIVE_BASE_0_MD5='68f1eb4f257d8da4c4eab2104770c49b'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='580000'
ARCHIVE_BASE_0_VERSION='1.07.213-gog2.0.0.4'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_MAIN_FILES='
arena2
data
dagger.ico
dagger.exe
fall.exe
fixsave.exe
setup.exe
fixsave.txt
patched.txt
test.*
*.386
*.bnk
*.cfg
*.ini
*.scr'
CONTENT_DOC_MAIN_FILES='
*.pdf'

USER_PERSISTENT_DIRECTORIES='
pics
save0
save1
save2
save3
save4
save5'
USER_PERSISTENT_FILES='
*.cfg
arena2/bio.dat
arena2/copyfile.dat
arena2/rumor.dat
arena2/mapsave.sav
arena2/*.DAT
arena2/*.AMF'

APP_MAIN_EXE='fall.exe'
APP_MAIN_OPTIONS='z.cfg'
APP_MAIN_ICON='dagger.ico'

## Easier upgrade from packages generated with pre-20190302.3 scripts.
PKG_MAIN_PROVIDES="${PKG_MAIN_PROVIDES:-}
the-elder-scrolls-2-daggerfall-data"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
