#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Assassin's Creed
# send your bug reports to contact@dotslashplay.it
###

script_version=20250228.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='assassins-creed-1'
GAME_NAME='Assassinʼs Creed'

ARCHIVE_BASE_EN_0_NAME='setup_assassins_creed_1.02_v2_(28524).exe'
ARCHIVE_BASE_EN_0_MD5='b14aa9508ce9653597558a6d834e2766'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_PART1_NAME='setup_assassins_creed_1.02_v2_(28524)-1.bin'
ARCHIVE_BASE_EN_0_PART1_MD5='08f2ac5b1c558483ea27c921a7d7aad7'
ARCHIVE_BASE_EN_0_PART2_NAME='setup_assassins_creed_1.02_v2_(28524)-2.bin'
ARCHIVE_BASE_EN_0_PART2_MD5='150870977feb60c9f344e35d220e1198'
ARCHIVE_BASE_EN_0_SIZE='7200000'
ARCHIVE_BASE_EN_0_VERSION='1.02-gog28524'
ARCHIVE_BASE_EN_0_URL='https://www.gog.com/game/assassins_creed_directors_cut'

ARCHIVE_BASE_FR_0_NAME='setup_assassins_creed_1.02_v2_(french)_(28524).exe'
ARCHIVE_BASE_FR_0_MD5='eb346d8ec12bb055f941446d24207dbd'
ARCHIVE_BASE_FR_0_TYPE='innosetup'
ARCHIVE_BASE_FR_0_PART1_NAME='setup_assassins_creed_1.02_v2_(french)_(28524)-1.bin'
ARCHIVE_BASE_FR_0_PART1_MD5='08f2ac5b1c558483ea27c921a7d7aad7'
ARCHIVE_BASE_FR_0_PART2_NAME='setup_assassins_creed_1.02_v2_(french)_(28524)-2.bin'
ARCHIVE_BASE_FR_0_PART2_MD5='2e31309a834daa7c7640a4848e701574'
ARCHIVE_BASE_FR_0_SIZE='7200000'
ARCHIVE_BASE_FR_0_VERSION='1.02-gog28524'
ARCHIVE_BASE_FR_0_URL='https://www.gog.com/game/assassins_creed_directors_cut'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
assassinscreed_dx10.exe
assassinscreed_dx9.exe
assassinscreed_game.exe
binkw32.dll
eax.dll'
CONTENT_GAME_L10N_FILES='
datapc_streamedsounds???.forge'
CONTENT_GAME_DATA_FILES='
*.forge
defaultbindings.map
resources
videos'
CONTENT_DOC_L10N_FILES_EN='
manual
eula/english*
readme/english*'
CONTENT_DOC_L10N_FILES_FR='
manual
eula/french*
readme/french*'

WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES="
users/\${USER}/AppData/Roaming/Ubisoft/Assassin's Creed"

APP_MAIN_EXE='assassinscreed_dx9.exe'
APP_MAIN_ICON='assassinscreed_game.exe'

PACKAGES_LIST='
PKG_BIN
PKG_L10N
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_L10N_BASE
PKG_DATA'

PKG_L10N_BASE_ID="${GAME_ID}-l10n"
PKG_L10N_ID_EN="${PKG_L10N_BASE_ID}-en"
PKG_L10N_ID_FR="${PKG_L10N_BASE_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_BASE_ID"
PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_FR='French localization'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Ensure ability fo fully control the camera with the mouse
# cf. https://appdb.winehq.org/objectManager.php?sClass=version&iId=28057#notes

registry_dump_mouse_grab_file='registry-dumps/mouse-grab.reg'
registry_dump_mouse_grab_content='Windows Registry Editor Version 5.00

[HKEY_CURRENT_USER\Software\Wine\X11 Driver]
"GrabFullscreen"="Y"'
CONTENT_GAME_BIN_FILES="${CONTENT_GAME_BIN_FILES:-}
$registry_dump_mouse_grab_file"
APP_REGEDIT="${APP_REGEDIT:-} $registry_dump_mouse_grab_file"
REQUIREMENTS_LIST="${REQUIREMENTS_LIST:-}
iconv"

# Set game text language

registry_dump_language_file='registry-dumps/mouse-grab.reg'
registry_dump_language_content_EN='Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\Software\Ubisoft\Assassin'\''s Creed]
"Language"="English"'
registry_dump_language_content_FR='Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\Software\Ubisoft\Assassin'\''s Creed]
"Language"="French"'
CONTENT_L10N_DATA_FILES="${CONTENT_L10N_DATA_FILES:-}
$registry_dump_language_file"
APP_REGEDIT="${APP_REGEDIT:-} $registry_dump_language_file"
REQUIREMENTS_LIST="${REQUIREMENTS_LIST:-}
iconv"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Ensure ability fo fully control the camera with the mouse
	mkdir --parents "$(dirname "$registry_dump_mouse_grab_file")"
	printf '%s' "$registry_dump_mouse_grab_content" |
		iconv --from-code=UTF-8 --to-code=UTF-16 --output="$registry_dump_mouse_grab_file"

	# Set game text language
	registry_dump_language_content=$(context_value 'registry_dump_language_content')
	mkdir --parents "$(dirname "$registry_dump_language_file")"
	printf '%s' "$registry_dump_language_content" |
		iconv --from-code=UTF-8 --to-code=UTF-16 --output="$registry_dump_language_file"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
