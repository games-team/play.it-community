#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Jacek Szafarkiewicz
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Dungeons 2 expansions:
# - A Chance of Dragons
# - A Song of Sand and Fire
# send your bug reports to contact@dotslashplay.it
###

script_version=20240615.2

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='dungeons-2'
GAME_NAME='Dungeons 2'

EXPANSION_ID_DRAGONS='a-chance-of-dragons'
EXPANSION_NAME_DRAGONS='A Chance of Dragons'

EXPANSION_ID_SONG='a-song-of-sand-of-fire'
EXPANSION_NAME_SONG='A Song of Sand and Fire'

ARCHIVE_BASE_DRAGONS_0_NAME='gog_dungeons_2_a_chance_of_dragons_2.0.0.1.sh'
ARCHIVE_BASE_DRAGONS_0_MD5='d6fcaa7bc9051c8c67301a4af58a7632'
ARCHIVE_BASE_DRAGONS_0_SIZE='10000'
ARCHIVE_BASE_DRAGONS_0_VERSION='1.6.1.29-gog2.0.0.1'
ARCHIVE_BASE_DRAGONS_0_URL='https://www.gog.com/game/dungeons_2_a_chance_of_dragons'

ARCHIVE_BASE_SONG_0_NAME='gog_dungeons_2_a_song_of_sand_and_fire_2.0.0.1.sh'
ARCHIVE_BASE_SONG_0_MD5='70e6beffef5e2197280fe7c9d8052ea5'
ARCHIVE_BASE_SONG_0_SIZE='10000'
ARCHIVE_BASE_SONG_0_VERSION='1.6.1.29-gog2.0.0.1'
ARCHIVE_BASE_SONG_0_URL='https://www.gog.com/game/dungeons_2_a_song_of_sand_and_fire'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES='
Dungeons2_Data'

PKG_MAIN_DEPS="$GAME_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
