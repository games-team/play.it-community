#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Difference Between Us
# send your bug reports to contact@dotslashplay.it
###

script_version=20240428.3

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='the-difference-between-us'
GAME_NAME='The Difference Between Us'

ARCHIVE_BASE_0_NAME='The-Difference-Between-Us-linux.tar.bz2'
ARCHIVE_BASE_0_MD5='7888797ccda89452a2543ba35c46083c'
ARCHIVE_BASE_0_SIZE='64000'
ARCHIVE_BASE_0_VERSION='1.0-itch'
ARCHIVE_BASE_0_URL='https://robindaydream.itch.io/the-difference-between-us'

CONTENT_PATH_DEFAULT='The-Difference-Between-Us-linux'
CONTENT_LIBS_BIN_FILES='
libavcodec.so.57
libavformat.so.57
libavresample.so.3
libavutil.so.55
libGLEW.so.1.7
libpython2.7.so.1.0
libswresample.so.2
libswscale.so.4'
## libpng12.so.0 is not only a dependency of the shipped SDL2 build,
## it is actually required to prevent a crash on launch.
CONTENT_LIBS_BIN_FILES="${CONTENT_LIBS_BIN_FILES:-}
libpng12.so.0"
CONTENT_LIBS_BIN64_PATH="${CONTENT_PATH_DEFAULT}/lib/linux-x86_64"
CONTENT_LIBS_BIN64_FILES="$CONTENT_LIBS_BIN_FILES"
CONTENT_LIBS_BIN32_PATH="${CONTENT_PATH_DEFAULT}/lib/linux-i686"
CONTENT_LIBS_BIN32_FILES="$CONTENT_LIBS_BIN_FILES"
CONTENT_GAME_BIN64_FILES='
lib/linux-x86_64/eggs
lib/linux-x86_64/lib
lib/linux-x86_64/python
lib/linux-x86_64/pythonw
lib/linux-x86_64/zsync
lib/linux-x86_64/zsyncmake
lib/linux-x86_64/The-Difference-Between-Us'
CONTENT_GAME_BIN32_FILES='
lib/linux-i686/eggs
lib/linux-i686/lib
lib/linux-i686/python
lib/linux-i686/pythonw
lib/linux-i686/zsync
lib/linux-i686/zsyncmake
lib/linux-i686/The-Difference-Between-Us'
CONTENT_GAME_DATA_FILES='
game
renpy
lib/pythonlib2.7
The-Difference-Between-Us.py'
CONTENT_DOC_DATA_FILES='
README.html'

## Using system-provided renpy (8.2.1) triggers an early crash, with the following trace:
##
## Full traceback:
##   File "script.rpyc", line 197, in script
##   File "/usr/share/games/renpy/renpy/ast.py", line 615, in execute
##     renpy.exports.say(who, what, *args, **kwargs)
##   File "/usr/share/games/renpy/renpy/exports.py", line 1493, in say
##     who(what, *args, **kwargs)
##   File "/usr/share/games/renpy/renpy/character.py", line 1394, in __call__
##     self.do_display(who, what, cb_args=self.cb_args, dtt=dtt, **display_args)
##   File "/usr/share/games/renpy/renpy/character.py", line 1045, in do_display
##     display_say(who,
##   File "/usr/share/games/renpy/renpy/character.py", line 686, in display_say
##     what_text = renpy.display.screen.get_widget(what_text[0], what_text[1], what_text[2])
##   File "/usr/share/games/renpy/renpy/display/screen.py", line 1534, in get_displayable
##     screen.update()
##   File "/usr/share/games/renpy/renpy/display/screen.py", line 697, in update
##     self.screen.function(**self.scope)
##   File "game/screens.rpy", line 13, in execute
##   File "game/screens.rpy", line 13, in execute
##   File "game/screens.rpy", line 58, in execute
##   File "game/screens.rpy", line 592, in execute
##   File "game/screens.rpy", line 592, in execute
##   File "/usr/share/games/renpy/renpy/sl2/slast.py", line 2650, in execute
##     self.const_ast.keywords(context)
## AttributeError: 'NoneType' object has no attribute 'keywords'

APP_MAIN_EXE_BIN64='lib/linux-x86_64/The-Difference-Between-Us'
APP_MAIN_EXE_BIN32='lib/linux-i686/The-Difference-Between-Us'
APP_MAIN_OPTIONS='-EO The-Difference-Between-Us.py'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libGL.so.1
libGLU.so.1
libm.so.6
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libSDL2_image-2.0.so.0
libSDL2_ttf-2.0.so.0
libutil.so.1
libX11.so.6
libXext.so.6
libXi.so.6
libXmu.so.6
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Write launchers

## Use the default Ren'Py icon, instead of a generic icon.
desktop_field_icon() {
	printf 'renpy'
}
launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
