#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Heal
# send your bug reports to contact@dotslashplay.it
###

script_version=20240119.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='heal'
GAME_NAME='Heal'

ARCHIVE_BASE_0_NAME='setup_heal_1.0_(66845).exe'
ARCHIVE_BASE_0_MD5='7b30197a77652ff9fbbff9fe03bdbafa'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='241396'
ARCHIVE_BASE_0_VERSION='1.0-gog66845'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/heal'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_MAIN_FILES='
heal_gog.exe'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Roaming/healgog'

APP_MAIN_EXE='heal_gog.exe'

PKG_MAIN_ARCH='32'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
