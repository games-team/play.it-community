#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Bombshell
# send your bug reports to contact@dotslashplay.it
###

script_version=20240501.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='bombshell'
GAME_NAME='Bombshell'

ARCHIVE_BASE_0_NAME='setup_bombshell__1.2.10466(ion_furry)_(34762).exe'
ARCHIVE_BASE_0_MD5='05b5188629b378360154420be32296aa'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_bombshell__1.2.10466(ion_furry)_(34762)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='9a779d8df62ca33c5215ac7f3403f999'
ARCHIVE_BASE_0_PART2_NAME='setup_bombshell__1.2.10466(ion_furry)_(34762)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='186058e6d9058ed65ffaf4382ce8a1a0'
ARCHIVE_BASE_0_SIZE='9100000'
ARCHIVE_BASE_0_VERSION='1.2.10466-gog34762'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/bombshell'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN64_FILES='
binaries/win64'
CONTENT_GAME_BIN32_FILES='
binaries/win32'
CONTENT_GAME_DATA_FILES='
engine
shellgame'
CONTENT_DOC_DATA_PATH='help'
CONTENT_DOC_DATA_FILES='
gdf_fieldmanual.pdf'

WINE_PERSISTENT_DIRECTORIE='
users/${USER}/Documents/My Games/Bombshell/ShellGame'
WINE_WINEPREFIX_TWEAKS='mono'
## Grab the mouse cursor, to prevent issues when aiming with a mouse.
WINE_WINETRICKS_VERBS='grabfullscreen=y'

APP_MAIN_EXE_BIN64='binaries/win64/bombshell.exe'
APP_MAIN_EXE_BIN32='binaries/win32/bombshell.exe'
APP_MAIN_ICON='binaries/win32/bombshell.exe'
APP_MAIN_ICON_WRESTOOL_OPTIONS='--type=14 --name=117'
## The application type must be set explicitly,
## or it would be wrongly guessed as a Mono one.
APP_MAIN_TYPE='wine'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
