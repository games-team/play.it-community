#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# D: The Game
# send your bug reports to contact@dotslashplay.it
###

script_version=20240621.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='d-the-game'
GAME_NAME='D: The Game'

ARCHIVE_BASE_0_NAME='gog_d_the_game_2.0.0.2.sh'
ARCHIVE_BASE_0_MD5='4a9f18efc065a80723130b73c5327095'
ARCHIVE_BASE_0_SIZE='930000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.0.0.2'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/d_the_game'

CONTENT_PATH_DEFAULT='data/noarch/data'
CONTENT_GAME_MAIN_FILES='
AVI
BMP
DRV
EFC
VESA.AVI
SOUND.BAT
DS.DAT
LAURA.PIF
*.EXE'

GAME_IMAGE='DS.DAT'

USER_PERSISTENT_DIRECTORIES='
data'

APP_MAIN_EXE='LAURA.EXE'
APP_MAIN_ICON='../support/icon.png'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
