#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 VA
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Broforce
# send your bug reports to contact@dotslashplay.it
###

script_version=20231115.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='broforce'
GAME_NAME='Broforce'

ARCHIVE_BASE_0_NAME='broforce_1034_20190611_30229.sh'
ARCHIVE_BASE_0_MD5='8cbe5d69f77aaf36f99a74f39b9d200f'
ARCHIVE_BASE_0_SIZE='460000'
ARCHIVE_BASE_0_VERSION='1034.20190611-gog30229'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/broforce'

ARCHIVE_BASE_MULTIARCH_0_NAME='gog_broforce_2.3.0.4.sh'
ARCHIVE_BASE_MULTIARCH_0_MD5='1187889af4979b1718c5529ccfb4d741'
ARCHIVE_BASE_MULTIARCH_0_SIZE='1400000'
ARCHIVE_BASE_MULTIARCH_0_VERSION='5399v201606232041-gog15203'

UNITY3D_NAME='Broforce'
## TODO: Add an explicit list of plugins to include.

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME0_BIN_FILES="
${UNITY3D_NAME}_Data/Plugins"
CONTENT_GAME0_BIN64_FILES="
${UNITY3D_NAME}_Data/Plugins/x86_64"
CONTENT_GAME0_BIN32_FILES="
${UNITY3D_NAME}_Data/Plugins/x86"

PACKAGES_LIST='
PKG_BIN
PKG_DATA'
PACKAGES_LIST_MULTIARCH='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
## TODO: Complete the dependencies list.
##       cf. https://forge.dotslashplay.it/play.it/scripts/-/snippets/28
PKG_BIN_DEPENDENCIES_LIBRARIES='
libstdc++.so.6
libfreetype.so.6
libXrandr.so.2
libXcursor.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
## TODO: Check if the xrandr commmand is actually required.
PKG_BIN_DEPENDENCIES_COMMANDS='
xrandr'
PKG_BIN64_DEPENDENCIES_COMMANDS="$PKG_BIN_DEPENDENCIES_COMMANDS"
PKG_BIN32_DEPENDENCIES_COMMANDS="$PKG_BIN_DEPENDENCIES_COMMANDS"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

case "$(current_archive)" in
	('ARCHIVE_BASE_MULTIARCH_'*)
		set_current_package 'PKG_BIN32'
		# shellcheck disable=SC2119
		launchers_write
		set_current_package 'PKG_BIN64'
		# shellcheck disable=SC2119
		launchers_write
	;;
	(*)
		set_current_package 'PKG_BIN'
		# shellcheck disable=SC2119
		launchers_write
	;;
esac

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
