#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Ironclad Tactics
# send your bug reports to contact@dotslashplay.it
###

script_version=20240717.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='ironclad-tactics'
GAME_NAME='Ironclad Tactics'

ARCHIVE_BASE_0_NAME='gog_ironclad_tactics_deluxe_edition_2.0.0.4.sh'
ARCHIVE_BASE_0_MD5='a0a0e2195364e57bc39e180d442e1f6b'
ARCHIVE_BASE_0_SIZE='953272'
ARCHIVE_BASE_0_VERSION='1.0-gog2.0.0.4'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/ironclad_tactics_deluxe_edition'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN_FILES='
libRotor.so'
## libRotor.so is linked against libsteam_api.so,
## so we have to include it too.
CONTENT_LIBS0_BIN_FILES='
libsteam_api.so'
CONTENT_GAME_BIN_FILES='
mono'
CONTENT_GAME_DATA_FILES='
Comic
Fonts
Music
Sounds
Sprites
*.dll
*.dll.config
*.exe
*.exe.config
*.glsl'
CONTENT_DOC_DATA_FILES='
LICENSE.txt'

## The shipped mono build is used instead of the system-provided one
## to avoid a crash when trying to display the game introduction.
APP_MAIN_EXE='mono'
APP_MAIN_OPTIONS='Game.exe'
APP_MAIN_ICON='Game.exe'
APP_MAIN_PRERUN='
# Set environment for shipped mono binary
export MONO_PATH=.'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libSDL2_mixer-2.0.so.0
libstdc++.so.6'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

## Apply common Mono tweaks
APP_MAIN_PRERUN="$(application_prerun 'APP_MAIN')
$(mono_launcher_tweaks)"

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
