#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 BetaRays
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Tangledeep
# send your bug reports to contact@dotslashplay.it
###

script_version=20241218.2

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='tangledeep'
GAME_NAME='Tangledeep'

ARCHIVE_BASE_0_NAME='Tangledeep_153a_LinuxUniversal.zip'
ARCHIVE_BASE_0_MD5='b278678eeb55e55a73df389213afdeeb'
ARCHIVE_BASE_0_SIZE='1194135'
ARCHIVE_BASE_0_VERSION='1.53a-humble1'
ARCHIVE_BASE_0_URL='https://www.humblebundle.com/store/tangledeep'

ARCHIVE_BASE_MULTIARCH_1_NAME='Tangledeep_124k_LinuxUniversal.zip'
ARCHIVE_BASE_MULTIARCH_1_MD5='b708a12e20816dba8e863290dc5580d0'
ARCHIVE_BASE_MULTIARCH_1_SIZE='730000'
ARCHIVE_BASE_MULTIARCH_1_VERSION='1.24k-humble190410'

ARCHIVE_BASE_MULTIARCH_0_NAME='tangledeep_linux.zip'
ARCHIVE_BASE_MULTIARCH_0_MD5='ce38aaab0bf4838697fd1f76e30722f1'
ARCHIVE_BASE_MULTIARCH_0_SIZE='690000'
ARCHIVE_BASE_MULTIARCH_0_VERSION='1.23e-humble1'

UNITY3D_NAME='Tangledeep'
UNITY3D_PLUGINS_MULTIARCH='
ScreenSelector.so'

CONTENT_PATH_DEFAULT='linuxuniversal'
CONTENT_PATH_DEFAULT_MULTIARCH_0='tangledeep_123e_linux'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'
PACKAGES_LIST_MULTIARCH='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libz.so.1'
PKG_BIN_DEPENDENCIES_LIBRARIES_MULTIARCH='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES_MULTIARCH="$PKG_BIN_DEPENDENCIES_LIBRARIES_MULTIARCH"
PKG_BIN32_DEPENDENCIES_LIBRARIES_MULTIARCH="$PKG_BIN_DEPENDENCIES_LIBRARIES_MULTIARCH"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Prevent the inclusion of Steam libraries
	case "$(current_archive)" in
		('ARCHIVE_BASE_MULTIARCH_'*)
			# These archives already have an explicit list of plugins to include.
		;;
		(*)
			rm --recursive "$(unity3d_name)_Data/Plugins"
		;;
	esac
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

case "$(current_archive)" in
	('ARCHIVE_BASE_MULTIARCH_'*)
		launchers_generation 'PKG_BIN64'
		launchers_generation 'PKG_BIN32'
	;;
	(*)
		launchers_generation 'PKG_BIN'
	;;
esac

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
