#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# System Shock 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20240522.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='system-shock-2'
GAME_NAME='System Shock 2'

ARCHIVE_BASE_2_NAME='setup_system_shocktm_2_2.48_(31077).exe'
ARCHIVE_BASE_2_MD5='b8bbec00aef894db6641e46a0446cbb2'
ARCHIVE_BASE_2_TYPE='innosetup'
ARCHIVE_BASE_2_SIZE='660000'
ARCHIVE_BASE_2_VERSION='2.48-gog31077'
ARCHIVE_BASE_2_URL='https://www.gog.com/game/system_shock_2'

ARCHIVE_BASE_1_NAME='setup_system_shock_2_2.47_nd_(22087).exe'
ARCHIVE_BASE_1_MD5='cc2ff390b566364447dc5bd05757fe57'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='670000'
ARCHIVE_BASE_1_VERSION='2.47-gog22087'

ARCHIVE_BASE_0_NAME='setup_system_shock_2_2.46_update_3_(19935).exe'
ARCHIVE_BASE_0_MD5='cdafcdea01556eccab899f94503843df'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='670000'
ARCHIVE_BASE_0_VERSION='2.46.3-gog19935'

ARCHIVE_BASE_OLDPATH_2_NAME='setup_system_shock_2_2.46_update_2_(18733).exe'
ARCHIVE_BASE_OLDPATH_2_MD5='39fab64451ace95966988bb90c7bb17e'
ARCHIVE_BASE_OLDPATH_2_TYPE='innosetup'
ARCHIVE_BASE_OLDPATH_2_SIZE='680000'
ARCHIVE_BASE_OLDPATH_2_VERSION='2.46.2-gog18733'

ARCHIVE_BASE_OLDPATH_1_NAME='setup_system_shock_2_2.46_update_(18248).exe'
ARCHIVE_BASE_OLDPATH_1_MD5='b76803e4a632b58527eada8993999143'
ARCHIVE_BASE_OLDPATH_1_TYPE='innosetup'
ARCHIVE_BASE_OLDPATH_1_SIZE='690000'
ARCHIVE_BASE_OLDPATH_1_VERSION='2.46.1-gog18248'

ARCHIVE_BASE_OLDPATH_0_NAME='setup_system_shock_2_2.46_nd_(11004).exe'
ARCHIVE_BASE_OLDPATH_0_MD5='98c3d01d53bb2b0dc25d7ed7093a67d3'
ARCHIVE_BASE_OLDPATH_0_TYPE='innosetup'
ARCHIVE_BASE_OLDPATH_0_SIZE='680000'
ARCHIVE_BASE_OLDPATH_0_VERSION='2.46-gog11004'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_OLDPATH='app'
CONTENT_GAME_BIN_FILES='
*.ax
*.bnd
*.cfg
*.crt
*.dll
*.exe
*.osm
*/*.cfg
*/*.dll
*/*.exe'
CONTENT_GAME_DATA_FILES='
*.bin
*.dif
*.dml
ilist.*
patch*
binds
data
sq_scripts'
CONTENT_GAME0_BIN_PATH="${CONTENT_PATH_DEFAULT}/__support/app"
CONTENT_GAME0_BIN_PATH_OLDPATH="${CONTENT_PATH_DEFAULT_OLDPATH}/__support/app"
CONTENT_GAME0_BIN_FILES='
*.cfg
*.ini'
CONTENT_DOC_DATA_FILES='
*.pdf
*.txt
*.wri
doc
editor/*.txt'

USER_PERSISTENT_DIRECTORIES='
current
save_0
save_1
save_2
save_3
save_4
save_5
save_6
save_7
save_8
save_9
save_10
save_11
save_12
save_13
save_14'
USER_PERSISTENT_FILES='
*.log
*.bnd
*.cfg
*.ini'

WINE_VIRTUAL_DESKTOP='auto'

APP_MAIN_EXE='ss2.exe'
APP_MAIN_EXE_0='shock2.exe'
APP_MAIN_EXE_OLDPATH='shock2.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
