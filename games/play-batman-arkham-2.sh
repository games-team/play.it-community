#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Batman: Arkham 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20241124.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='batman-arkham-2'
GAME_NAME='Batman: Arkham City'

ARCHIVE_BASE_0_NAME='setup_batman_arkham_city_goty_1.1_(38264).exe'
ARCHIVE_BASE_0_MD5='e8bfd823cfcfec2147f736bb696a85c8'
## Do not convert the file paths to lowercase.
ARCHIVE_BASE_0_EXTRACTOR='innoextract'
ARCHIVE_BASE_0_EXTRACTOR_OPTIONS=' '
ARCHIVE_BASE_0_PART1_NAME='setup_batman_arkham_city_goty_1.1_(38264)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='5e1213208d59a42ec0fd808bfee2f189'
ARCHIVE_BASE_0_PART2_NAME='setup_batman_arkham_city_goty_1.1_(38264)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='fa9fcbaf8ef9de81dc1638bbc9db0353'
ARCHIVE_BASE_0_PART3_NAME='setup_batman_arkham_city_goty_1.1_(38264)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='685be8b8bbd0fe3f831fb5cf0506a735'
ARCHIVE_BASE_0_PART4_NAME='setup_batman_arkham_city_goty_1.1_(38264)-4.bin'
ARCHIVE_BASE_0_PART4_MD5='0ccafb2161130774710b470692ba8b39'
ARCHIVE_BASE_0_PART5_NAME='setup_batman_arkham_city_goty_1.1_(38264)-5.bin'
ARCHIVE_BASE_0_PART5_MD5='2122f11dc24618e8c902e6706c26f7c2'
ARCHIVE_BASE_0_SIZE='20000000'
ARCHIVE_BASE_0_VERSION='1.1-gog38264'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/batman_arkham_city_goty'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
Binaries
engine
Setup
BmGame/Config
*.dll'
CONTENT_GAME_L10N_EN_FILES='
BmGame/Localization/INT
BmGame/CookedPCConsole/English(US)'
CONTENT_GAME_L10N_FR_FILES='
BmGame/Localization/FRA
BmGame/CookedPCConsole/French(France)'
CONTENT_GAME_DATA_MOVIES_FILES='
BmGame/Movies
BmGame/MoviesStereo'
CONTENT_GAME_DATA_TEXTURES_FILES='
BmGame/CookedPCConsole/*.tfc'
CONTENT_GAME_DATA_FILES='
BmGame/CookedPCConsole/*.bin
BmGame/CookedPCConsole/*.upk
BmGame/CookedPCConsole/CookedBanks.txt
BmGame/CookedPCConsole/GuidMap
BmGame/CookedPCConsole/SFX
BmGame/Splash'
CONTENT_DOC_L10N_EN_FILES='
readme.rtf'
CONTENT_DOC_L10N_FR_FILES='
readme_FRA.rtf'

WINE_DIRECT3D_RENDERER='dxvk'
WINE_DLLOVERRIDES_DEFAULT='winemenubuilder.exe,mshtml='
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/WB Games/Batman Arkham City GOTY'
WINE_WINEPREFIX_TWEAKS='mono'
## Cursor is prevented to leave the game window, avoiding issues with mouse look.
WINE_WINETRICKS_VERBS='grabfullscreen=y'
## The game will throw an error on launch if PhysX is not installed.
WINE_WINETRICKS_VERBS="${WINE_WINETRICKS_VERBS:-} physx"

APP_MAIN_EXE='Binaries/Win32/BmLauncher.exe'
## Force the application type, or it will be mistaken for a Mono one.
APP_MAIN_TYPE='wine'
## Force language environment, to work around the lack of a game setting.
APP_MAIN_PRERUN='# Force language environment, to work around the lack of a game setting.
if [ -e "BmGame/Localization/FRA" ]; then
	export LANG=fr
elif [ -e "BmGame/Localization/INT" ]; then
	export LANG=en
fi
'
## Run the game binary from its parent directory.
APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}"'
# Run the game binary from its parent directory
cd "$(dirname "$APP_EXE")"
APP_EXE=$(basename "$APP_EXE")
'

PACKAGES_LIST='
PKG_BIN
PKG_L10N_EN
PKG_L10N_FR
PKG_DATA_MOVIES
PKG_DATA_TEXTURES
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_DATA_MOVIES_ID="${PKG_DATA_ID}-movies"
PKG_DATA_MOVIES_DESCRIPTION="$PKG_DATA_DESCRIPTION - movies"
PKG_DATA_DEPS="${PKG_DATA_DEPS:-} $PKG_DATA_MOVIES_ID"

PKG_DATA_TEXTURES_ID="${PKG_DATA_ID}-textures"
PKG_DATA_TEXTURES_DESCRIPTION="$PKG_DATA_DESCRIPTION - textures"
PKG_DATA_DEPS="${PKG_DATA_DEPS:-} $PKG_DATA_TEXTURES_ID"

PKG_L10N_ID="${GAME_ID}-l10n"
PKG_L10N_EN_ID="${PKG_L10N_ID}-en"
PKG_L10N_FR_ID="${PKG_L10N_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_ID"
PKG_L10N_EN_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_FR_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_EN_DESCRIPTION='English localization'
PKG_L10N_FR_DESCRIPTION='French localization'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_L10N_ID $PKG_DATA_ID"

# Set the game language based on the installed localization

SCRIPT_DEPS="${SCRIPT_DEPS:-} unix2dos"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Set the game language based on the installed localization
## TODO: Dump all this mess and redo it in a cleaner way during the content inclusion step.

config_file_launcher_source="$(package_path 'PKG_BIN')$(path_game_data)/BmGame/Config/Launcher.ini"
config_file_launcher_ini_section='\[Launcher\.Settings\]'
config_file_launcher_ini_field='ForceUseCulture'

config_file_launcher_en_destination="$(package_path 'PKG_L10N_EN')$(path_game_data)/BmGame/Config/Launcher.ini"
config_file_launcher_en_ini_value='en'
mkdir --parents "$(dirname "$config_file_launcher_en_destination")"
sed --expression="s/^${config_file_launcher_ini_section}/&\\n${config_file_launcher_ini_field}=${config_file_launcher_en_ini_value}/" \
	"$config_file_launcher_source" > "$config_file_launcher_en_destination"
unix2dos --quiet "$config_file_launcher_en_destination"

config_file_launcher_fr_destination="$(package_path 'PKG_L10N_FR')$(path_game_data)/BmGame/Config/Launcher.ini"
config_file_launcher_fr_ini_value='fr'
mkdir --parents "$(dirname "$config_file_launcher_fr_destination")"
sed --expression="s/^${config_file_launcher_ini_section}/&\\n${config_file_launcher_ini_field}=${config_file_launcher_fr_ini_value}/" \
	"$config_file_launcher_source" > "$config_file_launcher_fr_destination"
unix2dos --quiet "$config_file_launcher_fr_destination"

rm "$config_file_launcher_source"

config_file_engine_source="$(package_path 'PKG_BIN')$(path_game_data)/engine/config/BaseEngine.ini"
config_file_engine_ini_field='Language'

config_file_engine_en_destination="$(package_path 'PKG_L10N_EN')$(path_game_data)/engine/config/BaseEngine.ini"
config_file_engine_en_ini_value='INT'
mkdir --parents "$(dirname "$config_file_engine_en_destination")"
sed --expression="s/^${config_file_engine_ini_field}=.*/${config_file_engine_ini_field}=${config_file_engine_en_ini_value}/" \
	"$config_file_engine_source" > "$config_file_engine_en_destination"
unix2dos --quiet "$config_file_engine_en_destination"

config_file_engine_fr_destination="$(package_path 'PKG_L10N_FR')$(path_game_data)/engine/config/BaseEngine.ini"
config_file_engine_fr_ini_value='FRA'
mkdir --parents "$(dirname "$config_file_engine_fr_destination")"
sed --expression="s/^${config_file_engine_ini_field}=.*/${config_file_engine_ini_field}=${config_file_engine_fr_ini_value}/" \
	"$config_file_engine_source" > "$config_file_engine_fr_destination"
unix2dos --quiet "$config_file_engine_fr_destination"

rm "$config_file_engine_source"

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
case "$(messages_language)" in
	('fr')
		lang_string='version %s :'
		lang_en='anglaise'
		lang_fr='française'
	;;
	('en'|*)
		lang_string='%s version:'
		lang_en='English'
		lang_fr='French'
	;;
esac
printf '\n'
printf "$lang_string" "$lang_en"
print_instructions 'PKG_BIN' 'PKG_L10N_EN' 'PKG_DATA' 'PKG_DATA_MOVIES' 'PKG_DATA_TEXTURES'
printf "$lang_string" "$lang_fr"
print_instructions 'PKG_BIN' 'PKG_L10N_FR' 'PKG_DATA' 'PKG_DATA_MOVIES' 'PKG_DATA_TEXTURES'

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
