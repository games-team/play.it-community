#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Ixion
# send your bug reports to contact@dotslashplay.it
###

script_version=20240531.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='ixion'
GAME_NAME='Ixion'

ARCHIVE_BASE_0_NAME='setup_ixion_1.0.6.5_(64bit)_(69670).exe'
ARCHIVE_BASE_0_MD5='753b96e6b35d4adea73ca842f2e8f0e7'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_ixion_1.0.6.5_(64bit)_(69670)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='54d9f45d4ed211a14aea70797d1400c1'
ARCHIVE_BASE_0_PART2_NAME='setup_ixion_1.0.6.5_(64bit)_(69670)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='c4b119088ea2190b1d99e5efd38bd7ac'
ARCHIVE_BASE_0_SIZE='12480607'
ARCHIVE_BASE_0_VERSION='1.0.6.5-gog69670'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/ixion'

UNITY3D_NAME='ixion'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_DATA_SHAREDASSETS1_FILES="
${UNITY3D_NAME}_data/sharedassets?.*"
CONTENT_GAME_DATA_SHAREDASSETS2_FILES="
${UNITY3D_NAME}_data/sharedassets??.*"

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/BulwarkStudios/Ixion'
## The game menu background fails to render with wined3d.
WINE_DIRECT3D_RENDERER='dxvk'

PACKAGES_LIST='
PKG_BIN
PKG_DATA_SHAREDASSETS1
PKG_DATA_SHAREDASSETS2
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_DATA_SHAREDASSETS_ID="${PKG_DATA_ID}-sharedassets"
PKG_DATA_SHAREDASSETS1_ID="${PKG_DATA_SHAREDASSETS_ID}-1"
PKG_DATA_SHAREDASSETS2_ID="${PKG_DATA_SHAREDASSETS_ID}-2"
PKG_DATA_SHAREDASSETS_DESCRIPTION="$PKG_DATA_DESCRIPTION - shared assets"
PKG_DATA_SHAREDASSETS1_DESCRIPTION="$PKG_DATA_SHAREDASSETS_DESCRIPTION - 1"
PKG_DATA_SHAREDASSETS2_DESCRIPTION="$PKG_DATA_SHAREDASSETS_DESCRIPTION - 2"
PKG_DATA_DEPS="${PKG_DATA_DEPS:-} $PKG_DATA_SHAREDASSETS1_ID $PKG_DATA_SHAREDASSETS2_ID"

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
## TODO: Check if some specific GStreamer decoders are required.

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
