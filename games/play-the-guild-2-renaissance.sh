#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Guild 2 Renaissance
# send your bug reports to contact@dotslashplay.it
###

script_version=20231009.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='the-guild-2-renaissance'
GAME_NAME='The Guild 2 Renaissance'

ARCHIVE_BASE_0_NAME='setup_the_guild2_renaissance_2.2.0.5.exe'
ARCHIVE_BASE_0_MD5='86389c3154c2ea6ef3b072278f1e9c6c'
ARCHIVE_BASE_0_EXTRACTOR='innoextract'
ARCHIVE_BASE_0_EXTRACTOR_OPTIONS='--lowercase --gog'
ARCHIVE_BASE_0_PART1_NAME='setup_the_guild2_renaissance_2.2.0.5-1.bin'
ARCHIVE_BASE_0_PART1_MD5='ae4c17c8e3793befeec8b9a16e4f2b0c'
ARCHIVE_BASE_0_PART1_EXTRACTOR='unar'
ARCHIVE_BASE_0_SIZE='3800000'
ARCHIVE_BASE_0_VERSION='4.21-gog2.2.0.5'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/the_guild_2_renaissance'

CONTENT_PATH_DEFAULT='game'
CONTENT_GAME_BIN_FILES='
dbghelp.dll
fmod.dll
guildii.exe
mfc71.dll
modlauncher.exe
msvcp71.dll
msvcr71.dll
stlport.5.0.dll
stlportd.5.0.dll
wmencoderen.exe'
CONTENT_GAME_DATA_FILES='
camerapaths
*.raw
db
gui
*.url
mods
movie
msx
objects
particles
resource
savegames
scenes
scripts
sfx
shader
shots
sim_commands.dat
textures
worlds'
CONTENT_GAME0_DATA_PATH='support/app'
CONTENT_GAME0_DATA_FILES='
config.ini
input.ini'
CONTENT_DOC_DATA_FILES='
manual.pdf
*.txt'

USER_PERSISTENT_FILES='
*.ini'

APP_MAIN_EXE='guildii.exe'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

## TODO: Setting up a WINE virtual desktop on first launch might prevent display problems on some setups.

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
