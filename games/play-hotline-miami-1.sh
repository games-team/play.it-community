#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Daguhh
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Hotline Miami 1
# send your bug reports to contact@dotslashplay.it
###

script_version=20240623.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='hotline-miami-1'
GAME_NAME='Hotline Miami'

ARCHIVE_BASE_HUMBLE_0_NAME='HotlineMiami_linux_1392944501.tar.gz'
ARCHIVE_BASE_HUMBLE_0_MD5='f68e5680f7f54f12b313cb533af53548'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.0-humble.2014-02-20'
ARCHIVE_BASE_HUMBLE_0_SIZE='590000'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/hotline-miami'

ARCHIVE_BASE_GOG_0_NAME='gog_hotline_miami_2.0.0.4.sh'
ARCHIVE_BASE_GOG_0_MD5='ce95f10b1cba248b531228936c491eb4'
ARCHIVE_BASE_GOG_0_VERSION='1.0-gog2.0.0.4'
ARCHIVE_BASE_GOG_0_SIZE='540000'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/hotline_miami'

CONTENT_PATH_DEFAULT_HUMBLE='.'
CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_GAME_BIN_FILES='
hotline_launcher
Hotline'
CONTENT_GAME_DATA_FILES='
*.ogg
*.wad'

APP_MAIN_EXE='hotline_launcher'
## Only the GOG.com archive provides a game icon.
APP_MAIN_ICON_GOG='../support/icon.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libCgGL.so
libCg.so
libc.so.6
libdl.so.2
libfontconfig.so.1
libfreetype.so.6
libgcc_s.so.1
libGL.so.1
libGLU.so.1
libm.so.6
libogg.so.0
libopenal.so.1
libpthread.so.0
librt.so.1
libstdc++.so.6
libvorbisfile.so.3
libvorbis.so.0
libX11.so.6
libX11-xcb.so.1
libxcb.so.1
libXext.so.6
libXi.so.6
libXrandr.so.2
libXrender.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Set execution permissions on all binaries.
	chmod 755 'Hotline'
)

# Include game data

## Only the GOG.com archive provides a game icon.
case "$(current_archive)" in
	('ARCHIVE_BASE_GOG_'*)
		content_inclusion_icons 'PKG_DATA'
	;;
esac
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
