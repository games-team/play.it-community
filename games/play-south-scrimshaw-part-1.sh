#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# South Scrimshaw, Part 1
# send your bug reports to contact@dotslashplay.it
###

script_version=20231125.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='south-scrimshaw-part-1'
GAME_NAME='South Scrimshaw, Part One'

ARCHIVE_BASE_0_NAME='south_scrimshaw_part_one_1_0_66489.sh'
ARCHIVE_BASE_0_MD5='b68e7e8fa9491d38a55f007911b94da0'
ARCHIVE_BASE_0_SIZE='1699476'
ARCHIVE_BASE_0_VERSION='1.0-gog66489'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/south_scrimshaw_part_one'

RENPY_NAME='SouthScrimshaw'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN_PATH="${CONTENT_PATH_DEFAULT}/lib/py2-linux-x86_64"
CONTENT_LIBS_BIN_FILES='
librenpython.so'
CONTENT_GAME_BIN_FILES="
lib/py2-linux-x86_64/python
lib/py2-linux-x86_64/pythonw
lib/py2-linux-x86_64/zsync
lib/py2-linux-x86_64/zsyncmake
lib/py2-linux-x86_64/${RENPY_NAME}"
CONTENT_GAME_DATA_FILES="
game
renpy
lib/python2.7
${RENPY_NAME}.py"

APP_MAIN_EXE="lib/py2-linux-x86_64/${RENPY_NAME}"
APP_MAIN_ICON='SouthScrimshaw.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libGL.so.1
libm.so.6
libpthread.so.0
libutil.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
