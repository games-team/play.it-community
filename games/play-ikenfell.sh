#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Ikenfell
# send your bug reports to contact@dotslashplay.it
###

script_version=20240501.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='ikenfell'
GAME_NAME='Ikenfell'

ARCHIVE_BASE_0_NAME='setup_ikenfell_1.0.3_(64bit)_(52030).exe'
ARCHIVE_BASE_0_MD5='edf00ffea67cb1c5007cf0dbaba618c4'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='439559'
ARCHIVE_BASE_0_VERSION='1.0.3-gog52030'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/ikenfell'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
fmod.dll
fmodstudio.dll
sdl2.dll
ikenfellwin.exe'
CONTENT_GAME_DATA_FILES='
atlas
audio
data
scripts'
## The following .dll files are Mono libraries.
CONTENT_GAME0_DATA_PATH="${CONTENT_PATH_DEFAULT}/contents/resources"
CONTENT_GAME0_DATA_FILES='
gameengine.dll
littlewitch.dll
mono.posix.dll
mono.security.dll
moonsharp.interpreter.dll
mscorlib.dll
system.configuration.dll
system.core.dll
system.data.dll
system.dll
system.drawing.dll
system.numerics.dll
system.runtime.serialization.dll
system.security.dll
system.xml.dll
system.xml.linq.dll'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Roaming/ikenfell'
WINE_WINEPREFIX_TWEAKS='mono'

APP_MAIN_EXE='ikenfellwin.exe'
## The type must by explicitly set to ensure the binary is run through WINE, not Mono.
APP_MAIN_TYPE='wine'
## The game crashes on launch when the ALSA backend of SDL is used.
APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}"'
# The game crashes on launch when the ALSA backend of SDL is used
if [ "${SDL_AUDIODRIVER:-}" = "alsa" ]; then
	unset SDL_AUDIODRIVER
fi
'
## The game crashes on launch when the Wayland backend of SDL is used.
APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}"'
# The game crashes on launch when the Wayland backend of SDL is used
if [ "${SDL_VIDEODRIVER:-}" = "wayland" ]; then
	unset SDL_VIDEODRIVER
fi
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
