#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Journey to the Savage Planet
# send your bug reports to contact@dotslashplay.it
###

script_version=20240611.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='journey-to-the-savage-planet'
GAME_NAME='Journey to the Savage Planet'

ARCHIVE_BASE_0_NAME='setup_journey_to_the_savage_planet_1.0.10_(44657).exe'
ARCHIVE_BASE_0_MD5='d70cddb3d207b8f557ce6dfddc90c9eb'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_journey_to_the_savage_planet_1.0.10_(44657)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='0ae4643eb7a58450449c4fec4a0aea75'
ARCHIVE_BASE_0_PART2_NAME='setup_journey_to_the_savage_planet_1.0.10_(44657)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='155d3018829b5e4875a445d221dae8c0'
ARCHIVE_BASE_0_SIZE='6500000'
ARCHIVE_BASE_0_VERSION='1.0.10-gog44657'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/journey_to_the_savage_planet'

UNREALENGINE4_NAME='towers'

CONTENT_PATH_DEFAULT='.'

APP_MAIN_EXE="${UNREALENGINE4_NAME}.exe"

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
