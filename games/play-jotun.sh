#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Jotun
# send your bug reports to contact@dotslashplay.it
###

script_version=20230625.2

GAME_ID='jotun'
GAME_NAME='Jotun'

ARCHIVE_BASE_0='jotun_12_09_2019_32415.sh'
ARCHIVE_BASE_0_MD5='67b9b5ee91016014cb02be170a3a295a'
ARCHIVE_BASE_0_SIZE='4100000'
ARCHIVE_BASE_0_VERSION='2019.09.12-gog32415'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/jotun'

ARCHIVE_BASE_OLD_2='gog_jotun_2.3.0.5.sh'
ARCHIVE_BASE_OLD_2_MD5='e79a13252802fe4fe008e817aa2d4f43'
ARCHIVE_BASE_OLD_2_SIZE='4200000'
ARCHIVE_BASE_OLD_2_VERSION='2017.07.27-gog2.3.0.5'

ARCHIVE_BASE_OLD_1='gog_jotun_2.2.0.4.sh'
ARCHIVE_BASE_OLD_1_MD5='451d27e2e3747ed137e6ec5f3956c5da'
ARCHIVE_BASE_OLD_1_SIZE='4200000'
ARCHIVE_BASE_OLD_1_VERSION='2016.12.21-gog2.2.0.4'

ARCHIVE_BASE_OLD_0='gog_jotun_2.1.0.2.sh'
ARCHIVE_BASE_OLD_0_MD5='3f6976f56a4da1d6f274407b1342de7f'
ARCHIVE_BASE_OLD_0_SIZE='4200000'
ARCHIVE_BASE_OLD_0_VERSION='1.0-gog2.1.0.2'

UNITY3D_NAME='Jotun'
UNITY3D_PLUGINS='
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN32_FILES="
${UNITY3D_NAME}.x86
${UNITY3D_NAME}_Data/Mono/x86"
CONTENT_GAME_BIN64_FILES="
${UNITY3D_NAME}.x86_64
${UNITY3D_NAME}_Data/Mono/x86_64"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data"

PACKAGES_LIST='PKG_BIN32 PKG_BIN64 PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN32_ARCH='32'
PKG_BIN64_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN_DEPENDENCIES_LIBRARIES_OLD='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXrandr.so.2'
PKG_BIN32_DEPENDENCIES_LIBRARIES_OLD="$PKG_BIN_DEPENDENCIES_LIBRARIES_OLD"
PKG_BIN64_DEPENDENCIES_LIBRARIES_OLD="$PKG_BIN_DEPENDENCIES_LIBRARIES_OLD"

# Load common functions

target_version='2.23'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

for PKG in 'PKG_BIN32' 'PKG_BIN64'; do
	# shellcheck disable=SC2119
	launchers_write
done

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
