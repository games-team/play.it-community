#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Dorfromantik
# send your bug reports to contact@dotslashplay.it
###

script_version=20240603.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='dorfromantik'
GAME_NAME='Dorfromantik'

ARCHIVE_BASE_1_NAME='setup_dorfromantik_1.1.5.1_(64bit)_(63493).exe'
ARCHIVE_BASE_1_MD5='bb9e95a19d859e32c1d65164601522ce'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_dorfromantik_1.1.5.1_(64bit)_(63493)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='97fe88ae0f76a094a8db809364a204f0'
ARCHIVE_BASE_1_SIZE='592862'
ARCHIVE_BASE_1_VERSION='1.1.5.1-gog63493'
ARCHIVE_BASE_1_URL='https://www.gog.com/en/game/dorfromantik'

ARCHIVE_BASE_0_NAME='setup_dorfromantik_1.0.5_(64bit)_(55407).exe'
ARCHIVE_BASE_0_MD5='2d22a2a6d6d0e7b0527f48ae055ba138'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_dorfromantik_1.0.5_(64bit)_(55407)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='debd7fbad4a3a1aaed0f143ec7c2327c'
ARCHIVE_BASE_0_SIZE='630000'
ARCHIVE_BASE_0_VERSION='1.0.5-gog55407'

UNITY3D_NAME='dorfromantik'

CONTENT_PATH_DEFAULT='.'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Toukana Interactive/Dorfromantik'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
