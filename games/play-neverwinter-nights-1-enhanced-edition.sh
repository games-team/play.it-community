#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2021 Christian Stalp
# SPDX-FileCopyrightText: © 2023 Jacek Szafarkiewicz
set -o errexit

###
# Neverwinter Nights 1 Enhanced Edition
# send your bug reports to contact@dotslashplay.it
###

script_version=20241212.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='neverwinter-nights-1-enhanced-edition'
GAME_NAME='Neverwinter Nights: Enhanced Edition'

ARCHIVE_BASE_EN_6_NAME='neverwinter_nights_enhanced_edition_88_8193_36_13_72824.sh'
ARCHIVE_BASE_EN_6_MD5='922f474f2e1573abc66f803c752f2159'
ARCHIVE_BASE_EN_6_SIZE='6250881'
ARCHIVE_BASE_EN_6_VERSION='88.8193.36.13-gog72824'
ARCHIVE_BASE_EN_6_URL='https://www.gog.com/game/neverwinter_nights_enhanced_edition_pack'

ARCHIVE_BASE_DE_6_NAME='neverwinter_nights_enhanced_edition_german_88_8193_36_13_72824.sh'
ARCHIVE_BASE_DE_6_MD5='2218e12a8d09e1b799f11402c93d1eec'
ARCHIVE_BASE_DE_6_SIZE='6250881'
ARCHIVE_BASE_DE_6_VERSION='88.8193.36.13-gog72824'
ARCHIVE_BASE_DE_6_URL='https://www.gog.com/game/neverwinter_nights_enhanced_edition_pack'

ARCHIVE_BASE_FR_6_NAME='neverwinter_nights_enhanced_edition_french_88_8193_36_13_72824.sh'
ARCHIVE_BASE_FR_6_MD5='ed1a605cc15b5416d0eda7840dd4f8d9'
ARCHIVE_BASE_FR_6_SIZE='6250881'
ARCHIVE_BASE_FR_6_VERSION='88.8193.36.13-gog72824'
ARCHIVE_BASE_FR_6_URL='https://www.gog.com/game/neverwinter_nights_enhanced_edition_pack'

ARCHIVE_BASE_PL_6_NAME='neverwinter_nights_enhanced_edition_polish_88_8193_36_13_72824.sh'
ARCHIVE_BASE_PL_6_MD5='351e857d4e6a75d18721a3964a937bfe'
ARCHIVE_BASE_PL_6_SIZE='6250881'
ARCHIVE_BASE_PL_6_VERSION='88.8193.36.13-gog72824'
ARCHIVE_BASE_PL_6_URL='https://www.gog.com/game/neverwinter_nights_enhanced_edition_pack'

ARCHIVE_BASE_EN_5_NAME='neverwinter_nights_enhanced_edition_87_8193_35_40_65678.sh'
ARCHIVE_BASE_EN_5_MD5='c1191f9e19f1da52f1424025017cf56f'
ARCHIVE_BASE_EN_5_SIZE='6253452'
ARCHIVE_BASE_EN_5_VERSION='87.8193.35.40-gog65678'

ARCHIVE_BASE_FR_5_NAME='neverwinter_nights_enhanced_edition_french_87_8193_35_40_65678.sh'
ARCHIVE_BASE_FR_5_MD5='319194e7a229b24b19f7454d8ed860d6'
ARCHIVE_BASE_FR_5_SIZE='6253452'
ARCHIVE_BASE_FR_5_VERSION='87.8193.35.40-gog65678'

ARCHIVE_BASE_PL_5_NAME='neverwinter_nights_enhanced_edition_polish_87_8193_35_40_65678.sh'
ARCHIVE_BASE_PL_5_MD5='d4c3d4c4af501424a60bf9e09e5a2f01'
ARCHIVE_BASE_PL_5_SIZE='6253452'
ARCHIVE_BASE_PL_5_VERSION='87.8193.35.40-gog65678'

ARCHIVE_BASE_EN_4_NAME='neverwinter_nights_enhanced_edition_85_8193_33_50393.sh'
ARCHIVE_BASE_EN_4_MD5='4e6c4f7bd75791a9c726b7b573cfafe5'
ARCHIVE_BASE_EN_4_SIZE='6300000'
ARCHIVE_BASE_EN_4_VERSION='85.8193.33-gog50393'

ARCHIVE_BASE_FR_4_NAME='neverwinter_nights_enhanced_edition_french_85_8193_33_50393.sh'
ARCHIVE_BASE_FR_4_MD5='19b036e3c07fd4aa9a835de05b4aea1f'
ARCHIVE_BASE_FR_4_SIZE='6200000'
ARCHIVE_BASE_FR_4_VERSION='85.8193.33-gog50393'

ARCHIVE_BASE_EN_3_NAME='neverwinter_nights_enhanced_edition_83_8193_23_47201.sh'
ARCHIVE_BASE_EN_3_MD5='b85d2ea70aa6dba1096e97e944d98141'
ARCHIVE_BASE_EN_3_SIZE='6300000'
ARCHIVE_BASE_EN_3_VERSION='83.8193.23-gog47201'

ARCHIVE_BASE_EN_2_NAME='neverwinter_nights_enhanced_edition_82_8193_20_1_43497.sh'
ARCHIVE_BASE_EN_2_MD5='c80e73f0305ad99355cc089c632b8b0c'
ARCHIVE_BASE_EN_2_SIZE='6200000'
ARCHIVE_BASE_EN_2_VERSION='82.8193.20.1-gog43497'

ARCHIVE_BASE_EN_1_NAME='neverwinter_nights_enhanced_edition_81_8193_16_41300.sh'
ARCHIVE_BASE_EN_1_MD5='a52646002ab14c452731b0636fdc8278'
ARCHIVE_BASE_EN_1_SIZE='6200000'
ARCHIVE_BASE_EN_1_VERSION='81.8193.16-gog41300'

ARCHIVE_BASE_FR_1_NAME='neverwinter_nights_enhanced_edition_french_81_8193_16_41300.sh'
ARCHIVE_BASE_FR_1_MD5='67f42c1a1604a26fdbcd0cbc800856f5'
ARCHIVE_BASE_FR_1_SIZE='6200000'
ARCHIVE_BASE_FR_1_VERSION='81.8193.16-gog41300'

ARCHIVE_BASE_EN_0_NAME='neverwinter_nights_enhanced_edition_80_8193_9_37029.sh'
ARCHIVE_BASE_EN_0_MD5='fb98f859b5f5516fc7df8b00c7264c07'
ARCHIVE_BASE_EN_0_VERSION='80.8193.9-gog37029'
ARCHIVE_BASE_EN_0_SIZE='5000000'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN_FILES='
bin/linux-x86'
CONTENT_GAME_DATA_FILES='
data
ovr
lang/??/data'
CONTENT_DOC_DATA_RELATIVE_PATH='lang'
CONTENT_DOC_DATA_FILES='
??/docs'

APP_MAIN_EXE='bin/linux-x86/nwmain-linux'
APP_MAIN_ICON='bin/win32/nwmain.exe'

APP_SERVER_ID="${GAME_ID}-server"
APP_SERVER_NAME="${GAME_NAME} - server"
APP_SERVER_EXE='bin/linux-x86/nwserver-linux'
APP_SERVER_ICON='bin/win32/nwserver.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
librt.so.1
libstdc++.so.6'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

## Force the use of system-provided SDL
APP_MAIN_PRERUN="$(application_prerun 'APP_MAIN')
# Force the use of the system SDL library
export SDL_DYNAMIC_API='$(path_libraries_system)/libSDL2-2.0.so.0'
"

## Run the game binary from its parent directory
game_exec_line() {
	cat <<- 'EOF'
	cd bin/linux-x86
	./nwmain-linux "$@"
	EOF
}
launchers_generation 'PKG_BIN' 'APP_MAIN'

## Run the game binary from its parent directory
game_exec_line() {
	cat <<- 'EOF'
	cd bin/linux-x86
	./nwserver-linux "$@"
	EOF
}
launchers_generation 'PKG_BIN' 'APP_SERVER'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
