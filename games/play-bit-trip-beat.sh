#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Bit.Trip Beat
# send your bug reports to contact@dotslashplay.it
###

script_version=20230101.1

# Set game-specific variables

GAME_ID='bit-trip-beat'
GAME_NAME='BIT.TRIP BEAT'

ARCHIVE_BASE_0='gog_bit_trip_beat_2.0.0.1.sh'
ARCHIVE_BASE_0_MD5='32b6fd23c32553aa7c50eaf4247ba664'
ARCHIVE_BASE_0_TYPE='mojosetup'
ARCHIVE_BASE_0_SIZE='120000'
ARCHIVE_BASE_0_VERSION='1.0.5-gog2.0.0.1'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/bittrip_beat'

CONTENT_PATH_DEFAULT='data/noarch/game/bit.trip.beat-1.0-32/bit.trip.beat'
CONTENT_GAME_BIN32_FILES='
bit.trip.beat'
CONTENT_GAME_BIN64_PATH='data/noarch/game/bit.trip.beat-1.0-64/bit.trip.beat'
CONTENT_GAME_BIN64_FILES='
bit.trip.beat'
CONTENT_GAME_DATA_FILES='
Effects
Fonts
Sounds
Models
Shaders
Textures
BEAT.png'
CONTENT_DOC0_DATA_PATH='data/noarch/game/bit.trip.beat-1.0-32'
CONTENT_DOC0_DATA_FILES='
README*
*.txt'

APP_MAIN_EXE='bit.trip.beat'
APP_MAIN_ICON='BEAT.png'

PACKAGES_LIST='PKG_BIN32 PKG_BIN64 PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN32_ARCH='32'
PKG_BIN32_DEPS="$PKG_DATA_ID"
PKG_BIN32_DEPENDENCIES_LIBRARIES='
libc.so.6
libgcc_s.so.1
libGL.so.1
libm.so.6
libogg.so.0
libopenal.so.1
libpthread.so.0
librt.so.1
libSDL-1.2.so.0
libstdc++.so.6
libvorbisfile.so.3
libvorbis.so.0
libz.so.1'

PKG_BIN64_ARCH='64'
PKG_BIN64_DEPS="$PKG_BIN32_DEPS"
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN32_DEPENDENCIES_LIBRARIES"

# Load common functions

target_version='2.20'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

for PKG in 'PKG_BIN32' 'PKG_BIN64'; do
	# shellcheck disable=SC2119
	launchers_write
done

# Build package

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
