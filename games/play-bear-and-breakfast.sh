#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Mopi
# SPDX-FileCopyrightText: © 2025 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Bear and Breakfast
# send your bug reports to contact@dotslashplay.it
###

script_version=20250114.1

PLAYIT_COMPATIBILITY_LEVEL='2.32'

GAME_ID='bear-and-breakfast'
GAME_NAME='Bear and Breakfast'

ARCHIVE_BASE_1_NAME='setup_bear_and_breakfast_1.8.29_(64bit)_(78616).exe'
ARCHIVE_BASE_1_MD5='79ff1d75fbba105e26596e2f95e3b4ea'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='1339579'
ARCHIVE_BASE_1_VERSION='1.8.29-gog78616'
ARCHIVE_BASE_1_URL='https://www.gog.com/en/game/bear_and_breakfast'

ARCHIVE_BASE_0_NAME='setup_bear_and_breakfast_1.7.3_(64bit)_(63113).exe'
ARCHIVE_BASE_0_MD5='fe6328ca58e77fde90a595d81ae56954'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='2100000'
ARCHIVE_BASE_0_VERSION='1.7.3-gog63113'

UNITY3D_NAME='bearandbreakfast'

CONTENT_PATH_DEFAULT='.'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/GummyCat/BearAndBreakfast'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
