#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2018 Jacek Szafarkiewicz
set -o errexit

###
# Trine 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20250303.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='trine-2'
GAME_NAME='Trine 2'

ARCHIVE_BASE_GOG_1='gog_trine_2_complete_story_2.0.0.5.sh'
ARCHIVE_BASE_GOG_1_MD5='dd7126c1a6210e56fde20876bdb0a2ac'
ARCHIVE_BASE_GOG_1_SIZE='3700000'
ARCHIVE_BASE_GOG_1_VERSION='2.01.425-gog2.0.0.5'
ARCHIVE_BASE_GOG_1_URL='https://www.gog.com/game/trine_2_complete_story'

ARCHIVE_BASE_GOG_0='gog_trine_2_complete_story_2.0.0.4.sh'
ARCHIVE_BASE_GOG_0_MD5='dae867bff938dde002eafcce0b72e5b4'
ARCHIVE_BASE_GOG_0_SIZE='3700000'
ARCHIVE_BASE_GOG_0_VERSION='2.01.425-gog2.0.0.4'

ARCHIVE_BASE_HUMBLE_0='trine2_complete_story_v2_01_build_425_humble_linux_full.zip'
ARCHIVE_BASE_HUMBLE_0_MD5='82049b65c1bce6841335935bc05139c8'
ARCHIVE_BASE_HUMBLE_0_SIZE='3700000'
ARCHIVE_BASE_HUMBLE_0_VERSION='2.01build425-humble141016'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/trine-2-complete-story'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='.'
CONTENT_LIBS_BIN_PATH_GOG="${CONTENT_PATH_DEFAULT_GOG}/lib/lib32"
CONTENT_LIBS_BIN_PATH_HUMBLE="${CONTENT_PATH_DEFAULT_HUMBLE}/lib/lib32"
CONTENT_LIBS_BIN_FILES='
libCg.so
libCgGL.so
libPhysXCooking.so
libPhysXCooking.so.1
libPhysXCore.so
libPhysXCore.so.1
libPhysXLoader.so
libPhysXLoader.so.1
libSDL-1.3.so.0'
CONTENT_GAME_BIN_FILES='
bin'
CONTENT_GAME_DATA_FILES='
data
trine2.png
*.fbq'
CONTENT_DOC_DATA_FILES='
readme*'

APP_MAIN_EXE='bin/trine2_linux_launcher_32bit'
APP_MAIN_ICON='trine2.png'
## Work around a crash in fullscreen mode
APP_MAIN_PRERUN='# Work around a crash in fullscreen mode
export SDL_VIDEO_ALLOW_SCREENSAVER=1
'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libasound.so.2
libatk-1.0.so.0
libc.so.6
libdbus-1.so.3
libdl.so.2
libexpat.so.1
libfontconfig.so.1
libfreetype.so.6
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libgdk-x11-2.0.so.0
libgio-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libGLU.so.1
libgobject-2.0.so.0
libgthread-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libopenal.so.1
libpango-1.0.so.0
libpangoft2-1.0.so.0
libpng12.so.0
libpthread.so.0
librt.so.1
libstdc++.so.6
libuuid.so.1
libvorbisfile.so.3
libX11.so.6
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Set execution permissions on all binaries
	chmod 755 bin/*
)

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
