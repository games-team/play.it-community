#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 macaron
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Zak McKracken and the Alien Mindbenders
# send your bug reports to contact@dotslashplay.it
###

script_version=20240506.2

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='zak-mckracken-and-the-alien-mindbenders'
GAME_NAME='Zak McKracken and the Alien Mindbenders'

ARCHIVE_BASE_0_NAME='zak_mckracken_and_the_alien_mindbenders_en_gog_2_20099.sh'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/zak_mckracken_and_the_alien_mindbenders'
ARCHIVE_BASE_0_MD5='eb144295c6387b71ac2e78986067ba30'
ARCHIVE_BASE_0_SIZE='190000'
ARCHIVE_BASE_0_VERSION='1.0-gog20099'
ARCHIVE_BASE_0_TYPE='mojosetup'

CONTENT_PATH_DEFAULT='data/noarch/data'
CONTENT_GAME_MAIN_FILES='
*.lfl
*.mp3'
CONTENT_DOC_MAIN_PATH='data/noarch/docs'
CONTENT_DOC_MAIN_FILES='
Zak McKracken - The National Inquisitor.pdf
Zak McKraken and the Alien Mindbenders - Hintbook.pdf
Zak McKraken and the Alien Mindbenders - Manual.pdf'

APP_MAIN_SCUMMID='scumm:zak'
APP_MAIN_ICON='../support/icon.png'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Convert all file paths to lowercase.
	tolower .
)

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
