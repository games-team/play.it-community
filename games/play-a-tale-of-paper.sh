#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# A Tale of Paper
# send your bug reports to contact@dotslashplay.it
###

script_version=20241105.1

GAME_ID='a-tale-of-paper'
GAME_NAME='A Tale of Paper: Refolded'

GAME_ID_DEMO="${GAME_ID}-demo"
GAME_NAME_DEMO="$GAME_NAME (demo)"

ARCHIVE_BASE_0_NAME='setup_a_tale_of_paper_1.0_(56083).exe'
ARCHIVE_BASE_0_MD5='8a1acfc78e34a89ce608e275674a829a'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_a_tale_of_paper_1.0_(56083)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='664731f03da4cffe8a9771d298476cb2'
ARCHIVE_BASE_0_SIZE='4100000'
ARCHIVE_BASE_0_VERSION='1.0-gog56083'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/a_tale_of_paper_refolded'

ARCHIVE_BASE_DEMO_0_NAME='setup_a_tale_of_paper_demo_1.0_(54424).exe'
ARCHIVE_BASE_DEMO_0_MD5='9c154ee673c94bc896b1b08ac7c6b13b'
ARCHIVE_BASE_DEMO_0_TYPE='innosetup'
ARCHIVE_BASE_DEMO_0_SIZE='930000'
ARCHIVE_BASE_DEMO_0_VERSION='1.0-gog54424'
ARCHIVE_BASE_DEMO_0_URL='https://www.gog.com/game/a_tale_of_paper_demo'

UNREALENGINE4_NAME='ataleofpaper'
UNREALENGINE4_NAME_DEMO='prototipo'

CONTENT_PATH_DEFAULT='.'

APP_MAIN_EXE="${UNREALENGINE4_NAME}/binaries/win64/prototipo-win64-shipping.exe"
APP_MAIN_EXE_DEMO="${UNREALENGINE4_NAME_DEMO}/binaries/win64/${UNREALENGINE4_NAME_DEMO}-win64-shipping.exe"
## The --name=101 wrestool option, default for UE4 games, should not be used here
APP_MAIN_ICON_WRESTOOL_OPTIONS='--type=14'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_DEMO="${GAME_ID_DEMO}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
