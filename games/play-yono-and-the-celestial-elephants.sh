#!/bin/sh
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Yono and the Celestial Elephants
# send your bug reports to contact@dotslashplay.it
###

script_version=20240517.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='yono-and-the-celestial-elephants'
GAME_NAME='Yono and the Celestial Elephants'

ARCHIVE_BASE_1_NAME='setup_yono_and_the_celestial_elephants_01.01_cn_update_(40286).exe'
ARCHIVE_BASE_1_MD5='1503a970416ab3cd2e68db7f577f9701'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='1200000'
ARCHIVE_BASE_1_VERSION='01.01-gog40286'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/yono_and_the_celestial_elephants'

ARCHIVE_BASE_0_NAME='setup_yono_and_the_celestial_elephants_01.01_(15299).exe'
ARCHIVE_BASE_0_MD5='c16fddaa24eded544fb9ee42d5b4e2a2'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='1200000'
ARCHIVE_BASE_0_VERSION='01.01-gog15299'

UNITY3D_NAME='yono and the celestial elephants'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Neckbolt/Yono and the Celestial Elephants'
## Improve controller support.
## Tested only with an XBox 360 controller.
WINE_WINETRICKS_VERBS='xinput'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
