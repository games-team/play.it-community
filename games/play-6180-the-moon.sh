#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# 6180 The Moon
# send your bug reports to contact@dotslashplay.it
###

script_version=20221210.1

GAME_ID='6180-the-moon'
GAME_NAME='6180 The Moon'

UNITY3D_NAME='6180 the moon'

ARCHIVE_BASE_0='6180_the_moon_2.1.0_Linux.zip'
ARCHIVE_BASE_0_MD5='79e81fb57d8d5dbf27a7c4be2dd0efd9'
ARCHIVE_BASE_0_SIZE='130000'
ARCHIVE_BASE_0_VERSION='2.1.0-humble141209'
ARCHIVE_BASE_0_URL='https://www.humblebundle.com/store/6180-the-moon'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES="
${UNITY3D_NAME}_Data/Mono
${UNITY3D_NAME}_Data/Plugins
${UNITY3D_NAME}.x86"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data"

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
ld-linux.so.2
libatk-1.0.so.0
libcairo.so.2
libc.so.6
libdl.so.2
libfontconfig.so.1
libfreetype.so.6
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libgdk-x11-2.0.so.0
libgio-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libGLU.so.1
libgmodule-2.0.so.0
libgobject-2.0.so.0
libgthread-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpango-1.0.so.0
libpangocairo-1.0.so.0
libpangoft2-1.0.so.0
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXext.so.6
libXrandr.so.2'

# Load common functions

target_version='2.19'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build package

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
