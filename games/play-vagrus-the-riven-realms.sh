#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Vagrus - The Riven Realms
# send your bug reports to contact@dotslashplay.it
###

script_version=20230130.1

GAME_ID='vagrus-the-riven-realms-prologue'
GAME_NAME='Vagrus - The Riven Realms: Prologue'

UNITY3D_NAME='Vagrus'

ARCHIVE_BASE_GOG_0='vagrus_the_riven_realms_prologue_1_1150613h_56533.sh'
ARCHIVE_BASE_GOG_0_MD5='b1c3ec004b8d49d9d40a7f54b12fea21'
ARCHIVE_BASE_GOG_0_TYPE='mojosetup'
ARCHIVE_BASE_GOG_0_SIZE='5500000'
ARCHIVE_BASE_GOG_0_VERSION='1.1.15-gog56533'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/vagrus_the_riven_realms_prologue'

ARCHIVE_BASE_ITCH_0='vagrus-demolinux.zip'
ARCHIVE_BASE_ITCH_0_MD5='4bddf380ed1355b455802cfd615d9ec7'
ARCHIVE_BASE_ITCH_0_SIZE='3800000'
ARCHIVE_BASE_ITCH_0_VERSION='0.5.31-itch.2021.05.12'
ARCHIVE_BASE_ITCH_0_URL='https://lostpilgrims.itch.io/vagrus'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_ITCH='.'
CONTENT_LIBS_BIN_PATH_GOG="${CONTENT_PATH_DEFAULT_GOG}/${UNITY3D_NAME}_Data/Plugins"
CONTENT_LIBS_BIN_PATH_ITCH="${CONTENT_PATH_DEFAULT_ITCH}/${UNITY3D_NAME}_Data/Plugins"
CONTENT_LIBS_BIN_FILES='
lib_burst_generated.so'
CONTENT_GAME_BIN_FILES="
UnityPlayer.so
${UNITY3D_NAME}.x86_64
${UNITY3D_NAME}_Data/MonoBleedingEdge/x86_64"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data/Managed
${UNITY3D_NAME}_Data/MonoBleedingEdge/etc
${UNITY3D_NAME}_Data/Resources
${UNITY3D_NAME}_Data/StreamingAssets
${UNITY3D_NAME}_Data/app.info
${UNITY3D_NAME}_Data/boot.config
${UNITY3D_NAME}_Data/globalgamemanagers
${UNITY3D_NAME}_Data/level?
${UNITY3D_NAME}_Data/*.assets
${UNITY3D_NAME}_Data/*.assets.resS
${UNITY3D_NAME}_Data/*.json
${UNITY3D_NAME}_Data/*.resource"

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libz.so.1'

# Load common functions

target_version='2.21'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icon

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build package

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
