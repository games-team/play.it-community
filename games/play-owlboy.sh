#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Owlboy
# send your bug reports to contact@dotslashplay.it
###

script_version=20240831.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='owlboy'
GAME_NAME='Owlboy'

ARCHIVE_BASE_0_NAME='owlboy-01302020-bin'
ARCHIVE_BASE_0_MD5='0d8d67c79b9359ee4b79913b6b640465'
## This MojoSetup installer is not relying on a Makeself wrapper
ARCHIVE_BASE_0_EXTRACTOR='bsdtar'
ARCHIVE_BASE_0_SIZE='518307'
ARCHIVE_BASE_0_VERSION='1.3.7013.40718-humble200130'
ARCHIVE_BASE_0_URL='https://www.humblebundle.com/store/owlboy'

ARCHIVE_BASE_MULTIARCH_3_NAME='owlboy-03152019-bin'
ARCHIVE_BASE_MULTIARCH_3_MD5='2966b183f43f220ade646cb3f7872c49'
## This MojoSetup installer is not relying on a Makeself wrapper
ARCHIVE_BASE_MULTIARCH_3_EXTRACTOR='bsdtar'
ARCHIVE_BASE_MULTIARCH_3_SIZE='550000'
ARCHIVE_BASE_MULTIARCH_3_VERSION='1.3.7013.40178-humble190325'

ARCHIVE_BASE_MULTIARCH_2_NAME='owlboy-12292017.bin'
ARCHIVE_BASE_MULTIARCH_2_MD5='c2e99502013c7d2529bc2aefb6416dcf'
## This MojoSetup installer is not relying on a Makeself wrapper
ARCHIVE_BASE_MULTIARCH_2_EXTRACTOR='bsdtar'
ARCHIVE_BASE_MULTIARCH_2_SIZE='570000'
ARCHIVE_BASE_MULTIARCH_2_VERSION='1.3.6564.30139-humble1'

ARCHIVE_BASE_MULTIARCH_1_NAME='owlboy-11022017-bin'
ARCHIVE_BASE_MULTIARCH_1_MD5='d3a1e4753a604431c58eb1ea26c35543'
## This MojoSetup installer is not relying on a Makeself wrapper
ARCHIVE_BASE_MULTIARCH_1_EXTRACTOR='bsdtar'
ARCHIVE_BASE_MULTIARCH_1_SIZE='570000'
ARCHIVE_BASE_MULTIARCH_1_VERSION='1.3.6515.19883-humble171102'

ARCHIVE_BASE_MULTIARCH_0_NAME='owlboy-05232017-bin'
ARCHIVE_BASE_MULTIARCH_0_MD5='f35fba69fadffbf498ca8a38dbceeac1'
## This MojoSetup installer is not relying on a Makeself wrapper
ARCHIVE_BASE_MULTIARCH_0_EXTRACTOR='bsdtar'
ARCHIVE_BASE_MULTIARCH_0_SIZE='570000'
ARCHIVE_BASE_MULTIARCH_0_VERSION='1.2.6382.15868-humble1'

CONTENT_PATH_DEFAULT='data'
CONTENT_LIBS_FILES='
libmojoshader.so
libXNAFileDialog.so'
## The shipped FAudio must be used to prevent game crashes
CONTENT_LIBS_FILES="$CONTENT_LIBS_FILES
libFAudio.so.0"
CONTENT_LIBS_LIBS64_PATH="${CONTENT_PATH_DEFAULT}/lib64"
CONTENT_LIBS_LIBS64_FILES="$CONTENT_LIBS_FILES"
CONTENT_LIBS_LIBS32_PATH="${CONTENT_PATH_DEFAULT}/lib"
CONTENT_LIBS_LIBS32_FILES="$CONTENT_LIBS_FILES"
CONTENT_GAME_MAIN_FILES='
content
monoconfig
monomachineconfig
Owlboy.bmp
Owlboy.exe
FNA.dll
FNA.dll.config
GamedevUtility.dll
MoonSharp.Interpreter.dll
SharpFont.dll
SharpFont.dll.config
TimSort.dll'
CONTENT_DOC_MAIN_FILES='
Linux.README'

USER_PERSISTENT_FILES='
content/fonts/*.ini
content/localizations/*/speechbubbleconfig.ini'

APP_MAIN_EXE='Owlboy.exe'
APP_MAIN_ICON='Owlboy.bmp'

PACKAGES_LIST='
PKG_MAIN
PKG_LIBS64'
PACKAGES_LIST_MULTIARCH='
PKG_MAIN
PKG_LIBS64
PKG_LIBS32'

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_LIBS'
PKG_MAIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libfreetype.so.6
libGL.so.1
libSDL2-2.0.so.0
libSDL2_image-2.0.so.0
libudev.so.1'
PKG_MAIN_DEPENDENCIES_MONO_LIBRARIES='
mscorlib.dll
Mono.Posix.dll
Mono.Security.dll
System.dll
System.Configuration.dll
System.Core.dll
System.Data.dll
System.Design.dll
System.Drawing.dll
System.Management.dll
System.Numerics.dll
System.Runtime.Serialization.dll
System.Security.dll
System.Transactions.dll
System.Xml.dll'


PKG_LIBS_ID="${GAME_ID}-libs"
PKG_LIBS64_ID="$PKG_LIBS_ID"
PKG_LIBS32_ID="$PKG_LIBS_ID"
PKG_LIBS_DESCRIPTION='Shipped libraries'
PKG_LIBS64_DESCRIPTION="$PKG_LIBS_DESCRIPTION"
PKG_LIBS32_DESCRIPTION="$PKG_LIBS_DESCRIPTION"
PKG_LIBS64_ARCH='64'
PKG_LIBS32_ARCH='32'

# Convert .ini files to Unix-style line separators

SCRIPT_DEPS="${SCRIPT_DEPS:-} find dos2unix"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Convert .ini files to Unix-style line separators
	find . -type f -name '*.ini' \
		-exec dos2unix --quiet '{}' +
)

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
