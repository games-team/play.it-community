#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Mopi
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Braveland games:
# - Braveland
# - Braveland Wizard
# - Braveland Pirate
# send your bug reports to contact@dotslashplay.it
###

script_version=20230917.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID_BRAVELAND='braveland'
GAME_NAME_BRAVELAND='Braveland'

GAME_ID_WIZARD='braveland-wizard'
GAME_NAME_WIZARD='Braveland Wizard'

GAME_ID_PIRATE='braveland-pirate'
GAME_NAME_PIRATE='Braveland Pirate'

# Archives

## Braveland (original game)

ARCHIVE_BASE_BRAVELAND_1_NAME='braveland_en_1_4_0_19_18418.sh'
ARCHIVE_BASE_BRAVELAND_1_MD5='7ceac41f0486310c04ec60fcdf7e2b46'
ARCHIVE_BASE_BRAVELAND_1_SIZE='320000'
ARCHIVE_BASE_BRAVELAND_1_VERSION='1.4.0.19-gog18418'
ARCHIVE_BASE_BRAVELAND_1_URL='https://www.gog.com/game/braveland'

ARCHIVE_BASE_BRAVELAND_0_NAME='gog_braveland_2.1.0.8.sh'
ARCHIVE_BASE_BRAVELAND_0_MD5='c60ac5170e53a7ed22fda6a3e6ce690e'
ARCHIVE_BASE_BRAVELAND_0_SIZE='320000'
ARCHIVE_BASE_BRAVELAND_0_VERSION='1.3.2.18-gog2.1.0.8'

## Braveland Wizard

ARCHIVE_BASE_WIZARD_2_NAME='braveland_wizard_en_1_1_4_14_19675.sh'
ARCHIVE_BASE_WIZARD_2_MD5='79fa2c8280b8b3370d85fc32907e2b51'
ARCHIVE_BASE_WIZARD_2_SIZE='450000'
ARCHIVE_BASE_WIZARD_2_VERSION='1.1.4.14-gog19675'
ARCHIVE_BASE_WIZARD_2_URL='https://www.gog.com/game/braveland_wizard'

ARCHIVE_BASE_WIZARD_1_NAME='braveland_wizard_en_1_1_3_13_18418.sh'
ARCHIVE_BASE_WIZARD_1_MD5='7153da6ceb0deda556ebdb7cfa4b9203'
ARCHIVE_BASE_WIZARD_1_SIZE='450000'
ARCHIVE_BASE_WIZARD_1_VERSION='1.1.3.13-gog18418'

ARCHIVE_BASE_WIZARD_0_NAME='gog_braveland_wizard_2.1.0.4.sh'
ARCHIVE_BASE_WIZARD_0_MD5='14346dc8d6e7ad410dd1b179763aa94e'
ARCHIVE_BASE_WIZARD_0_SIZE='450000'
ARCHIVE_BASE_WIZARD_0_VERSION='1.1.1.11-gog2.1.0.4'

## Braveland Pirate

ARCHIVE_BASE_PIRATE_2_NAME='braveland_pirate_en_1_1_1_10_19675.sh'
ARCHIVE_BASE_PIRATE_2_MD5='660ad8cd6eef0f7bd1eadcacf1c611c0'
ARCHIVE_BASE_PIRATE_2_SIZE='610000'
ARCHIVE_BASE_PIRATE_2_VERSION='1.1.1.10-gog19675'
ARCHIVE_BASE_PIRATE_2_URL='https://www.gog.com/game/braveland_pirate'

ARCHIVE_BASE_PIRATE_1_NAME='braveland_pirate_en_1_1_0_9_18418.sh'
ARCHIVE_BASE_PIRATE_1_MD5='445b705cc11b58fd289064aa78ef9210'
ARCHIVE_BASE_PIRATE_1_SIZE='610000'
ARCHIVE_BASE_PIRATE_1_VERSION='1.1.0.9-gog18418'

ARCHIVE_BASE_PIRATE_0_NAME='gog_braveland_pirate_2.1.0.3.sh'
ARCHIVE_BASE_PIRATE_0_MD5='982331936f2767daf1974c1686ec0bd3'
ARCHIVE_BASE_PIRATE_0_SIZE='610000'
ARCHIVE_BASE_PIRATE_0_VERSION='1.0.2.7-gog2.1.0.4'

# Archives content

CONTENT_PATH_DEFAULT='data/noarch/game'

UNITY3D_NAME_BRAVELAND='Braveland'
UNITY3D_NAME_WIZARD='Braveland Wizard'
UNITY3D_NAME_PIRATE='Braveland Pirate'
UNITY3D_PLUGINS='
ScreenSelector.so'

# Packages

PACKAGES_LIST='PKG_BIN32 PKG_BIN64 PKG_DATA'

PKG_DATA_DESCRIPTION='data'

PKG_BIN32_ARCH='32'
PKG_BIN64_ARCH='64'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libGLU.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1'
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

## Braveland (original game)

PKG_DATA_ID_BRAVELAND="${GAME_ID_BRAVELAND}-data"

PKG_BIN_DEPS_BRAVELAND="$PKG_DATA_ID_BRAVELAND"
PKG_BIN32_DEPS_BRAVELAND="$PKG_BIN_DEPS_BRAVELAND"
PKG_BIN64_DEPS_BRAVELAND="$PKG_BIN_DEPS_BRAVELAND"

## Braveland Wizard

PKG_DATA_ID_WIZARD="${GAME_ID_WIZARD}-data"

PKG_BIN_DEPS_PIRATE="$PKG_DATA_ID_PIRATE"
PKG_BIN32_DEPS_PIRATE="$PKG_BIN_DEPS_PIRATE"
PKG_BIN64_DEPS_PIRATE="$PKG_BIN_DEPS_PIRATE"

## Braveland Pirate

PKG_DATA_ID_PIRATE="${GAME_ID_PIRATE}-data"

PKG_BIN_DEPS_WIZARD="$PKG_DATA_ID_WIZARD"
PKG_BIN32_DEPS_WIZARD="$PKG_BIN_DEPS_WIZARD"
PKG_BIN64_DEPS_WIZARD="$PKG_BIN_DEPS_WIZARD"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game icon

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Write launchers

PKG='PKG_BIN32'
# shellcheck disable=SC2119
launchers_write
PKG='PKG_BIN64'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
