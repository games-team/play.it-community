#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Chained Echoes
# send your bug reports to contact@dotslashplay.it
###

script_version=20241217.2

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='chained-echoes'
GAME_NAME='Chained Echoes'

ARCHIVE_BASE_1_NAME='chained_echoes_1_322_71539.sh'
ARCHIVE_BASE_1_MD5='1bfa6fc06f6f6dce5943d5a28ed92951'
ARCHIVE_BASE_1_SIZE='822597'
ARCHIVE_BASE_1_VERSION='1.322-gog71539'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/chained_echoes'

ARCHIVE_BASE_0_NAME='chained_echoes_1_3_65510.sh'
ARCHIVE_BASE_0_MD5='b319014ce49e39b8ff4aa3540a79e874'
ARCHIVE_BASE_0_SIZE='830524'
ARCHIVE_BASE_0_VERSION='1.3-gog65510'

UNITY3D_NAME='Chained_Echoes'
UNITY3D_NAME_0='Chained Echoes'

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Prevent the inclusion of shipped Steam libraries
	rm --recursive "$(unity3d_name)_Data/Plugins"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
