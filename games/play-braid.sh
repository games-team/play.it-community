#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Braid
# send your bug reports to contact@dotslashplay.it
###

script_version=20241114.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='braid'
GAME_NAME='Braid'

ARCHIVE_BASE_1_NAME='gog_braid_2.0.0.3.sh'
ARCHIVE_BASE_1_MD5='0d60f92ed8d1c72afb11c217cc907264'
ARCHIVE_BASE_1_SIZE='170000'
ARCHIVE_BASE_1_VERSION='2015.06.11-gog2.0.0.3'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/braid'

ARCHIVE_BASE_0_NAME='gog_braid_2.0.0.2.sh'
ARCHIVE_BASE_0_MD5='22bac5c37b44916fea3e23092706d55d'
ARCHIVE_BASE_0_SIZE='170000'
ARCHIVE_BASE_0_VERSION='2015.06.11-gog2.0.0.2'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN_FILES='
Braid.bin.x86'
CONTENT_GAME_DATA_FILES='
data
Icon.png'
CONTENT_DOC_DATA_FILES='
licenses
*.txt'

APP_MAIN_EXE='Braid.bin.x86'
APP_MAIN_ICON='Icon.png'
## The game crashes on launch when using the wayland backend of SDL,
## even when using the system-provided build of SDL.
APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}"'
# The game crashes on launch when using the wayland backend of SDL
if [ "${SDL_VIDEODRIVER:-}" = "wayland" ]; then
	unset SDL_VIDEODRIVER
fi
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libCgGL.so
libCg.so
libc.so.6
libgcc_s.so.1
libGL.so.1
libm.so.6
libpthread.so.0
libSDL2-2.0.so.0
libstdc++.so.6'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
