#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Abandon Ship
# send your bug reports to contact@dotslashplay.it
###

script_version=20230623.2

GAME_ID='abandon-ship'
GAME_NAME='Abandon Ship'

ARCHIVE_BASE_0='abandon_ship_1_3_14934_41922.sh'
ARCHIVE_BASE_0_MD5='484e670989853f9c7460d0dd08b3ffba'
ARCHIVE_BASE_0_SIZE='11000000'
ARCHIVE_BASE_0_VERSION='1.3.14934-gog41922'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/abandon_ship'

UNITY3D_NAME='AbandonShip'
UNITY3D_PLUGINS='
libAkHarmonizer.so
libAkSoundEngine.so
libSynthOne.so
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN_FILES="
${UNITY3D_NAME}
${UNITY3D_NAME}_Data/Mono/x86_64/libmono.so
${UNITY3D_NAME}_Data/Mono/x86_64/libMonoPosixHelper.so"
CONTENT_GAME_DATA_ASSETS_FILES="
${UNITY3D_NAME}_Data/StreamingAssets/AssetBundles"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data"
CONTENT_DOC_DATA_FILES='
Licenses'

APP_MAIN_EXE="$UNITY3D_NAME"

PACKAGES_LIST='PKG_BIN PKG_DATA_ASSETS PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_DATA_ASSETS_ID="${PKG_DATA_ID}-assets"
PKG_DATA_ASSETS_DESCRIPTION="$PKG_DATA_DESCRIPTION - assets"
PKG_DATA_DEPS="$PKG_DATA_DEPS $PKG_DATA_ASSETS_ID"

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libstdc++.so.6
libz.so.1'

# Load common functions

target_version='2.23'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Link some Unity3D plugins in the game data path
# as the game engine fails to find it otherwise,
# even if it is in LD_LIBRARY_PATH.

for file_name in \
	'libAkSoundEngine.so' \
	'libAkHarmonizer.so' \
	'libSynthOne.so' \
	'ScreenSelector.so'
do
	file_source="$(path_libraries)/${file_name}"
	file_destination="$(package_path 'PKG_BIN')$(path_game_data)/$(unity3d_name)_Data/Plugins/x86_64/${file_name}"
	mkdir --parents "$(dirname "$file_destination")"
	ln --symbolic "$file_source" "$file_destination"
done

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
