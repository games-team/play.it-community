#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Urtuk: The Desolation
# send your bug reports to contact@dotslashplay.it
###

script_version=20240624.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='urtuk-the-desolation'
GAME_NAME='Urtuk: The Desolation'

ARCHIVE_BASE_0_NAME='urtuk_the_desolation_1_0_0_91b_53003.sh'
ARCHIVE_BASE_0_MD5='4fddaaa42c70b75258a78d4a5b103473'
ARCHIVE_BASE_0_SIZE='740000'
ARCHIVE_BASE_0_VERSION='1.0.0.91b-gog53003'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/urtuk_the_desolation'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_DATA_FILES='
desktop-1.0.jar'
CONTENT_GAME_BIN_SHIPPED_FILES='
jre
config.json
urtuk'

USER_PERSISTENT_FILES='
*.bin
*.bin.backup'

APP_MAIN_TYPE_BIN_SYSTEM='java'
APP_MAIN_EXE_BIN_SYSTEM='desktop-1.0.jar'
APP_MAIN_EXE_BIN_SHIPPED='urtuk'
APP_MAIN_ICON='../support/icon.png'

PACKAGES_LIST='
PKG_BIN_SYSTEM
PKG_BIN_SHIPPED
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_SHIPPED_ARCH='64'
PKG_BIN_ID="${GAME_ID}-bin"
PKG_BIN_SYSTEM_ID="${PKG_BIN_ID}-system"
PKG_BIN_SHIPPED_ID="${PKG_BIN_ID}-shipped"
PKG_BIN_PROVIDES="
$PKG_BIN_ID"
PKG_BIN_SYSTEM_PROVIDES="$PKG_BIN_PROVIDES"
PKG_BIN_SHIPPED_PROVIDES="$PKG_BIN_PROVIDES"
PKG_BIN_SYSTEM_DEPS="$PKG_DATA_ID"
PKG_BIN_SHIPPED_DEPS="$PKG_DATA_ID"
PKG_BIN_SHIPPED_DEPENDENCIES_LIBRARIES='
libasound.so.2
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
libstdc++.so.6
libthread_db.so.1
libX11.so.6
libXext.so.6
libXi.so.6
libXrender.so.1
libXtst.so.6'
PKG_BIN_SYSTEM_DESCRIPTION='Using system-provided Java'
PKG_BIN_SHIPPED_DESCRIPTION='Using shipped binaries'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN_SYSTEM'
launchers_generation 'PKG_BIN_SHIPPED'

# Build packages

packages_generation
case "$(messages_language)" in
	('fr')
		message='Utilisation des binaires fournis par %s :'
		bin_system='le système'
		bin_shipped='les développeurs'
	;;
	('en'|*)
		message='Using binaries provided by %s:'
		bin_system='the system'
		bin_shipped='the developers'
	;;
esac
printf '\n'
printf "$message" "$bin_system"
print_instructions 'PKG_DATA' 'PKG_BIN_SYSTEM'
printf "$message" "$bin_shipped"
print_instructions 'PKG_DATA' 'PKG_BIN_SHIPPED'

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
