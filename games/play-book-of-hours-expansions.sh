#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Book of Hours expansions:
# - Perpetual Edition
# - House of Light
# send your bug reports to contact@dotslashplay.it
###

script_version=20241209.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='book-of-hours'
GAME_NAME='Book of Hours'

EXPANSION_ID_PERPETUAL='perpetual-edition'
EXPANSION_NAME_PERPETUAL='Perpetual Edition'

EXPANSION_ID_LIGHT='house-of-light'
EXPANSION_NAME_LIGHT='House of Light'

# Archives

## Perpetual Edition

ARCHIVE_BASE_PERPETUAL_14_NAME='book_of_hours_perpetual_edition_dlc_2024_12_m_5_78248.sh'
ARCHIVE_BASE_PERPETUAL_14_MD5='9e6f91b10fed81668deea7c04c0739e7'
ARCHIVE_BASE_PERPETUAL_14_SIZE='923'
ARCHIVE_BASE_PERPETUAL_14_VERSION='2024.12.m.5-gog78248'

ARCHIVE_BASE_PERPETUAL_13_NAME='book_of_hours_perpetual_edition_dlc_2024_11_k_1_77536.sh'
ARCHIVE_BASE_PERPETUAL_13_MD5='73102bfa896fcb1f73db07aa202feed3'
ARCHIVE_BASE_PERPETUAL_13_SIZE='923'
ARCHIVE_BASE_PERPETUAL_13_VERSION='2024.11.k.1-gog77536'

ARCHIVE_BASE_PERPETUAL_12_NAME='book_of_hours_perpetual_edition_dlc_2024_10_j_14_77218.sh'
ARCHIVE_BASE_PERPETUAL_12_MD5='00a3bc29ebc84b5a77aadd53e1c549f5'
ARCHIVE_BASE_PERPETUAL_12_SIZE='923'
ARCHIVE_BASE_PERPETUAL_12_VERSION='2024.10.j.14-gog77218'

ARCHIVE_BASE_PERPETUAL_10_NAME='book_of_hours_perpetual_edition_dlc_2024_9_i_8_76549.sh'
ARCHIVE_BASE_PERPETUAL_10_MD5='169aa03df406345a720776756cd9e931'
ARCHIVE_BASE_PERPETUAL_10_SIZE='923'
ARCHIVE_BASE_PERPETUAL_10_VERSION='2024.09.i.8-gog76549'

ARCHIVE_BASE_PERPETUAL_8_NAME='book_of_hours_perpetual_edition_content_2024_3_e_15_72083.sh'
ARCHIVE_BASE_PERPETUAL_8_MD5='2b21b55d6889a4c18ae57cbb1b22cb04'
ARCHIVE_BASE_PERPETUAL_8_SIZE='75087'
ARCHIVE_BASE_PERPETUAL_8_VERSION='2024.03.e.15-gog72083'

ARCHIVE_BASE_PERPETUAL_7_NAME='book_of_hours_perpetual_edition_content_2024_2_e_11_71531.sh'
ARCHIVE_BASE_PERPETUAL_7_MD5='a120f5371c1297a8ae13095b7ee3ccdb'
ARCHIVE_BASE_PERPETUAL_7_SIZE='75087'
ARCHIVE_BASE_PERPETUAL_7_VERSION='2024.02.e.11-gog71531'

ARCHIVE_BASE_PERPETUAL_6_NAME='book_of_hours_perpetual_edition_content_2023_12_d_12_69519.sh'
ARCHIVE_BASE_PERPETUAL_6_MD5='77796819b6f4a466fed0638fcb685412'
ARCHIVE_BASE_PERPETUAL_6_SIZE='75408'
ARCHIVE_BASE_PERPETUAL_6_VERSION='2023.12.d.12-gog69519'

ARCHIVE_BASE_PERPETUAL_3_NAME='book_of_hours_perpetual_edition_content_2023_11_d_4_69228.sh'
ARCHIVE_BASE_PERPETUAL_3_MD5='d5bb3e49df9b8c142ac25aed53c78d01'
ARCHIVE_BASE_PERPETUAL_3_SIZE='75404'
ARCHIVE_BASE_PERPETUAL_3_VERSION='2023.11.d.4-gog69228'

ARCHIVE_BASE_PERPETUAL_2_NAME='book_of_hours_perpetual_edition_content_2023_10_c_11_68174.sh'
ARCHIVE_BASE_PERPETUAL_2_MD5='0be5366899fdd4f209153a2671ca87ba'
ARCHIVE_BASE_PERPETUAL_2_SIZE='75404'
ARCHIVE_BASE_PERPETUAL_2_VERSION='2023.10.c.11-gog68174'

ARCHIVE_BASE_PERPETUAL_1_NAME='book_of_hours_perpetual_edition_content_2023_9_b_12_67790.sh'
ARCHIVE_BASE_PERPETUAL_1_MD5='a0eafa7c516ad47b0c58e9af037b99b1'
ARCHIVE_BASE_PERPETUAL_1_SIZE='75408'
ARCHIVE_BASE_PERPETUAL_1_VERSION='2023.09.b.12-gog67790'

ARCHIVE_BASE_PERPETUAL_0_NAME='book_of_hours_perpetual_edition_content_2023_8_g_2_67165.sh'
ARCHIVE_BASE_PERPETUAL_0_MD5='c426a4968539633c0d0959744ea3d671'
ARCHIVE_BASE_PERPETUAL_0_SIZE='75408'
ARCHIVE_BASE_PERPETUAL_0_VERSION='2023.08.g.2-gog67165'

## House of Light

ARCHIVE_BASE_LIGHT_14_NAME='book_of_hours_house_of_light_2024_12_m_5_78248.sh'
ARCHIVE_BASE_LIGHT_14_MD5='53d1d93cf7b27ae023ecb18038c182bf'
ARCHIVE_BASE_LIGHT_14_SIZE='5200'
ARCHIVE_BASE_LIGHT_14_VERSION='2024.12.m.5-gog78248'
ARCHIVE_BASE_LIGHT_14_URL='https://www.gog.com/game/book_of_hours_house_of_light'

ARCHIVE_BASE_LIGHT_13_NAME='book_of_hours_house_of_light_2024_11_k_1_77536.sh'
ARCHIVE_BASE_LIGHT_13_MD5='ce5306c17782fe048218bdafd4543d6e'
ARCHIVE_BASE_LIGHT_13_SIZE='3330'
ARCHIVE_BASE_LIGHT_13_VERSION='2024.11.k.1-gog77536'

ARCHIVE_BASE_LIGHT_12_NAME='book_of_hours_house_of_light_2024_10_j_14_77218.sh'
ARCHIVE_BASE_LIGHT_12_MD5='d87df808a25ca1bb01e1f2e7f2c30622'
ARCHIVE_BASE_LIGHT_12_SIZE='3320'
ARCHIVE_BASE_LIGHT_12_VERSION='2024.10.j.14-gog77218'


CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_MAIN_FILES_PERPETUAL='
bh_Data/StreamingAssets/edition/semper.txt'
CONTENT_GAME_MAIN_FILES_LIGHT='
bh_Data/*/DLC_HOL_*'

PKG_PARENT_ID="$GAME_ID"

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_PARENT'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_default

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
