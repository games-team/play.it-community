#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Aquatic Adventure of the Last Human
# send your bug reports to contact@dotslashplay.it
###

script_version=20250303.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='the-aquatic-adventure-of-the-last-human'
GAME_NAME='The Aquatic Adventure of the Last Human'

ARCHIVE_BASE_0='The Aquatic Adventure of the Last Human.zip'
ARCHIVE_BASE_0_MD5='4699f7e0abfde9ded6addc432226b36f'
ARCHIVE_BASE_0_EXTRACTOR='unar' # unzip fails on this archive
ARCHIVE_BASE_0_SIZE='830000'
ARCHIVE_BASE_0_VERSION='0.9.1-itch1'
ARCHIVE_BASE_0_URL='https://ycjy.itch.io/aquaticadventure'

CONTENT_PATH_DEFAULT='linux'
CONTENT_GAME_BIN_FILES='
TheAquaticAdventureOfTheLastHuman
game.ini'
CONTENT_GAME_DATA_FILES='
assets
game.unx'
CONTENT_DOC_DATA_FILES='
sound?credits.txt'

APP_MAIN_EXE='TheAquaticAdventureOfTheLastHuman'
APP_MAIN_ICON='assets/icon.png'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libcurl.so.4+CURL_OPENSSL_3
libdl.so.2
libgcc_s.so.1
libGL.so.1
libGLU.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
librt.so.1
libssl.so.1.0.0
libstdc++.so.6
libX11.so.6
libXext.so.6
libXrandr.so.2
libXxf86vm.so.1
libz.so.1'

# Work around Mesa-related startup crash
# cf. https://gitlab.freedesktop.org/mesa/mesa/issues/1310

APP_MAIN_PRERUN="$APP_MAIN_PRERUN"'
# Work around Mesa-related startup crash
# cf. https://gitlab.freedesktop.org/mesa/mesa/issues/1310
export radeonsi_sync_compile=true'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
rm "${PLAYIT_WORKDIR}/gamedata/mac.rar"
rm "${PLAYIT_WORKDIR}/gamedata/windows.rar"
ARCHIVE_INNER="${PLAYIT_WORKDIR}/gamedata/linux.rar"
archive_extraction 'ARCHIVE_INNER'
rm "$ARCHIVE_INNER"

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
