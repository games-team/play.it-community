#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Sébastien “Elzen” Dufromentel
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Dig
# send your bug reports to contact@dotslashplay.it
###

script_version=20240605.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='the-dig'
GAME_NAME='The Dig'

ARCHIVE_BASE_EN_0_NAME='the_dig_en_gog_2_20100.sh'
ARCHIVE_BASE_EN_0_MD5='0fd830de17757f78dc9865dd7c06c785'
ARCHIVE_BASE_EN_0_SIZE='760000'
ARCHIVE_BASE_EN_0_VERSION='1.0-gog20100'
ARCHIVE_BASE_EN_0_URL='https://www.gog.com/game/the_dig'

ARCHIVE_BASE_FR_0_NAME='the_dig_fr_gog_2_20100.sh'
ARCHIVE_BASE_FR_0_MD5='b4c2b87f0305a82bb0fa805b01b014f1'
ARCHIVE_BASE_FR_0_SIZE='760000'
ARCHIVE_BASE_FR_0_VERSION='1.0-gog20100'
ARCHIVE_BASE_FR_0_URL='https://www.gog.com/game/the_dig'

CONTENT_PATH_DEFAULT='data/noarch/data'
CONTENT_GAME_L10N_FILES='
language.bnd
digvoice.bun
video/sq1.san
video/sq2.san
video/sq3.san
video/sq4.san
video/sq8a.san
video/sq8b.san
video/sq8c.san
video/sq9.san
video/sq14sc14.san
video/sq14sc22.san
video/sq17.san
video/sq18b.san
video/sq18sc15.san
video/digtxt.trs'
CONTENT_GAME_MAIN_FILES='
video
digmusic.bun
dig.la0
dig.la1'
CONTENT_DOC_L10N_PATH_EN='data/noarch/docs/english'
CONTENT_DOC_L10N_PATH_FR='data/noarch/docs/french'
CONTENT_DOC_L10N_FILES='
*.pdf'

APP_MAIN_SCUMMID='scumm:dig'
APP_MAIN_ICON='../support/icon.png'

PACKAGES_LIST='
PKG_L10N
PKG_MAIN'

PKG_L10N_ID="${GAME_ID}-l10n"
PKG_L10N_ID_EN="${PKG_L10N_ID}-en"
PKG_L10N_ID_FR="${PKG_L10N_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_ID"
PKG_L10N_DESCRIPTION_EN='English localization'
PKG_L10N_DESCRIPTION_FR='French localization'

PKG_MAIN_DEPS="$PKG_L10N_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Convert all file paths to lowercase.
	tolower .
)

# Include game data

content_inclusion_icons 'PKG_MAIN'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_MAIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
