#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# World to the West
# send your bug reports to contact@dotslashplay.it
###

script_version=20221210.2

GAME_ID='world-to-the-west'
GAME_NAME='World to the West'

UNITY3D_NAME='WorldToTheWest'

ARCHIVE_BASE_1='world_to_the_west_en_1_3_0_23314.sh'
ARCHIVE_BASE_1_MD5='ac200556d642b1529526375b65642574'
ARCHIVE_BASE_1_TYPE='mojosetup'
ARCHIVE_BASE_1_SIZE='2100000'
ARCHIVE_BASE_1_VERSION='1.3.0-gog23314'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/world_to_the_west'

ARCHIVE_BASE_0='gog_world_to_the_west_2.0.0.2.sh'
ARCHIVE_BASE_0_MD5='dd393d192c7569cc19edb0e3fff7851a'
ARCHIVE_BASE_0_TYPE='mojosetup'
ARCHIVE_BASE_0_SIZE='2500000'
ARCHIVE_BASE_0_VERSION='1.0.1-gog2.0.0.2'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN32_FILES="
${UNITY3D_NAME}_Data/Mono/x86
${UNITY3D_NAME}_Data/Plugins/x86
${UNITY3D_NAME}.x86"
CONTENT_GAME_BIN64_FILES="
${UNITY3D_NAME}_Data/Mono/x86_64
${UNITY3D_NAME}_Data/Plugins/x86_64
${UNITY3D_NAME}.x86_64"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data"

PACKAGES_LIST='PKG_BIN32 PKG_BIN64 PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN32_ARCH='32'
PKG_BIN32_DEPS="$PKG_DATA_ID"
PKG_BIN32_DEPENDENCIES_LIBRARIES='
ld-linux-x86-64.so.2
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'
PKG_BIN32_DEPENDENCIES_LIBRARIES_0='
ld-linux-x86-64.so.2
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXrandr.so.2
libz.so.1'

PKG_BIN64_ARCH='64'
PKG_BIN64_DEPS="$PKG_BIN32_DEPS"
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN32_DEPENDENCIES_LIBRARIES"
PKG_BIN64_DEPENDENCIES_LIBRARIES_0="$PKG_BIN32_DEPENDENCIES_LIBRARIES_0"

# Load common functions

target_version='2.19'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Enforce the use of a common Unity3D name
	unity3d_name=$(unity3d_name)
	alternative_name='WTTW_GOG_1_3_0_Linux'
	for name_suffix in \
		'.x86' \
		'.x86_64' \
		'_Data'
	do
		if [ -e "${alternative_name}${name_suffix}" ]; then
			mv "${alternative_name}${name_suffix}" "${unity3d_name}${name_suffix}"
		fi
	done
)

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

for PKG in 'PKG_BIN32' 'PKG_BIN64'; do
	# shellcheck disable=SC2119
	launchers_write
done

# Build package

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
