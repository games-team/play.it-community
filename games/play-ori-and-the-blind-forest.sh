#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
# SPDX-FileCopyrightText: © 2021 Thomas Vasileiou
set -o errexit

###
# Ori and the Blind Forest
# send your bug reports to contact@dotslashplay.it
###

script_version=20241130.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='ori-and-the-blind-forest'
GAME_NAME='Ori and the Blind Forest'

ARCHIVE_BASE_1_NAME='setup_ori_and_the_blind_forest_definitive_edition_1.0_(28474).exe'
ARCHIVE_BASE_1_MD5='510199277acd79e434ceaccf9b28a71c'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_ori_and_the_blind_forest_definitive_edition_1.0_(28474)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='44382e02153ed54ab4ed182c252fe9f5'
ARCHIVE_BASE_1_PART2_NAME='setup_ori_and_the_blind_forest_definitive_edition_1.0_(28474)-2.bin'
ARCHIVE_BASE_1_PART2_MD5='c8c12108e5e7cebd46a7a78c995a5f1a'
ARCHIVE_BASE_1_SIZE='11000000'
ARCHIVE_BASE_1_VERSION='1.0-gog28474'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/ori_and_the_blind_forest_definitive_edition'

ARCHIVE_BASE_0_NAME='setup_ori_and_the_blind_forest_de_2.0.0.2.exe'
ARCHIVE_BASE_0_MD5='1dedfb0663ebbe82d051a62dc68149b5'
ARCHIVE_BASE_0_EXTRACTOR='innoextract'
ARCHIVE_BASE_0_EXTRACTOR_OPTIONS='--lowercase --gog'
ARCHIVE_BASE_0_PART1_NAME='setup_ori_and_the_blind_forest_de_2.0.0.2-1.bin'
ARCHIVE_BASE_0_PART1_MD5='d5ec4ea264c372a4fdd52b5ecbd9efe6'
ARCHIVE_BASE_0_PART1_EXTRACTOR='unar'
ARCHIVE_BASE_0_PART2_NAME='setup_ori_and_the_blind_forest_de_2.0.0.2-2.bin'
ARCHIVE_BASE_0_PART2_MD5='94c3d33701eadca15df9520de55f6f03'
ARCHIVE_BASE_0_SIZE='11000000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.0.0.2'

UNITY3D_NAME='oride'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='game'
CONTENT_GAME_DATA_ASSETS_FILES="
${UNITY3D_NAME}_data/*.assets
${UNITY3D_NAME}_data/*.assets.ress"

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Local Settings/Application Data/Ori and the Blind Forest DE'

APP_MAIN_OPTIONS='-force-d3d9'

PACKAGES_LIST='
PKG_BIN
PKG_DATA_ASSETS
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
PKG_DATA_DEPENDENCIES_SIBLINGS='
PKG_DATA_ASSETS'

PKG_DATA_ASSETS_ID="${PKG_DATA_ID}-assets"
PKG_DATA_ASSETS_DESCRIPTION="$PKG_DATA_DESCRIPTION - assets"
## Ensure easy upgrade from packages generated using pre-20211129.5 game script
PKG_DATA_ASSETS_PROVIDES='
ori-and-the-blind-forest-assets'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
