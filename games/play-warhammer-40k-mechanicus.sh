#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Warhammer 40k: Mechanicus
# send your bug reports to contact@dotslashplay.it
###

script_version=20230415.1

GAME_ID='warhammer-40k-mechanicus'
GAME_NAME='Warhammer 40,000: Mechanicus'

ARCHIVE_BASE_1='warhammer_40_000_mechanicus_1_4_10_0_63084.sh'
ARCHIVE_BASE_1_MD5='92e8a72016854efe19ebab5cc7906c1b'
ARCHIVE_BASE_1_SIZE='11000000'
ARCHIVE_BASE_1_VERSION='1.4.10.0-gog63084'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/warhammer_40000_mechanicus'

ARCHIVE_BASE_0='warhammer_40_000_mechanicus_1_4_6_1_47625.sh'
ARCHIVE_BASE_0_MD5='672029ff6ad1ff34946201ca4d423737'
ARCHIVE_BASE_0_SIZE='11000000'
ARCHIVE_BASE_0_VERSION='1.4.6.1-gog47625'

UNITY3D_NAME='Mechanicus'
UNITY3D_PLUGINS='
libfmod.so
libfmodL.so
libfmodstudioL.so
libfmodstudio.so
libgvraudio.so
libresonanceaudio.so
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN_FILES="
${UNITY3D_NAME}.x86_64
${UNITY3D_NAME}_Data/Mono/x86_64"
CONTENT_GAME_DATA_SHAREDASSETS_FILES="
${UNITY3D_NAME}_Data/sharedassets*"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data"

APP_MAIN_EXE="${UNITY3D_NAME}.x86_64"

PACKAGES_LIST='PKG_BIN PKG_DATA_SHAREDASSETS PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_DATA_SHAREDASSETS_ID="${PKG_DATA_ID}-sharedassets"
PKG_DATA_SHAREDASSETS_DESCRIPTION="$PKG_DATA_DESCRIPTION - shared assets"
PKG_DATA_DEPS="$PKG_DATA_DEPS $PKG_DATA_SHAREDASSETS_ID"

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

# Load common functions

target_version='2.23'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
