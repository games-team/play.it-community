#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Mopi
# SPDX-FileCopyrightText: © 2024 Fabien Givors <captnfab@debian-facile.org>
set -o errexit

###
# The Elder Scrolls 5 expansions:
# - Anniversary Upgrade
# send your bug reports to contact@dotslashplay.it
###

script_version=20240310.1

GAME_ID='the-elder-scrolls-5-skyrim'
GAME_NAME='The Elder Scrolls V: Skyrim'

EXPANSION_ID_UPGRADE='anniversary-upgrade'
EXPANSION_NAME_UPGRADE='Anniversary Upgrade'

# Archives

## Anniversary Upgrade

ARCHIVE_BASE_UPGRADE_0='setup_the_elder_scrolls_v_skyrim_anniversary_upgrade_1.6.659.0.8_(64bit)_(59094).exe'
ARCHIVE_BASE_UPGRADE_0_MD5='6ca1eaf78efe871db75aa2e870bf6154'
ARCHIVE_BASE_UPGRADE_0_TYPE='innosetup'
ARCHIVE_BASE_UPGRADE_0_PART1='setup_the_elder_scrolls_v_skyrim_anniversary_upgrade_1.6.659.0.8_(64bit)_(59094)-1.bin'
ARCHIVE_BASE_UPGRADE_0_PART1_MD5='44910bb282008aebb5c92cd97af12413'
ARCHIVE_BASE_UPGRADE_0_SIZE='4700000'
ARCHIVE_BASE_UPGRADE_0_VERSION='1.6.659.0.8-gog59094'
ARCHIVE_BASE_UPGRADE_0_URL='https://www.gog.com/en/game/the_elder_scrolls_v_skyrim_anniversary_upgrade'

ARCHIVE_BASE_UPGRADE_1='setup_the_elder_scrolls_v_skyrim_anniversary_upgrade_0.1.3905696_(64bit)_(70738).exe'
ARCHIVE_BASE_UPGRADE_1_MD5='411afce1d728f456ef0989dc866feca2'
ARCHIVE_BASE_UPGRADE_1_TYPE='innosetup'
ARCHIVE_BASE_UPGRADE_1_PART1='setup_the_elder_scrolls_v_skyrim_anniversary_upgrade_0.1.3905696_(64bit)_(70738)-1.bin'
ARCHIVE_BASE_UPGRADE_1_PART1_MD5='97d7706343def94098e725f876cb1a64'
ARCHIVE_BASE_UPGRADE_1_SIZE='4700000'
ARCHIVE_BASE_UPGRADE_1_VERSION='0.1.3905696-gog70738'
ARCHIVE_BASE_UPGRADE_1_URL='https://www.gog.com/en/game/the_elder_scrolls_v_skyrim_anniversary_upgrade'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_MAIN_FILES='
data'

PKG_MAIN_ID_UPGRADE="${GAME_ID}-${EXPANSION_ID_UPGRADE}"
PKG_MAIN_DESCRIPTION_UPGRADE="$EXPANSION_NAME_UPGRADE"
PKG_MAIN_DEPS="$GAME_ID"

# Load common functions

target_version='2.22'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Build packages

write_metadata
build_pkg

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

# Print instructions

print_instructions

exit 0
