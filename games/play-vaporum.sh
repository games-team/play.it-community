#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Vaporum
# send your bug reports to contact@dotslashplay.it
###

script_version=20231004.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='vaporum'
GAME_NAME='Vaporum'

ARCHIVE_BASE_0='vaporum_patch_14_27515.sh'
ARCHIVE_BASE_0_MD5='2c65dd89fbeec16740e1d57263606ffa'
ARCHIVE_BASE_0_SIZE='3200000'
ARCHIVE_BASE_0_VERSION='1.14-gog27515'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/vaporum'

UNITY3D_NAME='VaporumGame'
UNITY3D_PLUGINS='
libMouseLib.so
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data
Localization"
CONTENT_DOC_DATA_FILES='
BuildInfo.txt
PatchNotes.txt'

USER_PERSISTENT_DIRECTORIES='
Saves
Settings'

APP_MAIN_EXE="${UNITY3D_NAME}.exe"

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

PKG='PKG_BIN32'
# shellcheck disable=SC2119
launchers_write
PKG='PKG_BIN64'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
