#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Ghost of a Tale
# send your bug reports to contact@dotslashplay.it
###

script_version=20241105.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='ghost-of-a-tale'
GAME_NAME='Ghost of a Tale'

ARCHIVE_BASE_0_NAME='setup_ghost_of_a_tale_833_(28317).exe'
ARCHIVE_BASE_0_MD5='ce98494f39f3908d33ef46e438d94da4'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_ghost_of_a_tale_833_(28317)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='e5a2d7c834572c438c324a5104692afb'
ARCHIVE_BASE_0_SIZE='5000000'
ARCHIVE_BASE_0_VERSION='8.33-gog28317'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/ghost_of_a_tale'

UNITY3D_NAME='goat'

CONTENT_PATH_DEFAULT='.'
## TODO: Check if these .cmd scripts are required
CONTENT_GAME0_BIN_FILES='
goat_clearpreferences.cmd
goat_createreport.cmd'
CONTENT_GAME0_DATA_FILES='
icon.ico
remote_config_cache_production_worldwide.json'
## TODO: Check if the Steam configuration file is required
CONTENT_GAME1_DATA_FILES='
steam_appid.txt'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/My Games/Ghost of a Tale'
## TODO: Check if a virtual desktop is required
WINE_VIRTUAL_DESKTOP='auto'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
