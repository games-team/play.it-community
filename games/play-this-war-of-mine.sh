#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2019 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# This War of Mine
# send your bug reports to contact@dotslashplay.it
###

script_version=20240623.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='this-war-of-mine'
GAME_NAME='This War of Mine'

ARCHIVE_BASE_9_NAME='this_war_of_mine_6_0_8_a_34693.sh'
ARCHIVE_BASE_9_MD5='cc7afc1412ae1b92db5b992c2dd76818'
ARCHIVE_BASE_9_SIZE='2800000'
ARCHIVE_BASE_9_VERSION='6.0.8a-gog34693'
ARCHIVE_BASE_9_URL='https://www.gog.com/game/this_war_of_mine'

ARCHIVE_BASE_8_NAME='this_war_of_mine_6_0_8_34211.sh'
ARCHIVE_BASE_8_MD5='13d21027e6950b03946071bf4c06cb9e'
ARCHIVE_BASE_8_SIZE='2800000'
ARCHIVE_BASE_8_VERSION='6.0.8-gog34211'

ARCHIVE_BASE_7_NAME='this_war_of_mine_6_0_7_33947.sh'
ARCHIVE_BASE_7_MD5='f983657b4cd3bad71047d1cc62654b34'
ARCHIVE_BASE_7_SIZE='2800000'
ARCHIVE_BASE_7_VERSION='6.0.7-gog33947'

ARCHIVE_BASE_6_NAME='this_war_of_mine_6_0_6_32269.sh'
ARCHIVE_BASE_6_MD5='65b0332c4355046d09e5c49f1925cec9'
ARCHIVE_BASE_6_SIZE='2700000'
ARCHIVE_BASE_6_VERSION='6.0.6-32269'

ARCHIVE_BASE_5_NAME='this_war_of_mine_6_0_5_32074.sh'
ARCHIVE_BASE_5_MD5='8cb7bc50a58162242f215df36ae3ccdb'
ARCHIVE_BASE_5_SIZE='2700000'
ARCHIVE_BASE_5_VERSION='6.0.5-gog32074'

ARCHIVE_BASE_4_NAME='this_war_of_mine_6_0_0_s3788_a10718_31527.sh'
ARCHIVE_BASE_4_MD5='b317099aa7c0c0a4942845893e4f7386'
ARCHIVE_BASE_4_SIZE='2700000'
ARCHIVE_BASE_4_VERSION='6.0.0-gog31527'

ARCHIVE_BASE_3_NAME='this_war_of_mine_5_1_0_26027.sh'
ARCHIVE_BASE_3_MD5='8c9221653e6fc94a6898f5ef66a3325f'
ARCHIVE_BASE_3_SIZE='2200000'
ARCHIVE_BASE_3_VERSION='5.1.0-gog26027'

ARCHIVE_BASE_2_NAME='this_war_of_mine_4_0_1_25361.sh'
ARCHIVE_BASE_2_MD5='c6d96f0722a35821ea30500d8e7658d8'
ARCHIVE_BASE_2_SIZE='2200000'
ARCHIVE_BASE_2_VERSION='4.0.1-gog25361'

ARCHIVE_BASE_1_NAME='this_war_of_mine_4_0_1_24802.sh'
ARCHIVE_BASE_1_MD5='17daac7e70ee2c783b12114573cb7757'
ARCHIVE_BASE_1_SIZE='1500000'
ARCHIVE_BASE_1_VERSION='4.0.1-gog24802'

ARCHIVE_BASE_0_NAME='this_war_of_mine_en_4_0_0_29_01_2018_18230.sh'
ARCHIVE_BASE_0_MD5='165f4d6158425c3d2861c533f10b5713'
ARCHIVE_BASE_0_SIZE='1500000'
ARCHIVE_BASE_0_VERSION='4.0.0-gog18230'

CONTENT_PATH_DEFAULT='data/noarch/game'
## This build of libcurl.so.4 includes the required "CURL_OPENSSL_3" symbol.
CONTENT_LIBS_BIN_FILES='
libcurl.so.4'
CONTENT_GAME_BIN_FILES='
This War of Mine'
CONTENT_GAME_BIN_FILES_3='
KosovoLinux'
CONTENT_GAME_BIN_FILES_0='
TWOMLinux'
CONTENT_GAME_DATA_GOG_FILES='
gog.idx
gog.dat'
CONTENT_GAME_DATA_FILES='
CustomContent
LocalizationPacks
WorkshopData
animations.idx
animations.dat
common.idx
common.dat
localizations.idx
localizations.dat
scenes.idx
scenes.dat
sounds.idx
sounds.dat
templates.idx
templates.dat
textures-s3.idx
textures-s3.dat
videos.idx
videos.dat
voices.idx
voices.dat
svnrev.txt
*.str'

APP_MAIN_EXE='This War of Mine'
APP_MAIN_EXE_3='KosovoLinux'
APP_MAIN_EXE_0='TWOMLinux'
APP_MAIN_ICON='../support/icon.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA_GOG
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_DATA_GOG_ID="${PKG_DATA_ID}-gog"
PKG_DATA_GOG_DESCRIPTION="$PKG_DATA_DESCRIPTION - GOG-specific files"
PKG_DATA_DEPS="${PKG_DATA_DEPS:-} $PKG_DATA_GOG_ID"

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libgcc_s.so.1
libGL.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# The game binary looks for libOpenAL.so instead of libopenal.so.1

case "$(option_value 'package')" in
	('arch'|'gentoo'|'egentoo')
		## Unversioned libopenal.so is already provided by the base library packages on Arch Linux and Gentoo.
	;;
	('deb')
		path_libraries_system=$(
			set_current_package 'PKG_BIN'
			path_libraries_system
		)
		library_source="${path_libraries_system}/libopenal.so.1"
		library_destination="$(package_path 'PKG_BIN')$(path_libraries)/libOpenAL.so"
		mkdir --parents "$(dirname "$library_destination")"
		ln --symbolic "$library_source" "$library_destination"
	;;
esac

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
