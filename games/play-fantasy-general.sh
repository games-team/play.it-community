#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2019 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Fantasy General
# send your bug reports to contact@dotslashplay.it
###

script_version=20231108.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='fantasy-general'
GAME_NAME='Fantasy General'

ARCHIVE_BASE_EN_0_NAME='gog_fantasy_general_2.0.0.8.sh'
ARCHIVE_BASE_EN_0_MD5='59b86b9115ae013d2e23a8b4b7b771fd'
ARCHIVE_BASE_EN_0_SIZE='260000'
ARCHIVE_BASE_EN_0_VERSION='1.0-gog2.0.0.8'
ARCHIVE_BASE_EN_0_URL='https://www.gog.com/game/fantasy_general'

ARCHIVE_BASE_FR_0_NAME='gog_fantasy_general_french_2.0.0.8.sh'
ARCHIVE_BASE_FR_0_MD5='1b188304b4cca838e6918ca6e2d9fe2b'
ARCHIVE_BASE_FR_0_SIZE='240000'
ARCHIVE_BASE_FR_0_VERSION='1.0-gog2.0.0.8'
ARCHIVE_BASE_FR_0_URL='https://www.gog.com/game/fantasy_general'

CONTENT_PATH_DEFAULT='data/noarch/data'
CONTENT_GAME_MAIN_FILES='
dat
exe
music
saves
shp
sound
game.gog
game.ins
*.bat'
CONTENT_DOC_MAIN_FILES='
*.txt'
CONTENT_DOC0_MAIN_PATH="${CONTENT_PATH_DEFAULT}/../docs"
CONTENT_DOC0_MAIN_FILES='
Fantasy General - Manual.pdf'

USER_PERSISTENT_DIRECTORIES='
saves'
USER_PERSISTENT_FILES='
dat/prefs.dat'

GAME_IMAGE='game.ins'

APP_MAIN_TYPE='dosbox'
APP_MAIN_EXE='exe/barena.exe'
APP_MAIN_ICON='../support/icon.png'
## Run the game binary from its parent directory.
APP_MAIN_PRERUN='# Run the game binary from its parent directory
APP_DIR=$(dirname "$APP_EXE")
APP_EXE=$(basename "$APP_EXE")
'
APP_MAIN_DOSBOX_PRERUN='
cd "$APP_DIR"
'

PKG_MAIN_ID="$GAME_ID"
PKG_MAIN_ID_EN="${GAME_ID}-en"
PKG_MAIN_ID_FR="${GAME_ID}-fr"
PKG_MAIN_PROVIDES="
$PKG_MAIN_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Convert all file paths to lowercase.
	tolower .
)

# Include game data

# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
