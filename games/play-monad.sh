#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Monad
# send your bug reports to contact@dotslashplay.it
###

script_version=20241107.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='monad'
GAME_NAME='Monad'

ARCHIVE_BASE_0_NAME='monad-win64.zip'
ARCHIVE_BASE_0_MD5='43f7b632cbd1d622859305b4b7d4839e'
ARCHIVE_BASE_0_SIZE='15000'
ARCHIVE_BASE_0_VERSION='1.0-itch.2019.07.31'
ARCHIVE_BASE_0_URL='https://shinyogre.itch.io/monad'

CONTENT_PATH_DEFAULT='monad-win'
CONTENT_GAME_MAIN_FILES='
monad.exe
game.ico
*.dll'
CONTENT_DOC_MAIN_FILES='
license.txt'

APP_MAIN_EXE='monad.exe'
APP_MAIN_ICON='game.ico'

PKG_MAIN_ARCH='64'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
