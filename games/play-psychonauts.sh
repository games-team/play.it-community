#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Psychonauts
# send your bug reports to contact@dotslashplay.it
###

script_version=20240624.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='psychonauts'
GAME_NAME='Psychonauts'

ARCHIVE_BASE_0_NAME='gog_psychonauts_2.0.0.4.sh'
ARCHIVE_BASE_0_MD5='7fc85f71494ff5d37940e9971c0b0c55'
ARCHIVE_BASE_0_SIZE='5200000'
ARCHIVE_BASE_0_VERSION='1.04-gog2.0.0.4'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/psychonauts'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN_FILES='
Psychonauts
DisplaySettings.ini'
CONTENT_GAME_DATA_FILES='
icon.bmp
PsychonautsData2.pkg
psychonauts.png
WorkResource'
CONTENT_DOC_DATA_PATH='data/noarch/docs'
CONTENT_DOC_DATA_FILES='
Psychonauts Manual Win.pdf'
CONTENT_DOC0_DATA_PATH="${CONTENT_PATH_DEFAULT}/Documents"
## FIXME: An explicit list of files should be set.
CONTENT_DOC0_DATA_FILES='
*'

USER_PERSISTENT_FILES='
DisplaySettings.ini
psychonauts.ini'

APP_MAIN_EXE='Psychonauts'
APP_MAIN_ICONS_LIST='APP_MAIN_ICON_PNG APP_MAIN_ICON_BMP'
APP_MAIN_ICON_PNG='psychonauts.png'
APP_MAIN_ICON_BMP='icon.bmp'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'
## Ensure easy upgrade from packages generated using pre-20211124.2 game script.
PKG_DATA_PROVIDES="${PKG_DATA_PROVIDES:-}
psychonauts-sounds"

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libgcc_s.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
libSDL-1.2.so.0
libstdc++.so.6'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
