#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Haven
# send your bug reports to contact@dotslashplay.it
###

script_version=20241124.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='haven'
GAME_NAME='Haven'

ARCHIVE_BASE_2_NAME='setup_haven_v1.1.302_official_(64bit)_(54386).exe'
ARCHIVE_BASE_2_MD5='8dca64cf157ae335dab462d52253aeeb'
ARCHIVE_BASE_2_TYPE='innosetup'
ARCHIVE_BASE_2_PART1_NAME='setup_haven_v1.1.302_official_(64bit)_(54386)-1.bin'
ARCHIVE_BASE_2_PART1_MD5='03a08ea5c6b1a80b7a2f869da91d2df8'
ARCHIVE_BASE_2_PART2_NAME='setup_haven_v1.1.302_official_(64bit)_(54386)-2.bin'
ARCHIVE_BASE_2_PART2_MD5='04e4c87ee647fd0511fdda7ff47dc787'
ARCHIVE_BASE_2_SIZE='5900000'
ARCHIVE_BASE_2_VERSION='1.1.302-gog54386'
ARCHIVE_BASE_2_URL='https://www.gog.com/game/haven'

ARCHIVE_BASE_1_NAME='setup_haven_v1.1.296_official_(64bit)_(53728).exe'
ARCHIVE_BASE_1_MD5='f5b9d5f71a2def217dc9e0f632a04360'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_haven_v1.1.296_official_(64bit)_(53728)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='598b4c5592f8f3aa4ab95c49cdf80fe0'
ARCHIVE_BASE_1_PART2_NAME='setup_haven_v1.1.296_official_(64bit)_(53728)-2.bin'
ARCHIVE_BASE_1_PART2_MD5='00990daa83f70d6440bb4ccabc07981a'
ARCHIVE_BASE_1_SIZE='5900000'
ARCHIVE_BASE_1_VERSION='1.1.296-gog53728'

ARCHIVE_BASE_0_NAME='setup_haven_v1.1.258-rc_official_(64bit)_(49981).exe'
ARCHIVE_BASE_0_MD5='1770f0efd829dc486cc37242085518d3'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_haven_v1.1.258-rc_official_(64bit)_(49981)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='5aec89c6383ab1ee8f1371a9130e01f5'
ARCHIVE_BASE_0_SIZE='4700000'
ARCHIVE_BASE_0_VERSION='1.1.258-gog49981'

UNITY3D_NAME='haven'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME0_DATA_RELATIVE_PATH='app'
CONTENT_GAME0_DATA_FILES="
${UNITY3D_NAME}_data"

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/TheGameBakers/Haven'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
video/quicktime, variant=(string)iso'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
