#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Infinium Strike
# send your bug reports to contact@dotslashplay.it
###

script_version=20231102.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='infinium-strike'
GAME_NAME='Infinium Strike'

ARCHIVE_BASE_1_NAME='gog_infinium_strike_2.1.0.3.sh'
ARCHIVE_BASE_1_MD5='c19f2032aae3011dc38cc85e7153fb44'
ARCHIVE_BASE_1_SIZE='2500000'
ARCHIVE_BASE_1_VERSION='1.0.5-gog2.1.0.3'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/infinium_strike'

ARCHIVE_BASE_0_NAME='gog_infinium_strike_2.1.0.2.sh'
ARCHIVE_BASE_0_MD5='57725aad8ba419d80788412f8f33f030'
ARCHIVE_BASE_0_SIZE='2500000'
ARCHIVE_BASE_0_VERSION='1.0.5-gog2.1.0.2'

UNITY3D_NAME='InfiniumStrike'
## TODO: Check if the Steam libraries can be dropped.
UNITY3D_PLUGINS='
libCSteamworks.so
libsteam_api.so
ScreenSelector.so'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_DOC_DATA_FILES='
InfiniumStrikeReadMe.txt'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXrandr.so.2'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Drop unused 64-bit libraries.
	rm --recursive \
		"$(unity3d_name)_Data/Mono/x86_64" \
		"$(unity3d_name)_Data/Plugins/x86_64"
)

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
