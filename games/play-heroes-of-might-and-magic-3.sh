#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2015 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Heroes of Might and Magic 3
# send your bug reports to contact@dotslashplay.it
###

script_version=20241215.2

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='heroes-of-might-and-magic-3'
GAME_NAME='Heroes of Might and Magic Ⅲ'

ARCHIVE_BASE_EN_1_NAME='setup_heroes_of_might_and_magic_3_complete_4.0_(3.2)_gog_0.1_(77075).exe'
ARCHIVE_BASE_EN_1_MD5='b1c0f5b5d83c17bede6bdd97762b648a'
ARCHIVE_BASE_EN_1_TYPE='innosetup'
ARCHIVE_BASE_EN_1_PART1_NAME='setup_heroes_of_might_and_magic_3_complete_4.0_(3.2)_gog_0.1_(77075)-1.bin'
ARCHIVE_BASE_EN_1_PART1_MD5='0f0aa941fdd99f78a1b58ff5e5d9eb74'
ARCHIVE_BASE_EN_1_SIZE='1028132'
ARCHIVE_BASE_EN_1_VERSION='4.0-gog77075'
ARCHIVE_BASE_EN_1_URL='https://www.gog.com/game/heroes_of_might_and_magic_3_complete_edition'

ARCHIVE_BASE_FR_1_NAME='setup_heroes_of_might_and_magic_3_complete_4.0_(3.2)_gog_0.1_(french)_(77075).exe'
ARCHIVE_BASE_FR_1_MD5='940d3b2dfc0c31427ec66d68058d73c5'
ARCHIVE_BASE_FR_1_TYPE='innosetup'
ARCHIVE_BASE_FR_1_PART1_NAME='setup_heroes_of_might_and_magic_3_complete_4.0_(3.2)_gog_0.1_(french)_(77075)-1.bin'
ARCHIVE_BASE_FR_1_PART1_MD5='c1933c4f7009b3b9729160b9021d7863'
ARCHIVE_BASE_FR_1_SIZE='1024066'
ARCHIVE_BASE_FR_1_VERSION='4.0-gog77075'
ARCHIVE_BASE_FR_1_URL='https://www.gog.com/game/heroes_of_might_and_magic_3_complete_edition'

ARCHIVE_BASE_RU_1_NAME='setup_heroes_of_might_and_magic_3_complete_4.0_(3.2)_gog_0.1_(russian)_(77075).exe'
ARCHIVE_BASE_RU_1_MD5='77817c7c60d5980f5fcebd55e7dcb08c'
ARCHIVE_BASE_RU_1_TYPE='innosetup'
ARCHIVE_BASE_RU_1_PART1_NAME='setup_heroes_of_might_and_magic_3_complete_4.0_(3.2)_gog_0.1_(russian)_(77075)-1.bin'
ARCHIVE_BASE_RU_1_PART1_MD5='32af9457f179c3f4bf3b10f76cfde71f'
ARCHIVE_BASE_RU_1_SIZE='1000278'
ARCHIVE_BASE_RU_1_VERSION='4.0-gog77075'
ARCHIVE_BASE_RU_1_URL='https://www.gog.com/game/heroes_of_might_and_magic_3_complete_edition'

ARCHIVE_BASE_EN_0_NAME='setup_heroes_of_might_and_magic_3_complete_4.0_(28740).exe'
ARCHIVE_BASE_EN_0_MD5='8dcd6c4a8c72c65a6920665e28245c57'
ARCHIVE_BASE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_EN_0_PART1_NAME='setup_heroes_of_might_and_magic_3_complete_4.0_(28740)-1.bin'
ARCHIVE_BASE_EN_0_PART1_MD5='4285d54f27c40e815905c7069b6f9f84'
ARCHIVE_BASE_EN_0_SIZE='1100000'
ARCHIVE_BASE_EN_0_VERSION='4.0-gog28740'

ARCHIVE_BASE_FR_0_NAME='setup_heroes_of_might_and_magic_3_complete_4.0_(french)_(28740).exe'
ARCHIVE_BASE_FR_0_MD5='be4b59590146299dbe77bda7a4ea4178'
ARCHIVE_BASE_FR_0_TYPE='innosetup'
ARCHIVE_BASE_FR_0_PART1_NAME='setup_heroes_of_might_and_magic_3_complete_4.0_(french)_(28740)-1.bin'
ARCHIVE_BASE_FR_0_PART1_MD5='88b71e0fd44e5be1ad6791e74120c61c'
ARCHIVE_BASE_FR_0_SIZE='1100000'
ARCHIVE_BASE_FR_0_VERSION='4.0-gog28740'

ARCHIVE_BASE_RU_0_NAME='setup_heroes_of_might_and_magic_3_complete_4.0_(russian)_(28740).exe'
ARCHIVE_BASE_RU_0_MD5='33018e8355f59b9e532d7429e1aaeeae'
ARCHIVE_BASE_RU_0_TYPE='innosetup'
ARCHIVE_BASE_RU_0_PART1_NAME='setup_heroes_of_might_and_magic_3_complete_4.0_(russian)_(28740)-1.bin'
ARCHIVE_BASE_RU_0_PART1_MD5='5d409ee2e2b3ec32fa9838ca1d601e02'
ARCHIVE_BASE_RU_0_SIZE='1100000'
ARCHIVE_BASE_RU_0_VERSION='4.0-gog28740'

ARCHIVE_BASE_OLDTEMPLATE_EN_1_NAME='setup_homm_3_complete_4.0_(10665).exe'
ARCHIVE_BASE_OLDTEMPLATE_EN_1_MD5='0c97452fc4da4e8811173f21df873fab'
ARCHIVE_BASE_OLDTEMPLATE_EN_1_TYPE='innosetup'
ARCHIVE_BASE_OLDTEMPLATE_EN_1_SIZE='1100000'
ARCHIVE_BASE_OLDTEMPLATE_EN_1_VERSION='4.0-gog10665'

ARCHIVE_BASE_OLDTEMPLATE_FR_1_NAME='setup_homm_3_complete_french_4.0_(10665).exe'
ARCHIVE_BASE_OLDTEMPLATE_FR_1_MD5='6c3ee33a531bd0604679581ab267d8a3'
ARCHIVE_BASE_OLDTEMPLATE_FR_1_TYPE='innosetup'
ARCHIVE_BASE_OLDTEMPLATE_FR_1_SIZE='1100000'
ARCHIVE_BASE_OLDTEMPLATE_FR_1_VERSION='4.0-gog10665'

ARCHIVE_BASE_OLDTEMPLATE_EN_0_NAME='setup_homm3_complete_2.0.0.16.exe'
ARCHIVE_BASE_OLDTEMPLATE_EN_0_MD5='263d58f8cc026dd861e9bbcadecba318'
ARCHIVE_BASE_OLDTEMPLATE_EN_0_TYPE='innosetup'
ARCHIVE_BASE_OLDTEMPLATE_EN_0_PART1_NAME='patch_heroes_of_might_and_magic_3_complete_2.0.1.17.exe'
ARCHIVE_BASE_OLDTEMPLATE_EN_0_PART1_MD5='815b9c097cd57d0e269beb4cc718dad3'
ARCHIVE_BASE_OLDTEMPLATE_EN_0_SIZE='1100000'
ARCHIVE_BASE_OLDTEMPLATE_EN_0_VERSION='3.0-gog2.0.1.17'

ARCHIVE_BASE_OLDTEMPLATE_FR_0_NAME='setup_homm3_complete_french_2.1.0.20.exe'
ARCHIVE_BASE_OLDTEMPLATE_FR_0_MD5='ca8e4726acd7b5bc13c782d59c5a459b'
ARCHIVE_BASE_OLDTEMPLATE_FR_0_TYPE='innosetup'
ARCHIVE_BASE_OLDTEMPLATE_FR_0_SIZE='1100000'
ARCHIVE_BASE_OLDTEMPLATE_FR_0_VERSION='3.0-gog2.1.0.20'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_OLDTEMPLATE='app'
CONTENT_GAME_BIN_WINE_FILES='
mp3dec.asi
binkw32.dll
ifc20.dll
ifc21.dll
mcp.dll
mss32.dll
smackw32.dll
h3blade.exe
h3bmaped.exe
h3camped.exe
h3ccmped.exe
h3maped.exe
heroes3.exe'
CONTENT_GAME0_BIN_WINE_PATH_OLDTEMPLATE_EN_0='tmp'
CONTENT_GAME0_BIN_WINE_FILES_OLDTEMPLATE_EN_0='
heroes3.exe'
CONTENT_GAME_DATA_FILES='
data
maps
mp3'
CONTENT_DOC_DATA_FILES='
eula
*.cnt
*.hlp
*.pdf
*.txt'

USER_PERSISTENT_DIRECTORIES='
config
games
maps
random_maps'
USER_PERSISTENT_FILES='
data/*.lod'

APP_MAIN_TYPE_BIN_VCMI='custom'
APP_MAIN_EXE_BIN_WINE='heroes3.exe'
APP_MAIN_ICON='heroes3.exe'

APP_EDITOR_MAP_ID="${GAME_ID}-map-editor"
APP_EDITOR_MAP_EXE='h3maped.exe'
APP_EDITOR_MAP_NAME="$GAME_NAME - Map editor"

APP_EDITOR_CAMPAIGN_ID="${GAME_ID}-campaign-editor"
APP_EDITOR_CAMPAIGN_EXE='h3ccmped.exe'
APP_EDITOR_CAMPAIGN_NAME="$GAME_NAME - Campaign editor"

PACKAGES_LIST='
PKG_BIN_VCMI
PKG_BIN_WINE
PKG_DATA'

PKG_BIN_ID="$GAME_ID"
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_BIN_VCMI_ID="${PKG_BIN_ID}-vcmi"
PKG_BIN_VCMI_PROVIDES="
$PKG_BIN_ID"
PKG_BIN_VCMI_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_VCMI_DEPENDENCIES_COMMANDS='
vcmilauncher'

PKG_BIN_WINE_ID="${PKG_BIN_ID}-wine"
PKG_BIN_WINE_ID_EN="${PKG_BIN_WINE_ID}-en"
PKG_BIN_WINE_ID_FR="${PKG_BIN_WINE_ID}-fr"
PKG_BIN_WINE_ID_RU="${PKG_BIN_WINE_ID}-ru"
PKG_BIN_WINE_ID_OLDTEMPLATE_EN="$PKG_BIN_WINE_ID_EN"
PKG_BIN_WINE_ID_OLDTEMPLATE_FR="$PKG_BIN_WINE_ID_FR"
PKG_BIN_WINE_PROVIDES="
$PKG_BIN_ID"
PKG_BIN_WINE_ARCH='32'
PKG_BIN_WINE_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_ID_EN="${PKG_DATA_ID}-en"
PKG_DATA_ID_FR="${PKG_DATA_ID}-fr"
PKG_DATA_ID_RU="${PKG_DATA_ID}-ru"
PKG_DATA_ID_OLDTEMPLATE_EN="$PKG_DATA_ID_EN"
PKG_DATA_ID_OLDTEMPLATE_FR="$PKG_DATA_ID_FR"
PKG_DATA_PROVIDES="
$PKG_DATA_ID"
PKG_DATA_DESCRIPTION='data'

# Allow to skip intro video on first launch + set default settings

registry_dump_settings_file='registry-dumps/default-settings.reg'
registry_dump_settings_content='Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\Software\New World Computing\Heroes of Might and Magic® III\1.0]
"Animate SpellBook"=dword:00000001
"Autosave"=dword:00000001
"Bink Video"=dword:00000001
"Blackout Computer"=dword:00000000
"Combat Army Info Level"=dword:00000000
"Combat Auto Creatures"=dword:00000001
"Combat Auto Spells"=dword:00000001
"Combat Ballista"=dword:00000001
"Combat Catapult"=dword:00000001
"Combat First Aid Tent"=dword:00000001
"Combat Shade Level"=dword:00000000
"Combat Speed"=dword:00000000
"Computer Walk Speed"=dword:00000003
"First Time"=dword:00000000
"Last Music Volume"=dword:00000005
"Last Sound Volume"=dword:00000005
"Main Game Full Screen"=dword:00000001
"Main Game Show Menu"=dword:00000001
"Main Game X"=dword:0000000a
"Main Game Y"=dword:0000000a
"Move Reminder"=dword:00000001
"Music Volume"=dword:00000005
"Network Default Name"="Player"
"Quick Combat"=dword:00000000
"Show Combat Grid"=dword:00000000
"Show Combat Mouse Hex"=dword:00000000
"Show Intro"=dword:00000001
"Show Route"=dword:00000001
"Sound Volume"=dword:00000005
"Test Blit"=dword:00000000
"Test Decomp"=dword:00000000
"Test Read"=dword:00000000
"Town Outlines"=dword:00000001
"Video Subtitles"=dword:00000001
"Walk Speed"=dword:00000002
"Window Scroll Speed"=dword:00000001'
CONTENT_GAME_BIN_WINE_FILES="${CONTENT_GAME_BIN_WINE_FILES:-}
$registry_dump_settings_file"
APP_REGEDIT="${APP_REGEDIT:-} $registry_dump_settings_file"
SCRIPT_DEPS="${SCRIPT_DEPS:-} iconv"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
## For old gog.com English archive, get the patched .exe
case "$(current_archive)" in
	('ARCHIVE_OLDTEMPLATE_EN_0')
		archive_extraction 'SOURCE_ARCHIVE_PART1'
	;;
esac
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Allow to skip intro video on first launch + set default settings
	mkdir --parents "$(dirname "$registry_dump_settings_file")"
	printf '%s' "$registry_dump_settings_content" | \
		iconv --from-code=UTF-8 --to-code=UTF-16 --output="$registry_dump_settings_file"

	# Fix the ability to run the game through WINE, by reverting a GOG patch
	# This replaces a call to "xdd.dll" with the original call to "ddraw.dll".
	# cf. https://www.gog.com/forum/heroes_of_might_and_magic_series/if_you_cant_get_homm3_to_work_after_the_latest_update_on_linux_with_wine_read_this/post1
	case "$(current_archive)" in
		('ARCHIVE_BASE_'??'_1')
			sed --in-place 's/xdd\.dll../ddraw.dll/' 'heroes3.exe'
		;;
	esac
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN_WINE'
custom_launcher() {
	launcher_headers
	cat <<- EOF
	GAME_PATH="$(path_game_data)"
	EOF
	cat <<- 'EOF'
	VCMI_DATA="${XDG_DATA_HOME:="${HOME}/.local/share"}/vcmi"

	for directory in data maps mp3; do
	    if [ ! -e "${VCMI_DATA}/${directory}" ]; then
	        mkdir --parents "$VCMI_DATA"
	        cp --recursive --remove-destination --symbolic-link "${GAME_PATH}/${directory}" "$VCMI_DATA"
	    fi
	done

	vcmilauncher

	exit 0
	EOF
}
launchers_generation 'PKG_BIN_VCMI'

# Build packages

packages_generation
printf '\n'
printf 'VCMI:'
print_instructions 'PKG_DATA' 'PKG_BIN_VCMI'
printf 'WINE:'
print_instructions 'PKG_DATA' 'PKG_BIN_WINE'

# Clean up

working_directory_cleanup

exit 0
