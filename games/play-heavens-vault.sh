#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Heaven's Vault
# send your bug reports to contact@dotslashplay.it
###

script_version=20240807.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='heavens-vault'
GAME_NAME='Heavenʼs Vault'

ARCHIVE_BASE_0_NAME='setup_heavens_vault_1.11_(64bit)_(31262).exe'
ARCHIVE_BASE_0_MD5='73476bbfccb0a97ec68fd403d23c85f0'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_heavens_vault_1.11_(64bit)_(31262)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='2f1dc13ab09f8fc592e1b8482ea5b0f4'
ARCHIVE_BASE_0_SIZE='4300000'
ARCHIVE_BASE_0_VERSION='1.11-gog31262'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/heavens_vault'

UNITY3D_NAME='heavensvault'

CONTENT_PATH_DEFAULT='.'

WINE_PERSISTENT_DIRECTORIES="
users/\${USER}/AppData/LocalLow/Inkle Ltd/Heaven's Vault"

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Use a Unity3D name not including tricky characters
	unity3d_name=$(unity3d_name)
	mv "heaven's vault.exe" "${unity3d_name}.exe"
	mv "heaven's vault_data" "${unity3d_name}_data"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
