#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Retrace
# send your bug reports to contact@dotslashplay.it
###

script_version=20240617.2

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='retrace'
GAME_NAME='Retrace'

ARCHIVE_BASE_0_NAME='retrace-win.zip'
ARCHIVE_BASE_0_MD5='a953b09f1e1226ac24998ea68cacfe0e'
ARCHIVE_BASE_0_SIZE='190000'
ARCHIVE_BASE_0_VERSION='1.0-itch'
ARCHIVE_BASE_0_URL='https://spiderlilystudios.itch.io/retrace'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
Retrace.exe
options.ini'
## TODO: Move the fonts to the standard system path.
CONTENT_GAME_DATA_FILES='
data
fonts
audiogroup?.dat
data.win'

USER_PERSISTENT_FILES='
options.ini'

APP_MAIN_EXE='Retrace.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
