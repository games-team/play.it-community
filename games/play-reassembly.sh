#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 BetaRays
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Reassembly
# send your bug reports to contact@dotslashplay.it
###

script_version=20250303.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='reassembly'
GAME_NAME='Reassembly'

ARCHIVE_BASE_GOG_0='reassembly_2019_4_04_28550.sh'
ARCHIVE_BASE_GOG_0_MD5='640cc7849af45fb221f349b9d901b9ab'
ARCHIVE_BASE_GOG_0_SIZE='120000'
ARCHIVE_BASE_GOG_0_VERSION='2019.03.30-gog28550'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/reassembly'

ARCHIVE_BASE_HUMBLE_0='anisopteragames_Reassembly_2019_04_11.tar.gz'
ARCHIVE_BASE_HUMBLE_0_MD5='ae516da186f0a2c6799eb8059a51337d'
ARCHIVE_BASE_HUMBLE_0_SIZE='120000'
ARCHIVE_BASE_HUMBLE_0_VERSION='2019.03.30-humble1'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/reassembly'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='Reassembly'
CONTENT_LIBS_BIN_PATH_GOG="${CONTENT_PATH_DEFAULT_GOG}/linux/linux64"
CONTENT_LIBS_BIN_PATH_HUMBLE="${CONTENT_PATH_DEFAULT_HUMBLE}/linux/linux64"
CONTENT_LIBS_BIN_FILES='
libGLEW.so.1.13'
CONTENT_GAME_BIN_FILES='
linux/ReassemblyRelease64'
CONTENT_GAME_DATA_FILES='
data
linux/reassembly_icon.png'
CONTENT_DOC_DATA_FILES='
*.txt'

APP_MAIN_EXE='linux/ReassemblyRelease64'
APP_MAIN_ICON='linux/reassembly_icon.png'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libcurl.so.4+CURL_OPENSSL_3
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
libSDL2-2.0.so.0
libSDL2_image-2.0.so.0
libSDL2_ttf-2.0.so.0
libstdc++.so.6
libvorbisfile.so.3
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
