#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Windosill
# send your bug reports to contact@dotslashplay.it
###

script_version=20231108.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='windosill'
GAME_NAME='Windosill'

ARCHIVE_BASE_0_NAME='Windosill'
ARCHIVE_BASE_0_MD5='853ab5f3ea6b2691718466f0479b867f'
ARCHIVE_BASE_0_SIZE='19000'
ARCHIVE_BASE_0_VERSION='1.0-itch'
ARCHIVE_BASE_0_URL='https://vectorpark.itch.io/windosill'

APP_MAIN_EXE='Windosill'

PKG_MAIN_ARCH='32'
PKG_MAIN_DEPENDENCIES_LIBRARIES='
libatk-1.0.so.0
libcairo.so.2
libc.so.6
libdl.so.2
libfontconfig.so.1
libfreetype.so.6
libgdk_pixbuf-2.0.so.0
libgdk-x11-2.0.so.0
libglib-2.0.so.0
libgmodule-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libnspr4.so
libnss3.so
libnssutil3.so
libpango-1.0.so.0
libpangocairo-1.0.so.0
libpangoft2-1.0.so.0
libplc4.so
libplds4.so
libpthread.so.0
librt.so.1
libsmime3.so
libssl3.so
libX11.so.6
libXcursor.so.1
libXext.so.6
libXrender.so.1
libXt.so.6'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Include game data

install -D --mode=755 \
	--target-directory="$(package_path 'PKG_MAIN')$(path_game_data)" \
	"$(archive_path "$(current_archive)")"

# Write launchers

# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
