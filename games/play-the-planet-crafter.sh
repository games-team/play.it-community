#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Fabien Givors <captnfab@debian-facile.org>
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Planet Crafter
# send your bug reports to contact@dotslashplay.it
###

script_version=20241106.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='the-planet-crafter'
GAME_NAME='The Planet Crafter'

ARCHIVE_BASE_0_NAME='setup_the_planet_crafter_v1.005_(73160).exe'
ARCHIVE_BASE_0_MD5='60ece555d812bed78d38f571dc2ddaf8'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_the_planet_crafter_v1.005_(73160)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='d0435d2e30c97cf7f1d5fa24e4290e3a'
ARCHIVE_BASE_0_PART2_NAME='setup_the_planet_crafter_v1.005_(73160)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='22f21c99887225b814a2d2e55eb007c4'
ARCHIVE_BASE_0_SIZE='8000000'
ARCHIVE_BASE_0_VERSION='1.005'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/the_planet_crafter'

UNITY3D_NAME='planet crafter'

CONTENT_PATH_DEFAULT='.'

WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/MijuGames/Planet Crafter'
## TODO: Check is these fonts are required with current WINE
WINE_WINETRICKS_VERBS='corefonts'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
video/quicktime, variant=(string)iso'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
