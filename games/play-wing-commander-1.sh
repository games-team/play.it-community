#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 BetaRays
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Wing Commander 1
# send your bug reports to contact@dotslashplay.it
###

script_version=20240505.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='wing-commander-1'
GAME_NAME='Wing Commander'

ARCHIVE_BASE_1_NAME='setup_wing_commander_1.0_(28045).exe'
ARCHIVE_BASE_1_MD5='850542ba9a543378a4894156b41ce511'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='45000'
ARCHIVE_BASE_1_VERSION='1.0-gog28045'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/wing_commander_1_2'

ARCHIVE_BASE_0='setup_wing_commander_2.1.0.18.exe'
ARCHIVE_BASE_0_MD5='a4a3a355489e66bcecd34d1d9041ebb5'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='49000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.1.0.18'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME_MAIN_FILES='
wc.exe
sm2.exe
gamedat
*.cfg'
CONTENT_GAME0_MAIN_PATH='__support/save'
CONTENT_GAME0_MAIN_FILES='
gamedat'
CONTENT_DOC_MAIN_FILES='
*.pdf'

USER_PERSISTENT_DIRECTORIES='
gamedat'
USER_PERSISTENT_FILES='
*.cfg'

APP_MAIN_EXE='wc.exe'
APP_MAIN_ICON='app/goggame-1207662643.ico'
APP_MAIN_DOSBOX_PRERUN='config -set cpu cycles=fixed 4000
loadfix -1'

APP_SM2_ID="${GAME_ID}-the-secret-missions-2"
APP_SM2_NAME="$GAME_NAME - The Secret Missions 2 - Crusade"
APP_SM2_EXE='sm2.exe'
APP_SM2_ICON='app/goggame-1207662643.ico'
APP_SM2_DOSBOX_PRERUN='config -set cpu cycles=fixed 4000
loadfix -1'

## Easier upgrade from packages generated with pre-20190224.1 scripts.
PKG_MAIN_PROVIDES="${PKG_MAIN_PROVIDES:-}
wing-commander"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
