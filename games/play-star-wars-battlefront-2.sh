#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Star Wars Battlefront 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20231004.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='star-wars-battlefront-2'
GAME_NAME='Star Wars Battlefront Ⅱ'

ARCHIVE_BASE_1_NAME='setup_star_wars_battlefront_ii_1.1_(10935).exe'
ARCHIVE_BASE_1_MD5='0b74c1d7cced6d6bde95605661b67673'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_PART1_NAME='setup_star_wars_battlefront_ii_1.1_(10935)-1.bin'
ARCHIVE_BASE_1_PART1_MD5='c7ad88e5eb89b3701dad3a6973d9918b'
ARCHIVE_BASE_1_PART2_NAME='setup_star_wars_battlefront_ii_1.1_(10935)-2.bin'
ARCHIVE_BASE_1_PART2_MD5='3a52d98aa43cb78fd661a656e5cf96a2'
ARCHIVE_BASE_1_SIZE='10000000'
ARCHIVE_BASE_1_VERSION='1.1-gog10935'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/star_wars_battlefront_ii'

ARCHIVE_BASE_0_NAME='setup_star_wars_battlefront_ii_1.1_multiplayer_update_2_(17606).exe'
ARCHIVE_BASE_0_MD5='f482ec251067336d3b8211774b4c44f6'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_star_wars_battlefront_ii_1.1_multiplayer_update_2_(17606)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='c34b41f594e55b1522d8826f19cf958f'
ARCHIVE_BASE_0_PART2_NAME='setup_star_wars_battlefront_ii_1.1_multiplayer_update_2_(17606)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='c9423f3983c67575c1c531e0d18e6a0f'
ARCHIVE_BASE_0_SIZE='11000000'
ARCHIVE_BASE_0_VERSION='1.1-gog17606'

CONTENT_PATH_DEFAULT='gamedata'
CONTENT_GAME_BIN_FILES='
*.dll
*.exe'
CONTENT_GAME_MOVIES_FILES='
data/_lvl_pc/movies'
CONTENT_GAME_DATA_FILES='
data'
CONTENT_DOC_DATA_PATH='.'
CONTENT_DOC_DATA_FILES='
*.pdf'

USER_PERSISTENT_DIRECTORIES='
savegames'
USER_PERSISTENT_FILES='
*.ini'

APP_MAIN_EXE='battlefrontii.exe'

PACKAGES_LIST='PKG_BIN PKG_MOVIES PKG_DATA'

PKG_MOVIES_ID="${GAME_ID}-movies"
PKG_MOVIES_DESCRIPTION='movies'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_MOVIES_ID $PKG_DATA_ID"

## TODO: Setting up a WINE virtual desktop on first launch might prevent display problems on some setups.

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
ARCHIVE_OPTIONAL_ICONS_0_NAME='star-wars-battlefront-2_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_0_MD5='322275011d37ac219f1c06c196477fa4'
ARCHIVE_OPTIONAL_ICONS_0_URL='https://downloads.dotslashplay.it/games/star-wars-battlefront-2'
archive_initialize_optional \
	'ARCHIVE_ICONS' \
	'ARCHIVE_OPTIONAL_ICONS'
## TODO: The library should provide a function to check if a given optional archive has been provided.
if [ -n "$(archive_path 'ARCHIVE_ICONS' 2>/dev/null || true)" ]; then
	archive_extraction 'ARCHIVE_ICONS'
fi

# Include game data

## TODO: The library should provide a function to check if a given optional archive has been provided.
if [ -n "$(archive_path 'ARCHIVE_ICONS' 2>/dev/null || true)" ]; then
	CONTENT_ICONS_DATA_PATH='.'
	CONTENT_ICONS_DATA_FILES='
	16x16
	32x32'
	content_inclusion 'ICONS_DATA' 'PKG_DATA' "$(path_icons)"
fi
content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
