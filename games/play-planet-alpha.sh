#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Planet Alpha
# send your bug reports to contact@dotslashplay.it
###

script_version=20240611.2

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='planet-alpha'
GAME_NAME='Planet Alpha'

ARCHIVE_BASE_0_NAME='setup_planet_alpha_1.0.5.1_(64bit)_(29703).exe'
ARCHIVE_BASE_0_MD5='34accc9442958f022da8a71965bf2fcd'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_planet_alpha_1.0.5.1_(64bit)_(29703)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='8e8b478e391a70f6e45e97b6def77155'
ARCHIVE_BASE_0_SIZE='8700000'
ARCHIVE_BASE_0_VERSION='1.0.5.1-gog29703'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/planet_alpha'

UNREALENGINE4_NAME='planetalpha'

CONTENT_PATH_DEFAULT='.'

APP_MAIN_EXE="${UNREALENGINE4_NAME}/binaries/win64/${UNREALENGINE4_NAME}-win64-shipping.exe"
APP_MAIN_ICON_WRESTOOL_OPTIONS='--type=14 --name=123'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
