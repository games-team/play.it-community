#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Stories: The Path of Destinies
# send your bug reports to contact@dotslashplay.it
###

script_version=20241106.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='stories-the-path-of-destinies'
GAME_NAME='Stories: The Path of Destinies'

ARCHIVE_BASE_1_NAME='setup_stories_the_path_of_destinies_update_4_(25956).exe'
ARCHIVE_BASE_1_MD5='61f632db33297ca2b178d4383545732c'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_VERSION='1.0-gog25956'
ARCHIVE_BASE_1_SIZE='2000000'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/stories_the_path_of_destinies'

ARCHIVE_BASE_0_NAME='setup_stories_-_the_path_of_destinies_0.0.13825_(16929).exe'
ARCHIVE_BASE_0_MD5='6f81dbadddbb4b30b4edda9ced9ddef8'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_VERSION='0.0.13825-gog16929'
ARCHIVE_BASE_0_SIZE='1700000'

UNREALENGINE4_NAME='stories'

CONTENT_PATH_DEFAULT='.'
CONTENT_PATH_DEFAULT_0='app'
CONTENT_GAME0_BIN_FILES='
language_setup.exe
language_setup.ini'
CONTENT_GAME0_DATA_FILES='
language_setup.png'

APP_MAIN_EXE="${UNREALENGINE4_NAME}/binaries/win64/${UNREALENGINE4_NAME}.exe"
## The --name=101 wrestool option, default for UE4 games, should not be used here
APP_MAIN_ICON_WRESTOOL_OPTIONS='--type=14'

## TODO: Check if the shipped language selector could be dropped
APP_LANGUAGE_ID="${GAME_ID}-language-selector"
APP_LANGUAGE_NAME="$GAME_NAME - Language Selector"
APP_LANGUAGE_CAT='Settings'
APP_LANGUAGE_EXE='language_setup.exe'
APP_LANGUAGE_ICON='language_setup.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
