#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Space Tail
# send your bug reports to contact@dotslashplay.it
###

script_version=20231111.3

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='space-tail'
GAME_NAME='Space Tail: Every Journey Leads Home'

# Archives

## Native Linux build

ARCHIVE_BASE_LINUX_0_NAME='space_tail_every_journey_leads_home_1_0_2r9_61068.sh'
ARCHIVE_BASE_LINUX_0_MD5='c267a2e1ef94cde68487e41cd3a4bbcf'
ARCHIVE_BASE_LINUX_0_SIZE='7700000'
ARCHIVE_BASE_LINUX_0_VERSION='1.0.2r9-gog61068'
ARCHIVE_BASE_LINUX_0_URL='https://www.gog.com/game/space_tail_every_journey_leads_home'

## Windows build
### This is useful for users of nVIDIA non-free graphics driver,
### as the native Linux build suffers from rendering issues with this driver.

ARCHIVE_BASE_WINDOWS_0_NAME='setup_space_tail_every_journey_leads_home_1.0.2r9_(64bit)_(61065).exe'
ARCHIVE_BASE_WINDOWS_0_MD5='5683d3493606fb5a2a60d9416e79c28d'
ARCHIVE_BASE_WINDOWS_0_TYPE='innosetup'
ARCHIVE_BASE_WINDOWS_0_PART1_NAME='setup_space_tail_every_journey_leads_home_1.0.2r9_(64bit)_(61065)-1.bin'
ARCHIVE_BASE_WINDOWS_0_PART1_MD5='3183c4b2d5db7c462a266e41ad683720'
ARCHIVE_BASE_WINDOWS_0_SIZE='7800000'
ARCHIVE_BASE_WINDOWS_0_VERSION='1.0.2r9-gog61065'
ARCHIVE_BASE_WINDOWS_0_URL='https://www.gog.com/game/space_tail_every_journey_leads_home'


UNITY3D_NAME_LINUX='Space Tail'
UNITY3D_NAME_WINDOWS='space tail'
UNITY3D_PLUGINS='
lib_burst_generated.so
libfmodstudio.so
libresonanceaudio.so'
## The game crashes on launch if the Steam library is not available.
UNITY3D_PLUGINS="${UNITY3D_PLUGINS:-}
libsteam_api.so"

CONTENT_PATH_DEFAULT_LINUX='data/noarch/game'
CONTENT_PATH_DEFAULT_WINDOWS='.'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Enjoy Studio/Space Tail'

# Packages

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_PROVIDES="
$PKG_DATA_ID"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ID="$GAME_ID"
PKG_BIN_PROVIDES="
$PKG_BIN_ID"
PKG_BIN_ARCH='64'

## Native Linux build

PKG_DATA_ID_LINUX="${GAME_ID}-linux-data"

PKG_BIN_ID_LINUX="${GAME_ID}-linux"
PKG_BIN_DEPS_LINUX="$PKG_DATA_ID_LINUX"
PKG_BIN_DEPENDENCIES_LIBRARIES_LINUX='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6'

## Windows build

PKG_DATA_ID_WINDOWS="${GAME_ID}-windows-data"

PKG_BIN_ID_WINDOWS="${GAME_ID}-windows"
PKG_BIN_DEPS_WINDOWS="$PKG_DATA_ID_WINDOWS"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Native Linux build - Link libresonanceaudio.so in the game data path as the game engine fails to find it otherwise

case "$(current_archive)" in
	('ARCHIVE_BASE_LINUX_'*)
		library_destination="$(package_path 'PKG_BIN')$(path_game_data)/$(unity3d_name)_Data/Plugins"
		mkdir --parents "$library_destination"
		ln --symbolic "$(path_libraries)/libresonanceaudio.so" "$library_destination"
	;;
esac

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
