#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Lego Harry Potter: Years 1-4
# send your bug reports to contact@dotslashplay.it
###

script_version=20230930.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='lego-harry-potter-years-1-4'
GAME_NAME='Lego Harry Potter: Years 1-4'

ARCHIVE_BASE_0='setup_lego_harry_potter_1-4_1.0_(17966).exe'
ARCHIVE_BASE_0_MD5='21ddbc82a225687a69044e9baee7109b'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1='setup_lego_harry_potter_1-4_1.0_(17966)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='ab78edad26acef08eb3a656dfbdf476e'
ARCHIVE_BASE_0_PART2='setup_lego_harry_potter_1-4_1.0_(17966)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='0ebb7b7a5772245258e53092c25a4708'
ARCHIVE_BASE_0_SIZE='6300000'
ARCHIVE_BASE_0_VERSION='1.0.0389-gog17966'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/lego_harry_potter_years_14'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
legoharrypotter.exe
language_setup.exe
language_setup.ini
language_setup.png'
CONTENT_GAME_DATA_FILES='
game.dat
game1.dat
game2.dat
game3.dat
game4.dat
game5.dat
game6.dat'
CONTENT_DOC_DATA_PATH="${CONTENT_PATH_DEFAULT}/docs"
CONTENT_DOC_DATA_FILES='
*'

APP_MAIN_EXE='legoharrypotter.exe'

APP_LANGUAGE_ID="${GAME_ID}-language-setup"
APP_LANGUAGE_NAME="$GAME_NAME - Language setup"
APP_LANGUAGE_CAT='Settings'
APP_LANGUAGE_EXE='language_setup.exe'
APP_LANGUAGE_ICON='language_setup.png'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
