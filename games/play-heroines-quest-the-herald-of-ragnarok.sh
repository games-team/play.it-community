#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Fabien Givors <captnfab@debian-facile.org>
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Heroine's Quest: The Herald of Ragnarok
# send your bug reports to contact@dotslashplay.it
###

script_version=20241216.3

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='heroines-quest-the-herald-of-ragnarok'
GAME_NAME='Heroine’s Quest: The Herald of Ragnarok'

ARCHIVE_BASE_FIREFLOWER_0_NAME='Heroines_Quest_Linux.zip'
ARCHIVE_BASE_FIREFLOWER_0_MD5='a421c0e15731510130d26343942f2553'
ARCHIVE_BASE_FIREFLOWER_0_SIZE='487786'
ARCHIVE_BASE_FIREFLOWER_0_VERSION='1.2.3-fireflower'
ARCHIVE_BASE_FIREFLOWER_0_URL='https://fireflowergames.com/products/heroines-quest'

ARCHIVE_BASE_GOG_0_NAME='heroine_s_quest_the_herald_of_ragnarok_1_2_9_2_68339.sh'
ARCHIVE_BASE_GOG_0_MD5='a9cfa6ca34e28fa4b06d7d6fc565421d'
ARCHIVE_BASE_GOG_0_SIZE='500000'
ARCHIVE_BASE_GOG_0_VERSION='1.2.9.2-gog68339'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/heroines_quest_the_herald_of_ragnarok'

CONTENT_PATH_DEFAULT_FIREFLOWER='.'
CONTENT_PATH_DEFAULT_GOG='data/noarch/game/data'
CONTENT_GAME_MAIN_FILES='
acsetup.cfg
*.0??
*.ags
*.exe
*.tra
*.vox'
CONTENT_DOC_MAIN_FILES='
licenses
history.txt'
CONTENT_DOC0_MAIN_RELATIVE_PATH_GOG='..'
CONTENT_DOC0_MAIN_FILES='
*.pdf'

APP_MAIN_SCUMMID='ags:heroinesquest'
APP_MAIN_ICON_FIREFLOWER="Heroine's Quest.exe"
APP_MAIN_ICON_GOG='../../support/icon.png'

## Ensure easy updates from packages generated with pre-20241216.2 game scripts
PKG_MAIN_PROVIDES="${PKG_MAIN_PROVIDES:-}
heroines-quest-the-herald-of-ragnarok-data"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
