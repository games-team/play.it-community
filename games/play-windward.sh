#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Windward
# send your bug reports to contact@dotslashplay.it
###

script_version=20250303.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='windward'
GAME_NAME='Windward'

## This game used to be sold by GOG,
## but is no longer available.
ARCHIVE_BASE_GOG_2='gog_windward_2.36.0.40.sh'
ARCHIVE_BASE_GOG_2_MD5='6afbdcfda32a6315139080822c30396a'
ARCHIVE_BASE_GOG_2_SIZE='130000'
ARCHIVE_BASE_GOG_2_VERSION='20170617.0-gog2.36.0.40'

ARCHIVE_BASE_GOG_1='gog_windward_2.35.0.39.sh'
ARCHIVE_BASE_GOG_1_MD5='12fffaf6f405f36d2f3a61b4aaab89ba'
ARCHIVE_BASE_GOG_1_SIZE='130000'
ARCHIVE_BASE_GOG_1_VERSION='20160707.0-gog2.35.0.39'

ARCHIVE_BASE_GOG_0='gog_windward_2.35.0.38.sh'
ARCHIVE_BASE_GOG_0_MD5='f5ce09719bf355e48d2eac59b84592d1'
ARCHIVE_BASE_GOG_0_SIZE='120000'
ARCHIVE_BASE_GOG_0_VERSION='20160707-gog2.35.0.38'

## The DRM-free build of Windard is no longer available from Humble Store,
## they only sell Steam keys now.
ARCHIVE_BASE_HUMBLE_1='WindwardLinux_HB_1505248588.zip'
ARCHIVE_BASE_HUMBLE_1_MD5='9ea99157d13ae53905757f2fb3ab5b54'
ARCHIVE_BASE_HUMBLE_1_SIZE='130000'
ARCHIVE_BASE_HUMBLE_1_VERSION='20170617.0-humble170912'

ARCHIVE_BASE_HUMBLE_0='WindwardLinux_HB.zip'
ARCHIVE_BASE_HUMBLE_0_MD5='f2d1a9a91055ecb6c5ce1bd7e3ddd803'
ARCHIVE_BASE_HUMBLE_0_SIZE='130000'
ARCHIVE_BASE_HUMBLE_0_VERSION='20160707-humble1'

UNITY3D_NAME='Windward'
UNITY3D_PLUGINS='
ScreenSelector.so'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='Windward'
CONTENT_PATH_DEFAULT_HUMBLE_0='.'

FAKE_HOME_PERSISTENT_DIRECTORIES='
Documents/Windward'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libGLU.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game icon

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
