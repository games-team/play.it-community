#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Jacek Szafarkiewicz
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Commandos 3
# send your bug reports to contact@dotslashplay.it
###

script_version=20231110.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='commandos-3'
GAME_NAME='Commandos 3: Destination Berlin'

ARCHIVE_BASE_0_NAME='setup_commandos_3_-_destination_berlin_1.42_hotfix2_(25143).exe'
ARCHIVE_BASE_0_MD5='2fa1ad6e7c7e918bdaa1adee5bb3a0ec'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='2100000'
ARCHIVE_BASE_0_VERSION='1.42-gog25143'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/commandos_2_3'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
*.dll
*.exe'
CONTENT_GAME_DATA_FILES='
data
output
directplay.cmd
*.pck'
CONTENT_DOC_DATA_FILES='
readme.rtf
eula.txt
support.txt
*.pdf'

USER_PERSISTENT_DIRECTORIES='
output'

APP_MAIN_EXE='commandos3.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

## FIXME: This game should be run in a WINE virtual desktop.

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Delete unwanted files
	rm --recursive \
		'__redist' \
		'commonappdata' \
		'tmp'
)

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
