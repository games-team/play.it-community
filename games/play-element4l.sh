#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 BetaRays
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Element4l
# send your bug reports to contact@dotslashplay.it
###

script_version=20240528.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='element4l'
GAME_NAME='Element4l'

## This DRM-free archive is no longer available from playism.com,
## as their store turned into yet another Steam keys reseller.
ARCHIVE_BASE_0_NAME='Element4l-WIN-1.2.3.zip'
ARCHIVE_BASE_0_MD5='04f761ddf4e9e9b14cad67ae32c1598e'
ARCHIVE_BASE_0_SIZE='320000'
ARCHIVE_BASE_0_VERSION='1.2.3-playism'

UNITY3D_NAME='element4l'

CONTENT_PATH_DEFAULT='Update-1.2.3-WIN'

WINE_REGEDIT_PERSISTENT_KEYS='
HKEY_CURRENT_USER\Software\I-Illusions\element4l'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
