#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Coma
# send your bug reports to contact@dotslashplay.it
###

script_version=20241107.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='the-coma-1'
GAME_NAME='The Coma'

ARCHIVE_BASE_0_NAME='the_coma_recut_2_1_5_gog_40905.sh'
ARCHIVE_BASE_0_MD5='05d35196c4a5ca8dce3e223d364fa139'
ARCHIVE_BASE_0_SIZE='1400000'
ARCHIVE_BASE_0_VERSION='2.1.5-gog40905'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/the_coma_recut'

UNITY3D_NAME='TheComaRecut'

CONTENT_PATH_DEFAULT='data/noarch/game'
## FIXME: The Unity3D plugins to include should be listed with UNITY3D_PLUGINS
CONTENT_GAME0_BIN_FILES="
${UNITY3D_NAME}_Name/Plugins"

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
## TODO: The dependencies list should be completed
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libstdc++.so.6
libgtk-x11-2.0.so.0
libz.so.1
libgdk_pixbuf-2.0.so.0
libgobject-2.0.so.0
libglib-2.0.so.0'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
