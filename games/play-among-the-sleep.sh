#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Mopi
# SPDX-FileCopyrightText: © 2021 Anna Lea
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Among the Sleep
# send your bug reports to contact@dotslashplay.it
###

script_version=20241111.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='among-the-sleep'
GAME_NAME='Among the Sleep'

ARCHIVE_BASE_GOG_0_NAME='among_the_sleep_en_3_0_1_16406.sh'
ARCHIVE_BASE_GOG_0_MD5='de6e5fc26d7d5954782a6e98966f34cf'
ARCHIVE_BASE_GOG_0_SIZE='3800000'
ARCHIVE_BASE_GOG_0_VERSION='3.0.1-gog16406'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/among_the_sleep'

ARCHIVE_BASE_HUMBLE_0_NAME='Among_the_Sleep_Linux.zip'
ARCHIVE_BASE_HUMBLE_0_MD5='0d7296eb48bfebeb9a37827ee94bd34c'
ARCHIVE_BASE_HUMBLE_0_SIZE='2900000'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.0-humble1'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/among-the-sleep'

UNITY3D_NAME_GOG='Among The Sleep'
UNITY3D_NAME_HUMBLE='Among the Sleep'
UNITY3D_PLUGINS='
ScreenSelector.so'
## TODO: Check if the Steam libraries are required
UNITY3D_PLUGINS="$UNITY3D_PLUGINS
libCSteamworks.so
libsteam_api.so"

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='.'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXrandr.so.2'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
