#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Fox and Flock
# send your bug reports to contact@dotslashplay.it
###

script_version=20240329.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='fox-and-flock'
GAME_NAME='Fox & Flock'

ARCHIVE_BASE_32BIT_0_NAME='ff-linux-32.zip'
ARCHIVE_BASE_32BIT_0_MD5='6e0dee8044a05ee5bfef8a9885f3f456'
ARCHIVE_BASE_32BIT_0_SIZE='320000'
ARCHIVE_BASE_32BIT_0_VERSION='1.0.0-humble1'
ARCHIVE_BASE_32BIT_0_URL='https://www.humblebundle.com/store/fox-flock'

ARCHIVE_BASE_64BIT_0_NAME='ff-linux-64.zip'
ARCHIVE_BASE_64BIT_0_MD5='d0c5d87d2415f4eb49c591612e877f42'
ARCHIVE_BASE_64BIT_0_SIZE='310000'
ARCHIVE_BASE_64BIT_0_VERSION='1.0.0-humble1'
ARCHIVE_BASE_64BIT_0_URL='https://www.humblebundle.com/store/fox-flock'

CONTENT_PATH_DEFAULT='Fox and Flock'
CONTENT_LIBS_BIN_FILES='
libffmpegsumo.so'
CONTENT_GAME_BIN_FILES='
foxandflock
nw.pak'
CONTENT_GAME_DATA_FILES='
dist
locales
icudtl.dat
package.json'

APP_MAIN_EXE='foxandflock'
APP_MAIN_ICONS_LIST='APP_MAIN_ICON_128 APP_MAIN_ICON_256'
APP_MAIN_ICON_SOURCE='dist/img/icon.png'
APP_MAIN_ICON_128='dist/img/icon_128.png'
APP_MAIN_ICON_256='dist/img/icon_256.png'
## Convert the shipped icon to standard resolutions.
SCRIPT_DEPS="${SCRIPT_DEPS:-} convert"

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH_32BIT='32'
PKG_BIN_ARCH_64BIT='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libasound.so.2
libcairo.so.2
libc.so.6
libdbus-1.so.3
libdl.so.2
libexpat.so.1
libfontconfig.so.1
libfreetype.so.6
libgcc_s.so.1
libgconf-2.so.4
libgdk_pixbuf-2.0.so.0
libgdk-x11-2.0.so.0
libgio-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libnotify.so.4
libnspr4.so
libnss3.so
libnssutil3.so
libpango-1.0.so.0
libpangocairo-1.0.so.0
libplc4.so
libpthread.so.0
librt.so.1
libsmime3.so
libstdc++.so.6
libX11.so.6
libXcomposite.so.1
libXcursor.so.1
libXdamage.so.1
libXext.so.6
libXfixes.so.3
libXi.so.6
libXrandr.so.2
libXrender.so.1
libXtst.so.6'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Link the shipped library into the game directory to avoid a crash on launch

library_path_source="$(path_libraries)/libffmpegsumo.so"
library_path_destination="$(package_path 'PKG_BIN')$(path_game_data)"
mkdir --parents "$library_path_destination"
ln --symbolic "$library_path_source" "$library_path_destination"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Delete unwanted files.
	find . -name '*.rsrc' -delete

	## Convert the shipped icon to standard resolutions.
	icon_source=$(icon_path 'APP_MAIN_ICON_SOURCE')
	for icon_resolution in '128x128' '256x256'; do
		icon_destination=$(printf '%s' "$icon_source" | sed "s/\.png$/_${icon_resolution%x*}&/")
		convert "$icon_source" -resize "$icon_resolution" "$icon_destination"
	done
	rm "$icon_source"
)

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
