#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Wandersong
# send your bug reports to contact@dotslashplay.it
###

script_version=20240806.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='wandersong'
GAME_NAME='Wandersong'

ARCHIVE_BASE_0_NAME='wandersong-drmfree.zip'
ARCHIVE_BASE_0_MD5='27851a4ba540505f11e122f4c9a686b5'
ARCHIVE_BASE_0_SIZE='2000000'
ARCHIVE_BASE_0_VERSION='1.0-itch1'
ARCHIVE_BASE_0_URL='https://banov.itch.io/wandersong'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
GMFile.dll
GMIni.dll
wandersong.exe'
CONTENT_GAME_DATA_FILES='
audioData
levels
palettes
*.dat
*.ogg'
CONTENT_DOC_DATA_FILES='
ws_credits'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/Local/wandersong'

APP_MAIN_EXE='wandersong.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
