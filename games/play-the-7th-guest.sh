#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Anna Lea
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The 7th Guest
# send your bug reports to contact@dotslashplay.it
###

script_version=20240619.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='the-7th-guest'
GAME_NAME='The 7th Guest'

ARCHIVE_BASE_0_NAME='the_7th_guest_en_patch_3_21683.sh'
ARCHIVE_BASE_0_MD5='eb2cd92e06e0594297e1d38e3552df6d'
ARCHIVE_BASE_0_SIZE='780000'
ARCHIVE_BASE_0_VERSION='1.3-gog21683'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/the_7th_guest_25th_anniversary_edition'

CONTENT_PATH_DEFAULT='data/noarch/data'
CONTENT_GAME_MAIN_FILES='
sphinx.fnt
fat.*
*.gjd
*.grv
*.rl'
CONTENT_DOC_MAIN_PATH='data/noarch/docs'
CONTENT_DOC_MAIN_FILES='
*.pdf
*.txt'

APP_MAIN_SCUMMID='groovie:t7g'
APP_MAIN_ICON='../support/icon.png'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	## Convert all file paths to lowercase.
	tolower .
)

# Include game data

content_inclusion_icons
content_inclusion_default

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
