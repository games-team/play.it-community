#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2019 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Akalabeth
# send your bug reports to contact@dotslashplay.it
###

script_version=20230626.1

GAME_ID='akalabeth'
GAME_NAME='Akalabeth: World of Doom'

ARCHIVE_BASE_0='gog_akalabeth_world_of_doom_2.0.0.3.sh'
ARCHIVE_BASE_0_MD5='11a770db592af2ac463e6cdc453b555b'
ARCHIVE_BASE_0_SIZE='13000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.0.0.3'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/akalabeth_world_of_doom'

CONTENT_PATH_DEFAULT='data/noarch/data'
CONTENT_GAME_MAIN_FILES='
*'
CONTENT_DOC_MAIN_PATH='data/noarch/docs'
CONTENT_DOC_MAIN_FILES='
*.pdf'

APP_MAIN_EXE='aklabeth.exe'
APP_MAIN_ICON='../support/icon.png'

# Load common functions

target_version='2.23'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"

	# Convert all paths to lowercase.
	tolower .
)

# Include game icons

# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
