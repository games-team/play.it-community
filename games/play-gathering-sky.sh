#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2019 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Gathering Sky
# send your bug reports to contact@dotslashplay.it
###

script_version=20230812.1

GAME_ID='gathering-sky'
GAME_NAME='Gathering Sky'

ARCHIVE_BASE_0='GatheringSky_Linux_64bit.zip'
ARCHIVE_BASE_0_MD5='c590edce835070a1ac2ae47ac620dc48'
ARCHIVE_BASE_0_SIZE='1200000'
ARCHIVE_BASE_0_VERSION='1.0-humble1'
ARCHIVE_BASE_0_URL='https://www.humblebundle.com/store/gathering-sky'

CONTENT_PATH_DEFAULT='packr/linux/GatheringSky'
CONTENT_GAME_DATA_FILES='
desktop-0.1.jar'
CONTENT_GAME_BIN_SHIPPED_FILES='
config.json
GatheringSky
jre'

# Launchers

## Common

APPLICATIONS_LIST='APP_MAIN'

## Ensure settings can be stored
APP_MAIN_PRERUN='# Ensure settings can be stored
mkdir --parents "$HOME/.prefs"
'

APP_MAIN_ICONS_LIST='APP_MAIN_ICON_16 APP_MAIN_ICON_32 APP_MAIN_ICON_128'
APP_MAIN_ICON_16='../../../images/Icon_16.png'
APP_MAIN_ICON_32='../../../images/Icon_32.png'
APP_MAIN_ICON_128='../../../images/Icon_128.png'

## Using system-provided Java

APP_MAIN_TYPE_BIN_SYSTEM='java'
APP_MAIN_JAVA_OPTIONS_BIN_SYSTEM='-Xmx1G'
APP_MAIN_EXE_BIN_SYSTEM='desktop-0.1.jar'

## Using shipped binaries

APP_MAIN_EXE_BIN_SHIPPED='GatheringSky'

# Packages

## Common

PACKAGES_LIST='PKG_BIN_SHIPPED PKG_BIN_SYSTEM PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ID="$GAME_ID"

## Using system-provided Java

PKG_BIN_SYSTEM_ID="${PKG_BIN_ID}-bin-system"
PKG_BIN_SYSTEM_PROVIDES="
$PKG_BIN_ID"
PKG_BIN_SYSTEM_DEPS="$PKG_DATA_ID"
PKG_BIN_SYSTEM_DESCRIPTION='Using system-provided Java'

## Using shipped binaries

PKG_BIN_SHIPPED_ARCH='64'
PKG_BIN_SHIPPED_ID="${PKG_BIN_ID}-bin-shipped"
PKG_BIN_SHIPPED_PROVIDES="
$PKG_BIN_ID"
PKG_BIN_SHIPPED_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libm.so.6
libpthread.so.0
libstdc++.so.6
libthread_db.so.1'
PKG_BIN_SYSTEM_DESCRIPTION='Using shipped binaries'

# Load common functions

target_version='2.24'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

## The archive includes the following entry, triggering an error during decompression:
## ?rwxr--r--  2.0 unx        0 bl defN 14-May-31 03:43 /
archive_extraction 'SOURCE_ARCHIVE' 2>/dev/null || true
ARCHIVE_INNER="${PLAYIT_WORKDIR}/gamedata/GatheringSky.tar.gz"
archive_extraction 'ARCHIVE_INNER'
rm "$ARCHIVE_INNER"

# Include game icon

PKG='PKG_DATA'
ARCHIVE_JAR="${PLAYIT_WORKDIR}/gamedata/$(content_path_default)/desktop-0.1.jar"
ARCHIVE_JAR_TYPE='zip'
archive_extraction 'ARCHIVE_JAR'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

for PKG in 'PKG_BIN_SHIPPED' 'PKG_BIN_SYSTEM'; do
	# shellcheck disable=SC2119
	launchers_write
done

# Build packages

packages_generation

# Print instructions

case "${LANG%_*}" in
	('fr')
		message='Utilisation des binaires fournis par %s :'
		bin_shipped='les développeurs'
		bin_system='le système'
	;;
	('en'|*)
		message='Using binaries provided by %s:'
		bin_shipped='the developers'
		bin_system='the system'
	;;
esac
printf '\n'
printf "$message" "$bin_shipped"
print_instructions 'PKG_BIN_SHIPPED' 'PKG_DATA'
printf "$message" "$bin_system"
print_instructions 'PKG_BIN_SYSTEM' 'PKG_DATA'

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
