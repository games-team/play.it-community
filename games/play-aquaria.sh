#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Aquaria
# send your bug reports to contact@dotslashplay.it
###

script_version=20240603.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='aquaria'
GAME_NAME='Aquaria'

ARCHIVE_BASE_1_NAME='gog_aquaria_2.0.0.5.sh'
ARCHIVE_BASE_1_MD5='4235398debdf268f233881fade9e0530'
ARCHIVE_BASE_1_SIZE='240000'
ARCHIVE_BASE_1_VERSION='1.1.3-gog2.0.0.5'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/aquaria'

ARCHIVE_BASE_0_NAME='gog_aquaria_2.0.0.4.sh'
ARCHIVE_BASE_0_MD5='1810de0d68028c6ec01d33181086180d'
ARCHIVE_BASE_0_SIZE='280000'
ARCHIVE_BASE_0_VERSION='1.1.3-gog2.0.0.4'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME_BIN_FILES='
config
aquaria
*.xml'
CONTENT_GAME_DATA_FILES='
_mods
data
gfx
mus
scripts
sfx
vox
aquaria.png'
CONTENT_DOC_DATA_FILES='
docs
*.txt'

## TODO: Check that these path must be writable.
USER_PERSISTENT_DIRECTORIES='
config'
USER_PERSISTENT_FILES='
*.xml'

FAKE_HOME_PERSISTENT_DIRECTORIES='
.Aquaria'

APP_MAIN_EXE='aquaria'
APP_MAIN_ICON='aquaria.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libgcc_s.so.1
libm.so.6
libopenal.so.1
libSDL-1.2.so.0
libstdc++.so.6'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
