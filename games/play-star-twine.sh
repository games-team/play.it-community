#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Star-Twine
# send your bug reports to contact@dotslashplay.it
###

script_version=20230129.1

GAME_ID='star-twine'
GAME_NAME='Star-Twine'

ARCHIVE_BASE_0='star-twine-linux.zip'
ARCHIVE_BASE_0_MD5='0a98b235d1e30fe8d89a4e9278fb3e92'
ARCHIVE_BASE_0_SIZE='150000'
ARCHIVE_BASE_0_VERSION='1.3.0-itch.2021.09.21'
ARCHIVE_BASE_0_URL='https://sparsegamedev.itch.io/star-twine'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_MAIN_FILES='
Star-Twine.exe
Star-Twine.png
Content
cursor.bmp
monoconfig
monomachineconfig
FNA.dll
FNA.dll.config
Lidgren.Network.dll
NetCommon.dll
Steamworks.NET.dll'
CONTENT_DOC_MAIN_FILES='
license.txt
Linux.README'

APP_MAIN_EXE='Star-Twine.exe'
APP_MAIN_ICON='Star-Twine.png'

PKG_MAIN_DEPS='mono'
PKG_MAIN_DEPENDENCIES_LIBRARIES='
libFAudio.so.0
libGL.so.1
libSDL2-2.0.so.0
libudev.so.1'
PKG_MAIN_DEPENDENCIES_MONO_LIBRARIES='
mscorlib.dll
Mono.Posix.dll
Mono.Security.dll
System.dll
System.Configuration.dll
System.Core.dll
System.Data.dll
System.Drawing.dll
System.Numerics.dll
System.Runtime.Serialization.dll
System.Security.dll
System.Xml.dll
System.Xml.Linq.dll'

# Include shipped libraries that can not be replaced by system ones

CONTENT_LIBS_LIBS_PATH="${CONTENT_PATH_DEFAULT}/lib64"
CONTENT_LIBS_LIBS_FILES='
libFNA3D.so.0
libsteam_api.so'

PACKAGES_LIST='PKG_MAIN PKG_LIBS'

PKG_LIBS_ID="${GAME_ID}-libs"
PKG_LIBS_ARCH='64'
PKG_LIBS_DEPENDENCIES_LIBRARIES='
ld-linux-x86-64.so.2
libc.so.6
libdl.so.2
libm.so.6
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0'

PKG_MAIN_DEPS="$PKG_MAIN_DEPS $PKG_LIBS_ID"

# Load common functions

target_version='2.21'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icon

# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
