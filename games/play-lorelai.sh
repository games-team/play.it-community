#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Lorelai
# send your bug reports to contact@dotslashplay.it
###

script_version=20240707.2

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='lorelai'
GAME_NAME='Lorelai'

ARCHIVE_BASE_0_NAME='setup_lorelai_1.1.0a_(40070).exe'
ARCHIVE_BASE_0_MD5='41931b22bd91c8a61835f1ab5b317b38'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_lorelai_1.1.0a_(40070)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='3c93e5c60612965bd44ddc2480c57987'
ARCHIVE_BASE_0_SIZE='11000000'
ARCHIVE_BASE_0_VERSION='1.1.0a-gog40070'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/lorelai'

UNITY3D_NAME='lorelai'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_DATA_ASSETS_FILES="
${UNITY3D_NAME}_data/sharedassets*.assets*"
## TODO: Check if the "ost" directory is required.
CONTENT_GAME0_DATA_FILES='
ost
lorelai_icon2.ico'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Harvester Games/Lorelai'

APP_MAIN_ICON='lorelai_icon2.ico'

PACKAGES_LIST='
PKG_BIN
PKG_DATA_ASSETS
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_DATA_ASSETS_ID="${PKG_DATA_ID}-sharedassets"
PKG_DATA_ASSETS_DESCRIPTION="$PKG_DATA_DESCRIPTION - shared assets"
PKG_DATA_DEPS="${PKG_DATA_DEPS:-} $PKG_DATA_ASSETS_ID"

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
