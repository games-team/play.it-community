#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Tzar
# send your bug reports to contact@dotslashplay.it
###

script_version=20241106.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='tzar'
GAME_NAME='Tzar: The Burden of the Crown'

ARCHIVE_BASE_0_NAME='setup_tzar_1.01_(10554).exe'
ARCHIVE_BASE_0_MD5='2c0be6258d5987fadb2d6e21e5bfae2a'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='320000'
ARCHIVE_BASE_0_VERSION='1.10-gog10554'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/tzar_the_burden_of_the_crown'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
setup.exe
tzarcampaign.exe
tzaredit.exe
tzar.exe
*.dll'
CONTENT_GAME_DATA_FILES='
campaign
data
help
hero icons
images
local
maps
movies
music
sound'
CONTENT_GAME0_BIN_RELATIVE_PATH='__support/app'
CONTENT_GAME0_BIN_FILES='
tzar.ini'
CONTENT_GAME0_DATA_RELATIVE_PATH='__support/app'
CONTENT_GAME0_DATA_FILES='
demos'
CONTENT_DOC_DATA_FILES='
tzar110.txt
*.pdf'

USER_PERSISTENT_FILES='
tzar.ini'
USER_PERSISTENT_DIRECTORIES='
demos
save'

# Microsoft Visual C++ library is required by the game settings application
WINE_WINETRICKS_VERBS='mfc42'

APP_MAIN_EXE='tzar.exe'

APP_SETTINGS_ID="${GAME_ID}-settings"
APP_SETTINGS_NAME="$GAME_NAME - Settings"
APP_SETTINGS_CAT='Settings'
APP_SETTINGS_EXE='setup.exe'

APP_EDIT_ID="${GAME_ID}-edit"
APP_EDIT_NAME="$GAME_NAME - Map Editor"
APP_EDIT_EXE='tzaredit.exe'

APP_CAMPAIGN_ID="${GAME_ID}-campaign"
APP_CAMPAIGN_NAME="$GAME_NAME - Campaign Editor"
APP_CAMPAIGN_EXE='tzarcampaign.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Play the introduction video on launch

REQUIREMENTS_LIST="${REQUIREMENTS_LIST:-}
dos2unix
unix2dos"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path 'GAME0_BIN')"

	# Play the introduction video on launch
	config_file='tzar.ini'
	config_section='\[DEFAULTS\]'
	config_entry='SCREENCHOICE=3'
	sed_expression="s/${config_section}/&\\n${config_entry}/"
	dos2unix --quiet "$config_file"
	sed --in-place --expression="$sed_expression" "$config_file"
	unix2dos --quiet "$config_file"
)

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
