#!/bin/sh
# SPDX-FileCopyrightText: © 2019 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Vanishing of Ethan Carter
# send your bug reports to contact@dotslashplay.it
###

script_version=20230428.5

GAME_ID='the-vanishing-of-ethan-carter'
GAME_NAME='The Vanishing of Ethan Carter'

ARCHIVE_BASE_0='setup_the_vanishing_of_ethan_carter_1.04_(18494).exe'
ARCHIVE_BASE_0_MD5='ac039c2c1dcfc093840b0a5ce15008c0'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1='setup_the_vanishing_of_ethan_carter_1.04_(18494)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='77dbaf659c02d6fbb77917be36bf463d'
ARCHIVE_BASE_0_PART2='setup_the_vanishing_of_ethan_carter_1.04_(18494)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='35e0cc8447e782b57525aed48837bd1a'
ARCHIVE_BASE_0_SIZE='10000000'
ARCHIVE_BASE_0_VERSION='1.04-gog18494'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/the_vanishing_of_ethan_carter'

UNREALENGINE4_NAME='astronautsgame'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN32_FILES='
binaries/win32'
CONTENT_GAME_BIN64_FILES='
binaries/win64'
CONTENT_GAME_DATA_FILES="
binaries/game
binaries/build.properties
engine
${UNREALENGINE4_NAME}"

WINE_DIRECT3D_RENDERER='dxvk'

WINE_PERSISTENT_DIRECTORIES="
users/${USER}/Documents/My Games/The Vanishing of Ethan Carter/${UNREALENGINE4_NAME}"

APP_MAIN_EXE_BIN32="binaries/win32/${UNREALENGINE4_NAME}-win32-shipping.exe"
APP_MAIN_EXE_BIN64="binaries/win64/${UNREALENGINE4_NAME}-win64-shipping.exe"
APP_MAIN_ICON="binaries/win32/${UNREALENGINE4_NAME}-win32-shipping.exe"

PACKAGES_LIST='PKG_BIN32 PKG_BIN64 PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN32_ARCH='32'
PKG_BIN64_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
video/quicktime, variant=(string)iso'
PKG_BIN32_DEPENDENCIES_GSTREAMER_PLUGINS="$PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS"
PKG_BIN64_DEPENDENCIES_GSTREAMER_PLUGINS="$PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS"

# Load common functions

target_version='2.23'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

for PKG in 'PKG_BIN32' 'PKG_BIN64'; do
	# shellcheck disable=SC2119
	launchers_write
done

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
