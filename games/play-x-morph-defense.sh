#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# X-Morph: Defense
# send your bug reports to contact@dotslashplay.it
###

script_version=20240807.1

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='x-morph-defense'
GAME_NAME='X-Morph: Defense'

ARCHIVE_BASE_0_NAME='setup_x-morph_defense_complete_edition_1.0.0_(64bit)_(27014).exe'
ARCHIVE_BASE_0_MD5='9d9eecd4c40390e6944f745720421931'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_x-morph_defense_complete_edition_1.0.0_(64bit)_(27014)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='2ca0e0fbd9305b419e1c1ce2f706753c'
ARCHIVE_BASE_0_PART2_NAME='setup_x-morph_defense_complete_edition_1.0.0_(64bit)_(27014)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='939bd7b04555bbedcf9a50876c461d76'
ARCHIVE_BASE_0_VERSION='1.0.0-gog27014'
ARCHIVE_BASE_0_SIZE='4600000'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/xmorph_defense_complete_edition'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
bin/*.dll
bin/*.exe
conf'
CONTENT_GAME_DATA_FILES='
packs'

WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/X-Morph Defense'

APP_MAIN_EXE='bin/shooter_win64_release.exe'
## Run the game binary from its parent directory
APP_MAIN_PRERUN='
# Run the game binary from its parent directory
cd "$(dirname "$APP_EXE")"
APP_EXE=$(basename "$APP_EXE")
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
