#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Godhood
# send your bug reports to contact@dotslashplay.it
###

script_version=20240408.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='godhood'
GAME_NAME='Godhood'

ARCHIVE_BASE_1_NAME='godhood_1_2_4_46551.sh'
ARCHIVE_BASE_1_MD5='441bcc84f29501da6ea40216113750d1'
ARCHIVE_BASE_1_SIZE='900000'
ARCHIVE_BASE_1_VERSION='1.2.4-gog46551'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/godhood'

ARCHIVE_BASE_0_NAME='godhood_1_0_5_40453.sh'
ARCHIVE_BASE_0_MD5='6e0b1ddd1b9575b2c7d1f61ca2d57681'
ARCHIVE_BASE_0_SIZE='900000'
ARCHIVE_BASE_0_VERSION='1.0.5-gog40453'

CONTENT_PATH_DEFAULT='data/noarch/game'
## TODO: Check if system-provided libraries could be used instead.
CONTENT_LIBS_BIN_FILES='
libc++abi.so.1
libc++.so.1'
CONTENT_GAME_BIN_FILES='
godhood'
CONTENT_GAME_DATA_FILES='
*.bni
data
i18n'

USER_PERSISTENT_DIRECTORIES='
userdata
savedata'

APP_MAIN_EXE='godhood'
APP_MAIN_ICON='../support/icon.png'
## Create required directory for saved games.
APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}"'
# Create required directory for saved games
mkdir --parents savedata/Player'
## The game crashes on launch when SDL_VIDEODRIVER is set to "wayland".
APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}"'
# The game crashes on launch when SDL_VIDEODRIVER is set to "wayland".
if [ "${SDL_VIDEODRIVER:-}" = "wayland" ]; then
	unset SDL_VIDEODRIVER
fi
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libcurl.so.4+CURL_OPENSSL_3
libdl.so.2
libgcc_s.so.1
libGL.so.1
libGLU.so.1
libm.so.6
libpthread.so.0
librt.so.1
libSDL2-2.0.so.0
libX11.so.6
libXcursor.so.1
libXrandr.so.2
libz.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launcher

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
