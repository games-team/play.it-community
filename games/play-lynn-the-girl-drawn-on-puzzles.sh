#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Lynn, The Girl Drawn On Puzzles
# send your bug reports to contact@dotslashplay.it
###

script_version=20231019.2

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='lynn-the-girl-drawn-on-puzzles'
GAME_NAME='Lynn, The Girl Drawn On Puzzles'

ARCHIVE_BASE_0_NAME='setup_lynn__the_girl_drawn_on_puzzles_1.0.01_(64bit)_(66351).exe'
ARCHIVE_BASE_0_MD5='2bf80a446117e86f15a95b74612dd7c3'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='945180'
ARCHIVE_BASE_0_VERSION='1.0.01-gog66351'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/lynn_the_girl_drawn_on_puzzles'

UNITY3D_NAME='lynn'

CONTENT_PATH_DEFAULT='.'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/boing/lynn'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
