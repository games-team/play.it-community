#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 BetaRays
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Celeste
# send your bug reports to contact@dotslashplay.it
###

script_version=20240226.2

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='celeste'
GAME_NAME='Celeste'

ARCHIVE_BASE_1_NAME='celeste-linux.zip'
ARCHIVE_BASE_1_MD5='518ea69d60079109a2575d428eff8a53'
ARCHIVE_BASE_1_SIZE='1200000'
ARCHIVE_BASE_1_VERSION='1.4.0.0-itch.2021.03.29'
ARCHIVE_BASE_1_URL='https://maddymakesgamesinc.itch.io/celeste'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_MAIN_FILES='
Content
Celeste.exe
Celeste.exe.config
Celeste.png
gamecontrollerdb.txt
Celeste.Content.dll
FNA.dll
FNA.dll.config
FNA3D.dll'
## Include shipped libraries that can not be replaced by system ones.
CONTENT_LIBS_LIBS32_PATH="${CONTENT_PATH_DEFAULT}/lib"
CONTENT_LIBS_LIBS32_FILES='
libfmod*.so*
libmojoshader.so'
CONTENT_LIBS_LIBS64_PATH="${CONTENT_PATH_DEFAULT}/lib64"
CONTENT_LIBS_LIBS64_FILES='
libfmod*.so*
libmojoshader.so
libFNA3D.so.0'

APP_MAIN_EXE='Celeste.exe'
APP_MAIN_ICON='Celeste.png'

PACKAGES_LIST='
PKG_MAIN
PKG_LIBS32
PKG_LIBS64'

PKG_MAIN_DEPS='mono'
PKG_MAIN_DEPENDENCIES_LIBRARIES='
libGL.so.1
libSDL2-2.0.so.0
libSDL2_image-2.0.so.0
libXrandr.so.2'
PKG_MAIN_DEPENDENCIES_MONO_LIBRARIES='
mscorlib.dll
Mono.Posix.dll
Mono.Security.dll
System.dll
System.Configuration.dll
System.Core.dll
System.Data.dll
System.Drawing.dll
System.Numerics.dll
System.Runtime.Serialization.dll
System.Security.dll
System.Xml.dll
System.Xml.Linq.dll'
## Include shipped libraries that can not be replaced by system ones.
## TODO: These shipped libraries probably depend on some system native libraries being available.
PKG_LIBS_ID="${GAME_ID}-libs"
PKG_LIBS32_ID="$PKG_LIBS_ID"
PKG_LIBS32_ARCH='32'
PKG_LIBS64_ID="$PKG_LIBS_ID"
PKG_LIBS64_ARCH='64'
PKG_MAIN_DEPS="${PKG_MAIN_DEPS:-} $PKG_LIBS_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"
	set_standard_permissions .
)

# Include game data

# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
