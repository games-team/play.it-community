#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Mopi
# SPDX-FileCopyrightText: © 2017 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Gnomoria
# send your bug reports to contact@dotslashplay.it
###

script_version=20240831.2

PLAYIT_COMPATIBILITY_LEVEL='2.30'

GAME_ID='gnomoria'
GAME_NAME='Gnomoria'

ARCHIVE_BASE_0_NAME='gog_gnomoria_2.0.0.1.sh'
ARCHIVE_BASE_0_MD5='3d0a9ed4fb45ff133b5a7410a2114455'
ARCHIVE_BASE_0_SIZE='230000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.0.0.1'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/gnomoria'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_FILES='
libmojoshader.so
libtheoraplay.so'
CONTENT_LIBS_LIBS64_PATH="${CONTENT_PATH_DEFAULT}/lib64"
CONTENT_LIBS_LIBS64_FILES="$CONTENT_LIBS_FILES"
CONTENT_LIBS_LIBS32_PATH="${CONTENT_PATH_DEFAULT}/lib"
CONTENT_LIBS_LIBS32_FILES="$CONTENT_LIBS_FILES"
CONTENT_GAME_MAIN_FILES='
Content
Mod Files
mono
Gnomoria.exe
Gnomoria.png
FNA.dll
FNA.dll.config
gnomorialib.dll'
## TODO: Check if the Steam library is required
CONTENT_GAME0_MAIN_FILES='
Steamworks.NET.dll'
CONTENT_DOC_MAIN_FILES='
Linux.README'

APP_MAIN_EXE='Gnomoria.exe'
APP_MAIN_ICON='Gnomoria.png'

PACKAGES_LIST='
PKG_MAIN
PKG_LIBS64
PKG_LIBS32'

PKG_MAIN_DEPENDENCIES_SIBLINGS='
PKG_LIBS'
PKG_MAIN_DEPENDENCIES_LIBRARIES='
libSDL2-2.0.so.0
libSDL2_image-2.0.so.0'
PKG_MAIN_DEPENDENCIES_MONO_LIBRARIES='
mscorlib.dll
Mono.Posix.dll
Mono.Security.dll
System.dll
System.Configuration.dll
System.Core.dll
System.Data.dll
System.Drawing.dll
System.Security.dll
System.Xml.dll'
## 7za is used by the save system
PKG_MAIN_DEPENDENCIES_COMMANDS='
7za'

PKG_LIBS_ID="${GAME_ID}-libs"
PKG_LIBS64_ID="$PKG_LIBS_ID"
PKG_LIBS32_ID="$PKG_LIBS_ID"
PKG_LIBS_DESCRIPTION='shipped libraries'
PKG_LIBS64_DESCRIPTION="$PKG_LIBS_DESCRIPTION"
PKG_LIBS32_DESCRIPTION="$PKG_LIBS_DESCRIPTION"
PKG_LIBS64_ARCH='64'
PKG_LIBS32_ARCH='32'
PKG_LIBS_DEPENDENCIES_LIBRARIES='
libc.so.6
libm.so.6
libogg.so.0
libpthread.so.0
libtheoradec.so.1
libvorbis.so.0'
PKG_LIBS64_DEPENDENCIES_LIBRARIES="$PKG_LIBS_DEPENDENCIES_LIBRARIES"
PKG_LIBS32_DEPENDENCIES_LIBRARIES="$PKG_LIBS_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons
content_inclusion_default

## Include a link to 7za, used by the save system
link_source='/usr/bin/7za'
link_destination="$(package_path 'PKG_MAIN')$(path_game_data)"
mkdir --parents "${link_destination}/lib64"
mkdir --parents "${link_destination}/lib"
ln --symbolic "$link_source" "${link_destination}/lib64"
ln --symbolic "$link_source" "${link_destination}/lib"

# Write launchers

launchers_generation

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
