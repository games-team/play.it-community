#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2016 Mopi
# SPDX-FileCopyrightText: © 2016 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Lovers in a Dangerous Spacetime
# send your bug reports to contact@dotslashplay.it
###

script_version=20230105.2

GAME_ID='lovers-in-a-dangerous-spacetime'
GAME_NAME='Lovers in a Dangerous Spacetime'

UNITY3D_NAME='LoversInADangerousSpacetime'

ARCHIVE_BASE_1='LoversInADangerousSpacetime-1.4.5_Linux.zip'
ARCHIVE_BASE_1_MD5='67b6bc5ba5590fb50e95996b267f8c60'
ARCHIVE_BASE_1_SIZE='880000'
ARCHIVE_BASE_1_VERSION='1.4.5-humble180427'
ARCHIVE_BASE_1_URL='https://www.humblebundle.com/store/lovers-in-a-dangerous-spacetime'

ARCHIVE_BASE_0='LoversInADangerousSpacetime-1.4.4_Linux.zip'
ARCHIVE_BASE_0_MD5='38927a73e1fe84620ebc876f8f039adb'
ARCHIVE_BASE_0_SIZE='880000'
ARCHIVE_BASE_0_VERSION='1.4.4-humble160908'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN32_FILES="
${UNITY3D_NAME}.x86
${UNITY3D_NAME}_Data/Mono/x86
${UNITY3D_NAME}_Data/Plugins/x86"
CONTENT_GAME_BIN64_FILES="
${UNITY3D_NAME}.x86_64
${UNITY3D_NAME}_Data/Mono/x86_64
${UNITY3D_NAME}_Data/Plugins/x86_64"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data"

PACKAGES_LIST='PKG_BIN32 PKG_BIN64 PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN32_ARCH='32'
PKG_BIN32_DEPS="$PKG_DATA_ID"
PKG_BIN32_DEPENDENCIES_LIBRARIES='
ld-linux.so.2
ld-linux-x86-64.so.2
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXrandr.so.2'

PKG_BIN64_ARCH='64'
PKG_BIN64_DEPS="$PKG_BIN32_DEPS"
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN32_DEPENDENCIES_LIBRARIES"

# Load common functions

target_version='2.20'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

for PKG in 'PKG_BIN32' 'PKG_BIN64'; do
	# shellcheck disable=SC2119
	launchers_write
done

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
