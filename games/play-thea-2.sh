#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Thea 2
# send your bug reports to contact@dotslashplay.it
###

script_version=20230418.1

GAME_ID='thea-2'
GAME_NAME='Thea 2: The Shattering'

ARCHIVE_BASE_0='setup_thea_2_the_shattering_2.0603.0666c_(64bit)_(38776).exe'
ARCHIVE_BASE_0_MD5='bbcbc5777263f2511d53374bb5799986'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1='setup_thea_2_the_shattering_2.0603.0666c_(64bit)_(38776)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='028c57b5a383336387c1de382416d24d'
ARCHIVE_BASE_0_SIZE='6200000'
ARCHIVE_BASE_0_VERSION='2.0603.0666-gog38776'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/thea_2_the_shattering'

UNITY3D_NAME='thea2'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES="
${UNITY3D_NAME}_data/plugins
${UNITY3D_NAME}.exe
mono
galaxy64.dll
galaxycsharpglue.dll
unityplayer.dll
unitycrashhandler64.exe"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_data"

USER_PERSISTENT_DIRECTORIES="
${UNITY3D_NAME}_data/Profiles
${UNITY3D_NAME}_data/streamingassets/DataHistory"

APP_MAIN_EXE="${UNITY3D_NAME}.exe"

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

target_version='2.23'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
