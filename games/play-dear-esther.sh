#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Dear Esther
# send your bug reports to contact@dotslashplay.it
###

script_version=20240329.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='dear-esther'
GAME_NAME='Dear Esther'

## This DRM-free Linux installer used to be sold by Humble Bundle, but they dropped it in favour of Steam keys.
ARCHIVE_BASE_0_NAME='dearesther-linux-06082013-bin'
ARCHIVE_BASE_0_MD5='951127a283c3caf2c76031c7611d431d'
## This is a MojoSetup installer, not relying on Makeself.
ARCHIVE_BASE_0_EXTRACTOR='bsdtar'
ARCHIVE_BASE_0_SIZE='1500000'
ARCHIVE_BASE_0_VERSION='1.6-humble1'

CONTENT_PATH_DEFAULT='data'
CONTENT_LIBS_BIN_PATH="${CONTENT_PATH_DEFAULT}/bin"
CONTENT_LIBS_BIN_FILES='
datacache.so
engine.so
filesystem_stdio.so
inputsystem.so
launcher.so
libMiles.so
libtier0.so
libtogl.so
libvstdlib.so
localize.so
materialsystem.so
scenefilecache.so
shaderapidx9.so
soundemittersystem.so
stdshader_dx9.so
studiorender.so
valve_avi.so
vaudio_miles.so
vgui2.so
vguimatsurface.so
vphysics.so
vscript.so'
CONTENT_GAME_BIN_FILES='
bin/linux32
bin/vconfig
bin/*.addin
bin/*.bin
bin/*.cfg
bin/*.csv
bin/*.fgd
dearesther_linux'
CONTENT_GAME_DATA_FILES='
dearesther
dearesther.png
platform'
CONTENT_DOC_DATA_FILES='
README-linux.txt'

USER_PERSISTENT_DIRECTORIES='
dearesther/cfg
dearesther/save'

APP_MAIN_EXE='dearesther_linux'
APP_MAIN_OPTIONS='-game dearesther'
APP_MAIN_ICON='dearesther.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libEGL.so.1
libfontconfig.so.1
libfreetype.so.6
libm.so.6
libopenal.so.1
libpthread.so.0
libSDL2-2.0.so.0
libstdc++.so.6'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# The game engine looks for some library in a hardcoded path

libraries_source=$(path_libraries)
libraries_destination="$(package_path 'PKG_BIN')$(path_game_data)/bin"
mkdir --parents "$libraries_destination"
for library_file in \
	'filesystem_stdio.so' \
	'launcher.so'
do
	ln --symbolic "${libraries_source}/${library_file}" "$libraries_destination"
done

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
