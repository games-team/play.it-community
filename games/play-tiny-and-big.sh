#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 HS-157
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Tiny and Big: Grandpa's Leftovers
# send your bug reports to contact@dotslashplay.it
###

script_version=20231004.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='tiny-and-big'
GAME_NAME='Tiny and Big: Grandpaʼs Leftovers'

ARCHIVE_BASE_GOG_0='tiny_and_big_grandpa_s_leftovers_en_1_4_2_15616.sh'
ARCHIVE_BASE_GOG_0_MD5='bdcc1ea8366dedcfe00b50c439fd5ec9'
ARCHIVE_BASE_GOG_0_SIZE='2400000'
ARCHIVE_BASE_GOG_0_VERSION='1.4.2-gog15616'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/tiny_and_big_grandpas_leftovers'

ARCHIVE_BASE_HUMBLE_0='tinyandbig_grandpasleftovers-retail-linux-1.4.1_1370968537.tar.bz2'
ARCHIVE_BASE_HUMBLE_0_MD5='c6c2bc286f11e4a232211c5176105890'
ARCHIVE_BASE_HUMBLE_0_SIZE='2400000'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.4.1-humble1370968537'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/tiny-big-grandpas-leftovers'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='tinyandbig'
## TODO: Check if some shipped libraries could be replaced with system-provided ones.
CONTENT_LIBS_BIN_FILES='
libcal3d.so
libCgGL.so
libCg.so'
CONTENT_LIBS_BIN32_PATH_GOG="${CONTENT_PATH_DEFAULT_GOG}/bin32"
CONTENT_LIBS_BIN32_PATH_HUMBLE="${CONTENT_PATH_DEFAULT_HUMBLE}/bin32"
CONTENT_LIBS_BIN32_FILES="$CONTENT_LIBS_BIN_FILES"
CONTENT_LIBS_BIN64_PATH_GOG="${CONTENT_PATH_DEFAULT_GOG}/bin64"
CONTENT_LIBS_BIN64_PATH_HUMBLE="${CONTENT_PATH_DEFAULT_HUMBLE}/bin64"
CONTENT_LIBS_BIN64_FILES="$CONTENT_LIBS_BIN_FILES"
CONTENT_GAME_BIN32_FILES='
bin32/tinyandbig'
CONTENT_GAME_BIN64_FILES='
bin64/tinyandbig'
CONTENT_GAME_DATA_FILES='
assets'

USER_PERSISTENT_FILES='
options.txt
*.save'

APP_MAIN_EXE_BIN32='bin32/tinyandbig'
APP_MAIN_EXE_BIN64='bin64/tinyandbig'

PACKAGES_LIST='PKG_BIN32 PKG_BIN64 PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN32_ARCH='32'
PKG_BIN64_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN32_DEPS="$PKG_BIN_DEPS"
PKG_BIN64_DEPS="$PKG_BIN_DEPS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libm.so.6
libopenal.so.1
libpthread.so.0
libstdc++.so.6
libX11.so.6'
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
ARCHIVE_OPTIONAL_ICONS_0_NAME='tiny-and-big_icons.tar.gz'
ARCHIVE_OPTIONAL_ICONS_0_MD5='043fa61c838ba6b2ef301c52660352b1'
ARCHIVE_OPTIONAL_ICONS_0_URL='https://downloads.dotslashplay.it/games/tiny-and-big'
archive_initialize_optional \
	'ARCHIVE_ICONS' \
	'ARCHIVE_OPTIONAL_ICONS'
## TODO: The library should provide a function to check if a given optional archive has been provided.
if [ -n "$(archive_path 'ARCHIVE_ICONS' 2>/dev/null || true)" ]; then
	archive_extraction 'ARCHIVE_ICONS'
fi

# Include game data

## TODO: The library should provide a function to check if a given optional archive has been provided.
if [ -n "$(archive_path 'ARCHIVE_ICONS' 2>/dev/null || true)" ]; then
	CONTENT_ICONS_DATA_PATH='.'
	CONTENT_ICONS_DATA_FILES='
	16x16
	32x32
	48x48
	64x64'
	content_inclusion 'ICONS_DATA' 'PKG_DATA' "$(path_icons)"
fi
content_inclusion_default

# Write launchers

PKG='PKG_BIN32'
# shellcheck disable=SC2119
launchers_write
PKG='PKG_BIN64'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
