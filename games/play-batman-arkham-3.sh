#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Batman: Arkham Origins
# send your bug reports to contact@dotslashplay.it
###

script_version=20241124.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='batman-arkham-3'
GAME_NAME='Batman: Arkham Origins'

ARCHIVE_BASE_0_NAME='setup_batman_arkham_origins_1.0_(37592).exe'
ARCHIVE_BASE_0_MD5='954bfc571f142dbcd8c21bb53aff9476'
## Do not convert file paths to lowercase.
ARCHIVE_BASE_0_EXTRACTOR='innoextract'
ARCHIVE_BASE_0_EXTRACTOR_OPTIONS=' '
ARCHIVE_BASE_0_PART1_NAME='setup_batman_arkham_origins_1.0_(37592)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='379e08cdded22ca72de6ef1592cf9ec3'
ARCHIVE_BASE_0_PART2_NAME='setup_batman_arkham_origins_1.0_(37592)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='483e7b78a963a844dc49fe0d563dba51'
ARCHIVE_BASE_0_PART3_NAME='setup_batman_arkham_origins_1.0_(37592)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='7bb72091ee2ca1499c4d4e4245b92a5f'
ARCHIVE_BASE_0_PART4_NAME='setup_batman_arkham_origins_1.0_(37592)-4.bin'
ARCHIVE_BASE_0_PART4_MD5='d705ed0ace1d108a97433e0474e8dbdb'
ARCHIVE_BASE_0_SIZE='18000000'
ARCHIVE_BASE_0_VERSION='1.0-gog37592'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/batman_arkham_origins'

CONTENT_PATH_DEFAULT='SinglePlayer'
CONTENT_GAME_BIN_FILES='
Binaries
Engine
BMGame/Config'
## TODO: The files for the English localization should be moved to a dedicated package too.
CONTENT_GAME_L10N_FR_FILES='
BMGame/CookedPCConsole/French(France)
BMGame/Localization/FRA
PCConsoleTOC_FRA.txt'
CONTENT_GAME_TEXTURES_FILES='
BMGame/CookedPCConsole/*.tfc'
CONTENT_GAME_DATA_FILES='
BMGame/EP2.dat
BMGame/EP3.dat
BMGame/EP4.dat
BMGame/Movies
BMGame/Splash
BMGame/WBID
BMGame/CookedPCConsole/SFX
BMGame/CookedPCConsole/*.bin
BMGame/CookedPCConsole/*.txt
BMGame/CookedPCConsole/*.upk
BMGame/CookedPCConsole/English(US)
BMGame/Localization/INT
PCConsoleTOC.txt'

## TODO: Check that write access to this directory is required.
USER_PERSISTENT_DIRECTORIES='
BMGame/Config'

WINE_DIRECT3D_RENDERER='dxvk'
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/WB Games/Batman Arkham Origins'
WINE_WINETRICKS_VERBS='physx'

APP_MAIN_EXE='Binaries/Win32/BatmanOrigins.exe'
## Run the game binary from its directory.
APP_MAIN_PRERUN='
# Run the game binary from its directory
cd "$(dirname "$APP_EXE")"
APP_EXE=$(basename "$APP_EXE")
'

PACKAGES_LIST='
PKG_BIN
PKG_L10N_FR
PKG_TEXTURES
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_L10N_ID="${GAME_ID}-l10n"
PKG_L10N_FR_ID="${PKG_L10N_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_ID"
PKG_L10N_FR_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_FR_DESCRIPTION='French localization'

PKG_TEXTURES_ID="${GAME_ID}-textures"
PKG_TEXTURES_DESCRIPTION='textures'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_L10N_ID $PKG_TEXTURES $PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
case "$(messages_language)" in
	('fr')
		lang_string='version %s :'
		lang_en='anglaise'
		lang_fr='française'
	;;
	('en'|*)
		lang_string='%s version:'
		lang_en='English'
		lang_fr='French'
	;;
esac
printf '\n'
printf "$lang_string" "$lang_en"
print_instructions 'PKG_BIN' 'PKG_DATA' 'PKG_TEXTURES'
printf "$lang_string" "$lang_fr"
print_instructions 'PKG_BIN' 'PKG_L10N_FR' 'PKG_DATA' 'PKG_TEXTURES'

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
