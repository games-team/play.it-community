#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Monster Prom
# send your bug reports to contact@dotslashplay.it
###

script_version=20241210.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='monster-prom'
GAME_NAME='Monster Prom'

ARCHIVE_BASE_12_NAME='monster_prom_v6_8b_71317.sh'
ARCHIVE_BASE_12_MD5='cff6c702d4b3f331f42f34f574fbb25a'
ARCHIVE_BASE_12_SIZE='3005768'
ARCHIVE_BASE_12_VERSION='6.8b-gog71317'
ARCHIVE_BASE_12_URL='https://www.gog.com/game/monster_prom'

ARCHIVE_BASE_11_NAME='monster_prom_6_4b_53339.sh'
ARCHIVE_BASE_11_MD5='27e03a4921ee67aa1284e9de2c5d2160'
ARCHIVE_BASE_11_SIZE='3100000'
ARCHIVE_BASE_11_VERSION='6.4b-gog53339'

ARCHIVE_BASE_10_NAME='monster_prom_5_16_45016.sh'
ARCHIVE_BASE_10_MD5='58e9029453111916107341867d2c8327'
ARCHIVE_BASE_10_SIZE='3000000'
ARCHIVE_BASE_10_VERSION='5.16-gog45016'

ARCHIVE_BASE_9_NAME='monster_prom_5_7_42255.sh'
ARCHIVE_BASE_9_MD5='59a60a6941773d48cca441501507209d'
ARCHIVE_BASE_9_SIZE='2500000'
ARCHIVE_BASE_9_VERSION='5.7-gog42255'

ARCHIVE_BASE_8_NAME='monster_prom_4_80_36450.sh'
ARCHIVE_BASE_8_MD5='5847214b0bcf816d03e165a16d0c19c4'
ARCHIVE_BASE_8_SIZE='2400000'
ARCHIVE_BASE_8_VERSION='4.80-gog36450'

ARCHIVE_BASE_7_NAME='monster_prom_4_79_36279.sh'
ARCHIVE_BASE_7_MD5='c1d893075a21af380031c953e856bd7c'
ARCHIVE_BASE_7_SIZE='2400000'
ARCHIVE_BASE_7_VERSION='4.79-gog36279'

ARCHIVE_BASE_6_NAME='monster_prom_4_77_36137.sh'
ARCHIVE_BASE_6_MD5='87a05cfec3a314c4a6ea1047154958cf'
ARCHIVE_BASE_6_SIZE='2400000'
ARCHIVE_BASE_6_VERSION='4.77-gog36137'

ARCHIVE_BASE_5_NAME='monster_prom_4_68_35225.sh'
ARCHIVE_BASE_5_MD5='c48257dadd81ac089b10567733beea48'
ARCHIVE_BASE_5_SIZE='2200000'
ARCHIVE_BASE_5_VERSION='4.68-gog35225'

ARCHIVE_BASE_4_NAME='monster_prom_4_61_33782.sh'
ARCHIVE_BASE_4_MD5='0ed680d8cf93810c80f2c2f02ce16ae6'
ARCHIVE_BASE_4_SIZE='2200000'
ARCHIVE_BASE_4_VERSION='4.61-gog33782'

ARCHIVE_BASE_3_NAME='monster_prom_4_57_33526.sh'
ARCHIVE_BASE_3_MD5='62a6e7d2bf6dc9ede39ec014cd73aaf4'
ARCHIVE_BASE_3_SIZE='2200000'
ARCHIVE_BASE_3_VERSION='4.57-gog33526'

ARCHIVE_BASE_2_NAME='monster_prom_4_44_30880.sh'
ARCHIVE_BASE_2_MD5='feea2789e951c992e714a0d01afb7348'
ARCHIVE_BASE_2_SIZE='2000000'
ARCHIVE_BASE_2_VERSION='4.44-gog30880'

ARCHIVE_BASE_1_NAME='monster_prom_4_38b_30736.sh'
ARCHIVE_BASE_1_MD5='4dc9b48a90220ecc0fcd91e44f640320'
ARCHIVE_BASE_1_SIZE='2000000'
ARCHIVE_BASE_1_VERSION='4.38b-gog30736'

ARCHIVE_BASE_0_NAME='monster_prom_2_44_26055.sh'
ARCHIVE_BASE_0_MD5='c558e2ba0540ba6651a35a3a5e2a146b'
ARCHIVE_BASE_0_SIZE='1400000'
ARCHIVE_BASE_0_VERSION='2.44-gog26055'

UNITY3D_NAME='MonsterProm'
UNITY3D_PLUGINS='
libStandaloneFileBrowser.so
ScreenSelector.so'
## TODO: Check if the Steam library is required.
UNITY3D_PLUGINS="$UNITY3D_PLUGINS
libsteam_api.so"

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_GAME0_DATA_FILES='
UserData'

USER_PERSISTENT_DIRECTORIES='
UserData'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-3.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
