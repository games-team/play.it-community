#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2017 Jacek Szafarkiewicz
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Airline Tycoon Deluxe
# send your bug reports to contact@dotslashplay.it
###

script_version=20240603.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='airline-tycoon-deluxe'
GAME_NAME='Airline Tycoon Deluxe'

ARCHIVE_BASE_0_NAME='gog_airline_tycoon_deluxe_2.0.0.9.sh'
ARCHIVE_BASE_0_MD5='dc8b78da150bd3b2089120cc2d24353c'
ARCHIVE_BASE_0_SIZE='1200000'
ARCHIVE_BASE_0_VERSION='1.08-gog2.0.0.9'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/airline_tycoon_deluxe'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN_PATH="${CONTENT_PATH_DEFAULT}/lib32"
## TODO: Replace this glob by an explicit list of libraries.
CONTENT_LIBS_BIN_FILES='
*'
CONTENT_GAME_BIN_FILES='
ATDeluxe'
CONTENT_GAME_L10N_FILES='
de
en
fr'
CONTENT_GAME_DATA_FILES='
gli
intro
misc
room
sound
video
*.bmp'
CONTENT_DOC_DATA_FILES='
LICENSE
README
CHANGELOG'

FAKE_HOME_PERSISTENT_DIRECTORIES='
.AirlineTycoonDeluxe'

APP_MAIN_EXE='ATDeluxe'
APP_MAIN_ICON='../support/icon.png'

PACKAGES_LIST='
PKG_BIN
PKG_L10N
PKG_DATA'

PKG_L10N_ID="${GAME_ID}-l10n"
PKG_L10N_DESCRIPTION='localizations'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
## TODO: Update the list of required native libraries.
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libstdc++.so.6
libSDL2-2.0.so.0
libGL.so.1'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
