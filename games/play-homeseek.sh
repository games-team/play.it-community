#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Homeseek
# send your bug reports to contact@dotslashplay.it
###

script_version=20240531.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='homeseek'
GAME_NAME='Homeseek'

ARCHIVE_BASE_0_NAME='setup_homeseek_1.0.9_(71375).exe'
ARCHIVE_BASE_0_MD5='253a29f9d133ce2980797c3b4ec5af65'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_homeseek_1.0.9_(71375)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='d9fd40bcd7623d8ae512ed898bd190ab'
ARCHIVE_BASE_0_PART2_NAME='setup_homeseek_1.0.9_(71375)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='b8bc0abf22d569a5262b7d45e9645f47'
ARCHIVE_BASE_0_SIZE='8641921'
ARCHIVE_BASE_0_VERSION='1.0.9-gog71375'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/homeseek'

UNITY3D_NAME='homeseek'

CONTENT_PATH_DEFAULT='.'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Traptics/Homeseek'
WINE_REGEDIT_PERSISTENT_KEYS='
HKEY_CURRENT_USER\Software\Traptics\Homeseek'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
video/quicktime, variant=(string)iso'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
