#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 Mopi
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Gun-Toting Cats
# send your bug reports to contact@dotslashplay.it
###

script_version=20240606.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='gun-toting-cats'
GAME_NAME='Gun-Toting Cats'

ARCHIVE_BASE_0_NAME='(Linux) GTCats Prototype.zip'
ARCHIVE_BASE_0_MD5='4e020ea013b59e8c04a52e1f7e1e32a9'
ARCHIVE_BASE_0_SIZE='56670'
ARCHIVE_BASE_0_VERSION='1.8-itch'
ARCHIVE_BASE_0_URL='https://kit9studio.itch.io/gun-toting-cats'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
Gun-Toting Cats Prototype 1.8'
CONTENT_GAME_DATA_FILES='
data.pck'
CONTENT_DOC_DATA_FILES='
licenses.txt'

APP_MAIN_EXE='Gun-Toting Cats Prototype 1.8'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libasound.so.2
libc.so.6
libdl.so.2
libGL.so.1
libm.so.6
libpthread.so.0
libpulse-simple.so.0
libpulse.so.0
libX11.so.6
libXcursor.so.1
libXinerama.so.1
libXi.so.6
libXrandr.so.2'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
