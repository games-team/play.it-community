#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2024 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Incredible Adventures of Van Helsing: Final Cut
# send your bug reports to contact@dotslashplay.it
###

script_version=20241124.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='van-helsing-final-cut'
GAME_NAME='The Incredible Adventures of Van Helsing: Final Cut'

ARCHIVE_BASE_0_NAME='setup_the_incredible_adventures_of_van_helsing_-_final_cut_1.1.0b_(b)_(23988).exe'
ARCHIVE_BASE_0_MD5='11823fff68df72308f3f8ba8f9dfa820'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_the_incredible_adventures_of_van_helsing_-_final_cut_1.1.0b_(b)_(23988)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='07fcaf654ce6cfb9026d1e6e40b06bf3'
ARCHIVE_BASE_0_PART2_NAME='setup_the_incredible_adventures_of_van_helsing_-_final_cut_1.1.0b_(b)_(23988)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='de5281e71e9d68bb196da32c09048ec9'
ARCHIVE_BASE_0_PART3_NAME='setup_the_incredible_adventures_of_van_helsing_-_final_cut_1.1.0b_(b)_(23988)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='721981718e036fb2fb4e848543241be7'
ARCHIVE_BASE_0_PART4_NAME='setup_the_incredible_adventures_of_van_helsing_-_final_cut_1.1.0b_(b)_(23988)-4.bin'
ARCHIVE_BASE_0_PART4_MD5='dc7981e674f6400d1919d56b735854fd'
ARCHIVE_BASE_0_PART5_NAME='setup_the_incredible_adventures_of_van_helsing_-_final_cut_1.1.0b_(b)_(23988)-5.bin'
ARCHIVE_BASE_0_PART5_MD5='0d9cec82a419c8abb5a7a414817e1446'
ARCHIVE_BASE_0_SIZE='35870060'
ARCHIVE_BASE_0_VERSION='1.1.0b-gog23988'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/the_incredible_adventures_of_van_helsing_final_cut'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
physx3common_x64.dll
physx3cooking_x64.dll
physx3_x64.dll
vanhelsing_x64.exe'
## TODO: Check if the Galaxy libraries are required.
CONTENT_GAME0_BIN_FILES='
galaxy64.dll
galaxypeer64.dll'
## English voices should always be included, as there is no alternative localized voices.
CONTENT_GAME_L10N_COMMON_FILES='
sounds/languages/eng'
CONTENT_GAME_L10N_EN_FILES='
strings/eng
ui/loadingscreens/eng
videos/eng'
CONTENT_GAME_L10N_FR_FILES='
ui/loadingscreens/fr
strings/french
videos/fr'
CONTENT_GAME_DATA_TERRAINS_1_FILES='
terrains/barrikados
terrains/belvaros
terrains/bossc2
terrains/cave
terrains/city1*
terrains/clockwork_keep
terrains/csatorna_*
terrains/dark_carnival
terrains/dr_csont
terrains/driad_boss_arena
terrains/final_boss
terrains/floating_monastery
terrains/gasmachine
terrains/gergo_kezdoteszt
terrains/giantwoods
terrains/gorge_of_eternal_peril
terrains/gyarnegyed
terrains/haunted_forest_*
terrains/hf_arena'
CONTENT_GAME_DATA_TERRAINS_2_FILES='
terrains/ink*
terrains/kapubirtok
terrains/kisbarlang
terrains/kohok*
terrains/kristalyos_ink
terrains/kutatolabor
terrains/lair_01_03
terrains/memorial_perk
terrains/nagyerdo
terrains/nightmare_hollow_ring*'
CONTENT_GAME_DATA_TERRAINS_3_FILES='
terrains/ostrom2
terrains/pokol*
terrains/poksziklak
terrains/pvp_karacsony
terrains/steamhall
terrains/szerpentin
terrains/tetok2
terrains/tetok_final
terrains/touchdown
terrains/tutorial
terrains/vh*
terrains/viztisztito*
terrains/wolf_den
terrains/wormcave'
CONTENT_GAME_DATA_FILES='
cfg
cgshaders
commontextures
effects
installutils
models
sounds
textures
ui
remote_config_cache_production_worldwide.json'
CONTENT_GAME0_DATA_PATH='app'
CONTENT_GAME0_DATA_FILES='
cfg/skills/monster/__gargoyle_zuhanas_effect.cfg
cfg/soldier/02_normal_ranged/atd_inkmachine_generalis.cfg'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/NeocoreGames/Van Helsing Final Cut'
WINE_REGEDIT_PERSISTENT_KEYS='
HKEY_CURRENT_USER\Software\NeoCore Games\Van Helsing Final Cut'

APP_MAIN_EXE='vanhelsing_x64.exe'

PACKAGES_LIST='
PKG_BIN
PKG_L10N_COMMON
PKG_L10N_EN
PKG_L10N_FR
PKG_DATA_TERRAINS_1
PKG_DATA_TERRAINS_2
PKG_DATA_TERRAINS_3
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_DATA_TERRAINS_ID="${PKG_DATA_ID}-terrains"
PKG_DATA_TERRAINS_1_ID="${PKG_DATA_TERRAINS_ID}-1"
PKG_DATA_TERRAINS_2_ID="${PKG_DATA_TERRAINS_ID}-2"
PKG_DATA_TERRAINS_3_ID="${PKG_DATA_TERRAINS_ID}-3"
PKG_DATA_TERRAINS_DESCRIPTION="$PKG_DATA_DESCRIPTION - terrains"
PKG_DATA_TERRAINS_1_DESCRIPTION="$PKG_DATA_TERRAINS_DESCRIPTION - 1"
PKG_DATA_TERRAINS_2_DESCRIPTION="$PKG_DATA_TERRAINS_DESCRIPTION - 2"
PKG_DATA_TERRAINS_3_DESCRIPTION="$PKG_DATA_TERRAINS_DESCRIPTION - 3"
PKG_DATA_DEPS="${PKG_DATA_DEPS:-} $PKG_DATA_TERRAINS_1_ID $PKG_DATA_TERRAINS_2_ID $PKG_DATA_TERRAINS_3_ID"

PKG_L10N_ID="${GAME_ID}-l10n"
PKG_L10N_COMMON_ID="${PKG_L10N_ID}-common"
PKG_L10N_EN_ID="${PKG_L10N_ID}-en"
PKG_L10N_FR_ID="${PKG_L10N_ID}-fr"
PKG_L10N_PROVIDES="
$PKG_L10N_ID"
PKG_L10N_EN_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_FR_PROVIDES="$PKG_L10N_PROVIDES"
PKG_L10N_DESCRIPTION='localization'
PKG_L10N_COMMON_DESCRIPTION="$PKG_L10N_DESCRIPTION - common files"
PKG_L10N_EN_DESCRIPTION="$PKG_L10N_DESCRIPTION - English"
PKG_L10N_FR_DESCRIPTION="$PKG_L10N_DESCRIPTION - French"
PKG_L10N_DEPS="$PKG_L10N_COMMON_ID"
PKG_L10N_EN_DEPS="$PKG_L10N_DEPS"
PKG_L10N_FR_DEPS="$PKG_L10N_DEPS"

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_L10N_ID $PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
deinterlace
video/x-ms-asf'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write the localizations configuration

## English localization

config_en="$(package_path 'PKG_L10N_EN')$(path_game_data)/strings/languages.xml"
cat > "$config_en" << EOF
<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
<Root>
	<LanguageName>
		<eng>English</eng>
	</LanguageName>
	<Languages>eng</Languages>
</Root>
EOF

## French localization

config_fr="$(package_path 'PKG_L10N_FR')$(path_game_data)/strings/languages.xml"
## TODO: Check if including the English locale is required for the English voices to be used.
cat > "$config_fr" << EOF
<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
<Root>
	<LanguageName>
		<FR>French</FR>
		<eng>English</eng>
	</LanguageName>
	<Languages>FR;eng</Languages>
</Root>
EOF

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
case "$(messages_language)" in
	('fr')
		message='version %s :'
		lang_en='anglaise'
		lang_fr='française'
	;;
	('en'|*)
		message='%s version:'
		lang_en='English'
		lang_fr='French'
	;;
esac
printf '\n'
## English localization
printf "$message" "$lang_en"
## Silence a ShellCheck false positive, word splitting is expected here
## SC2046 (warning): Quote this to prevent word splitting.
# shellcheck disable=SC2046
print_instructions $(packages_list | sed 's/PKG_L10N_FR//')
## French localization
printf "$message" "$lang_fr"
## Silence a ShellCheck false positive, word splitting is expected here
## SC2046 (warning): Quote this to prevent word splitting.
# shellcheck disable=SC2046
print_instructions $(packages_list | sed 's/PKG_L10N_EN//')

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
