#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Valhalla Hills
# send your bug reports to contact@dotslashplay.it
###

script_version=20240621.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='valhalla-hills'
GAME_NAME='Valhalla Hills'

ARCHIVE_BASE_0_NAME='gog_valhalla_hills_2.3.0.5.sh'
ARCHIVE_BASE_0_MD5='59286a003e654ae9f571abccc0375053'
ARCHIVE_BASE_0_SIZE='1251393'
ARCHIVE_BASE_0_VERSION='1.05.17-gog2.3.0.5'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/valhalla_hills'

UNREALENGINE4_NAME='ValhallaHills'

CONTENT_PATH_DEFAULT='data/noarch/game'

USER_PERSISTENT_DIRECTORIES='
Saved'

APP_MAIN_EXE='ValhallaHills/Binaries/Linux/ValhallaHills-Linux-Shipping'
APP_MAIN_ICON='../support/icon.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-3.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpango-1.0.so.0
libpthread.so.0
libQt5Core.so.5
libQt5Gui.so.5
libQt5Widgets.so.5
libQtCore.so.4
libQtGui.so.4
librt.so.1
libstdc++.so.6'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
