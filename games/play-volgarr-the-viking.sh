#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2020 HS-157
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Volgarr the Viking
# send your bug reports to contact@dotslashplay.it
###

script_version=20231108.1

PLAYIT_COMPATIBILITY_LEVEL='2.27'

GAME_ID='volgarr-the-viking'
GAME_NAME='Volgarr the Viking'

ARCHIVE_BASE_GOG_0_NAME='gog_volgarr_the_viking_2.1.0.3.sh'
ARCHIVE_BASE_GOG_0_MD5='8593287f13c3104aa45b9c91264b4260'
ARCHIVE_BASE_GOG_0_SIZE='200000'
ARCHIVE_BASE_GOG_0_VERSION='1.36c-gog2.1.0.3'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/volgarr_the_viking'

ARCHIVE_BASE_HUMBLE_0_NAME='VolgarrTheViking_v1.36c_Linux32.tar.gz'
ARCHIVE_BASE_HUMBLE_0_MD5='c3652629edb019838d8e1c7873f0716b'
ARCHIVE_BASE_HUMBLE_0_SIZE='180000'
ARCHIVE_BASE_HUMBLE_0_VERSION='1.36c-humble'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/volgarr-the-viking'

CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_PATH_DEFAULT_HUMBLE='Volgarr'
CONTENT_GAME_BIN_FILES='
Volgarr'
CONTENT_GAME_DATA_FILES='
Data.pk
icon.png'
CONTENT_GAME_DOC_FILES='
Readme.txt'

APP_MAIN_EXE='Volgarr'
APP_MAIN_ICON='icon.png'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"
## TODO: Update the list of required native libraries.
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libstdc++.so.6
libGL.so.1
libSDL2-2.0.so.0'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

set_current_package 'PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion
content_inclusion_default

# Write launchers

set_current_package 'PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
