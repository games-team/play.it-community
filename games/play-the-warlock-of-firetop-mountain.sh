#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2018 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# The Warlock of Firetop Mountain
# send your bug reports to contact@dotslashplay.it
###

script_version=20230222.1

GAME_ID='the-warlock-of-firetop-mountain'
GAME_NAME='The Warlock of Firetop Mountain'

ARCHIVE_BASE_1='WARLOCK_LINUX_487115.zip'
ARCHIVE_BASE_1_MD5='ae29e02f78225d69a3bdb661e13f2a3f'
ARCHIVE_BASE_1_SIZE='1600000'
ARCHIVE_BASE_1_VERSION='1.0-humble180508'
ARCHIVE_BASE_1_URL='https://www.humblebundle.com/store/the-warlock-of-firetop-mountain'

ARCHIVE_BASE_0='WARLOCK_LINUX_487088.zip'
ARCHIVE_BASE_0_MD5='17d4e909dbed98cd420eee5bb6a828d3'
ARCHIVE_BASE_0_SIZE='1600000'
ARCHIVE_BASE_0_VERSION='1.0-humble171022'

UNITY3D_NAME='The Warlock of Firetop Mountain'

CONTENT_PATH_DEFAULT='WARLOCK_LINUX_487115'
CONTENT_PATH_DEFAULT_0='WARLOCK_LINUX_487088'
## The game fails to start if libCSteamworks.so is not available,
## libsteam_api.so is a requirement of libCSteamworks.so.
CONTENT_LIBS_BIN32_PATH="${CONTENT_PATH_DEFAULT}/${UNITY3D_NAME}_Data/Plugins/x86"
CONTENT_LIBS_BIN32_FILES='
libCSteamworks.so
libsteam_api.so
ScreenSelector.so'
CONTENT_GAME_BIN32_FILES="
${UNITY3D_NAME}.x86
${UNITY3D_NAME}_Data/Mono/x86"
CONTENT_LIBS_BIN64_PATH="${CONTENT_PATH_DEFAULT}/${UNITY3D_NAME}_Data/Plugins/x86_64"
CONTENT_LIBS_BIN64_FILES='
libCSteamworks.so
libsteam_api.so
ScreenSelector.so'
CONTENT_GAME_BIN64_FILES="
${UNITY3D_NAME}.x86_64
${UNITY3D_NAME}_Data/Mono/x86_64"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data/Mono/etc
${UNITY3D_NAME}_Data/Managed
${UNITY3D_NAME}_Data/Resources
${UNITY3D_NAME}_Data/globalgamemanagers
${UNITY3D_NAME}_Data/ScreenSelector.png
${UNITY3D_NAME}_Data/level*
${UNITY3D_NAME}_Data/*.assets
${UNITY3D_NAME}_Data/*.resS
${UNITY3D_NAME}_Data/*.resource"

PACKAGES_LIST='PKG_BIN32 PKG_BIN64 PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN32_ARCH='32'
PKG_BIN64_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN32_DEPS="$PKG_DATA_ID"
PKG_BIN64_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libGL.so.1
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libX11.so.6
libXcursor.so.1
libXrandr.so.2
libz.so.1'
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

# Load common functions

target_version='2.21'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# The game engine fails to load some plugins unless they are found in a hardcoded path

file_name='ScreenSelector.so'
file_source="$(path_libraries)/${file_name}"
for file_destination in \
	"$(package_path 'PKG_BIN32')$(path_game_data)/$(unity3d_name)_Data/Plugins/x86/${file_name}" \
	"$(package_path 'PKG_BIN64')$(path_game_data)/$(unity3d_name)_Data/Plugins/x86_64/${file_name}"
do
	mkdir --parents "$(dirname "$file_destination")"
	ln --symbolic "$file_source" "$file_destination"
done

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'
(
	cd "${PLAYIT_WORKDIR}/gamedata/$(content_path_default)"
	set_standard_permissions .
)

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

for PKG in 'PKG_BIN32' 'PKG_BIN64'; do
	# shellcheck disable=SC2119
	launchers_write
done

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
