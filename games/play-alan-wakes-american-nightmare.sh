#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Alan Wake's American Nightmare
# send your bug reports to contact@dotslashplay.it
###

script_version=20240728.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='alan-wakes-american-nightmare'
GAME_NAME='Alan Wake’s American Nightmare'

ARCHIVE_BASE_0_NAME='setup_alan_wake_american_nightmare_2.1.0.24.exe'
ARCHIVE_BASE_0_MD5='dc500730b639a48897c00f5791e5b2f3'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='2500000'
ARCHIVE_BASE_0_VERSION='1.0-gog2.1.0.24'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/alan_wakes_american_nightmare'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
*.dll
*.exe'
CONTENT_GAME_DATA_FILES='
data
shaders'

## Grant required write access to some paths to the user.
## TODO: The ./play.it library should provide a way to give write access to a volatile path,
##       without automatically moving this path to persistent storage.
USER_PERSISTENT_DIRECTORIES='
shaders'

## Apply d3dcompiler_43 winetricks verb.
## TODO: Check if it is still required with current WINE builds.
WINE_WINETRICKS_VERBS='d3dcompiler_43'

APP_MAIN_EXE='alan_wakes_american_nightmare.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
