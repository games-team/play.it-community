#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Iratus
# send your bug reports to contact@dotslashplay.it
###

script_version=20241111.1

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='iratus'
GAME_NAME='Iratus: Lord of the Dead'

## The "language-specific" installers actually include all languages.
## There is no need to build distinct packages for each one.

ARCHIVE_BASE_EN_0_NAME='iratus_lord_of_the_dead_linux_181_13_00_48040.sh'
ARCHIVE_BASE_EN_0_MD5='e238fdf4277942c22805409a4d347635'
ARCHIVE_BASE_EN_0_SIZE='3351586'
ARCHIVE_BASE_EN_0_VERSION='181.03.00-gog48040'
ARCHIVE_BASE_EN_0_URL='https://www.gog.com/game/iratus_lord_of_the_dead'

ARCHIVE_BASE_FR_0_NAME='iratus_lord_of_the_dead_french_linux_181_03_00_45013.sh'
ARCHIVE_BASE_FR_0_MD5='9dd7899c4f5254bb555219530a32dc95'
ARCHIVE_BASE_FR_0_SIZE='3331947'
ARCHIVE_BASE_FR_0_VERSION='181.03.00-gog48040'
ARCHIVE_BASE_FR_0_URL='https://www.gog.com/game/iratus_lord_of_the_dead'

UNITY3D_NAME='Iratus'
UNITY3D_PLUGINS='
ScreenSelector.so'
## TODO: Check if the Steam library is required
UNITY3D_PLUGINS="$UNITY3D_PLUGINS
libsteam_api.so"

CONTENT_PATH_DEFAULT='data/noarch/game'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_BIN_ARCH='64'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
