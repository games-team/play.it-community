#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Lego Batman 1
# send your bug reports to contact@dotslashplay.it
###

script_version=20230930.1

PLAYIT_COMPATIBILITY_LEVEL='2.26'

GAME_ID='lego-batman-1'
GAME_NAME='Lego Batman: The Videogame'

ARCHIVE_BASE_0_NAME='setup_lego_batman_1.0_(18156).exe'
ARCHIVE_BASE_0_MD5='4d77c482455866c160b0b17f88e9aff0'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_lego_batman_1.0_(18156)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='49153c984f14b57c846d20cc038d117a'
ARCHIVE_BASE_0_SIZE='4500000'
ARCHIVE_BASE_0_VERSION='1.0-gog18156'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/lego_batman_the_videogame'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
*.exe
binkw32.dll
language_setup.ini'
CONTENT_GAME_DATA_FILES='
*.dat
language_setup.png
audio
movies'
CONTENT_DOC_DATA_FILES='
eula.rtf
readme.rtf'

APP_MAIN_EXE='legobatman.exe'

APP_LANGUAGE_ID="${GAME_ID}-language-setup"
APP_LANGUAGE_NAME="$GAME_NAME - Language setup"
APP_LANGUAGE_CAT='Settings'
APP_LANGUAGE_EXE='language_setup.exe'
APP_LANGUAGE_ICON='language_setup.png'

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

packages_generation

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
