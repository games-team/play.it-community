#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Batman: Arkham 1
# send your bug reports to contact@dotslashplay.it
###

script_version=20240501.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='batman-arkham-1'
GAME_NAME='Batman: Arkham Asylum'

ARCHIVE_BASE_0_NAME='setup_batman_arkham_asylum_goty_1.1_(38915).exe'
ARCHIVE_BASE_0_MD5='46dc5afd1cf4a41f4c910a8c43fe8023'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1_NAME='setup_batman_arkham_asylum_goty_1.1_(38915)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='59df55da8ffce48afd9a196c816769c3'
ARCHIVE_BASE_0_PART2_NAME='setup_batman_arkham_asylum_goty_1.1_(38915)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='8b7b283f4ea74c7f208bdd26c2f7a5b4'
ARCHIVE_BASE_0_SIZE='8700000'
ARCHIVE_BASE_0_VERSION='1.1-gog38915'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/batman_arkham_asylum_goty'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
binaries
*.dll'
CONTENT_GAME_DATA_FILES='
bmgame
engine'
CONTENT_DOC_DATA_FILES='
*.rtf'

WINE_DIRECT3D_RENDERER='dxvk'
WINE_DLLOVERRIDES_DEFAULT='winemenubuilder.exe,mshtml='
WINE_PERSISTENT_DIRECTORIES='
users/${USER}/Documents/Square Enix/Batman Arkham Asylum GOTY'
WINE_WINEPREFIX_TWEAKS='mono'
## Cursor is prevented to leave the game window, avoiding issues with mouse look.
WINE_WINETRICKS_VERBS='grabfullscreen=y'
## The game will throw an error on launch if PhysX is not installed.
WINE_WINETRICKS_VERBS="${WINE_WINETRICKS_VERBS:-} physx"

APP_MAIN_EXE='binaries/bmlauncher.exe'
APP_MAIN_ICON='binaries/shippingpc-bmgame.exe'
## Force the application type, or it will be mistaken for a Mono one.
APP_MAIN_TYPE='wine'
## Run the game binary from its parent directory.
APP_MAIN_PRERUN="${APP_MAIN_PRERUN:-}"'
# Run the game binary from its parent directory
cd "$(dirname "$APP_EXE")"
APP_EXE=$(basename "$APP_EXE")
'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
