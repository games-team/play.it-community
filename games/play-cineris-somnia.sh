#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 BetaRays
# SPDX-FileCopyrightText: © 2021 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Cineris Somnia
# send your bug reports to contact@dotslashplay.it
###

script_version=20240705.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='cineris-somnia'
GAME_NAME='Cineris Somnia'

## This game installer is no longer available from playism.com,
## as they closed their store down in favour of Steam.
ARCHIVE_BASE_0_NAME='CINERIS_SOMNIA_PLAYISM_20181109.zip'
ARCHIVE_BASE_0_MD5='c25ea529ffb12b0e055d05117c5bc24d'
ARCHIVE_BASE_0_SIZE='3200000'
ARCHIVE_BASE_0_VERSION='1.01-playism20181109'

UNITY3D_NAME='Cineris Somnia'

CONTENT_PATH_DEFAULT='20181109_101'
CONTENT_DOC_DATA_FILES='
*.txt'

WINE_PERSISTENT_DIRECTORIES='
users/${USER}/AppData/LocalLow/Nayuta Studio/Cineris Somnia'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
