#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Battle Chef Brigade
# send your bug reports to contact@dotslashplay.it
###

script_version=20230222.3

GAME_ID='battle-chef-brigade'
GAME_NAME='Battle Chef Brigade'

ARCHIVE_BASE_0='battle_chef_brigade_14725_624_23675.sh'
ARCHIVE_BASE_0_MD5='d35140bf757e387a2e47198f96356d00'
ARCHIVE_BASE_0_TYPE='mojosetup'
ARCHIVE_BASE_0_SIZE='1300000'
ARCHIVE_BASE_0_VERSION='14725.624-gog23675'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/battle_chef_brigade'

UNITY3D_NAME='BattleChefBrigade'

CONTENT_PATH_DEFAULT='data/noarch/game'
CONTENT_LIBS_BIN_PATH="${CONTENT_PATH_DEFAULT}/${UNITY3D_NAME}_Data/Plugins/x86_64"
CONTENT_LIBS_BIN_FILES="
ScreenSelector.so"
CONTENT_GAME_BIN_FILES="
${UNITY3D_NAME}.x86_64
${UNITY3D_NAME}_Data/Mono/x86_64"
CONTENT_GAME_DATA_FILES="
${UNITY3D_NAME}_Data/Managed
${UNITY3D_NAME}_Data/Mono/etc
${UNITY3D_NAME}_Data/Resources
${UNITY3D_NAME}_Data/StreamingAssets
${UNITY3D_NAME}_Data/globalgamemanagers
${UNITY3D_NAME}_Data/level?
${UNITY3D_NAME}_Data/level??
${UNITY3D_NAME}_Data/*.assets
${UNITY3D_NAME}_Data/*.assets.resS
${UNITY3D_NAME}_Data/*.resource"

PACKAGES_LIST='PKG_BIN PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libgdk_pixbuf-2.0.so.0
libglib-2.0.so.0
libgobject-2.0.so.0
libgtk-x11-2.0.so.0
libm.so.6
libpthread.so.0
librt.so.1
libstdc++.so.6
libz.so.1'

# Load common functions

target_version='2.21'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
