#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2023 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Scarf
# send your bug reports to contact@dotslashplay.it
###

script_version=20230706.1

GAME_ID='scarf'
GAME_NAME='Scarf'

ARCHIVE_BASE_0='setup_scarf_1.1_(64bit)_(53216).exe'
ARCHIVE_BASE_0_MD5='128b44bb5ba007934e91ad2a0ff6f693'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_PART1='setup_scarf_1.1_(64bit)_(53216)-1.bin'
ARCHIVE_BASE_0_PART1_MD5='ef5faab9ebab9772dd159d5cafde0311'
ARCHIVE_BASE_0_PART2='setup_scarf_1.1_(64bit)_(53216)-2.bin'
ARCHIVE_BASE_0_PART2_MD5='e388bd53a584a6bb9e58ae9b4af0b3b4'
ARCHIVE_BASE_0_PART3='setup_scarf_1.1_(64bit)_(53216)-3.bin'
ARCHIVE_BASE_0_PART3_MD5='000370e5c0bac838f8f50f9b2cfc3f18'
ARCHIVE_BASE_0_PART4='setup_scarf_1.1_(64bit)_(53216)-4.bin'
ARCHIVE_BASE_0_PART4_MD5='761da3606e1e2dfa741b5e8be51cd9f3'
ARCHIVE_BASE_0_SIZE='14000000'
ARCHIVE_BASE_0_VERSION='1.1-gog53216'
ARCHIVE_BASE_0_URL='https://www.gog.com/game/scarf'

UNREALENGINE4_NAME='scarf'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES="
engine
${UNREALENGINE4_NAME}/binaries
${UNREALENGINE4_NAME}/plugins
${UNREALENGINE4_NAME}.exe"
CONTENT_GAME_MOVIES_FILES="
${UNREALENGINE4_NAME}/content/movies"
CONTENT_GAME_DATA_FILES="
${UNREALENGINE4_NAME}"

HUGE_FILES_DATA="
${UNREALENGINE4_NAME}/content/paks/${UNREALENGINE4_NAME}-windowsnoeditor.pak"

WINE_DIRECT3D_RENDERER='dxvk'

WINE_PERSISTENT_DIRECTORIES="
users/\${USER}/AppData/Local/${UNREALENGINE4_NAME}/Saved"

APP_MAIN_EXE="${UNREALENGINE4_NAME}.exe"
APP_MAIN_ICON_WRESTOOL_OPTIONS='--type=14 --name=101'

PACKAGES_LIST='PKG_BIN PKG_MOVIES PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_MOVIES_ID="${GAME_ID}-movies"
PKG_MOVIES_DESCRIPTION='movies'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_MOVIES_ID $PKG_DATA_ID"
PKG_BIN_DEPENDENCIES_GSTREAMER_PLUGINS='
video/quicktime, variant=(string)iso'

# Load common functions

target_version='2.23'

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction 'SOURCE_ARCHIVE'

# Include game icons

PKG='PKG_DATA'
# shellcheck disable=SC2119
icons_inclusion

# Include game data

content_inclusion_default

# Delete temporary files

rm --recursive "${PLAYIT_WORKDIR}/gamedata"

# Write launchers

PKG='PKG_BIN'
# shellcheck disable=SC2119
launchers_write

# Build packages

write_metadata
build_pkg

# Print instructions

print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
