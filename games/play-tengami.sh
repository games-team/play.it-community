#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2019 BetaRays
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Tengami
# send your bug reports to contact@dotslashplay.it
###

script_version=20240610.1

PLAYIT_COMPATIBILITY_LEVEL='2.29'

GAME_ID='tengami'
GAME_NAME='Tengami'

## This game archive is no longer distributed,
## since Playism store closed down in favour of Steam.
ARCHIVE_BASE_0_NAME='Tengami Win0113.zip'
ARCHIVE_BASE_0_MD5='956f8bff6b8bdef8702e994dea23b8dc'
ARCHIVE_BASE_0_SIZE='830000'
ARCHIVE_BASE_0_VERSION='0113-playism'

CONTENT_PATH_DEFAULT='app'
CONTENT_GAME_BIN_FILES='
glew32.DLL
SDL2.dll
Tengami.exe'
CONTENT_GAME_DATA_FILES='
Binary'

USER_PERSISTENT_DIRECTORIES='
Save'

APP_MAIN_EXE='Tengami.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='32'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Use persistent storage for user data

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default
ARCHIVE_INNER_PATH="${PLAYIT_WORKDIR}/gamedata/Tengami Win/Tengami_Game_Win20150113.exe"
## Do not convert file paths to lowercase.
ARCHIVE_INNER_EXTRACTOR='innoextract'
ARCHIVE_INNER_EXTRACTOR_OPTIONS=' '
archive_extraction 'ARCHIVE_INNER'
rm --force "$(archive_path 'ARCHIVE_INNER')"

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
