#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2018 Mopi
# SPDX-FileCopyrightText: © 2020 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Dustforce
# send your bug reports to contact@dotslashplay.it
###

script_version=20241218.2

PLAYIT_COMPATIBILITY_LEVEL='2.31'

GAME_ID='dustforce'
GAME_NAME='Dustforce DX'

ARCHIVE_BASE_HUMBLE_0_NAME='Dustforce-Linux-2014-12-22.sh'
ARCHIVE_BASE_HUMBLE_0_MD5='e2fa7efd5b9ec64fc453f32480f63ad4'
ARCHIVE_BASE_HUMBLE_0_SIZE='409003'
ARCHIVE_BASE_HUMBLE_0_VERSION='2024.12.22-humble1'
ARCHIVE_BASE_HUMBLE_0_URL='https://www.humblebundle.com/store/dustforce-dx'

ARCHIVE_BASE_GOG_0_NAME='gog_dustforce_dx_2.2.0.5.sh'
ARCHIVE_BASE_GOG_0_MD5='5423718cf31f2602c9d0b30b79d1f409'
ARCHIVE_BASE_GOG_0_SIZE='409517'
ARCHIVE_BASE_GOG_0_VERSION='2014.12.22-gog2.2.0.5'
ARCHIVE_BASE_GOG_0_URL='https://www.gog.com/game/dustforce_dx'

CONTENT_PATH_DEFAULT_HUMBLE='data'
CONTENT_PATH_DEFAULT_GOG='data/noarch/game'
CONTENT_LIBS_BIN_FILES='
libcurl.so.3'
CONTENT_LIBS_BIN64_RELATIVE_PATH_HUMBLE='x86_64/lib64'
CONTENT_LIBS_BIN64_RELATIVE_PATH_GOG='lib64'
CONTENT_LIBS_BIN64_FILES="$CONTENT_LIBS_BIN_FILES"
CONTENT_LIBS_BIN32_RELATIVE_PATH_HUMBLE='x86/lib'
CONTENT_LIBS_BIN32_RELATIVE_PATH_GOG='lib'
CONTENT_LIBS_BIN32_FILES="$CONTENT_LIBS_BIN_FILES"
CONTENT_GAME_BIN64_RELATIVE_PATH_HUMBLE='x86_64'
CONTENT_GAME_BIN64_FILES='
Dustforce.bin.x86_64'
CONTENT_GAME_BIN32_RELATIVE_PATH_HUMBLE='x86'
CONTENT_GAME_BIN32_FILES='
Dustforce.bin.x86'
CONTENT_GAME_DATA_RELATIVE_PATH_HUMBLE='noarch'
CONTENT_GAME_DATA_FILES='
content
Dustforce.png'
CONTENT_DOC_DATA_RELATIVE_PATH_HUMBLE='noarch'
CONTENT_DOC_DATA_FILES='
Linux.README
ReadMe.txt'

APP_MAIN_EXE_BIN64='Dustforce.bin.x86_64'
APP_MAIN_EXE_BIN32='Dustforce.bin.x86'
APP_MAIN_ICON_HUMBLE='noarch/Dustforce.png'
APP_MAIN_ICON_GOG='Dustforce.png'

PACKAGES_LIST='
PKG_BIN64
PKG_BIN32
PKG_DATA'

PKG_BIN64_ARCH='64'
PKG_BIN32_ARCH='32'
PKG_BIN_DEPENDENCIES_SIBLINGS='
PKG_DATA'
PKG_BIN64_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
PKG_BIN32_DEPENDENCIES_SIBLINGS="$PKG_BIN_DEPENDENCIES_SIBLINGS"
## libidn.so.11 is required by the shipped libcurl.so.3 library
PKG_BIN_DEPENDENCIES_LIBRARIES='
libc.so.6
libdl.so.2
libgcc_s.so.1
libGL.so.1
libidn.so.11
libm.so.6
libopenal.so.1
libpthread.so.0
libSDL2-2.0.so.0
libstdc++.so.6
libz.so.1'
PKG_BIN64_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"
PKG_BIN32_DEPENDENCIES_LIBRARIES="$PKG_BIN_DEPENDENCIES_LIBRARIES"

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Run the default initialization actions

initialization_default "$@"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN64'
launchers_generation 'PKG_BIN32'

# Build packages

packages_generation
print_instructions

# Clean up

working_directory_cleanup

exit 0
