#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: © 2021 Mopi
# SPDX-FileCopyrightText: © 2022 Antoine Le Gonidec <vv221@dotslashplay.it>
set -o errexit

###
# Yoku's Island Express
# send your bug reports to contact@dotslashplay.it
###

script_version=20240522.1

PLAYIT_COMPATIBILITY_LEVEL='2.28'

GAME_ID='yokus-island-express'
GAME_NAME='Yoku’s Island Express'

ARCHIVE_BASE_1_NAME='setup_gog_yoku_latest_drm_free_210927_(50319).exe'
ARCHIVE_BASE_1_MD5='5a1241ffdda722df9c7c4c8fc1b661fe'
ARCHIVE_BASE_1_TYPE='innosetup'
ARCHIVE_BASE_1_SIZE='1300000'
ARCHIVE_BASE_1_VERSION='2021.09.27-gog50319'
ARCHIVE_BASE_1_URL='https://www.gog.com/game/yokus_island_express'

ARCHIVE_BASE_0_NAME='setup_yokus_island_express_aug_24_2018_205911_(24871).exe'
ARCHIVE_BASE_0_MD5='49d3a80f0a039e7b7a4d5fcd4860e445'
ARCHIVE_BASE_0_TYPE='innosetup'
ARCHIVE_BASE_0_SIZE='1300000'
ARCHIVE_BASE_0_VERSION='2018.08.24-gog24871'

CONTENT_PATH_DEFAULT='.'
CONTENT_GAME_BIN_FILES='
bugsplat64.dll
bugsplatrc64.dll
d3dcompiler_47.dll
yoku.exe
bssndrpt64.exe'
CONTENT_GAME_DATA_FILES='
data
processed'
CONTENT_DOC_DATA_FILES='
licenses'

WINE_PERSISTENT_DIRECTORIES="
users/\${USER}/AppData/Roaming/Villa Gorilla/Yoku's Island Express"
WINE_VIRTUAL_DESKTOP='auto'

APP_MAIN_EXE='yoku.exe'

PACKAGES_LIST='
PKG_BIN
PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data"
PKG_DATA_DESCRIPTION='data'

PKG_BIN_ARCH='64'
PKG_BIN_DEPS="$PKG_DATA_ID"

# Load common functions

PLAYIT_LIB_PATHS="
$PWD
${XDG_DATA_HOME:="${HOME}/.local/share"}/play.it
/usr/local/share/games/play.it
/usr/local/share/play.it
/usr/share/games/play.it
/usr/share/play.it"

if [ -z "$PLAYIT_LIB2" ]; then
	for playit_lib_path in $PLAYIT_LIB_PATHS; do
		if [ -e "${playit_lib_path}/libplayit2.sh" ]; then
			PLAYIT_LIB2="${playit_lib_path}/libplayit2.sh"
			break
		fi
	done
fi
if [ -z "$PLAYIT_LIB2" ]; then
	printf '\n\033[1;31mError:\033[0m\n'
	printf 'libplayit2.sh not found.\n'
	exit 1
fi
# shellcheck source=libplayit2.sh
. "$PLAYIT_LIB2"

# Extract game data

archive_extraction_default

# Include game data

content_inclusion_icons 'PKG_DATA'
content_inclusion_default

# Write launchers

launchers_generation 'PKG_BIN'

# Build packages

packages_generation
print_instructions

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

exit 0
